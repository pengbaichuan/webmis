function tinctureBar(colorProfile){
    switch ( colorProfile ) {
        case "development":
            // green bar
            var color = 'linear-gradient(to bottom, rgba(98,125,77,1), rgba(31,59,8,1))'
            $(".navbar-inner:first").css("background-image", color );
            $(".navbar-inner:first .dropdown-toggle").css({"background-image":color,"color":"white"});
            $(".navbar .brand").first().css("color","black");
            break;
        case "staging":
            // red bar
            var color = 'linear-gradient(to bottom, #EE5F5B, #BD362F)'
            $(".navbar-inner:first").css("background-image", color );
            $(".navbar-inner:first .dropdown-toggle").css({"background-image":color,"color":"white"});
            $(".navbar .brand").first().css("color","black");
            break;
        case "test":
            // red bar
            var color = 'linear-gradient(to bottom, CornFlowerBlue, DarkTurquoise)'
            $(".navbar-inner:first").css("background-image", color );
            $(".navbar-inner:first .dropdown-toggle").css({"background-image":color,"color":"white"});
            $(".navbar .brand").first().css("color","white");
            break;
        case "gray":
            var color = 'linear-gradient(to bottom, gray, #A09C9C)'
            $(".navbar-inner:first").css("background-image", color );
            $(".navbar-inner:first .dropdown-toggle").css({"background-image":color,"color":"white"});
            $(".navbar .brand").first().css("color","white");
            break;
        case "blue":
            var color = 'linear-gradient(to bottom, blue, #061764)'
            $(".navbar-inner:first").css("background-image", color );
            $(".navbar-inner:first .dropdown-toggle").css({"background-image":color,"color":"white"});
            $(".navbar .brand").first().css("color","white");
            break;
        case "darkblue":
            var color = 'linear-gradient(to bottom, #061764, #010D43)'
            $(".navbar-inner:first").css("background-image", color );
            $(".navbar-inner:first .dropdown-toggle").css({"background-image":color,"color":"white"});
            $(".navbar .brand").first().css("color","white");
            break;
        case "orange":
            var color = 'linear-gradient(to bottom, orange, #FF8C06)'
            $(".navbar-inner:first").css("background-image", color );
            $(".navbar-inner:first .dropdown-toggle").css({"background-image":color,"color":"white"});
            $(".navbar .brand").first().css("color","black");
            break;
        case "purple":
            var color = 'linear-gradient(to bottom, purple, #7C25C9)'
            $(".navbar-inner:first").css("background-image", color );
            $(".navbar-inner:first .dropdown-toggle").css({"background-image":color,"color":"white"});
            $(".navbar .brand").first().css("color","black");
            break;
        case "red":
            var color = 'linear-gradient(to bottom, red, #D21F1F)'
            $(".navbar-inner:first").css("background-image", color );
            $(".navbar-inner:first .dropdown-toggle").css({"background-image":color,"color":"white"});
            $(".navbar .brand").first().css("color","black");
            break;
        case "pink":
            var color = 'linear-gradient(to bottom, pink, #FF85C2)'
            $(".navbar-inner:first").css("background-image", color );
            $(".navbar-inner:first .dropdown-toggle").css({"background-image":color,"color":"#fff"});
            $(".navbar .brand").first().css("color","#fff");
            break;
        case "green":
            var color = 'linear-gradient(to bottom, green, #4FA64B)'
            $(".navbar-inner:first").css("background-image", color );
            $(".navbar-inner:first .dropdown-toggle").css({"background-image":color,"color":"#fff"});
            $(".navbar .brand").first().css("color","#fff");
            break;
        case "yellow":
            var color = 'linear-gradient(to bottom, #FFD500, #E1E106)'
            $(".navbar-inner:first").css("background-image", color );
            $(".navbar-inner:first .dropdown-toggle").css({"background-image":color,"color":"blue"});
            $(".navbar .brand").first().css("color","blue");
            break;
        case "lime":
            var color = 'linear-gradient(to bottom, #26E91C, #0DFF00)'
            $(".navbar-inner:first").css("background-image", color );
            $(".navbar-inner:first .dropdown-toggle").css({"background-image":color,"color":"black"});
            $(".navbar .brand").first().css("color","black");
            break;
        case "smoke":
            var color = 'linear-gradient(to bottom, #D8D8D8, #FAFAFA)'
            $(".navbar-inner:first").css("background-image", color );
            $(".navbar-inner:first .dropdown-toggle").css({"background-image":color,"color":"black"});
            $(".navbar .brand").first().css("color","black");
            break;
        case "skyblue":
            var color = 'linear-gradient(to bottom, #0174DF, #2ECCFA)'
            $(".navbar-inner:first").css("background-image", color );
            $(".navbar-inner:first .dropdown-toggle").css({"background-image":color,"color":"white"});
            $(".navbar .brand").first().css("color","black");
            break;
        case "production":
        default:
            // production
            var color = 'linear-gradient(to bottom, #222222, #111111)'
            $(".navbar-inner:first").css("background-image",color );
            $(".navbar-inner:first .dropdown-toggle").css({"background-image":color,"color":"gray"});
            $(".navbar .brand").first().css("color","gray");
    }
}