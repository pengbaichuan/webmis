// Move the formfields to the search form on change of colum fields
function isFunction(possibleFunction) {
    return typeof(possibleFunction) === typeof(Function);
}

if (isFunction("addColumnSearchtoForm")){
// script already loaded
} else {
    function addColumnSearchtoForm(){
        var elarr = $("[id^=column_search_field]");
        $("select[id^=column_select_field]").change(function(){
            var new_id = $(this).attr('id').replace("select","search");
            $("#"+new_id).val($(this).val());
            // Auto submit selects
            $("form:first").submit()
        })
        $("input[id^=column_search_field]").keyup(function(event){
            if(event.which == 13){

                $("form:first").submit();
            }
        });
        if (elarr.length > 0){
            $("form:first").submit(function( event ) {
                elarr.clone(true).appendTo("form:first").hide();
            });
        }
    }
    addColumnSearchtoForm();
}

// Select text within a element
function selectText(element,append) {
    var doc = document
    , text = doc.getElementById(element)
    , range, selection
    ;
    if (doc.body.createTextRange) { //ms
        range = doc.body.createTextRange();
        range.moveToElementText(text);
        range.select();
    } else if (window.getSelection) { //all others
        selection = window.getSelection();
        range = doc.createRange();
        range.selectNodeContents(text);
        if (typeof append === 'undefined') {
            selection.removeAllRanges();
        } else {
            if ( append === false) {
                selection.removeAllRanges();
            }
        }
        selection.addRange(range);
    }
}

// Transfer scaffold error div into bootstrap alert
$("#error_explanation").addClass("alert alert-danger");