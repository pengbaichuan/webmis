class AllocationExceptionsController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"

  # GET /allocation_exceptions
  # GET /allocation_exceptions.json
  def index
    @allocation_exceptions = AllocationException.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @allocation_exceptions }
    end
  end

  # GET /allocation_exceptions/1
  # GET /allocation_exceptions/1.json
  def show
    @allocation_exception = AllocationException.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @allocation_exception }
    end
  end

  # GET /allocation_exceptions/new
  # GET /allocation_exceptions/new.json
  def new
    @allocation_exception = AllocationException.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @allocation_exception }
    end
  end

  # GET /allocation_exceptions/1/edit
  def edit
    @allocation_exception = AllocationException.find(params[:id])
  end

  # POST /allocation_exceptions
  # POST /allocation_exceptions.json
  def create
    @allocation_exception = AllocationException.new(strong_params)

    respond_to do |format|
      if @allocation_exception.save
        format.html { redirect_to @allocation_exception, notice: 'Allocation exception was successfully created.' }
        format.json { render json: @allocation_exception, status: :created, location: @allocation_exception }
      else
        format.html { render action: "new" }
        format.json { render json: @allocation_exception.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /allocation_exceptions/1
  # PUT /allocation_exceptions/1.json
  def update
    @allocation_exception = AllocationException.find(params[:id])

    respond_to do |format|
      if @allocation_exception.update_attributes(strong_params)
        format.html { redirect_to @allocation_exception, notice: 'Allocation exception was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @allocation_exception.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /allocation_exceptions/1
  # DELETE /allocation_exceptions/1.json
  def destroy
    @allocation_exception = AllocationException.find(params[:id])
    @allocation_exception.destroy

    respond_to do |format|
      format.html { redirect_to allocation_exceptions_url }
      format.json { head :no_content }
    end
  end
end
