module BacktickSSH
  def `(cmd)
    ssh(cmd)
  end
  
  def ssh(cmd,server_ip='msdes040', user='johan.hendrikx', port=22)
    stdout = ""
    Net::SSH.start( @server_ip, @user, :port => @port) do |session|
      session.exec!(cmd) do |channel, stream, data|
        stdout << data if stream == :stdout
      end
      return stdout
    end
  end
end

class ApiController < ApplicationController
  include BacktickSSH
  def load

    server= params[:id]
    @server_ip = params[:id]
    @user = ENV['prod_user']
    @port = 22
    
    output = `free -m`
    if output.size == 0
      throw
    end

    memory = output.scan(/buffers\/cache\:[ ]*([0-9]*)[ ]*([0-9]*)/).flatten
    if !memory[0] || !memory[1]
      throw
    end

    load = `cat /proc/loadavg`.scan(/[^ ]*/)
    
    idle = "86400000"
    cpu = `cat /proc/cpuinfo`
    
    cores = cpu.scan(/cores\t: (\d)/).uniq.flatten.to_s.to_i
    model = cpu.scan(/model name\t: (.*)/).uniq.to_s
    
    begin
      idlebin = File.join(RAILS_ROOT, "lib", "bin", "idle")
      idle = `chmod +x #{idlebin};#{idlebin}`.gsub("ms", "").strip
    rescue => e
    end
    
    if load.size < 3
      throw
    end

    load_1 = load[0]
    load_5 = load[1]
    load_15 = load[2]

    total_memory = memory[0].to_i + memory[1].to_i
    used_memory = memory[0]
    free_memory = memory[1]

    render :js => {:total_memory => total_memory,
      :free_memory  => memory[1].to_i,
      :used_memory  => memory[0].to_i,
      :load_1 => load_1.to_f,
      :load_5 => load_5.to_f,
      :load_15 => load_15.to_f,
      :idle => idle,
      :cores => cores,
      :cpu_model => model,
      :operating_user => "#{current_user.user_name}",
      :available => ENV['JOB_SCHEDULING_ALLOWED'],
      :running_jobs => 1}.to_json,
      :status       => 200
  rescue => e
    puts e.message
    puts e.backtrace
    error(500, "UNSUPPORTED_PLATFORM", "Scheduler API not Supported")
  end

  def show
    render :text => 'bla'  
  end


  def async_steps
    begin
      throw # async steps not yet suppported
    rescue
      error(500, code, message)
    end
  end

  def execute_job
  
    # should execute dakota asynchronously on remote server
    message = ""
    code = 0

    begin
      data = ActiveSupport::JSON.decode(request.body.read)

      if !data["jobid"]
        message = "JobId not provided"
        code = 500
        throw
      end

      testjob = TestJob.find(data["jobid"])
    
      if !testjob
        message = "JobId not found"
        code = 500
        throw
      else
        # triggered distributed using api, so always share...
        testjob.shared_yn = 1
        testjob.save
      end

      if data["addon_parameters"]
        # create run to run only part of full job ... this will enable load balancing
      end

      run = testjob.trigger(hostname)
      render :js => {:runid => run.id}.to_json,
        :status       => 200

    rescue => e
      puts e.message
      puts e.backtrace
      error(500, code, message)
    end
  end

  def status
    message = ""
    code = 0
  
    begin
      #data = ""
      data = ActiveSupport::JSON.decode(request.body.read)
    
      if !data["runid"]
        message = "RunId not provided"
        code = 500
        throw
      end
    
      testrun = TestRun.find(data["runid"])

      if !testrun
        message = "RunId not found"
        code = 500
        throw
      end

      render :js => {:runid => testrun.id,
        :status => testrun.status,
        :server => testrun.server}.to_json,
        :status       => 200

    rescue => e
      puts e.message
      puts e.backtrace
      error(500, code, message)
    end
  end

  def can_run_job
    #always accept job
    render :js => {:result => true}
  end

  def error(status, code, message)
    render :js => {:response_type => "ERROR", :response_code => code, :message => message}.to_json, :status => status
  end


 
end