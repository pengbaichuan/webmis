class ApiLogsController < ApplicationController
  before_filter :authorize
  authorize_resource
  helper_method :sort_column, :sort_direction, :search
  layout 'webmis'
  
  # GET /api_logs
  # GET /api_logs.json
  def index
    
    if params[:search] && params[:search].size > 0
      @api_logs = ApiLog.where('message like ? or params like ?',"%#{params[:search]}%","%#{params[:search]}%").paginate(:per_page => 30, :page => params[:page]).order(sort_column + " " + sort_direction)
    else
      @api_logs = ApiLog.paginate(:per_page => 30, :page => params[:page]).order(sort_column + " " + sort_direction)
    end
    if request.xhr?
      render(:partial => "api_log", :collection => @api_logs)
    end
  end

  # GET /api_logs/1
  # GET /api_logs/1.json
  def show
    @api_log = ApiLog.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @api_log }
    end
  end

  # GET /api_logs/new
  # GET /api_logs/new.json
  def new
    @api_log = ApiLog.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @api_log }
    end
  end

  # GET /api_logs/1/edit
  def edit
    @api_log = ApiLog.find(params[:id])
  end

  # POST /api_logs
  # POST /api_logs.json
  def create
    @api_log = ApiLog.new(strong_params)

    respond_to do |format|
      if @api_log.save
        format.html { redirect_to @api_log, notice: 'Api log was successfully created.' }
        format.json { render json: @api_log, status: :created, location: @api_log }
      else
        format.html { render action: "new" }
        format.json { render json: @api_log.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /api_logs/1
  # PUT /api_logs/1.json
  def update
    @api_log = ApiLog.find(params[:id])

    respond_to do |format|
      if @api_log.update_attributes(strong_params)
        format.html { redirect_to @api_log, notice: 'Api log was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @api_log.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /api_logs/1
  # DELETE /api_logs/1.json
  def destroy
    @api_log = ApiLog.find(params[:id])
    @api_log.destroy

    respond_to do |format|
      format.html { redirect_to api_logs_url }
      format.json { head :no_content }
    end
  end
  
  
 private  
  def sort_column  
    params[:sort] || "api_logs.created_at"  
  end  
    
  def sort_direction  
    params[:direction] || "desc"  
  end
  
  def search
    params[:search] || ""
  end
end
