class ApplicationController < ActionController::Base

  layout "webmis"
  #protect_from_forgery # :secret => 'ba6ea71709c5f702b27200265503d9e1'
  protect_from_forgery with: :exception
  self.allow_forgery_protection = false
  
  before_action do
    resource = controller_name.singularize.to_sym
    method = "#{resource}_params"
    params[resource] &&= send(method) if respond_to?(method, true)
  end

  before_filter :set_project_id

  rescue_from CanCan::AccessDenied do |exception|
    # redirect_to main_app.root_url, :error => exception.message
    if request.referer
      controller_name = ''
      action_name = ''
      required_roles = ''
      if request.filtered_parameters['controller']
        controller_name = request.filtered_parameters['controller']
      end
      if request.filtered_parameters['action']
        action_name = request.filtered_parameters['action']
      end
      if !controller_name.blank? && !action_name.blank?
        role_actions = RoleAction.where(:controller_name => controller_name, :action_name => action_name)
        if role_actions.empty?
          role_actions = RoleAction.defaults.where(:controller_name => controller_name)
        end
        required_roles = role_actions.collect{|ra|ra.role.title}.uniq.join(' or ')
      end
      if !required_roles.blank?
        required_roles = "Role '" + required_roles + "' required."
      end
      flash[:error] = "Access denied. " + required_roles
      redirect_to request.referer, :error => exception.message
    else
      redirect_to :controller => 'main', :action => 'index'
    end
  end

#  rescue_from Exception, :with => :render_simple_error if Rails.env.staging?
#  # error reports only on staging without loosing production functionality like cache
#  def render_simple_error(e)
#    render :text => "#{e.message} -- #{e.class}<br/>#{e.backtrace.join("<br/>")}",layout: 'simple_error'
#  end
  
  def authorize
    session[:ref] = request.referer
    if  params[:key]
      keyrecord = ApiKey.find_by_key(params[:key])
      if keyrecord
        if login(keyrecord.user.user_name)
          flash.keep
          return true
        else
          flash[:error] = "Invalid credentials, please check your apikey."
          redirect_to :controller => 'login', :action => 'index'
        end
      else
        flash[:error] = "Invalid credentials, please check your apikey."
        redirect_to :controller => 'login', :action => 'index'
      end
    else
      if (!session[:username] and !session[:group]) or session[:group] == 4
        flash[:error] = "Please login first."
        session[:ref] = request.fullpath
        redirect_to :controller => 'login', :action => 'index'
      end
    end
  end  
  
  def permission_denied
    ref = request.fullpath
    flash[:error] = "Access Denied, please consult administrator."
    redirect_to :controller => 'main', :action => :denied, :bl => ref
  end
  
  def current_user
   @current_user ||= User.find(session[:user_id]) if session[:user_id]
   @current_user.current_project_id = session[:project_id] if session[:user_id] && session[:project_id]
   return @current_user
  end
  helper_method :current_user
 
  def set_host_from_request_for_mail
    ActionMailer::Base.default_url_options = { :host => request.host_with_port }
  end
  
#  alias :std_redirect_to :redirect_to
#  def redirect_to(*args)
#    flash.keep
#    std_redirect_to *args
#  end

  def is_int?(str)
    begin
      !!Integer(str)
    rescue ArgumentError, TypeError
      false
    end
  end

  def column_search_query
    return_hash = {}
    unless params[:column_search_field].blank?
      column_search_string_arr = []
      column_search_value_arr  = []
      params[:column_search_field].each do |k,v|
        unless v.blank?
          table = k.gsub('-','.')
          exact_match = table.gsub!(/^exact\./, '') || table.gsub!(/^select\./, '')
          if exact_match
            column_search_string_arr << "#{table} = ?"
            column_search_value_arr  << v.strip
          else
            column_search_string_arr << "#{table} LIKE ?"
            column_search_value_arr  << "%#{v.strip}%"
          end
        end
      end
      unless column_search_string_arr.empty?
        return_hash["query"] = "(#{column_search_string_arr.join(' AND ')})"
        return_hash["values"]= column_search_value_arr
      end
    end
    return_hash
  end

  def generate_id_sequence_cookie(obj,page_number=1)
    cookie_name = "id_sequence_#{params[:controller]}"
    list = obj.collect{|o|o.id}
    if page_number.nil? || (is_int?(page_number.to_s) && page_number.to_i <= 1)
      cookies[cookie_name] = {:value => list.join(','), :expires => Time.now + 3600}
    else
      if cookies[cookie_name]
        append_list = cookies[cookie_name].split(',') << list
      else
        append_list = [ list ]
      end
      cookies[cookie_name] = {:value => append_list.join(','), :expires => Time.now + 3600}
    end
  end

  def set_project_id
    Thread.current[:project_id] = session[:project_id]
  end

  def env_production?
    if session[:environment] == "production"
      return true
    else
      return false
    end
  end

  def editing_not_allowed(obj,msg="Editing of this record is not allowed.")
    flash[:error] = msg
      if request.env["HTTP_REFERER"]
        redirect_to :back
      else
        redirect_to :controller => obj.class.to_s.tableize
      end
  end

  def render_file_tree_modal(path)
    @path = path
    render :partial => "shared/file_tree"
  end
  helper_method :render_file_tree_modal

  def render_file_browse_modal(path)
    @path = path
    render :partial => "shared/file_browse_modal"
  end
  helper_method :render_file_browse_modal

  def get_file_tree_json(path)
   FileSystemUtil.directory_hash(path.to_s).to_json.html_safe
  end
  helper_method :get_file_tree_json

 def session_to_default_project
   session[:project_id] = Project.default.id
   session[:color_profile] = Project.default.color_profile
   session[:environment] = Project.default.environment.name
 end

#merged from backoffice project
  def log_to_apilog(level, message)
    ApiLog.create(
      :controller => request.env['action_dispatch.request.parameters']["controller"],
      :action => request.env['action_dispatch.request.parameters']["action"],
      :ip => request.env['REMOTE_ADDR'],
      :session => session[:session_id],
      :message => message,
      :user => 'anonymous',
      :level => level,
      :params => request.env["action_dispatch.request.request_parameters"].to_json
          
      # populate other useful attributes here
    )
  end
  
  def infolog(message)
    log_to_apilog("INFO", message)
  end
  
  def errorlog(message)
    log_to_apilog("ERROR", message)
  end

  def rm_surrounding_quotes(params_h={})
    # Remove surrounding quotes
    params_h.each do |k,v|
      value = v.gsub(/\'\z|"\z/,'').gsub(/\A\'|\A\"/,'')
      params_h[k] = value
    end
    return params_h
  end

  def strong_params(form_params=nil)
    form_params = form_params ? form_params.to_sym : params[:controller].singularize.to_sym
    params.require(form_params).permit!
  end

end
