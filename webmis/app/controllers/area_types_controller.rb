class AreaTypesController < ApplicationController
  before_filter :authorize
  authorize_resource

  layout "webmis"

  def index
    @area_types = AreaType.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @area_types }
    end
  end

  # GET /area_types/1
  # GET /area_types/1.xml
  def show
    @area_type = AreaType.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @area_type }
    end
  end

  # GET /area_types/new
  # GET /area_types/new.xml
  def new
    @area_type = AreaType.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @area_type }
    end
  end

  # GET /area_types/1/edit
  def edit
    @area_type = AreaType.find(params[:id])
  end

  # POST /area_types
  # POST /area_types.xml
  def create
    @area_type = AreaType.new(strong_params)

    respond_to do |format|
      if @area_type.save
        flash[:notice] = 'AreaType was successfully created.'
        format.html { redirect_to(@area_type) }
        format.xml  { render :xml => @area_type, :status => :created, :location => @area_type }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @area_type.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /area_types/1
  # PUT /area_types/1.xml
  def update
    @area_type = AreaType.find(params[:id])

    respond_to do |format|
      if @area_type.update_attributes(strong_params)
        flash[:notice] = 'AreaType was successfully updated.'
        format.html { redirect_to(@area_type) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @area_type.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /area_types/1
  # DELETE /area_types/1.xml
  def destroy
    @area_type = AreaType.find(params[:id])
    @area_type.destroy

    respond_to do |format|
      format.html { redirect_to(area_types_url) }
      format.xml  { head :ok }
    end
  end
end
