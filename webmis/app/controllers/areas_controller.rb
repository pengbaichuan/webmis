class AreasController < ApplicationController

  before_filter :authorize
  authorize_resource
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search

  # index not used as this is handled by the regions controller
  def index
    if params[:reset]
      params.delete :q
      params.delete :reset
      
      params[:sort] = 'name'
      params[:direction] = 'asc'
    end
    conditions = "1=1 "
    conditions_string_ary = []
    conditions_param_values = []
    
    unless params[:q].blank?
      conditions_string_ary << " and (regions.name LIKE ? OR country_iso_code LIKE ? or country_iso3_code LIKE ? OR area_code LIKE ?)"
      4.times do
        conditions_param_values << "\%#{params[:q].strip}\%"
      end
    end
    conditions = conditions + conditions_string_ary.join(" ")
    @areas = Area.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])
    
    if request.xhr?
      flash.keep[:notice]
      render(:partial => "area", :collection => @areas)
    end      
  end

  def show
    @area = Area.find(params[:id])
  end

  def new
    @area = Area.new
    @parents = []
    Region.where(:isactive => true).group_by { |m| m.ruby_type }.each do |c|
      @parents << [[c[0]],c[1].collect {|s| [s.name,s.id]}]
    end
  end

  def edit
    @area = Area.find(params[:id])
    @parents = []
    Region.where(:isactive => true).group_by { |m| m.ruby_type }.each do |c|
      @parents << [[c[0]],c[1].collect {|s| [s.name,s.id]}]
    end
  end

  def create
    @parents = []
    @area = Area.new(strong_params)
    @parents = []
    Region.where(:isactive => true).group_by { |m| m.ruby_type }.each do |c|
      @parents << [[c[0]],c[1].collect {|s| [s.name,s.id]}]
    end
    @area.display_type_id = AreaType.where(:name => 'Area').last.id

    respond_to do |format|
      if @area.save
        @area.app_module_ids = [AppModule.where(:abbr => 'MIS').last.id]
        format.html { redirect_to(@area, :notice => 'Area was successfully created.') }
        format.xml  { render :xml => @area, :status => :created, :location => @area }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @area.errors, :status => :unprocessable_entity }
      end
    end    
  end

  def update
    @area = Area.find(params[:id])
    @area.app_module_ids = [AppModule.where( :abbr => 'MIS').last.id]
    @area.display_type_id = AreaType.where(:name => 'Area').last.id
    @parents = []
    Region.where(:isactive => true).group_by { |m| m.ruby_type }.each do |c|
      @parents << [[c[0]],c[1].collect {|s| [s.name,s.id]}]
    end
    respond_to do |format|
      if @area.update_attributes(strong_params)
        format.html { redirect_to(@area, :notice => 'Area was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @area.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @area = Area.find(params[:id])
    @area.destroy
    flash[:notice] = "Area succesfully deleted!"
    respond_to do |format|
      format.html { redirect_to(areas_url) }
      format.xml  { head :ok }
    end
  end

  def generate_dakota_id
    render :text => Region.generate_dakota_id
  end
  
  private 
  def sort_column
    params[:sort] || "name"
  end
  
  def sort_direction
    params[:direction] || "asc"
  end
  
  def search
    params[:search] || ""
  end  
end
