# coding: utf-8

class ArticleLicensesController < ApplicationController
  before_filter :authorize
  authorize_resource
  # GET /article_licenses
  # GET /article_licenses.xml
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search

  def index
    @external_contacts = {}
    ArticleLicense.select('external_contact_id').uniq.each do |id|
      ec = ExternalContact.where("id='#{id.external_contact_id}'").first
      @external_contacts[ec.company.name] = ec.id
    end
    conditions = '1=1'
    if params[:reset]
      params.delete :search
      params.delete :search_company
      params.delete :search_label
      params.delete :reset

      params[:sort] = 'external_contact'
      params[:direction] = 'asc'
    end

    conditions_string_ary = []
    conditions_param_values = []

    company = params[:search_company]
    unless company.blank?
      conditions_string_ary << ' and (article_licenses.external_contact_id = ?)'
      conditions_param_values << company
    end

    label = params[:search_label]
    unless label.blank?
      conditions_string_ary << ' and (article_licenses.label = ?)'
      conditions_param_values << label
    end

    unless params[:search].blank?
      conditions_string_ary << " and (article_licenses.label LIKE ?
              or high_level_boms.internal_reference LIKE ?"
      2.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
      found_contacts = @external_contacts.select{|k,v| k.match(params[:search].strip)}
      if found_contacts.empty?
        conditions_string_ary << ")"
      else
        conditions_string_ary << " or article_licenses.external_contact_id in ('#{found_contacts.values.join(',')}'))"
      end

    end

    conditions = conditions + conditions_string_ary.join(" ")

    if (sort_column == 'external_contact')
      @article_licenses = ArticleLicense.includes(:high_level_bom).where([conditions] + conditions_param_values).references(:high_level_boms).sort_by{ |r| r.external_contact.company.name }
      @article_licenses.reverse! if sort_direction == 'desc'
    else
      @article_licenses = ArticleLicense.includes(:high_level_bom).where([conditions] + conditions_param_values).references(:high_level_boms).order(sort_column + ' ' +sort_direction)
    end

    @article_licenses = @article_licenses.paginate(per_page: 100, page: params[:page])
    generate_id_sequence_cookie(@article_licenses,params[:page])

    if params[:search]
      flash.now[:notice] = @article_licenses.total_entries.to_s + " records found with #{params[:search]}."
    end

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @article_licenses }
    end
  end

  def license_fee_report
    @external_contacts = {}
    ArticleLicense.select('external_contact_id').uniq.each do |id|
      ec = ExternalContact.where("id='#{id.external_contact_id}'").first
      @external_contacts[ec.company.name] = ec.id
    end
    if params[:reset] || ! params[:search_company]
      params.delete :search_label
      params.delete :reset
      params[:search_company] = @external_contacts.values.last
      params[:search_date] = Date.today - 1.months
    else
      params[:search_date] = Date.new(params[:search_year].to_i,params[:search_month].to_i)
    end

    po = PurchaseOrder.purchase_license_fee_by_report(params[:search_company],params[:search_date],current_user.id,params[:search_label])
    @article_licenses = po[1]
    @license_fee_reports = po[2]
  end

  def html2pdf_license_fee_po
    @date = Date.strptime(params[:date], "%Y-%m-%d")
#    @external_contact = ExternalContact.find(params[:company])
#    @article_licenses = find_article_licenses(params[:company],params[:label])
#    @license_fee_reports = find_shipping_orderlines(params[:company],params[:label],@date)
# purchase_license_fee_by_report(external_contact_id,period_date,user_id,label="",po_reference="n.a.",remarks="",create=false)
    po = PurchaseOrder.purchase_license_fee_by_report(params[:company],@date,current_user.id,params[:label])
    @external_contact = po[0]
    @article_licenses = po[1]
    @license_fee_reports = po[2]

    @cu = current_user
    @imagepath = 'http://' + ENV["host"] + "/webmis/images/mapscapelogo4docs.png"
    t = Time.now
    ts = t.strftime('%Y%m%d')

    PurchaseOrder.purchase_license_fee_by_report(params[:company],@date,current_user.id,params[:label],params[:po_reference],params[:remarks],true)
    respond_to do |format|
          format.html
          format.pdf do
            render :pdf => "purchase_order_#{params[:po_reference]}_#{ts}.pdf",
              :margin => { :bottom => 20 },
              :footer => { :html => { :template => '/pdf_footer', :formats => [:html] }},
              :encoding => "utf8",:notice => 'Purchase order is created.'
          end
    end
  end

  # GET /article_licenses/1
  # GET /article_licenses/1.xml
  def show
    @article_license = ArticleLicense.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @article_license }
    end
  end

  # GET /article_licenses/new
  # GET /article_licenses/new.xml
  def new
    @article_license = ArticleLicense.new
    @article_license.active_yn = true
    @article_license.currency = "€"

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @article_license }
    end
  end

  # GET /article_licenses/1/edit
  def edit
    @article_license = ArticleLicense.find(params[:id])
  end

  # POST /article_licenses
  # POST /article_licenses.xml
  def create
    @article_license = ArticleLicense.new(strong_params)

    respond_to do |format|
      if @article_license.save
        format.html { redirect_to(@article_license, :notice => 'ArticleLicense was successfully created.') }
        format.xml  { render :xml => @article_license, :status => :created, :location => @article_license }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @article_license.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /article_licenses/1
  # PUT /article_licenses/1.xml
  def update
    @article_license = ArticleLicense.find(params[:id])

    respond_to do |format|
      if @article_license.update_attributes(strong_params)
        format.html { redirect_to(@article_license, :notice => 'ArticleLicense was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @article_license.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /article_licenses/1
  # DELETE /article_licenses/1.xml
  def destroy
    @article_license = ArticleLicense.find(params[:id])
    @article_license.destroy

    respond_to do |format|
      format.html { redirect_to(article_licenses_url) }
      format.xml  { head :ok }
    end
  end

  private
  def sort_column
    params[:sort] || "external_contact"
  end

  def sort_direction
    params[:direction] || "asc"
  end

  def search
    params[:search] || ""
  end

end
