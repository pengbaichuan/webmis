# coding: utf-8

class ArticlesController < ApplicationController
  before_filter :authorize
  authorize_resource

  layout 'webmis'
  helper_method :sort_column, :sort_direction, :search

  def index
    conditions = '1=1'
    if params[:reset]
      params.delete :search
      params.delete :search_bom
      params.delete :search_active
      params.delete :reset

      params[:sort] = 'customer_reference'
      params[:direction] = 'asc'
    end

    conditions_string_ary = []
    conditions_param_values = []

    bom = params[:search_bom]
    unless bom.blank?
      conditions_string_ary << ' and (articles.high_level_bom_id = ?)'
      conditions_param_values << bom
    end

    active = params[:search_active]
    unless active.blank?
      conditions_string_ary << " and (articles.active = ?)"
      conditions_param_values << active
    end

    unless params[:search].blank?
      conditions_string_ary << ' and (articles.customer_reference like ?
                                    or articles.name like ?
                                    or high_level_boms.internal_reference like ?)'
      3.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
    end

    conditions = conditions + conditions_string_ary.join(" ")

    @articles = Article.includes(:high_level_bom).where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(per_page: 100, page: params[:page])

    if params[:search]
      flash.now[:notice] = @articles.total_entries.to_s + " records found with #{params[:search]}."
    end

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @articles }
    end
  end

  # GET /articles/1
  # GET /articles/1.xml
  def show
    @article = Article.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @article }
    end
  end

  # GET /articles/new
  # GET /articles/new.xml
  def new
    @article = Article.new
    @article.purchase_currency = "€"
    @article.currency = "€"
    @article.created_by_user_id = current_user.id.to_s

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @article }
    end
  end

  # GET /articles/1/edit
  def edit
    @article = Article.find(params[:id])
  end

  # POST /articles
  # POST /articles.xml
  def create
    @article = Article.new(strong_params)
    @article.created_by_user_id = current_user.id.to_s

    respond_to do |format|
      if @article.save
        format.html { redirect_to(@article, :notice => 'Article was successfully created.') }
        format.xml  { render :xml => @article, :status => :created, :location => @article }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @article.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /articles/1
  # PUT /articles/1.xml
  def update
    @article = Article.find(params[:id])

    respond_to do |format|
      if @article.update_attributes(strong_params)
        format.html { redirect_to(@article, :notice => 'Article was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @article.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.xml
  def destroy
    @article = Article.find(params[:id])
    @article.destroy

    respond_to do |format|
      format.html { redirect_to(articles_url) }
      format.xml  { head :ok }
    end
  end

  private
  def sort_column
    params[:sort] || "customer_reference"
  end

  def sort_direction
    params[:direction] || "asc"
  end

  def search
    params[:search] || ""
  end

end
