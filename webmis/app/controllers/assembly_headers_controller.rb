class AssemblyHeadersController < ApplicationController
  
  before_filter :authorize
  authorize_resource
  
  # GET /assembly_headers
  # GET /assembly_headers.xml
  layout "application"
  helper_method :sort_column, :sort_direction, :search, :sec_sort_column
  
  def index
    if params[:reset]
      params.delete :inactive
      params.delete :search
      params.delete :reset
      params.delete :sort
      params.delete :direction
      params.delete :column_search_field
    end

    conditions_string_ary = []
    conditions_param_values = []

    conditions_string_ary << " 1=1 "
    conditions = ""
    if params[:search] && params[:search] != ""
      q = params[:search].strip
      conditions_search = "#{conditions} (assembly_headers.id = ?
                                   OR assembly_headers.name LIKE ?
                                   OR assembly_headers.product_id LIKE ?
                                   OR assembly_headers.output_format LIKE ?
                                   OR assembly_headers.remarks LIKE ?)"
      conditions_param_values << q.to_i # only for id
      4.times do
        conditions_param_values << "%#{q}%"
      end
      conditions_string_ary << conditions_search
    end

    if !column_search_query.empty?
      conditions_string_ary << "#{column_search_query['query']}"
      conditions_param_values +=  column_search_query["values"]
    end
    conditions += conditions_string_ary.join( ' AND ' )
    
    @assembly_headers = AssemblyHeader.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])
    generate_id_sequence_cookie(@assembly_headers,params[:page])
    
    if request.xhr?
      flash.keep[:notice]
      render(:partial => "assembly_header", :collection => @assembly_headers)
    else  
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @assembly_headers }
      end
    end
  end

  # GET /assembly_headers/1
  # GET /assembly_headers/1.xml
  def show
    @assembly_header = AssemblyHeader.find(params[:id])
    @assembly_lines = AssemblyLine.where("assembly_header_id = #{params[:id]}").order("reference_id")

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @assembly_header }
    end
  end

  # GET /assembly_headers/new
  # GET /assembly_headers/new.xml
  def new
    @assembly_header = AssemblyHeader.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @assembly_header }
    end
  end

  # GET /assembly_headers/1/edit
  def edit    
    if params[:reset]
      params.delete :search
      params.delete :reset
      params.delete :search_region
      params.delete :search_datatype
      params.delete :search_supplier
      params.delete :search_release  

      params[:sort] = "view_receptions_and_conversiondbs.referenceid"
      params[:direction] = 'desc'
    end

    if params[:sort].blank? || params[:sort] == 'assembly_headers.id'
      params[:sec_sort] = "view_receptions_and_conversiondbs.referenceid"
    else
      params[:sec_sort] = params[:sort]
    end
     
    conditions = '1=1'
    conditions_string_ary = []
    conditions_param_values = []
    
  
    unless params[:search].blank?
      conditions_string_ary << " and (view_receptions_and_conversiondbs.referenceid = ?)"
      1.times do
        conditions_param_values << params[:search].strip.to_i
      end
    end    
    
    conditions = conditions + conditions_string_ary.join(" ")
    @assembly_header = AssemblyHeader.find(params[:id])
    @assembly_lines = AssemblyLine.where("assembly_header_id = #{params[:id]}").order("reference_id")
    if !params[:search].blank?
      @assembly_pickers = ViewReceptionsAndConversiondb.joins([:region]).where([conditions] + conditions_param_values).order(sec_sort_column + ' ' + sort_direction).paginate(:per_page => 20, :page => params[:page])
    else
      #show empty list when no search criteria given
      @assembly_pickers = ViewReceptionsAndConversiondb.joins([{:data_release => :company_abbr}, :region]).where("1=0").paginate(:per_page => 20, :page => params[:page])
    end
    @update_region_tags = []
    if !@assembly_header.product_id.blank? && Product.where(:volumeid => @assembly_header.product_id.strip.upcase).size != 0
      @update_region_tags = Product.where(:volumeid => @assembly_header.product_id.strip.upcase).first.expr_update_region_tags
    end

    if request.xhr?
      flash.keep[:notice]
      render(:partial => "assembly_picker", :collection => @assembly_pickers)
    end  
    
  end

  # POST /assembly_headers
  # POST /assembly_headers.xml
  def create
    @assembly_header = AssemblyHeader.new(strong_params)
    @assembly_header.version = 0

    respond_to do |format|
      if @assembly_header.save
        flash[:notice] = 'AssemblyHeader was successfully created.'
        format.html { redirect_to(@assembly_header) }
        format.xml  { render :xml => @assembly_header, :status => :created, :location => @assembly_header }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @assembly_header.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /assembly_headers/1
  # PUT /assembly_headers/1.xml
  def update
    @assembly_header = AssemblyHeader.find(params[:id])
    @assembly_lines_grid = AssemblyLine.where("assembly_header_id = #{params[:id]}")

    if params[:release]
      if !@assembly_header.version
        params[:assembly_header][:version] = 1
      else
        params[:assembly_header][:version] = @assembly_header.version + 1
      end
    end
    respond_to do |format|
      if @assembly_header.update_attributes(strong_params)
        flash[:notice] = 'Assembly was successfully updated.'
        format.html { redirect_to(@assembly_header) }
        format.xml  { head :ok }
      else
        @message = {}
        @assembly_lines = AssemblyLine.where("assembly_header_id = #{params[:id]}").order("reference_id")
        @assembly_pickers = ViewReceptionsAndConversiondb.joins([{:data_release => :company_abbr}, :region]).paginate(:per_page => 50, :page => "1")
        @update_region_tags = []
        if !@assembly_header.product_id.blank? && Product.where(:volumeid => @assembly_header.product_id.strip.upcase).size != 0
          @update_region_tags = Product.where(:volumeid => @assembly_header.product_id.strip.upcase).first.expr_update_region_tags
        end
        format.html { render :action => "edit" }
        format.xml  { render :xml => @assembly_header.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /assembly_headers/1
  # DELETE /assembly_headers/1.xml
  def destroy
    @assembly_header = AssemblyHeader.find(params[:id])
    @assembly_header.destroy
    
    respond_to do |format|
      flash[:notice] = "Assembly is permanently removed from database."
      format.html { redirect_to(:action => :index) }
      format.xml  { head :ok }
    end
  end

  def add_checked   
    
    @assembly_header = AssemblyHeader.find(params[:id])
    @update_region_tags = []
    if @assembly_header.product_id.to_s != '' && !@assembly_header.product_id.nil?
      @update_region_tags = Product.where(:volumeid => @assembly_header.product_id.strip.upcase).first.expr_update_region_tags
    end
    @message = Hash.new
    
    checked = []
    params.keys.map{|x| checked << x.scan(/check_(.*)_reception_(.*)_(.*)_(.*)$/)[0] if x.scan(/check_(.*)_reception_(.*)_(.*)$/)[0] }

    checked.each do |file|
      @div_id = "-" + file[2] + "-" + file[1]
      found = AssemblyLine.where("assembly_header_id = #{params[:id]} and file_name = '#{file[0]}'").first
      if !found
        ViewReceptionsAndConversiondb.create_assembly_line(file[1],file[0],params[:id])
      else
        @message[:notice] = "Skipped already added files"
        flash.keep[:notice] = "Skipped already added files"
      end
    end

    @assembly_lines = AssemblyLine.where("assembly_header_id = #{params[:id]}").order("reference_id")
    render :layout => false

  end

  def add
    @message = Hash.new
    @assembly_header = AssemblyHeader.find(params[:header_id])
    @update_region_tags = []
    if !@assembly_header.product_id.blank?
      @update_region_tags = Product.where(:volumeid => @assembly_header.product_id.strip.upcase).first.expr_update_region_tags
    end    

    if !params[:reception_id].blank?
      found = AssemblyLine.where("assembly_header_id = #{params[:header_id]} and file_name = '#{params[:filename]}'").first
      if !found
        ViewReceptionsAndConversiondb.create_assembly_line(params[:reception_id],params[:filename],@assembly_header.id)
      else
        @message[:notice] = "Skipped already added files"
      end
    else
      asl = AssemblyLine.new
      asl.assembly_header_id = params[:header_id]
      asl.save
    end

    @assembly_lines_grid = AssemblyLine.where("assembly_header_id = #{params[:header_id]}").order('reference_id')
    render :layout => false
  end

  def table_edit
    assembly_line = AssemblyLine.find(params[:id])
    
    if !params[:value]
      params[:value] = ""
    end
    #if assembly_line.respond_to? params[:field].to_sym
    assembly_line.update_attributes(params[:field] => params[:value])
    #end

    result = params[:value]
    case params[:field]
    when "brand_id"
      result = hwconfig.brand.name
    when "odm_id"
      result = hwconfig.odm.name
    end
    render :text => result
    return
  end

  def destroy_line
    if AssemblyLine.find(params[:line_id]).destroy
      @divid = params[:line_id]
    else
      @divid = 'already destroyed'
    end
    @assembly_header = AssemblyHeader.find(params[:id])
    @assembly_lines = AssemblyLine.where("assembly_header_id = #{params[:id]}").order("reference_id")
    @update_region_tags = []
    if !@assembly_header.product_id.blank?
      @update_region_tags = Product.where(:volumeid => @assembly_header.product_id.strip.upcase).first.expr_update_region_tags
    end    
    render :layout => false
  end

  def add_tag4mre
    @assembly_header = AssemblyHeader.find(params[:id])
    @divid = params[:line_id]
    ln = AssemblyLine.find(params[:line_id])
    if (ln.file_name.to_s.scan(/(\smre)\z/).size > 0)
      ln.file_name = ln.file_name.to_s.gsub(/(\smre)\z/,' tag4mre')
    else
      if (ln.file_name.to_s.scan(/(\stag4mre)\z/).size > 0)
        #nothing
        @message = 'already tagged'
        
      else  
        ln.file_name = ln.file_name.to_s + ' tag4mre'
        @message = 'tagged'
      end        
    end 
    ln.save
    
    @assembly_lines = AssemblyLine.where("assembly_header_id = #{params[:id]}").order("reference_id")
    render :layout => false
  end
  
  def add_update_region_tag
    @assembly_header = AssemblyHeader.find(params[:id])
    @update_region_tags = Product.where(:volumeid => @assembly_header.product_id.strip.upcase).first.expr_update_region_tags
    @divid = params[:line_id]
    @update_region = params[:update_region]
    
    ln = AssemblyLine.find(params[:line_id])
    if @update_region != ''
      if (ln.file_name.to_s.reverse.split(' ')[1])
        ln.file_name = ln.file_name.to_s.gsub(ln.file_name.to_s.reverse.split(' ').first,@update_region)
      else
        ln.file_name = ln.file_name.to_s + ' ' + @update_region
        @message = 'tagged'
      end
    else
      @message = 'No update region selected!'
    end
    ln.save
    
    @assembly_lines = AssemblyLine.where("assembly_header_id = #{params[:id]}").order("reference_id")
    render :layout => false
  end  
  
  def add_mre
    @message = Hash.new
    @assembly_header = AssemblyHeader.find(params[:id])
    ln = AssemblyLine.find(params[:line_id])
    if (ln.file_name.to_s.scan(/(\stag4mre)\z/).size > 0)
      ln.file_name = ln.file_name.to_s.gsub(/(\stag4mre)\z/,' mre')
    else
      if (ln.file_name.to_s.scan(/(\smre)\z/).size > 0)
        #nothing
      else      
        ln.file_name = ln.file_name.to_s + ' mre'
      end
    end    
    ln.save
    @assembly_lines_grid = AssemblyLine.where("assembly_header_id = #{params[:id]}").order("reference_id")
    render :layout => false
  end 

  def remove_mre_tag
    @assembly_header = AssemblyHeader.find(params[:id])
    ln = AssemblyLine.find(params[:line_id])
    file_name = ln.file_name.to_s.gsub(/(\stag4mre)\z/,'') #if ln.file_name.to_s.gsub!(/(\stag4mre)\z/,'')
    @update_region_tags = []
    if !@assembly_header.product_id.blank?
      @update_region_tags = Product.where(:volumeid => @assembly_header.product_id.strip.upcase).first.expr_update_region_tags if Product.where(:volumeid => @assembly_header.product_id.strip.upcase).first
    end
    file_name = file_name.to_s.gsub(" #{ln.file_name.to_s.reverse.split(' ')[0].reverse}",'') if ln.file_name.to_s.reverse.split(' ')[1]
    #file_name = file_name.to_s.gsub(/(\smre)\z/,'') #if ln.file_name.to_s.gsub!(/(\smre)\z/,'')
    if ln.file_name.to_s.strip != file_name
      ln.file_name = file_name
      ln.save
      @message = 'tag removed'
    else 
      @message = 'no tag to remove'
    end
    @divid = params[:line_id]
    @assembly_lines = AssemblyLine.where("assembly_header_id = #{params[:id]}").order("reference_id")
    render :layout => false
  end
  
  def add_all_tag4mre
    @assembly_header = AssemblyHeader.find(params[:assembly_header_id])
    @message = Hash.new
    @assembly_lines = AssemblyLine.where("assembly_header_id = #{@assembly_header.id}")
    @assembly_lines.each do |ln|
      ln.file_name = ln.file_name.to_s + ' tag4mre' if ((ln.file_name.to_s.scan(/(\stag4mre)\z/).size <= 0) && (ln.file_name.to_s.scan(/(\smre)\z/).size <= 0))
      ln.file_name = ln.file_name.to_s.gsub(/(\smre)\z/,' tag4mre') if (ln.file_name.to_s.scan(/(\smre)\z/).size > 0)
      ln.save
    end
    @assembly_lines_grid = AssemblyLine.where("assembly_header_id = #{@assembly_header.id}").order("reference_id")
    render :layout => false
  end
    
  def add_all_mre
    @message = Hash.new
    @assembly_header = AssemblyHeader.find(params[:assembly_header_id])
    @assembly_lines = AssemblyLine.where("assembly_header_id = #{@assembly_header.id.to_s}")
    @assembly_lines.each do |ln|
      ln.file_name = ln.file_name.to_s + ' tag4mre' if ((ln.file_name.to_s.scan(/(\smre)\z/).size <= 0) && (ln.file_name.to_s.scan(/(\stag4mre)\z/).size <= 0))
      ln.file_name = ln.file_name.to_s.gsub(/(\stag4mre)\z/,' mre') if (ln.file_name.to_s.scan(/(\stag4mre)\z/).size > 0)
      ln.save
    end
    @assembly_lines_grid = AssemblyLine.where("assembly_header_id = #{@assembly_header.id.to_s}").order("reference_id")
    render :layout => false
  end
    
  def remove_all_mre_tag
    @message = Hash.new
    @assembly_header = AssemblyHeader.find(params[:assembly_header_id])
    @update_region_tags = []
    if !@assembly_header.product_id.blank?
      @update_region_tags = Product.where(:volumeid => @assembly_header.product_id.strip.upcase).first.expr_update_region_tags
    end
    @assembly_lines = AssemblyLine.where("assembly_header_id = #{@assembly_header.id.to_s}")
    @assembly_lines.each do |ln|
      ln.file_name = ln.file_name.to_s.gsub(/(\stag4mre)\z/,'') #if ln.file_name.to_s.gsub!(/(\stag4mre)\z/,'')
      ln.file_name = ln.file_name.to_s.gsub(/(\mre)\z/,'') #if ln.file_name.to_s.gsub!(/(\smre)\z/,'')
      ln.save
    end
    @assembly_lines_grid = AssemblyLine.where("assembly_header_id = #{@assembly_header.id.to_s}").order("reference_id")
    render :layout => false
  end

  def replace_data
    @message = Hash.new
    @assembly_header = AssemblyHeader.find(params[:id])
    @update_region_tags = []
    if !@assembly_header.product_id.blank?
      @update_region_tags = Product.where(:volumeid => @assembly_header.product_id.strip.upcase).first.expr_update_region_tags
    end
    @assembly_lines = AssemblyLine.where("assembly_header_id = #{@assembly_header.id.to_s}").order("reference_id")
      
    if !params[:s_name].blank? && params[:s_name] == "conversion"
      db = ConversionDatabase.find(params[:sid]).conversion_id
      @conversion = ConversionDatabase.present.where(:conversion_id => db)
    elsif params[:s_name] && params[:s_name] == "dset"
      # not replaceable
    else
      @reception = Reception.find(params[:sid])
    end
      
    cnt = 0
    ncnt = 0
      
    @assembly_lines.each do |ln|
      done = false
      tag = ""
      tag = " tag4mre" if ln.file_name.to_s.scan(/(\stag4mre)\z/).size > 0
      tag = " mre"     if ln.file_name.to_s.scan(/(\smre)\z/).size > 0
      if ln.file_name.to_s.scan(/(\s.*\z)/).size > 0
        tag = ln.file_name.to_s.scan(/(\s.*\z)/).last.join
      end
      # /data/ops/dhive/prod/eur/eur/tom/o_20130209-0136-aut_tom_201209_20130208p_at2/aut_tom_201209_20130208p_at2.dh.sq3
      sfile = ln.file_name.gsub(/#{Regexp.escape(tag.chomp)}$/x,'').to_s.split('/').last
      tags = sfile.split(" ")
      if tag == "" && tags.size == 2
        tag = " " + tags[1]
      end
      sfile  = sfile.split(" ").first.scan(/(\w{2,3}_\w{3})_(\w*)_(.*)/x)
      expand(sfile)

      # [["aut_tom", "201209_20130208p", "at2.dh.sq3"]]
      if sfile[0]
        smatch = sfile[0].first + sfile[0].last # "aut_tomat2.dh.sq3"
        sequal = sfile[0][1]
      end

      #Conversion
      if params[:source] == "conversion" && smatch != ""
        @conversion.each do |c|
          # /data/ops/dhive/prod/eur/eur/tom/o_20130701-0100-aut_tom_201306_20130701d_at2/aut_tom_201306_20130701d_at2.dh.sq3
          nfile =  c.path_and_file.split('/').last.scan(/(\w{2,3}_\w{3})_(\w*)_(.*)/x)
          expand(nfile)
          # [["aut_tom", "201306_20130701d", "at2.dh.sq3"]]
          if nfile[0]
            nmatch = nfile[0].first + nfile[0].last # "aut_tomat2.dh.sq3"
            nequal = nfile[0][1]
          end
            
          if nmatch.to_s != "" && nmatch.to_s.strip == smatch.to_s.strip && sequal != nequal #match
            #check whether the input formats match
            @conv = Conversion.find(c.conversion_id)
            ln_conv = Conversion.find(ln.reference_id) if ln.reference_id
            ln_reception = Reception.find(ln.reference_id) if ln.reference_id && ln_conv.nil?
            ln_input_format = ln_conv.input_format if ln_conv
            ln_input_format = ln_reception.data_type.name if ln_conv.nil? && ln_reception
            if ln_input_format == @conv.input_format
              # replace lines
              ln.data_release_id = @conv.data_release_id
              ln.data_release_name = @conv.data_release.company_abbr.name.to_s + " " + @conv.data_release.name.to_s
              ln.reference_id = c.conversion_id.to_s
              ln.file_name = c.path_and_file.to_s.strip + tag
              ln.save
              done =true
              @message[:notice] = @message[:notice].to_s + "\n" + "<strong>REPLACED:</strong> #{ln.file_name}.<br />"
              cnt = cnt + 1
            end
          end
               
        end
      end
       
      #Reception
      if params[:source] == "reception" && smatch != ""
        @reception.storage_location.split.each do |r|
          nfile =  r.split('/').last.scan(/(\w{2,3}_\w{3})_(\w*)_(.*)/x)
          if nfile[0]
            nmatch = nfile[0].first + nfile[0].last
            nequal = nfile[0][1]
          end
           
          if nmatch.to_s != "" && nmatch.to_s.strip == smatch.to_s.strip && sequal != nequal
            #replace lines
            ln.data_release_id = @reception.data_release_id
            ln.data_release_name = @reception.data_release.company_abbr.name.to_s + " " + @reception.data_release.name.to_s
            ln.reference_id = @reception.referenceid
            ln.file_name = r.to_s.strip + tag
            ln.save
            done = true
            @message[:notice] = @message[:notice].to_s + "\n" + "<strong>REPLACED:</strong> #{ln.file_name}.<br />"
            cnt = cnt + 1
          end
        end
      end
        
      if !done
        ncnt = ncnt + 1
        @message[:notice] = @message[:notice].to_s + "\n" + "SKIPPED: #{ln.file_name}.<br />" if ln.file_name != ""
      end
    end
      
    @message[:notice] = @message[:notice].to_s + cnt.to_s + " Dhive(s) replaced. "
    @message[:notice] = @message[:notice].to_s + ncnt.to_s + " Dhive(s) skipped."
    @assembly_lines = AssemblyLine.where("assembly_header_id = #{@assembly_header.id.to_s}").order("reference_id")
    @assembly_lines_grid = AssemblyLine.where("assembly_header_id = #{@assembly_header.id.to_s}").order("reference_id")
    render :layout => false
      
  end
       
  def destroy_checked

    #if params[:reception_id]
    #  reception = Reception.find(params[:reception_id])
    #end
    @message = Hash.new
    checked = []
    params.keys.map{|x| checked << x.scan(/check_(.*)_reception_(.*)$/)[0] if x.scan(/check_(.*)_reception_(.*)$/)[0] }

    checked.each do |file|

      found = AssemblyLine.where("assembly_header_id = #{params[:header_id]} and file_name = '#{file[0]}'").first
      if found
        found.destroy
      end
    end

    @assembly_lines_grid = AssemblyLine.where("assembly_header_id = #{params[:header_id]}").order("reference_id")
    render :layout => false
  end
 
  def create_csv
    header = AssemblyHeader.find(params[:id])
    @report = header.assembly_lines
    
    respond_to do |format|
      format.html # index.html.erb
      format.xml { render :xml => @report, :filename => "assembly_#{header.product_id.to_s}"}
      format.csv { render :csv => @report, :filename => "assembly_#{header.product_id.to_s}"}
    end
  end

  def cloneit
    @header = AssemblyHeader.find(params[:id])

    @copy_header = @header.dup
    @copy_header.name = Time.now.strftime("%H%M%S") + " COPY OF: " + @header.name.to_s + @header.product_id.to_s
    @copy_header.product_id = nil
     

    @header.assembly_lines.each do |line|
      @copy_header.assembly_lines << line.dup
    end
    @copy_header.version = nil
    @copy_header.save


    redirect_to(:action => 'index')

  end

  def versionit
    @header = AssemblyHeader.find(params[:id])

    @copy_header = @header.dup
    @header.assembly_lines.each do |line|
      @copy_header.assembly_lines << line.dup
    end
    @copy_header.version = @header.version + 1
    @copy_header.save

    redirect_to(:action => 'index')

  end

  private

 def expand(source_file)
    if !(source_file && source_file[0] && source_file[0][2])
      return source_file
    end
    file = source_file[0][2]
    source_file[0][0].gsub!("_nok","_her")
    if file.match(/([A-z|-]*)_([0-9]*).dh.sq3/)
      # new naming convention detected .. expand region to full name
      regioncode = file.scan(/([A-z|-]*)_([0-9]*).dh.sq3/)[0][0]
      r = Region.find_by_region_code(regioncode)
      if !r
        # Backwards compatibility patch they renamed usa-states from AK to USA-AK
        r = Region.find_by_region_code("usa-"+regioncode)
      end
      if r
        source_file[0][2] = r.name.to_s.downcase.gsub(" ","_") + ".dh.sq3"
      end
    elsif file.match(/([A-z|-]{2,8}).dh.sq3/)
      regioncode = file.scan(/([A-z|-]{2,8}).dh.sq3/)[0][0]
      r = Region.find_by_region_code(regioncode)
      if !r
        # Backwards compatibility patch they renamed usa-states from AK to USA-AK
        r = Region.find_by_region_code("usa-"+regioncode)
      end
      if r
        source_file[0][2] = r.name.to_s.downcase.gsub(" ","_") + ".dh.sq3"
      end
    end
    return source_file
  end


  def sort_column  
    params[:sort] || "assembly_headers.id"  
  end

  def sec_sort_column  
    params[:sec_sort] || "view_receptions_and_conversiondbs.source,view_receptions_and_conversiondbs.referenceid"  
  end  
    
  def sort_direction  
    params[:direction] || "desc"  
  end
  
  def search
    params[:search] || ""
  end  




end
