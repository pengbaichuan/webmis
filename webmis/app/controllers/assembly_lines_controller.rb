class AssemblyLinesController < ApplicationController
  # GET /assembly_lines
  # GET /assembly_lines.xml
  def index
    @assembly_lines = AssemblyLine.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @assembly_lines }
    end
  end

  # GET /assembly_lines/1
  # GET /assembly_lines/1.xml
  def show
    @assembly_line = AssemblyLine.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @assembly_line }
    end
  end

  # GET /assembly_lines/new
  # GET /assembly_lines/new.xml
  def new
    @assembly_line = AssemblyLine.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @assembly_line }
    end
  end

  # GET /assembly_lines/1/edit
  def edit
    @assembly_line = AssemblyLine.find(params[:id])
  end

  # POST /assembly_lines
  # POST /assembly_lines.xml
  def create
    @assembly_line = AssemblyLine.new(strong_params)

    respond_to do |format|
      if @assembly_line.save
        flash[:notice] = 'AssemblyLine was successfully created.'
        format.html { redirect_to(@assembly_line) }
        format.xml  { render :xml => @assembly_line, :status => :created, :location => @assembly_line }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @assembly_line.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /assembly_lines/1
  # PUT /assembly_lines/1.xml
  def update
    @assembly_line = AssemblyLine.find(params[:id])

    respond_to do |format|
      if @assembly_line.update_attributes(strong_params)
        flash[:notice] = 'AssemblyLine was successfully updated.'
        format.html { redirect_to(@assembly_line) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @assembly_line.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /assembly_lines/1
  # DELETE /assembly_lines/1.xml
  def destroy
    @assembly_line = AssemblyLine.find(params[:id])
    @assembly_line.destroy
    
    respond_to do |format|
      format.html { redirect_to(:controller => 'assembly_headers', :action => 'edit', :id => params[:header_id]) }
      format.xml  { head :ok }
    end
  end


end
