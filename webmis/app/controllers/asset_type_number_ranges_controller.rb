class AssetTypeNumberRangesController < ApplicationController
  before_filter :authorize
  authorize_resource
  
  layout "webmis" 
  # GET /asset_type_number_ranges
  # GET /asset_type_number_ranges.xml
  def index
    @asset_type_number_ranges = AssetTypeNumberRange.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @asset_type_number_ranges }
    end
  end

  # GET /asset_type_number_ranges/1
  # GET /asset_type_number_ranges/1.xml
  def show
    @asset_type_number_range = AssetTypeNumberRange.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @asset_type_number_range }
    end
  end

  # GET /asset_type_number_ranges/new
  # GET /asset_type_number_ranges/new.xml
  def new
    @asset_type_number_range = AssetTypeNumberRange.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @asset_type_number_range }
    end
  end

  # GET /asset_type_number_ranges/1/edit
  def edit
    @asset_type_number_range = AssetTypeNumberRange.find(params[:id])
  end

  # POST /asset_type_number_ranges
  # POST /asset_type_number_ranges.xml
  def create
    @asset_type_number_range = AssetTypeNumberRange.new(strong_params)

    respond_to do |format|
      if @asset_type_number_range.save
        flash[:notice] = 'AssetTypeNumberRange was successfully created.'
        format.html { redirect_to(@asset_type_number_range) }
        format.xml  { render :xml => @asset_type_number_range, :status => :created, :location => @asset_type_number_range }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @asset_type_number_range.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /asset_type_number_ranges/1
  # PUT /asset_type_number_ranges/1.xml
  def update
    @asset_type_number_range = AssetTypeNumberRange.find(params[:id])

    respond_to do |format|
      if @asset_type_number_range.update_attributes(strong_params)
        flash[:notice] = 'AssetTypeNumberRange was successfully updated.'
        format.html { redirect_to(@asset_type_number_range) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @asset_type_number_range.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /asset_type_number_ranges/1
  # DELETE /asset_type_number_ranges/1.xml
  def destroy
    @asset_type_number_range = AssetTypeNumberRange.find(params[:id])
    @asset_type_number_range.destroy

    respond_to do |format|
      format.html { redirect_to(asset_type_number_ranges_url) }
      format.xml  { head :ok }
    end
  end
end
