class AssetTypesController < ApplicationController
  before_filter :authorize, :except => :get_document_id
  authorize_resource :except => :get_document_id
  
  # GET /asset_types
  # GET /asset_types.xml
    layout "webmis"
  def index
    @asset_types = AssetType.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @asset_types }
    end
  end

  # GET /asset_types/1
  # GET /asset_types/1.xml
  def show
    @asset_type = AssetType.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @asset_type }
    end
  end

  # GET /asset_types/new
  # GET /asset_types/new.xml
  def new
    @asset_type = AssetType.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @asset_type }
    end
  end

  # GET /asset_types/1/edit
  def edit
    @asset_type = AssetType.find(params[:id])

   
  end

  # POST /asset_types
  # POST /asset_types.xml
  def create
    @asset_type = AssetType.new(strong_params)

    respond_to do |format|
      if @asset_type.save
        flash[:notice] = 'AssetType was successfully created.'
        format.html { redirect_to(@asset_type) }
        format.xml  { render :xml => @asset_type, :status => :created, :location => @asset_type }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @asset_type.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /asset_types/1
  # PUT /asset_types/1.xml
  def update
    @asset_type = AssetType.find(params[:id])

    respond_to do |format|
      if @asset_type.update_attributes(strong_params)
        flash[:notice] = 'AssetType was successfully updated.'
        format.html { redirect_to(@asset_type) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @asset_type.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /asset_types/1
  # DELETE /asset_types/1.xml
  def destroy
    @asset_type = AssetType.find(params[:id])
    @asset_type.destroy

    respond_to do |format|
      format.html { redirect_to(asset_types_url) }
      format.xml  { head :ok }
    end
  end

  def create_number
    @types = AssetType.all.order(:name)
    @current_user = current_user
    #if  @asset.docrole != 'Others'
    # params[:extend_view] = 'off'
    #end
    
    if params[:name] && params[:name].strip.size > 0
       if params[:title] && params[:title].strip.size > 0
        asset = AssetType.find_by_name(params[:name])
        #@nextnumber = b.get_next_number :quarter => 'Q1', :supplier => 1 , :suffix => 'aaa123', :rangetype => 'Aftermarket NT EUR'
        @nextnumber = asset.get_next_number params
        asset = Asset.new
        asset.ref_id = @nextnumber
        asset.requested_by = current_user.first_name + " " + current_user.last_name if current_user
        asset.remarks = params[:remarks]
        asset.assettype = params[:name]
        asset.title = params[:title] 
        asset.suffix = params[:suffix]
        if !params[:extend_view]
          asset.role = "Others"
        else
          asset.role = current_user.docrole
        end
        asset.save

       else
         flash[:error] = 'Please provide a title'
       end

    else
       if params[:name]
        flash[:error] = "Please provide type"
       end
    end 
  end

#merged from backoffice project
 def get_document_id
    @types = AssetType.all.order(:name)
    
    #if  @asset.docrole != 'Others'
    # params[:extend_view] = 'off'
    #end

    if params[:type] && params[:type].strip.size > 0
      if params[:title] && params[:title].strip.size > 0
        asset = AssetType.find_by_name(params[:type])
        #@nextnumber = b.get_next_number :quarter => 'Q1', :supplier => 1 , :suffix => 'aaa123', :rangetype => 'Aftermarket NT EUR'
        @nextnumber = asset.get_next_number params
        asset = Asset.new
        asset.ref_id = @nextnumber
        asset.requested_by = "API"
        asset.remarks = params[:remarks]
        asset.assettype = params[:name]
        asset.title = params[:title]
        asset.suffix = params[:suffix]
        asset.role = "Others"
        asset.save
        return render :text => {:document_id => @nextnumber}.to_json
      else
        message = "Title missing"
        errorlog("SYNC ERROR: " + message)
        return render :text => message, :status => :unprocessable_entity
      end

    else
      message = "Type missing or not correct, choose between " + AssetType.all.map{|x|x.name}.join(",")
      errorlog("SYNC ERROR: " + message)
      return render :text => message, :status => :unprocessable_entity
    end
    
  end
end
