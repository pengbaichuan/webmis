class AssetsController < ApplicationController
  before_filter :authorize
  authorize_resource

  layout "webmis"
  helper_method :sort_column, :sort_direction, :search
  
  # GET /assets
  # GET /assets.xml
  def index
    if params[:reset]
      params.delete :search
      params.delete :reset
      params[:asset_type] = 'All'

      params[:sort] = 'assets.created_at'
      params[:direction] = 'desc'
    end    
    
    conditions = "1 = 1"
    
    if params[:search] && params[:search] != ""
      q = params[:search].strip
      conditions = "#{conditions} and (assets.ref_id LIKE ? 
                                     or assets.title LIKE ? 
                                       or assets.requested_by LIKE ?)"
    end
    
    if params[:search] && params[:asset_type] != 'All'
      conditions = "#{conditions} and (assets.assettype = '#{params[:asset_type]}')"
    end
    
    conditions = "#{conditions} and #{conditions_role}"
    @assets = Asset.where(conditions,"%#{q}%","%#{q}%","%#{q}%").order(sort_column + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])

    if request.xhr?
      render(:partial => "asset", :collection => @assets)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @assets }
      end
    end
  end

  # GET /assets/1
  # GET /assets/1.xml
  def show
    conditions = conditions_role + " AND (assets.id = #{params[:id]})"
    @asset = Asset.where(conditions).first
    if !@asset
      flash[:error] =  "Document not found or you have no access to this information"
      redirect_to :action  => :index
      return
    end
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @asset }
    end
  end

  def update
    conditions = conditions_role + " AND (assets.id = #{params[:id]})"
    @asset = Asset.where(conditions)
    if !@asset
      flash[:error] =  "Document not found or you have no access to this information"
      redirect_to :action  => :index
      return
    end
    ref_id =  @asset.ref_id.scan(/(.*)#{@asset.suffix}$/).join
    if ref_id && ref_id != ''
      params[:asset][:ref_id] = ref_id + params[:asset][:suffix]
    end

    if !params[:extend_view]
          @asset.role = "Others"
        else
          @asset.role = current_user.docrole
     end
     
    respond_to do |format|
      if @asset.update_attributes(strong_params)
        flash[:notice] = 'Asset was successfully updated.'
        format.html { redirect_to(@asset) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @asset.errors, :status => :unprocessable_entity }
      end
    end
  end
 
  def edit
    conditions = conditions_role + " AND (assets.id = #{params[:id]})"
    @asset = Asset.where(conditions)
    
    if !@asset
      flash[:notice] =  "Document not found or you have no access to this information"
      redirect_to :action  => :index
      return
    end

    if @asset.role != 'Others'
      params[:extend_view] = 'off'
    end
    
  end

  private  
  def sort_column  
    params[:sort] || "assets.created_at"  
  end
   
  def sort_direction  
    params[:direction] || "desc"  
  end
  
  def search
    params[:search] || ""
  end    
  
  protected

  def conditions_role
    user = current_user
    conditions = ""
    conditions = conditions  + " role in ("
    c = 0
    user.docrolesallowed.each do |role|
      conditions = conditions + "," if c > 0
      c = c + 1
      conditions = conditions + "'#{role}'"
    end
    conditions = conditions  + ")"

    return conditions
  end


end
