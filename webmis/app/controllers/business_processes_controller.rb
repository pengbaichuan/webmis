class BusinessProcessesController < ApplicationController
  before_filter :authorize
  authorize_resource
  
  layout 'webmis'
  
  # GET /business_processes
  # GET /business_processes.xml
  #layout 'ppf'
  
  def index
    @business_processes = BusinessProcess.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @business_processes }
    end
  end

  # GET /business_processes/1
  # GET /business_processes/1.xml
  def show
    @business_process = BusinessProcess.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @business_process }
    end
  end

  # GET /business_processes/new
  # GET /business_processes/new.xml
  def new
    @business_process = BusinessProcess.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @business_process }
    end
  end

  # GET /business_processes/1/edit
  def edit
    @business_process = BusinessProcess.find(params[:id])
  end

  # POST /business_processes
  # POST /business_processes.xml
  def create
    @business_process = BusinessProcess.new(strong_params)
    @mail_event = MailEvent.new
    
    respond_to do |format|
      if @business_process.save
        @mail_event.name = "po_status_#{@business_process.name}"
        @mail_event.save
        flash[:notice] = 'BusinessProcess was successfully created.'
        format.html { redirect_to(@business_process) }
        format.xml  { render :xml => @business_process, :status => :created, :location => @business_process }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @business_process.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /business_processes/1
  # PUT /business_processes/1.xml
  def update
    @business_process = BusinessProcess.find(params[:id])
    @mail_event = MailEvent.find_by_name("po_status_#{@business_process.name}")
    respond_to do |format|
      if @business_process.update_attributes(strong_params)
        @mail_event = MailEvent.new if @mail_event.nil?       
        @mail_event.name = "po_status_#{@business_process.name}"
        @mail_event.save
        flash[:notice] = 'BusinessProcess was successfully updated.'
        format.html { redirect_to(@business_process) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @business_process.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /business_processes/1
  # DELETE /business_processes/1.xml
  def destroy
    @business_process = BusinessProcess.find(params[:id])
    @business_process.destroy

    respond_to do |format|
      format.html { redirect_to(business_processes_url) }
      format.xml  { head :ok }
    end
  end
end
