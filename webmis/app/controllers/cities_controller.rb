class CitiesController < ApplicationController

  before_filter :authorize
  authorize_resource
  layout 'webmis'  
  helper_method :sort_column, :sort_direction, :search

  # index not used as this is handled by the regions controller
  def index
    if params[:reset]
      params.delete :q
      params.delete :reset

      params[:sort] = 'name'
      params[:direction] = 'asc'
    end
    conditions = "1=1 "
    conditions_string_ary = []
    conditions_param_values = []

    unless params[:q].blank?
      conditions_string_ary << " and (regions.name LIKE ? OR city_iso_code LIKE ?)"
      2.times do
        conditions_param_values << "\%#{params[:q].strip}\%"
      end
    end
    conditions = conditions + conditions_string_ary.join(" ")
    	@cities = City.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])
    
    if request.xhr?
      flash.keep[:notice]
      render(:partial => "city", :collection => @cities)
    end    
  end

  def show
    @city = City.find(params[:id])

  end

  def new
    @city = City.new
    @city.isactive = true
  end

  def edit
    @city = City.find(params[:id])
  end

  def create
    @city = City.new(strong_params)
    @city.type = 'City'
    @city.display_type_id = AreaType.where(:name => 'City').last.id

    respond_to do |format|
      if @city.save
        # the 'method app_module_ids=' must be placed after the save as it will created and save child objects of @city, so @city must also be a saved entity
        @city.app_module_ids=([AppModule.where(:abbr => 'MIS').last.id])
        format.html { redirect_to @city, :notice => "City succesfully saved."	 }
        format.json { render json: @city, status: :created, location: @city }
      else
        format.html { render action: "new" }
        format.json { render json: @city.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @city = City.find(params[:id])
    @city.app_module_ids = [AppModule.where(:abbr => 'MIS').last.id]

    respond_to do |format|
      if @city.update_attributes(strong_params)
        format.html { redirect_to(@city, :notice => 'City was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @city.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @city = City.find(params[:id])
    if @city.destroy
	flash[:notice] =  "City succesfully deleted!"    	
	redirect_to_index
    else
	flash[:error] = "Database error! Please contact an administrator!"
	redirect_to_index
    end
  end

  def generate_dakota_id
    render :text => Region.generate_dakota_id
  end

  def redirect_to_index
     if params[:q]
	redirect_to :action => :index, :q => params[:q]
     else
        redirect_to :action => :index
     end
  end
  
  private  
  def sort_column  
    params[:sort] || "name"  
  end  
    
  def sort_direction  
    params[:direction] || "asc"
  end
  
  def search
    params[:search] || ""
  end  

end
