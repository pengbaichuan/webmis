class CommitmentChangeReasonsController < ApplicationController
  before_filter :authorize
  authorize_resource

  layout 'webmis'
  helper_method :sort_column, :sort_direction, :search

  def index
    if params[:reset]
      params.delete :search
      params.delete :reset

      params[:sort] = 'commitment_change_reasons.reason'
      params[:direction] = 'desc'
    end

    conditions_string_ary = ['1=1']
    conditions_param_values = []

    unless params[:search].blank?
      conditions_string_ary << " and (reason LIKE ?
              OR commitment_type LIKE ? OR commitment_type LIKE ? OR id = #{params[:search].strip.to_i})"
      2.times do
        conditions_param_values << "%#{params[:search].strip}%"
      end
      conditions_param_values << "#{params[:search].strip.gsub(' ','_')}" # 3th one
    end

    @commitment_change_reasons = CommitmentChangeReason.where([conditions_string_ary.join(' ')] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])

    if request.xhr?
      render(:partial => 'commitment_change_reason', :collection => @commitment_change_reasons)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @commitment_change_reasons }
      end
    end
  end

  def show
    @commitment_change_reason = CommitmentChangeReason.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @commitment_change_reason }
    end
  end

  def new
    @commitment_change_reason = CommitmentChangeReason.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @commitment_change_reason }
    end
  end

  def edit
    @commitment_change_reason = CommitmentChangeReason.find(params[:id])
  end

  def create
    @commitment_change_reason = CommitmentChangeReason.new(strong_params)

    respond_to do |format|
      if @commitment_change_reason.save
        format.html { redirect_to @commitment_change_reason, notice: 'Commitment change reason was successfully created.' }
        format.json { render json: @commitment_change_reason, status: :created, location: @commitment_change_reason }
      else
        format.html { render action: 'new' }
        format.json { render json: @commitment_change_reason.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @commitment_change_reason = CommitmentChangeReason.find(params[:id])

    respond_to do |format|
      if @commitment_change_reason.update_attributes(strong_params)
        format.html { redirect_to @commitment_change_reason, notice: 'Commitment change reason was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @commitment_change_reason.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @commitment_change_reason = CommitmentChangeReason.find(params[:id])
    @commitment_change_reason.destroy

    respond_to do |format|
      format.html { redirect_to commitment_change_reasons_url }
      format.json { head :no_content }
    end
  end

  private
  def sort_column
    params[:sort] || 'reason'
  end

  def sort_direction
    params[:direction] || 'desc'
  end

  def search
    params[:search] || ''
  end
end
