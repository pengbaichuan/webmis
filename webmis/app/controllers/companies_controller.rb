class CompaniesController < ApplicationController

  before_filter :authorize
  authorize_resource param_method: :strong_params
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search
  
  # GET /companies
  # GET /companies.json
  def index
    if params[:reset]
      params.delete :removed
      params.delete :search
      params.delete :reset
      params[:sort] = 'companies.name'
      params[:direction] = 'asc'
      params.delete :column_search_field
    end

    conditions_string_ary = []
    conditions_param_values = []

    if params[:removed]
      @removed = true
      conditions_string_ary << "1=1"
    else
      conditions_string_ary << "(companies.status != '#{Company::STATUS_OBSOLETE}')"
    end

    unless params[:search].blank?
      q = params[:search].strip
      conditions_string_ary << "(companies.id = ?
                                OR companies.name LIKE ?
                                OR external_contacts.first_name LIKE ?
                                OR external_contacts.last_name  LIKE ?
                                OR companies.shipping_address_postalcode LIKE ?
                                OR companies.shipping_address_street LIKE ?
                                OR companies.shipping_address_city LIKE ?
                                OR companies.shipping_address_state LIKE ?
                                OR companies.shipping_address_country LIKE ?
                                OR companies.phone_office LIKE ?
                                OR companies.phone_fax LIKE ?)"
      conditions_param_values << q.to_i
      10.times do
        conditions_param_values << "%#{q}%"
      end
    end

    if !column_search_query.empty?
      conditions_string_ary << "#{column_search_query['query']}"
      conditions_param_values +=  column_search_query["values"]
    end
    conditions = conditions_string_ary.join( ' AND ' )

    @companies = Company.where([conditions] + conditions_param_values).eager_load(:external_contacts).order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])
    generate_id_sequence_cookie(@companies,params[:page])

    if request.xhr?
     render(:partial => "company", :collection => @companies,:params => params)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @companies }
      end
    end
  end

  # GET /companies/1
  # GET /companies/1.json
  def show
    @company = Company.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @company }
    end
  end

  # GET /companies/new
  # GET /companies/new.json
  def new
    @company = Company.new
    @company.status = Company::STATUS_ACTIVE

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @company }
    end
  end

  # GET /companies/1/edit
  def edit
    @company = Company.find(params[:id])
  end

  # POST /companies
  # POST /companies.json
  def create
    @company = Company.new(strong_params)
    @company.status = Company::STATUS_ACTIVE
    respond_to do |format|
      if @company.save
        format.html { redirect_to @company, notice: 'Company was successfully created.' }
        format.json { render json: @company, status: :created, location: @company }
      else
        format.html { render action: "new" }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /companies/1
  # PUT /companies/1.json
  def update
    @company = Company.find(params[:id])

    respond_to do |format|
      if @company.update_attributes(strong_params)
        format.html { redirect_to @company, notice: 'Company was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /companies/1
  # DELETE /companies/1.json
  def destroy
    @company = Company.find(params[:id])
    @company.destroy

    respond_to do |format|
      format.html { redirect_to companies_url }
      format.json { head :no_content }
    end
  end

  def change_status
    @company = Company.find(params[:id])
    respond_to do |format|
      if @company.change_status(params[:status])
        format.html { redirect_to :back, notice: "Company was successfully changed to #{@company.status_string}." }
        format.json { render json: @company, status: :created, location: @company }
      else
        format.html { render action: "show" }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

private
  def sort_column
    !params[:sort].blank? ? params[:sort] : "companies.name"
  end

  def sort_direction
    !params[:direction].blank? ? params[:direction] : "asc"
  end

  def search
    params[:search] || ""
  end
end
