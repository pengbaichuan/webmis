class CompanyAbbrsController < ApplicationController
  before_filter :authorize, :except => :get_supplier_name_id
  authorize_resource :except => :get_supplier_name_id

  layout "webmis"
  helper_method :sort_column, :sort_direction, :search
  
  # GET /company_abbrs
  # GET /company_abbrs.xml

  def index

    conditions =  '1=1'
    q= ''
    
    if params[:reset]
      params.delete :search
      params.delete :reset

      params.delete :sort
      params.delete :direction
    end
    
    if params[:search] && params[:search] != ""
        q = params[:search].strip.strip
        conditions = "#{conditions} AND (company_name LIKE ?
                                            OR ms_abbr_3 LIKE ? 
                                            OR dakota_id = #{params[:search].strip.to_i} ) AND (relation_type = 'Supplier')"
    end
    
    #@tags = CompanyAbbr.tags
    if params[:c] && params[:c].size > 0
       @company_abbrs = CompanyAbbr.tagged_with(params[:c],:match => :any).paginate(:per_page => 100, :page => params[:page])
    else
        @company_abbrs = CompanyAbbr.where(conditions,"%#{q}%",
        "%#{q}%").order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])
    end
    
    if request.xhr?
        flash.keep[:notice]
        render(:partial => "company_abbr", :collection => @company_abbrs)
      else
      respond_to do |format|
        format.html # index.html.erb
        format.xml { render :xml => @company_abbrs}
        format.csv { render :csv => @company_abbrs, :filename => 'suppliers'}
      end
    end
  end

  # GET /company_abbrs/1
  # GET /company_abbrs/1.xml
  def show
    @company_abbr = CompanyAbbr.find(params[:id])
    @file_transfer_accounts = FileTransferAccount.where(:company_abbrs_id => params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @company_abbr }
    end
  end

  # GET /company_abbrs/new
  # GET /company_abbrs/new.xml
  def new
    @company_abbr = CompanyAbbr.new
    @company_abbr.is_active = 1

  end

  # GET /company_abbrs/1/edit
  def edit
    @company_abbr = CompanyAbbr.find(params[:id])
    @file_transfer_accounts = FileTransferAccount.where(:company_abbrs_id => params[:id])
    
  end

  # POST /company_abbrs
  # POST /company_abbrs.xml
  def create
    @company_abbr = CompanyAbbr.new(strong_params.update(:updated_by => session[:username]))
    @company_abbr.relation_type = 'Supplier'

    respond_to do |format|
      if @company_abbr.save
        flash[:notice] = 'CompanyAbbr was successfully created.'
        format.html { redirect_to(@company_abbr) }
        format.xml  { render :xml => @company_abbr, :status => :created, :location => @company_abbr }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @company_abbr.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /company_abbrs/1
  # PUT /company_abbrs/1.xml
  def update
    @company_abbr = CompanyAbbr.find(params[:id])

    respond_to do |format|
      if @company_abbr.update_attributes(strong_params.update(:updated_by => session[:username]))
        flash[:notice] = 'CompanyAbbr was successfully updated.'
        format.html { redirect_to(@company_abbr) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @company_abbr.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /company_abbrs/1
  # DELETE /company_abbrs/1.xml
  def destroy
    @company_abbr = CompanyAbbr.find(params[:id])
    @company_abbr.destroy

    respond_to do |format|
      format.html { redirect_to(company_abbrs_url) }
      format.xml  { head :ok }
    end
  end

  def dakota_supplier_ids_file
    send_data CompanyAbbr.dakota_supplier_ids_file, :filename => 'SUPPLIER_IDS.TXT'
  end

  def generate_dakota_id
    render :text => CompanyAbbr.generate_dakota_id
  end

 #merged from backoffice project
  def get_supplier_name_id
    # Designed based on CVENV method cv_privatelib
    result = "FAILURE"
    supplier_id = '-1'
    supplier_name = ""
    if !params[:supplier_code].blank?
      company_abbr = CompanyAbbr.find_by_ms_abbr_3(params[:supplier_code])
      if company_abbr
        if !company_abbr.dakota_id.blank?
          supplier_id = company_abbr.dakota_id.to_s
          supplier_name = company_abbr.company_name.strip
          result = "SUCCESS"
        end
      end
    end
    render :text => "#{result},#{supplier_name},#{supplier_id}"
  end

  def dakota_supplier_ids
    render :text => CompanyAbbr.dakota_supplier_ids
  end
   
  private  
  def sort_column  
    params[:sort] || "company_abbrs.company_name"  
  end  
    
  def sort_direction  
    params[:direction] || "asc"  
  end
  
  def search
    params[:search] || ""
  end 

  def company_abbr_params
     attrs = CompanyAbbr.new.attributes.keys
     params.require(:company_abbr).permit(attrs)
  end
end
