class ConfigParametersController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"
    
  helper_method :sort_column, :sort_direction, :search
  
  # GET /config_parameters GET /config_parameters.json
  def index
    @config_parameters = ConfigParameter.all
    if params[:reset]
      params.delete :search
      params.delete :reset     
      
      params[:sort] = 'config_parameters.cfg_name'
      params[:direction] = 'asc'
    end
    
    conditions = "project_id = #{session[:project_id]}"
    conditions_string_ary = []
    conditions_param_values = []
    
    unless params[:search].blank?
      conditions_string_ary << " and (cfg_name LIKE ? OR cfg_group LIKE ?)"
      2.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
    end
    conditions = conditions + conditions_string_ary.join(" ")
    @config_parameters = ConfigParameter.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])
    generate_id_sequence_cookie(@config_parameters,params[:page])
    if request.xhr?
      flash.keep[:notice]
      render(:partial => "config_parameter", :collection => @config_parameters)
    else     
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @config_parameters }
      end
    end
  end

  # GET /config_parameters/1
  # GET /config_parameters/1.json
  def show
    @config_parameter = ConfigParameter.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @config_parameter }
    end
  end

  # GET /config_parameters/new
  # GET /config_parameters/new.json
  def new
    @config_parameter = ConfigParameter.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @config_parameter }
    end
  end

  # GET /config_parameters/1/edit
  def edit
    @config_parameter = ConfigParameter.find(params[:id])
  end

  # POST /config_parameters
  # POST /config_parameters.json
  def create
    @config_parameter = ConfigParameter.new(config_parameter_params)
    @config_parameter.project_id = session[:project_id]


    respond_to do |format|
      if @config_parameter.save
        format.html { redirect_to @config_parameter, notice: 'Config parameter was successfully created.' }
        format.json { render json: @config_parameter, status: :created, location: @config_parameter }
      else
        format.html { render action: "new" }
        format.json { render json: @config_parameter.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /config_parameters/1
  # PUT /config_parameters/1.json
  def update
    @config_parameter = ConfigParameter.find(params[:id])

    respond_to do |format|
      if @config_parameter.update_attributes(config_parameter_params)
        session[:color_profile] = Project.find(session[:project_id]).color_profile
        format.html { redirect_to @config_parameter, notice: 'Config parameter was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @config_parameter.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /config_parameters/1
  # DELETE /config_parameters/1.json
  def destroy
    @config_parameter = ConfigParameter.find(params[:id])
    @config_parameter.destroy

    respond_to do |format|
      format.html { redirect_to config_parameters_url }
      format.json { head :no_content }
    end
  end
private
  def config_parameter_params
    params.require(:config_parameter).permit!
  end

  def sort_column  
    params[:sort] || "config_parameters.cfg_name"  
  end  
    
  def sort_direction  
    params[:direction] || "asc"  
  end
  
  def search
    params[:search] || ""
  end
  
end
