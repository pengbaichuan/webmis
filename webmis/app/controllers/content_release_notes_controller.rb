class ContentReleaseNotesController < ApplicationController
  before_filter :authorize
  authorize_resource
  
  layout 'webmis'
  
  # GET /content_release_notes
  # GET /content_release_notes.xml
  def index
    @content_release_notes = ContentReleaseNote.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @content_release_notes }
    end
  end

  # GET /content_release_notes/1
  # GET /content_release_notes/1.xml
  def show
    @content_release_note = ContentReleaseNote.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @content_release_note }
    end
  end

  # GET /content_release_notes/new
  # GET /content_release_notes/new.xml
  def new
    @content_release_note = ContentReleaseNote.new
    if params[:product_release_note_id]
      @product_release_note = ProductReleaseNote.find(params[:product_release_note_id])
      if @product_release_note.status != 0
        @product_release_note.status = 0
        @product_release_note.save
        flash[:notice] = "You changed the release notes, status is set to DRAFT."
        logger.info 'changed release notes by ' + current_user.first_name + " " + current_user.last_name
        logger.info '--------------------------------------------'
      end
      @content_release_note.product_release_note_id = params[:product_release_note_id]
      @content_release_note.rank = 20
      @content_release_note.status = 0
    end
    if params[:title]
      @content_release_note.title = params[:title]
      @content_release_note.rank = 10
    end

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @content_release_note }
    end
  end

  # GET /content_release_notes/1/edit
  def edit
    @content_release_note = ContentReleaseNote.find(params[:id])
  end

  # POST /content_release_notes
  # POST /content_release_notes.xml
  def create
    @content_release_note = ContentReleaseNote.new(strong_params)
    @content_release_note.status = 30
    @content_release_note.rank = 100

    respond_to do |format|
      if @content_release_note.save
        format.html { redirect_to(:controller => :product_release_notes,:action => :show, :id => @content_release_note.product_release_note.id) }
        format.xml  { render :xml => @content_release_note, :status => :created, :location => @content_release_note }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @content_release_note.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /content_release_notes/1
  # PUT /content_release_notes/1.xml
  def update
    @content_release_note = ContentReleaseNote.find(params[:id])
    @content_release_note.assign_attributes(strong_params)

    if @content_release_note.content_changed? && @content_release_note.addition_required_yn
      @content_release_note.addition_required_yn = false
    end            

    respond_to do |format|
      if @content_release_note.save       
        flash[:notice] = 'Content of release note was successfully updated.'
        @product_release_note = @content_release_note.product_release_note
        if @product_release_note.status != 0
          @product_release_note.status = 0
          @product_release_note.save
          flash[:notice] = "You changed the release notes, status is set to DRAFT."
          logger.info 'changed release notes by ' + current_user.first_name + " " + current_user.last_name
          logger.info '--------------------------------------------'              
        end
       
        format.html { redirect_to(:controller => :product_release_notes,:action => :show, :id => @content_release_note.product_release_note.id) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @content_release_note.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /content_release_notes/1
  # DELETE /content_release_notes/1.xml
  def destroy
    @content_release_note = ContentReleaseNote.find(params[:id])
    @content_release_note.destroy

    respond_to do |format|
      format.html { redirect_to(content_release_notes_url) }
      format.xml  { head :ok }
    end
  end

  def get_versions_and_regions_for_project
    render :layout => false
  end

  def get_jira_release_note
    project = params["project_key"]
    region = params["region"]
    fixversion = params["fixversion"]
    status = "Closed"
    status = params["status"] if params["status"]
    jql = "project = '#{project}' AND issuetype in standardIssueTypes() AND status = '#{status}' and \"Product Region\" = '#{region}' and fixversion = '#{fixversion}'"
    @content = JiraIssue.where(jql)
    render :layout => false
  end
end
