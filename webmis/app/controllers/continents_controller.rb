class ContinentsController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"  
  helper_method :sort_column, :sort_direction, :search

  # index not used as this is handled by the regions controller
  def index
    if params[:reset]
      params.delete :q
      params.delete :reset
      
      params[:sort] = 'name'
      params[:direction] = 'asc'
    end
    conditions = "1=1 "
    conditions_string_ary = []
    conditions_param_values = []
    
    unless params[:q].blank?
      conditions_string_ary << " and (regions.name LIKE ? OR country_iso_code LIKE ? or country_iso3_code LIKE ? OR continent_code LIKE ?)"
      4.times do
        conditions_param_values << "\%#{params[:q].strip}\%"
      end
    end
    conditions = conditions + conditions_string_ary.join(" ")
    @continents = Continent.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])
    
    if request.xhr?
      flash.keep[:notice]
      render(:partial => "continent", :collection => @continents)
    end      
  end

  def show
    @continent = Continent.find(params[:id])
  end

  def new
    @continent = Continent.new
    @continent.isactive = true
  end

  def edit
    @continent = Continent.find(params[:id])
  end

  def create
    @continent = Continent.new(strong_params)
    @continent.type = 'Continent'
    @continent.display_type_id = AreaType.where("name like 'Continent'").first.id

    respond_to do |format|
      if @continent.save
        format.html { redirect_to @continent, :notice => "Continent was succesfully saved."	 }
        format.json { render json: @continent, :status => :created, :location => @continent }
      else
        format.html { render action: "new" }
        format.json { render json: @continent.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    @continent = Continent.find(params[:id])
    @continent.app_module_ids = [AppModule.where(:abbr => 'MIS').last.id]

    respond_to do |format|
	    if @continent.update_attributes(strong_params)
		    format.html {redirect_to @continent, :notice => 'Continent was successfully updated.' }
        format.json { render :json => @continent, :status => :created, :location => @continent }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @continent.errors, :status => :unprocessable_entity }
	    end
    end
  end

  def delete_region_from_region
    @coverage_region = RegionRelation.find(params[:id])
    temp = @coverage_region.region_id
    @coverage_region.destroy
    redirect_to :action => :edit, :id => temp
  end

  def destroy
    @continent = Continent.find(params[:id])
    @cr = RegionRelation.where("region_id = "+@continent.id.to_s+" or child_region_id = "+@continent.id.to_s)
    for @region_relation in @cr
	@region_relation.destroy
    end
    if @continent.destroy
	flash[:notice] = "Continent succesfully deleted!"
	redirect_to_index
    else
	flash[:notice] = "Database error! Please contact an administrator!"
	redirect_to_index
    end
  end

  def live_search
    conditions = "1=1"
    @regions = ""
    if params[:filter] && params[:filter] != "" && params[:filter].size >= 3
	conditions = conditions + " and (name like '%"+params[:filter]+"%')"
      @regions = Region.where(conditions)
    end
    render :layout => false
  end

  def redirect_to_index
     if params[:q]
	redirect_to :action => :index, :q => params[:q]
     else
        redirect_to :action => :index
     end
  end

  def generate_dakota_id
    render :text => Region.generate_dakota_id
  end
  
  
  
private 
  def sort_column
    params[:sort] || "name"
  end
  
  def sort_direction
    params[:direction] || "asc"
  end
  
  def search
    params[:search] || ""
  end    
end
