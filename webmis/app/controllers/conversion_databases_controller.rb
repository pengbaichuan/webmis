class ConversionDatabasesController < ApplicationController
  before_filter 'authorize', :except => [:exist, :get_details]
  authorize_resource :except => [:exist, :get_details]

  layout "webmis"
  helper_method :sort_column, :sort_direction, :search
  
  def index
    @removed = false

    if params[:reset]
      params.delete :search
      params.delete :reset
      params.delete :search_status
      params.delete :removed
      params.delete :start_date
      params.delete :finish_date
      params[:sort] = 'conversion_databases.id'
      params[:direction] = 'desc'
      params.delete :column_search_field
    end

    conditions = "(production_orders.project_id = #{session[:project_id]} OR conversions.production_orderline_id is null) AND "
    if params[:removed]
      @removed = true
      conditions += " 1=1 "
    elsif !params[:removal_index].blank?
      params[:direction] || "asc"
      conditions += "conversion_databases.file_system_status < #{ConversionDatabase::FILE_SYSTEM_STATUS_MARKED_FOR_REMOVAL}"
    else
      conditions += "conversion_databases.file_system_status < #{ConversionDatabase::FILE_SYSTEM_STATUS_REMOVED}"
    end
    conditions_string_ary = []
    conditions_param_values = []
    conditions_string_ary << conditions

    unless params[:search_status].blank?
      conditions_string_ary << "AND conversion_databases.status = #{params[:search_status].to_i}"
    end

    unless params[:start_date].blank?
      range_begin = params[:start_date]
      range_end = Date.today.strftime("%d-%m-%Y")
      if !params[:finish_date].blank?
        range_end = params[:finish_date]
      end
    end

    unless params[:finish_date].blank?
      if params[:start_date].blank?
        range_begin = ConversionDatabase.available.first.created_at.strftime("%d-%m-%Y")
        range_end = Date.today.strftime("%d-%m-%Y")
      end
    end

    if range_begin
      conditions_string_ary << "AND conversion_databases.created_at BETWEEN '#{DateTime.parse(range_begin)}' AND '#{DateTime.parse(range_end)}'"
    end

    unless params[:search].blank?
      q = params[:search].strip
      conditions_string_ary << "AND (conversion_databases.id = ?
                                OR conversions.id = ?
                                OR data_releases.name LIKE ?
                                OR data_releases.aliases LIKE ?
                                OR company_abbrs.company_name LIKE ?
                                OR conversion_databases.product_id like ?
                                OR conversion_databases.db_type like ?
                                OR conversion_databases.path_and_file like ?
                                OR conversion_databases.remarks like ?)"
      conditions_param_values << q.to_i
      8.times do
        conditions_param_values << "%#{q}%"
      end
    end

    if !column_search_query.empty?
      conditions_string_ary << "AND #{column_search_query['query']}"
      conditions_param_values += column_search_query["values"]
    end
    conditions = conditions_string_ary.join( ' ' )

    jointype = "JOIN"
    jointype = "LEFT OUTER JOIN" if env_production?
    sql_join = "JOIN conversions conversions2 ON conversions2.id = conversion_databases.conversion_id
           #{jointype} production_orderlines on production_orderlines.id = conversions2.production_orderline_id
           #{jointype} production_orders on production_orderlines.production_order_id = production_orders.id"


    @conversion_databases = ConversionDatabase.joins(sql_join).where([conditions] + conditions_param_values).eager_load([ :conversion => [:data_release => [:company_abbr]] ],[:region]).order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])
    generate_id_sequence_cookie(@conversion_databases,params[:page])
    
    if request.xhr?
     render(:partial => "conversion_database", :collection => @conversion_databases,:params => params)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @conversion_databases }
      end
    end

  end

  def batch_mark_for_removal
    params[:databases].each do |conversion_database_id|
      conversion_database = ConversionDatabase.find(conversion_database_id)
      conversion_database.mark_for_removal(current_user)
      conversion_database.save!
    end
    render :nothing => true
  end
  
  # GET /conversion_databases/1
  # GET /conversion_databases/1.xml
  def show
    @conversion_database = ConversionDatabase.find(params[:id])
    @validation_results_view = params[:validation_results_view]
    @conversions = Conversion.find(@conversion_database.conversion_id)
    @related_databases =  ConversionDatabase.present.where(:conversion_id => @conversion_database.conversion_id)

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @conversion_database }
    end
  end

  def handle
    @conversion_database = ConversionDatabase.find(params[:id])
    @validation_results_view = params[:validation_results_view]
    @conversions = Conversion.find(@conversion_database.conversion_id)
    @related_databases =  ConversionDatabase.present.where(:conversion_id => @conversion_database.conversion_id)

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @conversion_database }
    end
  end

  def change_status
    @conversion_database = ConversionDatabase.find(params[:id])
    @conversion_database.status = params[:status]
    if params[:status] == ConversionDatabase::STATUS_REJECTED.to_s
      # Mark database for removal
      @conversion_database.mark_for_removal(current_user,"Validation error rejected by '#{current_user.full_name}' database marked for removal.")
    end
    @conversion_database.save!
    respond_to do |format|
      format.html { redirect_to(@conversion_database, :notice => "Status changed to #{@conversion_database.status_str}") }
      format.xml  { render :xml => @conversion_database }
    end
  end

  def mark_for_removal
    @conversion_database = ConversionDatabase.find(params[:id])
    begin
      @conversion_database.mark_for_removal(current_user,"#{current_user.full_name} marked database for removal.")
      @conversion_database.save!
      flash[:notice] = "Database marked for removal."
    rescue => e
      flash[:error] = "#{e.message}"
    end
    respond_to do |format|
      format.html { redirect_to :back }
      format.xml  { render :xml => @conversion_database }
    end 
  end

  def unmark_for_removal
    @conversion_database = ConversionDatabase.find(params[:id])
    begin
      @conversion_database.unmark_for_removal(current_user)
      @conversion_database.save!
      flash[:notice] = "Database unmarked."
    rescue => e
      flash[:error] = "#{e.message}"
    end
    respond_to do |format|
      format.html { redirect_to :back }
      format.xml  { render :xml => @conversion_database }
    end
  end

  # GET /conversion_databases/new
  # GET /conversion_databases/new.xml
  def new
    @conversion_database = ConversionDatabase.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @conversion_database }
    end
  end

  # GET /conversion_databases/1/edit
  def edit
    # NOT USED ANYMORE
    @conversion_database = ConversionDatabase.find(params[:id])
  end

  # POST /conversion_databases
  # POST /conversion_databases.xml
  def create
    @conversion_database = ConversionDatabase.new(strong_params)

    respond_to do |format|
      if @conversion_database.save
        format.html { redirect_to(@conversion_database, :notice => 'ConversionDatabase was successfully created.') }
        format.xml  { render :xml => @conversion_database, :status => :created, :location => @conversion_database }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @conversion_database.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /conversion_databases/1
  # PUT /conversion_databases/1.xml
  def update
    # NOT USED ANYMORE
    @conversion_database = ConversionDatabase.find(params[:id])

    respond_to do |format|
      if @conversion_database.update_attributes(strong_params)
        format.html { redirect_to(@conversion_database, :notice => 'ConversionDatabase was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @conversion_database.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /conversion_databases/1
  # DELETE /conversion_databases/1.xml
  def msdestroy
    @conversion_database = ConversionDatabase.find(params[:id])
    @conversion_database.destroy

    respond_to do |format|
      format.html { redirect_to(conversion_databases_url) }
      format.xml  { head :ok }
    end
  end
  
  def marked_for_removal_index
    jointype = "JOIN"
    jointype = "LEFT OUTER JOIN" if env_production?
    sql_join = "JOIN conversions ON conversions.id = conversion_databases.conversion_id
           #{jointype} production_orderlines on production_orderlines.id = conversions.production_orderline_id
           #{jointype} production_orders on production_orderlines.production_order_id = production_orders.id"

    @conversion_databases = ConversionDatabase.joins(sql_join).where(:file_system_status => ConversionDatabase::FILE_SYSTEM_STATUS_MARKED_FOR_REMOVAL).where("production_orders.project_id = #{session[:project_id]} OR conversions.production_orderline_id is null")
  end
   
  def db_remove_script
    proceed = false
    @conversion_databases = []
    if (params[:remove_selected] || params[:delete_selected]) && params[:selected]
      proceed = true
      databases = ConversionDatabase.find([params[:selected]])
    end
    
    if params[:remove_all]
      proceed = true
      databases = ConversionDatabase.marked_for_removal
    end  
    
    if proceed
      if params[:remove_selected] || params[:remove_all]
        databases.each do |line|
          next unless line.project_id == session[:project_id]
          line.set_file_system_status(ConversionDatabase::FILE_SYSTEM_STATUS_REMOVED)
          line.remarks = line.remarks.to_s + "Removed as batch by #{current_user.first_name.to_s + " " + current_user.last_name.to_s}."
          line.save
          @conversion_databases << line
        end
      elsif params[:delete_selected]
        result = ConversionDatabase.remove_multiple(databases)
        if result
          flash[:notice] = "All selected databases where succesfully removed"
        else
          flash[:error] = "Some databases where not removed, please check removal remarks"
        end
        redirect_to :action => 'marked_for_removal_index'
        return
      end
    else
      flash[:notice] = "No databases to remove."
      redirect_to :back
    end
  end
  
  def download_db_remove_script
    content = ConversionDatabase.manual_removal_script([params[:dbs]])
    send_data content,  :filename => "test.sh" 
  end

  def download_stdout_log
    @conversion_database = ConversionDatabase.find(params[:id])
    send_data @conversion_database.stdout_log, :filename => "stdout_#{@conversion_database.shortest}.txt"
  end

  def render_file_tree
    @conversion_database = ConversionDatabase.find(params[:id])
    @path = @conversion_database.path.to_s
    render_file_browse_modal(@path)
  end

#merged from backoffice project
  def exist
    if !params[:path] && !params[:path].blank?
     message = "path parameter missing"
     return render :text => message, :status => :unprocessable_entity
    else
     q = params[:path].strip.chop.reverse.chop.reverse
     databases = ConversionDatabase.available.where("path_and_file LIKE ?" ,"\%#{q}\%")
     if databases && !databases.empty?
       return render :text => databases.last.path_and_file, :layout => false
     else
       return render :text => 'false', :status => :unprocessable_entity, :layout => false
     end
    end
  end

  def get_details
    if !params[:path] && !params[:path].blank?
     message = "path parameter missing"
     return render :text => message, :status => :unprocessable_entity
    else
      q = params[:path].strip.chop.reverse.chop.reverse # path-parameter comes quoted
      databases = ConversionDatabase.available.where("path_and_file LIKE ?" ,"\%#{q}\%")
      if databases && !databases.empty?
        return_hash = Hash.new
        databases.each do |db|
          return_hash[db.id.to_s] = db.detail_hash
        end
        return render :text =>  return_hash.to_json
      else
        return render :text => "Could not find databases for \'#{q}\'.", :status => :unprocessable_entity
      end
    end
  end
  
  private  
  def sort_column  
    params[:sort] || "conversion_databases.id"  
  end  
    
  def sort_direction
    if params[:removal_index].blank?
      params[:direction] || "desc"
    else
      params[:direction] || "asc"
    end
  end
  
  def search
    params[:search] || ""
  end
end
