class ConversionEnvironmentsController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search

  # GET /conversion_environments
  # GET /conversion_environments.json
  def index
    
    if params[:reset]
      params.delete :search
      params.delete :reset
      params.delete :incl_inactive

      params[:sort] = 'conversion_environments.name'
      params[:direction] = 'desc'
    end

    conditions = '1=1 '
    conditions_string_ary = []
    conditions_param_values = []

    unless params[:incl_inactive] == 'on'
       conditions_string_ary << " and (conversion_environments.active_yn = 1)"
    end

    unless params[:search].blank?
      conditions_string_ary << " and (conversion_environments.id LIKE ?
              or conversion_environments.name LIKE ?
               or conversion_environments.remarks LIKE ?)"
      3.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
    end

    conditions = conditions + conditions_string_ary.join(" ")

    @conversion_environments = ConversionEnvironment.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])

    if request.xhr?
      render(:partial => "conversion_environment", :collection => @conversion_environments)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @conversion_environments }
      end
    end
  end

  # GET /conversion_environments/1
  # GET /conversion_environments/1.json
  def show
    @conversion_environment = ConversionEnvironment.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @conversion_environment }
    end
  end

  # GET /conversion_environments/new
  # GET /conversion_environments/new.json
  def new
    @conversion_environment = ConversionEnvironment.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @conversion_environment }
    end
  end

  # GET /conversion_environments/1/edit
  def edit
    @conversion_environment = ConversionEnvironment.find(params[:id])
  end

  # POST /conversion_environments
  # POST /conversion_environments.json
  def create
    @conversion_environment = ConversionEnvironment.new(strong_params)

    respond_to do |format|
      if @conversion_environment.save
        format.html { redirect_to @conversion_environment, notice: 'Conversion environment was successfully created.' }
        format.json { render json: @conversion_environment, status: :created, location: @conversion_environment }
      else
        format.html { render action: "new" }
        format.json { render json: @conversion_environment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /conversion_environments/1
  # PUT /conversion_environments/1.json
  def update
    @conversion_environment = ConversionEnvironment.find(params[:id])

    respond_to do |format|
      if @conversion_environment.update_attributes(strong_params)
        format.html { redirect_to @conversion_environment, notice: 'Conversion environment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @conversion_environment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /conversion_environments/1
  # DELETE /conversion_environments/1.json
  def destroy
    @conversion_environment = ConversionEnvironment.find(params[:id])
    @conversion_environment.destroy

    respond_to do |format|
      format.html { redirect_to conversion_environments_url }
      format.json { head :no_content }
    end
  end

  def toggle_active_yn
    @conversion_environment = ConversionEnvironment.find(params[:id])
      if @conversion_environment.active_yn
        @conversion_environment.active_yn = false
        flash[:notice] = 'Conversion environment deactivated.'
      else
        @conversion_environment.active_yn = true
        flash[:notice] =  'Conversion environment activated.'
      end
    @conversion_environment.save

    respond_to do |format|
      format.html { redirect_to(:back) }
      format.xml  { head :ok }
    end
  end

  private
  def sort_column
    params[:sort] || "conversion_environments.name"
  end

  def sort_direction
    params[:direction] || "desc"
  end

  def search
    params[:search] || ""
  end
end
