class ConversionJobsController < ApplicationController
  before_filter :authorize
  authorize_resource param_method: :strong_params
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search

  # GET /conversion_jobs
  # GET /conversion_jobs.json
  def index
    if params[:reset]
      params.delete :search_status
      params.delete :search
      params.delete :sort
      params.delete :direction

      # when from jobs/tagged_failed_jobs_report
      params.delete :date_range_a
      params.delete :date_range_b
      params.delete :fail_category

      params.delete :sort
      params.delete :direction
    end

    conditions_string_ary = ["( jobs.project_id = #{session[:project_id]} )"]
    conditions_param_values = []

    status = params[:search_status]
    unless status.blank?
      conditions_string_ary << "AND (jobs.status = ?)"
      conditions_param_values << status
    end

    if params[:fail_reason]
      conditions_string_ary << "AND (jobs.fail_reason <> '')"
    end

    unless params[:search].blank?
      conditions_string_ary << "AND (jobs.id = #{params[:search].to_i}
        or jobs.conversion_id LIKE ?
         or jobs.run_server LIKE ?
          or jobs.working_dir LIKE ?
           or jobs.run_script LIKE ?
            or jobs.bug_tracker_id LIKE ?) OR (tags.name LIKE ?)"
      6.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
    end

    # when from jobs/tagged_failed_jobs_report
    unless params[:fail_category].blank?
      range_end = Date.today.strftime("%d-%m-%Y")
      unless params[:date_range_a].blank?
        range_begin = params[:date_range_a]
        if !params[:date_range_b].blank?
          range_end = params[:date_range_b]
        end
      else
        range_begin = ConversionJob.fail_reason_tagged.first.fail_category_logged_on.strftime("%d-%m-%Y")
      end

      unless params[:date_range_b].blank?
        if params[:date_range_a].blank?
          range_end = Date.today.strftime("%d-%m-%Y")
        end
      end
      conditions_string_ary << "AND (jobs.fail_category_logged_on BETWEEN '#{DateTime.parse(range_begin)}' AND '#{DateTime.parse(range_end)}')"
      if params[:fail_category] != 'all'
        conditions_string_ary << "AND (jobs.fail_category = ?)"
        conditions_param_values << params[:fail_category]
      end
    end

    conditions = conditions_string_ary.join(" ")
    selection = "jobs.id,jobs.status,file_system_status,run_server,jobs.conversion_id,start_time,finish_time,jobs.lock_version"    
    @conversion_jobs = ConversionJob.includes(:tags,:production_orderline).references(:tags,:production_orderline).select(selection).where([conditions] + conditions_param_values).references(:taggings).order(sort_column + ' ' + sort_direction).paginate(:per_page => 30, :page => params[:page])
    generate_id_sequence_cookie(@conversion_jobs,params[:page])

    if request.xhr?
      render(:partial => "conversion_job", :collection => @conversion_jobs)
    end
  end
  
  def failures
    if params[:sort].blank?
      params[:sort] = sort_column
      params[:direction] = sort_direction
    end
    selection = "jobs.id,jobs.status,file_system_status,run_server,jobs.conversion_id,start_time,finish_time"
    @conversion_jobs = ConversionJob.failures.includes(:conversion,:production_orderline).select(selection).where("jobs.project_id = #{session[:project_id]}").order(sort_column + ' ' + sort_direction).paginate(per_page: 30, page: params[:page])

    if request.xhr?
      render(:partial => "failed_conversion_job", :collection => @conversion_jobs)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @conversion_jobs }
      end
    end
  end

  def tagging_fail_reason
    @success_rows = []
    params[:ids].each do |job_id|
      if is_int?(job_id)
        job = ConversionJob.find(job_id)
        job.fail_category = params[:fail_reason]
        job.fail_category_logged_by_user_id = current_user.id
        job.fail_category_logged_on = Time.now
        if job.save
          @success_rows << job.id
        end
      end
    end
    render :layout => false
  end

  # GET /conversion_jobs/1
  # GET /conversion_jobs/1.json
  def show
    @conversion_job = ConversionJob.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @conversion_job }
    end
  end

  def terminal
    @conversion_job = ConversionJob.find(params[:id])
    result = @conversion_job.terminal(request.remote_ip,current_user)
    render :text => "res: " + result + "|"
  end

  def tail_z_log
    @conversion_job = ConversionJob.find(params[:id])
    @z_log_data = @conversion_job.tail_z_log

    render(:partial => "z_log", :collection => [@conversion_jobs,@z_log_data])
  end

  # GET /conversion_jobs/new
  # GET /conversion_jobs/new.json
  def new
    @conversion_job = ConversionJob.new(:project_id => session[:project_id])

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @conversion_job }
    end
  end

  # GET /conversion_jobs/1/edit
  def edit
    @conversion_job = ConversionJob.find(params[:id])
    if @conversion_job.allow_edit?
      if @conversion_job.status != ConversionJob::STATUS_MANUAL
        ConversionJob.transaction do
          # avoid pickup by scheduler during edit
          @conversion_job.set_status(ConversionJob::STATUS_MANUAL)
          @conversion_job.log_action("Editing job. Status changed to #{@conversion_job.status_str}.",current_user)
          begin
            @conversion_job.save!
          rescue ActiveRecord::StaleObjectError
            flash[:error] = "This job was changed while you tried to editing it. Please try again later."
            redirect_to :action => 'show', :id => @conversion_job.id       
          rescue => e
            flash[:error] = e.message
            redirect_to :action => 'show', :id => @conversion_job.id
          end
        end
      end
      @script = @conversion_job.job_script
    else
      flash[:error] = "Editing is not allowed for the current status of the conversion job."
      redirect_to :action => 'show'
    end
  end

  # POST /conversion_jobs
  # POST /conversion_jobs.json
  def create
    @conversion_job = ConversionJob.new(strong_params)

    respond_to do |format|
      if @conversion_job.save
        format.html { redirect_to @conversion_job, notice: 'Conversion job was successfully created.' }
        format.json { render json: @conversion_job, status: :created, location: @conversion_job }
      else
        format.html { render action: "new" }
        format.json { render json: @conversion_job.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /conversion_jobs/1
  # PUT /conversion_jobs/1.json
  def update
    begin
      ConversionJob.transaction do
        @conversion_job = ConversionJob.find(params[:id])
        server_name = params[:conversion_job][:run_server].blank? ? '-' : params[:conversion_job][:run_server]

        if params[:commit] == "Execute"
          @conversion_job.stdout_log = "Job is manually executed on server '#{server_name}'."
          @conversion_job.set_status(ConversionJob::STATUS_MANUAL)
          perform_now = true
        elsif params[:commit] == "Schedule"
          @conversion_job.stdout_log = "Job is manually queued" + (server_name.blank? ? '.' : " on server '#{server_name}'.")
          @conversion_job.set_status(ConversionJob::STATUS_QUEUED)
          @conversion_job.fail_reason = ""
          perform_now = false
        elsif params[:commit] == "Save"
          @conversion_job.stdout_log = "Job is saved."
          @conversion_job.set_status(ConversionJob::STATUS_MANUAL)
          perform_now = false
        else
          raise "Unknown submit operation '#{params[:commit]}'."
        end
        @conversion_job.fail_reason = ''
        @conversion_job.log_scheduler("Job manually set to #{@conversion_job.status_str} for server #{server_name}.", current_user)
        @conversion_job.log_action(@conversion_job.stdout_log, current_user)

        @conversion_job.tag_list = "#{params[:free_tags]}"

        if @conversion_job.job_script && params[:conversion_job][:run_script] != @conversion_job.job_script.to_json
          # script has been changed by the user
          @conversion_job.job_script = JobScript.new(params[:conversion_job][:run_script])
          @conversion_job.job_script.updated_by = current_user.full_name 
        end

        @conversion_job.assign_attributes(strong_params)
        @conversion_job.log_action("#{params[:commit]}d",current_user)

        respond_to do |format|
          if @conversion_job.save!(params[:conversion_job])
            @conversion_job.perform if perform_now

            format.html { redirect_to @conversion_job, notice: 'Conversion job was successfully updated.' }
            format.json { head :no_content }
          else
            flash[:error] = "Error updating job"
            format.html { render action: "edit" }
            format.json { render json: @conversion_job.errors, status: :unprocessable_entity }
          end
        end
      end
    rescue ActiveRecord::StaleObjectError
      flash[:error] = "This job was changed while you were editing it. Please try again."
      redirect_to :action => 'edit'
    end
  end

  # DELETE /conversion_jobs/1
  # DELETE /conversion_jobs/1.json
  def destroy
    @conversion_job = ConversionJob.find(params[:id])
    @conversion_job.destroy

    respond_to do |format|
      format.html { redirect_to conversion_jobs_url }
      format.json { head :no_content }
    end
  end

  def handle_crashed
    @conversion_job = ConversionJob.find(params[:id])
    @conversion_job.lock_version = params[:lock_version]

    @conversion_job.bug_tracker_id = params[:bug_tracker_id]
    unless params[:job_fail_reason].blank?
      @conversion_job.fail_category = params[:job_fail_reason]
      @conversion_job.fail_category_logged_by_user_id = current_user.id
      @conversion_job.fail_category_logged_on = Time.now
    end
    @conversion_job.save!

    if !@conversion_job.allow_cleanup?
      flash[:warn] = 'Cleanup already in progress or no longer allowed.'
    elsif params[:move_to_crash_dir] == 'true' && !params[:bug_tracker_id].blank?
      begin
        @conversion_job.log_action("Moved working directory to #{ConfigParameter.get("conversion_job_crash_dir",session[:project_id])}#{params[:bug_tracker_id]}.",current_user)
        @conversion_job.move_to_crashed
        flash[:notice] = "Moving to crash directory"
      rescue Exception => e
        flash[:error] = e.message
        puts e.message
        puts e.backtrace
      end
    else
      @conversion_job.clear_working_dir
      flash[:info] = "Working directory cleared"
    end
    redirect_to :action => "show"
  end

  def clean_working_dir
    @conversion_job = ConversionJob.find(params[:id])
    @conversion_job.clear_working_dir
    redirect_to :action => "show"
  end

  def cancel
    begin
      @conversion_job = ConversionJob.find(params[:id])
      @conversion_job.lock_version = params[:lock_version]
      @conversion_job.cancel(current_user.full_name)
      Postoffice.conversion_job_status_change(@conversion_job).deliver_now
    rescue ActiveRecord::StaleObjectError
      flash[:error] = "This job was changed while you were cancelling it."
    end
    redirect_to :action => "show"
  end

  def edit_new_copy
    @conversion_job_org = ConversionJob.find(params[:id])
    @conversion_job_org.lock_version = params[:lock_version] unless params[:lock_version].blank?
    if @conversion_job_org.allow_reschedule?
      begin
        ConversionJob.transaction do
          @conversion_job = @conversion_job_org.reschedule(current_user, manual_yn = true)
          redirect_to :action => 'edit', :id => @conversion_job.id
        end
      rescue ActiveRecord::StaleObjectError
        flash[:error] = 'This job was changed while you were editing it. Please try again.'
        redirect_to :action => 'show', :id => @conversion_job_org.id
      rescue => e
        flash[:error] = "Error rescheduling: #{e.message}"
        redirect_to :action => 'show', :id => @conversion_job_org.id
      end
    else
      if @conversion_job_org.busy?
        flash[:error] = 'Editing a new copy of this job is not allowed yet. Please wait till move action is finished.'
      else
        flash[:error] = 'Editing a new copy of this job is no longer allowed. The production orderline it belongs to probably just finished.'
      end
      redirect_to :action => 'show', :id => @conversion_job_org.id
    end
  end

  def reschedule
    @conversion_job_org = ConversionJob.find(params[:id])
    @conversion_job_org.lock_version = params[:lock_version] unless params[:lock_version].blank?
    if @conversion_job_org.allow_reschedule?
      begin
        @conversion_job = @conversion_job_org.reschedule(current_user)
        redirect_to :action => 'show', :id => @conversion_job.id
      rescue ActiveRecord::StaleObjectError
        flash[:error] = 'This job was changed while you were editing it. Please try again.'
        redirect_to :action => 'show', :id => @conversion_job_org.id
      rescue => e
        flash[:error] = "Error rescheduling: #{e.message}"
        redirect_to :action => 'show', :id => @conversion_job_org.id
      end
    else
      flash[:error] = 'Rescheduling is no longer allowed. The production orderline this belongs to probably just finished.'
      redirect_to :action => 'show', :id => @conversion_job_org.id
    end
  end

  def generated_script
    script = JobScript.new(params[:script])
    conversion_job = ConversionJob.find(params[:id])
    script.updated_by = current_user.full_name unless params[:script] == conversion_job.job_script.to_json
    render :text => script.to_script("CONVERSION_JOB_ID", params[:id])
  end

  private
  def sort_column
    params[:sort] || "jobs.id"
  end

  def sort_direction
    params[:direction] || "desc"
  end

  def search
    params[:search] || ""
  end
end
