class ConversionsController < ApplicationController
  before_filter :authorize
  authorize_resource

  layout "application"
  helper_method :sort_column, :sort_direction, :search

  def index
    @datareleases = DataRelease.production.collect{|c|[c.name,c.name] if c}.uniq.compact.sort.reverse
    if params[:reset]
      params.delete :search
      params.delete :reset
      params.delete :sort
      params.delete :direction
      params.delete :column_search_field
    end
    conditions_string_ary = []
    conditions_param_values = []

    if env_production?
      conditions_string_ary <<  "( production_orders.project_id = #{session[:project_id]} OR conversions.production_orderline_id is null )"
    else
      conditions_string_ary << "( production_orders.project_id = #{session[:project_id]} )"
    end

    if params[:mine]
      @cu = current_user.full_name
      conditions_string_ary <<  "(conversions.request_by = '#{@cu}')"
    end

    olid = params[:olid]
    unless olid.blank?
      conditions_string_ary <<  "(conversions.production_orderline_id = ?)"
      conditions_param_values << olid
    end

    unless params[:search].blank?
      conditions_string_ary << "(conversions.id = #{params[:search].to_i}
                                 OR conversions.productid LIKE ?
                                 OR data_releases.name LIKE ?
                                 OR company_abbrs.company_name LIKE ?
                                 OR database_name LIKE ?
                                 OR additional_info LIKE ?
                                 OR reference_numbers LIKE ?
                                 OR data_files LIKE ?
                                 OR template_filename LIKE ?
                                 OR conversiontools.code LIKE ?
                                 OR cvtool_bundles.name LIKE ?
                                 OR assembly_headers.name LIKE ?
                                 OR data_releases.aliases LIKE ?)"
      12.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
    end
    if !column_search_query.empty?
      conditions_string_ary << "#{column_search_query['query']}"
      conditions_param_values +=  column_search_query["values"]
    end
    conditions = conditions_string_ary.join(" AND ")


    @favorites = Conversion.tagged_with("favorite", :on => :favorites, :owned_by => current_user)
    @conversions = Conversion.joins('LEFT OUTER JOIN data_releases ON data_releases.id = conversions.data_release_id
                                               JOIN company_abbrs ON company_abbrs.id = data_releases.company_abbr_id
                                               LEFT JOIN conversiontools ON conversiontools.id = conversiontool_id
                                               LEFT OUTER JOIN cvtool_bundles on cvtool_bundles.id = cvtool_bundle_id
                                               LEFT OUTER JOIN assembly_headers on assembly_headers.id = assembly_id
                                               LEFT OUTER JOIN production_orderlines on production_orderline_id = production_orderlines.id
                                               LEFT OUTER JOIN production_orders on production_orderlines.production_order_id = production_orders.id').where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])
    generate_id_sequence_cookie(@conversions,params[:page])

    if request.xhr?
      flash.keep[:notice]
      render(:partial => "conversion", :collection => @conversions)
    end


  end

  # GET /conversions/1 GET /conversions/1.xml
  def show
    @results_source = ""
    @conversion = Conversion.find(params[:id])

    @database = ConversionDatabase.where(:conversion_id => params[:id]).order(:filename)
    @results = @database.collect {|x|[x.id,x.path_and_file]}
    @results_id = @database.collect {|x|x.id} if !@results.empty?
    @results_source = 'conversion_databases' if !@results.empty?

    if @conversion.reception_reference && @conversion.reception_reference.size > 0
      r = Reception.find(@conversion.reception_reference)
      if r
        @conversion.reception_reference = r.id
        if @results.empty? && @conversion.reception_reference.to_s != ""
          @results = [] << Reception.find(@conversion.reception_reference).storage_location.to_s
          @results_id = [] << Reception.find(@conversion.reception_reference).id.to_s
          @results_source = 'receptions'
        end
      end
    end
    @favorite = false
    if @conversion.owner_tags_on(current_user, :favorites).size > 0
      @favorite = true
    end


    respond_to do |format|
      format.html
      format.xml  { render :xml => @conversion }
    end
  end

  def handle
    @results_source = ""
    @conversion = Conversion.find(params[:id])

    @database = ConversionDatabase.where(:conversion_id => params[:id])
    @results = @database.collect {|x|[x.id,x.path_and_file]}
    @results_id = @database.collect {|x|x.id} if !@results.empty?
    @results_source = 'conversion_databases' if !@results.empty?

    if @conversion.reception_reference && @conversion.reception_reference.size > 0
      r = Reception.find(@conversion.reception_reference)
      if r
        @conversion.reception_reference = r.id
        if @results.empty? && @conversion.reception_reference.to_s != ""
          @results = [] << Reception.find(@conversion.reception_reference).storage_location.to_s
          @results_id = [] << Reception.find(@conversion.reception_reference).id.to_s
          @results_source = 'receptions'
        end
      end
    end
    @favorite = false
    if @conversion.owner_tags_on(current_user, :favorites).size > 0
      @favorite = true
    end

    @mark_for_removal_values = determine_mark_for_removal_values(@conversion)

    respond_to do |format|
      format.html
      format.xml  { render :xml => @conversion }
    end
  end

  def report
    @conversion = Conversion.find(params[:id])
    @input_report_data = @conversion.input_report_data
  end

  # GET /conversions/new GET /conversions/new.xml
  def new
    @conversion = Conversion.new
    @conversion.request_date = Date.today
    if params[:return_to_controller] == "production_orders"
      @conversion.production_orderline_id = params[:production_orderline_id]
      if !@conversion.production_orderline.product_id.blank?
        @conversion.productid = @conversion.production_orderline.product_id
        @conversion.assign_attributes( Conversion.map_attributes_from_product_id(@conversion.productid) )
      end
    end
    # init_new
    respond_to do |format|
      format.html {render :action => "new"}
      format.xml  {render :xml => @conversion }
    end
  end

  def cloneit
    @conversion_org = Conversion.find(params[:cloneid])
    @conversion = @conversion_org
    fmsg = ''

    begin
      Conversion.transaction do
        @database = []
        include_tpd = params[:inctpd] ? true : false
        without_production_orderline = params[:nopo] ? true : false
        @conversion = @conversion_org.duplicate(current_user,without_production_orderline,include_tpd)
        @conversion.save!

        fmsg = "Succesfully cloned conversion #{@conversion_org.id.to_s} to #{@conversion.id.to_s}"

        if @conversion.production_orderline
          pol = @conversion.production_orderline
          pol.chosen_conversion_id  = @conversion.id
          pol.save!
        else
          fmsg = fmsg + '<br />Cloned without relation to a production order or product.'
        end
      end
    rescue => e
      fmsg = e.message
      flash[:error] = fmsg
      redirect_to :back , :error => fmsg
    end

    @pass_params = params
    @pass_params.delete :action
    @pass_params.delete :controller
    flash[:notice] = fmsg
    redirect_to({:action => "edit", :id => @conversion.id,:params=>@pass_params},:notice => fmsg)
  end

  def new_patch_request
    @conversion_org = Conversion.find(params[:id])
    @conversion = @conversion_org.create_patch_request(current_user,params[:conversion_database_id])
    fmsg = "Succesfully created patch request based on outcome of conversion #{@conversion_org.id.to_s}"
    redirect_to({:action => "edit", :id => @conversion.id}, :notice => fmsg)
  end

  # GET /conversions/1/edit
  def edit
    @conversion = Conversion.find(params[:id])
    @pass_params = params

    if @conversion.job_support?
      redirect_to :action => "index"
      flash[:error] = "This is a automate job support request, no editing allowed"
    end
  end

  def allocate_data
    @succesful = true
    @conversion = Conversion.find(params[:id])
    @errormsg = ""
    begin
      @conversion.allocate_data
      @conversion.add_metadata
      count = @conversion.create_assemblies_from_design
      @errormsg = "ALLOCATION SUCCESFUL / #{count} ASSEMBLIES CREATED"
    rescue => e
      puts e.message
      puts e.backtrace
      @succesful = false
      @errormsg = e.message
    end
  end

  def new_tpd
    @conversion = DummyConversion.new
  end

  def tpd
    @conversion = Conversion.find(params[:id])
  end

  # POST /conversions POST /conversions.xml
  def create
    message = ''
    @conversion = Conversion.new(strong_params)
    @conversion.request_by = current_user.full_name
    @conversion.request_user_id = current_user.id
    @conversion.end_product_yn = 'No' if @conversion.end_product_yn.blank?

    # Define source of input data
    if params[:conversion][:reference_numbers]
      references = params[:conversion][:reference_numbers].split("\r\n")
      data = params[:conversion][:data_files]
      begin
        @conversion.reference_numbers = @conversion.define_input_source(references, data, @conversion.purpose).join("\r\n")
      rescue => e
        @conversion.errors[:base] << e
      end
    end

    respond_to do |format|
      if @conversion.save && !@conversion.errors.any?
        message = message + "The conversion was created with Id : " + @conversion.id.to_s
        # Update production order
        unless params[:conversion][:production_orderline_id].blank?
          @production_orderline = ProductionOrderline.find(params[:conversion][:production_orderline_id])
          @production_order = @production_orderline.production_order
          @production_orderline.chosen_conversion_id = @conversion.id
          @production_orderline.save!
          @production_orderline.transition(current_user)
          @production_orderline.update_bp_message('Conversion prepared')
          @production_orderline.reload

          message += "Production order #{@production_order.id} #{@production_order.name.to_s} line status is updated to #{@production_orderline.bp_name}.<br />"
        end

        if params[:return_to_controller] && params[:return_to_action]
          flash[:notice] = message
          format.html { redirect_to :action => params[:return_to_action].to_s, :controller => params[:return_to_controller].to_s, :id => params[:return_to_id].to_s }
        else
          format.html { redirect_to(@conversion, :notice => message) }
          format.xml  { render :xml => @conversion, :status => :created, :location => @conversion }
        end
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @conversion.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /conversions/1 PUT /conversions/1.xml
  def update
    message = ''
    @conversion = Conversion.find(params[:id])
    org_status  = @conversion.status
    @conversion.assign_attributes(strong_params)
    # reject = false Define source of input data
    if params[:conversion][:reference_numbers]
      references = params[:conversion][:reference_numbers].split("\r\n")
      data = params[:conversion][:data_files]
      begin
        @conversion.reference_numbers = @conversion.define_input_source(references, data, @conversion.purpose).join("\r\n")
      rescue => e
        @conversion.errors[:base] << e
      end
    end

    respond_to do |format|
      if @conversion.save && !@conversion.errors.any?
        message += "Conversion request updated."

        # Update production order
        if @conversion.production_orderline
          # Extra check to avoid distorted order flow
          if @conversion.allow_production_orderline_transition?
            message += @conversion.production_orderline.update_from_manual_conversion_request(current_user,@conversion)
          end
        end

        # Mail events
        @conversion.deliver_manual_update_mail(params[:conversion][:outcome],org_status)

        if params[:return_to_controller] && params[:return_to_action]
          flash[:notice] = message
          redirect_to :action => params[:return_to_action].to_s, :controller => params[:return_to_controller].to_s, :id => params[:return_to_id].to_s
        else
          format.html { redirect_to(@conversion, :notice => message) }
          format.xml  { render :xml => @conversion, :status => :created, :location => @conversion }
        end
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @conversion.errors, :status => :unprocessable_entity }
      end
    end
  end

  def edit_result
    @conversion = Conversion.find(params[:id])   
    if @conversion.job_support?
      redirect_to :action => "index"
      flash[:error] = "This is a automate job support request, no editing allowed"
    end
    if @conversion.status == Conversion::STATUS_REJECTED
      redirect_to @conversion
      flash[:error] = "This request was rejected, no editing allowed"
    end
    if @conversion.status == Conversion::STATUS_ENTRY
      redirect_to @conversion
      flash[:error] = "This request in not requested yet, no editing allowed"
    end
  end

  def update_result
    @conversion = Conversion.find(params[:id])
    org_status = @conversion.status
    message = ''
    @conversion.assign_attributes(strong_params)

    # Reject request ?
    if params[:reject]
     @conversion.set_status(Conversion::STATUS_REJECTED)
      if @conversion.production_orderline
        pol = @conversion.production_orderline
        pol.chosen_conversion_id = nil
        pol.save!
      end
    end

    # Renew result conversion_databases ?
    if !params[:conversion][:result_files].blank?
      result_databases = []
      prev_conversion_databases = []
      result_databases = @conversion.result_files.split("\n").collect{|f|f.strip if !f.to_s.strip.blank?}.compact
      prev_conversion_databases = @conversion.conversion_databases.available
      if prev_conversion_databases.empty? || (result_databases.sort != prev_conversion_databases.collect{|db|db.path_and_file}.sort || !result_databases.empty?)
        prev_conversion_databases.each{|cd|cd.remove_from_manual_conversion_request} # Remove previous results for renew
        result_databases.each{|result_db|ConversionDatabase.create_from_manual_conversion_request(@conversion,result_db)} # New conversion_databases needs to be created
      end

      # link to correct production order line and fix product_id if needed (derived product)
      if @conversion.production_orderline
        @conversion.production_orderline.production_order.production_orderlines.each do |line|
          @conversion.conversion_databases.each do |db|
            if line.product.file_for_product?(db.path_and_file)
              db.product_id = line.product_id
              db.production_orderline_id = line.id
              db.save!
            end
          end
        end
      end
    end

    # Handle outcome-field
    @conversion.map_outcome_to_status(params[:conversion][:outcome],params[:conversion][:tpd_products])
    if params[:conversion][:outcome] != 'No result'
      @conversion.size_bytes = params[:size_bytes].to_s.gsub(",","").gsub(".","").to_i
      @conversion.conversion_date = Time.now
    end
    proceed = false
    Conversion.transaction do
      if @conversion.save
        proceed = true
      end
      # Inform order
      if org_status != @conversion.status && !@conversion.production_orderline.nil?
        @conversion.production_orderline.update_bp_message("#{@conversion.status_str} - #{params[:conversion][:outcome]}")
        # Extra check to avoid distorted order flow
        if @conversion.allow_production_orderline_transition?
          message += @conversion.production_orderline.update_from_manual_conversion_request(current_user,@conversion,true )
        end
      end
    end


    respond_to do |format|
      if proceed
        message += "Conversion request updated."

        # Mail events
        @conversion.deliver_manual_update_mail(params[:conversion][:outcome],org_status)

        format.html { redirect_to(@conversion, :notice => message) }
        format.xml  { render :xml => @conversion, :status => :created, :location => @conversion }
      else
        format.html { render :action => "edit_result" }
        format.xml  { render :xml => @conversion.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /conversions/1 DELETE /conversions/1.xml
  def destroy
    @conversion = Conversion.find(params[:id])
    @conversion.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => "conversions" , :action => :index) }
      format.xml  { head :ok }
    end
  end

  def mail
    if params[:production_orderline_id].blank?
      conv = Conversion.find(params[:id])
    else
      conv = Conversion.where(:production_orderline_id => params[:production_orderline_id],:status => Conversion::STATUS_ENTRY).last
      production_orderline = ProductionOrderline.find(params[:production_orderline_id])
      production_orderline.transition(current_user)
      production_orderline.update_bp_message('Conversion requested')
    end

    if conv
      conv.deliver_next_mail
      flash[:notice] = 'Conversion request notification sent.'
    else
      flash[:error] = 'No conversion with status ENTRY found. Conversion request notification could not be sent. '
    end

    if params[:return_to_controller] && params[:return_to_action]
      redirect_to :action => params[:return_to_action].to_s,
        :controller => params[:return_to_controller].to_s, :id => params[:return_to_id].to_s
    else
      redirect_to :back
    end
  end

  def get_cvtool_bundles
    @bundle = Array.new
    @bundle << ""
    @cb = CvtoolBundle.select_cvtool_bundles(params[:conversaiontool_id])
    render :partial => "cvtool_bundle_select"
  end

  def get_datareleases
    @conversion = Conversion.new
    if (params[:caid] && params[:caid] != "")
      @data_releases = DataRelease.order('name desc').where('production = 1 and data_releases.name is not null and company_abbr_id = ' + params[:caid])
    else
      @data_releases = []
    end
    render :partial => "datarelease_select"
  end

  def get_datareleases_for_overview
    if !params[:supplier_name].blank?
      companyabbr = CompanyAbbr.active.find_by_company_name(params[:supplier_name].strip)
      @datareleases = DataRelease.where(company_abbr_id:companyabbr.id).collect{|c|[c.name,c.name] if c}.uniq.sort.reverse
    else
      @datareleases = DataRelease.production.collect{|c|[c.name,c.name] if c}.uniq.compact.sort.reverse
    end
    render :partial => "data_release_select_for_overview", :object => @datareleases, :layout => false
  end

  def get_production_orders
    if params[:productid]
      @porders = ProductionOrderline.joins("production_orderlines, production_orders").where("production_orderlines.product_id = '#{params[:productid]}' and production_orders.allow_production = 1").uniq.collect{|x|[x.id,x.production_order.name]if x.production_order}
    end
    render :partial => "po_order_select"
  end

  def get_data_by_pid
    if params[:productid] && @product = Product.find_by_real_volumeid("#{params[:productid]}")
      @attrs = Conversion.map_attributes_from_product_id(@product.volumeid)
      @porders = ProductionOrderline.joins("production_orderlines, production_orders").where("production_orderlines.product_id = '#{params[:productid]}' and production_orders.allow_production = 1").uniq.collect{|x|[x.id,x.production_order.name] if x.production_order}

      sql_filename = @product.nds_name.to_s.gsub(' ','%').gsub('.xx','')
      @cv_templates = CvtoolTemplate.where("name LIKE '#{sql_filename}%' and isactive = 1").order(:updated_at).collect {|c| [c.name,c.name]}
      render :layout => false
    end
  end

  def import_assembly
    assembly_id = params[:assembly_id]
    @refs = ''
    @datafiles = ''
    if !assembly_id.blank?
      @assembly_header = AssemblyHeader.find(assembly_id)
    else
      @assembly_header = AssemblyHeader.where("product_id = '#{params[:product_id]}' and output_format = '#{params[:to_format]}'").first
      @assembly_header = AssemblyHeader.where("product_id = '#{params[:product_id]}'").first unless @assembly_header
    end

    if @assembly_header
      data = @assembly_header.export_for_conversion
      @refs = data[0]
      @datafiles = data[1]
    end

    render :layout => false
  end

  def import_process_data
    if params[:process_data_id] && !params[:process_data_id].blank?
      @process_data = ProcessData.find(params[:process_data_id])
    end
    render :layout => false
  end

  def to_remove
    @conversion = Conversion.find(params[:id])
    begin
      Conversion.transaction do
        @conversion.set_status(Conversion::STATUS_REJECTED)
        @conversion.rejection_remark = "REMOVED (999) by " + current_user.full_name.to_s
        @conversion.save!

        if @conversion.production_orderline_id
          @production_orderline = ProductionOrderline.find(@conversion.production_orderline_id)
          @production_orderline.bp_name = "REQUEST CONVERSION"
          @production_orderline.save!
          os = Orderstatuslog.new
          os.production_order_id = @production_orderline.production_order_id
          os.completion_date = Time.now
          os.level = 'PRODUCT'
          os.user = current_user.user_name
          os.sequence = 50
          os.status = "REQUEST CONVERSION"
          os.remarks = "Conversion request removed."
          os.reference = @conversion.productid.to_s
          os.save!
        end

      end
    rescue => e
      flash[:error] = "Something went wrong during the removal.<br />#{e.message}"
    end
    flash[:notice] = "Conversion with id:#{@conversion.id.to_s} succesfully removed."
    respond_to do |format|
      format.html { redirect_to(:controller => "conversions" , :action => :index) }
      format.xml  { head :ok }
    end
  end

  def toggle_mark_for_removal
    conversion = Conversion.find(params[:id])
    if conversion.end_status? && !conversion.outcome_persistent_yn
      begin
        if conversion.conversion_databases.available.empty?
          conversion.unmark_for_removal(current_user)
          @flash_msg = ['success','Success','Outcome unmarked for removal.']
        else
          conversion.terminate(current_user)
          @flash_msg = ['success', 'Success', 'Outcome marked for removal.']
        end
      rescue => e
        puts e.message
        puts e.backtrace
        @flash_msg = ['error', '(Un)marking for removal failed',e.message.gsub("'",'')]
      end
    else
      if !conversion.end_status?
        @flash_msg = ['error', 'Marking for removal failed', 'Conversion is not in end status']
      else
        @flash_msg = ['error', 'Marking for removal failed', 'Conversion is marked as persistent.']
      end
    end
    @mark_for_removal_values = determine_mark_for_removal_values(conversion)
    unless params[:toggle]
      respond_to do |format|
        format.html { redirect_to(:back) }
        format.xml  { head :ok }
      end
    end
  end

  def remove_tag
    @conversion = Conversion.find(params[:id])
    current_user.tag(@conversion, :with => "", :on => :favorites)
    @favorite = false
    redirect_to :back, :notice => "Conversion request removed from your favorites."
  end

  def favorite
    @conversion = Conversion.find(params[:id])
    current_user.tag(@conversion, :with => "favorite", :on => :favorites)
    redirect_to :back, :notice => "Conversion request #{@conversion.id} added to your favorites."
  end

  def check_cvtool
    if params[:cvtool_id].to_i > 0
      @conversiontool = Conversiontool.find(params[:cvtool_id])
      if !@conversiontool.prod_released_yn || @conversiontool.remarks.to_s != ""
        render :partial => "cvtool_alert"
      else
        render :nothing => true
      end
    else
      render :nothing => true
    end
  end

  def reset_outcome
    @conversion = Conversion.find(params[:id])
    if @conversion.reset_result(current_user.full_name)
      redirect_to @conversion, :notice => "Outcome reset."
    else
      redirect_to :back, :error => "Outcome cannot be reset."
    end
  end

  def show_validation_errors
    @conversions = Conversion.joins('LEFT OUTER JOIN production_orderlines on production_orderline_id = production_orderlines.id
      LEFT OUTER JOIN production_orders on production_orderlines.production_order_id = production_orders.id
      INNER JOIN conversion_databases on conversion_databases.conversion_id = conversions.id').where(
      "conversions.status <= #{Conversion::STATUS_FINISHED} AND conversion_databases.status = #{ConversionDatabase::STATUS_HAS_ERRORS} AND production_orders.project_id = #{session[:project_id]}"
    ).order('conversions.id').uniq
    # find the outstanding ticket numbers for the validation errors
    tickets = []
    @conversions.each do |conv|
      conv.validation_errors.where(:resolved_yn => false).each do |err|
        tickets << err.ticket_nr if err.ticket_nr
      end
    end
    @ticket_hash = {}
    tickets.uniq.sort.each do |ticket_nr|
      valerror_hash = {}
      ValidationError.where(:ticket_nr => ticket_nr, :resolved_yn => false).order(:name).each do |validation_error|
        valerror_hash[validation_error.name] = [] unless valerror_hash.has_key?(validation_error.name)
        valerror_hash[validation_error.name] << validation_error.conversion_database
      end
      @ticket_hash[ticket_nr] = valerror_hash
    end
  end

  def handle_validation_errors
    conversion = Conversion.find(params[:handle_id])

    # single quotes in the error_name are returned as hex character codes. replace them
    error_name = params[:handle_error_name].gsub("&#x27;","'")
    ticket_nr = params[:handle_ticket_nr] unless params[:handle_ticket_nr].blank?
    selected_db_ids = params[:handle_selected_db_ids].split(',')

    conversion.handle_validation_errors(error_name, params[:handle_ok] == 'true', params[:accept_validation_error] == 'true', selected_db_ids, current_user, ticket_nr)

    redirect_to(show_validation_errors_conversions_path, :notice => "Databases with validation error '#{params[:handle_error_name]}' #{params[:handle_ok] == 'false' ? 'rejected' : (ticket_nr.blank? ? 'accepted' : 'open till further investigation')}.")
  end

  def close_validation_errors
    ticket_nr = params[:close_ticket_nr]
    conversion_jobs = []
    ValidationError.where("ticket_nr = '#{ticket_nr}'").each do |validation_error|
      conversion_jobs << validation_error.close_validation_error(params[:close_ok] == 'true', current_user)
    end

    unless params[:conversiontool_id].blank? || params[:cvtool_bundle_id].blank?
      # rebuild the databases marked for removal
      conversion_jobs.uniq.each do |cj|
        # the list of databases to remove can be nil as they are already marked above.
        cj.regenerate(current_user, params[:conversiontool_id], params[:cvtool_bundle_id])
      end
      ConversionJob.transaction do
        conversion_jobs.uniq.collect{|cj|cj.conversion}.uniq.each do |cv|
          # update conversion / order
          begin
            cv.monitor_conversion
          rescue IsActiveRuntimeError => e
            ol = cv.production_orderline
            ol.bp_message = e
            ol.save!
          end
        end
      end
    end

    redirect_to(show_validation_errors_conversions_path, :notice => "Databases belonging to ticket #{params[:close_ticket_nr]} #{params[:close_ok] == 'true' ? 'accepted' : (params[:close_converstool_id].blank? ? 'marked for removal' : 'rescheduled')}.")
  end

  def fill_marking_for_removal_modal_confirm
    @conversion = Conversion.find(params[:id])
  end

  def restart_failed_and_crashed_conversion_jobs
    @conversion = Conversion.find(params[:id])
    if !params[:restart_job_ids].blank?
      begin
        ConversionJob.transaction do
          params[:restart_job_ids].each do |jid|
            conversion_job = ConversionJob.find(jid)
            conversion_job.clear_working_dir
            new_conversion_job = conversion_job.regenerate(current_user, params[:conversiontool_id], params[:cvtool_bundle_id])
            new_conversion_job.queue
          end
        end
      rescue => e
        @mesg = e.message
      end
    end
  end

  def toggle_outcome_persistence
    @conversion = Conversion.find(params[:id])

    begin
      @conversion.toggle_outcome_persistence
      @flash_msg = ["success","Success","Outcome persistence is set to #{@conversion.outcome_persistent_yn}."]
    rescue => e
      @flash_msg = ["error","Toggle failed",e.message]
    end
  end

  def patch_requests_report
    @years_for_select = []
    years = Date.current.year + 1
    5.times do
      years = (years - 1)
      @years_for_select << years.to_s
    end
    @conversions_hash = Conversion.patch_request_hash_by_year(Date.current.year)
    @totals_hash = Conversion.patch_request_totals_hash_by_year_cvtool_bundle(Date.current.year)
  end

  def change_year_patch_requests_report
    year = params[:year]
    if year.blank?
      year = Date.current.year
    end
    @conversions_hash = Conversion.patch_request_hash_by_year(year.to_i)
    @totals_hash = Conversion.patch_request_totals_hash_by_year_cvtool_bundle(year.to_i)
  end

  def conversions_for_select
    # Infinity scroll for select2
    conditions = '1=1'
    conditions_string_ary = ["(1=1)"]
    conditions_param_values = []

    unless params[:q].blank?
      params[:q].strip.split('+').each do |q|
        conditions_string_ary << "(conversions.id LIKE ?
                                 OR conversions.productid LIKE ?
                                 OR conversions.input_format LIKE ?
                                 OR conversions.output_format LIKE ?
                                 OR data_releases.name LIKE ?
                                 OR regions.name LIKE ?
                                 OR company_abbrs.company_name LIKE ?
                                 OR company_abbrs.ms_abbr_3 LIKE ?)"
        8.times do
          conditions_param_values << "\%#{q}\%"
        end
      end
    end
    conditions = conditions_string_ary.join(" AND ")
    @conversions = Conversion
    .joins({:data_release => :company_abbr},:region)
    .order("conversions.id DESC").where([conditions] + conditions_param_values)
    .select('conversions.id', 'conversions.input_format', 'conversions.output_format', 'company_abbrs.ms_abbr_3', 'data_releases.name as datarelease', 'regions.name as regionname,conversions.productid')
    .paginate(:per_page => params[:page_limit], :page => params[:page])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: "{\"total\":#{@conversions.total_entries}, \"results\":#{@conversions.to_json}}"}
    end
  end

  def fill_cloned_attributes
    @conversion_clone = Conversion.find(params[:clone_id])
  end

  private

  def init_new(errors=nil)
    # Depricated, but kept for bug analyzing
    # Can be removed after a while (07/2015)
    @conversion = @conversion || Conversion.new
    @conversion.errors[:base] << errors if errors
    @database = @database || []
    @cu = current_user.first_name + " " + current_user.last_name

    @conversion_request_by = @cu
    @conversion.ca_brn = "n.a."
    @conversion.request_date = Time.now

    if params[:production_orderline_id]
      @conversion.production_orderline_id = params[:production_orderline_id]
      @conversion.productid =  ProductionOrderline.find(params[:production_orderline_id]).product_id.to_s
      @prev_conversion = ProductionOrderline.find(params[:production_orderline_id]).conversions.order('id desc').find_all{|c| c.valid?}
    end

    @assembly_option_list = AssemblyHeader.test.collect{|a|[a.name,a.id]}

    @pass_params = params
    @pass_params.delete :action
    @pass_params.delete :controller
  end

  def init_edit(errors=nil)
    # Depricated, but kept for bug analyzing
    # Can be removed after a while (07/2015)
    @conversion = @conversion || Conversion.find(params[:id])
    @conversion.errors[:base] << errors if errors
    @database = ConversionDatabase.present.where(:conversion_id => params[:id])
    @cu = current_user.first_name + " " + current_user.last_name

    @results = @database.collect {|x|x.path_and_file if !x.path_and_file.nil?}.join("\n").gsub("\n\n","\n")

    @conversion.request_by = current_user.full_name if @conversion.request_by.to_s == ""
    @conversion.size_bytes = @database.last.size_bytes if @database && @database.last
    @outputformat = @conversion.output_format if @conversion.output_format #dHive RDB etc

    # determine assembly option_list
    @assembly_option_list = []
    begin
      # check whether the conversion assembly is in the test scope. If not an exception will be thrown, so instead of an if, we use a rescue
      AssemblyHeader.test.find(@conversion.assembly_id)
    rescue
      @assembly_option_list << [@conversion.assembly.name, @conversion.assembly.id] if @conversion.assembly
    end
    @assembly_option_list += AssemblyHeader.test.collect{|a|[a.name,a.id]}

    if @conversion.reception_reference && @conversion.reception_reference.size > 0
      r = Reception.find(@conversion.reception_reference)
      if r
        @conversion.reception_reference = r.id
        if @results.empty? && @conversion.reception_reference.to_s != ""
          @results = Reception.find(@conversion.reception_reference).storage_location.to_s
        end
      end
    end
    @pass_params = params
    @pass_params.delete :action
    @pass_params.delete :controller
  end

  def determine_mark_for_removal_values(conversion)
    result = {}
    if conversion.end_status? && !conversion.outcome_persistent_yn && conversion.conversion_databases.available.empty? && conversion.conversion_databases.marked_for_removal.size > 0
      result[:button] = 'Unmark outcome for removal'
      result[:label] = 'Unmark outcome for removal'
      result[:question] = 'Are you sure that the outcome of this conversion should no longer be marked for removal?'
      result[:enabled] = true
    else
      result[:button] = 'Mark outcome for removal'
      if conversion.end_status? && !conversion.outcome_persistent_yn && conversion.conversion_databases.available.size > 0 && conversion.product_locations.size > 0
        result[:label] = 'Product registration found'
        result[:question] = 'You are marking outcome which is registered as released product, <strong>are you sure?</strong>'
        result[:enabled] = true
      else
        result[:label] = 'Mark outcome for removal'
        result[:question] = 'Are you sure that the outcome of this conversion can be removed?'
        result[:enabled] = conversion.status == Conversion::STATUS_FINISHED && !conversion.outcome_persistent_yn && conversion.conversion_databases.available.size > 0
      end
    end
    result
  end

  def sort_column
    if !params[:sort].blank?
      params[:sort]
    else
      "conversions.updated_at"
    end
  end

  def sort_direction
    if !params[:direction].blank?
      params[:direction]
    else
      "desc"
    end
  end

  def search
    params[:search] || ""
  end

end
