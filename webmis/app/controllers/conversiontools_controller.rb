class ConversiontoolsController < ApplicationController
  before_filter :authorize
  authorize_resource
 
#
  # GET /conversiontools
  # GET /conversiontools.xml
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search
  
  def index
    @include_inactive = false
    if params[:reset]
      params.delete :search
      params.delete :reset

      params.delete :sort
      params.delete :direction
      params[:inclinactive] = '0'
      @include_inactive = false
    end
    conditions = 'conversiontools.isactive != 0 '
    conditions_string_ary = []
    conditions_param_values = []

    if params[:inclinactive] == '1'
      @include_inactive = true
      conditions = '1=1 '
    end

    unless params[:search].blank?
      conditions_string_ary << "AND (conversiontools.code LIKE ?)"
      1.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
    end
    conditions = conditions + conditions_string_ary.join(" ")

    @conversiontools = Conversiontool.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])

    if request.xhr?
      render(:partial => "conversiontool", :collection => @conversiontools)
    end
  end

  # GET /conversiontools/1
  # GET /conversiontools/1.xml
  def show
    @conversiontool = Conversiontool.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @conversiontool }
    end
  end

  # GET /conversiontools/new
  # GET /conversiontools/new.xml
  def new
    @conversiontool = Conversiontool.new
    @conversiontool.isactive = true

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @conversiontool }
    end
  end

  # GET /conversiontools/1/edit
  def edit
    @conversiontool = Conversiontool.find(params[:id])
    @region = Region.first
    begin
	@a = Conversiontool.where("id in (select MIN(id) from conversiontools where id > "+@conversiontool .id.to_s+")").first
	if @a
		@next = @a.id
		@result_next = true
	end
    rescue => error
	@result_next = false
    end

    begin
	@b = Conversiontool.where("id in (select MAX(id) from conversiontools where id < "+@conversiontool .id.to_s+")").first
	if @b
		@previous = @b.id
		@result_previous = true
	end
    rescue => error
	@result_previous = false
    end
  end

  # POST /conversiontools
  # POST /conversiontools.xml
  def create
    @conversiontool = Conversiontool.new(strong_params)
    if params[:conversiontool][:prod_released_yn] == ""
       Conversiontool.prod_released_yn = false
    end
    @conversiontool.tool_type = params[:tool_type]

    respond_to do |format|
      if @conversiontool.save
        flash[:notice] = 'Conversiontool was successfully created.'
        format.html { redirect_to(@conversiontool) }
        format.xml  { render :xml => @conversiontool, :status => :created, :location => @conversiontool }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @conversiontool.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /conversiontools/1
  # PUT /conversiontools/1.xml
  def update
    @conversiontool = Conversiontool.find(params[:id])
    respond_to do |format|
      if @conversiontool.update_attributes(strong_params)
        flash[:notice] = 'Conversiontool was successfully updated.'
        format.html { redirect_to(@conversiontool) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @conversiontool.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /conversiontools/1
  # DELETE /conversiontools/1.xml
  def destroy
    @conversiontool = Conversiontool.find(params[:id])
    @conversiontool.destroy

    respond_to do |format|
      format.html { redirect_to(conversiontools_url) }
      format.xml  { head :ok }
    end
  end

  def toggle_isactive
    @conversiontool = Conversiontool.find(params[:id])
    if @conversiontool.isactive
      @conversiontool.isactive = false
      flash[:notice] = 'Conversiontool deactivated.'
    else
      @conversiontool.isactive = true
      flash[:notice] = 'Conversiontool activated.'
    end
    @conversiontool.save

    respond_to do |format|
      format.html { redirect_to(:back) }
      format.xml  { head :ok }
    end
  end

  def sync
    Conversiontool.sync_with_filesystem(true,false)  # (0)true: set conversiontools found on the filesystem to active (1)false: do not import toolspec
    params.delete("_method")  # we don't want to pass around the action and 'post' method, just the regular parameters
    params.delete("action")
    respond_to do |format|
      format.html { redirect_to conversiontools_url(params), :notice => "Active conversion tools synchronized with the file system." }
      format.xml  { head :ok }
    end
  end 
  
  private  
  def sort_column  
    params[:sort] || "conversiontools.id"  
  end  
    
  def sort_direction  
    params[:direction] || "desc"  
  end
  
  def search
    params[:search] || ""
  end

end
