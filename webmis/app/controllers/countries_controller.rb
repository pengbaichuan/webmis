class CountriesController < ApplicationController

  before_filter :authorize
  authorize_resource
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search  

  # index not used as this is handled by the regions controller
  def index
    if params[:reset]
      params.delete :q
      params.delete :reset
      
      params[:sort] = 'name'
      params[:direction] = 'asc'
    end
    conditions = "1=1 "
    conditions_string_ary = []
    conditions_param_values = []
    
    unless params[:q].blank?
      conditions_string_ary << " and (regions.name LIKE ? OR country_iso_code LIKE ? or country_iso3_code LIKE ? OR area_code LIKE ?)"
      4.times do
        conditions_param_values << "\%#{params[:q].strip}\%"
      end
    end
    conditions = conditions + conditions_string_ary.join(" ")
    @countries = Country.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 75, :page => params[:page])
    
    if request.xhr?
      flash.keep[:notice]
      render(:partial => "country", :collection => @countries)
    end 
    
  end

  def show
    @country = Country.find(params[:id])
  end

  def new
    @country = Country.new

  end

  def edit
    @country = Country.find(params[:id])
  end

  def create
    @country = Country.new(strong_params)
    @country.type = 'Country'
    @country.display_type_id = AreaType.where("name like 'Country'").first.id
    
    respond_to do |format|
      if @country.save
        format.html { redirect_to @country, :notice => "Country succesfully added."	 }
        format.json { render json: @country, status: :created, location: @country }
      else
        format.html { render action: "new" }
        format.json { render json: @country.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @country = Country.find(params[:id])
    @country.app_module_ids = [AppModule.where(:abbr => 'MIS').last.id]
    respond_to do |format|
	    if @country.update_attributes(strong_params)
        format.html { redirect_to @country, :notice => "Country succesfully added."  }
        format.json { render json: @country, status: :created, location: @country }
      else
        format.html { render action: "edit" }
        format.json { render json: @country.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @country = Country.find(params[:id])
    if @country.destroy
	flash[:notice] =  "Country succesfully deleted!"    	
	redirect_to_index
    else
	flash[:error] = "Database error! Please contact an administrator!"
    end
  end

  def redirect_to_index
     if params[:q]
	redirect_to :action => :index, :q => params[:q]
     else
        redirect_to :action => :index, :controller => :regions
     end
  end

  def generate_dakota_id
    render :text => Region.generate_dakota_id
  end
  
  private  
  def sort_column  
    params[:sort] || "name"  
  end  
    
  def sort_direction  
    params[:direction] || "asc"  
  end
  
  def search
    params[:search] || ""
  end
  
end
