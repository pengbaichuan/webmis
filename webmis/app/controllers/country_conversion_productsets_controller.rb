class CountryConversionProductsetsController < ApplicationController
  # TODO Can be removed
  # GET /country_conversion_productsets
  # GET /country_conversion_productsets.xml
  layout "general"
  
  def index
    @country_conversion_productsets = CountryConversionProductset.all
    @country_conversion_productsets_grid = initialize_grid(CountryConversionProductset,
       :enable_export_to_csv => true,
       :csv_file_name => 'country_conversions')
    
    export_grid_if_requested ||    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @country_conversion_productsets }
    end
    
  end

  # GET /country_conversion_productsets/1
  # GET /country_conversion_productsets/1.xml
  def show
    @country_conversion_productset = CountryConversionProductset.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @country_conversion_productset }
    end
  end

  # GET /country_conversion_productsets/new
  # GET /country_conversion_productsets/new.xml
  def new
    @country_conversion_productset = CountryConversionProductset.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @country_conversion_productset }
    end
  end

  # GET /country_conversion_productsets/1/edit
  def edit
    @country_conversion_productset = CountryConversionProductset.find(params[:id])
  end

  # POST /country_conversion_productsets
  # POST /country_conversion_productsets.xml
  def create
    @country_conversion_productset = CountryConversionProductset.new(strong_params)

    respond_to do |format|
      if @country_conversion_productset.save
        flash[:notice] = 'CountryConversionProductset was successfully created.'
        format.html { redirect_to(@country_conversion_productset) }
        format.xml  { render :xml => @country_conversion_productset, :status => :created, :location => @country_conversion_productset }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @country_conversion_productset.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /country_conversion_productsets/1
  # PUT /country_conversion_productsets/1.xml
  def update
    @country_conversion_productset = CountryConversionProductset.find(params[:id])

    respond_to do |format|
      if @country_conversion_productset.update_attributes(strong_params)
        flash[:notice] = 'CountryConversionProductset was successfully updated.'
        format.html { redirect_to(@country_conversion_productset) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @country_conversion_productset.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /country_conversion_productsets/1
  # DELETE /country_conversion_productsets/1.xml
  def destroy
    @country_conversion_productset = CountryConversionProductset.find(params[:id])
    @country_conversion_productset.destroy

    respond_to do |format|
      format.html { redirect_to(country_conversion_productsets_url) }
      format.xml  { head :ok }
    end
  end
end
