class CustomerContentDefinitionsController < ApplicationController
  before_filter :authorize
  authorize_resource

  layout "webmis"
  helper_method :sort_column, :sort_direction, :search
  
  # GET /customer_content_definitions
  # GET /customer_content_definitions.json
  def index
    conditions =  '1=1'
    conditions_param_values = []
    
    if params[:reset]
      params.delete :search
      params.delete :reset
      params.delete :search_product_line

      params[:sort] = 'label'
      params[:direction] = 'asc'
    end
    
    product_line_id = params[:search_product_line]
    unless product_line_id.blank?
      conditions += " and (customer_content_definitions.product_line_id = ?)"
      conditions_param_values << product_line_id
    end

    if params[:search] && params[:search] != ""
      conditions += " and (customer_content_definitions.label LIKE ? OR 
                            ('#{CustomerContentDefinition.outputarray[CustomerContentDefinition::OUTPUT_TARGET_CONVERSION_DB]}' LIKE ? AND
                                customer_content_definitions.output_target = #{CustomerContentDefinition::OUTPUT_TARGET_CONVERSION_DB}) OR                     
                                ('#{CustomerContentDefinition.outputarray[CustomerContentDefinition::OUTPUT_TARGET_RELEASE_NOTES]}' LIKE ? AND
                                    customer_content_definitions.output_target = #{CustomerContentDefinition::OUTPUT_TARGET_RELEASE_NOTES}) OR
                                  ('#{CustomerContentDefinition.outputarray[CustomerContentDefinition::OUTPUT_TARGET_XML]}' LIKE ? AND
                                      customer_content_definitions.output_target = #{CustomerContentDefinition::OUTPUT_TARGET_XML}) OR
                                    (customer_content_specifications.name LIKE ?) OR
                                      (product_lines.name LIKE ?)
                          )"
      6.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
    end
    
    @customer_content_definitions = CustomerContentDefinition.joins('LEFT OUTER JOIN product_lines ON product_lines.id = customer_content_definitions.product_line_id ' +
                                                                    'LEFT OUTER JOIN customer_content_specifications ON customer_content_specifications.id = customer_content_definitions.customer_content_specification_id'
                                                                    ).where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(per_page: 100, page: params[:page])
    
    if request.xhr?
        flash.keep[:notice]
        render(partial: "customer_content_definition", collection: @customer_content_definitions)
      else
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @customer_content_definitions }
      end
    end

  end

  # GET /customer_content_definitions/1
  # GET /customer_content_definitions/1.json
  def show
    @product_line_id = params[:product_line_id] # used to see whether the method was called from the product_line show view
    @customer_content_definition = CustomerContentDefinition.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @customer_content_definition }
    end
  end

  # GET /customer_content_definitions/new
  # GET /customer_content_definitions/new.json
  def new
    @product_line_id = params[:product_line_id] # used to see whether the method was called from the product_line show view
    @customer_content_definition = CustomerContentDefinition.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @customer_content_definition }
      format.js {render :nothing => true}
    end
  end

  # GET /customer_content_definitions/1/edit
  def edit
    @product_line_id = params[:product_line_id] # used to see whether the method was called from the product_line show view
    @customer_content_definition = CustomerContentDefinition.find(params[:id])
  end

  # POST /customer_content_definitions
  # POST /customer_content_definitions.json
  def create
    @product_line_id = params[:product_line_id] unless params[:product_line_id].blank?
    @customer_content_definition = CustomerContentDefinition.new(strong_params)
    respond_to do |format|
      if @customer_content_definition.save
        flash[:notice] = 'Customer content definition was successfully created.'
        format.html { if @product_line_id 
                        redirect_to({controller: :product_lines, action: :show, id: @product_line_id})
                      else
                        redirect_to @customer_content_definition
                      end
                    }
        format.json { render json: @customer_content_definition, status: :created, location: @customer_content_definition }
      else
        flash[:alert] = 'Customer content definition could not be saved.'
        format.html { render action: "new" }
        format.json { render json: @customer_content_definition.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /customer_content_definitions/1
  # PUT /customer_content_definitions/1.json
  def update
    @product_line_id = params[:product_line_id] unless params[:product_line_id].blank? # used to see whether the method was called from the product_line show view
    @customer_content_definition = CustomerContentDefinition.find(params[:id])

    respond_to do |format|
      if @customer_content_definition.update_attributes(strong_params)
        flash[:notice] = 'Customer content definition was successfully updated.'
        format.html { if @product_line_id
                        redirect_to({controller: :product_lines, action: :show, id: @product_line_id})
                      else
                        redirect_to @customer_content_definition
                      end
                    }
        format.json { head :no_content }
      else
        flash[:alert] = "Customer content definition could not be saved."
        format.html { render action: "edit" }
        format.json { render json: @customer_content_definition.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /customer_content_definitions/1
  # DELETE /customer_content_definitions/1.json
  # DELETE /customer_content_definitions/1.xml
  def destroy
    @product_line_id = params[:product_line_id] # used to see whether the method was called from the product_line show view
    @customer_content_definition = CustomerContentDefinition.find(params[:id])
    @customer_content_definition.destroy

    respond_to do |format|
      format.html { if @product_line_id 
                       redirect_to({controller: :product_lines, action: :show, id: @product_line_id})
                    else
                       redirect_to @customer_content_definitions_url
                   end
                  }
      format.json { head :no_content }
      format.xml  { head :ok }
    end
  end

  private  
  def sort_column  
    params[:sort] || "label"  
  end  
    
  def sort_direction  
    params[:direction] || "asc"  
  end
  
  def search
    params[:search] || ""
  end 
end
