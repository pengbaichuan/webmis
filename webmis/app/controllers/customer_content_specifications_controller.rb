class CustomerContentSpecificationsController < ApplicationController
  before_filter :authorize
  authorize_resource

  layout "webmis"
  helper_method :sort_column, :sort_direction, :search, :search_active
  
  # GET /customer_content_specifications
  # GET /customer_content_specifications.json
  def index
    conditions =  '1=1'

    if params[:reset]
      params.delete :search
      params.delete :search_status
      params.delete :obsolete
      params.delete :reset

      params[:search_active] = 1
      params[:sort] = 'name'
      params[:direction] = 'asc'
    end

    conditions_string_ary = []
    conditions_param_values = []
    
    include_obsolete = params[:obsolete]

    status = params[:search_status]
    if status.blank?
      conditions_string_ary << ' and (status is null or status !="obsolete")' unless include_obsolete
    else
      conditions_string_ary << ' and (status=?)'
      conditions_param_values << status
      include_obsolete ||= (status == 'obsolete')
    end

    # set default to 1 for the initial index screen
    active = params[:search_active] ? params[:search_active] : 1
    unless active.blank?
      conditions_string_ary << ' and (is_active = ?)'
      conditions_param_values << active
    end

    params[:obsolete] = include_obsolete
    conditions = conditions + conditions_string_ary.join(" ")

    if (sort_column == 'latest_approved')
      @customer_content_specifications = CustomerContentSpecification.where([conditions]+conditions_param_values).sort_by{ |r| r.latest_approved ? r.latest_approved.version : 0 }
      @customer_content_specifications.reverse! if sort_direction == 'desc'
    else
      @customer_content_specifications = CustomerContentSpecification.where([conditions]+conditions_param_values).order(sort_column + ' ' + sort_direction)
    end

    unless params[:search].blank?
      search = params[:search].strip
      matched_customer_content_specifications = []
      @customer_content_specifications.each do |customer_content_specification|
        if ( (customer_content_specification.name && customer_content_specification.name.include?(search) ) ||
             (customer_content_specification.type_string.include?(search) )
           )
          matched_customer_content_specifications << customer_content_specification
        end
      end
      @customer_content_specifications = matched_customer_content_specifications
    end
    
    @customer_content_specifications = @customer_content_specifications.paginate(per_page: 100, page: params[:page])
    
    if request.xhr?
        flash.keep[:notice]
        render(partial: "customer_content_specification", collection: @customer_content_specifications)
      else
      respond_to do |format|
        if params[:search] || params[:search_status]
          flash.now[:notice] = @customer_content_specifications.total_entries.to_s + " records found."
        end
        format.html # index.html.erb
        format.json { render json: @customer_content_specifications }
      end
    end
  end

  # GET /customer_content_specifications/1
  # GET /customer_content_specifications/1.json
  def show
   @customer_content_specification = CustomerContentSpecification.find(params[:id])
   show_customer_content_specification
  end

  def approved
    spec = CustomerContentSpecification.find(params[:id])
    if spec_version = spec.approved
      @customer_content_specification = spec.use_version(spec_version.version)
    end
    show_customer_content_specification
  end

  def handle
    @customer_content_specification = CustomerContentSpecification.find(params[:id])
    @product_line_id = params[:product_line_id] # used to see whether the method was called from the product_line show view
    @current_user = current_user
  end

  private
  def show_customer_content_specification
    @product_line_id = params[:product_line_id] # used to see whether the method was called from the product_line show view
    respond_to do |format|
      format.html { render :action => 'show' }
      format.json { render :json => @customer_content_specification }
    end
  end
  public

  # # GET /customer_content_specifications/new
  # # GET /customer_content_specifications/new.json
  # def new
  #   @product_line_id = params[:product_line_id] # used to see whether the method was called from the product_line show view
  #   @customer_content_specification = CustomerContentSpecification.new
  #   @customer_content_specification.is_active = true

  #   respond_to do |format|
  #     format.html # new.html.erb
  #     format.json { render json: @customer_content_specification }
  #   end
  # end

  # GET /customer_content_specifications/1/edit
  def edit
    @product_line_id = params[:product_line_id] # used to see whether the method was called from the product_line show view
    @customer_content_specification = CustomerContentSpecification.find(params[:id])
    # the default new status of an edit should always be ':draft'
    @customer_content_specification.status = :draft
  end  

  # POST /customer_content_specifications
  # POST /customer_content_specifications.json
  def create
    # for new customer_content_specifications, first create one with status new and then edit it as a regular customer_content_specification
    # note that we skip the traditional 'new' method here
    @product_line_id = params[:product_line_id] unless params[:product_line_id].blank? # used to see whether the method was called from the product_line show view
    @customer_content_specification = CustomerContentSpecification.new(:status => 'new', :comment => 'manual initial version', :updated_by => session[:username])

    respond_to do |format|
      if @customer_content_specification.save(:validate => false)
        format.html { redirect_to edit_customer_content_specification_path(:id => @customer_content_specification.id) }
        format.json { render :json => @customer_content_specification, :status => :created, location: @customer_content_specification }
      else
        format.html { redirect_to customer_content_specifications_url, :alert => 'Could not create new customer_content_specification.' }
        format.json { render :json => @customer_content_specification.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /customer_content_specifications/1
  # PUT /customer_content_specifications/1.json
  def update
    @product_line_id = params[:product_line_id] unless params[:product_line_id].blank? # used to see whether the method was called from the product_line show view
    @customer_content_specification = CustomerContentSpecification.find(params[:id])
    params[:customer_content_specification][:generation_code] = nil if params[:customer_content_specification][:generation_code].blank?

    respond_to do |format|
      if @customer_content_specification.update_attributes(strong_params.update(:status => 'draft', :updated_by => session[:username]))
        format.html { redirect_to handle_customer_content_specification_url, :id => @customer_content_specification.id, :product_line_id => @product_line_id,
                                   :notice => 'Customer content specification was successfully updated.' }
        format.json { head :no_content }
      else
        flash[:alert] = 'Customer content specification could not be saved.'
        format.html { render :action => "edit" }
        format.json { render :json => @customer_content_specification.errors, :status => :unprocessable_entity }
      end
    end
  end


  # DELETE /customer_content_specifications/1
  # DELETE /customer_content_specifications/1.json
  def destroy
    @product_line_id = params[:product_line_id] # used to see whether the method was called from the product_line show view
    @customer_content_specification = CustomerContentSpecification.find(params[:id])
    @customer_content_specification.destroy

    respond_to do |format|
      format.html { if @product_line_id
                      redirect_to product_line_url, :id => @product_line_id
                    else
                      redirect_to customer_content_specifications_url 
                    end
                  }
      format.json { head :no_content }
    end
  end

  # POST /customer_content_specifications/draft
  def draft
    change_status :draft, "'draft' action"
  end

  # POST /customer_content_specifications/review
  def review
    change_status :review, "'review' action"
  end

  # POST /customer_content_specifications/approve
  def approve
    change_status :approved, "'approve' action"
  end

  # POST /customer_content_specifications/approve_directly
  def approve_directly
    change_status :approved, "'approve' action"
  end

  # POST /customer_content_specifications/obsolete
  def obsolete
    change_status :obsolete, "'obsolete' action"
  end

  # POST /customer_content_specifications/revert
  def revert
    @product_line_id = params[:product_line_id] # used to see whether the method was called from the product_line show view
    customer_content_specification = CustomerContentSpecification.find(params[:id])
    customer_content_specification.use_version(customer_content_specification.latest_approved.version)
    # skip the validation as the status transition does not have to be a valid one in this case
    customer_content_specification.save(:validate => false)

    # the above will increase the version number by one, so revert it
    # note that just adapting the version number will not result in a new version of the customer_content_specification
    customer_content_specification.version = customer_content_specification_original.version
    customer_content_specification.save

    # destroy all version which came after the version we reverted to
    CustomerContentSpecificationVersion.where("customer_content_specification_id = :id and version > :version",
                                              {:id => params[:id], :version => customer_content_specification_original.version}).destroy_all

    notice_str = 'Customer content specification successfully reverted to #{customer_content_specification.version}.'    
    respond_to do |format|
      format.html { if @product_line_id
                      redirect_to product_line_url, :id => @product_line_id, :notice => notice_str
                    else
                      redirect_to customer_content_specifications_url, :notice => notice_str
                    end
                  }
    end
  end

  def customer_content_values
     @customer_content_specification = CustomerContentSpecification.find(params[:id])
     if request.xhr?
        render(:partial => "customer_content_value", :collection => @customer_content_specification.customer_content_values)
      else
      respond_to do |format|
        render :nothing => true
      end
    end
  end

  private  
  def change_status(status, comment)
    @product_line_id = params[:product_line_id] # used to see whether the method was called from the product_line show view
    @customer_content_specification = CustomerContentSpecification.find(params[:id])
    @customer_content_specification.status = status
    @customer_content_specification.comment = comment
    @customer_content_specification.updated_by = session[:username]
    if @customer_content_specification.save
      notice_str = "Status was successfull changed to #{status}."
      if @product_line_id
        redirect_to product_line_url, :id => @product_line_id, :notice => notice_str
      else
        redirect_to customer_content_specifications_url, :notice => notice_str
      end
    else
      alert_str = "Status change to '#{status}' failed."
      if @product_line_id
        redirect_to product_line_url, :id => @product_line_id, :alert => alert_str
      else
        redirect_to customer_content_specifications_url, :alert => alert_str
      end
    end
  end

  def sort_column  
    params[:sort] || "name"  
  end  
    
  def sort_direction  
    params[:direction] || "asc"  
  end
  
  def search
    params[:search] || ""
  end 
  def search_active
    params[:search_active] || 1
  end

end
