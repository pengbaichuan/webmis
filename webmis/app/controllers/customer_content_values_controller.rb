class CustomerContentValuesController < ApplicationController
  before_filter :authorize
  authorize_resource

  layout "webmis"
  helper_method :sort_column, :sort_direction, :search
  
  # GET /customer_content_values
  # GET /customer_content_values.json
  def index
    conditions =  '1=1'
    conditions_param_values = []
    
    if params[:reset]
      params.delete :search
      params.delete :reset
      params.delete :search_specification

      params[:sort] = 'id'
      params[:direction] = 'asc'
    end
    
    customer_content_specification_id = params[:search_specification]
    unless customer_content_specification_id.blank?
      conditions += " and (customer_content_values.customer_content_specification_id = ?)"
      conditions_param_values << customer_content_specification_id
    end

    if params[:search] && params[:search] != ""
      conditions += " and (customer_content_specifications.name LIKE ? OR 
                             customer_content_values.customer_version LIKE ? OR
                               customer_content_values.value LIKE ?
                          )"
      3.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
    end
    
    @customer_content_values = CustomerContentValue.joins('LEFT OUTER JOIN customer_content_specifications ON customer_content_specifications.id = customer_content_values.customer_content_specification_id'
                                              ).where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(per_page: 100, page: params[:page])
    
    if request.xhr?
        flash.keep[:notice]
        render(partial: "customer_content_value", collection: @customer_content_values)
      else
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @customer_content_values }
      end
    end

  end

  # GET /customer_content_values/1
  # GET /customer_content_values/1.json
  def show
    @specification_id = params[:specification_id]   # used to see whether the method was called from the customer content specification show view
    @product_line_id = params[:product_line_id]   # used to see whether the customer content specification show view was called from the product_line show view
    @customer_content_value = CustomerContentValue.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @customer_content_value }
    end
  end

  # GET /customer_content_values/new
  # GET /customer_content_values/new.json
  def new
    @specification_id = params[:specification_id]   # used to see whether the method was called form the customer content specification show view 
    @product_line_id = params[:product_line_id]   # used to see whether the customer content specification show view was called from the product_line show view
    @customer_content_value = CustomerContentValue.new
    @customer_content_value.customer_content_specification = CustomerContentSpecification.find(@specification_id) if @specification_id
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @customer_content_value }
    end
  end

  # GET /customer_content_values/1/edit
  def edit
    @specification_id = params[:specification_id]   # used to see whether the method was called from the customer content specification show view
    @product_line_id = params[:product_line_id]   # used to see whether the customer content specification show view was called from the product_line show view
    @customer_content_value = CustomerContentValue.find(params[:id])
  end

  # POST /customer_content_values
  # POST /customer_content_values.json
  def create
    @specification_id = params[:specification_id] unless params[:specification_id].blank?
    @product_line_id = params[:product_line_id] unless params[:product_line_id].blank?
    params[:customer_content_value][:customer_content_specification_id] = @specification_id if @specification_id    # if created from a customer content specification, take that specification
    @customer_content_value = CustomerContentValue.new(strong_params)

    respond_to do |format|
      if @customer_content_value.save
        flash[:notice] = 'Customer content was successfully created.'
        format.html { redirect_to({ action: :show, id:@customer_content_value.id , specification_id: @specification_id, product_line_id: @product_line_id }) }
        format.json { render json: @customer_content_value, status: :created, location: @customer_content_value }
      else
        flash[:alert] = 'Customer content value could not be saved.'
        format.html { render action: "new" }
        format.json { render json: @customer_content_value.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /customer_content_values/1
  # PUT /customer_content_values/1.json
  def update
    @specification_id = params[:specification_id] unless params[:specification_id].blank?   # used to see whether the method was called from the product_line show view
    @product_line_id = params[:product_line_id] unless params[:product_line_id].blank?  # used to see whether the customer content specification show view was called from the product_line show view
    @customer_content_value = CustomerContentValue.find(params[:id])

    respond_to do |format|
      if @customer_content_value.update_attributes(strong_params)
        flash[:notice] = 'Customer content value was successfully updated.'
        format.html { redirect_to({action: :show, id:@customer_content_value.id , specification_id: @specification_id, product_line_id: @product_line_id }) }
        format.json { head :no_content }
      else
        flash[:alert] = "Customer content value could not be saved."
        format.html { render action: "edit" }
        format.json { render json: @customer_content_value.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /customer_content_values/1
  # DELETE /customer_content_values/1.json
  # DELETE /customer_content_values/1.xml
  def destroy
    @specification_id = params[:specification_id]   # used to see whether the method was called from the product_line show view
    @product_line_id = params[:product_line_id]  # used to see whether the customer content specification show view was called from the product_line show view
    @customer_content_value = CustomerContentValue.find(params[:id])
    @customer_content_value.destroy

    respond_to do |format|
      format.html { if @specification_id 
                       redirect_to({controller: :customer_content_specifications, action: :show, id: @specification_id, product_line_id: @product_line_id})
                    else
                       redirect_to @customer_content_values_url
                   end
                  }
      format.json { head :no_content }
      format.xml  { head :ok }
    end
  end

  def render_content
    @customer_content_value = CustomerContentValue.find(params[:id])
    respond_to do |format|
      format.json { render json: @customer_content_value.value, status: :created}
      format.xml { render xml: JSON.parse(@customer_content_value.value).to_xml, status: :created }
    end
  end

  private  
  def sort_column  
    params[:sort] || "id"  
  end  
    
  def sort_direction  
    params[:direction] || "asc"  
  end
  
  def search
    params[:search] || ""
  end 
end
