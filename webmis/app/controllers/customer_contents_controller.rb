class CustomerContentsController < ApplicationController
  before_filter :authorize
  authorize_resource

  layout "webmis"
  helper_method :sort_column, :sort_direction, :search
  
  # GET /customer_contents
  # GET /customer_contents.json
  def index
    @production_orderline_id = params[:production_orderline_id] unless params[:production_orderline_id].to_s.blank?
    @product_id = params[:product_id] unless params[:product_id].to_s.blank?
    conditions =  '1=1'
    conditions_param_values = []

    if @production_orderline_id
      conditions += " and customer_contents.production_orderline_id = ? "
      conditions_param_values << @production_orderline_id
    end

    if @product_id
      conditions += " and customer_contents.product_id = ? "
      conditions_param_values << @product_id
    end

    if params[:reset]
      params.delete :search
      params.delete :reset

      params[:sort] = 'id'
      params[:direction] = 'asc'
    end
    
    if params[:search] && params[:search] != ""
      conditions += " and (customer_contents.value LIKE ? OR 
                             customer_content_specifications.name LIKE ? OR
                               customer_contents.product_id LIKE ?
                          )"
      3.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
    end


    @customer_contents = CustomerContent.joins('INNER JOIN customer_content_specifications ON customer_content_specifications.id = customer_contents.customer_content_specification_id '
                                              ).where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(per_page: 100, page: params[:page])
    
    if request.xhr?
        flash.keep[:notice]
        render(partial: "customer_content", collection: @customer_contents)
      else
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @customer_contents }
      end
    end

  end

  # GET /customer_contents/1
  # GET /customer_contents/1.json
  def show
    @production_orderline_id = params[:production_orderline_id]   # used to see whether the method was called from the production orderline show view
    @product_id = params[:product_id]   # used to see whether the method was called from the product edit view
    @customer_content = CustomerContent.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @customer_content }
    end
  end

  # GET /customer_contents/new
  # GET /customer_contents/new.json
  def new
    @production_orderline_id = params[:production_orderline_id]   # used to see whether the method was called from the production orderline show view
    @product_id = params[:product_id]   # used to see whether the method was called from the product edit view
    @customer_content = CustomerContent.new
    @customer_content.production_orderline = ProductionOrderline.find(@production_orderline_id) if @production_orderline_id
    if @product_id
      @customer_content.product = Product.find(@product_id)
    elsif @customer_content.production_orderline
      @customer_content.product = Product.find(@customer_content.production_orderline.product_id)
    end
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @customer_content }
    end
  end

  # GET /customer_contents/1/edit
  def edit
    @production_orderline_id = params[:production_orderline_id]   # used to see whether the method was called from the production orderline show view
    @product_id = params[:product_id]   # used to see whether the method was called from the product edit view
    @customer_content = CustomerContent.find(params[:id])
  end

  # POST /customer_contents
  # POST /customer_contents.json
  def create
    @production_orderline_id = params[:production_orderline_id] unless params[:production_orderline_id].blank?
    @product_id = params[:product_id] unless params[:product_id].blank?
    params[:customer_content][:production_orderline_id] = @production_orderline_id if @production_orderline_id    # if created from a production orderline, take that production orderline
    if @product_id
      params[:customer_content][:product_id] = @product_id
    elsif params[:customer_content][:production_orderline_id]
      # we only have a production_orderline, find its product and set it here so we can always access the product of customer_content directly
      production_orderline = ProductOrderline.find(params[:customer_content][:production_orderline_id])
      params[:customer_content][:product_id] = production_orderline.product_id
    end
    @customer_content = CustomerContent.new(strong_params)

    respond_to do |format|
      if @customer_content.save
        flash[:notice] = 'Customer content was successfully created.'
        if @product_id && !@production_orderline_id
          format.html { redirect_to edit_product_path(id: @product_id)}
        else
          format.html { redirect_to customer_content_path(@customer_content, production_orderline_id: @production_orderline_id, product_id: @product_id) }
        end
        format.json { render json: @customer_content, status: :created, location: @customer_content }
      else
        flash[:alert] = 'Customer content could not be saved.'
        format.html { render action: "new" }
        format.json { render json: @customer_content.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /customer_contents/1
  # PUT /customer_contents/1.json
  def update
    @production_orderline_id = params[:production_orderline_id] unless params[:production_orderline_id].blank?   # used to see whether the method was called from the production orderline show view
    @product_id = params[:product_id] unless params[:product_id].blank?   # used to see whether the method was called from the product edit view
    @customer_content = CustomerContent.find(params[:id])

    respond_to do |format|
      if @customer_content.update_attributes(strong_params)
        flash[:notice] = 'Customer content was successfully updated.'
        if @product_id && !@production_orderline_id
          format.html { redirect_to edit_product_path(id: @product_id)}
        else
          format.html { redirect_to customer_content_path(@customer_content, production_orderline_id: @production_orderline_id, product_id: @product_id) }
        end
        format.json { head :no_content }
      else
        flash[:alert] = "Customer content could not be saved."
        format.html { render action: "edit" }
        format.json { render json: @customer_content.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /customer_contents/1
  # DELETE /customer_contents/1.json
  # DELETE /customer_contents/1.xml
  def destroy
    @production_orderline_id = params[:production_orderline_id]   # used to see whether the method was called from the production orderline show view
    @product_id = params[:product_id]   # used to see whether the method was called from the product edit view
    @customer_content = CustomerContent.find(params[:id])
    @customer_content.destroy

    respond_to do |format|
      format.html { redirect_to customer_contents_path(production_orderline_id: @production_orderline_id, product_id: @product_id) }
      format.json { head :no_content }
      format.xml  { head :ok }
    end
  end

  def render_value
    @customer_content = CustomerContent.find(params[:id])
    respond_to do |format|
      format.json { render json: @customer_content.value, status: :created}
      format.xml { render xml: JSON.parse(@customer_content.value).to_xml, status: :created }
    end
  end

  private  
  def sort_column  
    params[:sort] || "id"  
  end  
    
  def sort_direction  
    params[:direction] || "asc"  
  end
  
  def search
    params[:search] || ""
  end 
end
