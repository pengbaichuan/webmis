class CustomerDepotsController < ApplicationController
  before_filter :authorize
  authorize_resource
  
  layout 'webmis'
  helper_method :sort_column, :sort_direction, :search
  
  # GET /customer_depots
  # GET /customer_depots.xml
  def index
    if params[:reset]
      params.delete :search
      params.delete :reset   

      params.delete :sort
      params.delete :direction
    end
    
    conditions = "1=1"
    conditions_string_ary = []
    conditions_param_values = []    
    
    unless params[:search].blank?
      conditions_string_ary << " and (label LIKE ?
              or name LIKE ? 
               or city LIKE ? 
                or country LIKE ? 
                 or contact_name LIKE ? 
                  or unloading_point LIKE ? 
                   or vat_statement LIKE ?
                    or customer_code LIKE ?
                     or supplier_code LIKE ?)"
      9.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
    end
    
    conditions = conditions + conditions_string_ary.join(" ")
    @customer_depots = CustomerDepot.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 30, :page => params[:page])

    if request.xhr?
      render(:partial => "customer_depot", :collection => @customer_depots)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @customer_depots }
      end
    end
  end

  # GET /customer_depots/1
  # GET /customer_depots/1.xml
  def show
    @customer_depot = CustomerDepot.find(params[:id])
    
    @roles = []
    @roles << ["Ordering"] if @customer_depot.ordering_yn
    @roles << ["Shipping"] if @customer_depot.shipping_yn
    @roles << ["Invoicing"] if @customer_depot.invoicing_yn
    @roles << ["none"] if @roles.size == 0

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @customer_depot }
    end
  end

  # GET /customer_depots/new
  # GET /customer_depots/new.xml
  def new
    @customer_depot = CustomerDepot.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @customer_depot }
    end
  end

  # GET /customer_depots/1/edit
  def edit
    @customer_depot = CustomerDepot.find(params[:id])
  end

  # POST /customer_depots
  # POST /customer_depots.xml
  def create
    @customer_depot = CustomerDepot.new(strong_params)

    respond_to do |format|
      if @customer_depot.save
        format.html { redirect_to(@customer_depot, :notice => 'CustomerDepot was successfully created.') }
        format.xml  { render :xml => @customer_depot, :status => :created, :location => @customer_depot }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @customer_depot.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /customer_depots/1
  # PUT /customer_depots/1.xml
  def update
    @customer_depot = CustomerDepot.find(params[:id])

    respond_to do |format|
      if @customer_depot.update_attributes(strong_params)
        format.html { redirect_to(@customer_depot, :notice => 'CustomerDepot was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @customer_depot.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /customer_depots/1
  # DELETE /customer_depots/1.xml
  def destroy
    @customer_depot = CustomerDepot.find(params[:id])
    @customer_depot.destroy

    respond_to do |format|
      format.html { redirect_to(customer_depots_url) }
      format.xml  { head :ok }
    end
  end
  
  private  
  def sort_column  
    params[:sort] || "customer_depots.label"  
  end  
    
  def sort_direction  
    params[:direction] || "asc"  
  end
  
  def search
    params[:search] || ""
  end
  
end
