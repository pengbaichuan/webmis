class CustomerTargetsController < ApplicationController
  before_filter :authorize
  authorize_resource

  layout "webmis"
  helper_method :sort_column, :sort_direction, :search
  
  # GET /customer_targets
  # GET /customer_targets.json
  def index
    conditions =  '1=1'
    conditions_param_values = []
    
    if params[:reset]
      params.delete :search
      params.delete :reset

      params[:sort] = 'id'
      params[:direction] = 'asc'
    end
    @production_orderline_id = params[:production_orderline_id] unless params[:production_orderline_id].to_s.blank?
    @product_id = params[:product_id] unless params[:product_id].to_s.blank?
    @customer_content_id = params[:customer_content_id] unless params[:customer_content_id].to_s.blank?

    if @customer_content_id
      conditions += " and (customer_contents.id = ?)"
      conditions_param_values << @customer_content_id
    end
    if @production_orderline_id
      conditions += " and (customer_contents.production_orderline_id = ?)"
      conditions_param_values << @production_orderline_id
    end
    if @product_id
      conditions += " and (customer_contents.product_id = ?)"
      conditions_param_values << @product_id
    end

    if params[:search] && params[:search] != ""
      conditions += " and (customer_targets.label LIKE ? OR 
                             customer_targets.output_target LIKE ? OR
                               customer_content_specifications.name LIKE ? OR
                                 customer_content_definitions.id LIKE ? OR
                                   customer_contents.product_id LIKE ? OR
                                     customer_content_specifications.name LIKE ?
                          )"
      6.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
    end
    
    @customer_targets = CustomerTarget.joins('INNER JOIN customer_contents ON customer_contents.id = customer_targets.customer_content_id ' +
                                             'LEFT OUTER JOIN customer_content_definitions ON customer_content_definitions.id = customer_targets.customer_content_definition_id ' +
                                             'LEFT OUTER JOIN customer_content_specifications ON customer_content_specifications.id = customer_content_definitions.customer_content_specification_id '
                                              ).where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(per_page: 100, page: params[:page])
    
    if request.xhr?
        flash.keep[:notice]
        render(partial: "customer_target", collection: @customer_targets)
      else
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @customer_targets }
      end
    end

  end

  # GET /customer_targets/1
  # GET /customer_targets/1.json
  def show
    @customer_content_id = params[:customer_content_id]   # used to see whether the method was called from the customer content show view
    @production_orderline_id = params[:production_orderline_id]   # used to see whether the customer content show view was called from the production orderline show view
    @product_id = params[:product_id]   # used to see whether the customer content show view was called from the product edit view
    @customer_target = CustomerTarget.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @customer_target }
    end
  end

  # GET /customer_targets/new
  # GET /customer_targets/new.json
  def new
    @customer_content_id = params[:customer_content_id]   # used to see whether the method was called from the customer content show view
    @production_orderline_id = params[:production_orderline_id]   # used to see whether the customer content show view was called from the production orderline show view
    @product_id = params[:product_id]   # used to see whether the customer content show view was called from the product edit view
    @customer_target = CustomerTarget.new
    @customer_target.customer_content = CustomerContent.find(@customer_content_id) if @customer_content_id
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @customer_target }
    end
  end

  # GET /customer_targets/1/edit
  def edit
    @customer_content_id = params[:customer_content_id]   # used to see whether the method was called from the customer content show view
    @production_orderline_id = params[:production_orderline_id]   # used to see whether the customer content show view was called from the production orderline show view
    @product_id = params[:product_id]   # used to see whether the customer content show view was called from the product edit view
    @customer_target = CustomerTarget.find(params[:id])
  end

  # POST /customer_targets
  # POST /customer_targets.json
  def create
    @customer_content_id = params[:customer_content_id] unless params[:customer_content_id].blank?
    @production_orderline_id = params[:production_orderline_id] unless params[:production_orderline_id].blank?
    @product_id = params[:product_id] unless params[:product_id].blank?
    params[:customer_target][:customer_content_id] = @customer_content_id if @customer_content_id    # if created from a customer content, take that content
    @customer_target = CustomerTarget.new(strong_params)

    respond_to do |format|
      if @customer_target.save
        flash[:notice] = 'Customer target was successfully created.'
        if @product_id && !@production_orderline_id
          format.html { redirect_to edit_product_path(id: @product_id)}
        else
          format.html { redirect_to({ action: :show, id: @customer_target.id , customer_content_id: @customer_content_id, production_orderline_id: @production_orderline_id, product_id: @product_id }) }
        end
        format.json { render json: @customer_target, status: :created, location: @customer_target }
      else
        flash[:alert] = 'Customer target could not be saved.'
        format.html { render action: "new" }
        format.json { render json: @customer_target.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /customer_targets/1
  # PUT /customer_targets/1.json
  def update
    @customer_content_id = params[:customer_content_id] unless params[:customer_content_id].blank?
    @production_orderline_id = params[:production_orderline_id] unless params[:production_orderline_id].blank?
    @product_id = params[:product_id] unless params[:product_id].blank?
    @customer_target = CustomerTarget.find(params[:id])

    respond_to do |format|
      if @customer_target.update_attributes(strong_params)
        flash[:notice] = 'Customer target was successfully updated.'
        if @product_id && !@production_orderline_id
          format.html { redirect_to edit_product_path(id: @product_id)}
        else
          format.html { redirect_to({action: :show, id: @customer_target.id , customer_content_id: @customer_content_id, production_orderline_id: @production_orderline_id, product_id: @product_id }) }
        end
        format.json { head :no_content }
      else
        flash[:alert] = "Customer target could not be saved."
        format.html { render action: "edit" }
        format.json { render json: @customer_target.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /customer_targets/1
  # DELETE /customer_targets/1.json
  # DELETE /customer_targets/1.xml
  def destroy
    @customer_content_id = params[:customer_content_id]   # used to see whether the method was called from the customer content show view
    @production_orderline_id = params[:production_orderline_id]   # used to see whether the customer content show view was called from the production orderline show view
    @product_id = params[:product_id]   # used to see whether the customer content show view was called from the product edit view
    @customer_target = CustomerTarget.find(params[:id])
    @customer_target.destroy

    respond_to do |format|
      format.html { if @customer_content_id 
                      redirect_to({controller: :customer_contents, action: :show, id: @customer_content_id, production_orderline_id: @production_orderline_id, product_id: @product_id})
                    elseif @production_orderline_id
                      redirect_to customer_targets_url(production_orderline_id: @production_orderline_id, product_id: @product_id)
                    elsif @product_id
                      redirect_to edit_product_path(id: @product_id)
                    else
                      redirect_to customer_targets_url(production_orderline_id: @production_orderline_id)
                    end
                  }
      format.json { head :no_content }
      format.xml  { head :ok }
    end
  end

  private  
  def sort_column  
    params[:sort] || "id"  
  end  
    
  def sort_direction  
    params[:direction] || "asc"  
  end
  
  def search
    params[:search] || ""
  end 
end
