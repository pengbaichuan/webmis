class CvtoolBundlesController < ApplicationController
  before_filter :authorize
  authorize_resource
  
  layout 'webmis'
  helper_method :sort_column, :sort_direction, :search
  
  # GET /cvtool_bundles
  # GET /cvtool_bundles.xml
  def index
    @include_inactive = false
    if params[:reset]
      params.delete :search
      params.delete :reset

      params[:sort] = 'cvtool_bundles.id'
      params[:direction] = 'desc'
      params[:inclinactive] = "0"
      @include_inactive = false
    end
    conditions = 'cvtool_bundles.active_yn != 0 '
    conditions_string_ary = []
    conditions_param_values = []

    if params[:inclinactive] == '1'
      @include_inactive = true
      conditions = '1=1 '
    end

    unless params[:search].blank?
      conditions_string_ary << "AND (cvtool_bundles.id = #{params[:search].strip.to_i} OR cvtool_bundles.name LIKE ? OR cvtool_bundles.remarks LIKE ?)"
      2.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
    end
    conditions = conditions + conditions_string_ary.join(" ")

    @cvtool_bundles = CvtoolBundle.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])

    if request.xhr?
      render(:partial => "cvtool_bundle", :collection => @cvtool_bundles)
    end
  end

  # GET /cvtool_bundles/1
  # GET /cvtool_bundles/1.xml
  def show
    @cvtool_bundle = CvtoolBundle.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @cvtool_bundle }
    end
  end

  # GET /cvtool_bundles/new
  # GET /cvtool_bundles/new.xml
  def new
    @cvtool_bundle = CvtoolBundle.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @cvtool_bundle }
    end
  end

  # GET /cvtool_bundles/1/edit
  def edit
    @cvtool_bundle = CvtoolBundle.find(params[:id])
  end

  # POST /cvtool_bundles
  # POST /cvtool_bundles.xml
  def create
    @cvtool_bundle = CvtoolBundle.new(strong_params)

    respond_to do |format|
      if @cvtool_bundle.save
        format.html { redirect_to(@cvtool_bundle, :notice => 'CvtoolBundle was successfully created.') }
        format.xml  { render :xml => @cvtool_bundle, :status => :created, :location => @cvtool_bundle }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @cvtool_bundle.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /cvtool_bundles/1
  # PUT /cvtool_bundles/1.xml
  def update
    @cvtool_bundle = CvtoolBundle.find(params[:id])

    respond_to do |format|
      if @cvtool_bundle.update_attributes(strong_params)
        format.html { redirect_to(@cvtool_bundle, :notice => 'CvtoolBundle was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @cvtool_bundle.errors, :status => :unprocessable_entity }
      end
    end
  end

  def toggle_active_yn
    @cvtool_bundle = CvtoolBundle.find(params[:id])
    if @cvtool_bundle.active_yn
      @cvtool_bundle.active_yn = false
      flash[:notice] = 'CV-tool bundle deactivated.'
    else
      @cvtool_bundle.active_yn = true
      flash[:notice] = 'CV-tool bundle activated.'
    end
    @cvtool_bundle.save

    respond_to do |format|
      format.html { redirect_to(:back) }
      format.xml  { head :ok }
    end
  end

  # DELETE /cvtool_bundles/1
  # DELETE /cvtool_bundles/1.xml
  def destroy
    @cvtool_bundle = CvtoolBundle.find(params[:id])
    @cvtool_bundle.destroy

    respond_to do |format|
      format.html { redirect_to(cvtool_bundles_url) }
      format.xml  { head :ok }
    end
  end

  def get_cvtool_bundles
    @cb = CvtoolBundle.select_cvtool_bundles(params[:conversiontool_id])
    @cvtool_bundle_id = params[:cvtool_bundle_id]
    @tag = params[:tag]
    @name = params[:useworkaround] && params[:useworkaround] == "1" ? "cvtool_bundle_id_workaround" : "cvtool_bundle_id"
    render :partial => "cvtool_bundle_select"
  end

  private  
  def sort_column  
    params[:sort] || "cvtool_bundles.name"  
  end  
    
  def sort_direction  
    params[:direction] || "asc"  
  end
  
  def search
    params[:search] || ""
  end


end
