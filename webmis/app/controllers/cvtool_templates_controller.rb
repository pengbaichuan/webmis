class CvtoolTemplatesController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search
  
  # GET /cvtool_templates
  # GET /cvtool_templates.xml
  layout "webmis"
  
  def index
     if params[:reset]
      params.delete :search
      params.delete :reset
      params.delete :sort
      params.delete :direction
    end

    conditions_string_ary = ["1=1"]
    conditions_param_values = []

    unless params[:search].blank?
      q = params[:search].strip
      conditions_string_ary << "(cvtool_templates.id = ? OR
                                 cvtool_templates.name LIKE ? OR
                                 cvtool_templates.filename LIKE ? OR
                                 cvtool_templates.description LIKE ?)"
      conditions_param_values << q.to_i # for id
      3.times do
        conditions_param_values << "%#{q}%"
      end
    end

    conditions = conditions_string_ary.join(' AND ')

    @cvtool_templates = CvtoolTemplate.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])
    generate_id_sequence_cookie(@cvtool_templates,params[:page])

    if request.xhr?
      render(:partial => "cvtool_template", :collection => @cvtool_templates)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @cvtool_templates }
      end
    end
  end

  # GET /cvtool_templates/1
  # GET /cvtool_templates/1.xml
  def show
    @cvtool_template = CvtoolTemplate.find(params[:id])
    @formatted_file_data = "#"
    if @cvtool_template.template
      @cvtool_template.template.split("\n").each do |line|
        @formatted_file_data = @formatted_file_data + line + "\n"
      end
    end

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @cvtool_template }
    end
  end

  # GET /cvtool_templates/new
  # GET /cvtool_templates/new.xml
  def new
    @cvtool_template = CvtoolTemplate.new
    @cvtool_template.isactive = 1

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @cvtool_template }
    end
  end

  # GET /cvtool_templates/1/edit
  def edit   
    @cvtool_template = CvtoolTemplate.find(params[:id])
    begin
      
    @formatted_file_data = "#"
    if @cvtool_template.template
      @cvtool_template.template.split("\n").each do |line|
        @formatted_file_data = @formatted_file_data + line + "\n"
      end
    end
       
	  @a = CvtoolTemplate.where("id in (select MIN(id) from cvtool_templates where id > "+@cvtool_template.id.to_s+")").first

    if @a
      @next = @a.id
      @result_next = true
    end
    rescue => error
	  @result_next = false
    end

    begin
	@b = CvtoolTemplate.where("id in (select MAX(id) from cvtool_templates where id < "+@cvtool_template.id.to_s+")").first
	if @b
		@previous = @b.id
		@result_previous = true
	end
    rescue => error
	@result_previous = false
    end
  end

  # POST /cvtool_templates
  # POST /cvtool_templates.xml
  def create
    @cvtool_template = CvtoolTemplate.new(strong_params)
    @formatted_file_data = "#"
    if params[:uploaded_file].to_s != ""
        @file_data = params[:uploaded_file].read
        @cvtool_template.filename = params[:uploaded_file].original_filename.to_s
        @cvtool_template.template = @file_data
        @cvtool_template.template_by_user_id = current_user.id
        @cvtool_template.save    
        @file_data.split("\n").each do |line|
          @formatted_file_data = @formatted_file_data + line + "\n"
        end
    end

    respond_to do |format|
      if @cvtool_template.save
        flash[:notice] = 'CvtoolTemplate was successfully created.'
        format.html { redirect_to(@cvtool_template) }
        format.xml  { render :xml => @cvtool_template, :status => :created, :location => @cvtool_template }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @cvtool_template.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /cvtool_templates/1
  # PUT /cvtool_templates/1.xml
  def update
    @cvtool_template = CvtoolTemplate.find(params[:id])

    respond_to do |format|
      if @cvtool_template.update_attributes(strong_params)
        flash[:notice] = 'CvtoolTemplate was successfully updated.'
        format.html { redirect_to(@cvtool_template) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @cvtool_template.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /cvtool_templates/1
  # DELETE /cvtool_templates/1.xml
  def destroy
    @cvtool_template = CvtoolTemplate.find(params[:id])
    @cvtool_template.destroy

    respond_to do |format|
      format.html { redirect_to(cvtool_templates_url) }
      format.xml  { head :ok }
    end
  end
  
  def upload_script
    @cvtool_template = CvtoolTemplate.find(params[:id])
    @formatted_file_data = "#"
    
    if params[:uploaded_file].to_s != ""
        @file_data = params[:uploaded_file].read
        @cvtool_template.filename = params[:uploaded_file].original_filename.to_s
        @cvtool_template.template = @file_data
        @cvtool_template.template_by_user_id = current_user.id
        @cvtool_template.save    
        @file_data.split("\n").each do |line|
          @formatted_file_data = @formatted_file_data + line + "\n"
        end
    end
    
    if @cvtool_template.save
      flash[:notice] = 'Conversion template was successfully uploaded, and saved.'
    end
      redirect_to :action => :edit, :id => params[:id]
  end
  
   def sendscript
    filename = @cvtool_template = CvtoolTemplate.find(params[:id]).filename.to_s + ".txt"    
    if request.env['HTTP_USER_AGENT'] =~ /msie/i #this is required if you want this to work with IE
      headers['Pragma'] = 'public'
      headers["Content-type"] = "text/plain"
      headers['Cache-Control'] = 'no-cache, must-revalidate, post-check=0, pre-check=0'
      headers['Content-Disposition'] = "attachment; filename=\"#{filename}\""
      headers['Expires'] = "0"
    else
      headers["Content-Type"] ||= 'text/plain'
      headers["Content-Disposition"] = "attachment; filename=\"#{filename}\""
    end
    
    render :text => CvtoolTemplate.find(params[:id]).template
  end

private
  def sort_column
    !params[:sort].blank? ? params[:sort] : "cvtool_templates.isactive"
  end

  def sort_direction
    !params[:direction].blank? ? params[:direction] : "desc"
  end

  def search
    params[:search] || ""
  end
end
