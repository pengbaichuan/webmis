class DashboardGridWidgetsController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"
    
  # GET /dashboard_grid_widgets
  # GET /dashboard_grid_widgets.json
  def index
    @dashboard_grid_widgets = DashboardGridWidget.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @dashboard_grid_widgets }
    end
  end

  # GET /dashboard_grid_widgets/1
  # GET /dashboard_grid_widgets/1.json
  def show
    @dashboard_grid_widget = DashboardGridWidget.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @dashboard_grid_widget }
    end
  end

  # GET /dashboard_grid_widgets/new
  # GET /dashboard_grid_widgets/new.json
  def new
    @dashboard_grid_widget = DashboardGridWidget.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @dashboard_grid_widget }
    end
  end

  # GET /dashboard_grid_widgets/1/edit
  def edit
    @dashboard_grid_widget = DashboardGridWidget.find(params[:id])
  end

  # POST /dashboard_grid_widgets
  # POST /dashboard_grid_widgets.json
  def create
    @dashboard_grid_widget = DashboardGridWidget.new(strong_params)

    respond_to do |format|
      if @dashboard_grid_widget.save
        format.html { redirect_to @dashboard_grid_widget, notice: 'Dashboard grid widget was successfully created.' }
        format.json { render json: @dashboard_grid_widget, status: :created, location: @dashboard_grid_widget }
      else
        format.html { render action: "new" }
        format.json { render json: @dashboard_grid_widget.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /dashboard_grid_widgets/1
  # PUT /dashboard_grid_widgets/1.json
  def update
    @dashboard_grid_widget = DashboardGridWidget.find(params[:id])

    respond_to do |format|
      if @dashboard_grid_widget.update_attributes(strong_params)
        format.html { redirect_to @dashboard_grid_widget, notice: 'Dashboard grid widget was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @dashboard_grid_widget.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dashboard_grid_widgets/1
  # DELETE /dashboard_grid_widgets/1.json
  def destroy
    @dashboard_grid_widget = DashboardGridWidget.find(params[:id])
    @dashboard_grid_widget.destroy

    respond_to do |format|
      format.html { redirect_to dashboard_grid_widgets_url }
      format.json { head :no_content }
    end
  end

  def save_position
    params[:widgets].reject! { |c| c.empty? }.each do |id_str|
      widget = DashboardGridWidget.where(:dashboard_widget_id => id_str.scan(/\d/),:dashboard_grid_id => params[:dashboard_grid_id]).first
      widget.position = params[:widgets].index(id_str)
      widget.save
    end
    render :nothing => true
  end
end
