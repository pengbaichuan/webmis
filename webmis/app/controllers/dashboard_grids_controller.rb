class DashboardGridsController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"
  
  helper_method :sort_column, :sort_direction, :search
  # GET /dashboard_grids
  # GET /dashboard_grids.json
  def index
    @dashboard_grids = DashboardGrid.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @dashboard_grids }
    end
  end

  def my_dashboards
    @dashboard_grids = current_user.dashboard_grids.paginate(:per_page => 50, :page => params[:page])
    @name_user = "#{current_user.first_name} #{current_user.last_name}"
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @dashboard_grids }
    end
  end

  # GET /dashboard_grids/1
  # GET /dashboard_grids/1.json
  def show
    @dashboard_grid = DashboardGrid.find(params[:id])
    @dashboard_widgets = @dashboard_grid.dashboard_grid_widgets.order(:position).collect{|w|w.dashboard_widget}.compact.reject{ |arr| arr.all?(&:blank?) if !arr.nil? && arr.blank?}

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @dashboard_grid }
    end
  end

  # GET /dashboard_grids/new
  # GET /dashboard_grids/new.json
  def new
    @dashboard_grid = DashboardGrid.new
     @dashboard_grid.user_id = current_user.id

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @dashboard_grid }
    end
  end

  # GET /dashboard_grids/1/edit
  def edit
    @dashboard_grid = DashboardGrid.find(params[:id])
  end

  # POST /dashboard_grids
  # POST /dashboard_grids.json
  def create
    @dashboard_grid = DashboardGrid.new(strong_params)

    respond_to do |format|
      if @dashboard_grid.save
        format.html { redirect_to @dashboard_grid, notice: 'Dashboard grid was successfully created.' }
        format.json { render json: @dashboard_grid, status: :created, location: @dashboard_grid }
      else
        format.html { render action: "new" }
        format.json { render json: @dashboard_grid.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /dashboard_grids/1
  # PUT /dashboard_grids/1.json
  def update
    @dashboard_grid = DashboardGrid.find(params[:id])

    respond_to do |format|
      if @dashboard_grid.update_attributes(strong_params)
        format.html { redirect_to @dashboard_grid, notice: 'Dashboard grid was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @dashboard_grid.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dashboard_grids/1
  # DELETE /dashboard_grids/1.json
  def destroy
    @dashboard_grid = DashboardGrid.find(params[:id])
    @dashboard_grid.destroy

    respond_to do |format|
      format.html { redirect_to dashboard_grids_url }
      format.json { head :no_content }
    end
  end

  def unused_widgets
    @dashboard_grid = DashboardGrid.find(params[:id])
    used_widget_ids = @dashboard_grid.dashboard_widgets.collect{|uw|uw.id}
    if !used_widget_ids.empty?
    @unused_widgets = DashboardWidget.where("id <> #{used_widget_ids.join(' and id <> ')}")
    else
      @unused_widgets = DashboardWidget.all
    end
     respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @unused_widgets.to_json }
    end
  end

private
  def sort_column
    params[:sort] || "name"
  end

  def sort_direction
    params[:direction] || "asc"
  end

  def search
    params[:search] || ""
  end

end
