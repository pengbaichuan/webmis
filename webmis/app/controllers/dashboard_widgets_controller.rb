class DashboardWidgetsController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"
  
  
  layout "webmis", :only => [:index, :show, :new, :edit]
  # layout "dashboard_widgets"

  # GET /dashboard_widgets
  # GET /dashboard_widgets.json
  def index
    @dashboard_widgets = DashboardWidget.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @dashboard_widgets }
    end
  end

  # GET /dashboard_widgets/1
  # GET /dashboard_widgets/1.json
  def show
    @dashboard_widget = DashboardWidget.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @dashboard_widget }
    end
  end

  # GET /dashboard_widgets/new
  # GET /dashboard_widgets/new.json
  def new
    @dashboard_widget = DashboardWidget.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @dashboard_widget }
    end
  end

  # GET /dashboard_widgets/1/edit
  def edit
    @dashboard_widget = DashboardWidget.find(params[:id])
  end

  # POST /dashboard_widgets
  # POST /dashboard_widgets.json
  def create
    @dashboard_widget = DashboardWidget.new(strong_params)

    respond_to do |format|
      if @dashboard_widget.save
        format.html { redirect_to @dashboard_widget, notice: 'Dashboard widget was successfully created.' }
        format.json { render json: @dashboard_widget, status: :created, location: @dashboard_widget }
      else
        format.html { render action: "new" }
        format.json { render json: @dashboard_widget.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /dashboard_widgets/1
  # PUT /dashboard_widgets/1.json
  def update
    @dashboard_widget = DashboardWidget.find(params[:id])

    respond_to do |format|
      if @dashboard_widget.update_attributes(strong_params)
        format.html { redirect_to @dashboard_widget, notice: 'Dashboard widget was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @dashboard_widget.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dashboard_widgets/1
  # DELETE /dashboard_widgets/1.json
  def destroy
    @dashboard_widget = DashboardWidget.find(params[:id])
    @dashboard_widget.destroy

    respond_to do |format|
      format.html { redirect_to dashboard_widgets_url }
      format.json { head :no_content }
    end
  end

  def drop_widget_from_grid
    @dashboard_widget = DashboardWidget.find(params[:id])
    @dashboard_widget.dashboard_grid_widgets.eager_load("dashboard_grid").where("dashboard_grids.user_id = #{current_user.id}").collect{|dgw|dgw.destroy}
    render :nothing => true
  end

  def render_widget
    @dashboard_widget = DashboardWidget.find(params[:id])
    view_action = "widget_#{@dashboard_widget.name.downcase.tr(" ", "_")}"
    if params[:new_widget] && !params[:new_widget].blank?
      dashboard_grid_widget = DashboardGridWidget.new
      dashboard_grid_widget.dashboard_widget_id = @dashboard_widget.id
      dashboard_grid_widget.dashboard_grid_id = params[:dashboard_grid_id]
      dashboard_grid_widget.position = 1000
      DashboardGridWidget.transaction do
        dashboard_grid_widget.save
      end
    end
    eval(view_action)
    render view_action.to_sym, layout: "dashboard_widgets"
  end

  # WIDGETS
  #layout 'dashboard_widgets'
  def widget_demo_01

    @dashboard_widget = DashboardWidget.find_by_name("Demo 01")
    @arr=[]
    ProductionOrder.where(:bp_name => 'PRODUCTION').each do |po|
      line = po.production_orderlines.collect{|pol|[pol.product.volumeid,pol.percentage_done_in_transition,pol.product.name,pol.bp_name] if pol.product}
      line.compact.each do |l|
        if l[1] == nil
          line[line.index(l)] = [l[0],13,l[2],l[3]]
        end
      end
      @arr = @arr +line if !line.nil?
    end
  end

  def widget_demo_02
    @dashboard_widget = DashboardWidget.find_by_name("Demo 02")

    @pie_data = []
    @month_conversion = Conversion.where("updated_at > '#{Time.now - 30.days}'")
    @month_conversion.collect{|x|x.outcome}.uniq.compact.each do |outcome|
      h={}
      h["label"] = outcome
      h["data"] = @month_conversion.where(:outcome => "#{outcome}").size
      @pie_data << h
    end
  end

  def widget_demo_03
    @dashboard_widget = DashboardWidget.find_by_name("Demo 03")
    # dummy
  end

  def widget_demo_04
    @dashboard_widget = DashboardWidget.find_by_name("Demo 04")
    @serverdata = Server.where(:active=>true).collect{|x|[x.server_id,x.assigned_jobs.collect{|j|j.size}[1],x.assigned_jobs]}
  end

  def widget_my_production_orders
    @dashboard_widget = DashboardWidget.find_by_name("My Production orders")
    @production_order_lines = ProductionOrderline.eager_load(:production_order).where("production_orders.requestor = '#{current_user.first_name} #{current_user.last_name}'").order('production_orders.updated_at desc,production_orders.bp_name')
  end

  def widget_production_orderlines_requiring_attention  
    @dashboard_widget = DashboardWidget.find_by_name("Production orderlines requiring attention")
    @production_orderlines = ProductionOrderline.eager_load(:production_order).where("production_orders.bp_name = 'PRODUCTION' and production_orderlines.bp_name != 'CANCELLED' and production_orderlines.bp_name != 'COMPLETE'").find_all{|pol| pol.conversion_jobs.where("jobs.status < #{Job::STATUS_CANCELLED} AND jobs.status >= #{Job::STATUS_MANUAL}").any?}
    @production_orderlines = @production_orderlines.sort_by{|pol| pol.conversion_jobs.where("jobs.status < #{Job::STATUS_CANCELLED} AND jobs.status >= #{Job::STATUS_MANUAL}").count}.reverse!
  end

end
