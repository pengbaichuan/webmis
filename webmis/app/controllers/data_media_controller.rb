class DataMediaController < ApplicationController
 before_filter :authorize
 authorize_resource
  
 layout 'webmis'
  # GET /data_media
  # GET /data_media.xml
  def index
    @data_media = DataMedium.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @data_media }
    end
  end

  # GET /data_media/1
  # GET /data_media/1.xml
  def show
    @data_medium = DataMedium.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @data_medium }
    end
  end

  # GET /data_media/new
  # GET /data_media/new.xml
  def new
    @data_medium = DataMedium.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @data_medium }
    end
  end

  # GET /data_media/1/edit
  def edit
    @data_medium = DataMedium.find(params[:id])
  end

  # POST /data_media
  # POST /data_media.xml
  def create
    @data_medium = DataMedium.new(strong_params)

    respond_to do |format|
      if @data_medium.save
        flash[:notice] = 'DataMedium was successfully created.'
        format.html { redirect_to(@data_medium) }
        format.xml  { render :xml => @data_medium, :status => :created, :location => @data_medium }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @data_medium.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /data_media/1
  # PUT /data_media/1.xml
  def update
    @data_medium = DataMedium.find(params[:id])

    respond_to do |format|
      if @data_medium.update_attributes(strong_params)
        flash[:notice] = 'DataMedium was successfully updated.'
        format.html { redirect_to(@data_medium) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @data_medium.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /data_media/1
  # DELETE /data_media/1.xml
  def destroy
    @data_medium = DataMedium.find(params[:id])
    @data_medium.destroy

    respond_to do |format|
      format.html { redirect_to(data_media_url) }
      format.xml  { head :ok }
    end
  end
end
