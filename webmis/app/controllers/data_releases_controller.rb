class DataReleasesController < ApplicationController
  before_filter :authorize
  authorize_resource
  # GET /data_releases
  # GET /data_releases.xml
  layout "application"
  helper_method :sort_column, :sort_direction, :search

  def index
    @search_opts = {:inactive => "0"}

    if params[:reset]
      params.delete :inactive
      params.delete :search
      params.delete :reset
      params.delete :sort
      params.delete :direction
      params.delete :column_search_field
    end

    conditions_string_ary = []
    conditions_param_values = []

    if params[:inactive]
      @search_opts = {:inactive => "1"}
      conditions_string_ary << "(1=1)"
    else
      conditions_string_ary << "(data_releases.production = 1)"
    end

    unless params[:search].blank?
      q = params[:search].strip
      conditions_string_ary << "(data_releases.id = ?
                                 OR data_releases.name LIKE ?
                                 OR data_releases.release_code LIKE ?
                                 OR data_releases.aliases LIKE ?
                                 OR company_abbrs.company_name LIKE ?
                                 OR company_abbrs.ms_abbr_3 LIKE ?)"
      conditions_param_values << q.to_i # only for id
      5.times do
        conditions_param_values << "\%#{q}\%"
      end
    end

    if !column_search_query.empty?
      conditions_string_ary << "#{column_search_query['query']}"
      conditions_param_values +=  column_search_query["values"]
    end
    conditions = conditions_string_ary.join( ' AND ' )

    @data_releases = DataRelease.includes("company_abbr").where([conditions] + conditions_param_values).references(:company_abbrs).order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])
    generate_id_sequence_cookie(@data_releases,params[:page])

    if request.xhr?
       render(:partial => "data_release", :collection => @data_releases)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @data_releases }
      end
    end
  end

  # GET /data_releases/1
  # GET /data_releases/1.xml
  def show
    @data_release = DataRelease.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @data_release }
    end
  end

  # GET /data_releases/new
  # GET /data_releases/new.xml
  def new
    @data_release = DataRelease.new
    @data_release.production = true
    @data_release.active = true
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @data_release }
    end
  end

  # GET /data_releases/1/edit
  def edit
    @data_release = DataRelease.find(params[:id])
  end

  # POST /data_releases
  # POST /data_releases.xml
  def create
    @data_release = DataRelease.new(strong_params)

    respond_to do |format|
      if @data_release.save
        flash[:notice] = 'DataRelease was successfully created.'
        format.html { redirect_to(@data_release) }
        format.xml  { render :xml => @data_release, :status => :created, :location => @data_release }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @data_release.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /data_releases/1
  # PUT /data_releases/1.xml
  def update
    @data_release = DataRelease.find(params[:id])

    respond_to do |format|
      if @data_release.update_attributes(strong_params)
        flash[:notice] = 'DataRelease was successfully updated.'
        format.html { redirect_to(@data_release) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @data_release.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /data_releases/1
  # DELETE /data_releases/1.xml
  def destroy
    @data_release = DataRelease.find(params[:id])
    @data_release.destroy

    respond_to do |format|
      format.html { redirect_to(data_releases_url) }
      format.xml  { head :ok }
    end
  end

  def render_prev_data_releases
    @data_release = DataRelease.find(params[:id])
    @data_release.company_abbr_id = params[:company_abbr_id]
    render partial:'prev_data_releases_select'
  end
  
private  
  def sort_column  
    params[:sort] || "data_releases.created_at"
  end  
    
  def sort_direction  
    params[:direction] || "desc"
  end
  
  def search
    params[:search] || ""
  end
end
