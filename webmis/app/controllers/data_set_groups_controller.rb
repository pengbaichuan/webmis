class DataSetGroupsController < ApplicationController

  before_filter :authorize
  authorize_resource param_method: :strong_params
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search

  # GET /data_set_groups
  # GET /data_set_groups.json
  def index
    if params[:reset]
      params.delete :removed
      params.delete :search
      params.delete :reset
      params[:sort] = 'data_set_groups.company_abbr'
      params[:direction] = 'asc'
      params.delete :column_search_field
    end

    if params[:removed]
      @removed = true
      conditions = "1=1"
    else
      conditions = "(data_set_groups.status != '#{DataSetGroup::STATUS_REMOVED}')"
    end
    conditions_string_ary = []
    conditions_param_values = []
    conditions_string_ary << conditions

    unless params[:search].blank?
      q = params[:search].strip
      conditions_string_ary << "AND (data_set_groups.id = ?
                                OR data_set_groups.data_release LIKE ?
                                OR data_set_groups.region_code LIKE ?
                                OR data_set_groups.data_type LIKE ?
                                OR data_set_groups.company_abbr LIKE ?
                                OR data_set_groups.filling LIKE ?
                                OR data_set_groups.suffix LIKE ?)"
      conditions_param_values << q.to_i
      6.times do
        conditions_param_values << "%#{q}%"
      end
      data_release_aliases = DataRelease.where("aliases LIKE '\%#{q}\%'")
      data_release_aliases.collect{|d|d.name}.each do |a|
        conditions_string_ary << " OR data_set_groups.data_release = '#{a}'"
      end
    end
    
    if !column_search_query.empty?
      conditions_string_ary << "AND #{column_search_query['query']}"
      conditions_param_values += column_search_query["values"]
    end
    conditions = conditions_string_ary.join( ' ' )
    @data_set_groups = DataSetGroup.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])
    generate_id_sequence_cookie(@data_set_groups,params[:page])

    if request.xhr?
     render(:partial => "data_set", :collection => @data_set_groups,:params => params)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @data_set_groups }
      end
    end
  end

  # GET /data_set_groups/1
  # GET /data_set_groups/1.json
  def show
    @data_set_group = DataSetGroup.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @data_set_group }
    end
  end

  # GET /data_set_groups/1/handle
  # GET /data_set_groups/1/handle.json
  def handle
    @data_set_group = DataSetGroup.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @data_set_group }
    end
  end

  # GET /data_set_groups/new
  # GET /data_set_groups/new.json
  def new
    @data_set_group = DataSetGroup.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @data_set_group }
    end
  end

  # GET /data_set_groups/1/edit
  def edit
    @data_set_group = DataSetGroup.find(params[:id])
  end

  # POST /data_set_groups
  # POST /data_set_groups.json
  def create
    dss = []
    if params[:dss]
      dss = params[:dss]
    end
    @data_set_group = DataSetGroup.new(strong_params)

    respond_to do |format|
      if @data_set_group.save && @data_set_group.sync_lines(dss)
        format.html { redirect_to @data_set_group, notice: 'Data set group was successfully created.' }
        format.json { render json: @data_set_group, status: :created, location: @data_set_group }
      else
        format.html { render action: "new" }
        format.json { render json: @data_set_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /data_set_groups/1
  # PUT /data_set_groups/1.json
  def update
    @data_set_group = DataSetGroup.find(params[:id])
    dss = []
    dss = params[:dss] if params[:dss]
    respond_to do |format|
      if @data_set_group.update_attributes(strong_params) && @data_set_group.sync_lines(dss)
        format.html { redirect_to @data_set_group, notice: 'Data set group was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @data_set_group.errors, status: :unprocessable_entity }
      end
    end
  end

  def filling_for_data_type
    if !params[:id].blank?
      @data_set_group = DataSetGroup.find(params[:id])
    else
      @data_set_group = nil
    end
    if !params[:data_type].blank?
      @datatype_fillings = DataType.find_by_name(params[:data_type]).fillings.active.order(:name).collect {|f| [ "#{f.name}","#{f.name}" ] }
    else
      @datatype_fillings = []
    end
  end

  def release_for_company_abbr
    if !params[:id].blank?
      @data_set = DataSetGroup.find(params[:id])
    else
      @data_set = nil
    end
    if !params[:company_abbr].blank?
      @company_abbr_data_releases = CompanyAbbr.find_by_ms_abbr_3(params[:company_abbr]).data_releases.production.order(:name).collect {|f| [ "#{f.name}","#{f.name}" ] }
    else
      @company_abbr_data_releases = []
    end
  end

  def remove
    @data_set_group = DataSetGroup.find(params[:id])
    @data_set_group.status = DataSetGroup::STATUS_REMOVED

    respond_to do |format|
      begin
        @data_set_group.save!
        format.html { redirect_to data_set_groups_url, notice: 'Data set group was successfully removed.' }
        format.json { head :no_content }
      rescue => e
        format.html { redirect_to :back, error: "Data set group could not be removed.<br />#{e.message}" }
        format.json { render json: @data_set_group.errors, status: :unprocessable_entity }
      end
    end
  end

  def restore
    @data_set_group = DataSetGroup.find(params[:id])
    @data_set_group.status = DataSetGroup::STATUS_ACTIVE

    respond_to do |format|
      begin
        @data_set_group.save!
        format.html { redirect_to @data_set_group, notice: 'Data set group was successfully restored.' }
        format.json { render json: @data_set_group, status: :created, location: @data_set_group }
      rescue => e
        format.html { redirect_to :back, error: "Data set group could not be restored.<br />#{e.message}" }
        format.json { render json: @data_set_group.errors, status: :unprocessable_entity }
      end
    end
  end

  def clone
    @old_data_set_group = DataSetGroup.find(params[:id])

    respond_to do |format|
      begin
        @data_set_group = @old_data_set_group.duplicate
        format.html { redirect_to edit_data_set_group_path(@data_set_group), notice: 'Data set group was successfully duplicated.' }
        format.json { head :no_content }
      rescue => e
        format.html { redirect_to :back, error: "Data set group could not be duplicated.<br />#{e.message}" }
        format.json { render json: @data_set_group.errors, status: :unprocessable_entity }
      end
    end
  end

  def check_production_order
    @data_set_group = DataSetGroup.find(params[:id])
    if !@data_set_group.production_order_allowed?(session[:project_id])
      flash[:error] = "One or more data sets in this group are due to status disabled for creating a production order."
      redirect_to :back
      return false
    end
    if session[:project_id] == Project.default.id
      @use_test_versions = false
    else
      @use_test_versions = true
    end

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @data_set_group }
    end
  end
  
private
  def sort_column  
    params[:sort] || "data_set_groups.company_abbr"
  end  
  
  def sort_direction  
    params[:direction] || "asc"
  end
  
  def search
    params[:search] || ""
  end   
end
