class DataSetsController < ApplicationController

  before_filter :authorize
  authorize_resource param_method: :strong_params
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search

  # GET /data_sets
  # GET /data_sets.json
  def index
    if params[:reset]
      params.delete :removed
      params.delete :removal
      params.delete :search
      params.delete :reset
      params[:sort] = 'data_sets.id'
      params[:direction] = 'desc'
      params.delete :column_search_field
    end
    conditions = "1=1"
    if params[:removed]
      @removed = true
    else
      conditions += " AND (data_sets.status != '#{DataSet::STATUS_REMOVED}')"
    end
    if params[:removal]
      @removal = true
    else
      conditions += " AND (data_sets.status != '#{DataSet::STATUS_REMOVAL_CANDIDATE}')"
    end
    conditions_string_ary = []
    conditions_param_values = []
    conditions_string_ary << conditions

    unless params[:search].blank?
      q = params[:search].strip
      conditions_string_ary << 'AND (data_sets.id = ?
                                OR data_releases.name LIKE ?
                                OR data_sets.name LIKE ?
                                OR data_types.name LIKE ?
                                OR company_abbrs.company_name LIKE ?
                                OR regions.name LIKE ?
                                OR data_sets.remarks LIKE ?
                                OR receptions.references LIKE ?
                                OR direct_receptions_data_sets.references LIKE ?)'
      conditions_param_values << q.to_i
      8.times do
        conditions_param_values << "%#{q}%"
      end
    end

    if !column_search_query.empty?
      conditions_string_ary << "AND #{column_search_query['query']}"
      conditions_param_values += column_search_query["values"]
    end
    conditions = conditions_string_ary.join( ' ' )

    @data_sets = DataSet.where([conditions] + conditions_param_values).eager_load(:region,:data_release,:company_abbr,:data_type, :filling, :receptions, :direct_receptions).order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])
    generate_id_sequence_cookie(@data_sets,params[:page])

    if request.xhr?
     render(:partial => "data_set", :collection => @data_sets,:params => params)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @data_sets }
      end
    end
  end

  # GET /data_sets/1
  # GET /data_sets/1.json
  def show
    @data_set = DataSet.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @data_set }
    end
  end

  def handle
    @data_set = DataSet.find(params[:id])
    # when coming from review email link session[:project_id] should always be production
    if session[:project_id] != Project.default.id
      session_to_default_project
      flash[:notice] = "Your project is switched to #{Project.default.name}."
      redirect_to handle_data_set_path(@data_set)
      return false
    end

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @data_set }
    end
  end

  # GET /data_sets/new
  # GET /data_sets/new.json
  def new
    @data_set = DataSet.new
    @coverage_regions = []

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @data_set }
    end
  end

  # GET /data_sets/1/edit
  def edit
    @data_set = DataSet.find(params[:id])
    if @data_set.allow_edit?
      @data_set.status = DataSet::STATUS_DRAFT
      @coverage_regions = @data_set.regions.order(:name)
    else
      if @data_set.status == DataSet::STATUS_REVIEW
        flash[:error] = "Set is in status #{@data_set.status_string}, editing not allowed."
        redirect_to :back
      else
        redirect_to in_use_orders_data_set_path(@data_set)
      end   
    end
  end

  def in_use_orders
    @data_set = DataSet.find(params[:id])
    @production_orders = @data_set.production_orders.active.uniq
  end

  # POST /data_sets
  # POST /data_sets.json
  def create
    @data_set = DataSet.new(strong_params)

    respond_to do |format|
      if @data_set.save && coverage = @data_set.create_or_find_coverage_region(params[:coverage_regions].split(',').uniq.reject { |r| r.empty? })
        @data_set.coverage_id = coverage.id
        @data_set.updated_by = current_user.user_name
        @data_set.save!
        grp_msg = ''
        if params[:create_group] && params[:create_group] == '1'
          grp_msg = @data_set.create_or_add_to_group(true)
        end
        if !params[:data_set_group_id].blank?
          add_remove_group(params[:data_set_group_id],"add")
        end
        sync_parameters_json(@data_set,params[:parameters_json],params[:do_not_process])
        format.html { redirect_to @data_set, notice: "Data set was successfully created.<br />#{grp_msg}" }
        format.json { render json: @data_set, status: :created, location: @data_set }
      else
        @coverage_regions = Region.find(params[:coverage_regions].split(',').reject { |r| r.empty? })
        format.html { render action: "new" }
        format.json { render json: @data_set.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /data_sets/1
  # PUT /data_sets/1.json
  def update
    @data_set = DataSet.find(params[:id])
    data_type = DataType.find(params[:data_set][:data_type_id]) unless params[:data_set][:data_type_id].blank?
    if data_type.nil? || data_type.fillings.empty?
      # when fillings are removed from data_type after creation of this ds
      params[:data_set][:filling_id] = nil
    end

    respond_to do |format|
      if @data_set.update_attributes(strong_params) && coverage = @data_set.create_or_find_coverage_region(params[:coverage_regions].split(',').uniq.reject { |r| r.empty? })
        @data_set.coverage_id = coverage.id
        if @data_set.process_data_elements.active.size > 0
          @data_set.status = DataSet::STATUS_IN_PROGRESS
        else
          @data_set.status = DataSet::STATUS_DRAFT
        end
        @data_set.updated_by = current_user.user_name
        @data_set.save!
        if params[:parameters_json]
          sync_parameters_json(@data_set,params[:parameters_json],params[:do_not_process])
        end
        format.html { redirect_to @data_set, notice: 'Data set was successfully updated.' }
        format.json { head :no_content }
      else
        @coverage_regions = Region.order(:name).find(params[:coverage_regions].split(',').reject { |r| r.empty? })
        format.html { render action: "edit" }
        format.json { render json: @data_set.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /data_sets/1
  # DELETE /data_sets/1.json
  def destroy
    @data_set = DataSet.find(params[:id])
    @data_set.destroy

    respond_to do |format|
      format.html { redirect_to data_sets_url }
      format.json { head :no_content }
    end
  end

  def filling_for_data_type
    if !params[:id].blank?
      @data_set = DataSet.find(params[:id])
    else
      @data_set = nil
    end
    if !params[:data_type_val].blank?
      @datatype_fillings = DataType.find(params[:data_type_val]).fillings.active.order(:name).collect {|f| [ "#{f.name}","#{f.id}" ] }
    else
      @datatype_fillings = []
    end
  end

  def release_for_company_abbr
    if !params[:id].blank?
      @data_set = DataSet.find(params[:id])
    else
      @data_set = nil
    end
    if !params[:company_abbr_val].blank?
      @company_abbr_data_releases = CompanyAbbr.find(params[:company_abbr_val]).data_releases.production.order(:name).collect {|f| [ "#{f.name}","#{f.id}" ] }
    else
      @company_abbr_data_releases = []
    end
  end

  def update_coverage_regions_list_view
    if !params[:coverage_regions].blank? && params[:coverage_regions] != ','
      @coverage_regions = Region.find(params[:coverage_regions].split(',').reject { |r| r.empty? })
    else
      @coverage_regions = []
    end
  end

  def change_status
    @data_set = DataSet.find(params[:id])
    @data_set.status = params[:status]
    @data_set.updated_by = current_user.user_name
    if !@data_set.review_user_id.blank? && params[:status] == DataSet::STATUS_PRODUCTION.to_s
      @data_set.review_user_id = nil
      @data_set.review_request_user_id = nil
    end
    if @data_set.save
      flash[:notice] = "Data set changed to status #{@data_set.status_string}."
    else
      flash[:error] = "Data set could not be changed to status #{@data_set.status_string}."
    end
    redirect_to :back
  end

  def clone
    @data_set = DataSet.find(params[:id])
    used_data_release_ids = DataSet.where(:company_abbr_id => @data_set.company_abbr_id,:region_id =>  @data_set.region_id,:data_type_id =>@data_set.data_type_id,:filling_id => @data_set.filling_id).collect{|ds|ds.data_release_id}.compact.uniq.join(',')
    @company_abbr_data_releases = CompanyAbbr.find(@data_set.company_abbr_id).data_releases.production.where("id NOT IN (#{used_data_release_ids})").order(:name).collect {|f| [ "#{f.name}","#{f.id}" ] }
  end

  def duplicate
    @prev_data_set = DataSet.find(params[:id])
  
    respond_to do |format|
      if @data_set = @prev_data_set.duplicate(params[:data_set][:data_release_id])
        format.html { redirect_to @data_set, notice: 'Data set was successfully duplicated.' }
      else
        format.html { redirect_to :back, notice: 'Data set clone failed.' }
      end
    end
  end

  def add_to_group
    @data_set = DataSet.find(params[:id])
    add_remove_group(params[:data_set_group_id],"add")
  end

  def remove_from_group
    @data_set = DataSet.find(params[:id])
    add_remove_group(params[:data_set_group_id],"del")
  end

  def create_group
    @data_set = DataSet.find(params[:id])
    @msg = @data_set.create_or_add_to_group(true)
    render :layout => false
  end

  def check_production_order
    @data_set = DataSet.find(params[:id])
    if session[:project_id] == Project.default.id
      @use_test_versions = false
    else
      @use_test_versions = true
    end

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @data_set }
    end
  end

  def checked_production_order
    @checklog = Checklog.new(only_check = true, Checklog::LOGLEVEL_WARNING)
    @checklog_level = Checklog::LOGLEVEL_WARNING
    @heading = "Checking creation from process data"
    ProductionOrder.transaction do
      begin
        production_order = perform_production_order_creation(@checklog)
        production_order.execute if production_order
      rescue => e
        @checklog.add_error_line("Check production order encountered: \r\n" + e.message)
        @checklog.add_backtrace_line(e.backtrace.join("\r\n")) unless e.class == ArgumentError
        @checklog.update_result(Checklog::STATUS_EXCEPTION)
      end

      raise ActiveRecord::Rollback, "Just performing a check."
    end
  end
  
  def create_production_order
    @checklog = Checklog.new(only_check = false, Checklog::LOGLEVEL_INFO)
    begin
      @production_order = perform_production_order_creation(@checklog)
      raise "Production order could not be created." unless @production_order
      redirect_to @production_order, :notice => 'Production order is successfully created.'
    rescue => e
      @err = e.message
      puts e.message
      puts e.backtrace
      redirect_to :action => 'check_production_order', :params => params.merge(:error => @err)
    end
  end

  def edit_process_data_elements
    @data_set = DataSet.find(params[:id])
    if @data_set.allow_edit?
      @data_set.status = DataSet::STATUS_IN_PROGRESS
      @new_process_data_element = ProcessDataElement.new({:data_set_id => @data_set.id })
    else
      if @data_set.status == DataSet::STATUS_REVIEW
        flash[:error] = "Set is in status #{@data_set.status_string}, editing not allowed."
        redirect_to :back
      else
        redirect_to in_use_orders_data_set_path(@data_set)
      end
    end
  end

  def browse_directory
    @data_set = DataSet.find(params[:id])
    @path = @data_set.generate_directory.to_s
    render_file_tree_modal(@path)
  end

  def receptions_for_select
    conditions = '1=1'
    conditions_string_ary = []
    conditions_param_values = []

    unless params[:q].blank?
      params[:q].strip.split('+').each do |q|
        conditions_string_ary << " and (receptions.id LIKE ?
              or receptions.status LIKE ?
               or receptions.region_name LIKE ?
                or data_releases.name LIKE ? 
                 or data_types.name LIKE ?
                  or company_abbrs.company_name LIKE ?
                   or receptions.purpose LIKE ?
                    or receptions.storage_location LIKE ?
                     or receptions.remarks LIKE ?
                      or receptions.references LIKE ?)"
        10.times do
          conditions_param_values << "\%#{q}\%"
        end
      end
    end
    conditions = conditions + conditions_string_ary.join(" ")
    @receptions_for_select2 = Reception.available.includes([{:data_release => :company_abbr},:region,:data_type]).references(:data_releases,:company_abbrs,:regions,:data_types).where([conditions] + conditions_param_values).paginate(:per_page => params[:page_limit], :page => params[:page])
    respond_to do |format|
      format.json { render json: @receptions_for_select2.to_json(:include => {:data_release => {:include => :company_abbr},:region => {},:data_type =>{}}) }
    end

  end

  def get_split_by_key
    @process_data_specification = ProcessDataSpecification.find(params[:element_specification_id])
    respond_to do |format|
      format.json { render :json => @process_data_specification }
    end
  end

  def save_process_data_element
    @data_set = DataSet.find(params[:id])
    @data_set.updated_by = current_user.user_name
    @data_set.status = DataSet::STATUS_IN_PROGRESS
    @data_set.save
    if params[:process_data_element_id].blank?
      process_data_element = ProcessDataElement.new({:removed_yn => false,:data_set_id => @data_set.id})
    else
      process_data_element = ProcessDataElement.find(params[:process_data_element_id])
    end
      reception_id =   params[:reception_id]   if !params[:reception_id].blank?
      split_by_value = params[:split_by_value] if !params[:split_by_value].blank?
      old_directory = process_data_element.directory.to_s
      process_data_element.process_data_specification_id = params[:process_data_specification_id]
      process_data_element.reception_id = reception_id
      process_data_element.split_by_value = split_by_value
      begin
        raise "Process data specification is mandatory." if process_data_element.process_data_specification_id.blank?
        process_data_element.directory = process_data_element.generate_directory(false,true)
        if old_directory && !old_directory.blank?
          FileSystemUtil.move_directory(old_directory,process_data_element.directory)
        end
        process_data_element.save!
        process_data_element.log_status({:user => current_user.user_name})       
        render :partial => "process_data_element", :collection => @data_set.process_data_elements.active
      rescue => e
        render :text => "ERROR: #{e}"
      end
  end

  def remove_process_data_element
    @data_set = DataSet.find(params[:id])
    ProcessDataElement.transaction do

      process_data_element = ProcessDataElement.find(params[:process_data_element_id][0])
      process_data_element.removed_yn = true
      begin
        process_data_element.log_status({:user => current_user.user_name})
        process_data_element.save!
        if process_data_element.directory.to_s.match(process_data_element.data_set.generate_directory)
          process_data_element.remove_and_sync(current_user)
        end
        render :partial => "process_data_element", :collection => @data_set.process_data_elements.active
      rescue => e
        render :text => "ERROR: #{e}"
      end
      @data_set.updated_by = current_user.user_name
      if @data_set.process_data_elements.active.size > 0
        @data_set.status = DataSet::STATUS_IN_PROGRESS
      else
        @data_set.status = DataSet::STATUS_DRAFT
      end
      @data_set.save!
    end
  end

  def save_poi_mapping_layout
    @data_set = DataSet.find(params[:id])
    @data_set.updated_by = current_user.user_name
    if params[:poi_mapping_layout_id].blank?
      @data_set.poi_mapping_layout_id = nil
    else
      @data_set.poi_mapping_layout_id = params[:poi_mapping_layout_id]
    end
    begin
     @data_set.save!
     if @data_set.poi_mapping_layout
       @data_set.log_status({:user => current_user.user_name,:remarks => "Added POI Mapping Layout file #{@data_set.poi_mapping_layout.source_filename}"})
     else
       @data_set.log_status({:user => current_user.user_name,:remarks => "Removed POI Mapping Layout file"})
     end
      render :nothing => true
    rescue => e
      render :text => "ERROR: #{e}"
    end
  end

  def save_process_data_element_action_log
    begin
      @data_set = DataSet.find(params[:id])
      log_line = ProcessDataLine.new()
      log_line.data_set_id = @data_set.id
      process_data_element = ProcessDataElement.find(params[:process_data_element_id])
      process_data_sub_event = ProcessDataSubEvent.find(params[:process_data_sub_event_id])
      log_line.process_data_element_id = process_data_element.id
      log_line.process_data_sub_event_id = process_data_sub_event.id
      log_line.cr_reference = params[:cr_reference].strip
      log_line.relation_line_id = process_data_element.process_data_lines.first.id if process_data_element.process_data_lines.size > 0
      log_line.user_id = current_user.id
      log_line.log_forward = params[:log_forward]
      log_line.save!
      render :text => "Event \"#{process_data_sub_event.name}\" added to action log."
    rescue => e
      render :text => "ERROR: #{e}"
    end  
  end

  def sync_with_directory_structure
    @data_set = DataSet.find(params[:id])
    @data_set.updated_by = current_user.user_name
        
    begin     
      @data_set.sync_elements(current_user)
      @data_set.save
      @data_set.reload
      render :partial => "process_data_element", :collection => @data_set.process_data_elements.active
    rescue => e
      if @data_set.process_data_elements.active.empty?
        @data_set.status = DataSet::STATUS_DRAFT
        @data_set.save!
      end
      render :text => "ERROR: #{e}"
    end
  end

  def validate_structure
    @data_set = DataSet.find(params[:id])
    begin
      @data_set.validate_element_file_structure
      render :text => "No problems in elements and file structure detected."
    rescue => e
      render :text => "ERROR: #{e}"
    end
  end

  def element_counter
    @data_set = DataSet.find(params[:id])
    render :text => @data_set.process_data_elements.active.size.to_s
  end

  def render_data_set_pre_selection
    @data_set = DataSet.find(params[:id])
    product_design = ProductDesign.find(params[:product_design_id])
    begin
      @preselection = product_design.data_set_pre_selection(@data_set.id,session[:project_id])
      @candidates = product_design.data_set_candidates(@data_set.id,session[:project_id])
      render :partial => "datasets_for_order", :object => [@data_set,@preselection,@candidates]
    rescue => e
      render :text => "ERROR: #{e}"
    end
  end
  
  def show_process_data_elements
    @data_set = DataSet.find(params[:id])
    @process_data_elements = @data_set.process_data_elements.active
    render :partial => "show_process_data_element", :collection => @process_data_elements.order(:directory)
  end
  
  def download_file
    send_data FileSystemUtil.read(params[:file_path].strip),
      :filename => params[:file_name].strip
  end

  def render_regions_for_input
    @data_set = DataSet.find(params[:data_set_id])
    render :text => @data_set.processable_regions.collect{|r|r.region.region_code}.join(',')
  end

  def request_review
    begin
    @data_set = DataSet.find(params[:id])
    @data_set.validate_element_file_structure
    @data_set.request_review(current_user.id,params[:review_user_id])
      render :text => User.find(params[:review_user_id]).full_name
    rescue => e
      render :text => "ERROR: #{e.message}"
    end
  end

  def review_result
    begin
      @data_set = DataSet.find(params[:id])
      if params[:result] == 'approve'
        @data_set.result_review(true,params[:review_result].strip)
      else
        @data_set.result_review(false,params[:review_result].strip)
      end
      render :text => "success:Review sucessfully submitted."
    rescue => e
      render :text => "error:#{e.backtrace}"
    end
  end

  def show_jobs_modal
    @data_set = DataSet.find(params[:id])
    @conversion_jobs = Conversion.find(params[:conversion_id]).conversion_jobs
    if @conversion_jobs.empty?
      render :text => "No conversion jobs available"
    else
      render :partial => "jobs_modal", :object => @conversion_jobs
    end
  end

private
  def add_remove_group(ds_group_id,action="add")
    ds_group = DataSetGroup.find(ds_group_id)
    if action == 'add'
      ds_ids = (ds_group.data_sets.collect{|ds|ds.id} << @data_set.id).uniq
    else
      ds_ids = (ds_group.data_sets.collect{|ds|ds.id}.delete_if { |key|key == @data_set.id })
    end
    ds_group.sync_lines(ds_ids)
  end

  def sync_parameters_json(data_set,parameters=[],do_not_process_values=[])
    parameters=[] if !parameters
    do_not_process_values=[] if !do_not_process_values
    CoveragesRegion.transaction do
      parameters.each do |k,v|
        coverage_region = data_set.coverage.coverages_regions.find_by_region_id(k)
        coverage_region.parameter_settings = v
        coverage_region.save!
      end
      do_not_process_values.each do |k,v|
        coverage_region = data_set.coverage.coverages_regions.find_by_region_id(k)
        if (v.blank? || v == "false")
          coverage_region.do_not_process = false
        else
          coverage_region.do_not_process = true
        end
        coverage_region.save!
      end
    end
  end

  def perform_production_order_creation(checklog)
    @data_set = DataSet.find(params[:id])
    product_design = ProductDesign.find_correct_version(params[:product_design_id], params[:use_test_versions_yn], checklog)
    @conversion_steps = product_design.modelhash["conversion_steps"]
    # chosen dataset(s) with coverage
    dataset_hash = {}
    params[:datasets].each do |dataset_id,regions|
      dataset_hash[dataset_id] = regions.split(',')
    end
    raise "No valid product_design found" unless product_design
    @matchreport = product_design.create_process_data_matchreport(dataset_hash)
    @matchreport.matches.each do |x|
      if params.keys.index{|pk|pk.match(/^#{x.package_id}_include_checkbox/)}
        x.include = true
      end
      if !params.keys.index{|pk|pk.match(/_include_checkbox/)}
        # if no checkboxes found require all sourcedata from design
        x.include = true
      end
    end

    force_create = []
    count = 0
    @conversion_steps.each do |step|
      if params["force_create_#{count}"]
        force_create << true
      else
        force_create << false
      end
      count = count + 1
    end


    simulator_dakota= params[:simulate_dakota] ? true : false

    cve = params[:conversion_environment_id].blank? ? nil : params[:conversion_environment_id]
    checklog.add_attributes({
        "Product design" => product_design ? "#{product_design.name} (#{product_design.id})" : nil,
        "CV-tool Release" => params[:conversiontool_id].blank? ? nil : "#{Conversiontool.find(params[:conversiontool_id]).code} (#{params[:conversiontool_id]})",
        "CV-tool Bundle" => params[:cvtool_bundle_id].blank? ? nil : "#{CvtoolBundle.find(params[:cvtool_bundle_id]).name} (#{params[:cvtool_bundle_id]})",
        "Use test versions" => params[:use_test_versions_yn],
        "Simulate dakota" => params[:simulate_dakota],
        "Conversion environement (CVE)" => cve
      })
    check_bundle(checklog, params[:conversiontool_id], params[:cvtool_bundle_id])
    if product_design
      production_order = product_design.create_production_order_from_process_data(@data_set.id, dataset_hash,
        params[:conversiontool_id],  params[:cvtool_bundle_id], params[:order_type_id],
        current_user.id,@data_set.status_string, params[:use_test_versions_yn], cve, force_create,
        checklog, @matchreport, params[:project_id], simulator_dakota, params[:test_strategy])
    else
      checklog.add_error_line("No valid product design found, production order not created.")
      production_order = nil
    end
    return production_order
  end

  def check_bundle(checklog, conversiontool_id, cvtool_bundle_id)
    unless conversiontool_id.to_s.blank? || cvtool_bundle_id.to_s.blank?
      conversiontool = Conversiontool.find(conversiontool_id)
      cvtool_bundle = CvtoolBundle.find(cvtool_bundle_id)

      unless conversiontool && cvtool_bundle
        checklog.add_error_line("Specified conversion tool not found in WebMIS.") unless conversiontool
        checklog.add_error_line("Specified tool bundle not found in WebMIS") unless cvtool_bundle
        raise ArgumentError, "Specified bundle data not found in WebMIS."
      end
      bundledir = File.join(ConfigParameter.get('cvtool_release_directory',session[:project_id]), 'Bundles', conversiontool.code, cvtool_bundle.name)
      unless File.directory?(bundledir)
        checklog.add_error_line("Bundle #{conversiontool.code}/#{cvtool_bundle.name} not found on the filesystem.")
        raise ArgumentError, "Specified bundle not found on the filesystem."
      end
    end
  end

  def sort_column
    !params[:sort].blank? ? params[:sort] : "data_sets.id"
  end

  def sort_direction
    !params[:direction].blank? ? params[:direction] : "desc"
  end

  def search
    params[:search] || ""
  end
 
end
