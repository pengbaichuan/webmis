class DataTypesController < ApplicationController
  before_filter :authorize, :except => :api_index
  authorize_resource :except => :api_index

  layout 'webmis'
  helper_method :sort_column, :sort_direction, :search

  def index

    if params[:reset]
      params.delete :search
      params.delete :search_status
      params.delete :inactive
      @inactive = false
      params.delete :reset

      params.delete :sort
      params.delete :direction
    end

    conditions_string_ary = []
    conditions_param_values = []

    if params[:inactive]
      @inactive = true
      conditions_string_ary << "( 1=1 )"
    else
      conditions_string_ary << "( data_types.isactive = 1 )"
    end

    unless params[:search_status].blank?
      status = params[:search_status]
      conditions_string_ary << '(status = ?)'
      conditions_param_values << status
    end

    unless params[:search].blank?
      search = params[:search].strip
      conditions_string_ary << "( data_types.id = ? OR
                                  data_types.name LIKE ? OR
                                  data_types.description LIKE ? OR
                                  data_types.rules LIKE ? OR
                                  fillings.name LIKE ? )"
      5.times do
        conditions_param_values << "%#{search}%"
      end
    end

    conditions = conditions_string_ary.join(" AND ")

    if (sort_column == 'latest_approved')
      @data_types = DataType.includes(:fillings).where([conditions]+conditions_param_values).references(:fillings).sort_by{ |r| r.latest_approved ? r.latest_approved.version : 0 }
      @data_types.reverse! if sort_direction == 'desc'
    else
      @data_types = DataType.includes(:fillings).where([conditions]+conditions_param_values).references(:fillings).order(sort_column + ' ' + sort_direction)
    end

    @data_types = @data_types.paginate(:per_page => 100, :page => params[:page])
    
    if request.xhr?
      render(:partial => "data_type", :collection => @data_types)
    end
  end

  # GET /data_types/1
  # GET /data_types/1.xml
  def show
    @data_type = DataType.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @data_type }
    end
  end

  def approved
    dt = DataType.find(params[:id])
    if dt_version = dt.approved
      @data_type = dt.use_version(dt_version.version)
    end
     respond_to do |format|
      format.html { render :action => 'show' }
      format.json { render :json => @data_type }
    end
  end

  def latest
    @data_type = DataType.find(params[:id])
    @data_type_version = @data_type
    respond_to do |format|
      format.html { render :action => 'show' }
      format.json { render :json => @data_type }
    end
  end

  def handle
    @data_type = DataType.find(params[:id])
    @data_type_version = @data_type
  end

  # # GET /data_types/new
  # # GET /data_types/new.xml
  def new
    @data_type = DataType.new(:status => 'new', :comment => 'manual initial version', :updated_by => session[:username])
    @fillings_for_select = Filling.active
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @data_type }
    end
  end

  # GET /data_types/1/edit
  def edit
    @data_type = DataType.find(params[:id])
    filling_conditions = "(1=1)"
    filling_conditions += " AND id not in (#{@data_type.fillings.collect{|f|f.id}.join(',')})" if !@data_type.fillings.empty?
    @fillings_for_select = Filling.active.where(filling_conditions)
    # as new data types should be easy to enter and probably won't have validation, set the status
    # of new data type to accepted for now. If you edit an existing data type the normal status rules apply.
    if @data_type.status == "new"
      @data_type.status = "approved"
    else
      @data_type.status = "draft"
    end
    @data_type.bug_tracker_id = nil
    @data_type.comment = nil
  end

  # POST /data_types
  # POST /data_types.xml
  def create
    @data_type = DataType.new(strong_params)
    @data_type.updated_by = session[:username]
    @data_type.status = "draft"
    respond_to do |format|
      begin
      DataType.transaction do
        @data_type.save!
        if params[:filling]
          old_filling_ids = @data_type.data_type_fillings.collect{|f|f.filling_id}
          new_filling_ids = params[:filling].values
          @data_type.update_filling(old_filling_ids,new_filling_ids)
        end
        flash[:notice] = 'DataType was successfully created.'
        format.html { redirect_to @data_type, :notice => 'New DataType created.' }
        format.xml  { render :xml => @data_type, :status => :created, :location => @data_type }
      end
      rescue
      @fillings_for_select = Filling.active
      format.html { render :action => "new", :params =>  params}
      format.xml  { render :xml => @data_type.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /data_types/1
  # PUT /data_types/1.xml
  def update
    @data_type = DataType.find(params[:id])
    filling_conditions = "(1=1)"
    filling_conditions += " AND id not in (#{@data_type.fillings.collect{|f|f.id}.join(',')})" if !@data_type.fillings.empty?
    @fillings_for_select = Filling.active.where(filling_conditions)
    # only use the statuses when automatic validation will be used. If not just set the status to approved and
    # be done with it.
    params[:status]=:approved unless params[:data_type][:automatic_validation_yn] == "1"

        if params[:filling]
          old_filling_ids = @data_type.data_type_fillings.collect{|f|f.filling_id}
          new_filling_ids = params[:filling].values
          @data_type.update_filling(old_filling_ids,new_filling_ids)
        else
          # No filling required
          @data_type.data_type_fillings.each do |dtf|
            dtf.destroy
          end
        end


    respond_to do |format|
      if @data_type.update_attributes(strong_params.update(:status => 'draft', :updated_by => session[:username]))
        format.html { redirect_to({ :action => 'handle', :id => @data_type.id }, :notice => 'DataType was successfully updated.') }
        format.xml  { head :ok }
      else
        flash.now[:alert] = "DataType could not be saved."
        format.html { render :action => "edit" }
        format.xml  { render :xml => @data_type.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /data_types/1
  # DELETE /data_types/1.xml
  def destroy
    @data_type = DataType.find(params[:id])
    @data_type.destroy

    respond_to do |format|
      format.html { redirect_to data_types_url, notice: "DataType deleted." }
      format.json { head :no_content }
    end
  end

  # POST /data_types/draft
  def draft
    change_status :draft, "'draft' action"
  end

  # POST /data_types/review
  def review
    change_status :review, "'review' action"
  end

  # POST /data_types/approve
  def approve
    change_status :approved, "'approve' action"
  end

  # POST /data_types/approve_directly
  def approve_directly
    change_status :approved, "'approve' action"
  end

  # POST /data_types/obsolete
  def obsolete
    change_status :obsolete, "'obsolete' action"
  end

  # POST /data_types/revert
  def revert
    data_type = DataType.find(params[:id])
    data_type_original = data_type.latest_approved
    data_type.status = data_type_original.status
    data_type.comment = data_type_original.comment
    data_type.bug_tracker_id = data_type_original.bug_tracker_id
    data_type.name = data_type_original.name
    data_type.description = data_type_original.description
    data_type.automatic_validation_yn = data_type_original.automatic_validation_yn
    data_type.validation_executable = data_type_original.validation_executable
    data_type.updated_by = data_type_original.updated_by
    data_type.schedule_manual = data_type_original.schedule_manual
    data_type.schedule_cores = data_type_original.schedule_cores
    data_type.schedule_memory_base = data_type_original.schedule_memory_base
    data_type.schedule_memory_times_input = data_type_original.schedule_memory_times_input
    data_type.schedule_diskspace_base = data_type_original.schedule_diskspace_base
    data_type.schedule_diskspace_times_input = data_type_original.schedule_diskspace_times_input
    data_type.schedule_allow_virtual = data_type_original.schedule_allow_virtual
    data_type.schedule_exclusive = data_type_original.schedule_exclusive
    #skip the validation as the status transition does not have to be a valid one in this case
    data_type.save(validate: false)

    # the above will increase the version number by one, so revert it
    # note that just adapting the version number will not result in a new version of @data_type
    data_type.version = data_type_original.version
    data_type.save

    # destroy all versions which come after the version we reverted to
    DataTypeVersion.where("data_type_id = :id and version > :version",
      {id: params[:id], version: data_type_original.version}).destroy_all

    respond_to do |format|
      format.html { redirect_to data_types_url, notice: "DataType successfully reverted to #{data_type.version}." }
    end
  end

  def add_filling_to_list
    filling_set = params[:filling_ids].split(",").delete_if {|f| f == "0" }
    @data_type_fillings = Filling.find(filling_set)
    filling_conditions = "(1=1)"
    filling_conditions += " AND id not in (#{filling_set.join(',')})"
    @fillings_for_select = Filling.active.where(filling_conditions)
  end

  def remove_filling_from_list
    filling_set = params[:filling_ids].split(",").delete_if {|f| f == "0" }
    @data_type_fillings = Filling.find(filling_set)
    filling_conditions = "(1=1)"
    filling_conditions += " AND id not in (#{filling_set.join(',')})" if !filling_set.blank?
    @fillings_for_select = Filling.active.where(filling_conditions)
  end

#merged from backoffice
 def api_index
    @dts = DataType.active.collect{|x|x.attributes.delete_if{|x,y|x == 'rules' || x == 'isactive' || x == 'bugtrackerid'}}
    render :json => @dts
  end

  private
  def change_status(status, comment)
    @data_type = DataType.find(params[:id])
    @data_type.status = status
    @data_type.comment = comment
    @data_type.updated_by = session[:username]
    if status == :obsolete
      @data_type.isactive = false
    end
    if @data_type.save
      redirect_to :back, notice: "Status was successfully changed to #{status}."
    else
      fmsg = "Status change to '#{status}' failed."
      emsg = []
      if @data_type.errors.any?
        @data_type.errors.full_messages.each do |msg|
          emsg << msg
        end
      end
      fmsg = fmsg + '<br />' + emsg.join('<br />')
      redirect_to :back, alert: fmsg
    end
  end

  def sort_column
    params[:sort] || "data_types.name"
  end

  def sort_direction
    params[:direction] || "asc"
  end

  def search
    params[:search] || ""
  end
end
