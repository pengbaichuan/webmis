class DefaultContentReleaseNotesController < ApplicationController
  before_filter :authorize
  authorize_resource
  
  layout 'webmis'
  helper_method :sort_column, :sort_direction, :search
    
  # GET /default_content_release_notes
  # GET /default_content_release_notes.xml
  def index
    if params[:reset]
      params.delete :search
      params.delete :reset
      params.delete :c
      
      params.delete :sort
      params.delete :direction
      
    end
    
    if params[:c] && params[:c].to_i > 0
      conditions = "default_content_release_notes.product_category_id = #{params[:c]}"
    else
      conditions = '1=1'
    end    

    if params[:search] && params[:search] != ""
      q = params[:search].strip
      conditions = "#{conditions} and (default_content_release_notes.title LIKE ? or 
                    default_content_release_notes.content LIKE ? or product_categories.abbreviation LIKE ?)"
    end    

    @default_content_release_notes = DefaultContentReleaseNote.joins([:product_category,:user]).where(conditions,
      "%#{q}%","%#{q}%","%#{q}%").order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])
    
    if request.xhr?
       render(:partial => "default_content_release_note", :collection => @default_content_release_notes)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @default_content_release_notes }
      end
    end
  end

  # GET /default_content_release_notes/1
  # GET /default_content_release_notes/1.xml
  def show
    @default_content_release_note = DefaultContentReleaseNote.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @default_content_release_note }
    end
  end

  # GET /default_content_release_notes/new
  # GET /default_content_release_notes/new.xml
  def new
    @default_content_release_note = DefaultContentReleaseNote.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @default_content_release_note }
    end
  end

  # GET /default_content_release_notes/1/edit
  def edit
    @default_content_release_note = DefaultContentReleaseNote.find(params[:id])
  end

  # POST /default_content_release_notes
  # POST /default_content_release_notes.xml
  def create
    @default_content_release_note = DefaultContentReleaseNote.new(strong_params)
    @default_content_release_note.user_id = current_user.id
    @default_content_release_note.rank = 9999

    respond_to do |format|
      if @default_content_release_note.save
        format.html { redirect_to(@default_content_release_note, :notice => 'DefaultContentReleaseNote was successfully created.') }
        format.xml  { render :xml => @default_content_release_note, :status => :created, :location => @default_content_release_note }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @default_content_release_note.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /default_content_release_notes/1
  # PUT /default_content_release_notes/1.xml
  def update
    @default_content_release_note = DefaultContentReleaseNote.find(params[:id])

    respond_to do |format|
      if @default_content_release_note.update_attributes(strong_params)
        format.html { redirect_to(default_content_release_notes_url,:notice => 'DefaultContentReleaseNote was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @default_content_release_note.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /default_content_release_notes/1
  # DELETE /default_content_release_notes/1.xml
  def destroy
    @default_content_release_note = DefaultContentReleaseNote.find(params[:id])
    @default_content_release_note.destroy

    respond_to do |format|
      format.html { redirect_to(default_content_release_notes_url) }
      format.xml  { head :ok }
    end
  end
  
  def ranking
    @default_content_release_notes = DefaultContentReleaseNote.where(:product_category_id => params[:pcid]).order([:rank ,:subrank])
    
    if request.xhr?
      render(:partial => "_ranked", :collection => @default_content_release_notes)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @default_content_release_notes }
      end
    end
  end
  
  def rank_it
    @default_content_release_note = DefaultContentReleaseNote.find(params[:id])
    
    if params[:direction] == 'up'
      subs = DefaultContentReleaseNote.where("product_category_id = #{@default_content_release_note.product_category_id} and rank = #{@default_content_release_note.rank} and subrank is not null").order(:subrank)
      hit = DefaultContentReleaseNote.where("product_category_id = #{@default_content_release_note.product_category_id} and rank < #{@default_content_release_note.rank}").order(:rank).last
      if hit
        mani = DefaultContentReleaseNote.where("product_category_id = #{@default_content_release_note.product_category_id} and rank = #{hit.rank}")
        mani.each do |m|
          m.rank = (@default_content_release_note.rank)
          m.save
        end
      end
      @default_content_release_note.rank = (@default_content_release_note.rank - 1)
      @default_content_release_note.save   
      subs.each do |s|
        s.rank = (@default_content_release_note.rank)
        s.save
      end
    end
    
    if params[:direction] == 'down'
      subs = DefaultContentReleaseNote.where("product_category_id = #{@default_content_release_note.product_category_id} and rank = #{@default_content_release_note.rank} and subrank is not null").order(:subrank)
      hit = DefaultContentReleaseNote.where("product_category_id = #{@default_content_release_note.product_category_id} and rank > #{@default_content_release_note.rank}").order(:rank).first
      if hit
        mani = DefaultContentReleaseNote.where("product_category_id = #{@default_content_release_note.product_category_id} and rank = #{hit.rank}")
        mani.each do |m|
          m.rank = (@default_content_release_note.rank)
          m.save
        end
      end
      @default_content_release_note.rank = (@default_content_release_note.rank)
      @default_content_release_note.save
      subs.each do |s|
        s.rank = (@default_content_release_note.rank)
        s.save
      end      
    end
    
    if params[:direction] == 'right'
      hit = DefaultContentReleaseNote.where("product_category_id = #{@default_content_release_note.product_category_id} and rank < #{@default_content_release_note.rank}").order('rank asc').last
      if hit
        @default_content_release_note.rank = hit.rank
        subhit = DefaultContentReleaseNote.where("product_category_id = #{@default_content_release_note.product_category_id} and rank = #{hit.rank}").order(:subrank).last
        if subhit
          if !subhit.subrank.nil?
            @default_content_release_note.subrank = (subhit.subrank + 1)
          else
            @default_content_release_note.subrank = 1
          end
        end        
      end
      @default_content_release_note.subrank = 1 if @default_content_release_note.subrank.nil?
      @default_content_release_note.save
    end
    
    if params[:direction] == 'left'
      subs = DefaultContentReleaseNote.where("product_category_id = #{@default_content_release_note.product_category_id} and rank = #{@default_content_release_note.rank} and subrank is not null").order(:subrank)
      @default_content_release_note.subrank = nil
      hit = DefaultContentReleaseNote.where("product_category_id = #{@default_content_release_note.product_category_id} and rank > #{@default_content_release_note.rank}").order('rank asc').first
      if hit
        mani = DefaultContentReleaseNote.where("product_category_id = #{@default_content_release_note.product_category_id} and rank = #{hit.rank}")
        mani.each do |m|
          m.rank = (@default_content_release_note.rank)
          m.save
        end
      end
      @default_content_release_note.rank = (@default_content_release_note.rank + 1)
      @default_content_release_note.save
      sr = 0
      subs.each do |s|
        s.subrank = (sr + 1)
        s.save
        sr = (sr + 1)
      end      
    end
    
    r = 0
    DefaultContentReleaseNote.where(:product_category_id => @default_content_release_note.product_category_id).order('rank, subrank asc').each do |s|
      if s.subrank.nil?
        r = r + 1
        s.rank = r 
      else
        s.rank = r
      end
      s.save  
    end
    
    DefaultContentReleaseNote.where(:product_category_id => @default_content_release_note.product_category_id).each do |z|    
      if z.rank.nil?
        z.rank = 9999
      end
      z.save
    end

    @default_content_release_notes = DefaultContentReleaseNote.where(:product_category_id => @default_content_release_note.product_category_id).order('rank,subrank asc')
    render :layout => false,:collection => @default_content_release_notes
  end
  
  private  
    def sort_column  
      params[:sort] || "default_content_release_notes.product_category_id"  
    end  

    def sort_direction  
      params[:direction] || "desc"  
    end

    def search
      params[:search] || ""
    end  
end
