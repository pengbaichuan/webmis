class DesignTagsController < ApplicationController
  # GET /design_tags
  # GET /design_tags.json
  
  before_filter :authorize
  authorize_resource
  layout "webmis"
  
  helper_method :sort_column, :sort_direction, :search
  
  def index
    conditions = '1=1'
    if params[:reset]
      params.delete :search
      params.delete :reset
      
      params[:sort] = 'design_tags.id'
      params[:direction] = 'desc'
    end
    
    conditions_string_ary = []
    conditions_param_values = []
    
    unless params[:search].blank?
      conditions_string_ary << " and (design_tags.name LIKE ?
              or design_tags.description LIKE ?)"
      2.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
    end
    
    conditions = conditions + conditions_string_ary.join(" ")    
    
    if params[:tree]
      conditions = '1=1'
      if !params[:tree].blank?
        conditions = 'id = ' + params[:tree]
      end
      @design_tags = DesignTag.suppliers.where(conditions).order('parent_id, name, description')
    else
      @design_tags = DesignTag.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])
    end
    
    if request.xhr?
      render(:partial => "design_tag", :collection => @design_tags)
    else  
      respond_to do |format|   
        if params[:tree]
          format.html { render action: "index_tree" } 
        else
          format.html { render action: "index" } 
        end
        format.json { render json: @design_tags }
      end
    end
  end

  # GET /design_tags/1
  # GET /design_tags/1.json
  def show
    @design_tag = DesignTag.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @design_tag }
    end
  end

  # GET /design_tags/new
  # GET /design_tags/new.json
  def new
    @design_tag = DesignTag.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @design_tag }
    end
  end

  # GET /design_tags/1/edit
  def edit
    @design_tag = DesignTag.find(params[:id])
    @design_tag.parent_id = @design_tag.parent_id.to_s
  end

  # POST /design_tags
  # POST /design_tags.json
  def create
    @design_tag = DesignTag.new(design_tag_params)

    respond_to do |format|
      if @design_tag.save
        format.html { redirect_to @design_tag, notice: 'Design tag was successfully created.' }
        format.json { render json: @design_tag, status: :created, location: @design_tag }
      else
        format.html { render action: "new" }
        format.json { render json: @design_tag.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /design_tags/1
  # PUT /design_tags/1.json
  def update
    @design_tag = DesignTag.find(params[:id])

    respond_to do |format|
      if @design_tag.update_attributes(design_tag_params)
        format.html { redirect_to @design_tag, notice: 'Design tag was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @design_tag.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /design_tags/1
  # DELETE /design_tags/1.json
  def destroy
    @design_tag = DesignTag.find(params[:id])
    @design_tag.destroy

    respond_to do |format|
      format.html { redirect_to design_tags_url }
      format.json { head :no_content }
    end
  end
  
  private

  def design_tag_params
    params.require(:design_tag).permit!
  end

  def sort_column  
    params[:sort] || "id"  
  end  
    
  def sort_direction  
    params[:direction] || "desc"  
  end
  
  def search
    params[:search] || ""
  end  
end
