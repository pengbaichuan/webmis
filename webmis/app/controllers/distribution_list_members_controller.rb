class DistributionListMembersController < ApplicationController
  before_filter :authorize
  authorize_resource
    layout "webmis"
  # GET /distribution_list_members
  # GET /distribution_list_members.xml
  def index
    @distribution_list_members = DistributionListMember.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @distribution_list_members }
    end
  end

  # GET /distribution_list_members/1
  # GET /distribution_list_members/1.xml
  def show
    @distribution_list_member = DistributionListMember.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @distribution_list_member }
    end
  end

  # GET /distribution_list_members/new
  # GET /distribution_list_members/new.xml
  def new
    @distribution_list_member = DistributionListMember.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @distribution_list_member }
    end
  end

  # GET /distribution_list_members/1/edit
  def edit
    @distribution_list_member = DistributionListMember.find(params[:id])
    @companies = Company.joins("JOIN accounts_contacts on accounts_contacts.account_id = accounts.id").order(:name)
    @company = ExternalContact.find(@distribution_list_member.external_contact_id)
    @contacts = Company.find(@company.company.id).external_contacts
    @amount = (0..20).to_a
  end

  # POST /distribution_list_members
  # POST /distribution_list_members.xml
  def create
    @distribution_list_member = DistributionListMember.new(strong_params)
    @distribution_list_member.distribution_list_id = params[:distribution_list_member][:distribution_list_id]

    respond_to do |format|
      if @distribution_list_member.save
        format.html { redirect_to(:controller => "distribution_lists", :action => "edit", :id => params[:distribution_list_member][:distribution_list_id] ,:notice => 'DistributionListMember was successfully updated.') }
        format.xml  { render :xml => @distribution_list_member, :status => :created, :location => @distribution_list_member }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @distribution_list_member.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /distribution_list_members/1
  # PUT /distribution_list_members/1.xml
  def update
    @distribution_list_member = DistributionListMember.find(params[:id])
    @back = @distribution_list_member.distribution_list_id.to_s
     @distribution_list_member.external_contact_id = params[:distribution_list_member]["external_contact_id_#{@distribution_list_member.id.to_s}"]
    params[:distribution_list_member].delete("external_contact_id_#{@distribution_list_member.id.to_s}")

    respond_to do |format|
      if @distribution_list_member.update_attributes(strong_params)
        format.html { redirect_to(:controller => "distribution_lists", :action => "edit", :id => @back ,:notice => 'DistributionListMember was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @distribution_list_member.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /distribution_list_members/1
  # DELETE /distribution_list_members/1.xml
  def destroy
    @distribution_list_member = DistributionListMember.find(params[:id])
    @back = @distribution_list_member.distribution_list_id
    @distribution_list_member.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => "distribution_lists", :action => "edit", :id => @back ,:notice => 'DistributionListMember was successfully updated.') }
      format.xml  { head :ok }
    end
  end
  
  def select_contact
   @contacts = Company.find(params[:company_id]).external_contacts
   render :partial => "contacts_select"
  end
  
  def select_hard_copies
    @distribution_list_member = DistributionListMember.find(params[:id])
    @amount = (0..20).to_a
    if params["company_id"] == "1"
      render :partial => "hardcopy_fields"      
    end
  end
  
end
