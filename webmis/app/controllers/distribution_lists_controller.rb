class DistributionListsController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"
  
  # GET /distribution_lists
  # GET /distribution_lists.xml
  def index
    if params[:reset]
      params.delete :search
      params.delete :reset          
    end
    
    conditions = "(distribution_lists.deleted_yn is null or distribution_lists.deleted_yn = 0)"
    
    if params[:search] && params[:search] != ""
      q = params[:search].strip
      conditions = "#{conditions} and (distribution_lists.name LIKE ? or 
                    distribution_lists.remarks LIKE ? or product_categories.abbreviation LIKE ?)"
    end 
        
    @distribution_lists = DistributionList.joins(:product_category).where(conditions,"%#{q}%","%#{q}%","%#{q}%")    
    
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @distribution_lists }
    end
  end

  # GET /distribution_lists/1
  # GET /distribution_lists/1.xml
  def show
    @distribution_list = DistributionList.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @distribution_list }
    end
  end

  # GET /distribution_lists/new
  # GET /distribution_lists/new.xml
  def new
    @distribution_list = DistributionList.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @distribution_list }
    end
  end

  # GET /distribution_lists/1/edit
  def edit
    @distribution_list = DistributionList.find(params[:id])
    @distr_list_id = params[:id]
    @new_distribution_list_member = DistributionListMember.new
    @new_distribution_list_member.electronic_delivery_yn = true
    @members = @distribution_list.distribution_list_members
    conditions = "accounts.deleted is null or accounts.deleted = 0"
    @companies = Company.joins("JOIN accounts_contacts on accounts_contacts.account_id = accounts.id").where(conditions).order("name")
    @amount = (0..20).to_a
    @contacts = ExternalContact.find_all_dist("first")
  end

  # POST /distribution_lists
  # POST /distribution_lists.xml
  def create
    @distribution_list = DistributionList.new(strong_params)
    

    respond_to do |format|
      if @distribution_list.save
        format.html { redirect_to(:action => "edit", :id => @distribution_list.id) }
        format.xml  { render :xml => @distribution_list, :status => :created, :location => @distribution_list }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @distribution_list.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /distribution_lists/1
  # PUT /distribution_lists/1.xml
  def update
    @distribution_list = DistributionList.find(params[:id])
    
    @distribution_list_member = DistributionListMember.where(:distribution_list_id => params[:id]).first
    
    @members = DistributionListMember.where(:distribution_list_id => params[:id])
    conditions = "accounts.deleted is null or accounts.deleted = 0"
    @companies = Company.joins("JOIN accounts_contacts on accounts_contacts.account_id = accounts.id").where(conditions).order(:name)
    @amount = (0..20).to_a
    @distribution_list_member.external_contact_id = params[:distribution_list_member]["external_contact_id_#{@distribution_list_member.id.to_s}"] if params[:distribution_list_member] && !params[:new_distribution_list_member]
    params[:distribution_list_member].delete("external_contact_id_#{@distribution_list_member.id.to_s}") if params[:distribution_list_member] && !params[:new_distribution_list_member]
    @distribution_list_member.update_attributes(params_distribution_list_member) if params[:distribution_list_member] && !params[:new_distribution_list_member]
    
    if params["new_distribution_list_member"]
      
      @new_distribution_list_member = DistributionListMember.new(params_new_distribution_list_member)
      @new_distribution_list_member.distribution_list_id = @distribution_list.id
      @new_distribution_list_member.save
    end
    
    respond_to do |format|
      if (!params[:distribution_list].blank? && @distribution_list.update_attributes(strong_params)) || params[:distribution_list].blank?
        #format.html { redirect_to(@distribution_list, :notice => 'DistributionList was successfully updated.') }
        flash[:notice] = 'DistributionList was successfully updated.'
        if params["members"] || params["new_members"]
          format.html { redirect_to(:action => "edit") }
        else
          format.html { redirect_to(@distribution_list, :notice => 'DistributionList was successfully updated.') }
        end
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @distribution_list.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /distribution_lists/1
  # DELETE /distribution_lists/1.xml
  def destroy
    @distribution_list = DistributionList.find(params[:id])
    @distribution_list.destroy

    respond_to do |format|
      format.html { redirect_to(distribution_lists_url) }
      format.xml  { head :ok }
    end
  end
  
  def select_contact
   @contacts = Company.find(params[:company_id]).external_contacts
   @distribution_list_member = DistributionListMember.find(params[:mid])
   render :partial => "contacts_select"
  end
  
  def select_hard_copies
    @distribution_list_member = DistributionListMember.find(params[:id])
    
      @amount = (0..20).to_a
    if params["company_id"] == "1"
      render :partial => "hardcopy_fields"
    else
       
    end

  end
    
  def select_new_hard_copies
    #@new_distribution_list_member = DistributionListMember.find(params[:id])
    @amount = (0..20).to_a

      render :partial => "new_hardcopy_fields"
   

  end
  
  def delete_y
    @distribution_list = DistributionList.find(params[:id])
    @distribution_list.deleted_yn = 1
    @distribution_list.save
    
        respond_to do |format|
      format.html { redirect_to(distribution_lists_url) }
      format.xml  { head :ok }
    end
  end

private
  def params_distribution_list_member
    attrs = DistributionListMember.new.attributes.keys
    params.require(:distribution_list_member).permit(attrs)
  end
  
  def params_new_distribution_list_member
    attrs = DistributionListMember.new.attributes.keys
    params.require(:new_distribution_list_member).permit(attrs)
  end
  
end
