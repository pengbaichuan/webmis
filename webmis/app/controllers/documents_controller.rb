class DocumentsController < ApplicationController
  before_filter :authorize
  authorize_resource
  # GET /documents
  # GET /documents.xml
  layout 'webmis'
  helper_method :sort_column, :sort_direction, :search
  
  def index
    if params[:reset]
      params.delete :search
      params.delete :reset

      params[:sort] = 'documents.id'
      params[:direction] = 'desc'
    end    
    conditions_string_ary = []
    conditions_param_values = []
    
    unless params[:search].blank?
      q = params[:search].strip
      conditions_string_ary << "(documents.id = ?
                               OR documents.filename LIKE ?
                               OR documents.description LIKE ?
                               OR documents.sync_filepath LIKE ?
                               OR documents.document_object_id LIKE ?
                               OR documents.package_directory LIKE ?)"
      conditions_param_values << q.to_i # only for id
      5.times do
        conditions_param_values << "%#{q}%"
      end
    end

    if !column_search_query.empty?
      conditions_string_ary << "#{column_search_query['query']}"
      conditions_param_values +=  column_search_query["values"]
    end
    conditions = conditions_string_ary.join( ' AND ' )

    @documents = Document.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])
    generate_id_sequence_cookie(@documents,params[:page])
    
    if request.xhr?
      render(:partial => "document", :collection => @documents)
    else    
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @documents }
      end
    end
  end

  # GET /documents/1
  # GET /documents/1.xml
  def show
    @document = Document.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @document }
    end
  end

  # GET /documents/new
  # GET /documents/new.xml
  def new
    @document = Document.new
    @document.production_order_id = params[:production_order_id]

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @document }
    end
  end

  # GET /documents/1/edit
  def edit
    @document = Document.find(params[:id])
  end

  # POST /documents
  # POST /documents.xml
  def create
    #puts params[:document]
    
    @document = Document.new(strong_params)
    @document.user_id = current_user.id
    @docfound = Document.where("filename = '#{@document.filename}'").select('id,filename').first if !@document.filename.blank?
    @desc  = params[:document][:description]
    
    if @docfound
      @document = Document.find(@docfound.id)
      #@docfound = true
    end

    respond_to do |format|
      if @docfound
        dv = DocumentVersion.where("document_id = #{@document.id} and version = #{@document.version}").select('id,document_id,filename').first
        if params[:returncontroller] && params[:returncontroller].to_s.size > 0
         format.html { redirect_to(:controller => params[:returncontroller].to_s, :action => params[:returnaction],:id => params[:returnid], :documentversionid => dv.id)}
        else
          flash[:error] = "Document with filename #{@docfound.filename} was already found (id = #{@docfound.id.to_s}), rename document or update existing document"
          @document = Document.new
          @document.description = @desc
          
          format.html { render :action => "new" }
        end
      elsif @document.save

        if params[:aid] && params[:aid].size > 0
          
         
          asset = Asset.find(params[:aid])
          asset.document_id =  @document.id
          asset.save
        end

        flash[:notice] = 'Document was successfully created.'
        
        if params[:aid]  && params[:aid].size > 0
          @new_user = User.find(@document.user_id)
          new_user_str = @new_user.first_name.to_s + " " + @new_user.last_name.to_s
          if Asset.find(params[:aid]).requested_by != new_user_str
            Postoffice.notify_document_overwrite(@new_user,params[:aid],@document.id).deliver
          end
          format.html { redirect_to(:controller => 'assets', :action => 'index') }
        elsif params[:returncontroller] && params[:returncontroller].to_s.size > 0
          
          if params[:returncontroller] == "product_release_notes"
            @product_release_note = ProductReleaseNote.find(params[:returnid])
            @product_release_note.status = 0 # Draft
            @product_release_note.save
          end

          if params[:returncontroller].to_s == 'products'
            id= Includeddocument.new
            id.product_id = params[:returnid].to_s
            id.document_id = @document.id
            id.product.last_change = "Document #{@document.id} - #{@document.filename} was added"
            id.product.save
            id.save
          end
          
          if params[:returncontroller].to_s == 'productsets'
            id= Includedproductsetdocument.new
            id.productset_id = params[:returnid].to_s
            id.document_id = @document.id
            id.productset.setchanges = "Document #{@document.id.to_s} - #{@document.filename} was added"
            id.productset.save
            id.save
          end
          format.html { redirect_to(:controller => params[:returncontroller].to_s, :action => params[:returnaction].to_s, :id => params[:returnid].to_s, :documentid => @document.id)}

        else
          format.html { redirect_to(@document) }
        end
        format.xml  { render :xml => @document, :status => :created, :location => @document }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @document.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /documents/1
  # PUT /documents/1.xml
  def update
    result = false
    updatepds = false
    Document.transaction do
      
      @document = Document.find(params[:id])
      @old_document_version = @document.get_version(@document.version)
      @document.user_id = current_user.id
      result = @document.update_attributes(strong_params)
      
      if params[:returncontroller] == "product_release_notes"
        @product_release_note = ProductReleaseNote.find(params[:returnid])
        @product_release_note.status = 0 # Draft
        @product_release_note.save
      end
      
      if params[:returncontroller].to_s == 'products'

        id = Includeddocument.find(params[:includeddocumentid].to_s)
        id.product_id = params[:returnid].to_s
        id.document_id = @document.id
        id.product.last_change = "Document #{@document.document_id} - #{@document.filename} was updated"
        id.product.save
        id.save
      elsif params[:returncontroller].to_s == 'productsets'
        id= Includedproductsetdocument.find(params[:includeddocumentid].to_s)
        id.productset_id = params[:returnid].to_s
        id.document_id = @document.id
        id.productset.setchanges = "Document #{@document.document_id} - #{@document.filename} was updated"
        id.productset.save
        id.save
      else 
        updatepds = false
      end
    end
    respond_to do |format|
      if result
        #check for related products/productsets
        flash[:notice] = 'Document was successfully updated.'
        if params[:aid] && params[:aid].size > 0
          @new_user = User.find(@document.user_id)
          new_user_str = @new_user.first_name.to_s + " " + @new_user.last_name.to_s
          if Asset.find(params[:aid]).requested_by != new_user_str
            Postoffice.notify_document_overwrite(@new_user,params[:aid],@document.id).deliver
          end          
          format.html { redirect_to(:controller => 'assets', :action => 'index') }
        elsif params[:returncontroller] && params[:returncontroller].to_s.size > 0
          format.html { redirect_to(:controller => params[:returncontroller].to_s, :action => params[:returnaction].to_s, :id => params[:returnid].to_s, :documentid => @document.id)}
        else
          if updatepds
            format.html { render :action => "pds_update_confirm"}
          else
            format.html { redirect_to(@document) }
          end
        end
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @document.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update_pds
    #exchange document id's
    #update version
    dv = DocumentVersion.where("id = #{params[:new_doc_id]}").select('filename,document_id').first
    # get relevant parameters
    products = []
    objects = []
    params.each{|x,y| objects << x if x.match /^PRODUCT/}

    objects.each do |obj|
      Includeddocument.transaction do
        id  = obj.to_s.scan(/-(.*)/).to_s
        if obj.match(/PRODUCT-/)
          ip = Includeddocument.find(id)

          ip.document_id = params[:new_doc_id].to_i
          ip.save
          products << ip.product.volumeid
          ip.product.last_change = "Document #{dv.document_id} - #{dv.filename} was updated"
          ip.product.save
        else
          ip = Includedproductsetdocument.find(id)
          ip.document_id = params[:new_doc_id].to_i
          ip.save
          products << ip.productset.setcode
          ip.productset.setchanges = "Document #{dv.document_id} - #{dv.filename} was updated"
          ip.productset.save
        end
      end
      
    end
    flash[:notice] = "Product(set)s " + products.inspect + " succesfully updated with new documents"
    redirect_to :action => 'index'
  end

  # DELETE /documents/1
  # DELETE /documents/1.xml
  def delete
    @document = Document.find(params[:id])
    @document.destroy

    respond_to do |format|
      if params[:returncontroller] && params[:returncontroller].to_s.size > 0
        flash[:notice] = "Document is permanetly deleted"
        logger.info 'DOCUMENT REMOVED: by ' + current_user.first_name + " " + current_user.last_name
        logger.info '--------------------------------------------'
        if params[:returncontroller] == "product_release_notes"
          @product_release_note = ProductReleaseNote.find(params[:returnid])
          @product_release_note.status = 0 # Draft
          @product_release_note.save
        end
        format.html { redirect_to(:controller => params[:returncontroller].to_s, :action => params[:returnaction].to_s, :id => params[:returnid].to_s) }
      else
        format.html { redirect_to(documents_url) }
      end
      format.xml  { head :ok }
    end
  end

  def download
    unless !params[:ver].blank?
      version = Document.find(params[:id]).get_latest_version.version
    else
      version = params[:ver]
    end
    @binary_data = DocumentVersion.where("document_id = #{params[:id]} and version = #{version}").first
    @binary = @binary_data.binary_data

    filename = 'unknown'
    if @binary_data.content_type == "application/pdf"
      if !@binary_data.filename.blank?
        filename = @binary_data.filename
      end
      send_data @binary, :disposition => "inline",:filename => "v#{@binary_data.version}_#{filename}", :type => "application/pdf"
    else
      if !@binary_data.filename.blank?
        filename = @binary_data.filename.gsub(']','').gsub('[','').gsub('"',"")
      end
      #workaround for wrong import documents
      send_data(@binary,  :filename=> "#{filename}")
    end
  end

  def select
    p = nil
    if params[:product_id]
      Product.transaction do
        flash[:notice] = "Document was succesfully added to product"
        p = Product.find(params[:product_id])
        ip = Includeddocument.new
        #ip.productset_id = p.id
        doc = Document.find(params[:id])
        ip.document_id = doc.id
        ip.product_id = params[:product_id]
        
        p.save
        #p.includedproducts << ip
        ip.save
      end
    elsif params[:productset_id]
      Productset.transaction do
        flash[:notice] = "Document was succesfully added to productset"
        p = Productset.find(params[:productset_id])
        ip = Includedproductsetdocument.new
        #ip.productset_id = p.id
        doc = Document.find(params[:id])
        ip.document_id = doc.id
        ip.productset_id = params[:productset_id]
        p.addchanges = "\nAdded document to productset"
        p.setchanges = "\nAdded document #{doc.filename} to productset"
        p.save
        #p.includedproducts << ip
        ip.save
      end
    end


    if params[:productset_id]
      redirect_to :controller => 'productsets' , :action => 'edit', :id => params[:productset_id], :anchor => "footer"
    elsif
      redirect_to :controller => 'products', :action => 'edit', :id => params[:product_id], :anchor => "footer"
    end

  end

  private
  # not used and probably not valid anymore
  # def choose_layout
  #   if params[:production_order_id]
  #     "general"
  #   else
  #     "webmis"
  #   end
  # end
  
  def sort_column  
    params[:sort] || "documents.id"  
  end  
  
  def sort_direction  
    params[:direction] || "desc"  
  end
  
  def search
    params[:search] || ""
  end  

  
end
