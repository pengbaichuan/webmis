class ErrorsController < ApplicationController

  # 404
  def not_found
   @exception = env["action_dispatch.exception"];

   respond_to do |format|
      format.html { render status: 404 }
      format.any  { render text: "404 Not Found", status: 404 }
    end
  end

  # 500
  def internal_server_error
    details = []
    fulltrace = []
    @exception = env["action_dispatch.exception"]
    @dispatch = ActionDispatch::ExceptionWrapper.new(env, @exception)

    @subject = "#{@dispatch.env["ORIGINAL_FULLPATH"]}: "
    @subject += @dispatch.env["action_dispatch.exception"].message.to_s.gsub('`',"'")

    details << @dispatch.application_trace
    details << @dispatch.exception
    
    @details = details.map{|x| "^^ " + x.to_s}.join("\n")
    @trace = "^^Full trace:\n"
    
    @dispatch.full_trace.each do |ft|
      # Filter unwanted lines
      fulltrace << "^^" + ft.strip if !ft.to_s.match('actionpack|activesupport|journey|rack|railties|haml|rubies')
    end
    @trace += fulltrace.join("^^\n").strip
    @trace += "\n\n^^Request URL:"
    @trace += @dispatch.env["REQUEST_URI"]

    @trace += "\n\n^^Parameter(s):\n"
    @dispatch.env["action_dispatch.request.request_parameters"].each do |k,v|
      if v.class.name == 'ActiveSupport::HashWithIndifferentAccess'
        @trace += "^^#{k} => \n"
        @trace += "^^{\n"
        v.each do |hk,hv|
          @trace += "^^\s\s\s#{hk} => #{hv}\n"
        end
        @trace += "^^}\n"
      else
        @trace += "^^#{k} => #{v}\n"
      end
    end
    @current_user ||= User.find(session[:user_id]) if session[:user_id]

    Rails.logger.info "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
    Rails.logger.info "^^EXCEPTION at #{Time.now.strftime('%m-%d-%Y %I:%M %p')}"
    Rails.logger.info "^^Victim: #{@current_user.user_name}"
    Rails.logger.info "^^Details: #{@details}"
    Rails.logger.info "#{@trace}"
    Rails.logger.info "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"

    Postoffice.internal_server_error(@subject,@details,@trace,@current_user).deliver

   respond_to do |format|
      format.html { render status: 500 }
      format.any  { render text: "500 Internal server error", status: 500 }
    end
  end

end
