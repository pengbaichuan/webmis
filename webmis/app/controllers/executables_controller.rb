class ExecutablesController < ApplicationController
  before_filter :authorize, :except => [:synchronize_tool, :synchronize_bundle]
  authorize_resource :except => [:synchronize_tool, :synchronize_bundle]

  layout "webmis"
  helper_method :sort_column, :sort_direction, :search

  def index
    conditions = '1=1'
    if params[:reset]
      params.delete :search
      params.delete :search_status
      params.delete :obsolete
      params.delete :reset

      params[:sort] = 'name'
      params[:direction] = 'desc'
    end

    conditions_string_ary = []
    conditions_param_values = []

    unless params[:search].blank?
      conditions_string_ary << " and (executables.id =#{params[:search].strip.to_i} OR
                                 executables.name LIKE ? OR
                                 executables.description LIKE ? OR
                                 executables.input LIKE ? OR
                                 executables.output LIKE ?)"
      4.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
    end


    include_obsolete = params[:obsolete]
    status = params[:search_status]
    if status.blank?
      conditions_string_ary << ' and (status is null or status !="obsolete")' unless include_obsolete
    else
      conditions_string_ary << ' and (status=?)'
      conditions_param_values << status
      include_obsolete ||= (status == 'obsolete')
    end

    params[:obsolete] = include_obsolete
    conditions = conditions + conditions_string_ary.join(" ")

    if (sort_column == 'latest_approved')
      @executables = Executable.where([conditions]+conditions_param_values).sort_by{ |r| r.latest_approved ? r.latest_approved.version : 0 }
      @executables.reverse! if sort_direction == 'desc'
    else
      @executables = Executable.where([conditions]+conditions_param_values).order(sort_column + ' ' + sort_direction)
    end
    @executables = @executables.paginate(:per_page => 50, :page => params[:page])
    @current_user = current_user

    if request.xhr?
      flash.keep[:notice]
      render(:partial => "executable", :collection => @executables)
    end
  end

  # GET /executables/1
  # GET /executables/1.json
  def show
    @executable = Executable.find(params[:id])
    @executable_version = @executable
    show_executable
  end

  def approved
    @executable = Executable.find(params[:id])
    @executable_version = @executable.use_latest_approved
    show_executable
  end

  private
  def show_executable
    @executable_version.input = JSON.parse(!@executable_version.input.to_s.blank? ? @executable_version.input : "[]")
    @executable_version.output = JSON.parse(!@executable_version.output.to_s.blank? ? @executable_version.output : "[]")
    if @executable_version.parameters.to_s.size > 1
      # paramshash = JSON.parse(@executable_version.parameters)
      #paramshash["parameters"].each do |param|
      #  param["datatype"] = ""
      #end
      #@executable_version.parameters = paramshash["parameters"]
    end

    respond_to do |format|
      format.html { render :action => 'show' } # show.html.erb
      format.json { render :json => @executable }
    end
  end
  public

  def handle
    @executable = Executable.find(params[:id])
    @executable.input = JSON.parse(!@executable.input.to_s.blank? ? @executable.input : "[]")
    @executable.output = JSON.parse(!@executable.output.to_s.blank? ? @executable.output : "[]")
    @executable_version = @executable.versions.last
    @executable_version.input = JSON.parse(!@executable_version.input.to_s.blank? ? @executable_version.input : "[]")
    @executable_version.output = JSON.parse(!@executable_version.output.to_s.blank? ? @executable_version.output : "[]")
    @current_user = current_user
  end

  def find_executable
    @executable = Executable.find_by_name(params[:id])
    if @executable
      @executable.input = JSON.parse(@executable.input)
      @executable.output = JSON.parse(@executable.output)
    end
    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @executable }
    end
  end


  # # GET /executables/new
  # # GET /executables/new.json
  # def new
  #   @executable = Executable.new
  #   @executable.input = "[]"
  #   @executable.output = "[]"

  #   respond_to do |format|
  #     format.html # new.html.erb
  #     format.json { render json: @executable }
  #   end
  # end

  # GET /executables/1/edit
  def edit
    @executable = Executable.find(params[:id])
    # the default new status of an edit should always be ':draft'
    @executable.status = :draft
  end

  # POST /executables
  # POST /executables.json
  def create
    # for new executables, first create one with status new and then edit it as a regular executable
    # note that we skip the traditional 'new' method here
    @executable = Executable.new(:status => 'new', :comment => 'manual initial version', :updated_by => session[:username])
    @executable.input = "[]"
    @executable.output = "[]"
    respond_to do |format|
      if @executable.save(:validate => false)
        format.html { redirect_to( { :action => 'edit', :id => @executable.id }, :notice => 'New executable created.') }
        format.json { render :json => @executable, :status => :created, :location => @executable }
      else
        format.html { redirect_to executables_url, :alert => 'Could not create new executable.' }
        format.json { render :json => @executable.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /executables/1
  # PUT /executables/1.json
  def update
    @executable = Executable.find(params[:id])
    @executable.input = params_to_datatypes_json(params["in_datatypes"])
    @executable.output = params_to_datatypes_json(params["out_datatypes"])

    respond_to do |format|
      if @executable.update_attributes(executable_params.update(:status => 'draft', :updated_by => session[:username]))
        format.html { redirect_to({ :action => 'handle', :id => @executable.id } , :notice => 'Executable was successfully updated.') }
        format.json { head :no_content }
      else
        flash.now[:alert] = "Executable could not be saved."
        format.html { render :action => "edit" }
        format.json { render :json => @executable.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /executables/1
  # DELETE /executables/1.json
  def destroy
    @executable = Executable.find(params[:id])
    @executable.destroy

    respond_to do |format|
      format.html { redirect_to executables_url, :notice => "Executable deleted." }
      format.json { head :no_content }
    end
  end

  def add_datatype
    puts params.inspect
    @dt = DataType.new
    @dt.name = params[:datatype_name]
    @dt.description = params[:datatype_description]
    @dt.isactive = 1
    @dt.save
    @dt.reload
    respond_to do |format|
      format.json { render :json => @dt.to_json, :status => :created, :location => @dt }
      format.xml { render :xml => @dt.to_xml, :status => :created, :location => @dt }
    end
  end

  # POST /executables/draft
  def draft
    change_status :draft, "'draft' action"
  end

  # POST /executables/review
  def review
    change_status :review, "'review' action"
  end

  # POST /executables/approve
  def approve
    change_status :approved, "'approve' action"
  end

  # POST /executables/approve_directly
  # distinction between approve and approve_directly is purely to distinguish between 
  def approve_directly
    change_status :approved, "'approve' action"
  end

  # POST /executables/obsolete
  def obsolete
    change_status :obsolete, "'obsolete' action"
  end

  # POST /executables/revert
  def revert
    executable = Executable.find(params[:id])
    executable_original = executable.latest_approved
    executable.status = executable_original.status
    executable.comment = executable_original.comment
    executable.bug_tracker_id = executable_original.bug_tracker_id
    executable.name = executable_original.name
    executable.description = executable_original.description
    executable.input = executable_original.input
    executable.output = executable_original.output
    executable.parameters = executable_original.parameters
    executable.updated_by = executable_original.updated_by
    executable.schedule_manual = executable_original.schedule_manual
    executable.schedule_cores = executable_original.schedule_cores
    executable.schedule_memory_base = executable_original.schedule_memory_base
    executable.schedule_memory_times_input = executable_original.schedule_memory_times_input
    executable.schedule_diskspace_base = executable_original.schedule_diskspace_base
    executable.schedule_diskspace_times_input = executable_original.schedule_diskspace_times_input
    executable.schedule_allow_virtual = executable_original.schedule_allow_virtual
    executable.schedule_exclusive = executable_original.schedule_exclusive
    #skip the validation as the status transition does not have to be a valid one in this case
    executable.save(:validate => false)

    # the above will increase the version number by one, so revert it
    # note that just adapting the version number will not result in a new version of @executable
    executable.version = executable_original.version
    executable.save

    # destroy all versions which come after the version we reverted to
    ExecutableVersion.where("executable_id = :id and version > :version",
                                 {id: params[:id], :version => executable_original.version}).destroy_all

    respond_to do |format|
      format.html { redirect_to executables_url, :notice => "Executable successfully reverted to #{executable.version}." }
    end
  end

#merged from backoffice project
  # GET /executables/synchronize_tool_stub
  # internal helper method to interactively call the synchronize_tool method
  # not intended for external use. May be removed after development
  def synchronize_tool_stub
    # dummy method
  end

  # GET /executables/synchronize_bundle_stub
  # internal helper method to interactively call the synchronize_bundle method
  # not intended for external use. May be removed after development
  def synchronize_bundle_stub
    # dummy method
  end

  # POST /executables/synchronize_tool
  def synchronize_tool
    tool_name = params[:name]
    tool_release = params[:release]
    contents = params[:contents][:file].read
    infolog("SYNC INFO: Processing synchronize_tool(name: #{tool_name}, release: #{tool_release}, contents: #{contents}")

    if tool_name.blank? || tool_release.blank? || contents.blank?
      message = "The request failed."
      message += " Name parameter missing or empty." if tool_name.blank?
      message += " Release parameter missing or empty." if tool_release.blank?
      message += " Contents parameter missing or empty." if contents.blank?

      errorlog("SYNC ERROR: " + message)
      return render :text => message, :status => :unprocessable_entity
    end

    # try to parse the contents, which should be in JSON format
    begin
      parsed_contents = ToolSpec.new(contents).tools
    rescue Exception => error
      # if we can't parse the contents, there is no point in continuing
      message = "Contents could not be parsed: #{error.message}"
      puts error.backtrace
      errorlog("SYNC ERROR: " + message)
      return render :text => message, :status => :unprocessable_entity
    end

    unless parsed_contents["tools"] && parsed_contents["tools"].size > 0
      message = "No executable specified in the contents."
      errorlog("SYNC ERROR: " + message)
      return render :text => message, :status => :unprocessable_entity
    end

    ToolSpec.synchronize_tool(tool_name, tool_release, contents, mail_exception = false)

    head :ok
  end

  # POST /executables/synchronize_bundle
  def synchronize_bundle
    bundles = params[:bundles]
    bundle = params[:bundle]
    release = params[:release]
    contents = params[:contents][:file].read

    # infolog("SYNC INFO: Processing synchronize_bundle(bundles: #{bundles}, bundle: #{bundle}, release: #{release}, contents: #{contents})")

    if bundle.blank? || release.blank? || contents.blank?
      message = "The request failed."
      message += " Bundle name parameter missing or empty." if bundle.blank?
      message += " Release parameter missing or empty." if release.blank?
      message += " Contents parameter missing or empty." if contents.blank?

      errorlog("SYNC ERROR: " + message)
      return render :text => message, :status => :unprocessable_entity
    end
    infolog("SYNC INFO: no bundles parameter specified") if bundles.blank?

    # try to parse the contents, which should be in JSON format
    begin
      parsed_contents = ToolSpec.new(contents).tools
    rescue Exception => error
      # if we can't parse the contents, there is no point in continuing
      message = "Contents could not be parsed: #{error.message} #{error.backtrace}"
      errorlog("SYNC ERROR: " + message)
      return render :text => message, :status => :unprocessable_entity
    end

    unless parsed_contents["tools"] && parsed_contents["tools"].size > 0
      message = "No executable specified in the contents."
      errorlog("SYNC ERROR: " + message)
      return render :text => message, :status => :unprocessable_entity
    end

    ToolSpec.synchronize_bundle(bundles, bundle, release, contents, mail_exception = false)

    head :ok
  end


  private

 def executable_params
    params.require(:executable).permit!
  end

  def change_status(status, comment)
    @executable = Executable.find(params[:id])
    if status != :approved || @executable.to_be_reviewed_change_made_by != current_user.user_name
      @executable.status = status
      @executable.comment = comment
      @executable.updated_by = session[:username]
      if @executable.save
        Postoffice.review_request(@executable).deliver  if status == :review
        redirect_to executables_url, :notice => "Status was successfully changed to #{status}."
      else
        redirect_to executables_url, :alert => "Status change to '#{status}' failed."
      end
    else
      redirect_to executables_url, :alert => "Status cannot be set to approved by #{current_user}, please let someone else review and approve the changes."
    end
  end

  def params_to_datatypes_json(datatypes_params)
    if datatypes_params
      datatypes = JSON.parse(datatypes_params)
      datatypes.each do |dt|
        datatype = DataType.find_by_name(dt["datatype"])
        datatype.use_latest_baseline
        dt["id"] = datatype.id if dt["id"].blank?
        dt["version"] = datatype.version if dt["version"].blank?
      end
      return datatypes.to_json
    else
      return [].to_json
    end
  end


  def sort_column
    params[:sort] || "name"
  end

  def sort_direction
    params[:direction] || "desc"
  end

  def search
    params[:search] || ""
  end

end
