class ExternalContactsController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "application"
  helper_method :sort_column, :sort_direction, :search
  # GET /external_contacts
  # GET /external_contacts.xml
  
  def index
    @search_opts = {:inactive => "0"}
    if params[:reset]
      params.delete :inactive
      params.delete :search
      params.delete :reset
      params.delete :sort
      params.delete :direction
      params.delete :column_search_field
    end

    conditions_string_ary = []
    conditions_param_values = []

    if params[:inactive]
      @search_opts = {:inactive => "1"}
      conditions_string_ary << "1=1"
    else
      conditions_string_ary << "(external_contacts.status != '#{ExternalContact::STATUS_OBSOLETE}')"
    end

    unless params[:search].blank?
      q = params[:search].strip
      conditions_string_ary << "(companies.id = ?
                                OR companies.name LIKE ?
                                OR external_contacts.first_name LIKE ?
                                OR external_contacts.last_name  LIKE ?
                                OR external_contacts.primary_address_street LIKE ?
                                OR external_contacts.primary_address_postalcode LIKE ?
                                OR external_contacts.primary_address_city LIKE ?
                                OR external_contacts.primary_address_state LIKE ?
                                OR external_contacts.primary_address_country LIKE ?
                                OR external_contacts.phone_work LIKE ?
                                OR external_contacts.phone_mobile LIKE ?
                                OR external_contacts.email LIKE ?
                                OR external_contacts.title LIKE ?)"
      conditions_param_values << q.to_i # only for id
      12.times do
        conditions_param_values << "%#{q}%"
      end
    end

    if !column_search_query.empty?
      conditions_string_ary << "#{column_search_query['query']}"
      conditions_param_values +=  column_search_query["values"]
    end
    conditions = conditions_string_ary.join( ' AND ' )

    @external_contacts = ExternalContact.eager_load(:company).where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])
    generate_id_sequence_cookie(@external_contacts,params[:page])
    if request.xhr?
      render(:partial => "external_contact", :collection => @external_contacts)
    else        
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @external_contacts }
      end
    end
  end

    # GET /external_contacts/1
    # GET /external_contacts/1.xml
  def show
    @external_contact = ExternalContact.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @external_contact }
    end
  end

  def handle
    @external_contact = ExternalContact.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @external_contact }
    end
  end

  def new
    @external_contact = ExternalContact.new
    @external_contact.status = ExternalContact

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @external_contact }
    end
  end

  # GET /companies/1/edit
  def edit
    @external_contact = ExternalContact.find(params[:id])
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @external_contact }
    end
  end


  def create
    @external_contact = ExternalContact.new(strong_params)
    @external_contact.status = ExternalContact::STATUS_ACTIVE
    respond_to do |format|
      if @external_contact.save
        format.html { redirect_to @external_contact, notice: 'Contact was successfully created.' }
        format.json { render json: @external_contact, status: :created, location: @external_contact }
      else
        format.html { render action: "new" }
        format.json { render json: @external_contact.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @external_contact = ExternalContact.find(params[:id])

    respond_to do |format|
      if @external_contact.update_attributes(strong_params)
        format.html { redirect_to @external_contact, notice: 'Contact was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @external_contact.errors, status: :unprocessable_entity }
      end
    end
  end

  def change_status
    @external_contact = ExternalContact.find(params[:id])
    respond_to do |format|
      if @external_contact.change_status(params[:status])
        format.html { redirect_to :back, notice: "Contact was successfully changed to #{@external_contact.status_string}." }
        format.json { render json: @external_contact, status: :created, location: @external_contact }
      else
        format.html { render action: "show" }
        format.json { render json: @external_contact.errors, status: :unprocessable_entity }
      end
    end
  end
    
private
  
  def sort_column  
    params[:sort] || "companies.name"
  end  
  
  def sort_direction  
    params[:direction] || "asc"  
  end
  
  def search
    params[:search] || ""
  end    


end
