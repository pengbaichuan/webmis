class FileTransferAccountsController < ApplicationController
  before_filter :authorize
  authorize_resource  
  
  # GET /file_transfer_accounts
  # GET /file_transfer_accounts.xml
  layout "webmis"
  def index
    @file_transfer_accounts = FileTransferAccount.where(:active => true)
    
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @file_transfer_accounts }
    end
  end

  # GET /file_transfer_accounts/1
  # GET /file_transfer_accounts/1.xml
  def show
    @file_transfer_account = FileTransferAccount.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @file_transfer_account }
    end
  end

  # GET /file_transfer_accounts/new
  # GET /file_transfer_accounts/new.xml
  def new
    if (params[:id])
      @compknow = params[:id]
    end
    @file_transfer_account = FileTransferAccount.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @file_transfer_account}
    end
  end

  # GET /file_transfer_accounts/1/edit
  def edit
    @file_transfer_account = FileTransferAccount.find(params[:id])
    if params[:return_to]
           @edit_by_company = params[:return_to]
      end
    end


  # POST /file_transfer_accounts
  # POST /file_transfer_accounts.xml
  def create
    @file_transfer_account = FileTransferAccount.new(strong_params)

    respond_to do |format|
      if @file_transfer_account.save
        flash[:notice] = 'File Transfer Account was successfully created.'
        #format.html { redirect_to(@file_transfer_account) }
        if params[:current_company]
        format.html { redirect_to(edit_company_abbr_path(params[:current_company])) }
        else
        format.html { redirect_to(file_transfer_accounts_url) }
        end
        format.xml  { render :xml => @file_transfer_account, :status => :created, :location => @file_transfer_account }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @file_transfer_account.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /file_transfer_accounts/1
  # PUT /file_transfer_accounts/1.xml
  def update
    @file_transfer_account = FileTransferAccount.find(params[:id])

    respond_to do |format|
      if @file_transfer_account.update_attributes(strong_params)
        flash[:notice] = 'File Transfer Account was successfully updated.'
        
        if params[:current_company]
          format.html { redirect_to(edit_company_abbr_path(params[:current_company])) }
        else
        format.html { redirect_to(file_transfer_accounts_url) }
        end
        
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @file_transfer_account.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /file_transfer_accounts/1
  # DELETE /file_transfer_accounts/1.xml
  def destroy
    @file_transfer_account = FileTransferAccount.find(params[:id])
    @file_transfer_account.destroy

    respond_to do |format|
      format.html { redirect_to(file_transfer_accounts_url) }
      format.xml  { head :ok }
    end
  end
  

end
