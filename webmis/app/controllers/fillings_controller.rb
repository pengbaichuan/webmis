class FillingsController < ApplicationController

  before_filter :authorize
  authorize_resource param_method: :strong_params
  layout "application"
  helper_method :sort_column, :sort_direction, :search

  # GET /fillings
  # GET /fillings.json
  def index
    @search_opts = {:inactive => "0"}
    
    if params[:reset]
      params.delete :inactive
      params.delete :search
      params.delete :reset
      params.delete :sort
      params.delete :direction
      params.delete :column_search_field
    end

    conditions_string_ary = []
    conditions_param_values = []

    if params[:inactive]
      @search_opts = {:inactive => "1"}
      conditions_string_ary << "(1=1)"
    else
      conditions_string_ary << "((fillings.removed_yn = 0 OR fillings.removed_yn IS NULL) AND (fillings.active_yn = 1))"
    end

    unless params[:search].blank?
      q = params[:search].strip
      conditions_string_ary << "(fillings.id = ?
                                OR fillings.name LIKE ?
                                OR data_types.name LIKE ?)"
      conditions_param_values << q.to_i # only for id
      2.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
    end

    if !column_search_query.empty?
      conditions_string_ary << "#{column_search_query['query']}"
      conditions_param_values +=  column_search_query["values"]
    end
    conditions = conditions_string_ary.join( ' AND ' )

    @fillings = Filling.includes(:data_types).where([conditions] + conditions_param_values).references(:data_types).order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])
    generate_id_sequence_cookie(@fillings,params[:page])
    
    if request.xhr?
      flash.keep[:notice]
      render(:partial => "filling", :collection => @fillings)
    end
  end

  # GET /fillings/1
  # GET /fillings/1.json
  def show
    @filling = Filling.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @filling }
    end
  end

  def handle
    @filling = Filling.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @filling }
    end
  end

  # GET /fillings/new
  # GET /fillings/new.json
  def new
    @filling = Filling.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @filling }
    end
  end

  # GET /fillings/1/edit
  def edit
    @filling = Filling.find(params[:id])
  end

  # POST /fillings
  # POST /fillings.json
  def create
    @filling = Filling.new(strong_params)

    respond_to do |format|
      if @filling.save
        format.html { redirect_to @filling, notice: 'Filling was successfully created.' }
        format.json { render json: @filling, status: :created, location: @filling }
      else
        format.html { render action: "new" }
        format.json { render json: @filling.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /fillings/1
  # PUT /fillings/1.json
  def update
    @filling = Filling.find(params[:id])

    respond_to do |format|
      if @filling.update_attributes(strong_params)
        format.html { redirect_to @filling, notice: 'Filling was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @filling.errors, status: :unprocessable_entity }
      end
    end
  end

  def remove
    @filling = Filling.find(params[:id])
    @filling.removed_yn = true
    @filling.active_yn = false

    respond_to do |format|
      if @filling.save
        format.html { redirect_to handle_filling_path(@filling), notice: 'Filling was successfully removed.' }
        format.json { head :no_content }
      else
        format.html { render action: "handle" }
        format.json { render json: @filling.errors, status: :unprocessable_entity }
      end
    end
  end

  def restore
    @filling = Filling.find(params[:id])
    @filling.removed_yn = false
    @filling.active_yn = true

    respond_to do |format|
      if @filling.save
        format.html { redirect_to handle_filling_path(@filling), notice: 'Filling was successfully restored.' }
        format.json { head :no_content }
      else
        format.html { render action: "handle" }
        format.json { render json: @filling.errors, status: :unprocessable_entity }
      end
    end
  end

  def activate
    @filling = Filling.find(params[:id])
    @filling.active_yn = true
    @filling.removed_yn = false
    
    respond_to do |format|
      if @filling.save
        format.html { redirect_to handle_filling_path(@filling), notice: 'Filling was successfully activated.' }
        format.json { head :no_content }
      else
        format.html { render action: "handle" }
        format.json { render json: @filling.errors, status: :unprocessable_entity }
      end
    end
  end

  def deactivate
    @filling = Filling.find(params[:id])
    @filling.active_yn = false

    respond_to do |format|
      if @filling.save
        format.html { redirect_to handle_filling_path(@filling), notice: 'Filling was successfully deactivated.' }
        format.json { head :no_content }
      else
        format.html { render action: "handle" }
        format.json { render json: @filling.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fillings/1
  # DELETE /fillings/1.json
  def destroy
    @filling = Filling.find(params[:id])
    @filling.destroy

    respond_to do |format|
      format.html { redirect_to fillings_url }
      format.json { head :no_content }
    end
  end

  private
  def sort_column
    params[:sort] || "fillings.name"
  end

  def sort_direction
    params[:direction] || "asc"
  end

  def search
    params[:search] || ""
  end
  
end
