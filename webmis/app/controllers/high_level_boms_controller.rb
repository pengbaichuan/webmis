class HighLevelBomsController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout 'webmis'
  
  # GET /high_level_boms
  # GET /high_level_boms.xml
  def index
    if params[:reset]
      params.delete :search
      params.delete :reset
    end    
    
    conditions = '1=1'
    
    if params[:search] && params[:search] != ""
      q = params[:search].strip
      conditions = "#{conditions} and (high_level_boms.internal_reference LIKE ? or 
                    high_level_boms.product_id LIKE ? or
                    products.volumeid LIKE ? or
                    receptions.storage_location LIKE ?)"
    end 
    
    @high_level_boms = HighLevelBom.joins([:product,:inlay,:label]).where(conditions,"%#{q}%","%#{q}%","%#{q}%","%#{q}%")

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @high_level_boms }
    end
  end

  # GET /high_level_boms/1
  # GET /high_level_boms/1.xml
  def show
    @high_level_bom = HighLevelBom.find(params[:id])
    @high_level_bom.created_by_user_id = current_user.id

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @high_level_bom }
    end
  end

  # GET /high_level_boms/new
  # GET /high_level_boms/new.xml
  def new
    @high_level_bom = HighLevelBom.new
    @data_type = DataType.where(:name => 'ART').first.id
    @high_level_bom.created_by_user_id = current_user.id
    @high_level_bom.deleted = false

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @high_level_bom }
    end
  end

  # GET /high_level_boms/1/edit
  def edit
    @high_level_bom = HighLevelBom.find(params[:id])
    @data_type = DataType.where(:name => 'ART').first.id
    @high_level_bom.created_by_user_id = current_user.id
  end

  # POST /high_level_boms
  # POST /high_level_boms.xml
  def create
    @high_level_bom = HighLevelBom.new(strong_params)
    @high_level_bom.created_by_user_id = current_user.id
    respond_to do |format|
      if @high_level_bom.save
        format.html { redirect_to(@high_level_bom, :notice => 'HighLevelBom was successfully created.') }
        format.xml  { render :xml => @high_level_bom, :status => :created, :location => @high_level_bom }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @high_level_bom.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /high_level_boms/1
  # PUT /high_level_boms/1.xml
  def update
    @high_level_bom = HighLevelBom.find(params[:id])
    @high_level_bom.created_by_user_id = current_user.id
    respond_to do |format|
      if @high_level_bom.update_attributes(strong_params)
        format.html { redirect_to(@high_level_bom, :notice => 'HighLevelBom was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @high_level_bom.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /high_level_boms/1
  # DELETE /high_level_boms/1.xml
  def destroy
    @high_level_bom = HighLevelBom.find(params[:id])
    @high_level_bom.destroy

    respond_to do |format|
      format.html { redirect_to(high_level_boms_url) }
      format.xml  { head :ok }
    end
  end
  
  def get_product_locations
    @product_locations = ProductLocation.all
    if params[:prid] != ''
        @product = Product.find(params[:prid])

        if @product
            product_volume_id = @product.volumeid       
            pid = product_volume_id.to_s.scan(/.{10}/).join
            @product_locations = ProductLocation.where("product_id LIKE '#{pid}%' ")            
        end        
    end
    render :partial => "product_location_select"
  end
end
