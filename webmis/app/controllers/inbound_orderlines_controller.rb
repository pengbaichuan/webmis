class InboundOrderlinesController < ApplicationController
  before_filter :authorize
  authorize_resource
  
  #
  #access_control :DEFAULT => 'admin',
  #               :index => 'operations engineer | readonly | admin',
  #               :edit => 'operations engineer | readonly | admin',
  #               :new => 'operations engineer | admin',
  #               :create => 'operations engineer | admin',
  #               :update => 'operations engineer | admin',
  #               :show => 'operations engineer | admin',
  #               :new => 'operations engineer | admin'

  # GET /inbound_orderlines
  # GET /inbound_orderlines.xml

  layout "webmis"
  def index
    if params[:grid] && params[:grid][:page] != ''
      page = params[:grid][:page]
      params['grid'].delete(:page)
    else
      page = 1
    end
    
    if params[:c] && params[:c].strip != ''
      conditions_suff = ""
      if Reception.where(:id => params[:c]).first
        receptionrefs = Reception.find(params[:c]).references.to_s.split(',')
      else
        receptionrefs = []
      end
      receptionrefs.each do |x|
        conditions_suff = conditions_suff + "or drfmsid like '%#{x.strip}%' "
      end
            #drfmsid like '%#{receptionref}%' or 
      conditions = "drfmsid like '%#{params[:c]}%' or area like '%#{params[:c]}%' or dbrelease like '%#{params[:c]}%' or dbversion like '%#{params[:c]}%'" + conditions_suff
     @inbound_orderlines = initialize_grid(InboundOrderline,:conditions => conditions, :per_page => 20 ,:order => 'id', :order_direction => 'desc', :page => page)
    else
     @inbound_orderlines = initialize_grid(InboundOrderline,:conditions => "", :per_page => 20, :order => 'id', :order_direction => 'desc', :page => page)
    end
    
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @inbound_orderlines }
    end
  end

  # GET /inbound_orderlines/1
  # GET /inbound_orderlines/1.xml
  def show
    @inbound_orderline = InboundOrderline.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @inbound_orderline }
    end
  end

  # GET /inbound_orderlines/new
  # GET /inbound_orderlines/new.xml
  def new
    @inbound_orderline = InboundOrderline.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @inbound_orderline }
    end
  end

  # GET /inbound_orderlines/1/edit
  def edit
    @inbound_orderline = InboundOrderline.find(params[:id])
    begin
	@a = InboundOrderline.where("id in (select MIN(id) from inbound_orderlines where id > "+@inbound_orderline.id.to_s+")").first
	if @a
		@next = @a.id
		@result_next = true
	end
    rescue => error
	@result_next = false
    end

    begin
	@b = InboundOrderline.where("id in (select MAX(id) from inbound_orderlines where id < "+@inbound_orderline.id.to_s+")").first
	if @b
		@previous = @b.id
		@result_previous = true
	end
    rescue => error
	@result_previous = false
    end
  end

  # POST /inbound_orderlines
  # POST /inbound_orderlines.xml
  def create
    @inbound_orderline = InboundOrderline.new(strong_params)

    @order = InboundOrder.find_by_orderref(@inbound_orderline.drfmsid)
    InboundOrderline.transaction do 
    if !@order
     @order = InboundOrder.new
   end 
    @order.data_release_id = @inbound_orderline.data_release_id
    @order.orderref = @inbound_orderline.drfmsid
    @order.save
    @inbound_orderline.inbound_order_id = @order.id
    respond_to do |format|
      if @inbound_orderline.save && @order.save
        flash[:notice] = 'InboundOrderline was successfully created.'
        format.html { redirect_to(@inbound_orderline) }
        format.xml  { render :xml => @inbound_orderline, :status => :created, :location => @inbound_orderline }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @inbound_orderline.errors, :status => :unprocessable_entity }
      end
    end
    end
  end

  # PUT /inbound_orderlines/1
  # PUT /inbound_orderlines/1.xml
  def update
    @inbound_orderline = InboundOrderline.find(params[:id])

    respond_to do |format|
      if @inbound_orderline.update_attributes(strong_params)
        flash[:notice] = 'Record was successfully updated.'
        format.html { redirect_to(@inbound_orderline) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @inbound_orderline.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /inbound_orderlines/1
  # DELETE /inbound_orderlines/1.xml
  def destroy
    @inbound_orderline = InboundOrderline.find(params[:id])
    @inbound_orderline.destroy

    respond_to do |format|
      format.html { redirect_to(inbound_orderlines_url) }
      format.xml  { head :ok }
    end
  end
end
