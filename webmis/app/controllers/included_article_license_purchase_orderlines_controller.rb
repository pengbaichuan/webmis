class IncludedArticleLicensePurchaseOrderlinesController < ApplicationController
  # GET /included_article_license_purchase_orderlines
  # GET /included_article_license_purchase_orderlines.json
  def index
    @included_article_license_purchase_orderlines = IncludedArticleLicensePurchaseOrderline.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @included_article_license_purchase_orderlines }
    end
  end

  # GET /included_article_license_purchase_orderlines/1
  # GET /included_article_license_purchase_orderlines/1.json
  def show
    @included_article_license_purchase_orderline = IncludedArticleLicensePurchaseOrderline.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @included_article_license_purchase_orderline }
    end
  end

  # GET /included_article_license_purchase_orderlines/new
  # GET /included_article_license_purchase_orderlines/new.json
  def new
    @included_article_license_purchase_orderline = IncludedArticleLicensePurchaseOrderline.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @included_article_license_purchase_orderline }
    end
  end

  # GET /included_article_license_purchase_orderlines/1/edit
  def edit
    @included_article_license_purchase_orderline = IncludedArticleLicensePurchaseOrderline.find(params[:id])
  end

  # POST /included_article_license_purchase_orderlines
  # POST /included_article_license_purchase_orderlines.json
  def create
    @included_article_license_purchase_orderline = IncludedArticleLicensePurchaseOrderline.new(strong_params)

    respond_to do |format|
      if @included_article_license_purchase_orderline.save
        format.html { redirect_to @included_article_license_purchase_orderline, notice: 'Included article license purchase orderline was successfully created.' }
        format.json { render json: @included_article_license_purchase_orderline, status: :created, location: @included_article_license_purchase_orderline }
      else
        format.html { render action: "new" }
        format.json { render json: @included_article_license_purchase_orderline.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /included_article_license_purchase_orderlines/1
  # PUT /included_article_license_purchase_orderlines/1.json
  def update
    @included_article_license_purchase_orderline = IncludedArticleLicensePurchaseOrderline.find(params[:id])

    respond_to do |format|
      if @included_article_license_purchase_orderline.update_attributes(strong_params)
        format.html { redirect_to @included_article_license_purchase_orderline, notice: 'Included article license purchase orderline was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @included_article_license_purchase_orderline.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /included_article_license_purchase_orderlines/1
  # DELETE /included_article_license_purchase_orderlines/1.json
  def destroy
    @included_article_license_purchase_orderline = IncludedArticleLicensePurchaseOrderline.find(params[:id])
    @included_article_license_purchase_orderline.destroy

    respond_to do |format|
      format.html { redirect_to included_article_license_purchase_orderlines_url }
      format.json { head :no_content }
    end
  end
end
