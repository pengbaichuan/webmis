# coding: utf-8

class InvoicesController < ApplicationController
  before_filter :authorize
  authorize_resource

  layout "webmis"
  helper_method :sort_column, :sort_direction, :search

  def index
    conditions = '1=1'
    if params[:reset]
      params.delete :search
      params.delete :reset

      params.delete :sort
      params.delete :direction
    end

    conditions_string_ary = []
    conditions_param_values = []

    # unpaid_invoices_overview is a token to keep track on whether you have to show
    # all invoices in the overview or only the uninvoiced one. It is maintained throughout the
    # show and edit views so when you go back to your overview, you'll end up in the expected one.
    @unpaid_invoices_overview = params[:unpaid_invoices_overview]

    if @unpaid_invoices_overview && !@unpaid_invoices_overview.blank?
      conditions_string_ary << ' and (status = "50")'
    else
      @unpaid_invoices_overview = nil
    end

    conditions = conditions + conditions_string_ary.join(" ")

    if (sort_column == 'status')
      @invoices = Invoice.where([conditions] + conditions_param_values).sort_by{ |r| r.status_string }
      @invoices.reverse! if sort_direction == 'desc'
    elsif (sort_column == 'customer')
      @invoices = Invoice.where([conditions] + conditions_param_values).sort_by{ |r| r.shipping_order.invoicing_customer_depot.supplier_code }
      @invoices.reverse! if sort_direction == 'desc'
    elsif (sort_column == 'PO ref')
      @invoices = Invoice.where([conditions] + conditions_param_values).sort_by{ |r| r.shipping_order.po_reference }
      @invoices.reverse! if sort_direction == 'desc'
    else
      @invoices = Invoice.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction)
    end

    unless params[:search].blank?
      search = params[:search].strip
      matched_invoices = []
      @invoices.each do |invoice|
        if ( (invoice.reference && invoice.reference.include?(search) ) ||
             invoice.status_string.include?(search) ||
             (invoice.shipping_order.invoicing_customer_depot && invoice.shipping_order.invoicing_customer_depot.supplier_code.include?(search) ) ||
             (invoice.shipping_order.po_reference && invoice.shipping_order.po_reference.include?(search) ) ||
             (invoice.due_date && invoice.due_date.strftime("%d %B %Y").include?(search) )
          )
          matched_invoices << invoice
        end
      end
      @invoices = matched_invoices
    end

    @invoices = @invoices.paginate(per_page: 100, page: params[:page])

    if request.xhr?
      flash.keep[:notice]
      render(:partial => "invoice", :collection => @invoices)
    end
  end

  # GET /invoices/1
  # GET /invoices/1.xml
  def show
    @invoice = Invoice.find(params[:id])
    @unpaid_invoices_overview = params[:unpaid_invoices_overview]

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # GET /invoices/new
  def new
    @invoice = Invoice.new
    shipping_order = ShippingOrder.find(params[:shipping_order_id])
    @invoice.shipping_order = shipping_order
    @invoice.status = 0

    get_invoicable_shipping_orderlines

    # @unpaid_invoices_overview is always false so no need to handle it.

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # GET /invoices/1/edit
  def edit
    @invoice = Invoice.find(params[:id])
    get_invoicable_shipping_orderlines(@invoice.id)
    @unpaid_invoices_overview = params[:unpaid_invoices_overview]
    # disable caching
  end

  # POST /invoices
  def create
    @invoice = Invoice.new(shipping_order_id: params[:invoice][:shipping_order_id])
    calculate_attribute_values

    respond_to do |format|
      format.html {
        if @invoice.save
          redirect_to @invoice, notice: 'Invoice was successfully created.'
        else
          get_invoicable_shipping_orderlines
          render action: "new", alert: 'Invoice could not be created.'
        end
      }
    end
  end

  # PUT /invoices/1
  def update
    @invoice = Invoice.find(params[:id])
    @unpaid_invoices_overview = true unless params[:unpaid_invoices_overview].blank?
    calculate_attribute_values

    respond_to do |format|
      format.html {
        if @invoice.save
          flash[:notice] = 'Invoice was successfully updated.'
          redirect_to invoice_url(@invoice, unpaid_invoices_overview: @unpaid_invoices_overview)
        else
          render action: "edit", alert: 'Invoice could not be updated.'
        end
      }
    end
  end

  # POST /invoices/1/invoice.pdf
  def invoice
    @invoice = Invoice.find(params[:id])
    invoice_to_delivered
    respond_to do |format|
      format.pdf {
        if @invoice.save
          redirect_to action: 'print', format: 'pdf', id: @invoice.id
        else
          redirect_to :back, alert: "Invoice could not be saved."
        end
      }
    end
  end

  # GET /invoices/1/print
  # GET /invoices/1/print.pdf
  def print
    @invoice = Invoice.find(params[:id])
    @unpaid_invoices_overview = params[:unpaid_invoices_overview]
    @cu = current_user
    @mapscape = CustomerDepot.where('label like "%mapscape%" and city like "eindhoven"').first

    respond_to do |format|
      format.html { render action: 'show' }
      format.pdf {
        render  pdf: "#{@invoice.reference}_#{@invoice.date.strftime('%Y%m%d')}",
                margin: { bottom: 20 },
                footer: { html: { template: '/pdf_footer', formats: [:html] }},
                encoding: "utf8"
      }
    end
  end

  # POST /invoices/1/paid
  def paid
    @invoice = Invoice.find(params[:id])
    @invoice.status = 60
    @invoice.save

    respond_to do |format|
      format.html { redirect_to :back, notice: "Invoice #{@invoice.reference} set to paid."}
    end
  end

  # DELETE /invoices/1
  # DELETE /invoices/1.xml
  def destroy
    @invoice = Invoice.find(params[:id])
    @invoice.destroy

    respond_to do |format|
      format.html { redirect_to invoices_url(unpaid_invoices_overview: params[:unpaid_invoices_overview]) }
    end
  end

  def credit_invoice
    @debet_invoice = Invoice.find(params[:id])

    respond_to do |format|
      format.html {
        if new_id = @debet_invoice.credit_invoice
          @invoice = Invoice.find(new_id)
          invoice_to_delivered
          redirect_to @invoice, notice: 'CREDIT invoice was successfully created.'
        else
          redirect_to :back, alert: "CREDIT invoice could not be created."
        end
      }
    end
  end

private
  def get_invoicable_shipping_orderlines(id = nil)
    if id
      @invoicable_shipping_orderlines =
        @invoice.shipping_order.shipping_orderlines.where("status=40 AND (invoice_id IS NULL OR invoice_id=?)",@invoice.id)
    else
      @invoicable_shipping_orderlines =
        @invoice.shipping_order.shipping_orderlines.where("status=40 AND invoice_id IS NULL")
    end
  end

  def calculate_attribute_values
    # generate an invoice number unless a manual one has been entered
    if params[:invoice][:reference].blank?
      params[:suffix]="WM"
      asset = AssetType.find_by_name("Invoice")
      nextnumber = asset.get_next_number(params)
      @invoice.reference = nextnumber
      asset = Asset.new
      asset.ref_id = @nextnumber
      asset.requested_by = current_user.first_name + " " + current_user.last_name
      asset.remarks = "Generated via invoice for shipping order #{params[:shipping_order_id]}."
      asset.assettype = "Invoice"
      asset.title = nextnumber
      asset.suffix = params[:suffix]
      asset.save
    else
      @invoice.reference = params[:invoice][:reference]
    end
    @invoice.remarks = params[:invoice][:remarks]
    if params[:invoice][:shipping_orderline_ids]
      @invoice.shipping_orderline_ids = params[:invoice][:shipping_orderline_ids]
    else
      @invoice.shipping_orderline_ids = []
    end
    @invoice.vat_percentage = @invoice.shipping_order.shipping_customer_depot.vat_percentage.nil? ? 0 : @invoice.shipping_order.shipping_customer_depot.vat_percentage 

    @invoice.total = 0
    @invoice.shipping_orderlines.each do |line|
      @invoice.total += (line.amount * line.article.price)
      @invoice.currency ||= line.article.currency
    end
    @invoice.currency ||= "€"   # set a default value to advoid nil issues
    @invoice.vat = @invoice.total / 100 * @invoice.vat_percentage

    # the actual invoice date will be recalculated when the invoice is actually generated
    # this date is just so the show invoice also displays a date for a draft invoice, hopefully
    # minimizing confusing
    calculate_invoice_dates
    @invoice.status = 0
  end

  def invoice_to_delivered
    # to prevent setting the invoice to delivered twice.
    if @invoice.status < 50
      @invoice.shipping_order_id = params[:shipping_order_id] unless @invoice.shipping_order
      calculate_invoice_dates
      @invoice.status = 50
    end
  end

  def calculate_invoice_dates
    @invoice.date = Time.now()
    if @invoice.shipping_order.invoicing_customer_depot.payment_term.to_i > 0
      @invoice.due_date = @invoice.date + @invoice.shipping_order.invoicing_customer_depot.payment_term.days
    else
      @invoice.due_date = @invoice.date + 30.days
    end
  end

  def sort_column
    params[:sort] || "reference"
  end

  def sort_direction
    params[:direction] || "asc"
  end

  def search
    params[:search] || ""
  end
end
