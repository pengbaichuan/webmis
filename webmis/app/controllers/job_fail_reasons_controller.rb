class JobFailReasonsController < ApplicationController
  before_filter :authorize
  authorize_resource

  layout "webmis"
  helper_method :sort_column, :sort_direction, :search

  # GET /job_fail_reasons
  # GET /job_fail_reasons.json
  def index
    conditions = '1=1'
    if params[:reset]
      params.delete :search
      params.delete :reset

      params.delete :sort
      params.delete :direction
    end

    conditions_string_ary = []
    conditions_param_values = []

    unless params[:search].blank?
      conditions_string_ary << " and (reason LIKE ?
              or job_type LIKE ? or job_type LIKE ? or id = #{params[:search].strip.to_i})"
      2.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
      conditions_param_values << "#{params[:search].strip.titleize.gsub(' ','').classify}" # 3th one
    end

    conditions = conditions + conditions_string_ary.join(" ")    
    @job_fail_reasons = JobFailReason.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])

    if request.xhr?
      render(:partial => "job_fail_reason", :collection => @job_fail_reasons)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @job_fail_reasons }
      end
    end
  end

  # GET /job_fail_reasons/1
  # GET /job_fail_reasons/1.json
  def show
    @job_fail_reason = JobFailReason.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @job_fail_reason }
    end
  end

  # GET /job_fail_reasons/new
  # GET /job_fail_reasons/new.json
  def new
    @job_fail_reason = JobFailReason.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @job_fail_reason }
    end
  end

  # GET /job_fail_reasons/1/edit
  def edit
    @job_fail_reason = JobFailReason.find(params[:id])
  end

  # POST /job_fail_reasons
  # POST /job_fail_reasons.json
  def create
    @job_fail_reason = JobFailReason.new(strong_params)

    respond_to do |format|
      if @job_fail_reason.save
        format.html { redirect_to @job_fail_reason, notice: 'Job fail reason was successfully created.' }
        format.json { render json: @job_fail_reason, status: :created, location: @job_fail_reason }
      else
        format.html { render action: "new" }
        format.json { render json: @job_fail_reason.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /job_fail_reasons/1
  # PUT /job_fail_reasons/1.json
  def update
    @job_fail_reason = JobFailReason.find(params[:id])

    respond_to do |format|
      if @job_fail_reason.update_attributes(strong_params)
        format.html { redirect_to @job_fail_reason, notice: 'Job fail reason was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @job_fail_reason.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /job_fail_reasons/1
  # DELETE /job_fail_reasons/1.json
  def destroy
    @job_fail_reason = JobFailReason.find(params[:id])
    @job_fail_reason.destroy

    respond_to do |format|
      format.html { redirect_to job_fail_reasons_url }
      format.json { head :no_content }
    end
  end

private
  def sort_column
    params[:sort] || "reason"
  end

  def sort_direction
    params[:direction] || "desc"
  end

  def search
    params[:search] || ""
  end
end
