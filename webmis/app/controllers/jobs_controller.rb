class JobsController < ApplicationController
  before_filter :authorize
  authorize_resource

  layout "webmis"

  def tagged_failed_jobs_report
    if params[:reset]
      params.delete :reset
      params.delete :date_range_a
      params.delete :date_range_b
    end

    unless params[:date_range_a].blank?
      range_begin = params[:date_range_a]
      range_end = Date.today.strftime("%d-%m-%Y")
      if !params[:date_range_b].blank?
        range_end = params[:date_range_b]
      end
    else
      range_begin = (Date.today - 1.month).strftime("%d-%m-%Y")
      params[:date_range_a] = range_begin
    end

    unless params[:date_range_b].blank?
      if params[:date_range_a].blank?
        range_begin = Job.fail_reason_tagged.first.fail_category_logged_on.strftime("%d-%m-%Y")
        range_end = Date.today.strftime("%d-%m-%Y")
      end
    else
      range_end = Date.today.strftime("%d-%m-%Y")
      params[:date_range_b] = range_end
    end

    conditions_string = "( jobs.project_id = #{session[:project_id]} )"
    conditions_array = []
    
    unless params[:rtype].blank?
        conditions_string += "AND (jobs.type = ?)"
        conditions_array = [params[:rtype]]
    end

    if range_begin
      conditions_string += "AND (fail_category_logged_on BETWEEN '#{DateTime.parse(range_begin)}' AND '#{DateTime.parse(range_end)}')"
    end
    @jobs = Job.fail_reason_tagged.where([conditions_string] + conditions_array).order(:type)
      respond_to do |format|
        format.html # index.html.erb
        format.xml { render :xml => @jobs}
        format.csv { render :csv => @jobs, :filename => "tagged_failed_jobs_report_#{Time.now.strftime("%Y%m%d-%H%M")}", :style => :tagged_failed_jobs_report}
      end
  end
end


