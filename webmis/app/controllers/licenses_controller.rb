class LicensesController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"
  
  # GET /licenses
  # GET /licenses.xml
  def index
    @licenses = License.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @licenses }
    end
  end

  # GET /licenses/1
  # GET /licenses/1.xml
  def show
    @license = License.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @license }
    end
  end

  # GET /licenses/new
  # GET /licenses/new.xml
  def new
    @license = License.new
    @license.gen_lic
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @license }
    end
  end

  # GET /licenses/1/edit
  def edit
    @license = License.find(params[:id])
  end

  # POST /licenses
  # POST /licenses.xml
  def create
    @license = License.new(strong_params)

    respond_to do |format|
      if @license.save
        flash[:notice] = 'License was successfully created.'
        format.html { redirect_to(@license) }
        format.xml  { render :xml => @license, :status => :created, :location => @license }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @license.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /licenses/1
  # PUT /licenses/1.xml
  def update
    @license = License.find(params[:id])

    respond_to do |format|
      if @license.update_attributes(strong_params)
        flash[:notice] = 'License was successfully updated.'
        format.html { redirect_to(@license) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @license.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /licenses/1
  # DELETE /licenses/1.xml
  def destroy
    @license = License.find(params[:id])
    @license.destroy

    respond_to do |format|
      format.html { redirect_to(licenses_url) }
      format.xml  { head :ok }
    end
  end

  def ch
    c = OpenSSL::Cipher::Cipher.new("aes-256-cbc")
    c.decrypt
    c.key = Digest::SHA1.hexdigest("mslic5329")
    c.iv = params[:i]
    d = c.update(params[:l])
    d << c.final

    l = License.find_by_licensenumber(params[:li])
    status = ""
    token2 = 0
    if l
      status = "OK"
      #secret calculation , known on both sides
      token2 = d.to_i - 165923
    else
      status = "expired"
    end

    toencrypt = "token: #{token2} status: #{status}"
    c = OpenSSL::Cipher::Cipher.new("aes-256-cbc")
    c.encrypt
    # your pass is what is used to encrypt/decrypt
    c.key = Digest::SHA1.hexdigest("mslic53295")
    c.iv = params[:i].to_s
    #tosend secret calculator number
    e = c.update(toencrypt.to_s)
    e << c.final

    render :text => e
  end


  


end
