class LocationsController < ApplicationController
  before_filter :authorize
  authorize_resource
  # GET /locations
  # GET /locations.xml
  layout "webmis"
  def index
    #@locations = Location.paginate(:all, :page => params[:page], :per_page => 15, :order => "location_code ASC")
    @locations = Location.all.paginate(:page => params[:page], :per_page => 15).order("location_code ASC")

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @locations }
    end
  end

  # GET /locations/1
  # GET /locations/1.xml
  def show
    @location = Location.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @location }
    end
  end

  # GET /locations/new
  # GET /locations/new.xml
  def new
    @location = Location.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @location }
    end
  end

  # GET /locations/1/edit
  def edit
    @location = Location.find(params[:id])
    begin
	@a = Location.where("id in (select MIN(id) from locations where id > "+@location.id.to_s+")").first
	if @a
		@next = @a.id
		@result_next = true
	end
    rescue => error
	@result_next = false
    end

    begin
	@b = Location.where("id in (select MAX(id) from locations where id < "+@location.id.to_s+")").first
	if @b
		@previous = @b.id
		@result_previous = true
	end
    rescue => error
	@result_previous = false
    end
  end

  # POST /locations
  # POST /locations.xml
  def create
    @location = Location.new(strong_params)

    respond_to do |format|
      if @location.save
        flash[:notice] = 'Location was successfully created.'
        format.html { redirect_to(@location) }
        format.xml  { render :xml => @location, :status => :created, :location => @location }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @location.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /locations/1
  # PUT /locations/1.xml
  def update
    @location = Location.find(params[:id])

    respond_to do |format|
      if @location.update_attributes(strong_params)
        flash[:notice] = 'Location was successfully updated.'
        format.html { redirect_to(@location) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @location.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /locations/1
  # DELETE /locations/1.xml
  def destroy
    @location = Location.find(params[:id])
    @location.destroy

    respond_to do |format|
      format.html { redirect_to(locations_url) }
      format.xml  { head :ok }
    end
  end
end
