class LoginController < ApplicationController
  require 'ldap'
  layout 'login'
  skip_authorization_check
  before_filter :set_host_from_request_for_mail

  def index
    session[:group] = nil
    session[:username] = nil
  end

  def login(username_input, password_input)
    begin
      found = search_ldap(username_input, password_input)
      session[:username] = username_input
      user = User.where("user_name = '"+username_input+"'").first
      if found && !user
        User.sync_ldap_users # Sync Users with new LDAP users and try again
        user = User.where("user_name = '"+username_input+"'").first
      end
      session[:role] = user.roles.where("module = 'MIS' or module = 'DOCGEN'").first if user
      session[:user_id] = user.id
      session[:environment] = Project.default.environment.name
      session[:project_id] = Project.default.id
      session[:color_profile] = Project.default.color_profile
      if found == true
        return true 
      else
        return false
      end
    rescue => e
      puts e.message
      puts e.backtrace
      return false
    end
  end
  
  def do_login
    testuser = false
    if params[:login][:username] == 'testuser'
      @testuser = User.find_by_user_name("testuser")
      if !@testuser
        u = User.new
        u.first_name = "Test"
        u.last_name = "User"
        u.user_name = "testuser"
        u.email = 'noreply@mapscape.eu'
        u.partner_code = 'MAPSCAPE'
        u.save!

        pu = ProjectUser.new
        pu.project_id = Project.default.id
        pu.user_id = u.id
        pu.save!

        up = UserProfile.new
        up.user_id = u.id
        up.last_used_project_id =  Project.default.id
        up.save!
        
        r = RolesUser.new
        r.user_id = u.id
          r.role_id = Role.find_by_title("Admin").id
        r.module = 'MIS'
        r.save!

        @testuser = u
      end        
      testuser = true
      session[:username] = params[:login][:username]
      session[:user_id] = @testuser.id

      session[:role] = @testuser.roles.where("module = 'MIS' or module = 'DOCGEN'").first
      #session[:user] = @testuser      
    end

    if testuser == true
      if params[:login][:password] == 'satnav43953' && (Rails.env == "staging" || Rails.env == 'test')
        session[:project_id] = @testuser.user_profile.last_used_project_id
        session[:color_profile] = Project.default.color_profile
        session[:environment] = @testuser.user_profile.last_used_project.environment.name
        redirect_to :controller => 'main', :action => 'index'
      else
        if Rails.env != "staging" && Rails.env != "test"
          flash[:error] =  "User only allowed on test environment!"
        else
          flash[:error] =  "Your password is incorrect!"
        end
        redirect_to :action => :index
      end
    else
      if self.login(params[:login][:username],params[:login][:password]) == true
        @user = User.find(session[:user_id])
        if @user.user_profile && !@user.user_profile.last_used_project_id.blank? && @user.user_profile.last_used_project.project_member?(session[:user_id])
          session[:project_id] = @user.user_profile.last_used_project_id
          session[:color_profile] = Project.find(@user.user_profile.last_used_project_id).color_profile
          session[:environment] = @user.user_profile.last_used_project.environment.name
        end
        if session[:role]
          if session[:ref].blank?
            flash.notice =  "Hello #{current_user.first_name}. Welcome to WebMIS."
            redirect_to( {:controller => 'main', :action => 'index'})
          else
            redirect_to session[:ref]
            flash.notice =  "Hello #{current_user.first_name}. Welcome back to WebMIS."
            session[:ref] = nil
          end
        else
          flash[:error] =  "You don't have permission to access the application"
          redirect_to :action => :index 
        end
      else
        flash[:error] =  "Your username and/or password is incorrect!"
        redirect_to :action => :index
      end    
    end 
  end

  private
  def search_ldap(username_input, password_input)
    host = ENV['ldap_host']
    port = ENV['ldap_port'].to_i
    ldap_conn = LDAP::SSLConn.new(host, port)
    ldap_conn.set_option( LDAP::LDAP_OPT_PROTOCOL_VERSION, 3 )

    found = false
    ldap_conn.search("ou=people,o=Mapscape_BV,dc=mapscape,dc=nl",1,"uid=#{username_input}") {|entry|
      found = true
      dn= "cn="+entry['cn'][0].to_s+",ou=people,o=Mapscape_BV,dc=mapscape,dc=nl"
      ldap_conn.bind(dn, password_input)
    }
    return found
  end

end
