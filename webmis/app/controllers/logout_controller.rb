class LogoutController < ApplicationController
skip_authorization_check  


def index
  
  session[:group]  = nil
  session[:username] = nil
  session[:admin_id] = nil
  session[:admin_expiry] = nil
  session[:ref] = nil
  reset_session
  
  flash[:notice] = 'you are logged out.'
  redirect_to(:controller => "login", :action => "index")
end

end
