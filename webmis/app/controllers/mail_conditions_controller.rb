class MailConditionsController < ApplicationController
  before_filter :authorize
  authorize_resource
  # GET /mail_conditions
  # GET /mail_conditions.xml
  layout "webmis"
  def index
    @mail_conditions = MailCondition.order(:field,:field_value).all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @mail_conditions }
    end
  end

  # GET /mail_conditions/1
  # GET /mail_conditions/1.xml
  def show
    @mail_condition = MailCondition.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @mail_condition }
    end
  end

  # GET /mail_conditions/new
  # GET /mail_conditions/new.xml
  def new
    @mail_condition = MailCondition.new
    @mail_condition.field = "DataType"

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @mail_condition }
    end
  end

  # GET /mail_conditions/1/edit
  def edit
    @mail_condition = MailCondition.find(params[:id])
  end

  # POST /mail_conditions
  # POST /mail_conditions.xml
  def create
    @mail_condition = MailCondition.new(strong_params)

    respond_to do |format|
      if @mail_condition.save
        flash[:notice] = 'Mail-Condition was successfully created.'
        #format.html { redirect_to(@mail_condition) }
        format.html { redirect_to(mail_conditions_url) }
        format.xml  { render :xml => @mail_condition, :status => :created, :location => @mail_condition }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @mail_condition.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /mail_conditions/1
  # PUT /mail_conditions/1.xml
  def update
    @mail_condition = MailCondition.find(params[:id])

    respond_to do |format|
      if @mail_condition.update_attributes(strong_params)
        flash[:notice] = 'Mail-Condition was successfully updated.'
        #format.html { redirect_to(@mail_condition) }
        format.html { redirect_to(mail_conditions_url) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @mail_condition.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /mail_conditions/1
  # DELETE /mail_conditions/1.xml
  def destroy
    @mail_condition = MailCondition.find(params[:id])
    @mail_condition.destroy

    respond_to do |format|
      format.html { redirect_to(mail_conditions_url) }
      format.xml  { head :ok }
    end
  end
end
