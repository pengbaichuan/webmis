class MailEventsController < ApplicationController
  before_filter :authorize
  authorize_resource
  
  layout 'webmis'
  # GET /mail_events
  # GET /mail_events.xml
  
  def index
    @mail_events = MailEvent.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @mail_events }
    end
  end

  # GET /mail_events/1
  # GET /mail_events/1.xml
  def show
    @mail_event = MailEvent.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @mail_event }
    end
  end

  # GET /mail_events/new
  # GET /mail_events/new.xml
  def new
    @mail_event = MailEvent.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @mail_event }
    end
  end

  # GET /mail_events/1/edit
  def edit
    @mail_event = MailEvent.find(params[:id])
  end

  # POST /mail_events
  # POST /mail_events.xml
  def create
    @mail_event = MailEvent.new(strong_params)
    @mail_event.save
    redirect_to :action => "index"
  end

  # PUT /mail_events/1
  # PUT /mail_events/1.xml
  def update
    @mail_event = MailEvent.find(params[:id])

    respond_to do |format|
      if @mail_event.update_attributes(strong_params)
        flash[:notice] = 'MailEvent was successfully updated.'
        format.html { redirect_to(show_sub_mail_event_path(@mail_event)) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @mail_event.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /mail_events/1
  # DELETE /mail_events/1.xml
  def destroy
    @mail_event = MailEvent.find(params[:id])
    @mail_event.destroy

    respond_to do |format|
      format.html { redirect_to(mail_events_url) }
      format.xml  { head :ok }
    end
  end

  def show_sub
    @mail_event = MailEvent.find(params[:id])
    @users = @mail_event.users
  end
end
