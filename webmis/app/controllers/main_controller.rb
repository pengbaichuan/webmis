class MainController < ApplicationController
  before_filter :authorize
  layout 'application'
  helper_method :sort_column, :sort_direction, :search

  def routes
    routes = []
    Rails.application.routes.routes.map do |route|
      routes << {controller: route.defaults[:controller], action: route.defaults[:action]} if route.defaults[:controller] && route.defaults[:action]
    end

    respond_to do |format|
      format.html { render :text => routes.join("<br />"), :layout => false }
      format.json { render :json => routes.to_json, :layout => false }
      format.xml  { render :xml => routes.to_xml, :layout => false }
    end
  end

  def index

  end
 
  def denied
    @backlink = "/webmis/main"
    @backlink = params[:bl] if params[:bl]
  end

  def searchit
    searchstr = params[:search].strip
    searchstr = searchstr.gsub(/^\*/,'')
    searchstr = searchstr.gsub(/\*$/,'')
    searchit  = "*#{searchstr}*"
    params[:search] = searchstr

    @search_results = ThinkingSphinx.search Riddle::Query.escape(searchit),:rank_mode => :fieldmask, :order => 'created_at desc', :limit => 1000
    
    if request.xhr?
      render(:partial => "search_result", :collection => @search_results)
    else
     
      respond_to do |format|
        format.html { render action:"searchit" }
        format.json { render json: @search_results }
      end
    end
  end

  def show
    
  end

  def change_project
    if params[:project_id].blank?
      flash[:error] = "No project submitted."
    else
      project = Project.find(params[:project_id])
      if project
        if project.project_member?(current_user.id)
          session[:project_id] = project.id
          session[:environment] = project.environment.name
          session[:color_profile] = project.color_profile
          profile = current_user.user_profile
          profile.last_used_project_id = project.id
          profile.save
          flash[:notice] = "Project sucessfully change to #{project.name}."
        else
          flash[:error] = "You are not member of chosen project."
        end
      else
        flash[:error] = "Project not found!"
      end
    end
    respond_to do |format|
      format.html { redirect_to(:controller => :main) }
    end
  end

  def get_json_file_tree
    render :json => FileSystemUtil.directory_hash(params[:path].to_s).to_json
  end

 private
  def sort_column
    params[:sort] || "id"
  end

  def sort_direction
    params[:direction] || "desc"
  end

  def search
    params[:search] || ""
  end

end
