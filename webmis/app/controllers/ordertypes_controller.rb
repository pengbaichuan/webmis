class OrdertypesController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout 'webmis'
  helper_method :sort_column, :sort_direction, :search

  # GET /ordertypes
  # GET /ordertypes.xml
  def index
    @removed = false

    if params[:reset]
      params.delete :search
      params.delete :reset
      params.delete :removed
    end

    if params[:removed]
      @removed = true
      conditions = "(1=1)"
    else
      conditions = "(ordertypes.status < #{Ordertype::STATUS_OBSOLETE})"
    end

    conditions_string_ary = []
    conditions_param_values = []
    conditions_string_ary << conditions

    unless params[:search].blank?
      q = params[:search].strip
      conditions_string_ary << "(ordertypes.id = ? OR
                                ordertypes.name LIKE ?
                                OR ordertypes.description LIKE ?
                                OR product_categories.abbreviation LIKE ?)"
      conditions_param_values << q.to_i
      3.times do
        conditions_param_values << "%#{q}%"
      end
    end
    conditions = conditions_string_ary.join(' AND ')
    @ordertypes = Ordertype.where([conditions] + conditions_param_values).includes(:product_category).references(:product_categories).paginate(:per_page => 100, :page => params[:page])
    generate_id_sequence_cookie(@ordertypes,params[:page])

    if request.xhr?
     render(:partial => "ordertype", :collection => @ordertypes,:params => params)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @ordertypes }
      end
    end
  end

  # GET /ordertypes/1
  # GET /ordertypes/1.xml
  def show
    @ordertype = Ordertype.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @ordertype }
    end
  end

  def edit_workflow
    @ordertype = Ordertype.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @ordertype }
    end
  end

  # GET /ordertypes/new
  # GET /ordertypes/new.xml
  def new
    @ordertype = Ordertype.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @ordertype }
    end
  end

  # GET /ordertypes/1/edit
  def edit
    @ordertype = Ordertype.find(params[:id])
  end

  # POST /ordertypes
  # POST /ordertypes.xml
  def create
    @ordertype = Ordertype.new(strong_params)

    respond_to do |format|
      if @ordertype.save
        flash[:notice] = 'Ordertype was successfully created.'
        format.html { redirect_to(@ordertype) }
        format.xml  { render :xml => @ordertype, :status => :created, :location => @ordertype }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @ordertype.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /ordertypes/1
  # PUT /ordertypes/1.xml
  def update
    @ordertype = Ordertype.find(params[:id])

    respond_to do |format|
      if @ordertype.update_attributes(strong_params)
        flash[:notice] = 'Ordertype was successfully updated.'
        format.html { redirect_to(@ordertype) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @ordertype.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def clone
    @org_ordertype = Ordertype.find(params[:id])
    
    @ordertype = @org_ordertype.dup
    @ordertype.name = 'CLONE-'+ Time.current.strftime("%Y%m%d%H%M%S") + '-' + @org_ordertype.name.to_s
    @ordertype.description = 'Cloned from ' + @org_ordertype.name.to_s
    @ordertype.save
    
    @org_ordertype.process_transitions.each do |pt|
      @process_transition = pt.dup
      @process_transition.ordertype_id = @ordertype.id
      @process_transition.save
    end
    
    respond_to do |format|
      if @ordertype.save
        flash[:notice] = 'Ordertype was successfully cloned.<br>Please adapt Name and Description,and if needed Product category.'
        format.html { render :action => "edit" }
        format.xml  { render :xml => @ordertype, :status => :created, :location => @ordertype }
      else
        format.html { render :action => "index" }
        format.xml  { render :xml => @ordertype.errors, :status => :unprocessable_entity }
      end
    end
  end

  def change_status
    @ordertype = Ordertype.find(params[:id])
    @ordertype.status = params[:status]
    respond_to do |format|
      if @ordertype.save!
        flash[:notice] = "Ordertype was successfully set to status #{@ordertype.status_str}"
        format.html { redirect_to(:back) }
        format.xml  { head :ok }
      else
        flash[:error] = "Ordertype failed changing status #{@ordertype.status_str} to #{Ordertype.status_array[params[:status]]}."
        format.html { redirect_to(:action => :index) }
        format.xml  { render :xml => @ordertype.errors, :status => :unprocessable_entity }
      end
    end
  end

private
  def sort_column
    params[:sort] || "ordertypes.id"
  end

  def sort_direction
    params[:direction] || "desc"
  end

  def search
    params[:search] || ""
  end
  
end
