class OutboundOrderlinesController < ApplicationController 
  before_filter :authorize
  authorize_resource

  layout 'webmis'
  helper_method :sort_column, :sort_direction, :search

  def index
    if params[:reset]
      params.delete :search
      params.delete :reset

      params.delete :sort
      params.delete :direction
    end

    # only show outbound orderlines which can be mapped to a conversion
    conditions = "outbound_orderlines.product_location_id is not null "
    conditions_string_ary = []
    conditions_param_values = []

    unless params[:search].blank?
      conditions_string_ary << 'AND (outbound_orderlines.product_id LIKE ?
        OR outbound_orderlines.filename LIKE ?
        OR outbound_orders.shipto_name LIKE ?)'
      3.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
    end
    conditions += conditions_string_ary.join(" ")

    @outbound_orderlines = OutboundOrderline.joins(:outbound_order).where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])

    if request.xhr?
      render(:partial => "outbound_orderline", :collection => @outbound_orderlines)
    end
  end

  def destroy_line
    @outbound_order= OutboundOrder.find(params[:outbound_id])
    if @outbound_orderline = OutboundOrderline.find(params[:id]).destroy
      flash.now[:notice] = 'Line succesfully removed.'
    end
    
   # format.html { redirect_to(:action => :edit,:controller => "outbound_orders", :id => @outbound_order.id) }
    respond_to do |format|  
      format.html { redirect_to :back }
      format.json  { head :no_content }
    end
  end 
  
  def edit
    @outbound_order= OutboundOrder.find(params[:outbound_id])
    @outbound_orderline = OutboundOrderline.find(params[:id])
    
  end
  
  def update
    @outbound_orderline = OutboundOrderline.find(params[:id])
    @outbound_order= OutboundOrder.find(params[:outbound_orderline][:outbound_order_id])
    
    respond_to do |format|
      if params[:outbound_orderline][:parent_linenr].to_s.blank?  || (@outbound_orderline.outbound_orderlines.empty? && params[:outbound_orderline][:cd_no].to_s.blank?)
        @outbound_orderline.update_attributes(strong_params)
        flash.notice = 'Outbound orderline updated.'
        format.html { redirect_to :action => "edit",:controller => "outbound_orders", :id => @outbound_order.id }
        format.json  { head :no_content }
      else
        if params[:outbound_orderline][:cd_no].to_s.blank?
          flash[:alert] = 'Outbound orderlines changed during your edit session, please retry.'
        else
          flash[:alert] = 'A medium can not be part of another outbound orderline.'
        end
        format.html { render :action => "edit" }
        format.json { render :json => @outbound_orderlines.errors, :status => :unprocessable_entity }
      end
    end
  end

  def report
    @outbound_orderline = OutboundOrderline.find(params[:id])
    @product_location = @outbound_orderline.product_location
    @conversion = @product_location.conversion
    @production_order = @conversion.production_orderline.production_order rescue nil
    @input_report_data = @conversion.input_report_data
  end

  private
  def sort_column
    params[:sort] || 'outbound_orderlines.id'
  end

  def sort_direction
    params[:direction] || 'desc'
  end

  def search
    params[:search] || ''
  end
end
