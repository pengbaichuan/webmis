require 'zip/zipfilesystem'
class OutboundOrdersController < ApplicationController
  before_filter :authorize
  authorize_resource

  layout "webmis"
  helper_method :sort_column, :sort_direction, :search
  
  def index
    if params[:reset]
      params.delete :search
      params.delete :reset

      params.delete :sort
      params.delete :direction
      params[:removed] = false
    end

    conditions = "1=1 "
    conditions_string_ary = []
    conditions_param_values = []
    
    unless params[:search].blank?
      conditions_string_ary << "and (outbound_orders.shipto_name LIKE ?
          OR outbound_orders.id LIKE ?
          OR outbound_orders.contact_person LIKE ?
          OR outbound_orderlines.cd_no LIKE ?
          OR outbound_orderlines.product_id LIKE ?
          OR outbound_orderlines.filename LIKE ?)"

      6.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
    end
    conditions = conditions + conditions_string_ary.join(" ")

    @outbound_orders = OutboundOrder.joins(:outbound_orderlines).group('outbound_orders.id').where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])

    if request.xhr?
      render(:partial => "outbound_order", :collection => @outbound_orders)
    end

  end

  def new
    @outbound_order = OutboundOrder.new
    @input_type = "mediums" if params[:mediums]
    conditions = "stock.status = 'ARCHIVED'"
      
    #@mediums_grid = initialize_grid(Stock,:name => 'mediums',:conditions => conditions,:order => 'id', :order_direction => 'desc' ,:per_page => 15)
    
    if params[:bulk] && !params[:distribution_list_id].blank?
      @outbound_order.contact_id = DistributionList.find(params[:distribution_list_id]).distribution_list_members.first.external_contact_id
      @outbound_order.courier = DistributionList.find(params[:distribution_list_id]).distribution_list_members.first.transfer_method
    end
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @outbound_order }
    end
  end

  def edit
    @outbound_order = OutboundOrder.find(params[:id])
    @input_type = params[:input_type] if params[:input_type]
    @input_type = "mediums" if params[:mediums]
    if params[:search] && params[:search] != ""
      conditions = "stock.status = 'ARCHIVED'" + params[:search]
    else
      conditions = "stock.status = 'ARCHIVED'"
    end       
    #@mediums_grid = initialize_grid(Stock,:conditions => conditions,:order => 'id', :order_direction => 'desc' ,:per_page => 15)
    @outboundorderlines = @outbound_order.outbound_orderlines
  end

  def create
    @outbound_order = OutboundOrder.new(strong_params)
    if params[:outbound_order][:contact_id] != ""
      contact = before_create_save(params[:outbound_order][:contact_id])
      @outbound_order.contact_person = contact[:contact_person]
      @outbound_order.shipto_name = contact[:shipto_name]
      @outbound_order.shipto_postalcode = contact[:shipto_postalcode]
      @outbound_order.shipto_streetname = contact[:shipto_streetname]
      @outbound_order.shipto_city = contact[:shipto_city]
      @outbound_order.shipto_state = contact[:shipto_state]
      @outbound_order.shipto_country = contact[:shipto_country]
      @outbound_order.shipto_id = contact[:shipto_id]
      @outbound_order.send_by = current_user.full_name
      @outbound_order.distribution_list_id = params[:distribution_list_id]
      
      #@outbound_order.save
      
      respond_to do |format|
        if @outbound_order.save
          flash[:notice] = 'Shipment is successfully registered.'
          format.html { redirect_to(:action => :ordr_lines, :id => @outbound_order.id, :input_type => params[:input_type],:bulk => true,:distribution_list_id => params[:distribution_list_id]) }
          format.xml  { render :xml => @outbound_order }     
        else 
          format.html { render :action => "new" }
          format.xml  { render :xml => @outbound_order.errors, :status => :unprocessable_entity }
        end
      end
    else             
      respond_to do |format |
        flash[:error] = 'Contact name cannot be empty!'
        format.html { redirect_to(:back) }
        format.xml  { render :xml => @outbound_order.errors, :status => :unprocessable_entity }
      end
    end   
  end

  def create_from_order
    
    if !params[:multiline].blank?
      @production_order = ProductionOrder.find(params[:production_order_id])
      @production_orderlines = @production_order.production_orderlines.where(:bp_name => "SHIPCONFIRM")
    else
      @production_orderlines = ProductionOrderline.where(:id => params[:production_order_line_id])
      @production_order = @production_orderlines.last.production_order
    end
    dl = DistributionList.find(@production_order.distribution_list_id)
    dlm = dl.distribution_list_members.first
    @outbound_order = OutboundOrder.new
    @outbound_order.distribution_list_id = @production_order.distribution_list_id
    @outbound_order.contact_id = dlm.external_contact_id
    @outbound_order.courier = dlm.transfer_method
    @outbound_order.distribution_list_member_id = dlm.id
    @outbound_order.send_by = current_user.full_name
    @outbound_order.send_date

    contact = before_create_save(@outbound_order.contact_id)
    @outbound_order.contact_person = contact[:contact_person]
    @outbound_order.shipto_name = contact[:shipto_name]
    @outbound_order.shipto_postalcode = contact[:shipto_postalcode]
    @outbound_order.shipto_streetname = contact[:shipto_streetname]
    @outbound_order.shipto_city = contact[:shipto_city]
    @outbound_order.shipto_state = contact[:shipto_state]
    @outbound_order.shipto_country = contact[:shipto_country]
    @outbound_order.shipto_id = contact[:shipto_id]
    @outbound_order.save
    
    @production_orderlines.each do |orderline|
      proceed = false
      outbound_orderline = OutboundOrderline.new
      if !orderline.stock.nil?
        # CD
        if orderline.stock.status == "ARCHIVED"
          proceed = true
          outbound_orderline.stock_id = orderline.stock.id
          outbound_orderline.cd_no = orderline.stock.medium_number
          outbound_orderline.filename = orderline.stock.databasename
        end
      end

      if !orderline.product_location.nil? && !proceed
        # Product
        proceed = true
        outbound_orderline.product_location_id = orderline.product_location.id
        outbound_orderline.filename = orderline.product_location.filename_from_storage_path
      end

      if proceed
        # General
        outbound_orderline.product_name = Product.where(:volumeid => "#{orderline.product_id}").first.name if Product.where(:volumeid => "#{orderline.product_id}").first
        outbound_orderline.product_id = orderline.product_id
        outbound_orderline.amount = dlm.hard_copy_amount if !dlm.hard_copy_amount.nil?
        outbound_orderline.outbound_order_id = @outbound_order.id
        outbound_orderline.production_orderline_id = orderline.id
        outbound_orderline.save
      end
    end

    respond_to do |format |
      format.html { redirect_to(:action => :edit, :id => @outbound_order.id,:bulk => true,:distribution_list_id => params[:distribution_list_id],:return_to_controller => params[:return_to_controller],:return_to_action => params[:return_to_action],:return_to_id => params[:return_to_id]) }
      format.xml  { render :xml => @outbound_order.errors, :status => :unprocessable_entity }
    end
  end
  
  def clone_line
    org_outbound_order = OutboundOrder.find(params[:id])
    @outbound_order = org_outbound_order.dup
    @outbound_order.contact_id = nil
    @outbound_order.company_id = nil
    @outbound_order.contact_person = nil
    @outbound_order.shipto_name = nil
    @outbound_order.shipto_postalcode = nil
    @outbound_order.shipto_streetname = nil
    @outbound_order.shipto_city = nil
    @outbound_order.shipto_state = nil
    @outbound_order.shipto_country = nil
    @outbound_order.shipto_id = nil
    @outbound_order.send_by = current_user.full_name
    @outbound_order.save
    
    outbound_order_lines = OutboundOrderline.where(:outbound_order_id => params[:id])
    outbound_order_lines.each do |ol|
      outbound_order_line = ol.dup
      outbound_order_line.outbound_order_id = @outbound_order.id
      outbound_order_line.save
    end
    
    respond_to do |format|
      format.html { redirect_to(:action => :edit, :id => @outbound_order.id) }
      format.xml  { render :xml => @outbound_order.errors, :status => :unprocessable_entity }
    end
  end
  
  def update
    @input_type = params[:input_type] if params[:input_type]
    if params[:outbound_order][:id]
      @outbound_order = OutboundOrder.find(params[:outbound_order][:id])
    else
      @outbound_order = OutboundOrder.new(strong_params)
    end

    @outbound_order.remarks = params[:outbound_order][:remarks]
    if params[:outbound_order][:contact_id] != ""
      contact = before_create_save(params[:outbound_order][:contact_id])
      @outbound_order.contact_person = contact[:contact_person]
      @outbound_order.contact_id = params[:outbound_order][:contact_id]
      @outbound_order.company_id = contact[:shipto_name]
      @outbound_order.shipto_name = contact[:shipto_name]
      @outbound_order.shipto_postalcode = contact[:shipto_postalcode]
      @outbound_order.shipto_streetname = contact[:shipto_streetname]
      @outbound_order.shipto_city = contact[:shipto_city]
      @outbound_order.shipto_state = contact[:shipto_state]
      @outbound_order.shipto_country = contact[:shipto_country]
      @outbound_order.shipto_id = contact[:shipto_id]

      @outbound_order.outbound_orderlines.each do |ol|
        production_orderline = ol.production_orderline
        if !production_orderline.nil?
          production_orderline.transition if production_orderline.bp_name == 'SHIPMENT REQUESTED'
        end
      end

      @outbound_order.shipping_parent_id = @outbound_order.id
      respond_to do |format|
        if @outbound_order.update_attributes(strong_params) && params[:input_type] == "- new lines -"
          # a kludge, but the legacy way of working is poorly documented and we still need to support it and
          # trying to rewrite it is not worth the effort.
          # Here, the create bulk shipment (indicated by params[:bulk]) will create one outbound order for each
          # member on the distribution list. But for automatic shipment orders we only want one order as the packing and
          # upload to the fileserver will only be done once and the notification of all members of the distribution list 
          # will be done in one bulk mailing. Hence the check on courier != 'Automatic' to prevent the creation of multiple
          # outbound orders for the same product.
          if !params[:distribution_list_id].blank?
            dbl = DistributionList.find(params[:distribution_list_id])
          else
            dbl = @outbound_order.distribution_list
          end

          if params[:bulk] && dbl && @outbound_order.courier != 'Automatic'
            dbl.distribution_list_members.each do |member|
              next if member.external_contact_id.to_s == params[:outbound_order][:contact_id]
              @bulk_outbound_order = OutboundOrder.new(strong_params)
              @bulk_outbound_order.id = nil
              if member.external_contact_id != ""
                contact = before_create_save(member.external_contact_id)
                @bulk_outbound_order.contact_id = member.external_contact_id
                @bulk_outbound_order.contact_person = contact[:contact_person]
                @bulk_outbound_order.company_id = contact[:shipto_name]
                @bulk_outbound_order.shipto_name = contact[:shipto_name]
                @bulk_outbound_order.shipto_postalcode = contact[:shipto_postalcode]
                @bulk_outbound_order.shipto_streetname = contact[:shipto_streetname]
                @bulk_outbound_order.shipto_city = contact[:shipto_city]
                @bulk_outbound_order.shipto_state = contact[:shipto_state]
                @bulk_outbound_order.shipto_country = contact[:shipto_country]
                @bulk_outbound_order.shipto_id = contact[:shipto_id]
                @bulk_outbound_order.distribution_list_member_id = member.id
                @bulk_outbound_order.shipping_parent_id = @outbound_order.id
              end  
              @bulk_outbound_order.send_by = current_user.full_name
              @bulk_outbound_order.courier = member.transfer_method
              if member.hard_copy_amount.nil? || member.hard_copy_amount == 0
                @bulk_outbound_order.amount = "1"
              else
                @bulk_outbound_order.amount = member.hard_copy_amount
              end
              @bulk_outbound_order.save

              outbound_order_lines = OutboundOrderline.where(:outbound_order_id => params[:id])
              # For now: don't copy child outbound orderlines as they will have been packed in their parent orderline
              # This is not fool proof, but this is legacy code and just present to preserve the old way of working where necessary
              outbound_order_lines.where('parent_linenr is null').each do |ol|
                outbound_order_line = ol.dup
                outbound_order_line.outbound_order_id = @bulk_outbound_order.id
                outbound_order_line.save
              end
              
            end
            # destroy current record because a duplicate is created in loop
            @outbound_order = OutboundOrder.last
            @del_outbound_order= OutboundOrder.find(params[:id]).destroy
            @del_outbound_order_line = OutboundOrderline.where(:outbound_order_id => params[:id])
            @del_outbound_order_line.each do |x|
              x.destroy
            end
          end
          
          flash[:notice] = 'Shipment is successfully registered.'
          if !@outbound_order.release_mail_send
            format.html { redirect_to show_release_mail_outbound_order_path(@outbound_order)}
          elsif ((params[:return_to_controller] && params[:return_to_action]) && (params[:return_to_controller].to_s != "" && params[:return_to_action].to_s != ""))
            if params[:return_to_id].to_s == ""
              format.html { redirect_to :action => params[:return_to_action].to_s,
                :controller => params[:return_to_controller].to_s }
            else
              ret_id = params[:return_to_id].to_s
              format.html { redirect_to :action => params[:return_to_action].to_s,
                :controller => params[:return_to_controller].to_s, :id => ret_id }
            end
            format.xml  { render :xml => @outbound_order }
          else
            format.html { redirect_to(:action => :index)}
          end

        elsif params[:input_type] == "packing"
          format.html { redirect_to(:action => :edit_packing, :id =>@outbound_order.id)}
          format.xml { render :xml => @outbound_orders.errors, :status => :unprocessable_entity }
        else
          format.html { redirect_to(:action => :ordr_lines, :id => @outbound_order.id, :input_type => @input_type,:bulk => true,:distribution_list_id => params[:distribution_list_id]) }
          format.xml  { render :xml => @outbound_order.errors, :status => :unprocessable_entity }
        end
      end
    else

      respond_to do |format |
        flash[:error] = 'Contact name cannot be empty!'
        format.html { redirect_to(:back) }
        format.xml  { render :xml => @outbound_order.errors, :status => :unprocessable_entity }
      end
    end
  end

  def ordr_lines
    conditions = '1=1'
    @outbound_order = OutboundOrder.find(params[:id])
    if @outbound_order.contact_id == ""
      flash[:error] = 'Contact name cannot be empty!'
    end  
    if params[:setcode] && params[:setcode] != ""
      conditions = "stock.status = 'ARCHIVED'" if params[:input_type] == "mediums"
    else
      conditions = "stock.status = 'ARCHIVED'" if params[:input_type] == "mediums"
    end
    
     if ((params[:setcode] && params[:setcode].strip != '') && (params[:input_type] && params[:input_type] == "mediums"))
      ps = Productset.find(params[:setcode])
      #params.delete(:setcode)
      @ps = ps
      products = ps.includedproducts
      s = "("
      products.each do |product|
        s = s + "'" + product.product.volumeid.to_s + "',"
      end
      s = s[0,s.size-1]
      s = s + ")"
      if s.size > 2
        conditions = conditions + " and product_ref in #{s} "
      end
    end
    
    if ((params[:product_id] && params[:product_id] != '') && (params[:input_type] && params[:input_type] == "products"))
      conditions = "product_id LIKE '%#{params[:product_id]}%'" 
      params.delete(:product_id)
    else
      conditions = '1=1' if params[:input_type] == "products";
    end
    
    @input_type = params[:input_type]
    @outbound_order = OutboundOrder.find(params[:id])
    @outboundorderlines = OutboundOrderline.where(:outbound_order_id => params[:id])
    @mediums_grid = Stock.where(conditions).order('id DESC').limit('50').all
    if @mediums_grid.size == 0 && params[:input_type] == "mediums"
      flash[:warning] = 'No media found.'
    end
    @products_grid = ProductLocation.where(conditions).order('created_at DESC').limit('50') if params[:input_type] == "products"
    @outbound_order_line = OutboundOrderline.new
    @ol = OutboundOrderline.new
  end
  
  def get_order_lines_mediums

    @outbound_order = OutboundOrder.find(params[:id])
    @mediumlines = params[:mediumlines]
    
    if @mediumlines
      @mediumlines.each do |x|
        @stock = Stock.find(x)
        @outboundorderline = OutboundOrderline.new
        @outboundorderline.cd_no = @stock.medium_number
        @outboundorderline.filename = @stock.databasename
        @outboundorderline.product_id = @stock.product_ref
        @outboundorderline.product_name = @stock.commercial_name
        @outboundorderline.outbound_order_id = params[:id]
        @outboundorderline.stock_id = @stock.id
        @outboundorderline.amount = params[:amount][x]
        if @dupl_line = OutboundOrderline.where(:cd_no => @stock.medium_number, :outbound_order_id => params[:id])
          if @dupl_line != []
            OutboundOrderline.delete(@dupl_line)
            @duplicate = "yes" 
          end          
        end
        @outboundorderline.save    
      end
    end
    
  respond_to do |format |
    if @outboundorderline  
      if (@duplicate == "yes")
        flash[:notice] = 'Media added, and/or replaced.'
      else
      flash[:notice] = 'Media added.'
      end
    end
    if params[:setcode] && params[:setcode].size > 0
      format.html { redirect_to(:action => :ordr_lines, :id => @outbound_order.id, :input_type => "mediums",:bulk => true,:distribution_list_id => params[:distribution_list_id]) }
      format.xml  { render :xml => @outbound_order.errors, :status => :unprocessable_entity }
    else
      format.html { redirect_to(:action => :edit, :id => @outbound_order.id,:bulk => true,:distribution_list_id => params[:distribution_list_id]) }
      format.xml  { render :xml => @outbound_order.errors, :status => :unprocessable_entity }
    end
      end
    
    
  end
  
  def get_order_lines_products

    @outbound_order = OutboundOrder.find(params[:id])
        if params[:bulk] && !params[:distribution_list_id].blank?
          
        end
    @productlines = params[:productlines]
    
    if @productlines
      @productlines.each do |x|
        @product = ProductLocation.find(x)
        @outboundorderlines = OutboundOrderline.new
        @outboundorderlines.filename = @product.filename_from_storage_path
        @outboundorderlines.product_id = @product.product_id
        @outboundorderlines.product_location_id = @product.id
        
        if @outboundorderlines.product_id && @outboundorderlines.product_id.to_s != ''
          # fu..ing dot damage control
          if @outboundorderlines.product_id.to_s.scan(/\A(.*-\d{4}\.1)/)
            prod_ref = @outboundorderlines.product_id.to_s.scan(/\A(.*-\d{4})/).join
          else
            prod_ref = @outboundorderlines.product_id.to_s
          end
        end
        
        @outboundorderlines.product_name = Product.where(:volumeid => "#{prod_ref}").first.name if Product.where(:volumeid => "#{prod_ref}").first
        @outboundorderlines.outbound_order_id = params[:id]
        @outboundorderlines.amount = "1"
        if @dupl_line = OutboundOrderline.where(:filename => @product.filename_from_storage_path, :outbound_order_id => params[:id])
          if @dupl_line != []
            @dupl_line.collect{|dl|OutboundOrderline.delete(dl)}
            @duplicate = "yes" 
          end          
        end
        @outboundorderlines.save    
      end
    end
    
  respond_to do |format |
    if @outboundorderlines  
      if (@duplicate == "yes")
        flash[:notice] = 'Product(s) added, and/or replaced.'
      else
      flash[:notice] = 'Product(s) added.'
      end
    end
        format.html { redirect_to(:action => :edit, :id => @outbound_order.id,:bulk => true,:distribution_list_id => params[:distribution_list_id]) }
        format.xml  { render :xml => @outbound_order.errors, :status => :unprocessable_entity }
    end
    
    
  end
  
  def get_order_lines_manually
    @outbound_order = OutboundOrder.find(params[:id])
    @outbound_order_line = OutboundOrderline.new(strong_params("outbound_orderline"))
    @outbound_order_line.outbound_order_id = params[:id]
    
    
    respond_to do |format |
      if @outbound_order_line.save
        flash[:notice] = 'Manually added.'
        format.html { redirect_to(:action => :edit, :id => @outbound_order.id,:bulk => true,:distribution_list_id => params[:distribution_list_id]) }
        format.xml  { render :xml => @outbound_order.errors, :status => :unprocessable_entity }
      else
        format.html { render :action => "ordr_lines" }
        format.xml  { render :xml => @outbound_order.errors, :status => :unprocessable_entity }
      end
    end
  
  end
  
  def edit_packing
    @packing = Packing.new(:status => Packing::STATUS_DRAFT)
    @outbound_order = OutboundOrder.find(params[:id])
  end

  def update_packing
    @outbound_order = OutboundOrder.find(params[:id])
    begin
      OutboundOrder.transaction do
        @packing = Packing.new(strong_params("packing"))
        @outbound_order.packings << @packing
        # a save! would raise an exception, in which case the validation errors are lost
        if @packing.save
          # reload needed to obtain the packing.id
          @packing.reload
          params[:packinglines].each do |id|
            outbound_orderline = @outbound_order.outbound_orderlines.find(id)
            packing_line = PackingLine.new
            outbound_orderline.packing_lines << packing_line
            @packing.packing_lines << packing_line
            packing_line.save
            # add the packing_line errors to the packing errors so they can be displayed in the edit_packing view.
            packing_line.errors.each {|e| @packing.errors[:base] << e }
          end
        end

        # trigger rollback if one of the packinglines gave an error
        raise "New packing could not be saved!" unless @packing.errors.empty?
      end
      @packing.create_packing_job
      @packing.update_status
      redirect_to outbound_order_path @outbound_order
    rescue => e
      @packing = Packing.new(strong_params("packing"))
      respond_to do |format |
        puts e.message
        puts e.backtrace
        flash[:error] = e.message
        # to make sure the bungled pack is not in @outbound_order.packs
        @outbound_order.reload
        format.html { render :edit_packing }
        format.xml  { render :xml => @outbound_order.errors, :status => :unprocessable_entity }
      end
    end
  end

  def cancel_packing
    @outbound_order = OutboundOrder.find(params[:id])
    packing = @outbound_order.packings.find(params[:packing_id])
    respond_to do |format|
      if packing && packing.cancel(current_user)
        flash.notice = "Packing cancelled."
      elsif packing
        flash.alert = "Could not cancel packing."
      else
        flash.alert = "Packing not found."
      end
      @outbound_order.update_status
      format.html { redirect_to :action => :edit_packing }
      format.xml { render :xml => @outbound_order.errors, :status => :unprocessable_entity }
    end
  end
  
  def show
    @outboundorder = OutboundOrder.find(params[:id])
    @outboundorderlines = @outboundorder.outbound_orderlines
  end

  def before_create_save(record)
    e = ExternalContact.find(record)
    c = e.company
    ec = Hash.new
    ec[:contact_person] = e.first_name.to_s + " " +  e.last_name.to_s.to_s
    ec[:shipto_name] = c.name
    ec[:shipto_postalcode] = c.shipping_address_postalcode
    ec[:shipto_streetname]  = c.shipping_address_street
    ec[:shipto_city]  = c.shipping_address_city
    ec[:shipto_state]  = c.shipping_address_state
    ec[:shipto_country]  = c.shipping_address_country
    ec[:shipto_id] = c.id
    return ec
  end

  def before_update_save(record)
    before_create_save(record)
  end

  def livesearch
    conditions = "1=1"
    @stock = ""
    if params[:filter] && params[:filter] != "" && (params[:filter].size >= 3) 
      conditions = conditions + " and (databasename like '%"+params[:filter]+"%' or medium_number like '%#{params[:filter]}%')"
      @stock = Stock.where(conditions)
    end
    render :layout => false
  end

  def create_out_order(c, ec,stock,amount,courier,lines,remarks)

    o = OutboundOrder.new
    o.company_id = c.id
    o.send_date = DateTime.now
    o.contact_person = ec.first_name.to_s + " " + ec.last_name.to_s
    o.shipto_name = c.name
    o.type = stock.cd_type
    o.shipto_postalcode = c.shipping_address_postalcode
    o.shipto_streetname = c.shipping_address_street
    o.shipto_country =  c.shipping_address_country
    o.send_by = current_user.first_name + " " + current_user.last_name
    o.map_no = stock.id
    o.cd_no = stock.medium_number
    o.amount = amount
    o.filename = stock.databasename
    o.from_name = o.send_by
    o.courier = courier
    o.contact_id = ec.id
    o.remarks = remarks
    
    if ec.company
      o.shipto_id = ec.company.id
    end
    
    o.save

    lines.each do |line|
      
      ol = OutboundOrderline.new
      ol.amount = amount
      if line.product_ref && line.product_ref.to_s != ''
      ol.product_id = line.product_ref
      else
        ol.product_id = "n.a."
      end
      ol.product_name = line.commercial_name
      ol.outbound_order_id = o.id
      ol.stock_id = line.id
      ol.filename = line.databasename
      ol.cd_no = line.medium_number
      ol.map_no = line.location
      
      ol.save     
      o.outbound_orderlines << ol
     
    end   
    o.save
  end

  def delete_me
    @outbound_order= OutboundOrder.find(params[:id]).destroy
    @outbound_order_line = OutboundOrderline.where(:outbound_order_id => params[:id])
    @outbound_order_line.each do |x|
      x.destroy
    end
    
   # format.html { redirect_to(:action => :edit,:controller => "outbound_orders", :id => @outbound_order.id) }
    respond_to do |format|
      flash[:notice] = "Shipment #{@outbound_order.id.to_s} is successfully deleted."      
      format.html { redirect_to(:action => :index, :id => @outbound_order.id) }
      format.xml  { render :xml => @outbound_order.errors, :status => :unprocessable_entity }
    end
  end
  
  def release_notes_request
    
    if params[:po_ps_id]
      @stock = {}
      @methode = "c"
      @productset = Productset.find(params[:po_ps_id])
      @productset.includedproducts.each do |p|
        pid = Product.find(p.product_id,:order => 'volumeid' ).volumeid      
        @stock["#{pid}"] = Stock.where("product_ref like \'#{pid}%%\'")
      end 
    end
    carin = ProductCategory.where('product_categories.abbreviation LIKE "CAR%"').first
    if carin.nil?
      @productsets = Productset.where('status = 20 and (removed_yn is null OR removed_yn = 0)').order(:setcode)
    else
      @productsets = Productset.where("product_category_id = #{carin.id} AND status = 20 and (removed_yn is null OR removed_yn = 0)").order(:setcode)
    end
    
  end
  
  def get_products_for_request
    @stock = {}
    if params[:id] != ""
        @productset = Productset.find(params[:id])
    

    @productset.includedproducts.each do |p|
      pid = Product.find(p.product_id).volumeid      
      @stock["#{pid}"] = Stock.where("product_ref like \'#{pid}%%\'")

    end

    @methode = params[:methode]
    end
    render :partial => "products_for_request"
  end
  
  def send_release_notes_request   
  allow_request = true
  @ps =  Productset.find(params[:productset_c][:id])
  @current = []
  @equivalent = []
  @remarks = "Requested by " + 
    current_user.first_name + " " + current_user.last_name + 
    ".<br /><br />" + params[:body]
  notice = ""

    if params[:productset_c][:id] != ""     
     # params[:c] is a hash with Stock.id's of current set
     params[:c].each do |c|
      if c[1] == "1"
        @current << c[0]
      end
    end

    if params[:productset_e][:id] != ""
        # params[:e] is a hash with Stock.id's of equivalent set     
        params[:e].each do |e|
          if e[1] == "1"
            @equivalent << e[0] 
          end     
        end
    else
      params[:productset_e][:id] = '0'
    end
   
   # update (new) productionorder of current set.
   @current.each do |stockid|
     @stock = Stock.find(stockid)
      poln = Stock.find(stockid).conversion.production_orderline if Stock.find(stockid).conversion
      if !poln.nil?
          #poln.bp_name = 'RELEASE'
          #poln.save
          po = poln.production_order
          @poln_bpnames = poln.production_order.production_orderlines.collect {|pol| pol.bp_name}.uniq
          @poln_bpnames.delete_if{|p|p == "RELEASE"}
          @poln_bpnames.delete_if{|p|p == "CANCELLED"}
          @poln_bpnames.delete_if{|p|p == "COMPLETE"}
          if @poln_bpnames.size == 0
              #po.status = 'RELEASE'
              po.rel_notes_requested_by_id = current_user.id
              po.rel_notes_requested_date = Time.now              
              po.save
              notice = notice + "Production-order #{po.id} updated for #{poln.product_id}.<br />"           
          else
              notice = notice + "Production-order #{po.id}-#{po.name} has unfinished product(s)!<br />"
              allow_request = false
          end
      else
        notice = notice + "#{@stock.product_ref} is not included in, or related to production order!<br />"
        allow_request = false
      end
    end  

   if allow_request
     puts '$$$ DEBUG: REQUEST WAS ALLOWED'
     Postoffice.carin_rel_notes_request(params[:productset_c][:id],params[:productset_e][:id],@current,@equivalent,@remarks).deliver
   end  
      
    respond_to do |format|
      if allow_request
        flash[:notice] = notice + "Request succesfully send."
      else
        flash[:error] = notice + "Request not send."
      end    
      format.html { redirect_to(:action => :release_notes_request, :id => 666) }
      format.xml  { render :xml => @outbound_order.errors, :status => :unprocessable_entity }
    end
    else
     respond_to do |format|
      flash[:error] = "Current set selection is required!."      
      format.html { redirect_to(:action => :release_notes_request, :id => 666) }
      format.xml  { render :xml => @outbound_order.errors, :status => :unprocessable_entity }
    end     
    end
  end
    
  def html2pdf_slip
    @outbound_order = OutboundOrder.find(params[:id])
    if @outbound_order.outbound_orderlines.size > 0
        @lines = @outbound_order.outbound_orderlines
    else
        @lines = []
    end
    
    @cu = current_user.full_name

    respond_to do |format|
    format.pdf do
            render :pdf => "packing_slip_#{@outbound_order.id}",
              :margin => { :bottom => 20 },
              :footer => { :html => { :template => '/pdf_footer', :formats => [:html] }},
              :encoding => "utf8" 
          end
    end
  end
  
  def show_release_mail
    @outbound_order = OutboundOrder.find(params[:id])
    @possible_release_notes = @outbound_order.possible_release_notes
    @current_user = current_user
  end
  
  def send_bulk_release_mail
    @outbound_order = OutboundOrder.find(params[:id])
    rn = params[:rn].to_i == 0
    rn_id = params[:rn_id].blank? ? nil : params[:rn_id].to_i
    distribution_list = @outbound_order.distribution_list || @outbound_order.distribution_list_member.distribution_list
    Postoffice.product_release_mail(@outbound_order, distribution_list.electronic_delivery_email_list, current_user. email, rn, rn_id).deliver_now
    OutboundOrder.where(:shipping_parent_id => @outbound_order.shipping_parent_id).each do |grouped_order|
      grouped_order.release_mail_send = true
      grouped_order.release_mail_send_date = Time.current
      grouped_order.save
    end

    flash[:notice]='Shipment notification is successfully send.'
    if request.xhr?
      render :js => "window.location.pathname='#{outbound_orders_path}'"
    else
      respond_to do |format|
        format.html { redirect_to outbound_orders_url,:notice => 'Shipment notification is successfully send.' }
        format.xml  { render :xml => @outbound_order }
      end
    end
  end
  
  def send_release_mail
    @outbound_order = OutboundOrder.find(params[:id])
    rn = params[:rn].to_i == 0
    rn_id = params[:rn_id].blank? ? nil : params[:rn_id].to_i
    Postoffice.product_release_mail(@outbound_order, @outbound_order.distribution_list_member.external_contact.email, current_user.email, rn, rn_id).deliver_now
    @outbound_order.release_mail_send = true
    @outbound_order.release_mail_send_date = Time.current

    flash[:notice]='Shipment notification is successfully sent.'
    if request.xhr? &&  @outbound_order.save
      render :js => "window.location.pathname='#{outbound_orders_path}'"
    else
      respond_to do |format|
        if @outbound_order.save
          flash[:notice] = 'Shipment notification is successfully sent.'
          format.html { redirect_to :back,:notice => 'Shipment notification is successfully sent.' }
          format.xml  { render :xml => @outbound_order }
        else
          flash[:error] = 'Shipment notification not sent.'
          format.html { redirect_to(:back) }
          format.xml  { render :xml => @outbound_order.errors, :status => :unprocessable_entity }
        end
      end
    end
  end

  def send_release_mail_to_self
    @outbound_order = OutboundOrder.find(params[:id])
    rn = params[:rn].to_i == 0
    rn_id = params[:rn_id].blank? ? nil : params[:rn_id].to_i
    Postoffice.product_release_mail(@outbound_order, nil, current_user.email, rn, rn_id).deliver_now
    OutboundOrder.where(:shipping_parent_id => @outbound_order.shipping_parent_id).each do |grouped_order|
        grouped_order.release_mail_send = true
        grouped_order.release_mail_send_date = Time.current
        grouped_order.save
    end
    if request.xhr?
      flash[:notice]='A copy of the shipment notification was sent to you. Order registered as send.'
      render :js => "window.location.pathname='#{outbound_orders_path}'"
    else
      respond_to do |format|
        format.html { redirect_to :back, :notice => 'A copy of the shipment notification was sent to you. Order registered as send.'}
        format.xml {render :xml => @outbound_order }
      end
    end
  end
  
  private  
  def sort_column  
    params[:sort] || "outbound_orders.id"  
  end  
    
  def sort_direction  
    params[:direction] || "desc"  
  end
  
  def search
    params[:search] || ""
  end  
end
