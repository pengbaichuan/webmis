class PackingJobsController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search
  # GET /packing_jobs
  # GET /packing_jobs.json
  def index
    if params[:reset]
      params.delete :search_status
      params.delete :search
      params.delete :sort
      params.delete :direction

      params.delete :sort
      params.delete :direction

      # when from jobs/tagged_failed_jobs_report
      params.delete :date_range_a
      params.delete :date_range_b
      params.delete :fail_category
    end

    conditions_string_ary = ['1=1']
    conditions_param_values = []

    status = params[:search_status]
    unless status.blank?
      conditions_string_ary << "(jobs.status = ?)"
      conditions_param_values << status
    end

    # when from jobs/tagged_failed_jobs_report
    unless params[:fail_category].blank?
      range_end = Date.today.strftime("%d-%m-%Y")
      unless params[:date_range_a].blank?
        range_begin = params[:date_range_a]
        if !params[:date_range_b].blank?
          range_end = params[:date_range_b]
        end
      else
        range_begin = PackingJob.fail_reason_tagged.first.fail_category_logged_on.strftime("%d-%m-%Y")
      end

      unless params[:date_range_b].blank?
        if params[:date_range_a].blank?
          range_end = Date.today.strftime("%d-%m-%Y")
        end
      end
      conditions_string_ary << "(jobs.fail_category_logged_on BETWEEN '#{DateTime.parse(range_begin)}' AND '#{DateTime.parse(range_end)}')"
      if params[:fail_category] != 'all'
        conditions_string_ary << "(jobs.fail_category = ?)"
        conditions_param_values << params[:fail_category]
      end
    end

    unless params[:search].blank?
      q = params[:search].strip
      conditions_string_ary << "(jobs.id = ? OR
                                 packings.filename LIKE ? OR
                                 jobs.run_server LIKE ?)"
      conditions_param_values << q.to_i # for id
      2.times do
        conditions_param_values << "%#{q}%"
      end
    end

    conditions = conditions_string_ary.join(" AND ")
    @packing_jobs = PackingJob.includes(:packing,:outbound_order).references(:packings).where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(per_page: 30, page: params[:page])

    if request.xhr?
      render(:partial => "packing_job", :collection => @packing_jobs)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @packing_jobs }
      end
    end
  end

  def failures
    if params[:sort].blank?
      params[:sort] = sort_column
      params[:direction] = sort_direction
    end
    @packing_jobs = PackingJob.failures.includes(:packing,:outbound_order).order(sort_column + ' ' + sort_direction)
    @packing_jobs = @packing_jobs.paginate(per_page: 30, page: params[:page])

    if request.xhr?
      render(:partial => "failed_packing_job", :collection => @packing_jobs)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @packing_jobs }
      end
    end
  end

  def tagging_fail_reason
    @success_rows = []
    params[:ids].each do |job_id|
      job = PackingJob.find(job_id)
      job.fail_category = params[:fail_reason]
      job.fail_category_logged_by_user_id = current_user.id
      job.fail_category_logged_on = Time.now
      if job.save
        @success_rows << job.id
      end
    end
    render :layout => false
  end

  # GET /packing_jobs/1
  # GET /packing_jobs/1.json
  def show
    @packing_job = PackingJob.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @packing_job }
    end
  end

  # GET /packing_jobs/new
  # GET /packing_jobs/new.json
  def new
    @packing_job = PackingJob.new(:project_id => session[:project_id])

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @packing_job }
    end
  end

  # GET /packing_jobs/1/edit
  def edit
    @packing_job = PackingJob.find(params[:id])
  end

  # POST /packing_jobs
  # POST /packing_jobs.json
  def create
    @packing_job = PackingJob.new(strong_params)

    respond_to do |format|
      if @packing_job.save
        format.html { redirect_to @packing_job, notice: 'Packing job was successfully created.' }
        format.json { render json: @packing_job, status: :created, location: @packing_job }
      else
        format.html { render action: "new" }
        format.json { render json: @packing_job.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /packing_jobs/1
  # PUT /packing_jobs/1.json
  def update
    begin
      PackingJob.transaction do
        @packing_job = PackingJob.find(params[:id])
        server_name = params[:packing_job][:run_server].blank? ? '-' : params[:packing_job][:run_server]

        if params[:commit] == "Execute"
          @packing_job.stdout_log = "Job is manually executed on server '#{server_name}'."
          @packing_job.set_status(Job::STATUS_MANUAL)
          perform_now = true
        elsif params[:commit] == "Schedule"
          @packing_job.stdout_log = "Job is manually queued" + (server_name == '-' ? '.' : " on server '#{server_name}'.")
          @packing_job.set_status(Job::STATUS_QUEUED)
          perform_now = false
        elsif params[:commit] == "Save"
          @packing_job.stdout_log = "Job is saved."
          @packing_job.set_status(Job::STATUS_MANUAL)
          perform_now = false
        else
          raise "Unknown submit operation '#{params[:commit]}'."
        end
        @packing_job.fail_reason = ''
        @packing_job.log_scheduler("Job manually set to #{@packing_job.status_str} for server #{server_name}.", current_user)
        @packing_job.log_action(@packing_job.stdout_log, current_user)

        respond_to do |format|
          if @packing_job.update_attributes!(strong_params)

            @packing_job.perform if perform_now

            format.html { redirect_to @packing_job, notice: 'Packing job was successfully updated.' }
            format.json { head :no_content }
          else
            flash[:error] = "Error updating job"
            format.html { render action: "edit" }
            format.json { render json: @packing_job.errors, status: :unprocessable_entity }
          end
        end
      end
    rescue ActiveRecord::StaleObjectError
      flash[:error] = "This job was changed while you were editing it. Please try again."
      redirect_to :action => 'edit'
    end
  end

  # DELETE /packing_jobs/1
  # DELETE /packing_jobs/1.json
  def destroy
    @packing_job = PackingJob.find(params[:id])
    @packing_job.destroy

    respond_to do |format|
      format.html { redirect_to packing_jobs_url }
      format.json { head :no_content }
    end
  end

  def clean_working_dir
    @packing_job = PackingJob.find(params[:id])
    @packing_job.clear_working_dir
    redirect_to :action => "show"
  end

  def cancel
    begin
      @packing_job = PackingJob.find(params[:id])
      @packing_job.lock_version = params[:lock_version]
      @packing_job.cancel(current_user.full_name)
    rescue ActiveRecord::StaleObjectError => e
      puts e.message
      puts e.backtrace
      flash[:error] = "This job was changed while you were cancelling it."
    end
    redirect_to :action => "show"
  end

  def edit_new_copy
    @packing_job_org = PackingJob.find(params[:id])
    @packing_job_org.lock_version = params[:lock_version] unless params[:lock_version].blank?
    if @packing_job_org.allow_reschedule?
      begin
        PackingJob.transaction do
          @packing_job = @packing_job_org.reschedule(current_user, manual_yn = true)
          redirect_to :action => 'edit', :id => @packing_job.id
        end
      rescue ActiveRecord::StaleObjectError
        flash[:error] = 'This job was changed while you were editing it. Please try again.'
        redirect_to :action => 'show', :id => @packing_job_org.id
      rescue => e
        flash[:error] = "Error rescheduling: #{e.message}"
        redirect_to :action => 'show', :id => @packing_job_org.id
      end
    else
      if @packing_job_org.busy?
        flash[:error] = 'Editing a new copy of this job is not allowed yet. Please wait till move action is finished.'
      else
        flash[:error] = 'Editing a new copy of this job is no longer allowed. The production orderline it belongs to probably just finished.'
      end
      redirect_to :action => 'show', :id => current_job.id
    end
  end


  def regenerate
    @packing_job_org = PackingJob.find(params[:id])
    @packing_job_org.lock_version = params[:lock_version] unless params[:lock_version].blank?
    if @packing_job_org.allow_reschedule?
      begin
        @packing_job = @packing_job_org.regenerate(current_user)
      rescue ActiveRecord::StaleObjectError
        flash[:error] = 'This job was changed while you were editing it. Please try again.'
        redirect_to :action => 'show', :id => @packing_job_org.id
      rescue => e
        flash[:error] = "Error regenerating : #{e.message}"
        redirect_to :action => 'show', :id => @packing_job_org.id
      end
      redirect_to :action => 'show', :id => @packing_job.id
    else
      flash[:error] = 'Rescheduling is no longer allowed. The outbound order this job belongs to probably just finished.'
      redirect_to :action => 'show', :id => @packing_job_org.id
    end
  end

  def reschedule
    @packing_job_org = PackingJob.find(params[:id])
    @packing_job_org.lock_version = params[:lock_version] unless params[:lock_version].blank?
    if @packing_job_org.allow_reschedule?
      begin
        @packing_job = @packing_job_org.reschedule(current_user)
      rescue ActiveRecord::StaleObjectError
        flash[:error] = 'This job was changed while you were editing it. Please try again.'
        redirect_to :action => 'show', :id => @packing_job_org.id
      rescue => e
        flash[:error] = "Error rescheduling : #{e.message}"
        redirect_to :action => 'show', :id => @packing_job_org.id
      end
      redirect_to :action => 'show', :id => @packing_job.id
    else
      flash[:error] = 'Rescheduling is no longer allowed. The outbound order this belongs to probably just finished.'
      redirect_to :action => 'show', :id => @packing_job_org.id
    end
  end

  private
    def sort_column
      params[:sort] || "jobs.id"
    end

    def sort_direction
      params[:direction] || "desc"
    end

    def search
      params[:search] || ""
    end
end
