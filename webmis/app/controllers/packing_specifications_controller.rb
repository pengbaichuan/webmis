class PackingSpecificationsController < ApplicationController
  before_filter :authorize
  authorize_resource

  layout "webmis"
  helper_method :sort_column, :sort_direction, :search

  def index
    conditions = '1=1'
    if params[:reset]
      params.delete :search
      params.delete :search_status
      params.delete :obsolete
      params.delete :reset

      params.delete :sort
      params.delete :direction
    end

    conditions_string_ary = []
    conditions_param_values = []

    include_obsolete = params[:obsolete]

    status = params[:search_status]
    if status.blank?
      conditions_string_ary << ' and (status is null or status !="obsolete")' unless include_obsolete
    else
      conditions_string_ary << ' and (status=?)'
      conditions_param_values << status
      include_obsolete ||= (status == 'obsolete')
    end

    params[:obsolete] = include_obsolete
    conditions = conditions + conditions_string_ary.join(" ")

    if (sort_column == 'latest_approved')
      @packing_specifications = PackingSpecification.where([conditions]+conditions_param_values).sort_by{ |r| r.latest_approved ? r.latest_approved.version : 0 }
      @packing_specifications.reverse! if sort_direction == 'desc'
    else
      @packing_specifications = PackingSpecification.where([conditions]+conditions_param_values).order(sort_column + ' ' + sort_direction)
    end

    unless params[:search].blank?
      search = params[:search].strip
      matched_packing_specifications = []
      @packing_specifications.each do |packing_specification|
        if ( ( packing_specification.name && packing_specification.name.include?(search) ) ||
             ( packing_specification.description && packing_specification.description.include?(search) )
           )
          matched_packing_specifications << packing_specification
        end
      end
      @packing_specifications = matched_packing_specifications
    end

    @packing_specifications = @packing_specifications.paginate(per_page: 100, page: params[:page])
    if request.xhr?
      render(:partial => "packing_specification", :collection => @packing_specifications)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @packing_specifications }
      end
    end
  end

  # GET /packing_specifications/1
  # GET /packing_specifications/1.json
  def show
    @packing_specification = PackingSpecification.find(params[:id])
  end

  def approved
    spec = PackingSpecification.find(params[:id]).use_latest_approved
    @packing_specification = spec.use_version(spec.version)
    respond_to do |format|
      format.html { render :action => 'show' }
      format.json { render :json => @packing_specification }
    end
  end

  def handle
    @packing_specification = PackingSpecification.find(params[:id])
    @packing_specification_version = @packing_specification
    @current_user = current_user
  end

  # GET /packing_specifications/1/edit
  def edit
    @packing_specification = PackingSpecification.find(params[:id])
    # the default new status of an edit should always be ':draft'
    @packing_specification.status = :draft
    @packing_specification.bug_tracker_id = nil
    @packing_specification.comment = nil
  end

  # POST /packing_specifications
  # POST /packing_specifications.json
  def create
    # for new packing_specifications, first create one with status new and then edit it as a regular packing_specification
    # note that we skip the traditional 'new' method here
    @packing_specification = PackingSpecification.new(:status => 'new', :comment => 'manual initial version', :updated_by => session[:username])
    respond_to do |format|
      if @packing_specification.save(:validate => false)
        format.html { redirect_to( { :action => 'edit', :id => @packing_specification.id }, :notice => 'New packing_specification created.') }
        format.json { render :json => @packing_specification, :status => :created, :location => @packing_specification }
      else
        format.html { redirect_to packing_specifications_url, :alert => 'Could not create new packing specification.' }
        format.json { render :json => @packing_specification.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /packing_specifications/1
  # PUT /packing_specifications/1.json
  def update
    @packing_specification = PackingSpecification.find(params[:id])
    respond_to do |format|
      if @packing_specification.update_attributes(strong_params.update(:status => 'draft', :updated_by => session[:username]))
        format.html { redirect_to({ :action => 'handle', :id => @packing_specification.id } , :notice => 'Packing specification was successfully updated.') }
        format.json { head :no_content }
      else
        flash.now[:alert] = "PackingSpecification could not be saved."
        format.html { render :action => "edit" }
        format.json { render :json => @packing_specification.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /packing_specifications/1
  # DELETE /packing_specifications/1.json
  def destroy
    @packing_specification = PackingSpecification.find(params[:id])
    @packing_specification.destroy

    respond_to do |format|
      format.html { redirect_to packing_specifications_url, :notice => "Packing specification deleted." }
      format.json { head :no_content }
    end
  end

  # POST /packing_specifications/draft
  def draft
    change_status :draft, "'draft' action"
  end

  # POST /packing_specifications/review
  def review
    change_status :review, "'review' action"
  end

  # POST /packing_specifications/approve
  def approve
    change_status :approved, "'approve' action"
  end

  # POST /packing_specifications/approve_directly
  def approve_directly
    change_status :approved, "'approve' action"
  end

  # POST /packing_specifications/obsolete
  def obsolete
    change_status :obsolete, "'obsolete' action"
  end

  # POST /packing_specifications/revert
  def revert
    packing_specification = PackingSpecification.find(params[:id])
    packing_specification_original = packing_specification.latest_approved
    packing_specification.status = packing_specification_original.status
    packing_specification.comment = packing_specification_original.comment
    packing_specification.bug_tracker_id = packing_specification_original.bug_tracker_id
    packing_specification.name = packing_specification_original.name
    packing_specification.description = packing_specification_original.description
    packing_specification.manual_yn = packing_specification_original.manual_yn
    packing_specification.run_script = packing_specification_original.run_script
    packing_specification.updated_by = packing_specification_original.updated_by
    packing_specification.schedule_manual = packing_specification_original.schedule_manual
    packing_specification.schedule_cores = packing_specification_original.schedule_cores
    packing_specification.schedule_memory_base = packing_specification_original.schedule_memory_base
    packing_specification.schedule_memory_times_input = packing_specification_original.schedule_memory_times_input
    packing_specification.schedule_diskspace_base = packing_specification_original.schedule_diskspace_base
    packing_specification.schedule_diskspace_times_input = packing_specification_original.schedule_diskspace_times_input
    packing_specification.schedule_allow_virtual = packing_specification_original.schedule_allow_virtual
    packing_specification.schedule_exclusive = packing_specification_original.schedule_exclusive
    #skip the validation as the status transition does not have to be a valid one in this case
    packing_specification.save(validate: false)

    # the above will increase the version number by one, so revert it
    # note that just adapting the version number will not result in a new version of @packing_specification
    packing_specification.version = packing_specification_original.version
    packing_specification.save

    # destroy all versions which come after the version we reverted to
    PackingSpecificationVersion.where("packing_specification_id = :id and version > :version",
                                 {id: params[:id], version: packing_specification_original.version}).destroy_all

    respond_to do |format|
      format.html { redirect_to packing_specifications_url, notice: "Packing specification successfully reverted to #{packing_specification.version}." }
    end
  end

  private
  def change_status(status, comment)
    @packing_specification = PackingSpecification.find(params[:id])
    @packing_specification.status = status
    @packing_specification.comment = comment
    @packing_specification.updated_by = session[:username]
    if @packing_specification.save
      redirect_to packing_specifications_url, notice: "Status was successfully changed to #{status}."
    else
      redirect_to packing_specifications_url, alert: "Status change to '#{status}' failed."
    end
  end

  def sort_column
    params[:sort] || "name"
  end

  def sort_direction
    params[:direction] || "desc"
  end

  def search
    params[:search] || ""
  end

end
