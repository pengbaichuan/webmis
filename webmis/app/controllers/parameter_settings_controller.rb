class ParameterSettingsController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search
  
  # GET /parameter_settings
  # GET /parameter_settings.json
  def index
    conditions = "project_id = #{session[:project_id]}"
    if params[:reset]
      params.delete :search
      params.delete :search_status
      params.delete :obsolete
      params.delete :reset

      params[:sort] = 'name'
      params[:direction] = 'asc'
    end

    conditions_string_ary = []
    conditions_param_values = []

    include_obsolete = params[:obsolete]

    status = params[:search_status]
    if status.blank?
      conditions_string_ary << ' and (status is null or status != "obsolete")' unless include_obsolete
    else
      conditions_string_ary << ' and (status = ?)'
      conditions_param_values << status
      include_obsolete ||= (status == 'obsolete')
    end

    params[:obsolete] = include_obsolete
    conditions = conditions + conditions_string_ary.join(" ")

    if (sort_column == 'latest_approved')
      @parameter_settings = ParameterSetting.where([conditions]+conditions_param_values).sort_by{ |r| r.latest_approved ? r.latest_approved.version : 0}
      @parameter_settings.reverse! if sort_direction == 'desc'
    else
      @parameter_settings = ParameterSetting.where([conditions]+conditions_param_values).order(sort_column + ' ' + sort_direction)
    end

    unless params[:search].blank?
      search = params[:search].strip
      matched_parameter_settings = []
      # latest_approved can be nil, handle it. The other attributes should be filled
      @parameter_settings.each do |parameter_setting|
        if ( parameter_setting.name.include?(search) ||
             ( parameter_setting.latest_approved && parameter_setting.latest_approved.version.to_s.include?(search) ) ||
             parameter_setting.version.to_s.include?(search) ||
             parameter_setting.status.include?(search)
           )
          matched_parameter_settings << parameter_setting
        end
      end
      @parameter_settings = matched_parameter_settings
    end

    @parameter_settings = @parameter_settings.paginate(per_page: 100, page: params[:page])

    respond_to do |format|
      if params[:search] || params[:search_status]
        flash.now[:notice] = @parameter_settings.total_entries.to_s + " records found."
      end
      format.html # index.html.erb
      format.json { render json: @parameter_settings }
    end
  end

  # GET /parameter_settings/1
  # GET /parameter_settings/1.json
  def show
    @product_line_id = params[:product_line_id] # used to see whether the method was called from the product_line show view
    @parameter_setting = ParameterSetting.find(params[:id])
    @parameter_setting_version = @parameter_setting

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @parameter_setting }
    end
  end

  # GET /parameter_settings/1/latest
  def approved
    @parameter_setting = ParameterSetting.find(params[:id]).use_latest_approved
    @parameter_setting_version = @parameter_setting.approved

    respond_to do |format|
      format.html { render action: "show"} # latest.html.erb
    end
  end

  def handle
    @parameter_setting = ParameterSetting.find(params[:id])
    @parameter_setting_version = @parameter_setting
    @current_user = current_user
  end

  # # GET /parameter_settings/new
  # # GET /parameter_settings/new.json
  # def new
  #   @parameter_setting = ParameterSetting.new

  #   respond_to do |format|
  #     format.html # new.html.erb
  #     format.json { render json: @parameter_setting }
  #   end
  # end

  # GET /parameter_settings/1/edit
  def edit
    if params[:product_design_id]
      @product_design = ProductDesign.find(params[:product_design_id])
    end
    @product_line_id = params[:product_line_id] # used to see whether the method was called from the product_line show view
    if params[:id] == "ID"
      @parameter_setting  = ParameterSetting.new
      @parameter_setting.status = 'draft'
      @parameter_setting.save!
    else
      @parameter_setting = ParameterSetting.find(params[:id])
    end
    # the default new status of an edit should always by ':draft'
    @parameter_setting.status = :draft
    if request.xhr?
      render :layout => false
    end
  end

  # POST /parameter_settings
  # POST /parameter_settings.json
  def create
    # for new parameter_settings, first create one with status new and then edit it as a regular parameter_setting
    # note that we skip the traditional 'new' method here
    @parameter_setting = ParameterSetting.new(:status => 'new', :comment => 'manual initial version', :updated_by => session[:username])
    @parameter_setting.project_id = session[:project_id]
    respond_to do |format|
      if @parameter_setting.save
        format.html { redirect_to( {:action => 'edit', :id => @parameter_setting.id }, :notice => 'New parameter setting created.') }
        format.json { render :json => @parameter_setting, :status => :created, :location => @parameter_setting }
      else
        format.html { redirect_to parameter_settings_url, :alert => 'Could not create new parameter setting.' }
        format.json { render :json => @parameter_setting.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /parameter_settings/1
  # PUT /parameter_settings/1.json
  def update
    @product_line_id = params[:product_line_id] unless params[:product_line_id].blank?
    params[:parameter_setting][:parameterset] = params["parameter_setting_parameterset"]
    @parameter_setting = ParameterSetting.find(params[:id])
    @parameter_setting.project_id = session[:project_id]
    respond_to do |format|
      if @parameter_setting.update_attributes!(strong_params.update(:status => 'draft',:updated_by => session[:username]))
        format.html {
            render :text => "Succesfully saved parameters"
        }
      else
        raise "Error saving parameters, update attributes failed, check server log"
      end
    end
  end

  # DELETE /parameter_settings/1
  # DELETE /parameter_settings/1.json
  def destroy
    @parameter_setting = ParameterSetting.find(params[:id])
    @parameter_setting.destroy

    respond_to do |format|
      format.html { redirect_to parameter_settings_url, notice: "Parameter setting deleted." }
      format.json { head :no_content }
    end
  end

  # POST /parameter_settings/draft
  def draft
    change_status :draft, "'draft' action"
  end

  # POST /parameter_settings/review
  def review
    change_status :review, "'review' action"
  end

  # POST /parameter_settings/approve
  def approve
    change_status :approved, "'approve' action"
  end

  # POST /parameter_settings/approve_direclty
  def approve_directly
    change_status :approved, "'approve' action"
  end

  # POST /parameter_settings/obsolete
  def obsolete
    change_status :obsolete, "'obsolete' action"
  end

  # POST /parameter_settings/revert
  def revert
    parameter_setting = ParameterSetting.find(params[:id])
    parameter_original = parameter_setting.latest_baseline
    parameter_setting.status = parameter_original.status
    parameter_setting.comment = parameter_original.comment
    parameter_setting.bug_tracker_id = parameter_original.bug_tracker_id
    parameter_setting.name = parameter_original.name
    parameter_setting.parameterset = parameter_original.parameterset
    parameter_setting.updated_by = parameter_original.updated_by
    parameter_setting.save(validate: false)

    # the above will increase the version number by one, so revert it
    # note that just adapting the version number will not result in a new version of @parameter_setting
    parameter_setting.version = parameter_original.version
    parameter_setting.save

    # destroy all versions which come after the version we reverted to
    ParameterSettingVersion.where("parameter_setting_id = :id and version > :version",
                                 {id: params[:id], version: parameter_original.version}).destroy_all

    respond_to do |format|
      format.html { redirect_to parameter_settings_url, notice: "Parameter setting successfully reverted to #{parameter_setting.version}." }
    end
  end

  def render_settings
    @parameter_setting = ParameterSetting.find(params[:id])
    if params[:version]
      @parameter_setting_version = @parameter_setting.versions.where("version='#{params[:version]}'").first
    else
      @parameter_setting_version = @parameter_setting
    end
    respond_to do |format|
      format.json { render json: @parameter_setting_version.parameterset, status: :created}
      format.xml { render xml: JSON.parse(@parameter_setting_version.parameterset).to_xml, status: :created }
    end
  end

  def sync
    @parameter_setting = ParameterSetting.find(params[:id])
    @parameter_setting.sync_template_path = params[:template_path]
    @parameter_setting.sync
    respond_to do |format|
      format.html { redirect_to( {:action => 'edit', :id => @parameter_setting.id , :product_design_id => params[:product_design_id], :level => params[:level]}) }
    end
  end

  private
  def change_status(status, comment)
    @parameter_setting = ParameterSetting.find(params[:id])
    if status != :approved || @parameter_setting.to_be_reviewed_change_made_by != current_user.user_name
      @parameter_setting.status = status
      @parameter_setting.comment = comment
      @parameter_setting.updated_by = session[:username]
     if @parameter_setting.save
        Postoffice.review_request(@parameter_setting).deliver if status == :review
        redirect_to parameter_settings_url, :notice => "Status was successfully changed to #{status}."
      else
        redirect_to parameter_settings_url, :alert => "Status change to '#{status}' failed."
      end
    else
      redirect_to parameter_settings_url, :alert => "Status cannot be set to approved by #{current_user}, please let someone else review and approve the changes."
    end
  end

  def sort_column
    params[:sort] || "name"
  end

  def sort_direction
    params[:direction] || "asc"
  end

  def search
    params[:search] || ""
  end

end
