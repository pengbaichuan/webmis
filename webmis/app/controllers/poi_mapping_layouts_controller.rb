class PoiMappingLayoutsController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search
  # GET /poi_mapping_layouts GET /poi_mapping_layouts.json
  def index
    if params[:reset]
      params.delete :search
      params.delete :reset

      params.delete :sort
      params.delete :direction
    end

    if params[:sort] == "input"
      incl = [:input_data_type]
      params[:sort] = "data_types.name"
    elsif params[:sort] == "output"
      incl = [:output_data_type]
      params[:sort] = "data_types.name"
    else
      incl = [:output_data_type,:input_data_type]
    end

    @all_poi_mapping_layouts= PoiMappingLayout.all

    params[:search] = Hash.new if (!params[:search] || params[:search].kind_of?(String))

    conditions = '1=1 '
    conditions_string_ary = []
    conditions_param_values = []

    params[:search].each do |key,val|
      val || ""
      unless val.blank? || key == 'free'
        conditions_string_ary << " and (#{key} = ?)"
        conditions_param_values << val
      end

      unless key.to_s != 'free' || val.blank?
        conditions_string_ary << " and (poi_mapping_layouts.definition_version LIKE ?
              or poi_mapping_layouts.description LIKE ?
               or company_abbrs.company_name LIKE ?
                or company_abbrs.ms_abbr_3 LIKE ?
                 or regions.name LIKE ?
                  or regions.continent_code LIKE ?
                   or regions.country_iso_code LIKE ?
                    or regions.country_iso3_code LIKE ?
                     or regions.city_iso_code LIKE ?
                      or regions.state_code LIKE ?
                       or regions.area_code LIKE ?
                        or data_types.description LIKE ?)"
        12.times do
          conditions_param_values << "\%#{val.strip}\%"
        end
      end
    end

    conditions = conditions + conditions_string_ary.join(" ")
    @poi_mapping_layouts = PoiMappingLayout.eager_load(:regions,:supplier,:customer,incl).where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])

    if request.xhr?
      render(:partial => "poi_mapping_layout", :collection => @poi_mapping_layouts)
    end
  end

  # GET /poi_mapping_layouts/1 GET /poi_mapping_layouts/1.json
  def show
    @poi_mapping_layout = PoiMappingLayout.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @poi_mapping_layout }
    end
  end

  # GET /poi_mapping_layouts/new GET /poi_mapping_layouts/new.json
  def new
    @poi_mapping_layout = PoiMappingLayout.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @poi_mapping_layout }
    end
  end

  def upload_new
    @err = []
    @poi_mapping_layout = PoiMappingLayout.new

    respond_to do |format|
      format.html # upload_new.html.erb
      format.json { render json: @poi_mapping_layout }
    end
  end

  def upload_create
    @poi_mapping_layout = PoiMappingLayout.new
    @err = []
    begin
      @poi_mapping_layout = PoiMappingLayout.import_file(params[:poi_mapping_layout][:upload][0].tempfile.path)
    rescue => e
      @err << e.message
    end
    if params[:poi_mapping_layout]
    if !@poi_mapping_layout.nil?
      rid = @poi_mapping_layout.id
      @poi_mapping_layout.source_filename = params[:poi_mapping_layout][:upload][0].original_filename
      @poi_mapping_layout.save
      # for now just export the PML file again upon import
      begin
        @poi_mapping_layout.export_file
      rescue => e
        @err << e.message
      end
    else
      rid = 0
    end
    
      returnhash = {"size" => params[:poi_mapping_layout][:upload][0].tempfile.size,
        "name"=>params[:poi_mapping_layout][:upload][0].original_filename,
        "url"=> url_for(:controller => 'poi_mapping_layouts', :action => "show", :id => "#{rid}"),
        "delete_url" => url_for(:controller => 'poi_mapping_layouts', :action => 'upload_new'),
        "delete_type" => "DELETE",
      }

      respond_to do |format|
        if !@poi_mapping_layout.nil? && @err.empty?
          format.html {
            render :json => [returnhash].to_json,
            :content_type => 'text/html',
            :layout => false
          }
          format.json { render json: {files: [returnhash]}, status: :created, location: @poi_mapping_layout}
        else
          format.html { render action: "upload_new" }
          format.json { render :json => returnhash["name"] + ": " + @err.join("\n"), status: :unprocessable_entity }
        end

      end
    end
  end

  # GET /poi_mapping_layouts/1/edit
  def edit
    @poi_mapping_layout = PoiMappingLayout.find(params[:id])
  end

  # POST /poi_mapping_layouts POST /poi_mapping_layouts.json
  def create
    @poi_mapping_layout = PoiMappingLayout.new(strong_params)

    respond_to do |format|
      if @poi_mapping_layout.save
        format.html { redirect_to @poi_mapping_layout, notice: 'Poi mapping layout was successfully created.' }
        format.json { render json: @poi_mapping_layout, status: :created, location: @poi_mapping_layout }
      else
        format.html { render action: "new" }
        format.json { render json: @poi_mapping_layout.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /poi_mapping_layouts/1 PUT /poi_mapping_layouts/1.json
  def update
    @poi_mapping_layout = PoiMappingLayout.find(params[:id])

    respond_to do |format|
      if @poi_mapping_layout.update_attributes(strong_params)
        format.html { redirect_to @poi_mapping_layout, notice: 'Poi mapping layout was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @poi_mapping_layout.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /poi_mapping_layouts/1 DELETE /poi_mapping_layouts/1.json
  def destroy
    @poi_mapping_layout = PoiMappingLayout.find(params[:id])
    @poi_mapping_layout.destroy

    respond_to do |format|
      format.html { redirect_to poi_mapping_layouts_url }
      format.json { head :no_content }
    end
  end

  def export_csv
    @poi_mapping_layout = PoiMappingLayout.find(params[:id])
    content = @poi_mapping_layout.csv_content
    content = @poi_mapping_layout.org_binary_data if !@poi_mapping_layout.org_binary_data.blank?
    send_data(content, type: 'text/csv', disposition: 'attachment', filename: "#{@poi_mapping_layout.source_filename}".downcase)
  end

private
  def sort_column
    params[:sort] || "poi_mapping_layouts.id"
  end

  def sort_direction
    params[:direction] || "asc"
  end

  def search
    params[:search][:free] || ""
  end

end
