class PoiMappingsController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search
    
  # GET /poi_mappings
  # GET /poi_mappings.json
  def index
    @poi_mappings = PoiMapping.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @poi_mappings }
    end
  end

  def content_filter
    if params[:reset]
      params.delete :search
      params.delete :reset

      params[:sort] = 'id'
      params[:direction] = 'asc'
    end

    params[:search] = Hash.new if (!params[:search] || params[:search].kind_of?(String))
    
    conditions = '1=1 '
    conditions_string_ary = []
    conditions_param_values = []

    params[:search].each do |key,val|
      val || ""
      unless val.blank? || key == 'free'
        conditions_string_ary << " and (#{key} = ?)"
        conditions_param_values << val
      end
      unless key.to_s != 'free' || val.blank?
        conditions_string_ary << " and (id LIKE ?
              or map_or_filter_type LIKE ?
               or map_or_filter_value LIKE ?
                or logical_type LIKE ?
                 or output_category LIKE ?
                  or input_description LIKE ?
                   or output_description LIKE ?)"
        7.times do
          conditions_param_values << "\%#{val.strip}\%"
        end
      end
    end
    
    conditions = conditions + conditions_string_ary.join(" ")
    @poi_mapping_layout = PoiMappingLayout.find(params[:poi_mapping_layout_id])
    @all_poi_mappings = @poi_mapping_layout.poi_mappings
    @poi_mappings = @all_poi_mappings.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])

    if request.xhr?
      render(:partial => "poi_mapping", :collection => @poi_mappings)
    end

  end

  # GET /poi_mappings/1
  # GET /poi_mappings/1.json
  def show
    @poi_mapping = PoiMapping.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @poi_mapping }
    end
  end

  def details
     @poi_mapping = PoiMapping.find(params[:id])
     render :layout => false
  end

  # GET /poi_mappings/new
  # GET /poi_mappings/new.json
  def new
    @poi_mapping = PoiMapping.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @poi_mapping }
    end
  end

  # GET /poi_mappings/1/edit
  def edit
    @poi_mapping = PoiMapping.find(params[:id])
  end

  # POST /poi_mappings
  # POST /poi_mappings.json
  def create
    @poi_mapping = PoiMapping.new(new_params)

    respond_to do |format|
      if @poi_mapping.save
        format.html { redirect_to @poi_mapping, notice: 'Poi mapping was successfully created.' }
        format.json { render json: @poi_mapping, status: :created, location: @poi_mapping }
      else
        format.html { render action: "new" }
        format.json { render json: @poi_mapping.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /poi_mappings/1
  # PUT /poi_mappings/1.json
  def update
    @poi_mapping = PoiMapping.find(params[:id])

    respond_to do |format|
      if @poi_mapping.update_attributes(strong_params)
        format.html { redirect_to @poi_mapping, notice: 'Poi mapping was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @poi_mapping.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /poi_mappings/1
  # DELETE /poi_mappings/1.json
  def destroy
    @poi_mapping = PoiMapping.find(params[:id])
    @poi_mapping.destroy

    respond_to do |format|
      format.html { redirect_to poi_mappings_url }
      format.json { head :no_content }
    end
  end

  private
  def sort_column
    params[:sort] || "id"
  end

  def sort_direction
    params[:direction] || "asc"
  end

  def search
    params[:search][:free] || ""
  end

end
