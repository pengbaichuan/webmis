class PointsController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"

  # GET /points
  # GET /points.ext_json
  def index
    @points = Point.all.paginate(:per_page => 50, :page => params[:page])
    respond_to do |format|
      format.html     # index.html.erb (no data required)
      #format.ext_json { render :json => find_points.to_ext_json(:class => :point, :count => Point.count) }
      format.json { render json: @points }
    end
  end

  # GET /points/1
  def show
    # show.html.erb
    @point = Point.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @point }
    end
  end

  # GET /points/new
  def new
    @point = Point.new(params[:point])
    # new.html.erb
  end

  # GET /points/1/edit
  def edit
    # edit.html.erb
    @point = Point.find(params[:id])
  end

  # POST /points
  def create
    @point = Point.new(strong_params)
    respond_to do |format|
      if @point.save
        format.html { redirect_to @point, notice: 'Point was successfully created.' }
        format.json { render json: @point, status: :created, location: @point }
      else
        format.html { render action: "new" }
        format.json { render json: @point.errors, status: :unprocessable_entity }
      end
    end    
  end

  # PUT /points/1
  def update
    @point = Point.find(params[:id])
    respond_to do |format|
      if @point.update_attributes(strong_params)
        format.html { redirect_to @point, notice: 'Point was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @point.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /points/1
  def destroy
    @point.destroy

    respond_to do |format|
      format.js  { head :ok }
    end
  rescue
    respond_to do |format|
      format.js  { head :status => 500 }
    end
  end

end
