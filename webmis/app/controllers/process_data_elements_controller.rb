class ProcessDataElementsController < ApplicationController
  # GET /process_data_elements
  # GET /process_data_elements.json
  def index
    @process_data_elements = ProcessDataElement.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @process_data_elements }
    end
  end

  # GET /process_data_elements/1
  # GET /process_data_elements/1.json
  def show
    @process_data_element = ProcessDataElement.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @process_data_element }
    end
  end

  # GET /process_data_elements/new
  # GET /process_data_elements/new.json
  def new
    @process_data_element = ProcessDataElement.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @process_data_element }
    end
  end

  # GET /process_data_elements/1/edit
  def edit
    @process_data_element = ProcessDataElement.find(params[:id])
  end

  # POST /process_data_elements
  # POST /process_data_elements.json
  def create
    @process_data_element = ProcessDataElement.new(strong_params)

    respond_to do |format|
      if @process_data_element.save
        format.html { redirect_to @process_data_element, notice: 'Process data element was successfully created.' }
        format.json { render json: @process_data_element, status: :created, location: @process_data_element }
      else
        format.html { render action: "new" }
        format.json { render json: @process_data_element.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /process_data_elements/1
  # PUT /process_data_elements/1.json
  def update
    @process_data_element = ProcessDataElement.find(params[:id])

    respond_to do |format|
      if @process_data_element.update_attributes(strong_params)
        format.html { redirect_to @process_data_element, notice: 'Process data element was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @process_data_element.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /process_data_elements/1
  # DELETE /process_data_elements/1.json
  def destroy
    @process_data_element = ProcessDataElement.find(params[:id])
    @process_data_element.destroy

    respond_to do |format|
      format.html { redirect_to process_data_elements_url }
      format.json { head :no_content }
    end
  end
end
