class ProcessDataLinesController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout 'webmis'
  # GET /process_data_lines
  # GET /process_data_lines.json
  def index
    @process_data_lines = ProcessDataLine.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @process_data_lines }
    end
  end

  # GET /process_data_lines/1
  # GET /process_data_lines/1.json
  def show
    @process_data_line = ProcessDataLine.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @process_data_line }
    end
  end

  # GET /process_data_lines/new
  # GET /process_data_lines/new.json
  def new
    @process_data_line = ProcessDataLine.new
    @events = ProcessDataSubEvent.where(:status => 10).order(:name)

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @process_data_line }
    end
  end

  # GET /process_data_lines/1/edit
  def edit
    @process_data_line = ProcessDataLine.find(params[:id])
  end

  # POST /process_data_lines
  # POST /process_data_lines.json
  def create
    @process_data_line = ProcessDataLine.new(strong_params)
#    if @process_data_line.process_data_id
#      pd = @process_data_line.process_data
#      pd.version = pd.version.to_i + 1
#      pd.save
#    end
    respond_to do |format|
      if @process_data_line.save
        if params[:inline]
          flash[:notice] = 'Process data line was successfully created.'
          format.html { redirect_to :controller => :process_data ,:action => :maintain,:id => @process_data_line.process_data.id }
        else        
          format.html { redirect_to @process_data_line, notice: 'Process data line was successfully created.' }
        end
        format.json { render json: @process_data_line, status: :created, location: @process_data_line }
      else
        format.html { render action: "new" }
        format.json { render json: @process_data_line.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /process_data_lines/1
  # PUT /process_data_lines/1.json
  def update
    @process_data_line = ProcessDataLine.find(params[:id])
    @events = ProcessDataSubEvent.where(:status => 10).order(:name)
#    if @process_data_line.process_data_id
#      pd = @process_data_line.process_data
#      pd.version = pd.version.to_i + 1
#      pd.save
#    end    

    respond_to do |format|
      if @process_data_line.update_attributes(strong_params)
        if params[:inline]
          format.html { redirect_to :controller => :process_data ,:action => :maintain,:id => @process_data_line.process_data.id, notice: 'Process data line was successfully updated.' }
        else
          format.html { redirect_to @process_data_line, notice: 'Process data line was successfully updated.' }
        end
        format.json { head :no_content }
      else
          format.html { render action: "edit" }
        format.json { render json: @process_data_line.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /process_data_lines/1
  # DELETE /process_data_lines/1.json
  def destroy
    @process_data_line = ProcessDataLine.find(params[:id])
    @process_data_line.destroy

    respond_to do |format|
      format.html { redirect_to process_data_lines_url }
      format.json { head :no_content }
    end
  end
end
