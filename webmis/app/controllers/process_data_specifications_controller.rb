class ProcessDataSpecificationsController < ApplicationController
  # GET /process_data_specifications
  # GET /process_data_specifications.json
  before_filter :authorize
  authorize_resource
  layout "application"
  helper_method :sort_column, :sort_direction, :search
  
  def index
    if params[:reset]
      params.delete :reset
      params.delete :search
      params.delete :sort
      params.delete :direction
      params.delete :column_search_field
    end
    
    conditions = '1=1'
    conditions_string_ary = []
    conditions_param_values = []
    
    unless params[:search].blank?
      conditions_string_ary << " and (process_data_specifications.id LIKE ?
              or process_data_specifications.name LIKE ? 
               or process_data_specifications.script_tag LIKE ? 
                or process_data_specifications.script_parameter LIKE ? 
                 or process_data_specifications.directory LIKE ? 
                  or process_data_specifications.description LIKE ?
                   or data_types.name LIKE ?
                    or process_data_specifications.filling LIKE ?)"
      8.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
    end
    if !column_search_query.empty?
      conditions_string_ary << " AND #{column_search_query['query']}"
      conditions_param_values += column_search_query["values"]
    end
    conditions = conditions + conditions_string_ary.join(" ")
    @process_data_specifications = ProcessDataSpecification.includes(:data_type).where([conditions] + conditions_param_values).references(:data_types).order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])
    generate_id_sequence_cookie(@process_data_specifications,params[:page])
    
    if request.xhr?
      render(:partial => "process_data_specification", :collection => @process_data_specifications)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @process_data_specifications }
      end
    end
  end

  # GET /process_data_specifications/1
  # GET /process_data_specifications/1.json
  def show
    @process_data_specification = ProcessDataSpecification.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @process_data_specification }
    end
  end

  # GET /process_data_specifications/new
  # GET /process_data_specifications/new.json
  def new
    @process_data_specification = ProcessDataSpecification.new
    @process_data_specification.script_tag = ''
    @process_data_specification.active_yn = true
    @datatype_fillings = []
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @process_data_specification }
    end
  end

  # GET /process_data_specifications/1/edit
  def edit
    @process_data_specification = ProcessDataSpecification.find(params[:id])
    @datatype_fillings = []
  end

  # POST /process_data_specifications
  # POST /process_data_specifications.json
  def create
    @process_data_specification = ProcessDataSpecification.new(strong_params)

    respond_to do |format|
      if @process_data_specification.save
        format.html { redirect_to @process_data_specification, notice: 'Process data specification was successfully created.' }
        format.json { render json: @process_data_specification, status: :created, location: @process_data_specification }
      else
        format.html { render action: "new" }
        format.json { render json: @process_data_specification.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /process_data_specifications/1
  # PUT /process_data_specifications/1.json
  def update
    @process_data_specification = ProcessDataSpecification.find(params[:id])

    respond_to do |format|
      if @process_data_specification.update_attributes(strong_params)
        format.html { redirect_to @process_data_specification, notice: 'Process data specification was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @process_data_specification.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /process_data_specifications/1
  # DELETE /process_data_specifications/1.json
  def destroy
    @process_data_specification = ProcessDataSpecification.find(params[:id])
    @process_data_specification.destroy

    respond_to do |format|
      format.html { redirect_to process_data_specifications_url }
      format.json { head :no_content }
    end
  end

  def filling_for_data_type
    if params[:id]
      @process_data_specification = ProcessDataSpecification.find(params[:id])
    else
      @process_data_specification = ProcessDataSpecification.new
    end
    if params[:data_type_id].blank?
      @datatype_fillings = []
    else
      @datatype_fillings = DataType.find(params[:data_type_id]).fillings.active.order(:name).collect {|f| [ "#{f.name}","#{f.name}" ] }
    end
  end
  
  private  
  def sort_column  
    params[:sort] || "process_data_specifications.name"  
  end  
    
  def sort_direction  
    params[:direction] || "asc"  
  end
  
  def search
    params[:search] || ""
  end  
end
