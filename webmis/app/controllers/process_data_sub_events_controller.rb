class ProcessDataSubEventsController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout 'application'
  helper_method :sort_column, :sort_direction, :search
  # GET /process_data_sub_events
  # GET /process_data_sub_events.json
  def index
    @search_opts = {:inactive => "0"}
    if params[:reset]
      params.delete :inactive
      params.delete :search
      params.delete :reset
      params.delete :sort
      params.delete :direction
      params.delete :column_search_field
    end

    conditions_string_ary = []
    conditions_param_values = []

    if params[:inactive]
      @search_opts = {:inactive => "1"}
      conditions_string_ary << "(1=1)"
    else
      conditions_string_ary << "(process_data_sub_events.status = #{ProcessDataEvent::STATUS_ACTIVE})"
    end

    unless params[:search].blank?
      q = params[:search].strip
      conditions_string_ary << "(process_data_sub_events.id LIKE ?
                                 OR process_data_sub_events.name LIKE ?)"
      conditions_param_values << q.to_i # only for id
      1.times do
        conditions_param_values << "\%#{q}\%"
      end
    end

    if !column_search_query.empty?
      conditions_string_ary << "#{column_search_query['query']}"
      conditions_param_values +=  column_search_query["values"]
    end

    conditions = conditions_string_ary.join(" AND ")

    @process_data_sub_events = ProcessDataSubEvent.eager_load("process_data_event").where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])
    generate_id_sequence_cookie(@process_data_sub_events,params[:page])

    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @process_data_sub_events }
    end
  end

  # GET /process_data_sub_events/1
  # GET /process_data_sub_events/1.json
  def show
    @process_data_sub_event = ProcessDataSubEvent.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @process_data_sub_event }
    end
  end

  def handle
    @process_data_sub_event = ProcessDataSubEvent.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @process_data_sub_event }
    end
  end

  def change_status
    @process_data_sub_event = ProcessDataSubEvent.find(params[:id])
    @process_data_sub_event.status = params[:status]
    respond_to do |format|
      if @process_data_sub_event.save!
        format.html { redirect_to :back, notice: "Event was successfully changed to #{@process_data_sub_event.status_string}." }
        format.json { render json: @process_data_sub_event, status: :created, location: @process_data_sub_event }
      else
        format.html { render action: "handle" }
        format.json { render json: @process_data_sub_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /process_data_sub_events/new
  # GET /process_data_sub_events/new.json
  def new
    @process_data_sub_event = ProcessDataSubEvent.new
    @process_data_events = ProcessDataEvent.where(:status => 10).order(:name)

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @process_data_sub_event }
    end
  end

  # GET /process_data_sub_events/1/edit
  def edit
    @process_data_sub_event = ProcessDataSubEvent.find(params[:id])
    @process_data_events = ProcessDataEvent.where(:status => 10).order(:name)
  end

  # POST /process_data_sub_events
  # POST /process_data_sub_events.json
  def create
    @process_data_sub_event = ProcessDataSubEvent.new(strong_params)

    respond_to do |format|
      if @process_data_sub_event.save
        format.html { redirect_to @process_data_sub_event, notice: 'Process data sub event was successfully created.' }
        format.json { render json: @process_data_sub_event, status: :created, location: @process_data_sub_event }
      else
        format.html { render action: "new" }
        format.json { render json: @process_data_sub_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /process_data_sub_events/1
  # PUT /process_data_sub_events/1.json
  def update
    @process_data_sub_event = ProcessDataSubEvent.find(params[:id])

    respond_to do |format|
      if @process_data_sub_event.update_attributes(strong_params)
        format.html { redirect_to @process_data_sub_event, notice: 'Process data sub event was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @process_data_sub_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /process_data_sub_events/1
  # DELETE /process_data_sub_events/1.json
  def destroy
    @process_data_sub_event = ProcessDataSubEvent.find(params[:id])
    @process_data_sub_event.destroy

    respond_to do |format|
      format.html { redirect_to process_data_sub_events_url }
      format.json { head :no_content }
    end
  end
  
  private  
  def sort_column  
    params[:sort] || "process_data_sub_events.name"
  end  
    
  def sort_direction  
    params[:direction] || "desc"  
  end
  
  def search
    params[:search] || ""
  end
  
end
