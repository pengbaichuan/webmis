class ProcessTransitionsController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout 'webmis'
  
  # GET /process_transitions
  # GET /process_transitions.xml
  def index
    @process_transitions = ProcessTransition.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @process_transitions }
    end
  end

  # GET /process_transitions/1
  # GET /process_transitions/1.xml
  def show
    @process_transition = ProcessTransition.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @process_transition }
    end
  end

  # GET /process_transitions/new
  # GET /process_transitions/new.xml
  def new
    @process_transition = ProcessTransition.new
    @process_transition.ordertype_id = params[:ot_id]

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @process_transitions }
    end
  end

  # GET /process_transitions/1/edit
  def edit
    @process_transition = ProcessTransition.find(params[:id])
  end

  def delete
    destroy
  end

  # POST /process_transitions
  # POST /process_transitions.xml
  def create
    @process_transition = ProcessTransition.new(strong_params)

    respond_to do |format|
      if @process_transition.save
        flash[:notice] = 'ProcessTransition was successfully created.'
        format.html { redirect_to :controller => 'ordertypes' , :action => 'edit_workflow', :id => @process_transition.ordertype_id }
        format.xml  { render :xml => @process_transition, :status => :created, :location => @process_transition }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @process_transition_errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /process_transitions/1
  # PUT /process_transitions/1.xml
  def update
    @process_transition = ProcessTransition.find(params[:id])

    respond_to do |format|
      if @process_transition.update_attributes(strong_params)
        flash[:notice] = 'ProcessTransitions was successfully updated.'
        format.html { redirect_to :controller => 'ordertypes' , :action => 'edit_workflow', :id => @process_transition.ordertype_id }
        
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @process_transition.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /process_transitions/1
  # DELETE /process_transitions/1.xml
  def destroy
    @process_transition = ProcessTransition.find(params[:id])
    @process_transition.destroy

    respond_to do |format|
      format.html { redirect_to(:controller => 'ordertypes' , :action => 'edit_workflow' , :id => params[:otid]) }
      format.xml  { head :ok }
    end
  end
end
