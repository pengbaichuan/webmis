class ProductCategoriesController < ApplicationController
  # GET /product_categories
  # GET /product_categories.xml
  before_filter :authorize
  authorize_resource

  layout "application"
  helper_method :sort_column, :sort_direction, :search
  
  def index
    @search_opts = {:inactive => "0"}
    if params[:reset]
      params.delete :inactive
      params.delete :search
      params.delete :reset
      params.delete :sort
      params.delete :direction
      params.delete :column_search_field
    end

    conditions_string_ary = []
    conditions_param_values = []

    if params[:inactive]
      @search_opts = {:inactive => "1"}
      conditions_string_ary << "1=1"
    else
      conditions_string_ary << "(product_categories.active = 1)"
    end

    unless params[:search].blank?
      q = params[:search].strip
      conditions_string_ary << "(product_categories.id = ?
                                OR product_categories.abbreviation LIKE ?
                                OR product_categories.description LIKE ?
                                OR product_categories.pds_import_file LIKE ?)"
      conditions_param_values << q.to_i # only for id
      3.times do
        conditions_param_values << "%#{q}%"
      end
    end

    if !column_search_query.empty?
      conditions_string_ary << "#{column_search_query['query']}"
      conditions_param_values +=  column_search_query["values"]
    end
    conditions = conditions_string_ary.join( ' AND ' )

    @product_categories = ProductCategory.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])
    generate_id_sequence_cookie(@product_categories,params[:page])

    if request.xhr?
      render(:partial => "product_category", :collection => @product_categories)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @product_categories }
      end
    end
  end

  # GET /product_categories/1
  # GET /product_categories/1.xml
  def show
    @product_category = ProductCategory.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @product_category }
    end
  end

  # GET /product_categories/new
  # GET /product_categories/new.xml
  def new
    @product_category = ProductCategory.new
    @product_category.active = true

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @product_category }
    end
  end

  # GET /product_categories/1/edit
  def edit
    @product_category = ProductCategory.find(params[:id])
  end

  # POST /product_categories
  # POST /product_categories.xml
  def create
    @product_category = ProductCategory.new(strong_params)

    respond_to do |format|
      if @product_category.save
        flash[:notice] = 'ProductCategory was successfully created.'
        format.html { redirect_to(@product_category) }
        format.xml  { render :xml => @product_category, :status => :created, :location => @product_category }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @product_category.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /product_categories/1
  # PUT /product_categories/1.xml
  def update
    @product_category = ProductCategory.find(params[:id])

    respond_to do |format|
      if @product_category.update_attributes(strong_params)
        flash[:notice] = 'ProductCategory was successfully updated.'
        format.html { redirect_to(@product_category) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @product_category.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /product_categories/1
  # DELETE /product_categories/1.xml
  def destroy
    @product_category = ProductCategory.find(params[:id])
    @product_category.destroy

    respond_to do |format|
      format.html { redirect_to(product_categories_url) }
      format.xml  { head :ok }
    end
  end

  private
  def sort_column
    params[:sort] || "product_categories.abbreviation"
  end

  def sort_direction
    params[:direction] || "asc"
  end

  def search
    params[:search] || ""
  end
end
