class ProductContentSpecificationsController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search

  # GET /product_content_specifications
  # GET /product_content_specifications.json
  def index
    if params[:reset]
      params.delete :search
      params.delete :reset

      params.delete :sort
      params.delete :direction
    end

    params[:search] = Hash.new if (!params[:search] || params[:search].kind_of?(String))

    conditions = '1=1 '
    conditions_string_ary = []
    conditions_param_values = []

    params[:search].each do |key,val|
      val || ""
      unless val.blank? || key == 'free'
        conditions_string_ary << " and (#{key} = ?)"
        conditions_param_values << val
      end

      unless key.to_s != 'free' || val.blank?
        conditions_string_ary << " and (product_content_specifications.name LIKE ?
              or product_content_specifications.sync_filename LIKE ?
               )"
        2.times do
          conditions_param_values << "\%#{val.strip}\%"
        end
      end
    end

    conditions = conditions + conditions_string_ary.join(" ")
    @product_content_specifications = ProductContentSpecification.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])

    respond_to do |format|
      format.html # index.html.erb

    end
  end
  # GET /product_content_specifications/1
  # GET /product_content_specifications/1.json
  def show
    @product_content_specification = ProductContentSpecification.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @product_content_specification }
    end
  end

  def details
    @product_content_specification = ProductContentSpecification.find(params[:id])
    render :layout => false
  end

  # GET /product_content_specifications/new
  # GET /product_content_specifications/new.json
  def new
    @product_content_specification = ProductContentSpecification.new
    @grid = load_grid(-1).to_json
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @product_content_specification }
    end
  end

  # GET /product_content_specifications/1/edit
  def edit
    @product_content_specification = ProductContentSpecification.find(params[:id])
    @grid = load_grid(params[:id]).to_json
  end

  # POST /product_content_specifications
  # POST /product_content_specifications.json
  def create
    @product_content_specification = ProductContentSpecification.new(strong_params)
    @grid = JSON.parse(params[:hidden_grid])
    
    respond_to do |format|
      ProductContentSpecification.transaction do
        if @product_content_specification.save
          store_grid(@product_content_specification.id,@grid)
          format.html { redirect_to @product_content_specification, notice: 'Region mapping was successfully created.' }
          format.json { render json: @product_content_specification, status: :created, location: @product_content_specification }
        else
          format.html { render action: "new" }
          format.json { render json: @product_content_specification.errors, status: :unprocessable_entity }
        end
      end
    end
  end


  def content_filter
    if params[:reset]
      params.delete :search
      params.delete :reset

      params[:sort] = 'region_mapping_lines.id'
      params[:direction] = 'asc'
    end

    params[:search] = Hash.new if (!params[:search] || params[:search].kind_of?(String))

    conditions = '1=1 '
    conditions_string_ary = []
    conditions_param_values = []


    params[:search].each do |key,val|
      val || ""
      unless val.blank? || key == 'free'
        conditions_string_ary << " and (#{key} = ?)"
        conditions_param_values << val
      end
      unless key.to_s != 'free' || val.blank?
        conditions_string_ary << " and (region_mapping_lines.id LIKE ?
              or data_types.name LIKE ?
               or data_releases.name LIKE ?
                or regions.name LIKE ?
                 or destination_region_code LIKE ?
                  ?)"
        6.times do
          conditions_param_values << "\%#{val.strip}\%"
        end
      end
    end

    conditions = conditions + conditions_string_ary.join(" ")
    @product_content_specification = ProductContentSpecification.find(params[:product_content_specification_id])
    @all_mappings = @product_content_specification.region_mapping_lines
    @mappings = @all_mappings.eager_load('data_type','region','data_release').where([conditions] + conditions_param_values).order(sort_column_line + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])

    #if request.xhr?
    #  render(:partial => "poi_mapping", :collection => @poi_mappings)
    #end

  end

  # PUT /product_content_specifications/1
  # PUT /product_content_specifications/1.json
  def update
    @product_content_specification = ProductContentSpecification.find(params[:id])
    @grid = JSON.parse(params[:hidden_grid])
    puts @grid.inspect

    
    respond_to do |format|
        @product_content_specification.assign_attributes(strong_params)
        begin
          ProductContentSpecification.transaction do
            @product_content_specification.sync_filename = DateTime.now
            @product_content_specification.save!
            store_grid(params[:id],@grid)
            format.html { redirect_to @product_content_specification, notice: 'Specification was succesfully updated.' }
            format.json { head :no_content }
          end
        rescue => e
          format.html { render action: "edit" }
          format.json { render json: @product_content_specification.errors, status: :unprocessable_entity }
        end
    end
  end

  # DELETE /product_content_specifications/1
  # DELETE /product_content_specifications/1.json
  def destroy
    @product_content_specification = ProductContentSpecification.find(params[:id])
    @product_content_specification.destroy

    respond_to do |format|
      format.html { redirect_to product_content_specifications_url }
      format.json { head :no_content }
    end
  end

  def datareleases
    releases = [""] + DataRelease.includes('company_abbr').production.map{|x|x.company_abbr.company_name + " " + x.name + " (" + x.id.to_s + ")" if x.company_abbr}
    render :text => releases.to_json
  end

  def data_types
    dt = [""] + DataType.active.map{|x|x.name + " (" + x.description.to_s + ")" if x.name}.compact
    render :text => dt.to_json
  end

  def regions
    dt = Region.active.map{|x|x.name.to_s + " (" + x.id.to_s + " " + x.class.name + ")"}
    render :text => dt.to_json
  end

  def filling
    filling = [""] + Filling.active.map{|f|f.name.to_s}.sort
    render :text => filling.to_json
  end
  
  def update_regions
    dt = UpdateRegion.active.map{|x|x.name.to_s}
    render :text => dt.to_json
  end
  
  private
  def sort_column
    params[:sort] || "id"
  end

  def sort_column_line
    params[:sort] || "region_mapping_lines.id"
  end

  def sort_direction
    params[:direction] || "asc"
  end

  def search
    params[:search][:free] || ""
  end

  def store_grid(pcs_id,grid)

    lines = ProductContentItem.where(product_content_specification_id:pcs_id)
    ProductContentItem.transaction do
      ProductContentItem.where("product_content_specification_id = #{pcs_id}").destroy_all
      lines = ProductContentItem.where(product_content_specification_id:pcs_id)
      puts "sizeee:: " + lines.inspect
      grid.each do |line|
        #next if line == grid[0] # skip headers
        if ( !line[0] && !line[1] ) # Source & Destination Region Code
          next
        else
          next if ( line[0].to_s == '' || line[1].to_s == '' )
        end
        l = ProductContentItem.new
        l.product_content_specification_id = pcs_id
        l.source_region_code_id = line[0].scan(/\((.*)\)$/).flatten[0].to_i if line[0]
        l.destination_region_code = line[1] if line[1]
        l.incr_update_yn = line[2]
        l.regional_update_yn = line[3]
        l.resource_identification = line[4]
        l.resource_identification_iu = line[5]
        if !line[6].blank?
          data_type_id = DataType.find_by_name(line[6].split('(')[0].strip).id
          if data_type_id
            l.source_data_type_id = data_type_id
          end
        end
        l.filling = line[7]
        puts "saving :: " + l.inspect
        l.save!
      end
    end

  end

  def load_grid(region_header_id)
    grid = []
    
    lines = ProductContentItem.where(product_content_specification_id:region_header_id)
    lines.each do |line|
      type = nil
      release = nil
      region = nil
      region = Region.find(line.source_region_code_id) if line.source_region_code_id && line.source_region_code_id != 0
      row = []
      # [0]
      if region
        row << region.name +  " (#{region.id.to_s})"
      else
        row << ""
      end
      # [1]
      row << line.destination_region_code
      # [2]
      if line.incr_update_yn
        row << line.incr_update_yn
      else
        row << false
      end
      # [3]
      if line.regional_update_yn
        row << line.regional_update_yn
      else
        row << false
      end
      # [4]
      if line.resource_identification
        row << line.resource_identification
      else
        row << ""
      end
      # [5]
      if line.resource_identification_iu
        row << line.resource_identification_iu
      else
        row << ""
      end
      # [6]
      if line.data_type
        dt_str = line.data_type.name + " (" + line.data_type.description.to_s + ")"
        row << dt_str
      else
        row << ""
      end
      # [7]
      if !line.filling.blank?
        row << line.filling
      else
        row << ""
      end
      grid << row
    end
    if grid.size == 0
      grid << ['','',"0","0",'','','','']
    end
    return grid
  end
end
