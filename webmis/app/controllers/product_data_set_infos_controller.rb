class ProductDataSetInfosController < ApplicationController
  before_filter :authorize
  authorize_resource  
  # GET /product_data_set_infos
  # GET /product_data_set_infos.xml
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search
  
  
  def index

    if params[:reset]
      params.delete :search
      params.delete :reset
      params.delete :sort
      params.delete :direction
    end    
    conditions_string_ary = ["1=1"]
    conditions_param_values = []
    
    unless params[:search].blank?
      q = params[:search].strip
      conditions_string_ary << "(product_data_set_infos.id = ?
                                OR company_abbrs.company_name LIKE ?
                                OR product_data_set_infos.name LIKE ?
                                OR product_data_set_infos.data_release LIKE ?
                                OR product_data_set_infos.data_info LIKE ?
                                OR products.volumeid LIKE ?
                                OR products.name LIKE ?)"
      conditions_param_values << q.to_i # for id
      6.times do
        conditions_param_values << "%#{q}%"
      end
    end
    conditions = conditions_string_ary.join(' AND ')
    @product_data_set_infos = ProductDataSetInfo.includes(:product,:data_type,:company_abbr,:data_set).where([conditions] + conditions_param_values).references(:company_abbrs,:products).order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])
    
    if request.xhr?
      render(:partial => "product_data_set_info", :collection => @product_data_set_infos)
    end
    
  end

  # GET /product_data_set_infos/1
  # GET /product_data_set_infos/1.xml
  def show
    @product_data_set_info = ProductDataSetInfo.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @product_data_set_info }
    end
  end

  # GET /product_data_set_infos/new
  # GET /product_data_set_infos/new.xml
  def new
    @product_data_set_info = ProductDataSetInfo.new
    @product_data_set_info.product_id = params[:link_id]

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @product_data_set_info }
    end
  end

  # GET /product_data_set_infos/1/edit
  def edit
    @product_data_set_info = ProductDataSetInfo.find(params[:id])
  end

  # POST /product_data_set_infos
  # POST /product_data_set_infos.xml
  def create

    ProductDataSetInfo.transaction do
        @product_data_set_info = ProductDataSetInfo.new(strong_params)
        if @product_data_set_info.product && @product_data_set_info.product.coverage
          @product_data_set_info.coverage_id = @product_data_set_info.product.coverage.duplicate.id
        end

      if params[:commit] == "Create for all products on productset"
        ps = Productset.find(params[:set_id])
        ps.products.each do |product|
          if product.id != @product_data_set_info.id
            p = @product_data_set_info.dup
            p.product_id = product.id
            if product.coverage
              p.coverage_id = product.coverage.duplicate.id
            end
            p.save
          end
        end
      end

      respond_to do |format|
        if @product_data_set_info.save
          flash[:notice] = 'DataSet Info was successfully added.'
          format.html { redirect_to(:controller => 'products', :action => 'edit', :id => @product_data_set_info.product_id, :set_id => params[:set_id]) }
          format.xml  { render :xml => @product_data_set_info, :status => :created, :location => @product_data_set_info }
        else
          raise ActiveRecord::Rollback, "Save failed"
          format.html { render :action => "new" }
          format.xml  { render :xml => @product_data_set_info.errors, :status => :unprocessable_entity }
        end
      end
    end
    
  end

  # PUT /product_data_set_infos/1
  # PUT /product_data_set_infos/1.xml
  def update
    @product_data_set_info = ProductDataSetInfo.find(params[:product_data_set_info][:id])

    respond_to do |format|
      if @product_data_set_info.update_attributes(strong_params)
        flash[:notice] = 'Product DataSet Info was successfully updated.'


        format.html {redirect_to(:action => 'edit', :controller => 'products', :id => @product_data_set_info.product_id, :set_id => params[:set_id]) }
        format.xml  {head :ok }
      else
        format.html {render :action => "edit" }
        format.xml  {render :xml => @product_data_set_info.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /product_data_set_infos/1
  # DELETE /product_data_set_infos/1.xml
  def destroy
    @product_data_set_info = ProductDataSetInfo.find(params[:id])
    @product_data_set_info.destroy

    respond_to do |format|
      format.html {redirect_to(product_data_set_infos_url) }
      format.xml  {head :ok }
    end
  end

  def get_data_releases_for_select
    @data_releases = DataRelease.production.where("company_abbr_id = #{params[:company_abbr_id]} and name like '%#{params[:name]}%'")
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @data_releases.map{ |dr| {:id=>dr.name,:text=>dr.name}}.to_json }
    end
  end

  def get_filling_for_select
    data_type = DataType.find(params[:data_type_id])
    @fillings = data_type.fillings
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @fillings.map{ |f| {:id=>f.name,:text=>f.name}}.to_json }
    end
  end
  
  private  
  def sort_column  
    params[:sort] || "product_data_set_infos.id"
  end  
    
  def sort_direction  
    params[:direction] || "desc"  
  end
  
  def search
    params[:search] || ""
  end  
  
end
