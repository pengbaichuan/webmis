class ProductDesignsController < ApplicationController
  before_filter :authorize, :except => [:api_index, :template, :subtemplate]
  authorize_resource :except => [:api_index, :template, :subtemplate]
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search, :sec_sort_column
  

  # GET /product_designs
  # GET /product_designs.json
  def index
    conditions = "(status is not null AND project_id = #{session[:project_id]})"
    if params[:reset]
      params.delete :search
      params.delete :search_status
      params.delete :search_exe_name
      params.delete :obsolete
      params.delete :reset

      params[:sort] = 'name'
      params[:direction] = 'desc'
    end

    conditions_string_ary = []
    conditions_param_values = []

    include_obsolete = params[:obsolete]

    status = params[:search_status]
    if status.blank?
      conditions_string_ary << ' and (status is null or status != "obsolete")' unless include_obsolete
    else
      conditions_string_ary << ' and (status = ?)'
      conditions_param_values << status
      include_obsolete ||= (status == 'obsolete')
    end

    design_type = params[:design_type]
    if !design_type.blank?
      conditions_string_ary << ' and (design_type = ?)'
      conditions_param_values << design_type
    end

    executable_name = params[:search_exe_name]
    if !executable_name.blank?
      exe_sql_string = " AND (model COLLATE utf8_bin LIKE \'\%%,\\\"name\\\":\\\"#{executable_name}\\\",%%\')"
      conditions = conditions + exe_sql_string
    end
    
    params[:obsolete] = include_obsolete
    conditions = conditions + conditions_string_ary.join(" ")

    if (sort_column == 'latest_approved')
      @product_designs = ProductDesign.where([conditions]+conditions_param_values).sort_by{ |r| r.latest_approved ? r.latest_approved.version : 0}
      @product_designs.reverse! if sort_direction == 'desc'
    else
      @product_designs = ProductDesign.where([conditions]+conditions_param_values).order(sort_column + ' ' + sort_direction)
    end

    unless params[:search].blank?
      search = params[:search].strip.downcase
      matched_product_designs = []
      @product_designs.each do |product_design|
        if ( product_design.name && product_design.name.downcase.include?(search) ) ||
             ( product_design.latest_approved && product_design.latest_approved.version.to_s.downcase.include?(search) ) ||
             product_design.version.to_s.downcase.include?(search) ||
             product_design.status.downcase.include?(search)
          matched_product_designs << product_design
        end
      end
      @product_designs = matched_product_designs
    end

    @product_designs = @product_designs.paginate(per_page: 100, page: params[:page])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @product_designs }
    end
  end

  # GET /product_designs/1
  # GET /product_designs/1.json
  def show
    @product_design = ProductDesign.find(params[:id]).use_latest
    @product_design_latest_approved = ProductDesign.find(params[:id]).use_latest_baseline

    if !params[:version].blank?
      @product_design = @product_design.object_from_version(params[:version])
    end

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @product_design }
    end
  end

  # GET /product_designs/1/latest
  def approved
    @product_design = ProductDesign.find(params[:id]).use_latest_baseline
    @product_design_latest_approved = @product_design

    respond_to do |format|
      format.html { render action: "show"} # latest.html.erb
    end
  end

  def handle
    @product_design = ProductDesign.find(params[:id]).use_latest
    @product_design_latest_approved = ProductDesign.find(params[:id]).use_latest_baseline
    @current_user = current_user
  end

  def html2pdf_show
    @product_design = ProductDesign.find(params[:id]).use_latest_baseline
    @product_design_latest_approved = @product_design
    @imagepath = 'http://' + ENV["host"] + "/webmis/images/mapscapelogo4docs.png" # Needed for working with PDFKit


    respond_to do |format|
      format.html
      format.pdf do
        render :pdf => "model",
          :page_size => 'A4',
          :orientation => 'Landscape',
          :disable_smart_shrinking => false,
          :disable_javascript => false,
          :lowquality => true,
          :margin => { :bottom => 20 },
          :encoding => "utf8",
          :layout => 'pdf.html',
          :show_as_html => params[:debug],
          :no_background    => false
      end
    end
  end

  def clone_it
    @design_org = ProductDesign.find(params[:id]).use_latest
    @design = @design_org.dup
    @design.name = "CLONE of " + @design_org.name
    @design.project_id = session[:project_id]
    
    @design_org.used_product_designs.each do |ud|
      @design.used_product_designs << ud.dup
    end

    @design.save!

    flash[:notice] = "Succesfully cloned design #{params[:id]} to #{@design.id.to_s}"
    redirect_to({:action => "index"},:notice => "Succesfully cloned design #{@design_org.name} to #{@design.id.to_s}")
  end

  # # POST /product_designs/new
  # # POST /product_designs/new.json
  # def new
  #   @product_design = ProductDesign.new(status: 'new', comment: 'manual initial version', updated_by: session[:username])
  #   @product_design.save
  #   respond_to do |format|
  #     format.html { redirect_to({action: 'edit', id: @product_design.id }, notice: 'New product design created.') }
  #     format.json { render json: @product_design, status: :created, location: @product_design }
  #   end
  # end

  def sync_model
    model= params[:model]
    @designhash = JSON.parse(model)

    if params[:source] && params[:source] == 'pol'
      pol = ProductionOrderline.find(params[:id])
      pol.product_design = model
      pol.save
    else
      ProductDesign.transaction do
        @product_design = ProductDesign.find(params[:id])
        @product_design.obsolete_and_add_missing_imported_designs(@designhash)
        @product_design.modelhash = @designhash
        @product_design.status = "intermediate"
        @product_design.lock_version = params[:lock_version]
      
        # don't automatically save product designs
        @product_design.save!
        @product_design.reload # get new lock_version
        @designhash[:lock_version] = @product_design.lock_version
      end
    end

    # add process message
    #@product_design.modelhash["process_message"] = [{:type => 'info', :text => 'Sync was succesful'}]
    #@product_design.modelhash["process_message"] << {:type => 'error', :text => 'Error'}
    #@product_design.modelhash["process_message"] << {:type => 'success', :text => 'Succesful!!!'}

    respond_to do |format|
      format.json { render json: @designhash.to_json, status: :created }
      format.xml { render xml: @designhash.to_xml, status: :created }
    end
  end

  def add_conversion_step
    model=request.body.read
    @product_design = ProductDesign.init_with_model(model)
    @executable = Executable.where(:name => params[:name]).first
    step = @product_design.add_step(@executable)

    respond_to do |format|
      format.json { render json: step.to_json, status: :created, location: @product_design }
      format.xml { render xml: step.to_xml, status: :created, location: @product_design }
    end
  end

  def get_package_id_for_package
    @product_design = ProductDesign.find(params[:id]).use_latest
    if @product_design.model.to_s.size > 0
      @designhash = JSON.parse(@product_design.model)
    end
    step = params[:step]
    hash = JSON.parse(params[:package_spec])

    @product_design.get_package_id_for_package_spec(hash, step)

  end

  # NOT USED ANYMORE
  # def allocate_by_design
  #   @product_design = ProductDesign.find(params[:id])
  #   allocateparams = {:strategy => 'best match'}
  #   productiondesign = ProductDesign.allocate_files(@product_design.modelhash, allocateparams)
  #   @product_design.modelhash = productiondesign[0]
  #   @product_design.save
  #    @product_design.modelhash["process_message"] = [{:type => 'info', :text => 'Allocation was done'}]
  #    #@product_design.modelhash["process_message"] << {:type => 'error', :text => 'Error'}
  #    #@product_design.modelhash["process_message"] << {:type => 'success', :text => 'Succesful!!!'}


  #    respond_to do |format|
  #         format.json { render json: @product_design.modelhash.to_json, status: :created, location: @product_design }
  #         format.xml { render xml: @product_design.modelhash.to_xml, status: :created, location: @product_design }
  #    end
  # end

  # def allocate
  #   model=request.body.read
  #   productiondesign = JSON.parse(model)
  #   allocateparams = {:strategy => 'best match'}
  #   productiondesign = ProductDesign.allocate_files(productiondesign)
  #   productiondesign[0]["process_message"] = [{:type => 'info', :text => 'Allocation was completed'}]
  #   respond_to do |format|
  #     format.json { render json: productiondesign[0].to_json, status: :created, location: @product_design }
  #     format.xml { render xml: productiondesign[0].to_xml, status: :created, location: @product_design }
  #   end
  # end

  # def create_production_order_old

  #   sdfsdf
  #   model=request.body.read
  #   productiondesign = JSON.parse(model)
  #   product_id = productiondesign["product_id"]
  #   comment = productiondesign["comment"]
  #   conversiontool_id = productiondesign["conversiontool_id"]
  #   cvtool_bundle_id = productiondesign["cvtool_bundle_id"]

  #   ProductDesign.create_production_order(product_id, comment )
  #   productiondesign["process_message"] = [{:type => 'info', :text => 'Production Order was created'}]
  #   respond_to do |format|
  #       format.json { render json: productiondesign.to_json, status: :created, location: @product_design }
  #       format.xml { render xml: productiondesign.to_xml, status: :created, location: @product_design }
  #   end
  # end

  def edit_model
    @product_design = ProductDesign.find(params[:id]).use_latest
    @parameter_setting = @product_design.parameter_setting
    if !@parameter_setting
      @parameter_setting = ParameterSetting.new
      @parameter_setting.status = :draft
      @parameter_setting.save
      @product_design.parameter_setting_id = @parameter_setting.id
      @product_design.save
    end

    @product_line = @product_design.product_line

    #setup_pickers
    #if request.xhr?
    # flash.keep[:notice]
    # render(:partial => "assembly_picker", :collection => @assembly_pickers)
    #end
  end

  def edit
    @product_design = ProductDesign.find(params[:id]).use_latest  
    if @product_design.data_release_id.blank?
      @data_releases_for_select = []
    else
      @data_releases_for_select = DataRelease.where("(id = #{@product_design.data_release.id}) or (production = 1 and company_abbr_id = '#{@product_design.data_release.company_abbr_id.to_s}' and data_releases.name is not null)").order('data_releases.updated_at desc').uniq
    end
    # the default new status of an edit should always be ':draft', set it.
    @product_design.status = :draft
    if @product_design.model.to_s.size > 0
      m = JSON.parse(@product_design.model)
      @product_line_selection_list = ProductLine.option_list(m["product_line"] ? m["product_line"]["id"] : nil)
    end
  end

  def get_datareleases
    if !params[:sid].blank? && params[:sid] != "0"
      @data_releases = DataRelease.where("data_releases.name is not null and production = 1 and company_abbr_id = '#{params[:sid]}'").order('name DESC')
    else
      @data_releases = []
    end
    render :partial => "datarelease_select"
  end 

  def render_model
    puts "render_model, source: '#{params[:source]}', id: '#{params[:id]}', version: '#{params[:version]}"
    if params[:source] && params[:source] == 'pol'
      @product_design = ProductionOrderline.find(params[:id]) # What is this ?
    else
      product_design = ProductDesign.find(params[:id])
      if params[:version]
        @product_design = product_design.use_version(params[:version])
      else
        @product_design = product_design.use_latest
      end
      @product_design.init
    end
    respond_to do |format|
      format.json { render json: @product_design.model, status: :created}
      format.xml { render xml: @product_design.modelhash.to_xml, status: :created }
    end
  end

  # POST /product_designs
  # POST /product_designs.json
  def create
    @product_design = ProductDesign.new({:comment => 'manual initial version', :updated_by => session[:username],:project_id => session[:project_id]})

    respond_to do |format|
      if @product_design.save(:validate => false)
        format.html { redirect_to( { action: 'edit', id: @product_design.id }, notice: 'New product design created.' ) }
        format.json { render json: @product_design, status: :created, location: @product_design }
      else
        format.html { redirect_to product_designs_url, alert: 'Could not create new product design.' }
        format.json { render json: @product_design.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_model
    @product_design = ProductDesign.find(params[:id])
    @product_design.model = params[:model]
    hash = JSON.parse(@product_design.model)
    if hash["product_line"]
      hash["product_line"] = ProductLine.to_hash(hash["product_line"]["id"],session[:project_id])
    end

    @product_design.modelhash = hash
    @product_design.name = hash["name"]
    @product_design.description = hash["description"]
    # don't save automatically, only on an explicit save
    # if @product_design.save
    respond_to do |format|
      format.json { render json: @product_design.model, status: :created, location: @product_design }
      format.xml { render xml: JSON.parse(@product_design.model).to_xml, status: :created, location: @product_design }
    end
    # else
    #  respond_to do |format|
    #      format.json { render json: @product_design.errors, status: :unprocessable_entity }
    #  end
    # end
  end

  def update
    @product_design = ProductDesign.find(params[:id])
    original_md5 = @product_design.product_design_md5
    # fill in the blanks with data from the database, e.g. the user can only select a product_line_id
    # the remaining product_line data has to be filled in now
    params[:product_design][:modelhash] = generate_modelhash_from_params if params[:product_design][:modelhash]
    if @product_design.data_release_id.blank?
      @data_releases_for_select = []
    else
      @data_releases_for_select = DataRelease.where("(id = #{@product_design.data_release.id}) or (production = 1 and company_abbr_id = '#{@product_design.data_release.company_abbr_id.to_s}' and data_releases.name is not null)").order('data_releases.updated_at desc').uniq
    end
    respond_to do |format|
      ProductDesign.transaction do
        if params[:product_design][:model] # force update of modelhash because of filtered nils
          designhash = JSON.parse(params[:product_design][:model])
          @product_design.obsolete_and_add_missing_imported_designs(designhash) # remove old crap
          designhash["source_record_id"] = @product_design.id
          designhash["source_record_version"] = @product_design.version + 1
          designhash["source_environment"] = Rails.env
          designhash["source_project"] = @product_design.project.name if @product_design.project
          @product_design.modelhash = designhash
        end
        if @product_design.update_attributes(product_design_params.update(:status => 'draft', :updated_by => session[:username]))
          synced = 0
          synced = @product_design.save_and_synchronize(original_md5).size
          if @product_design.status == 'approved' || @product_design.status == 'obsolete' || @product_design.status == 'review'
            Postoffice.product_design_status_change(params[:id]).deliver
          end
          format.html { redirect_to({ :action => 'handle', :id => @product_design.id, :synced => synced }, :notice => 'Product design was successfully updated.' ) }
          format.json { head :no_content }
        else
          if params[:product_design][:modelhash]
            # the old model is needed to make sure the old selected product_line is still in the list
            newmodel = JSON.parse(@product_design.model)
            target_id = newmodel["product_line"]["id"] if newmodel["product_line"]
            @product_line_selection_list = ProductLine.option_list(target_id)

            # but the edited values need to be sent back to the form, so the user can see the values he submitted
            @product_design.model = params[:product_design][:modelhash].to_json
            flash.now[:alert] = "Product design could not be saved."
            format.html { render :action => "edit" }
            format.json { render :json => @product_design.errors, :status => :unprocessable_entity }
          else
            #setup_pickers

            flash.now[:alert] = "Product design could not be saved."
            format.html { render :action => "edit_model"}
          end
        end
      end
    end
  end

  # DELETE /product_designs/1
  # DELETE /product_designs/1.json
  def destroy
    @product_design = ProductDesign.find(params[:id])
    @product_design.lock_version = params[:lock_version]
    @product_design.destroy

    respond_to do |format|
      format.html { redirect_to product_designs_url, :notice => "Product design deleted." }
      format.json { head :no_content }
    end
  end

  # POST /product_designs/draft
  def draft
    change_status :draft, "'draft' action"
  end

  # POST /product_designs/review
  def review
    change_status :review, "'review' action",params[:userids]
    Postoffice.product_design_status_change(params[:id]).deliver
  end

  # POST /product_designs/approve
  def approve
    change_status :approved, "'approve' action"
    Postoffice.product_design_status_change(params[:id]).deliver
  end

  # POST /product_designs/approve_directly
  def approve_directly
    change_status :approved, "'approve' action"
    Postoffice.product_design_status_change(params[:id]).deliver
  end

  # POST /product_designs/obsolete
  def obsolete
    change_status :obsolete, "'obsolete' action"
    Postoffice.product_design_status_change(params[:id]).deliver
  end

  # POST /product_designs/revert
  def revert

    product_design = ProductDesign.find(params[:id])
    original_md5 = product_design.product_design_md5
    product_original = product_design.latest_approved
    product_design.status = product_original.status
    product_design.comment = product_original.comment
    product_design.bug_tracker_id = product_original.bug_tracker_id
    product_design.name = product_original.name
    product_design.description = product_original.description
    product_design.model = product_original.model
    product_design.modelhash = JSON.parse(product_design.model)
    product_design.design_tag_id = product_original.design_tag_id
    product_design.updated_by = product_original.updated_by
    product_design.lock_version = params[:lock_version]
    #skip the validation as the status transition does not have to be a valid one in this case
    product_design.save(validate: false)

    # the above will increase the version number by one, so revert it
    # note that just adapting the version number will not result in a new version of @product_design
    product_design.version = product_original.version
    product_design.save!
    synced = 0
    if product_design.design_type == "frontend"
      #revert backend design with reverted conversion steps
      synced = product_design.save_and_synchronize(original_md5).size
    end

    # destroy all versions which come after the version we reverted to
    ProductDesignVersion.where("product_design_id = :id and version > :version",
      { id: params[:id], version: product_original.version } ).destroy_all

    respond_to do |format|
      format.html { redirect_to product_designs_url, notice: "Product design successfully reverted to revision #{product_design.version}. Synced : #{synced}" }
    end
  end

  def get_file_resources
    setup_pickers
    render :layout => false
  end

  def fetch_assembly_lines
    setup_pickers
    render(:partial => "assembly_picker", :collection => @assembly_pickers)
  end

  def get_matching_features
    @option_list = ProductFeature.option_list_for_product_line(params[:product_line_id],session[:project_id])
    render layout:false
  end

  def get_relevant_design_tags
    @taglist = DesignTag.find_by_supplier_and_datatype(params[:supplier], params[:datatype])
    if @taglist
      @taglist = @taglist.descendants.uniq
      ##.uniq
    else
      @taglist = []
    end

    @taglist = @taglist
    @taglist.uniq!
    render :layout => false
  end

  def get_matching_receptions
    model=request.body.read
    stephash = JSON.parse(model)

    params[:search_supplier] = stephash["supplier"]
    params[:search_datatype] = stephash["format"]
    params[:tags] = [] if !params[:tags]
    params[:tags] = stephash["tags"]
    params[:tags] << params[:search_supplier]
    params[:tags] << params[:search_datatype]
    setup_pickers


    @assembly_picker_matches = @assembly_pickers

    render  :layout => false
  end

  def get_files
    puts "get_files"
    model=request.body.read
    @filehash = JSON.parse(model)

    executable = params[:executable]
    path = @filehash["file_path"]

    list = []
    list << path
    exe = Executable.find_by_name(executable)
    if exe
      puts "found exe"
      if exe.source_data_def_type && exe.source_data_def_type == "FILE" && File.directory?(path)
        # check if directory
        list = Dir[path + '/**/*'].reject {|fn| File.directory?(fn) }
      end
    end

    respond_to do |format|
      format.json { render json: list.to_json, status: :created, location: @product_design }
      format.xml { render xml: list.to_xml, status: :created, location: @product_design }
    end

  end
  #used?
  def create_production_order
    pd = ProductDesign.find(params[:id])
    conversiontool_id = params["conversiontool_id"]
    cvtool_bundle_id = params["cvtool_bundle_id"]
    pd.create_production_order(params[:product_id], params[:comment],priority=1, user="johan.hendrikx", conversiontool_id, cvtool_bundle_id)

    respond_to do |format|
      format.json { render json: pd.modelhash.to_json, status: :created, location: pd }
      format.xml { render xml: pd.modelhash.to_xml, status: :created, location: pd }
    end
  end

  def show_diff
    if (params[:old_version] && params[:old_version].to_i > 0)
      @product_design_old = ProductDesign.find(params[:id]).use_version(params[:old_version])
    else
      @product_design_old = ProductDesign.new();
    end
    @product_design_new = ProductDesign.find(params[:id]).use_version(params[:new_version])
    render :layout => false
  end

  def render_target_filling
    @fillings = Filling.active
    if !params[:data_type_name].blank?
      @fillings = DataType.find_by_name(params[:data_type_name].strip).fillings
    end
    render :layout => false
  end

  def import_design
    pd = ProductDesign.find(params[:id])
    pd_to_import = ProductDesign.find(params[:id_to_import])
    pd.import_design(pd_to_import)
    pd.save!
    render :json => pd.modelhash.to_json
  end

#merged from backoffice project
  def api_index
    return_hash = {}
    query_conditions_string_ary = ['1=1']
    query_conditions_param_values = []
    params_h = rm_surrounding_quotes(params)

    id = params_h[:id]
    unless id.blank?
      query_conditions_string_ary <<  "AND product_designs.id = ?"
      query_conditions_param_values << id.strip.to_i
    end

    name = params_h[:name]
    unless name.blank?
      query_conditions_string_ary <<  "AND product_designs.name LIKE ?"
      query_conditions_param_values << "\%#{name.strip}\%"
    end

    region = params_h[:region]
    unless region.blank?
      query_conditions_string_ary <<  "AND regions.name LIKE ?"
      query_conditions_param_values << "\%#{region.strip}\%"
    end

    supplier = params_h[:data_supplier]
    unless supplier.blank?
      query_conditions_string_ary <<  "AND company_abbrs.company_name LIKE ?"
      query_conditions_param_values << "\%#{supplier.strip}\%"
    end

    data_release = params_h[:data_release]
    unless data_release.blank?
      query_conditions_string_ary <<  "( AND data_releases.name LIKE ? OR data_releases.release_code LIKE ? )"
      2.times do
        query_conditions_param_values << "\%#{data_release.strip}\%"
      end
    end

    executable = params_h[:executable]
    unless executable.blank?
      query_conditions_string_ary <<  "AND product_designs.model LIKE ?"
      query_conditions_param_values << "\%#{executable.strip}\%"
    end

    obsolete = params_h[:obsolete]
    if obsolete.blank? || obsolete == '0'
      query_conditions_string_ary <<  "AND ( product_designs.status <> 'obsolete' OR product_designs.status IS NULL )"
    else
      query_conditions_string_ary <<  "AND product_designs.status = 'obsolete'"
    end

    product_designs = ProductDesign.includes(:region,:company_abbr,:data_release).where([query_conditions_string_ary.join(" ")] + query_conditions_param_values).order("product_designs.name")

    product_designs.each do |product_design|
      region = ''
      data_supplier = ''
      data_release = ''
      region = product_design.region.name if product_design.region
      data_supplier = product_design.company_abbr.company_name if product_design.company_abbr
      data_release  = product_design.data_release.name if product_design.data_release
      return_hash[product_design.id] = { :name => product_design.name,
                                         :region=>region,
                                         :data_supplier=>data_supplier,
                                         :data_release=>data_release,
                                         :version=>product_design.version,
                                         :status=>product_design.status,
                                         :api_template_url => url_for(:action=>'template',:controller=>'product_designs',:id=>product_design.id, :only_path => false, :protocol => 'http'),
                                         :executables=>product_design.executables }
    end

    render :json => return_hash.to_json
  end

  def template
    product_design = nil
    qh = rm_surrounding_quotes(params)
    
    unless qh[:id].blank?
      q_id = qh[:id]
      qi = q_id.to_i
      if product_design == nil && qi.to_s == q_id
        begin
          product_design = ProductDesign.find(qi)
        rescue
          product_design = nil
        end
      end
    end

    unless qh[:name].blank?
      q_name = qh[:name]
      product_design = ProductDesign.find_by_name(q_name.strip)
    end

    if product_design == nil
      render :text => "No design found with given name or id '#{qh[:id]}#{qh[:name]}'."
    else
      render :json => product_design.api_template_hash.to_json
    end

  end

  def sub_template
    product_design = ProductDesign.find(params[:id])
    conversion_step = product_design.modelhash["conversion_steps"][params[:step].to_i]
    render :text => product_design.dakota_template_from_conversion_step(conversion_step)
  end

  def syncsubscript
    model = JSON.parse(params[:model])
    pd = ProductDesign.find(params[:id])
    pd.modelhash = model
    pd.sync_subscripts(params[:uuid],params[:subscript_path])
    render :text => 'succes'
  end

  private

  def product_design_params
    params.require(:product_design).permit! if params[:product_design][:product_design] || (params[:product_design] && params[:product_design].to_s.size > 5)
  end

  def setup_pickers
    if params[:reset]
      params.delete :search
      params.delete :reset
      params.delete :search_region
      params.delete :search_datatype
      params.delete :search_supplier
      params.delete :search_release

      params[:sort] = "view_receptions_and_conversiondbs.referenceid"
      params[:direction] = 'desc'
    end
    @params_s = params
    if params[:sort].blank? || params[:sort] == 'assembly_headers.id'
      params[:sec_sort] = "view_receptions_and_conversiondbs.referenceid"
    else
      params[:sec_sort] = params[:sort]
    end

    conditions = '1=1'
    conditions_string_ary = []
    conditions_param_values = []

    region = params[:search_region]
    unless region.blank?
      conditions_string_ary << ' and (regions.name = ?)'
      conditions_param_values << region
    end

    supplier = params[:search_supplier]
    unless supplier.blank?
      conditions_string_ary << " and (company_abbrs.ms_abbr_3 = ?)"
      conditions_param_values << supplier
    end

    release = params[:search_release]
    unless release.blank?
      conditions_string_ary << " and (data_releases.name = ?)"
      conditions_param_values << release
    end

    datatype = params[:search_datatype]
    unless datatype.blank?
      conditions_string_ary << " and (view_receptions_and_conversiondbs.datatype = ?)"
      conditions_param_values << datatype
    end

    unless params[:search].blank?
      conditions_string_ary << " and (view_receptions_and_conversiondbs.referenceid LIKE ?
                                     or view_receptions_and_conversiondbs.datatype LIKE ?
                                       or data_releases.name LIKE ?
                                        or regions.name LIKE ?)"
      4.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
    end

    conditions = conditions + conditions_string_ary.join(" ")
    @conditions = conditions
    @conditions_param_values = conditions_param_values
    #@assembly_header = AssemblyHeader.find(params[:id])
    #@assembly_lines = AssemblyLine.where("assembly_header_id = #{params[:id]}").order("reverse(substring_index(reverse(file_name),'/',1)),reverse(substring_index(reverse(file_name),'_',1)),reverse(substring_index(reverse(file_name),'-',1))")
    @assembly_lines = []

    @assembly_pickers = ViewReceptionsAndConversiondb.joins([{:data_release => :company_abbr}, :region]).where([conditions] + conditions_param_values).order(sec_sort_column + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])
  end

  def change_status(status, comment,userids=nil)
    # intermediate revisions should be ignored.
    @product_design = ProductDesign.find(params[:id]).use_latest
    @product_design.status = status
    @product_design.comment = comment
    @product_design.updated_by = session[:username]
    @product_design.lock_version = params[:lock_version]

    # The version will be updated when status change is saved, so the hash needs to be in sync
    designhash = JSON.parse(@product_design.model)
    designhash["source_record_version"] = @product_design.version.to_i + 1
    @product_design.model = designhash.to_json
    @product_design.modelhash = designhash

    notice = "Status was successfully changed to #{status}."
    sub_notice = ""
    ProductDesign.transaction do
      if @product_design.save
        if @product_design.status.to_s == HandleVersioning.statusarray[ProductDesign::STATUS_APPROVED]

          @product_design.in_use_active_product_designs.each do |iu_pd|
            if iu_pd.status == HandleVersioning.statusarray[ProductDesign::STATUS_PENDING]
              iu_pd.status = HandleVersioning.statusarray[ProductDesign::STATUS_REVIEW]
              iu_pd.save
              sub_notice += "<br />Related design #{iu_pd.id} #{iu_pd.name} set to #{iu_pd.status}."
            end
          end
        end

        notice += sub_notice
        # review request
        if userids
          Postoffice.review_request(@product_design,userids).deliver if status == :review
        end
        redirect_to handle_product_design_path(@product_design), notice: "#{notice}"
      else
        redirect_to handle_product_design_path(@product_design), alert: "Status change to '#{status}' failed."
      end
    end
  end

  def sort_column
    params[:sort] || "name"
  end

  def sec_sort_column
    params[:sec_sort] || "view_receptions_and_conversiondbs.referenceid"
  end

  def sort_direction
    params[:direction] || "desc"
  end

  def search
    params[:search] || ""
  end

  def generate_modelhash_from_params
    hash = JSON.parse(params[:product_design][:modelhash])
    hash["name"] = params[:product_design][:name]
    hash["description"] = params[:product_design][:description]
    # fill any missing data in hash["product_line"]
    hash["product_line"] = ProductLine.to_hash(hash["product_line"]["id"],session[:project_id]) if hash["product_line"] && !hash["product_line"]["id"].blank?
    hash
  end

 

end
