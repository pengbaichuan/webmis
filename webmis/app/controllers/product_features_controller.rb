class ProductFeaturesController < ApplicationController
  # GET /product_features
  # GET /product_features.json
  before_filter :authorize
  authorize_resource
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search

  def index
    conditions = "project_id = #{session[:project_id]}"
    if params[:reset]
      params.delete :search_status
      params.delete :search_product_line
      params.delete :search
      params.delete :obsolete
      params.delete :reset

      params[:sort] = 'product_features.name'
      params[:direction] = 'asc'
    end

    conditions_string_ary = []
    conditions_param_values = []

    include_obsolete = params[:obsolete]

    product_line = params[:search_product_line]
    unless product_line.blank?
      conditions_string_ary << ' and (product_line_id = ?)'
      conditions_param_values << product_line
    end

    status = params[:search_status]
    if status.blank?
      conditions_string_ary << ' and (status is null or status !="obsolete")' unless include_obsolete
    else
      conditions_string_ary << ' and (status = ?)'
      conditions_param_values << status
      include_obsolete ||= (status == 'obsolete')
    end

    params[:obsolete] = include_obsolete
    conditions = conditions + conditions_string_ary.join(" ")

    if sort_column == 'latest_approved'
      @product_features = ProductFeature.where([conditions]+conditions_param_values).sort_by{ |r| r.latest_approved ? r.latest_approved.version : 0}
      @product_features.reverse! if sort_direction == 'desc'
    elsif sort_column == 'product_line'
      @product_features = ProductFeature.where([conditions]+conditions_param_values).sort_by{ |r| r.product_line ? r.product_line.abbr : ""}
      @product_features.reverse! if sort_direction == 'desc'
    else
      @product_features = ProductFeature.where([conditions]+conditions_param_values).order(sort_column + ' ' +sort_direction)
    end

    unless params[:search].blank?
      search = params[:search].strip.downcase
      matched_product_features = []
      # latest approved and translation type can be nil, handle it, The other attributes should be filled
      @product_features.each do |product_feature|
        if ( product_feature.name && product_feature.name.to_s.downcase.include?(search) ||
             product_feature.abbr_code && product_feature.abbr_code.to_s.downcase.include?(search) ||
             product_feature.description && product_feature.description.to_s.downcase.include?(search) ||
             ( product_feature.translation_type && product_feature.translation_type.to_s.downcase.include?(search) ) ||
             ( product_feature.latest_approved && product_feature.latest_approved.version.to_s.include?(search) ) ||
             product_feature.version.to_s.downcase.include?(search) ||
             product_feature.status.to_s.downcase.include?(search)
           )
          matched_product_features << product_feature
        end
      end
      @product_features = matched_product_features
    end

    @product_features = @product_features.paginate(per_page: 100, page: params[:page])

    @used_product_lines = ProductFeature.used_product_lines

    respond_to do |format|
      if params[:search] || params[:search_status]
        flash.now[:notice] = @product_features.total_entries.to_s + " records found."
      end
      format.html # index.html.erb
      format.json { render json: @product_features }
    end
  end

  # GET /product_features/1
  # GET /product_features/1.json
  def show
    @product_feature = ProductFeature.find(params[:id])
    @product_feature_version = @product_feature

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @product_feature }
    end
  end

  # GET /product_features/1/latest
  def approved
    @product_feature = ProductFeature.find(params[:id]).use_latest_approved
    @product_feature_version = @product_feature

    respond_to do |format|
      format.html { render action: "show"} # latest.html.erb
    end
  end

  def handle
    @product_feature = ProductFeature.find(params[:id])
    @product_feature_version = @product_feature
    @current_user = current_user
  end

  # GET /product_features/new
  # GET /product_features/new.json
  def new
    @product_feature = ProductFeature.new(:status => 'draft', :comment => 'manual initial version', :updated_by => current_user.user_name)
    @product_line_selection_list = ProductLine.option_list(@product_feature.product_line_id)

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @product_feature }
    end
  end

  # GET /product_features/1/edit
  def edit
    @product_feature = ProductFeature.find(params[:id])
    # the default new status status of an edit should always be ':draft'
    @product_feature.status = :draft
    @product_line_selection_list = ProductLine.option_list(@product_feature.product_line_id)
  end

  def render_system_translation
    product_feature = ProductFeature.find(params[:id])
    product_feature_version = product_feature.versions.where("version='#{params[:version]}'").first
    # As the versioning was added later, there are still product_feature instances which don't have version, the check on product_feature_version
    # can be removed once all product_feature instances have versions
    system_translation = product_feature_version ? product_feature_version.system_translation : product_feature.system_translation
    # product valid JSON in case of empty contents
    system_translation = "{}" unless system_translation && !system_translation.empty?
    respond_to do |format|
      format.json { render json: system_translation, status: :created }
    end
  end

  # POST /product_features
  # POST /product_features.json
  def create
    #@product_feature = ProductFeature.new(:status => 'new', :comment => 'manual initial version', :updated_by => session[:username], :abbr_code => ProductFeature.maximum('id'))
    @product_feature = ProductFeature.new(params[:product_feature])
    @product_feature.status = 'draft'
    @product_feature.comment = 'manual initial version'
    @product_feature.updated_by = current_user.user_name
    @product_feature.project_id = session[:project_id]
    @product_line_selection_list = ProductLine.option_list(params[:product_feature][:product_line_id])
    respond_to do |format|
      if @product_feature.save
        format.html { redirect_to({ :action => 'handle', :id => @product_feature.id }, :notice => 'New product feature created.') }
        format.json { render :json => @product_feature, :status => :created, :location => @product_feature , :methods => [ :design_id,  :meta_string ]}
      else
        format.html { render :action => "new" }
        format.json { render :json => @product_feature.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /product_features/1
  # PUT /product_features/1.json
  def update
    @product_feature = ProductFeature.find(params[:id])
    respond_to do |format|
      if @product_feature.update_attributes(product_feature_params.update(:status => 'draft',
                                                                            :updated_by => session[:username]
                                                                          ))
        format.html { redirect_to({ :action => 'handle', :id => @product_feature.id }, :notice => 'Product feature was successfully updated.') }
        format.json { head :no_content }
      else
        @product_line_selection_list = ProductLine.option_list(params[:product_feature][:product_line_id])
        flash.now[:alert] =  "Product feature could not be saved."
        format.html { render :action => "edit" }
        format.json { render :json => @product_feature.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /product_features/1
  # DELETE /product_features/1.json
  def destroy
    @product_feature = ProductFeature.find(params[:id])
    @product_feature.destroy

    respond_to do |format|
      format.html { redirect_to product_features_url, :notice => "Product feature deleted." }
      format.json { head :no_content }
    end
  end

  # POST /product_features/draft
  def draft
    change_status :draft, "'draft' action"
  end

  # POST /product_features/review
  def review
    change_status :review, "'review' action"
  end

  # POST /product_features/approve
  def approve
    change_status :approved, "'approve' action"
  end

  # POST /product_features/approve_directly
  def approve_directly
    change_status :approved, "'approve' action"
  end

  # POST /product_features/obsolete
  def obsolete
    change_status :obsolete, "'obsolete' action"
  end

  # POST /product_features/revert
  def revert
    product_feature = ProductFeature.find(params[:id])
    product_original = product_feature.latest_approved
    product_feature.status = product_original.status
    product_feature.comment = product_original.comment
    product_feature.bug_tracker_id = product_original.bug_tracker_id
    product_feature.name = product_original.name
    product_feature.description = product_original.description
    product_feature.abbr_code = product_original.abbr_code
    product_feature.product_line_id = product_original.product_line_id
    product_feature.system_translation = product_original.system_translation
    product_feature.translation_type = product_original.translation_type
    product_feature.updated_by = product_original.updated_by
    #skip the validation as the status transition does not have to be a valid one in this case
    product_feature.save(validate: false)

    # the above will increase the version number by one, so revert it
    # note that just adapting the version number will not result in a new version of @product_feature
    product_feature.version = product_original.version
    product_feature.save

    # destroy all versions which come after the version we reverted to
    ProductFeatureVersion.where("product_feature_id = :id and version > :version",
                               { id: params[:id], version: product_original.version } ).destroy_all

    respond_to do |format|
      format.html { redirect_to product_features_url, notice: "Product feature successfully reverted to revision #{product_feature.version}." }
    end
  end

  def render_exe_as_json
    @executable = Executable.find_by_name(params[:exe_name])
    render :json => @executable
  end

  private

  def product_feature_params
    params.require(:product_feature).permit!
  end

  
  def change_status(status, comment)
    @product_feature = ProductFeature.find(params[:id])
    @product_feature.status = status
    @product_feature.comment = comment
    @product_feature.updated_by = session[:username]
    if @product_feature.save
      redirect_to product_features_url, notice: "Status was successfully changed to #{status}."
    else
      redirect_to product_features_url, alert: "Status change to '#{status}' failed."
    end
  end

  def sort_column
    params[:sort] || "product_features.name"
  end

  def sort_direction
    params[:direction] || "asc"
  end

  def search
    params[:search] || ""
  end

end
