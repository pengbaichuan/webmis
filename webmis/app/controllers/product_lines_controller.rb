class ProductLinesController < ApplicationController
  before_filter :authorize
  authorize_resource

  layout "webmis"
  helper_method :sort_column, :sort_direction, :search, :search_active
  
  # GET /product_lines
  # GET /product_lines.xml

  def index

    conditions =  "(parameter_settings.project_id = #{session[:project_id]} OR parameter_settings.id is null)"
    conditions_param_values = []
    
    if params[:reset]
      params.delete :search
      params.delete :reset

      params[:search_active] = 1
      params[:sort] = 'product_lines.name'
      params[:direction] = 'asc'
    end
    
    # set default to 1 for the initial index screen
    params[:search_active] = 1 unless params[:search_active]
    active = params[:search_active]
    unless active.blank?
      conditions += " and (product_lines.is_active = ?)"
      conditions_param_values << active
    end

    if params[:search] && params[:search] != ""
      conditions += " and (product_lines.abbr LIKE ? OR product_lines.name LIKE ? OR company_abbrs.company_name LIKE ? OR parameter_settings.name LIKE ?)"
      4.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
    end

    unless params[:product_line].blank?
      conditions += " and (product_lines.id = ? or product_lines.parent_id = ?) "
      2.times do
        conditions_param_values << params[:product_line]
      end
    end
    
    @product_lines = ProductLine.select("distinct product_lines.*").joins('LEFT OUTER JOIN company_abbrs ON company_abbrs.id = product_lines.company_abbr_id ' +
        'LEFT OUTER JOIN parameter_settings ON parameter_settings.product_line_id = product_lines.id').where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(per_page: 100, page: params[:page])
    generate_id_sequence_cookie(@product_lines,params[:page])
    if request.xhr?
      flash.keep[:notice]
      render(:partial => "product_line", :collection => @product_lines)
    else
      respond_to do |format|
        format.html # index.html.erb
      end
    end
  end

  # GET /product_lines/1
  # GET /product_lines/1.xml
  def show
    @product_line = ProductLine.find(params[:id])
    @customer_content_definition = CustomerContentDefinition.new(product_line_id: @product_line.id)
    if @product_line.parameter_setting(session[:project_id])
      @parameter_settings = JSON.parse(@product_line.parameter_setting(session[:project_id]).parameterset).sort_by{|x|[x["exe"],x["name"]]}
    else
      @parameter_settings = []
    end

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @product_line }
    end
  end

  # GET /product_lines/new
  # GET /product_lines/new.xml
  def new
    @product_line = ProductLine.new
    @product_line.is_active = true
    @product_line_allowed_data = []
    @project_members = []
    @data_types = []
    @regions = []
    @company_abbrs = []
    @parameter_settings = []
  end

  # GET /product_lines/1/edit
  def edit
    @product_line = ProductLine.find(params[:id])
    @project_members = @product_line.users
    @product_line_allowed_data = @product_line.product_line_allowed_data
    
    if @product_line.parameter_setting(session[:project_id])
      @parameter_settings = JSON.parse(@product_line.parameter_setting(session[:project_id]).parameterset).sort_by{|x|[x["exe"],x["name"]]}
    else
      @parameter_settings = []
    end
    
  end

  # POST /product_lines
  # POST /product_lines.xml
  def create
    @product_line = ProductLine.new(strong_params)
    if params[:members].blank?
      @project_members = []
    else
      @project_members = User.find(params[:members])
    end

    respond_to do |format|
      ProductLine.transaction do
        if @product_line.save
          @product_line.create_parameter_setting(session["project_id"])
          update_customer_content_definitions
          if !params[:members].blank?
            @product_line.update_project_members(params[:members])
          end
          @product_line.update_allowed_data(params[:allowed_data] || [])


          flash[:notice] = 'Target platform was successfully created.'
          format.html { redirect_to(@product_line) }
          format.xml  { render :xml => @product_line, :status => :created, :location => @product_line }
        else
          @product_line_allowed_data = []
          format.html { render :action => "new" }
          format.xml  { render :xml => @product_line.errors, :status => :unprocessable_entity }
        end
      end
    end
  end

  # PUT /product_lines/1
  # PUT /product_lines/1.xml
  def update
    @product_line = ProductLine.find(params[:id])
    if params[:members].blank?
      @project_members = []
    else
      @project_members = User.find(params[:members])
    end

    respond_to do |format|
      if @product_line.update_attributes(strong_params)
        update_customer_content_definitions
        project_member_user_ids = []
        
        if !params[:members].blank?
          project_member_user_ids = params[:members]
        end
        @product_line.update_project_members(project_member_user_ids)
        @product_line.update_allowed_data(params[:allowed_data] || [])

       
        flash[:notice] = 'Target platform was successfully updated.'
        format.html { redirect_to(@product_line) }
        format.xml  { head :ok }
      else
        @product_line_allowed_data = @product_line.product_line_allowed_data
        format.html { render :action => "edit" }
        format.xml  { render :xml => @product_line.errors, :status => :unprocessable_entity }
      end
    end
  end

  def parameters
    if ps = ParameterSetting.where(:id => params[:parameter_setting_id],:project_id => session[:project_id]).latest_approved
      @parameter_settings = JSON.parse(ps.parameterset).sort_by{|x|[x["exe"],x["name"]]}
    end
    if request.xhr?
      render(:partial => "parameter", :collection => @parameter_settings)
    else
      respond_to do |format|
        render :nothing => true
      end
    end
  end

  def update_project_members_list
    if params[:members].blank?
      @project_members = []
    else
      @project_members = User.active.find([params[:members]])
    end
  end



  private  
  def update_customer_content_definitions
    # check for each of the existing customer content definitions whether it was not removed
    still_present_ids = params[:ccdef_ids] ? params[:ccdef_ids].collect{|c| c[0].to_i} : []
    @product_line.customer_content_definitions.each do |ccdef|
      unless still_present_ids.include?(ccdef.id)
        ccdef.destroy
      end
    end

    # add newly defined customer content definitions
    if params[:ccdef_names] && params[:ccdef_labels] && params[:ccdef_output_targets]
      params[:ccdef_names].each do |key, name|
        customer_content_definition = CustomerContentDefinition.new()
        customer_content_definition.customer_content_specification = CustomerContentSpecification.find(name)
        customer_content_definition.label = params[:ccdef_labels][key]
        customer_content_definition.output_target = params[:ccdef_output_targets][key].to_i
        @product_line.customer_content_definitions << customer_content_definition
        customer_content_definition.save!
      end
    end
  end

  def sort_column  
    params[:sort] || "product_lines.name"  
  end  
    
  def sort_direction  
    params[:direction] || "asc"  
  end
  
  def search
    params[:search] || ""
  end 

  def search_active
    params[:search_active] || 1
  end

end
