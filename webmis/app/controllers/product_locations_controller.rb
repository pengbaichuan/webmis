class ProductLocationsController < ApplicationController 
  before_filter :authorize
  authorize_resource
  
  # GET /product_locations
  # GET /product_locations.xml
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search
  
  def index
    if params[:reset]
      params.delete :search
      params.delete :reset

      params.delete :sort
      params.delete :direction
    end
    conditions_string_ary = ["1=1"]
    conditions_param_values = []

    unless params[:search].blank?
      q = params[:search].strip
      conditions_string_ary << "(product_locations.id = ? OR
                                product_locations.storage_path LIKE ? OR
                                product_locations.internal_storage_path LIKE ? OR
                                product_locations.product_id LIKE ? OR
                                product_locations.conversion_id LIKE ? OR
                                product_locations.product_format LIKE ? OR
                                product_locations.area LIKE ? OR
                                product_locations.supplier LIKE ? OR
                                product_locations.data_release LIKE ?)"
      conditions_param_values << q.to_i # for id
      8.times do
        conditions_param_values << "%#{q}%"
      end
    end

    conditions = conditions_string_ary.join(' AND ')

    @product_locations = ProductLocation.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])

    if request.xhr?
      render(:partial => "product_location", :collection => @product_locations)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @product_locations }
      end
    end
  end

  # GET /product_locations/1
  # GET /product_locations/1.xml
  def show
    @product_location = ProductLocation.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @product_location }
    end
  end

  def report
    @product_location = ProductLocation.find(params[:id])
    @conversion = @product_location.conversion
    @production_order = @product_location.conversion.production_orderline.production_order rescue nil
    @input_report_data = @conversion.input_report_data
  end

  # GET /product_locations/new
  # GET /product_locations/new.xml
  def new
    @cvs = []
    @rcs = []
    @product_location = ProductLocation.new
    @product_location.registration_date = Date.today
    @pol = nil
    if params[:production_orderline_id]
      #@product_location.production_orderline_id = params[:production_orderline_id]
      @pol = ProductionOrderline.find(params[:production_orderline_id])
    end
    
    if params["production_order_id"]
      ProductLocation.transaction do
        @product_location.map_from_po(ProductionOrder.find(params["production_order_id"]))
      end
    end

    if params[:conversion_id]
      cv = Conversion.find(params[:conversion_id])
      productid = cv.productid
      productid = @pol.product_id if @pol
      @product_location.map_from_cv(cv, productid)
      @cvs << cv
    end
   
    if params[:reception_id]
      rc = Reception.find(params[:reception_id])
      @product_location.map_from_rc(rc)
      @cvs << cv
    end
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @product_location }
    end
  end

  # GET /product_locations/1/edit
  def edit
    @cvs = []
    @rcs = []
    @product_location = ProductLocation.find(params[:id])
    @product_location.registration_date.strftime('%d-%m-%Y') unless @product_location.registration_date.blank?
  end

  # POST /product_locations
  # POST /product_locations.xml
  def create
    
    @product_location = ProductLocation.new(strong_params)
    @cvs = Conversion.where(productid:params[:product_location][:product_id])
    @rcs = Reception.where(:id => params[:product_location][:reception_id])
    @product_location.registration_date = DateTime.now
    
    Conversion.transaction do
      respond_to do |format|
        if @product_location.save

          if !params[:production_orderline_id].blank?
            pol = ProductionOrderline.find(params[:production_orderline_id])
            pol.product_location_id = @product_location.id
            pol.save
          end

          message = @product_location.flag_conversion_databases_for_removed
          if @product_location.production_orderlines
            @product_location.production_orderlines.each do |line |
              line.transition if line.bp_name == "MASTERING"
              line.save
            end
          end
          flash[:notice] = 'Product was successfully registered.' + message
          if ((!params[:return_to_controller].blank? && !params[:return_to_action].blank? && !params[:return_to_id].blank?))
            format.html { redirect_to :action => params[:return_to_action].to_s,
                :controller => params[:return_to_controller].to_s, :id => params[:return_to_id] }
          else
            format.html { redirect_to(@product_location) }
            format.xml  { render :xml => @product_location, :status => :created, :location => @product_location }
          end
        else
          format.html { render :action => "new" }
          format.xml  { render :xml => @product_location.errors, :status => :unprocessable_entity }
        end
      end
    end
  end

 
  # PUT /product_locations/1
  # PUT /product_locations/1.xml
  def update
    @product_location = ProductLocation.find(params[:id])
    params[:product_location][:registration_date] = DateTime.strptime(params[:product_location][:registration_date],"%d-%m-%Y") if params[:product_location][:registration_date]
    respond_to do |format|
      ProductLocation.transaction do
        if @product_location.update_attributes(strong_params)
         message = @product_location.flag_conversion_databases_for_removed
         if @product_location.production_orderlines
            @product_location.production_orderlines.each do |line |
              line.transition if line.bp_name == "MASTERING"
              line.save
            end
          end

          flash[:notice] = 'ProductLocation was successfully updated.' + message
          format.html { redirect_to(@product_location) }
          format.xml  { head :ok }
        else
          format.html { render :action => "edit" }
          format.xml  { render :xml => @product_location.errors, :status => :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /product_locations/1
  # DELETE /product_locations/1.xml
  def destroy
    @product_location = ProductLocation.find(params[:id])
    @product_location.destroy

    respond_to do |format|
      format.html { redirect_to(product_locations_url) }
      format.xml  { head :ok }
    end
  end

  def get_details_by_conversion

    @cvs = []
    @gdc = []
    @rcs = []
    @cvs = Conversion.finished_available.where(:productid => params[:product_id])
    
    if params[:conversion_id] && !params[:conversion_id].blank? && params[:conversion_id] != "undefined"
      cv = Conversion.find(params[:conversion_id])
      @product_location = ProductLocation.new
      @product_location.map_from_cv(cv,params[:product_id])
      @product_location.product_id = @product_location.product_id.split('.')[0]
    else
      @product_location = ProductLocation.new
    end


    if @cvs.size == 0
      get_details_by_reception
    else
      render :layout => false
    end
  end
  
  def get_details_by_reception

    @rcs = []
    @gdr = []
    @cvs = []
    @rcs = Reception.where(:product_id => params[:product_id])
    if params[:reception_id] && params[:reception_id].size > 0 && params[:reception_id] != "undefined"
      rc = Reception.find(params[:reception_id])
      @product_location = ProductLocation.new
      @product_location.map_from_rc(rc)
      @product_location.product_id = @product_location.product_id.split('.')[0]
    else
      @product_location = ProductLocation.new
    end

    render :layout => false

  end
  
  def get_checksum_file_content
    @checksum = ProductLocation.read_checksum_from_product_location_storage_path(params[:storage_path])
    render :layout => false
  end
  
  private  
  def sort_column  
    params[:sort] || "product_locations.id"  
  end  
    
  def sort_direction  
    params[:direction] || "desc"  
  end
  
  def search
    params[:search] || ""
  end    
end
