class ProductReleaseNotesController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout 'webmis'
  # GET /product_release_notes
  # GET /product_release_notes.xml
  
  require "zip/zip"
  def index
    @product_release_notes = ProductReleaseNote.all.order("id desc")

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @product_release_notes }
    end
  end

  # GET /product_release_notes/1
  # GET /product_release_notes/1.xml
  def show
    @product_release_note = ProductReleaseNote.find(params[:id])
    @unused_static_release_note_titles = Array.new
    if @product_release_note.product_category
      @product_release_note.content_release_notes.where(:addition_required_yn => true).each do |srt|
        
          @unused_static_release_note_titles = @unused_static_release_note_titles << srt

      end
    end

    respond_to do |format|  
      format.html # show.html.erb
      format.xml  { render :xml => @product_release_note }
      format.pdf {
        file_name = ""
        if @product_release_note.productset
          file_name = @product_release_note.productset.name.to_s.gsub(/[^\w\s_-]+/, '').gsub(/(^|\b\s)\s+($|\s?\b)/, '\\1\\2').gsub(/\s+/, '_') 
        else
          file_name = @product_release_note.production_orderline.product.name.to_s.gsub(/[^\w\s_-]+/, '').gsub(/(^|\b\s)\s+($|\s?\b)/, '\\1\\2').gsub(/\s+/, '_') 
        end
        Dir.mktmpdir{|dir|
          File.open(ProductReleaseNotePdf.new(@product_release_note).write_pdf(dir), 'r') do |f|
            send_data f.read, :type => "text/xml""application/pdf", :filename => "#{file_name}.pdf"
          end
        }
      }
    end
  end

  # GET /product_release_notes/1/edit
  
  def create_from_production_order
     @product_release_note = ProductReleaseNote.new
     @product_release_note.production_order_id = params[:production_order_id]
     @product_release_note.user_id = current_user.id
     @product_release_note.status = 0
     
     if @product_release_note.save
       pcat = @product_release_note.production_order.ordertype.product_category_id

       ContentReleaseNote.prepare_default_content(pcat,@product_release_note.id)
       
       redirect_to :id => @product_release_note.id, :action => :show
     else
       flash[:notice] = "The creation of the release notes failed."
       redirect_to :back
     end     
  end
  
  def create_from_production_orderline
     @product_release_note = ProductReleaseNote.new
     @product_release_note.production_orderline_id = params[:production_orderline_id]
     @product_release_note.user_id = current_user.id
     @product_release_note.status = ProductReleaseNote::STATUS_DRAFT
     
     if @product_release_note.save
       pcat = @product_release_note.production_orderline.production_order.ordertype.product_category_id

       ContentReleaseNote.prepare_default_content(pcat,@product_release_note.id)
       
       redirect_to :id => @product_release_note.id, :action => :show
     else
       flash[:notice] = "The creation of the release notes failed."
       redirect_to :back
     end     
  end

  def edit
    @product_release_note = ProductReleaseNote.find(params[:id])
  end

  # PUT /product_release_notes/1
  # PUT /product_release_notes/1.xml
  def update
    @product_release_note = ProductReleaseNote.find(params[:id])

    respond_to do |format|
      if @product_release_note.update_attributes(strong_params)
        format.html { redirect_to(@product_release_note, :notice => 'ProductReleaseNote was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @product_release_note.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /product_release_notes/1
  # DELETE /product_release_notes/1.xml
  def destroy
    @product_release_note = ProductReleaseNote.find(params[:id])
    @product_release_note.destroy
    logger.info 'destroyed release notes by ' + current_user.first_name + " " + current_user.last_name
    logger.info '--------------------------------------------'

    respond_to do |format|
      format.html { redirect_to(product_release_notes_url) }
      format.xml  { head :ok }
    end
  end
  
  def move_updown
    @product_release_note = ProductReleaseNote.find(params[:id])
    if @product_release_note.status != 0
      flash.now[:notice] = "You changed the release notes, status is set to DRAFT."
      logger.info 'changed release notes by ' + current_user.first_name + " " + current_user.last_name
      logger.info '--------------------------------------------'
      @product_release_note.status = 0
      @product_release_note.save
    end
    
    if params[:direction] && params[:direction] == "up"
      ContentReleaseNote.find(params[:content_release_note_id]).ranking("down")
    else
      ContentReleaseNote.find(params[:content_release_note_id]).ranking("up")
    end
    render :partial => 'content_release_notes',:layout => false
  end
  
  def move_leftright
    @product_release_note = ProductReleaseNote.find(params[:id])
    if @product_release_note.status != 0
      flash.now[:notice] = "You changed the release notes, status is set to DRAFT."
      logger.info 'changed release notes by ' + current_user.first_name + " " + current_user.last_name
      logger.info '--------------------------------------------'
      @product_release_note.status = 0
      @product_release_note.save
    end
    
    if params[:direction] && params[:direction] == "left"
      ContentReleaseNote.find(params[:content_release_note_id]).subranking("left")
    else
      ContentReleaseNote.find(params[:content_release_note_id]).subranking("right")
    end
    render :partial => 'content_release_notes',:layout => false
  end
  
  def to_status
    @product_release_note = ProductReleaseNote.find(params[:id])
    nope = false
    
    if !@product_release_note.content_release_notes.where(:addition_required_yn => true).first.nil?
      nope = true
    end
    if @product_release_note.production_order && !@product_release_note.production_order.documents.where(:mandatory_yn => true,:binary_data => nil).first.nil?
      nope = true
    end
    if @product_release_note.production_orderline && !@product_release_note.production_orderline.production_order.documents.where(:mandatory_yn => true, :binary_data => nil).empty?
      nope = true
    end    
    if nope == false
      @product_release_note.status = params[:status]
      @product_release_note.save
      flash[:notice] = "Release notes set to #{@product_release_note.status_string}."
      logger.info 'changed status of release notes by ' + current_user.first_name + " " + current_user.last_name
      logger.info '--------------------------------------------'
      redirect_to :back
    else
      flash[:error] = 'mandatory content or attachments is not completed.<br>Status release notes is not changed!'
      redirect_to :back
    end
  end
  
  def package
    @product_release_note = ProductReleaseNote.find(params[:id])
    zipfile = ProductReleaseNotePdf.new(@product_release_note).generate

    #send_file zipfile, :type=>"application/zip"
    flash[:notice] = "Release notes package created."
    redirect_to :back
  end


end
