class ProductionOrderlinesController < ApplicationController
  # GET /production_orderlines
  # GET /production_orderlines.xml
  before_filter :authorize
  authorize_resource

  layout "webmis"
  helper_method :sort_column, :sort_direction, :search

  def show

  end

  def show_shipment_request
    @production_orderline = ProductionOrderline.find(params[:id])
  end

  def mastering_index
    @production_orderlines = ProductionOrderline.where(:bp_name => 'MASTERING', :bp_status => 'PROCESSING').eager_load(:production_order).order('production_order_id, ' + sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])
    if request.xhr?
      render(:partial => "mastering_production_orderline", :collection => @production_orderlines)
    else
      respond_to do |format|
        format.html
        format.json { render :json => @production_orderlines}
      end
    end
  end

  def shipment_index
    @production_orderlines = ProductionOrderline.where(:bp_name => 'SHIPMENT REQUESTED').eager_load(:production_order).order('production_order_id, ' + sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])
    if request.xhr?
      render(:partial => "production_orderline", :collection => @production_orderlines)
    else
      respond_to do |format|
        format.html
        format.json {render :json => @production_orderlines}
      end
    end
  end

  def request_shipment
    flmsg = ''
    @production_orderline = ProductionOrderline.find(params[:production_orderline_id])
    if @production_orderline.production_order.allow_shipment
      @production_orderline.transition
    else
      flmsg = 'Shipping is not allowed.'
    end

    respond_to do |format|
      if @production_orderline.production_order.allow_shipment && @production_orderline.save
        Postoffice.shipment_request(@production_orderline.id.to_s.split).deliver_now
        flash[:notice] = 'Shipment was successfully requested.'
        format.html { redirect_to(:controller => 'production_orders', :action => 'manage', :id => @production_orderline.production_order.id) }
        format.xml  { head :ok }
      else
        flash[:error] = "Shipment request failed. #{flmsg}"
        format.html { redirect_to(:controller => 'production_orders', :action => 'manage', :id => @production_orderline.production_order.id) }
        format.xml  { render :xml => @production_order.errors, :status => :unprocessable_entity }
      end
    end
  end

  # not used?
  def evaluate
    @production_orderline = ProductionOrderline.find(params[:production_orderline_id])
    @databases = Conversion.find(@production_orderline.chosen_conversion_id).source_conversion_databases
  end

  def release_or_reset
    begin
      reject_dbs = []
      params.keys.each{|x|reject_dbs << x.gsub("chk_","") if x.match(/chk_/)}
      puts "REJECT DBS :: " + reject_dbs.inspect
      @production_orderline = ProductionOrderline.find(params[:id])
      if params["release_status"] == "RELEASE"
        flash[:notice] = 'Product was succesfully released.'
        @production_orderline.transition
      else
        @production_orderline.reset(current_user,params[:conversiontool_id],params[:cvtool_bundle_id], reject_dbs)
        flash[:notice] = 'Product was succesfully reset.'
      end
      
    rescue ActiveRecord::StaleObjectError => e
      puts e.message
      puts e.backtrace
      flash[:error] = "Orderline change will you were resetting it. Reset NOT succeeded."
    end

    redirect_to :action => :manage, :id => @production_orderline.production_order.id, :controller => :production_orders
  end

  def request_shipment_bulk
    ids = []
    @production_order = ProductionOrder.find(params[:production_order_id])
    @production_orderlines = @production_order.production_orderlines.where(:bp_name => 'REQUEST SHIPMENT')
    @production_orderlines.each do |line|
        line.transition
        line.chosen_conversion_id = line.conversionlast.id
        ids << line.id.to_s
        line.save
    end
    respond_to do |format|
      if ids.size > 1
        Postoffice.shipment_request(ids).deliver_now
        flash[:notice] = 'Shipments are successfully requested.'
        format.html { redirect_to(:controller => 'production_orders', :action => 'manage', :id => @production_orderlines.first.production_order.id) }
        format.xml  { head :ok }
      else
        flash[:error] = 'Shipment request failed.'
        format.html { redirect_to(:controller => 'production_orders', :action => 'manage', :id => @production_orderlines.first.production_order.id) }
        format.xml  { render :xml => @production_order.errors, :status => :unprocessable_entity }
      end
    end
  end

  def on_time_delivery_report
    if params[:reset]
      params.delete :reset
      params.delete :date_range_a
      params.delete :date_range_b
      params.delete :ordertype_id
      params.delete :use_baseline_commitment
    end

    # default date range is the last month, counting from today
    date_range_a = Date.parse(params[:date_range_a])  rescue Date.today - 1.month
    date_range_b = Date.parse(params[:date_range_b])  rescue Date.today
    params[:date_range_a] = date_range_a.strftime('%d-%m-%Y')
    params[:date_range_b] = date_range_b.strftime('%d-%m-%Y')

    conditions_string  = "production_orders.project_id = #{session[:project_id]}"
    conditions_string += " AND outbound_orders.send_date BETWEEN '#{date_range_a}' AND '#{date_range_b}' "
    conditions_string += " AND production_orderlines.bp_name != '#{ProductionOrderline::BP_NAME_CANCELLED}'"
    conditions_string += " AND production_orders.bp_name != '#{ProductionOrder::BP_NAME_CANCELLED}'"
    conditions_string += ' AND ordertypes.is_frontend = false'
    conditions_array = []

    unless params[:product_line_id].blank?
      if params[:product_line_id] == '-'
        conditions_string += ' AND production_orderlines.product_line_id IS NULL'
      else
        conditions_string += ' AND production_orderlines.product_line_id = ?'
        conditions_array << params[:product_line_id]
      end
    else
      if params[:product_lines].class == Array
        conditions_string +=  " AND production_orderlines.product_line_id IN (#{params[:product_lines].join(',')})"
      end
    end

    join_string  =  'INNER JOIN outbound_orderlines ON outbound_orderlines.production_orderline_id = production_orderlines.id'
    join_string += ' INNER JOIN outbound_orders ON outbound_orderlines.outbound_order_id = outbound_orders.id'
    join_string += ' INNER JOIN production_orders on production_orderlines.production_order_id = production_orders.id'
    join_string += ' INNER JOIN ordertypes ON production_orders.ordertype_id = ordertypes.id'
    @production_orderlines = ProductionOrderline.joins(join_string).where([conditions_string] + conditions_array).uniq
    if request.xhr?
      render :partial => 'on_time_delivery_report'
    else
      respond_to do |format|
        format.html  # on_time_delivery_report.html.erb
        format.xml { render :xml => @production_orderlines}
        format.csv { render :csv => @production_orderlines, :filename => "on_time_#{params[:use_baseline_commitment] ? 'baseline' : 'modified'}_delivery_report_#{Time.now.strftime('%Y%m%d-%H%M')}", :style => :on_time_delivery_report}
      end
    end
  end

  private
  def sort_column
    params[:sort] || "production_orders.modified_commitment_date"
  end

  def sort_direction
    params[:direction] || "asc"
  end

  def search
    params[:search] || ""
  end
end