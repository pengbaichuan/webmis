class ProductionOrdersController < ApplicationController
  # GET /production_orders
  # GET /production_orders.xml
  before_filter :authorize, :except => [:parameters_for_product, :tool_bundles, :features_for_product, :poi_mapping_for_product, :improvement_processes_for_product, :output_location_for_product, :get_orderline]
  authorize_resource :except => [:parameters_for_product, :tool_bundles, :features_for_product, :poi_mapping_for_product, :improvement_processes_for_product, :output_location_for_product, :get_orderline]

  layout "webmis"
  helper_method :sort_column, :sort_direction, :search

  def index

    if params[:reset] == 'reset'
      params.delete :search
      params.delete :sort
      params.delete :direction
      params.delete(:finished)
      params[:reset] = 'done'
    end

    if params[:finished] == "1" && params[:search]
      @finished = true
    else
      @finished = false
    end
#disabled for performance reasons as this functionality is not used
    #@tags = ProductionOrder.all.collect {|t|t.tags.collect {|n|n.name}}.uniq.flatten
    @tags = []
    @productid_filter = ""
    @productid_tagfilter = []
    conditions = "production_orders.project_id = #{session[:project_id]}"

    if params[:search] && (params[:commit] != "reset" || !params[:commit]) && (params[:search].strip != '')
      @productid_filter = params[:search]
      production_order_ids = ProductionOrderline.where(["product_id LIKE ?","%#{params[:search].strip}%"])
      if production_order_ids.size > 0
        conditions = conditions + " AND"
        production_order_ids.collect {|p|conditions = conditions + " production_orders.id = '#{p.production_order_id}' OR"}
        conditions = conditions + " production_orders.id = '#{production_order_ids[0].production_order_id}' or production_orders.name like '%#{params[:search].strip}%'"
      else
        conditions = conditions + " AND"
        conditions = conditions + " production_orders.name like '%#{params[:search].strip}%'"
      end
    end

    if params[:status_filter]
      @finished = true
      conditions = conditions + " AND production_orders.bp_name = '#{params[:status_filter]}'"
    end
    if params[:id_filter]
      @finished = true
      conditions = conditions + " AND production_orders.id = '#{params[:id_filter]}'"
    end

    if params[:tag] && (params[:commit] != "reset" || !params[:commit])
      @productid_tagfilter = params[:tag]

      tagging = ProductionOrder.tagged_with("#{@productid_tagfilter}",:match => :any)
      tag_conditions= ""
      latest_hit = ""


      if !tagging.empty?
        tagging.each do |t|
          tag_conditions = tag_conditions + "production_orders.id = " + t.id.to_s  + " OR "
          latest_hit = "production_orders.id = " + t.id.to_s
        end
        conditions = "(" + conditions + ")" + " AND (" + tag_conditions + " #{latest_hit})"
      end
    end

    if params[:productset_id]
      conditions = conditions + " AND "
      conditions = conditions + "production_orders.productset_id = #{params[:productset_id]}"
    end

    if @finished == false
      conditions = conditions + " AND "
      conditions = conditions + "(production_orders.bp_name != '#{ProductionOrder::BP_NAME_CANCELLED}' AND production_orders.bp_name != '#{ProductionOrder::BP_NAME_COMPLETE}' and production_orders.bp_name != '#{ProductionOrder::BP_NAME_DELETED}')"
    end


    unless params[:product_line].blank?
       #conditions << "and production_orderlines.product_line_id = #{params[:product_line]}"
       lineqry = " and ((production_orders.id in (select production_order_id from production_orderlines where production_orderlines.production_order_id = production_orders.id and production_orderlines.product_line_id =  #{params[:product_line]})) OR " +
                 "      (production_orders.id in (select production_order_id from production_orderlines where production_orderlines.production_order_id = production_orders.id and production_orderlines.product_line_id in (select id from product_lines where parent_id = #{params[:product_line]}))))"
       conditions <<  lineqry
    end

    @production_orders = ProductionOrder.includes('ordertype').references("ordertypes").where(conditions).order(sort_column + ' ' + sort_direction).group("production_orders.id").paginate(:per_page => 50, :page => params[:page])

    params[:reset] = 'false'

    if request.xhr?
      render(:partial => "production_order", :collection => @production_orders)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @production_orders }
      end
    end
  end

  def get_ordertype
    @productset = Productset.find(params[:productset_id])
    @ordertypes = @productset.product_category.ordertypes.collect {|c| [c.name, c.id]}
    render :layout => false
  end

  def my_work
    myname = "#{current_user.first_name} #{current_user.last_name}" if current_user
    conditions = "production_orders.project_id = #{session[:project_id]}
                  AND production_orders.bp_name <> 'RELEASE'
                  AND (production_orders.owner = '#{myname}'
                  OR (select count(*) from production_orderlines where production_orderlines.production_order_id = production_orders.id and production_orderlines.owner ='#{myname}') > 0)"
    @production_orders = ProductionOrder.where(conditions).paginate(:per_page => 10, :page => params[:page])
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @production_orders }
    end
  end

  # GET /production_orders/1
  # GET /production_orders/1.xml
  def show
    @production_order = ProductionOrder.find(params[:id])
    @product_release_note = ProductReleaseNote.find_by_production_order_id(params[:id])

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @production_order }
    end
  end

  def dynamic_id_show
    # for usage with url for with js string as id
    @production_order = ProductionOrder.find(params[:id])
    redirect_to @production_order
  end

  def manage
    @production_order = ProductionOrder.find(params[:id])
    @product_release_note = ProductReleaseNote.find_by_production_order_id(params[:id])

    respond_to do |format|
      format.html
      format.xml  { render :xml => @production_order }
    end
  end

  # GET /production_orders/new
  # GET /production_orders/new.xml
  def new
    @production_order = ProductionOrder.new(:project_id => session[:project_id])
    if params[:production_order]
      params[:production_order][:project_id] = session[:project_id] unless params[:production_order][:project_id]
      @production_order = ProductionOrder.new(params[:production_order])
    end

    if params[:productset_id]
      @productset = Productset.find(params[:productset_id])
      @production_order.productset_id = params[:productset_id]
      @production_order.generate_name(@productset.setcode)
      @production_order.ordertype_id = @productset.product_category.ordertypes.order(:name).last.id
      @production_order.create_frontend_order_yn = true
      @production_order.allocate_by_update_region_yn = false
    end

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @production_order }
    end
  end

  def new_from_product
    if params[:production_order].blank?

      @production_order = ProductionOrder.new(:project_id => session[:project_id])

      if params[:product_id]
        @production_order.generate_name(params[:product_id])
        # @production_order.ordertype_id = @productset.product_category.ordertypes.last.id # 1st entry
        @production_order.create_frontend_order_yn = true
        @production_order.allocate_by_update_region_yn = false
      end
    else
      params[:production_order][:project_id] = session[:project_id] unless params[:production_order][:project_id]
      @production_order = ProductionOrder.new(params[:production_order])
    end

    if params[:product_id]
      product = Product.find(params[:product_id])
      if product.product_design
        @conversion_steps = product.product_design.modelhash["conversion_steps"]
      end
    end

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @production_order }
    end
  end

  def check
    @production_order = ProductionOrder.new(:project_id => session[:project_id])

    if params[:productset_id]
      @productset = Productset.find(params[:productset_id])
      @production_order.productset_id = params[:productset_id]
      @production_order.generate_name(@productset.setcode)
      @production_order.ordertype_id = @productset.product_category.ordertypes.last.id # 1st entry
    end
  end

  def check_from_product
    if params[:product_id]
      product = Product.find(params[:product_id])
      if product.product_design
        @conversion_steps = product.product_design.modelhash["conversion_steps"]
      end
    end
    @production_order = ProductionOrder.new(:project_id => session[:project_id])

    if params[:product_id]
      @production_order.generate_name(params[:product_id])
    end
  end

  # GET /production_orders/1/edit
  def edit
    @production_order = ProductionOrder.find(params[:id])
    @productset = @production_order.productset
  end

  def change_status
    @production_order = ProductionOrder.find(params[:id])
  end

  def assign
    @production_order = ProductionOrder.find(params[:id])
  end

  def confirm_assign
    @production_order = ProductionOrder.find(params[:id])
    @production_order.owner = params[:production_order][:owner]
    if @production_order.save
      redirect_to(:controller => 'production_orders', :action => 'index', :id => @production_order.id)
    else
      redirect_to(:controller => 'production_orders', :action => 'assign', :id => @production_order.id)
    end
  end

  def assign_product
    @production_orderline = ProductionOrderline.find(params[:id])
  end

  def confirm_assign_product
    @production_orderline = ProductionOrderline.find(params[:id])
    @production_orderline.owner = params[:production_orderline][:owner]
    if @production_orderline.save
      flash[:notice] = "Product was succesfully reassigned"
      redirect_to(:controller => 'production_orders', :action => 'manage', :id => @production_orderline.production_order.id)
    else
      redirect_to(:controller => 'production_orders', :action => 'assign_product', :id => @production_orderline.production_order.id)
    end
  end

  def complete_work
    @production_order = ProductionOrder.find(params[:id])
  end

  def cancel
    begin
      @production_order = ProductionOrder.find(params[:id])
      ProductionOrder.transaction do
        os = Hash.new
        os[:production_order_id] = params[:id]
        os[:level]  = "SET"
        os[:status] = @production_order.bp_name
        os[:user] = current_user.user_name if current_user
        @production_order.log_orderstatus(os)
        partly_complete = false
        @production_order.production_orderlines.each do |line|
          if line.bp_name == 'COMPLETE'
            partly_complete = true
          else
            line.cancel(current_user.user_name)
          end
        end
        # if part of the production_order has already been completed, you cannot cancel the whole production_order, only the parts which were not completed yet
        # the business process of the production_order should reflect this, hence the use of 'COMPLETE' if at least one line was 'COMPLETE'
        @production_order.bp_name = partly_complete ? ProductionOrder::BP_NAME_COMPLETE : ProductionOrder::BP_NAME_CANCELLED
        @production_order.save!
        Postoffice.po_confirm_work(@production_order.id,@production_order.bp_name).deliver
      end
      flash[:notice] = "Production order was successfully cancelled"
    rescue ActiveRecord::StaleObjectError => e
      puts e.message
      puts e.backtrace
      flash[:error] = "Order changed while you were cancelling it. Some jobs might not have been cancelled properly."
    end

    redirect_to :action  => 'index'
  end

  def terminate
    begin
      @production_order = ProductionOrder.find(params[:id])
      @production_order.terminate(current_user)
      flash[:notice] = "Production order was successfully terminated"
    rescue ActiveRecord::StaleObjectError => e
      puts e.message
      puts e.backtrace
      flash[:error] = "Order changed while you were terminating it. Some jobs might not have been killed properly."
    end
    redirect_to :action => :manage, :id => @production_order.id
  end

  def complete_work_product
    @production_orderline = ProductionOrderline.find(params[:poid])
    @production_order = @production_orderline.production_order
  end

  def confirm_work_product
    ProductionOrderline.transaction do
      @production_orderline = ProductionOrderline.find(params[:poid])
      @transition = ProcessTransition.find(params[:production_orderline][:bp_name])
      params[:production_orderline][:bp_name] = @transition.from_process.name

      respond_to do |format|
        @production_orderline.bp_name = @transition.from_process.name
        @production_orderline.bp_seqnr = @transition.sequence

        if @production_orderline.save
          if @transition.cascade_to_bp
            #cascade up if all lines have achieved to_bp status
            cascade = true
            if @production_orderline.production_order.production_orderlines.each do |line|
                # get bp_seqnr
                cascade = false if (line.bp_seqnr.to_i < @transition.sequence && line.bp_name != 'CANCELLED')
              end
              if cascade
                @production_orderline.production_order.bp_name = @transition.cascade_to_process.name
                @production_orderline.production_order.save!
                Postoffice.po_confirm_work(@production_orderline.production_order.id,@production_orderline.production_order.bp_name).deliver
                os = Hash.new
                os[:production_order_id] = params[:id]
                os[:level]    = "SET"
                os[:status]   = @transition.from_process.name
                os[:user]     = current_user.user_name if current_user
                os[:sequence] = @transition.sequence
                os[:remarks]  = params[:remarks] if params[:remarks]
                @production_orderline.production_order.log_orderstatus(os)
              end
            end
          end
          os = Hash.new
          os[:production_order_id] = @production_orderline.production_order.id
          os[:level] = @transition.from_level
          os[:user] = current_user.user_name
          os[:sequence] = @transition.sequence
          os[:status] = @transition.from_process.name
          os[:remarks] = params[:remarks] if params[:remarks]
          os[:reference] = @production_orderline.product_id
          @production_orderline.production_order.log_orderstatus(os)

          flash[:notice] = 'Production Order was successfully updated.'
          format.html { redirect_to(:controller => 'production_orders', :action => 'manage', :id => @production_orderline.production_order.id) }
          format.xml  { head :ok }
        else
          format.html { render :action => "complete_work_product" }
          format.xml  { render :xml => @production_order.errors, :status => :unprocessable_entity }
        end
      end
    end

  end

  def confirm_work
    ProductionOrder.transaction do
      @production_order = ProductionOrder.find(params[:id])
      @transition = ProcessTransition.find(params[:production_order][:bp_name])
      params[:production_order][:bp_seqnr] = @transition.sequence
      params[:production_order][:bp_name] = @transition.to_process.name


      respond_to do |format|
        if @production_order.update_attributes(strong_params)
          if @transition.cascade_to_bp
            @production_order.production_orderlines.each do |line|
              bp_name = @transition.cascade_to_process.name
              bp_seqnr = ProcessTransition.where("sequence > #{@transition.sequence} and from_bp = #{@transition.to_bp}").first
              line.set_bp_name(bp_name, bp_seqnr)
              line.save!
            end
          end
          os = Hash.new
          os[:production_order_id] = @production_order.id
          os[:level]    = @transition.from_level
          os[:user]     = current_user.user_name
          os[:sequence] = @transition.sequence
          os[:status]   = @transition.from_process.name
          os[:remarks]  = params[:remarks]
          @production_order.log_orderstatus(os)
          Postoffice.po_confirm_work(@production_order.id,@production_order.bp_name).deliver
          flash[:notice] = 'ProductionOrder was successfully updated.'

          format.html { redirect_to(:controller => 'production_orders', :action => 'manage', :id => @production_order.id) }
          format.xml  { head :ok }
        else
          format.html { render :action => "edit" }
          format.xml  { render :xml => @production_order.errors, :status => :unprocessable_entity }
        end
      end
    end
  end

  # POST /production_orders
  # POST /production_orders.xml
  def create
    begin
      create_outbound_order = params[:create_outbound_order]
      create_po(create_outbound_order, Checklog.new(only_check = false, Checklog::LOGLEVEL_INFO), from_product_yn = false)
      if @production_order
        Postoffice.po_confirm_work(@production_order.id,@production_order.bp_name).deliver if @production_order.bp_name == 'PLANNING'
        respond_to do |format|
          flash[:notice] = 'Production Order was successfully created.'
          format.html { redirect_to(@production_order) }
          format.xml  { render :xml => @production_order, :status => :created, :location => @production_order }
        end
      else
        raise "Production order creation failed."
      end
    rescue => e
      puts e.message
      puts e.backtrace unless e.class == ArgumentError
      @production_order = ProductionOrder.new(:project_id => session[:project_id])
      respond_to do |format|
        flash[:error] = "Cannot create production order : " + e.message
        format.html { render :action => "new" }
        format.xml  { render :xml => @production_order.errors, :status => :unprocessable_entity }
      end
    end
  end

  # POST /production_orders.xml
  def create_from_product
    begin
      product = Product.find(params[:product_id])
      product_design = product.product_design
      product_design = ProductDesign.find(params[:product_design_id]) if !params[:product_design_id].blank? # overrule of design by form
      if product_design
        @conversion_steps = product_design.modelhash["conversion_steps"]
      end
      create_outbound_order = params[:create_outbound_order]
      create_po(create_outbound_order, Checklog.new(only_check = false, Checklog::LOGLEVEL_INFO), from_product_yn = true)
      if @production_order
        Postoffice.po_confirm_work(@production_order.id,@production_order.bp_name).deliver if @production_order.bp_name == 'PLANNING'
      if request.xhr?
        render :text => @production_order.id
      else
        respond_to do |format|
          flash[:notice] = 'Production Order was successfully created.'
          format.html { redirect_to(@production_order) }
          format.xml  { render :xml => @production_order, :status => :created, :location => @production_order }
          format.js { render @production_order.id.to_s }
        end
      end
      else
        raise "Production order creation failed."
      end
    rescue => e
      puts e.message
      puts e.backtrace unless e.class == ArgumentError
      @production_order = ProductionOrder.new(strong_params)
      @production_order.project_id = session[:project_id]
      if request.xhr?
        render :text => "Cannot create production order : " + e.message
      else
      respond_to do |format|
        flash[:error] = "Cannot create production order : " + e.backtrace
        format.html { render :action => "new_from_product" }
        format.xml  { render :xml => @production_order.errors, :status => :unprocessable_entity }
      end
      end
    end
  end

  def checked
    @heading = "Checking production order creation"
    check_po(from_product_yn = false)
  end

  def checked_from_product
    product = Product.find(params[:product_id])
    product_design = product.product_design
    product_design = ProductDesign.find(params[:product_design_id]) if !params[:product_design_id].blank? # overrule of design by form
    if product_design
      @conversion_steps = product_design.modelhash["conversion_steps"]
    end
    @heading = "Checking production order creation"
    check_po(from_product_yn = true)
  end

  # PUT /production_orders/1
  # PUT /production_orders/1.xml
  def update
    @production_order = ProductionOrder.find(params[:id])
    unless params[:production_order][:baseline_commitment_date].blank?
      @production_order.commitment_dates << CommitmentDate.new(:new_date => params[:production_order][:baseline_commitment_date],
                                                               :commitment_type => ProductionOrder::COMMITMENT_BASELINE,
                                                               :commitment_change_reason_id => CommitmentChangeReason.initial(ProductionOrder::COMMITMENT_BASELINE).id,
                                                               :updated_by => current_user.user_name)
    end
    unless params[:production_order][:modified_commitment_date].blank?
      @production_order.commitment_dates << CommitmentDate.new(:new_date => params[:production_order][:modified_commitment_date],
                                                               :commitment_type => ProductionOrder::COMMITMENT_MODIFIED,
                                                               :commitment_change_reason_id => CommitmentChangeReason.initial(ProductionOrder::COMMITMENT_MODIFIED).id,
                                                               :updated_by => current_user.user_name)
    end

    if params[:production_order][:allow_shipment] == "1"
      @production_order.allow_planning = 1
      @production_order.allow_production = 1
      params[:production_order][:allow_planning] = "1"
      params[:production_order][:allow_production] = "1"
    end

    respond_to do |format|
      if @production_order.update_attributes(strong_params)
        @production_order.save
        flash[:notice] = 'ProductionOrder was successfully updated.'
        format.html { redirect_to(@production_order) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @production_order.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /production_orders/1
  # DELETE /production_orders/1.xml
  def destroy
    @production_order = ProductionOrder.find(params[:id])
    @production_order.destroy

    respond_to do |format|
      format.html { redirect_to(production_orders_url) }
      format.xml  { head :ok }
    end
  end

  def cancel_product
    begin
      @production_orderline = ProductionOrderline.find(params[:lid])
      @production_orderline.cancel(current_user.user_name)

      flash[:notice] = 'Product was succesfully cancelled'
    rescue ActiveRecord::StaleObjectError => e
      puts e.message
      puts e.backtrace
      flash[:error] = "Orderline change will you were cancelling it. Cancel NOT succeeded"
    end
    redirect_to :action => :manage, :id => @production_orderline.production_order.id
  end

  def terminate_product
    begin
      @production_orderline = ProductionOrderline.find(params[:lid])
      @production_orderline.terminate(current_user.user_name)

      flash[:notice] = 'Product was succesfully terminated'
    rescue ActiveRecord::StaleObjectError => e
      puts e.message
      puts e.backtrace
      flash[:error] = "Orderline change will you were terminating it. Terminate NOT succeeded"
    end
    redirect_to :action => :manage, :id => @production_orderline.production_order.id
  end

  def reset_product
    begin
      @production_orderline = ProductionOrderline.find(params[:lid])
      @production_orderline.reset(current_user,params[:conversiontool_id],params[:cvtool_bundle_id])
      flash[:notice] = 'Product was succesfully reset.'
    rescue ActiveRecord::StaleObjectError => e
      puts e.message
      puts e.backtrace
      flash[:error] = "Orderline change will you were resetting it. Reset NOT succeeded."
    end

    redirect_to :action => :manage, :id => @production_orderline.production_order.id
  end

  def reset_order
    begin
      @production_order = ProductionOrder.find(params[:id])
      @production_order.reset(current_user,params[:conversiontool_id],params[:cvtool_bundle_id])
      flash[:notice] = 'Order was succesfully reset.'
    rescue ActiveRecord::StaleObjectError => e
      puts e.message
      puts e.backtrace
      flash[:error] = "Order change will you were resetting it. Reset NOT succeeded."
    end

    redirect_to :action => :manage, :id => @production_order.id
  end

  def restart_failed_and_crashed_conversion_jobs
    @production_orderline = ProductionOrderline.find(params[:lid])
    @production_order = @production_orderline.production_order
    failed_jobs = @production_orderline.conversion_jobs.failures_on_server
    if !failed_jobs.empty?
      begin
      ConversionJob.transaction do
        failed_jobs.each do |job|
          job.clear_working_dir
          new_conversion_job = job.regenerate(current_user, params[:conversiontool_id], params[:cvtool_bundle_id])
          new_conversion_job.queue
        end
      end
      rescue => e
      @mesg = ["error",e.message]
      end
      @mesg = ["info","Conversion jobs are successfully restarted."]
    else
      @mesg = ["error","No conversion jobs for restart available."]
    end
  end

  def activate_product
    @production_order = ProductionOrder.find(params[:id])
    ProductionOrderline.transaction do
      @production_orderline = ProductionOrderline.find(params[:lid])
      @production_orderline.bp_name = @production_order.ordertype.process_transitions.order(:sequence).where(:cascade_to_level => 'PRODUCT').first.cascade_to_process.name
      @production_orderline.save
    end
    flash[:notice] = 'Product was succesfully activated'
    flash.now
    redirect_to :action => :manage, :id => @production_orderline.production_order.id
  end

  def feedback
    ProductionOrderline.transaction do
      @production_orderline = ProductionOrderline.find(params[:line_id])
      @production_orderline.bp_name = 'COMPLETE'
      @production_orderline.customer_feedback = params[:customer_feedback]
      @production_orderline.product_accepted =  params[:product_accepted]

      if @production_orderline.save
        os = Hash.new
        os[:production_order_id] = @production_orderline.production_order.id
        os[:completion_date] = Time.now
        os[:level] = 'PRODUCT'
        os[:user] = current_user.user_name if current_user
        os[:sequence] = 0
        os[:status] = "Product acceptance and feedback"
        os[:reference] = @production_orderline.product_id.to_s
        @production_orderline.production_order.log_orderstatus(os)

      end
    end
    flash[:notice] = "Product is succesfully #{@production_orderline.product_accepted}"
    redirect_to :action => :manage, :id => @production_orderline.production_order.id
  end

  def delete_product
    @production_order = ProductionOrder.find(params[:production_order_id])
    if @production_order.bp_name == "ORDERENTRY"

      ProductionOrderline.transaction do
        @production_orderline = ProductionOrderline.find(params[:id])
        @production_orderline.terminate(current_user.user_name)
        os = Hash.new
        os[:user] = current_user.user_name if current_user
        os[:status] = "DELETED"
        os[:reference] = @production_orderline.product_id.to_s
        @production_order.log_orderstatus(os)
      end
      flash[:notice] = 'Product was succesfully deleted'
      redirect_to :action => :manage, :id => @production_orderline.production_order.id
    else
      flash[:notice] = 'Product cannot be deleted in this state.'
      redirect_to :action => :manage, :id => @production_orderline.production_order.id
    end
  end

  def add_product
    @production_order = ProductionOrder.find(params[:po_id])
    ProductionOrderline.transaction do

      product = Product.find(params[:product_id])
      re_release = ProductionOrderline.where("product_id LIKE '#{product.volumeid}%'").last

      if re_release && re_release.product_id.to_s != ""
        if !re_release.product_id.to_s.scan(/\.\d{1,2}/).empty?
          seq = re_release.product_id.to_s.scan(/\d{1,2}$/).to_s.to_i + 1

          product.volumeid = product.volumeid + '.' + seq.to_s
        else
          product.volumeid = re_release.product_id + '.1'
        end
      end

      line = ProductionOrderline.new
      line.production_order_id = params[:po_id]
      line.product_id = product.volumeid
      line.quantity = 1
      line.remarks = product.remarks
      line.save!
      line.generate_customer_contents

      @production_order.set_bp_name('PRODUCTION', 1)
      @production_order.save!

      os = Hash.new
      os[:level] = 'SET'
      os[:user] = current_user.user_name
      os[:status] = @production_order.bp_name
      os[:reference] = line.product_id.to_s
      os[:remarks] = "RE-entry of product"
      @production_order.log_orderstatus(os)

    end

    flash[:notice] = 'Product was succesfully added'
    redirect_to :action => :manage, :id => params[:po_id]
  end

  def select_product
    @production_order = ProductionOrder.find(params[:id])
    @products = []
    @product = @production_order.productset.products
    @product.each do |p|
      match = ProductionOrderline.where(["production_order_id = ? AND product_id LIKE ?", params[:id],"#{p.volumeid}%"]).last
      if !match
        @products << p
        #      else
        #        if !match.product_id.to_s.scan(/\.(\d{1,2})/).empty?
        #          re_release_nr = match.product_id.to_s.scan(/\.(\d{1,2})/).to_s.to_i + 1
        #
        #        else
        #          re_release_nr = 1
        #
        #        end
        #        p.volumeid = p.volumeid.to_s + '.' + re_release_nr.to_s
        #        @products << p
      end
    end

  end

  def get_productsets_by_type

    if (params[:ordertype_id] && !params[:ordertype_id].blank?)
      @ordertype = Ordertype.find(params[:ordertype_id])
      if @ordertype.no_pds_yn
        @sps = ['Order without productset','']
      else
        pcat = @ordertype.product_category_id
        @sps = [""] + Productset.where(:product_category_id => pcat).order(:setcode).collect {|c| [ c.id.to_s  + " " + c.setcode,c.id ] }
      end
    else
      @sps = []
    end
    render :partial => "productset_select"

  end

  def release_notes_send
    @production_order = ProductionOrder.find(params[:id])
    @production_order.rel_notes_send_by_id = current_user.id if current_user
    @production_order.rel_notes_send_date = Time.now

    respond_to do |format|
      if @production_order.save
        flash[:notice] = 'Release notes of productset registered as sent.'
        format.html { redirect_to :action => :manage, :id => params[:id] }
        format.xml  { head :ok }
      else
        format.html { render :action => "manage" }
        format.xml  { render :xml => @production_order.errors, :status => :unprocessable_entity }
      end
    end
  end

  def trace
    @production_order = ProductionOrder.find(params[:id])
  end

  def remove_tag
    @production_order = ProductionOrder.find(params[:id])
    @production_order.tag_list =  @production_order.tags.collect {|t|t.name}.join(',').to_s.gsub("#{params[:tag]}",'')
    @production_order.save
    @production_order = ProductionOrder.find(params[:id])
    #render( :partial => 'show_tags' )
    render :layout => false
  end

  def add_tag
    @production_order = ProductionOrder.find(params[:id])
    if @production_order.tag_list == nil
      @production_order.tag_list = params[:new_tag].to_s
    else
      @production_order.tag_list =  @production_order.tags.collect {|t|t.name}.join(',') + ",#{params[:new_tag]}"
    end
    @production_order.save
    @production_order = ProductionOrder.find(params[:id])
    #render( :partial => 'show_tags' )
    render :layout => false
  end

  def show_conversion_jobs
    @conversion = Conversion.find(params[:conversion_id])
  end

  def get_conversion_steps_from_design
    @conversion_steps = []
    if !params[:product_design_id].blank?
      product_design = ProductDesign.find(params[:product_design_id])
      if product_design
        @conversion_steps = product_design.modelhash["conversion_steps"]
      end
    end
    render :partial => 'steps_list_force_create', :layout => false
  end

  def render_product_design_select
    @selected_product_design = ''
    @product_designs_for_select = []

    product = Product.find(params[:product_id]) if !params[:product_id].blank?
    if product
      orginal_product_design = product.product_design
      @product_designs_for_select = ProductDesign.backend.where(:project_id => session[:project_id])
      @product_designs_for_select << orginal_product_design if product && product.product_design

      @selected_product_design = orginal_product_design.id.to_s if product && product.product_design
    end

    render :partial => 'product_design_select', :layout => false
  end

  def refresh_lines
    @production_order = ProductionOrder.find(params[:id])
    render :partial => "order_lines", :object => @production_order
  end

  def refresh_lines_from_manage
    refresh_lines
  end

  def refresh_lines_from_show
    refresh_lines
  end

  def force_complete
    begin
      @production_order = ProductionOrder.find(params[:id])
      begin
        @production_order.force_complete(current_user.full_name)
      rescue => e
        flash[:error] = e.message
      end
      flash[:notice] = 'Order was succesfully forced to COMPLETE.'
    end

    redirect_to :action => :manage, :id => @production_order.id
  end

  def edit_baseline_commitment
    @production_order = ProductionOrder.find(params[:id])
    @production_order.baseline_commitment_date = @production_order.baseline_commitment_date.strftime('%d-%m-%Y') unless @production_order.baseline_commitment_date.blank?
  end

  def edit_modified_commitment
    @production_order = ProductionOrder.find(params[:id])
    @production_order.modified_commitment_date = @production_order.modified_commitment_date.strftime('%d-%m-%Y') unless @production_order.modified_commitment_date.blank?
  end

  def edit_expiry_date
    @production_order = ProductionOrder.find(params[:id])
    if @production_order.expiry_date.blank?
      @production_order.expiry_date = (Date.current + 7.days).strftime("%d-%m-%Y")
    else
      @production_order.expiry_date = @production_order.expiry_date.strftime("%d-%m-%Y")
    end
  end

  def update_baseline_commitment
    @production_order = ProductionOrder.find(params[:id])
    commitment_change_reason = CommitmentChangeReason.find(params[:commitment_reason_id]) unless params[:commitment_reason_id].blank?
    @production_order.baseline_commitment_date = params[:production_order][:baseline_commitment_date]


    respond_to do |format|
      if @production_order.baseline_commitment_date_changed? && commitment_change_reason && @production_order.update_baseline_commitment_date(@production_order.baseline_commitment_date, commitment_change_reason, current_user)
        flash[:notice] = 'ProductionOrder baseline commitment was successfully updated.'
        format.html { redirect_to @production_order }
      else
        if @production_order.baseline_commitment_date_changed? && commitment_change_reason
          flash[:error] = 'The baseline commitment could not be updated.'
        elsif @production_order.baseline_commitment_date_changed?
          flash[:error] = 'The reason was not specified'
        else
          flash[:error] = 'The submitted date was not changed'
        end
        @production_order.baseline_commitment_date = @production_order.baseline_commitment_date.strftime('%d-%m-%Y')
        format.html { render :action => 'edit_baseline_commitment' }
      end
    end
  end

  def update_modified_commitment
    @production_order = ProductionOrder.find(params[:id])
    commitment_change_reason = CommitmentChangeReason.find(params[:commitment_reason_id]) unless params[:commitment_reason_id].blank?
    @production_order.modified_commitment_date = params[:production_order][:modified_commitment_date]

    respond_to do |format|
      if @production_order.modified_commitment_date_changed? && commitment_change_reason && @production_order.update_modified_commitment_date(@production_order.modified_commitment_date, commitment_change_reason, current_user)
        flash[:notice] = 'ProductionOrder modified commitment was successfully updated.'
        format.html { redirect_to @production_order }
      else
        if @production_order.modified_commitment_date_changed? && commitment_change_reason
          flash[:error] = 'The modified commitment could not be updated.'
        elsif @production_order.modified_commitment_date_changed?
          flash[:error] = 'The reason was not specified'
        else
          flash[:error] = 'The submitted date was not changed'
        end
        @production_order.modified_commitment_date = @production_order.modified_commitment_date.strftime('%d-%m-%Y')
        format.html { render :action => 'edit_modified_commitment' }
      end
    end
  end

  def update_expiry_date
    @production_order = ProductionOrder.find(params[:id])
    @production_order.expiry_date = DateTime.strptime(params[:production_order][:expiry_date],"%d-%m-%Y") if params[:production_order][:expiry_date]

    respond_to do |format|
      if @production_order.expiry_date_changed? && @production_order.update_expiry_date(@production_order.expiry_date,current_user)
        flash[:notice] = 'ProductionOrder expiry date was successfully updated.'
        format.html { redirect_to(@production_order) }
        format.xml  { head :ok }
      else
        if !@production_order.expiry_date_changed?
          flash[:error] = "The submitted date was not changed"
        end
        @production_order.expiry_date = @production_order.expiry_date.strftime("%d-%m-%Y")
        format.html { render :action => "edit_expiry_date" }
        format.xml  { render :xml => @production_order.errors, :status => :unprocessable_entity }
      end
    end
  end
#merged from backoffice project

  def parameters_for_product
    begin
      parameters = {}
      orderline = get_orderline
      orderline.conversions.each do |conversion|
        conversion.parameters.each do |param|
          exes = Executable.find_by_parameter(param[0])
          exes.each do |exe|
            name = exe.name
            name = exe.subprocs if exe.subprocs && !exe.subprocs.blank?
            parameters[name] = [] if !parameters[name]
            parameters[name] << {:parameter_name => param[0], :parameter_value => param[1], :visibility => true}
            parameters[name].uniq!
          end
        end
      end
      resulthash = []
      parameters.each do |k,v|
        hash = {}
        hash["tool"] = k
        hash["parameters"] = v
        resulthash << hash
      end
      
      render :text => resulthash.to_json
    rescue => e
      puts e.message
      puts e.backtrace
      message = e.message
      errorlog("API ERROR: " + message)
      return render :text => message, :status => :unprocessable_entity
    end
  end

  def tool_bundles
    begin
      bundles = []
      orderline = get_orderline
      orderline.conversions.each do |conversion|
        bundles << {:release => conversion.conversiontool.code, :bundle => conversion.cvtool_bundle.name}
      end
      render :text => bundles.uniq.to_json
    rescue => e
      puts e.message
      puts e.backtrace
      message = e.message
      errorlog("API ERROR: " + message)
      return render :text => message, :status => :unprocessable_entity
    end

  end

  def features_for_product
    begin
      orderline = get_orderline
   
      features = JSON.parse(orderline.product_design)["features"]
      results = []
      if features
        features.each do |feature|
          featurehash = {}
          featurehash["feature"] = feature["name"]
          featurehash["parameters"] = []
          trans = ProductFeature.find_by_name(feature["name"]).system_translation
          if !trans or trans.to_s.size < 1
            next
          end
          translation = JSON.parse(trans) if trans
          if translation && translation["parts"]
          
            translation["parts"].each do |part|
              params_pf = {}
              if part["type"] == "PARAMETER"
                params_pf["parameter_name"] = part["parameter_name"]
                params_pf["parameter_value"] = part["parameter_value"]
                params_pf["visibility"] = true
                featurehash["parameters"] << params_pf
              end
            end
          end
          if featurehash["parameters"].size > 0
            results << featurehash
          end
        end
      end

      render :text => results.to_json
    rescue => e
      puts e.message
      puts e.backtrace
      message = e.message
      errorlog("API ERROR: " + message)
      return render :text => message, :status => :unprocessable_entity
    end
  end

  def poi_mapping_for_product

    begin
      orderline = get_orderline
      mappings = []
      orderline.product.poi_mapping_layout.poi_mappings.each{|x| mappings << x.attributes}
    
      mappings.each do |mapping|
        mapping.delete("created_at")
        mapping.delete("updated_at")
        mapping.delete("import_hash")
        mapping.delete("poi_mapping_layout_id")
      end

      render :text => mappings.to_json
    rescue => e
      puts e.message
      puts e.backtrace
      message = e.message
      errorlog("API ERROR: " + message)
      return render :text => message, :status => :unprocessable_entity
    end
  end

  def  improvement_processes_for_product
    begin
      orderline = get_orderline
      fulldesign = JSON.parse(orderline.product_design)
      results = []
      orderline.conversions.each do |conv|
        stepdesign = fulldesign["conversion_steps"][conv.design_sequence]
        if stepdesign["pre_improvement_steps"]
          stepdesign["pre_improvement_steps"].each do |exehash|
            exe = Executable.find_by_name(exehash["name"])
            visibility = true
            if exe && exe.is_improvement_process
              visibility = exe.customer_visibility
              results << {:name => exehash["name"],:visibility => visibility}
            end
          end
        end
        if stepdesign["post_improvement_steps"]
          stepdesign["post_improvement_steps"].each do |exehash|
            exe = Executable.find_by_name(exehash["name"])
            visibility = true
            if exe && exe.is_improvement_process
              visibility = exe.customer_visibility
              results << {:name => exehash["name"],:visibility => visibility}
            end
          end
        end
      end

      render :text => results.uniq.reverse.to_json
    rescue => e
      puts e.message
      puts e.backtrace
      message = e.message
      errorlog("API ERROR: " + message)
      return render :text => message, :status => :unprocessable_entity
    end

  end

  def output_location_for_product
    begin
      results = {}
      orderline = get_orderline
      conversions = orderline.conversions

      if conversions.size == 0
        raise "Related Conversion(s) not completed (yet)"
      end
      
      results[:locations] = []
      conversions.each do |conversion|
        results[:conversion_id] = conversion.id
        conversion.conversion_databases.each do |db|
          results[:locations]  << {:path_and_file => db.path_and_file, :db_type => db.db_type} if db.path_and_file
        end
      end
      render :text => results.to_json
  	rescue => e
      puts e.message
      puts e.backtrace
      message = e.message
      errorlog("API ERROR: " + message)
      return render :text => message, :status => :unprocessable_entity
    end
  end

  def get_orderline
   po = ProductionOrder.find(params[:id])
   ol = ProductionOrderline.where({:product_id => params["product_id"], :production_order_id => params[:id]}).first
   if !ol
        raise "Can't find production orderlines with product \"#{params["product_id"]}\"."
      end
    return ol
  end

  private
  def create_po(create_outbound_order, checklog, from_product_yn = false)
    force_create = params.keys.reject{|x|!x.match("force_create")}.collect{|x|x.scan(/force_create_(.*)/)}.flatten.collect{|x|x.to_i}

    # if allow_shipment is set, the allow_planning and allow_production checkboxes are disabled in the form, resulting in their value not being set
    if params[:production_order][:allow_shipment] == "1"
      params[:production_order][:allow_planning] = "1"
      params[:production_order][:allow_production] = "1"
    end


    if params[:production_order][:ordertype_id].to_s.blank?
      checklog.add_attributes({"Order type" => ""}, "", Checklog::LOGLEVEL_INFO)
      checklog.add_error_line("Production order not specified.")
      raise ArgumentError, "Ordertype is mandatory."
    else
      checklog.add_attributes({"Order type" => Ordertype.find(params[:production_order][:ordertype_id]).name}, "", Checklog::LOGLEVEL_INFO)
    end

    unless from_product_yn
      # check on product set
      if params[:production_order][:productset_id].to_s.blank?
        checklog.add_attributes({"Productset" => ""}, "", Checklog::LOGLEVEL_INFO)
        checklog.add_error_line("Productset not specified.")
        raise ArgumentError, "Productset is mandatory."
      else
        checklog.add_attributes({"Productset" => Productset.find(params[:production_order][:productset_id]).name}, "", Checklog::LOGLEVEL_INFO)
      end
    end

    # bundle information is not mandatory for manual entries, so the check on whether the bundle is specified is done in the
    # automatic part. But if a bundle is specified, it should point to an existing order for all orders, so that check is here.
    unless params[:conversiontool_id].to_s.blank? || params[:cvtool_bundle_id].to_s.blank?
      conversiontool = Conversiontool.find(params[:conversiontool_id])
      cvtool_bundle = CvtoolBundle.find(params[:cvtool_bundle_id])

      unless conversiontool && cvtool_bundle
        checklog.add_error_line("Specified conversion tool not found in WebMIS.") unless conversiontool
        checklog.add_error_line("Specified tool bundle not found in WebMIS") unless cvtool_bundle
        raise ArgumentError, "Specified bundle data not found in WebMIS."
      end
      bundledir = File.join(ConfigParameter.get('cvtool_release_directory',session[:project_id]), 'Bundles', conversiontool.code, cvtool_bundle.name)
      unless File.directory?(bundledir)
        checklog.add_error_line("Bundle #{conversiontool.code}/#{cvtool_bundle.name} not found on the filesystem.")
        raise ArgumentError, "Specified bundle not found on the filesystem."
      end
    end

    if from_product_yn
      product = Product.find(params[:product_id])
    else
      productset = Productset.find(params[:production_order][:productset_id]) unless params[:production_order][:productset_id].to_s.blank?
    end

    #params[:production_order][:allow_production] = "1" if params[:production_order][:allow_shipment] == "1"
    #params[:production_order][:allow_planning] = "1" if params[:production_order][:allow_production] == "1"

    @production_order = ProductionOrder.new(strong_params.update(
        :requestor => current_user.full_name,
        :major => !from_product_yn && productset ? productset.major : nil,
        :minor => !from_product_yn && productset ? productset.minor : nil,
        :supplier_name => productset ? productset.supplier_name : nil,
        :bp_seqnr => 0,:project_id => session[:project_id], :test_strategy => params[:test_strategy]
      ))
    if @production_order.baseline_commitment_date
      baseline_commitment_date = CommitmentDate.new(:new_date => @production_order.baseline_commitment_date,
                                                    :commitment_type => ProductionOrder::COMMITMENT_BASELINE,
                                                    :commitment_change_reason_id => CommitmentChangeReason.initial(ProductionOrder::COMMITMENT_BASELINE).id,
                                                    :updated_by => current_user.user_name)
      @production_order.commitment_dates << baseline_commitment_date
    end
    if @production_order.modified_commitment_date
      modified_commitment_date = CommitmentDate.new(:new_date => @production_order.modified_commitment_date,
                                                    :commitment_type => ProductionOrder::COMMITMENT_MODIFIED,
                                                    :commitment_change_reason_id => CommitmentChangeReason.initial(ProductionOrder::COMMITMENT_MODIFIED).id,
                                                    :updated_by => current_user.user_name)
      @production_order.commitment_dates << modified_commitment_date
    end
    ProductionOrder.transaction do
      @production_order.save!
      @production_order.checklog = checklog.new_sub("Production order #{@production_order.id}" + (@production_order.name.blank? ? "" : " (#{@production_order.name})"), Checklog::LOGLEVEL_INFO)
      @production_order.checklog.add_attributes(
        {"Requestor" => @production_order.requestor,
          "Version" => !@production_order.major.to_s.blank? ? "#{@production_order.major}.#{@production_order.minor}" : "",
          "Supplier" => @production_order.supplier_name,
          "Use test versions" => @production_order.use_test_versions_yn,
          "Use dakota simulator" => @production_order.use_dakota_simulator_yn,
          "Create front-end order" => @production_order.create_frontend_order_yn
        })
      @production_order.checklog.add_attributes(
        {"Business process sequence" => @production_order.bp_seqnr
        }, "at creation")
      @production_order.checklog.add_debug_line("Production order successfully saved.")

      @front_orders = []

      if from_product_yn
        product_list = [product]
      else
        @production_order.checklog.add_debug_line("Creating from productset, iterating through its products...")
        product_list = productset.nil? ? [] : productset.products.order("parent_id")
      end

      product_list.each do |product|

        if product.product_design && @production_order.ordertype.job_support_yn
          @production_order.checklog.add_debug_line("Product #{product.id} ('#{product.name}') found. Creating production orderlines.")
          pd = ProductDesign.find_correct_version(product.product_design_id, @production_order.use_test_versions_yn, @production_order.checklog)
          if pd
            if front_order = pd.create_production_order(product.volumeid,
                @production_order,
                params[:conversiontool_id],
                params[:cvtool_bundle_id],
                true,
                force_create,
                @production_order.use_test_versions_yn,
                nil,
                @production_order.use_dakota_simulator_yn)[0]

              @production_order.checklog.add_sub("Front-end production order #{front_order.id}", front_order.checklog)
              @front_orders << front_order
            else
              @production_order.checklog.add_debug_line("pd.create_prdocution_order failed for product #{product.volumeid.to_s}.")
            end
          else
            @production_order.checklog.add_debug_line("ProductDesign.find_correct_version returned nil for product design id #{product.design.id} - #{product.design.name.to_s}.")
          end
        else
          checklog.add_debug_line("Product #{product.id} ('#{product.name}') found. It does not have a product design. Creating production orderline.")
          parent_line = product.parent ? @production_order.production_orderlines.where("product_id = '#{product.parent.volumeid}'").first : nil
          line = ProductionOrderline.new(
            :product_id => product.volumeid,
            :created_from => product.created_from,
            :parent_linenr => (parent_line ? parent_line.id : nil),
            :quantity => 1,
            :bp_seqnr => 0,
            :remarks => product.remarks,
            :production_order_id => @production_order.id)
          line.save!
          line.checklog = @production_order.checklog.new_sub("Production orderline #{line.id}", Checklog::LOGLEVEL_INFO)
          line.generate_customer_contents
        end
      end
      @production_order.save!
      @production_order.checklog.add_debug_line("Production order successfully saved.")

      if create_outbound_order
        @production_order.production_orderlines.each{|ol| ol.create_outbound_order}
      end

      @production_order.transition(nil, 'forward')
    end
  end

  def check_po(from_product_yn)
    @checklog = Checklog.new(only_check = true, Checklog::LOGLEVEL_WARNING)
    @checklog_level = Checklog::LOGLEVEL_WARNING

    ProductionOrder.transaction do
      begin
        # Production order checker can't check shipment creations as the extra form falls outside its transaction, so ignore the create_outbound_order flag
        create_po(create_outbound_order = false, @checklog, from_product_yn)
        @production_order.execute if @production_order

        @front_orders.each do |front_order|
          front_order.execute
        end
      rescue => e
        @checklog.add_error_line("Check production order encountered:\r\n " + e.message)
        @checklog.add_backtrace_line(e.backtrace.join("\r\n")) unless e.class == ArgumentError
        @checklog.update_result(Checklog::STATUS_EXCEPTION, Checklog::LOGLEVEL_ERROR)
      end

      raise ActiveRecord::Rollback, "Just performing a check."
    end
  end

  def sort_column
    params.delete :sort if params[:sort].blank?
    params[:sort] || "production_orders.created_at"
  end

  def sort_direction
    params.delete :direction if params[:direction].blank?
    params[:direction] || "desc"
  end

  def search
    params[:search] || ""
  end



end
