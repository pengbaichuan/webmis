class ProductsController < ApplicationController
  before_filter :authorize, :except => [:get_product_details]
  authorize_resource :except => [:get_product_details]
  layout "webmis"

  helper_method :sort_column, :sort_direction, :search

  def product_lnks(id)
    @st = []
    product_links = Includedproduct.where(product_id:id)
    product_links.each do |i|
      ps = Productset.find(i.productset_id)
      @st << ps.setcode
    end
 
    return @st
      
  end

  def show
    @record  = Product.find(params[:id])
  end

  def show_nds
    ps = Product.find(params[:id])
    send_file("/data/newgroups/Product_Management/Product_Definitions/Individual_Sheets/NDS_#{ps.nds_name.gsub(" ","_")}.xls", :filename => "NDS_#{ps.nds_name.gsub(" ","_")}.xls", :type => 'application/octet-stream'.freeze)
  end
  
  def create_authorized?
    return current_user.has_role("admin")
  end

  def update_authorized?
    return current_user.has_role("admin")
  end

  def delete_authorized?
    return current_user.has_role("admin")
  end

  def disable
    sets = []
    @product = Product.find(params[:id])
    
    product_links = Includedproduct.where(product_id:params[:id])
    product_links.each do |i|
      if (i.productset_id != nil)
        ps = Productset.find(i.productset_id)
      
        ps.setchanges = i.product.volumeid + " was removed from productset"
        sets << ps.setcode
        i.productset_id = nil
        i.save
        ps.save
      end
    end
    
    flash[:notice] = "Product #{@product.volumeid} was removed and succesfully removed from productsets: #{sets.inspect}"  
    @product.removed_yn = 1
    @product.save!
    redirect_to :action => 'index'
  end
  
  def index
    if params[:reset]
      params.delete :search
      params.delete :product_line
      params.delete :reset

      params[:sort] = 'products.volumeid'
      params[:direction] = 'desc'
      params[:removed] = false
    end

    conditions_string_ary = ["(products.removed_yn = false OR products.removed_yn IS NULL)"]
    conditions_param_values = []
    data_releases = []
    unless params[:search].blank?
      data_releases = DataRelease.where("aliases LIKE '\%#{params[:search].strip}\%'").collect{|dr|dr.name}.uniq
      unless data_releases.empty?
        data_release_aliases_str = " OR" + data_releases.collect{|dr|"product_data_set_infos.aliases LIKE '\%#{dr.name}\%'"}.join(" OR ")
      end
      conditions_string_ary << "(products.id = ?
                                 OR products.name LIKE ?
                                 OR products.volumeid LIKE ?
                                 OR products.detailedcoverage LIKE ?
                                 OR products.nds_name LIKE ?
                                 OR productsets.setcode LIKE ?
                                 OR product_data_set_infos.data_release LIKE ?
                                 OR product_lines.name LIKE ?
                                 OR product_lines.abbr LIKE ?#{data_release_aliases_str})"
      conditions_param_values << params[:search].strip.to_i # for products.id
      8.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
    end

    unless params[:product_line].blank?
       conditions_string_ary << "(products.product_line_id = #{params[:product_line]} or product_lines.parent_id = #{params[:product_line]})"
    end


    conditions = conditions_string_ary.join(' AND ')

    
    @products = Product.eager_load(:product_line).joins("LEFT OUTER JOIN includedproducts ON
                                 includedproducts.product_id = products.id LEFT OUTER JOIN productsets ON 
                                 includedproducts.productset_id = productsets.id LEFT OUTER JOIN product_data_set_infos ON 
                                 product_data_set_infos.product_id = products.id").where([conditions] + conditions_param_values).group('volumeid').uniq.order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])
    if request.xhr?
      render(:partial => "product", :collection => @products)
    end    
  end

  def edit
    @product = Product.find(params[:id])
    @active_orders = @product.active_orders
    if @active_orders.empty? || !params[:force_edit].blank?

      if params[:force_edit] == "true"
        @product.force_edit(current_user)
      end

      # place_holder new coverage
      if @product.coverage
        @org_region_coverage = @product.coverage.regions
        @tmp_coverage = @product.coverage.dup
        @tmp_coverage.status = 0
        @tmp_coverage.save
        @product.coverage.regions.each do |r|
          new_r =  CoveragesRegion.new
          new_r.coverage_id = @tmp_coverage.id
          new_r.region_id = r.id
          new_r.save
        end
        @region_coverage = @tmp_coverage.regions.order(:name)
      else
        @tmp_coverage = Coverage.new
        @tmp_coverage.status = 0
        @tmp_coverage.save
        @region_coverage = []
      end
      # place_holder new secondary coverage
      if @product.secondary_coverage
        @org_sec_region_coverage = @product.secondary_coverage.regions
        @tmp_sec_coverage = @product.secondary_coverage.dup
        @tmp_sec_coverage.status = 0
        @tmp_sec_coverage.save
        @product.secondary_coverage.regions.each do |r|
          new_r =  CoveragesRegion.new
          new_r.coverage_id = @tmp_sec_coverage.id
          new_r.region_id = r.id
          new_r.save
        end
        @sec_region_coverage = @tmp_sec_coverage.regions
      else
        @tmp_sec_coverage = Coverage.new
        @tmp_sec_coverage.status = 0
        @tmp_sec_coverage.save
        @sec_region_coverage = []
      end
      @product_data_set_infos = @product.product_data_set_infos
    else
      render :active_orders_warning, :object => [@active_orders,@product]
    end
  end

  def select
    p = nil
    Productset.transaction do
      flash[:notice] = "Product was succesfully added to productset"
      p = Productset.find(params[:link_id])
      ip = Includedproduct.new
      #ip.productset_id = p.id
      ip.productset = p
      ip.product_id = params[:id]
      p.addchanges = "\nAdded product #{ip.product.volumeid} to productset"
      p.setchanges = "\nAdded product #{ip.product.volumeid} to productset"
      p.save
      #p.includedproducts << ip
      ip.save
    end
    redirect_to :controller => 'productsets', :action => 'edit', :id => p.id
  end

  def new
    @product = Product.new
    @tmp_coverage = Coverage.new
    @tmp_coverage.save
    @region_coverage = []
    @tmp_sec_coverage = Coverage.new
    @tmp_sec_coverage.save
    @sec_region_coverage = []  
   
  end

  def export_pdf
    @ps = Product.find(params[:id])
    send_data @ps.pdf.render, :disposition => "inline",:filename => "#{@ps.version}.pdf", :type => "application/pdf"
  end

  def destroy_link
    succes= false
    ProductDataSetInfo.transaction do
      pinfo = ProductDataSetInfo.find(params[:id])
      p = Product.find(params[:product_id])
      p.last_change = "#{pinfo.name} was removed from product"
      succes = pinfo.destroy
      p.save
    end
    

    if succes
      flash[:notice] = "Data set info for product was sucessfully removed"
    else
      flash[:error] = "Data set info for product was not removed"
    end

    redirect_to(:action => 'edit', :id => params[:product_id], :set_id => params[:set_id])
  end

  def destroy_link_document
    succes = false  
    Product.transaction do
      pinfo = Includeddocument.find(params[:id])
      succes = pinfo.destroy
      p = Product.find(pinfo.product_id)
      p.last_change = "Document was removed from product"
      p.save
    end

    if succes
      flash[:notice] = "Document was sucessfully removed"
    else
      flash[:error] = "Document was not removed"
    end

    redirect_to(:action => 'edit', :id => params[:product_id])
  end

  def create
      
    Product.transaction do

      @product = Product.new(params[:product])
      succesful = @product.save
      primary_coverage = Coverage.find(params[:tmp_coverage_id])
      if !primary_coverage.regions.empty?
        @product.primary_coverage_id = params[:tmp_coverage_id]
        @product.product_data_set_infos.each do |pdsi|
          pdsi.coverage_id = @product.coverage.duplicate.id
          pdsi.save
        end

      end
      secondary_coverage = Coverage.find(params[:tmp_sec_coverage_id])
      if !secondary_coverage.regions.empty?
        @product.secondary_coverage_id = params[:tmp_sec_coverage_id]

      end
      @product.save
      
      if !succesful
        @tmp_coverage = Coverage.new
        @tmp_coverage.save
        @region_coverage = []
        @tmp_sec_coverage = Coverage.new
        @tmp_sec_coverage.save
        @sec_region_coverage = [] 
        render :action => 'new'

        return
      end
      
      if params[:link_id] && !params[:link_id].blank?
        p = Productset.find(params[:link_id])
        ip = Includedproduct.new
        #ip.productset_id = p.id
        ip.productset = p
        ip.product_id = @product.id
        ip.save
        p.addchanges = "\nAdded product #{ip.product.volumeid} to productset"
        p.setchanges = "\nAdded product #{ip.product.volumeid} to productset"
        if p.save
          flash[:notice] = "Product was successfully created and added to productset"
        end
        @product.update_customer_contents
        redirect_to :action => 'edit', :controller => 'productsets',:id => p.id, :start_action => params[:start_action]
        return
      else
        @product.update_customer_contents
        flash[:notice] = "Product was successfully created"
        if request.xhr?
         render :text => url_for(:action => :edit,:id => @product.id, :start_action => params[:start_action])
        else
        redirect_to :action => 'edit' , :id => @product.id, :start_action => params[:start_action]
      end
      end
    end
    return


#    respond_to do |type|
#      type.html do
#        if params[:iframe]=='true' # was this an iframe post ?
#          responds_to_parent do
#            if successful?
#              render :action => 'create.rjs', :layout => false
#            else
#              render :action => 'form_messages.rjs', :layout => false
#            end
#          end
#        else
#          if successful?
#            if params[:record][:link_productset_id] && params[:record][:link_productset_id].to_s.size > 0
#              redirect_to :controller => 'productsets' , :action => 'edit', :layout => true, :id =>  params[:record][:link_productset_id], :start_action => params[:start_action]
#            else
#              flash[:info] = "Productset was successfully created"
#              return_to_main
#            end
#          else
#            render(:action => 'create_form', :layout => true)
#          end
#        end
#      end
#      type.js do
#        render :action => 'create.rjs', :layout => false
#      end
#      type.xml { render :xml => response_object.to_xml, :content_type => Mime::XML, :status => response_status }
#      type.json { render :text => response_object.to_json, :content_type => Mime::JSON, :status => response_status }
#      type.yaml { render :text => response_object.to_yaml, :content_type => Mime::YAML, :status => response_status }
#    end
  end

  def update
    @product = Product.find(params[:id])
    @product.update_attributes(strong_params)
    if @product.status == 999 || @product.removed_yn
      @product.status == 999
      @product.removed_yn = true
      @product.save
    end

    @product.update_coverage(params[:tmp_coverage_id],params[:tmp_sec_coverage_id])
    @product.update_customer_contents

      @product.product_data_set_infos.each do |pdsi|
        if pdsi.data_type && pdsi.data_type.name != "TMC"
          if pdsi.coverage
            pdsi.coverage.coverages_regions.collect{|cr|cr.destroy}
          end
        end

      if !params["pdsi"].blank?
        params["pdsi"].each do |pdsi,region_id|
          product_data_set = ProductDataSetInfo.find(pdsi.split('-')[0])
          if !product_data_set.coverage
            new_coverage = Coverage.new
            new_coverage.name = product_data_set.area_name
            new_coverage.save
            product_data_set.coverage_id = new_coverage.id
            product_data_set.save
          end
          product_data_set.coverage.add_region(region_id)
        end
      end
    end

      @product.product_data_set_infos.each do |pdsi|
        if pdsi.data_type && pdsi.data_type.name == "TMC"
          if pdsi.coverage
            pdsi.coverage.coverages_regions.collect{|cr|cr.destroy}
          end
        end

      if !params["tmc_pdsi"].blank?
        params["tmc_pdsi"].each do |pdsi,region_id|
          product_data_set = ProductDataSetInfo.find(pdsi.split('-')[0])
          if !product_data_set.coverage
            new_coverage = Coverage.new
            new_coverage.name = product_data_set.area_name
            new_coverage.save
            product_data_set.coverage_id = new_coverage.id
            product_data_set.save
          end
          product_data_set.coverage.add_region(region_id)
        end
      end
    end
   
    if params[:commit] == "Add dataset"
      redirect_to :action => 'new' , :controller => 'product_data_set_infos', :link_id => params[:id], :set_id => params[:set_id]
      return
    else
      flash[:notice] = "Product was succesfully updated"
      if params[:commit] == "Add existing dataset"
        redirect_to :action => 'index' , :controller => 'product_data_set_infos', :link_id => params[:id], :set_id => params[:set_id]
        return
      end
    end


    if params[:commit] == "Update All for Set"
      #propagate changes to all products for set
      checkboxes = params.select {|k,v|  k.to_s.index("prop_") }
      #puts "cb: " + checkboxes.inspect
      if checkboxes.flatten.size >= 1
        ps = Productset.find(params[:record][:link_productset_id])
        ps.products.each do |product|
          checkboxes[0].each do |checkbox|
            field = checkbox.scan(/prop_(.*)/).to_s
            eval("product.#{field} = params[:record][field.to_sym]")
          end
          product.save
        end
      end
    end

    redirect_to :action => 'edit', :id =>  params[:id], :start_action => params[:start_action], :force_edit => false
    return

    respond_to do |type|
      type.html do
        if params[:iframe]=='true' # was this an iframe post ?
          responds_to_parent do
            if successful?
              render :action => 'update.rjs', :layout => false
            else
              render :action => 'form_messages.rjs', :layout => false
            end
          end
        else # just a regular post
          if successful?
            if params[:record][:link_productset_id] && params[:record][:link_productset_id].to_s.size > 0
              redirect_to :controller => 'productsets' , :action => 'edit', :layout => true, :id =>  params[:record][:link_productset_id], :start_action => params[:start_action]
            else
              flash[:info] = "Product was successfully updated"
              return_to_main
            end
          else
            render(:action => 'update_form', :layout => true)
          end
        end
      end
      type.js do
        render :action => 'update.rjs', :layout => false
      end
      type.xml { render :xml => response_object.to_xml, :content_type => Mime::XML, :status => response_status }
      type.json { render :text => response_object.to_json, :content_type => Mime::JSON, :status => response_status }
      type.yaml { render :text => response_object.to_yaml, :content_type => Mime::YAML, :status => response_status }
    end
  end

  def clone
    @product = Product.find(params[:id])
    if @cloned_product = @product.duplicate
      flash[:info] = "Product was successfully cloned"
      redirect_to :action => 'edit', :id =>  @cloned_product
    else
      flash[:error] = "Product could not be cloned!"    
      redirect_to :back
    end
  end

  def show_nds
    ps = Product.find(params[:id])
    send_file("/data/newgroups/Product_Management/Product_Definitions/Individual_Sheets/NDS_#{ps.nds_name.gsub(" ","_")}.xls", :filename => "NDS_#{ps.nds_name.gsub(" ","_")}.xls", :type => 'application/octet-stream'.freeze)
  end
  
  def update_coverage_view_by_map
    if params[:id].blank?
      @product = Product.new(params[:product])
    else
      @product = Product.find(params[:id])
    end
    @tmp_coverage = Coverage.find(params[:tmp_coverage_id])
    new_region = Region.where(:country_iso_code => "#{params[:region_code]}", :ruby_type => "Country").last
    if !new_region.nil?
      existing = @tmp_coverage.coverages_regions.where(:region_id => new_region.id).last
      coverages_region = CoveragesRegion.new
      coverages_region.coverage_id = params[:tmp_coverage_id]
      coverages_region.region_id = new_region.id
      if existing.nil? # uniq
        coverages_region.save
      else
        existing.destroy
      end
    end
    @region_coverage = @tmp_coverage.regions
    
    render :layout => false
  end

  def update_coverage_view_by_select
    if params[:id].blank?
      @product = Product.new(params[:product])
    else
      @product = Product.find(params[:id])
    end
    @tmp_coverage = Coverage.find(params[:tmp_coverage_id])
    if params[:region_id].blank?
      new_region = Region.find(params[:region_selector])
    else
      new_region = Region.find(params[:region_id])
    end
    existing = @tmp_coverage.coverages_regions.where(:region_id => new_region.id).last
    coverages_region = CoveragesRegion.new
    coverages_region.coverage_id = params[:tmp_coverage_id]
    coverages_region.region_id = new_region.id
    if existing.nil? #uniq
      coverages_region.save
    end
  
    @region_coverage = @tmp_coverage.regions
  
    render :layout => false
  end

  def update_coverage_view_by_delete
    if params[:id].blank?
      @product = Product.new(params[:product])
    else
      @product = Product.find(params[:id])
    end
    
    @tmp_coverage = Coverage.find(params[:tmp_coverage_id])
    
    region_for_delete = @tmp_coverage.coverages_regions.where(:region_id => params[:region_id]).last
    region_for_delete.destroy
    @region_coverage = @tmp_coverage.regions

    render :layout => false
  end

  def save_coverage_for_data_sets
    @product = Product.find(params[:id])
    coverage_delta =   @product.compare_coverage(params[:tmp_coverage_id])

    if !coverage_delta.empty?
      @product.coverage.swap_temp_status(params[:tmp_coverage_id]) if @product.coverage
      @product.primary_coverage_id = params[:tmp_coverage_id]
      @product.last_change = coverage_delta.join(' ')
      @product.save
    end
    
    @org_region_coverage = @product.coverage.regions
    @tmp_coverage = @product.coverage.dup
    @tmp_coverage.status = 0
    @tmp_coverage.save
    @product.coverage.regions.each do |r|
      new_r =  CoveragesRegion.new
      new_r.coverage_id = @tmp_coverage.id
      new_r.region_id = r.id
      new_r.save
    end
    @region_coverage = @tmp_coverage.regions
  end
  
  def update_sec_coverage_view_by_map
    @tmp_sec_coverage = Coverage.find(params[:tmp_sec_coverage_id])
    new_region = Region.where(:country_iso_code => "#{params[:region_code]}", :ruby_type => "Country").last
    if !new_region.nil?
      existing = @tmp_sec_coverage.coverages_regions.where(:region_id => new_region.id).last
      coverages_region = CoveragesRegion.new
      coverages_region.coverage_id = params[:tmp_sec_coverage_id]
      coverages_region.region_id = new_region.id
      if existing.nil? #uniq
        coverages_region.save
      else
        existing.destroy
      end
    end
    @sec_region_coverage = @tmp_sec_coverage.regions
    @product_data_set_infos = @product.product_data_set_infos
    render :layout => false
  end

  def update_sec_coverage_view_by_select
    @tmp_sec_coverage = Coverage.find(params[:tmp_sec_coverage_id])
    new_region = Region.find(params[:region_id])
    existing = @tmp_sec_coverage.coverages_regions.where(:region_id => new_region.id).last
    coverages_region = CoveragesRegion.new
    coverages_region.coverage_id = params[:tmp_sec_coverage_id]
    coverages_region.region_id = new_region.id
    if existing.nil? #uniq
      coverages_region.save
    end
  
    @sec_region_coverage = @tmp_sec_coverage.regions
  
    render :layout => false
  end

  def update_sec_coverage_view_by_delete
    @tmp_sec_coverage = Coverage.find(params[:tmp_sec_coverage_id])
    region_for_delete = @tmp_sec_coverage.coverages_regions.where(:region_id => params[:region_id]).last
    region_for_delete.destroy
    @sec_region_coverage = @tmp_sec_coverage.regions
  
    render :layout => false
  end

  def pdsi_from_product
    @product = Product.find(params[:id])
    @region_coverage = @product.coverage.regions
    @tmp_coverage = @product.coverage
    if !params[:purpose].blank?
      case  params[:purpose]
      when 'update'
        @product_data_set_info = ProductDataSetInfo.find(params[:pdsi_id])
        params[:product_data_set_info] = from_product_params
        @product_data_set_info.update_attributes(from_product_params)
      when 'delete'
        ProductDataSetInfo.find(params[:pdsi_id]).destroy
      when 'new'
        @product_data_set_info = ProductDataSetInfo.new(from_product_params)
        @product_data_set_info.product_id = @product.id
        @product_data_set_info.save
      end
    end
    @product.uniquify_product_data_set_infos
    render :layout => false
  end

  def get_regions
    @tmp_coverage = Coverage.find(params[:tmp_coverage_id])
    
    @results = CSV.parse(params[:data]).flatten
    @normal_results = []
    isiso = true
    @results.each{|x| isiso = false if x.to_s.size != 3}
    @results.each do |result|
      if isiso
        r = Region.find_by_region_code(result)
        if r
          r = [r]
        end
        if r
          choices = {:org_val => r[0].name, :regions => []}
        else
          choices = {:org_val => result, :regions => []}
        end
      else
        r = Region.active.where(name:result)
        choices = {:org_val => result, :regions => []}
      end
      if r && r.size > 0
        r.each do |reg|
          choices[:regions] << {:region_code => reg.region_code, :found => true}
        end
      else
        choices[:regions] = [] if !choices[:regions]
        choices[:regions] << {:region_code => "UNKNOWN" , :found => false}
      end
      @normal_results << choices
    end
    render :layout => false
  end

  def sync_regions
    if params[:id].blank?
      @product = Product.new(params[:product])
    else
      @product = Product.find(params[:id])
    end
    @tmp_coverage = Coverage.find(params[:tmp_coverage_id])
    Region.transaction do
      params[:region_codes].split(',').each do |code|
          region = Region.find_by_region_code(code)
          @tmp_coverage.regions << region if region
        end
      end
      @tmp_coverage.save!
      @region_coverage = @tmp_coverage.regions
  end

  def filling_necessary
    data_type = DataType.find(params[:data_type_id])
    @filling = params[:filling]
    @fillings = data_type.fillings
    if !params[:product_data_set_info_id].blank?
      @product_data_set_info = ProductDataSetInfo.find(params[:product_data_set_info_id])
    end
    render :layout => false
  end

  def create_data_set_from_definition
    @product = Product.find(params[:id])
    @product.update_coverage(params[:tmp_coverage],params[:tmp_sec_coverage])
    
    if !params[:pdsi_id].blank?
      ProductDataSetInfo.find(params[:pdsi_id]).destroy
    end
    source_data_set = DataSet.find(params[:data_set_id])
    product_data_set_info = source_data_set.map_to_product_data_set_info
    product_data_set_info.product_id = @product.id
    product_data_set_info.save
    @product.uniquify_product_data_set_infos
    @region_coverage = @product.coverage.regions
    @tmp_coverage = @product.coverage
    @product.reload
    render :layout => false
  end

  def create_data_sets_from_definition_group
    @product = Product.find(params[:id])
    @product.update_coverage(params[:tmp_coverage],params[:tmp_sec_coverage])
    if !params[:pdsi_id].blank?
      ProductDataSetInfo.find(params[:pdsi_id]).destroy
    end
    DataSetGroup.transaction do
      DataSetGroup.find(params[:data_set_group_id]).data_sets.each do |grouped_data_set|
        product_data_set_info = grouped_data_set.map_to_product_data_set_info
        product_data_set_info.product_id = @product.id
        product_data_set_info.save!
      end
      @product.uniquify_product_data_set_infos
    end
    @region_coverage = @product.coverage.regions
    @tmp_coverage = @product.coverage
    @product.reload
    render :layout => false
  end

  def validate_volumeid
    p = Product.find_by_volumeid(params[:volumeid])
    allow = false
    message = ''
    if p
      if !params[:id].blank? && ( p.id.to_s == params[:id] )
         allow = true
      end
    else
      allow = true
    end
    message = "Product ID already in use." if !allow
    render :text => message
  end

#merged from backoffice
  def get_product_details
    if !params["product_id"]
     message = "product_id parameter missing"
     errorlog("SYNC ERROR: " + message)
     return render :text => message, :status => :unprocessable_entity
    else
      product = Product.find_by_volumeid(params["product_id"])
      if product
        return render :text => product.attributes.to_json
      else
        message = "product_id not found"
        errorlog("SYNC ERROR: " + message)
        return render :text => message, :status => :unprocessable_entity
      end
    end
  end

  protected
  def create_authorized?
    return current_user.has_role("admin") || current_user.has_role("product manager")
  end

  def update_authorized?
    return current_user.has_role("admin") || current_user.has_role("product manager")
  end

  def edit_authorized?
  end

  def delete_authorized?
    return false
  end

  def clone_authorized?
    return current_user.has_role("admin") || current_user.has_role("product manager")
  end

  private
  def sort_column  
    params[:sort] || "volumeid"  
  end  
    
  def sort_direction  
    params[:direction] || "desc"  
  end
  
  def search
    params[:search] || ""
  end

   def from_product_params
    params.require(:from_product).permit!
  end


end
