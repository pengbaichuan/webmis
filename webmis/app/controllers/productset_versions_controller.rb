require 'zip/zipfilesystem'
#require 'pdf/writer'
#require 'pdf/techbook'
#require 'pdf/simpletable'

class ProductsetVersionsController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"
 
 def print
  @record = ProductsetVersion.find(params[:id])
  render :action => 'show', :layout => 'print'
 end

 def create_order
  redirect_to :action => 'index'
 end

 def get_datareleases
    if (params[:id] && params[:id] != "")
            @dr = DataRelease.where('company_abbr_id = ' + params[:id])
    else
            @dr = []
    end
    render :partial => "datarelease_select"
 end

 def create
      do_create
      respond_to do |type|
        type.html do
          if params[:iframe]=='true' # was this an iframe post ?
            responds_to_parent do
              if successful?
                render :action => 'create.rjs', :layout => false
              else
                render :action => 'form_messages.rjs', :layout => false
              end
            end
          else
            if successful?
             if params[:commit] == "Add product"
              redirect_to :controller => 'products' , :action => 'new', :layout => true, :link_id => @record.id
             elsif params[:commit] == "Add existing product"
              redirect_to :controller => 'products' , :action => 'index', :layout => true, :link_id => @record.id
             else
              flash[:info] = "Product succesfully added"
              return_to_main
             end
            else
              render(:action => 'create_form', :layout => true)
            end
          end
        end
        type.js do
          render :action => 'create.rjs', :layout => false
        end
        type.xml { render :xml => response_object.to_xml, :content_type => Mime::XML, :status => response_status }
        type.json { render :text => response_object.to_json, :content_type => Mime::JSON, :status => response_status }
        type.yaml { render :text => response_object.to_yaml, :content_type => Mime::YAML, :status => response_status }
      end
end

 def update
      if params[:commit] == "Add product"
              redirect_to :controller => 'products' , :action => 'new', :layout => true, :link_id => params[:id]
      elsif params[:commit] == "Add existing product"
              redirect_to :controller => 'sproducts' , :action => 'index', :layout => true, :link_id => params[:id]
      else 
       do_update

      respond_to do |type|
        type.html do
          if params[:iframe]=='true' # was this an iframe post ?
            responds_to_parent do
              if successful?
                render :action => 'update.rjs', :layout => false
              else
                render :action => 'form_messages.rjs', :layout => false
              end
            end
          else # just a regular post
            if successful?
             if params[:commit] == "Add product"
              redirect_to :controller => 'products' , :action => 'new', :layout => true, :link_id => @record.id
             elsif params[:commit] == "Add existing product"
              redirect_to :controller => 'sproducts' , :action => 'index', :layout => true, :link_id => @record.id
             else
              flash[:info] = "Product succesfully updated"
              return_to_main
             end
            else
              render(:action => 'update_form', :layout => true)
            end
          end
        end
        type.js do
          render :action => 'update.rjs', :layout => false
        end
        type.xml { render :xml => response_object.to_xml, :content_type => Mime::XML, :status => response_status }
        type.json { render :text => response_object.to_json, :content_type => Mime::JSON, :status => response_status }
        type.yaml { render :text => response_object.to_yaml, :content_type => Mime::YAML, :status => response_status }
      end
    end
 end


 def edit
  super
  #
 end

 def delete_linked_product
   i = Includedproduct.find(params[:iid])
   #ProductsetVersion.transaction do
    ps = ProductsetVersion.find(params[:id])
    ps.setchanges = i.product.volumeid + " was removed from productset"
    i.productset_id = nil 
    i.save
    ps.save
   #end
   redirect_to :action => 'edit' , :id => params[:id]
 end

 def export_pdf
@ps = ProductsetVersion.find(params[:id])
send_data @ps.pdf.render, :filename => 'report.pdf', :type => "application/pdf"

 end 

 def export

 @ps = ProductsetVersion.find(params[:id])
 result = render_to_string :action => :ppf, :layout => false
 FileUtils.mkdir_p '/tmp/sessions/'
 FileUtils.mkdir_p '/tmp/sessions/' + session.session_id
 FileUtils.cp "public/templates/template.ods", "/tmp/sessions/#{session.session_id}/data.ods"
 Zip::ZipFile.open("/tmp/sessions/#{session.session_id}/data.ods") {
  |zipfile|
  zipfile.file.open("content.xml","w") {|f| f.puts result}
 }

 #send_file ("/tmp/sessions/#{session.session_id}/data.ods", :filename =>"#{@ps.setcode}.ods", :type =>'application/octet-stream'.freeze)

 end

 def index
  if session["as:productsets"] && session["as:productsets"]["list_filter"] && session["as:productsets"]["list_filter"]["textfilter"].values.to_s.size > 0
   @filter_active =  session["as:productsets"]["list_filter"]["textfilter"]
  else
   @filter_active =nil
  end 
  super
 end

 def update_table 
  if  session["as:productsets"] && session["as:productsets"]["list_filter"] && session["as:productsets"]["list_filter"]["textfilter"].values.to_s.size > 0
   @filter_active =  session["as:productsets"]["list_filter"]["textfilter"]
  else
   @filter_active =nil
  end 
  super
 end
 

 def cloneit 
  ps = ProductsetVersion.find(params[:id])
  psnew = ps.dup
  psnew.name = "#CLONED#" + ps.name.to_s
  psnew.status = 5
    psnew.minor = 0
    psnew.minor = 0
  psnew.save
  ps.includedproducts.each do |product|
    nip = Includedproduct.new
    pnew = product.product.dup
    pnew.volumeid = "#CLONED#" + pnew.volumeid
    pnew.save
    nip.product_id = pnew.id
    nip.productset_id = psnew.id
    nip.save
    psnew.includedproducts << nip 
  end 

  flash[:info] = "ProductsetVersion succesfully cloned"
  redirect_to :action => 'index'
 end



 def show_tmc
  ps = ProductsetVersion.find(params[:id])
  #send_file ("public/webmis/tmcsheets/#{ps.tmc_file}", :filename => "#{ps.tmc_file}", :type => 'application/octet-stream'.freeze)
 end

 def salespack
  sheetloc = "/data/newgroups/Product_Management/Product_Definitions/Individual_Sheets"
  @ps = ProductsetVersion.find(params[:id])
  nds_file = "NDS_" + @ps.nds_name.gsub(" ", "_") + ".xls"
  FileUtils.mkdir_p '/tmp/sessions/'
  FileUtils.mkdir_p '/tmp/sessions/' + session.session_id
  if File.exists?("/tmp/sessions/#{session.session_id}/salespack.zip")
  FileUtils.rm "/tmp/sessions/#{session.session_id}/salespack.zip"
  end 

  @files = Hash.new
  puts "NDS :" + nds_file

  Zip::ZipFile.open("/tmp/sessions/#{session.session_id}/salespack.zip", Zip::ZipFile::CREATE) {
   |zipfile|
   if File.exists?(sheetloc + "/" + nds_file)
     zipfile.add("#{nds_file}", sheetloc + "/" + nds_file)
     @files["Navigation database Specification"] = nds_file
   end
   if @ps.tmc_file && File.exists?("#{sheetloc}/#{@ps.tmc_file}")
     zipfile.add("#{@ps.tmc_file}", "#{sheetloc}/#{@ps.tmc_file}")
     @files["TMC Specification"] = @ps.tmc_file
   end

   @ps.products.each do |pr|
    if pr.nds_name 
       nds_file = "NDS_" + pr.nds_name.gsub(" ", "_") + ".xls"
       if File.exists?(sheetloc + "/" + nds_file)
         zipfile.add("#{nds_file}", sheetloc + "/" + nds_file)
         @files["Navigation database Specification #{pr.volume_id}"] = nds_file
       end
    end
   end

   index = render_to_string :action => 'index_salespack', :layout => false 
   zipfile.file.open("index.html","w") {|f| f.puts index}
   zipfile.file.open("ppf.pdf","w") {|f| f.puts @ps.pdf}
  }
  #send_file ("/tmp/sessions/#{session.session_id}/salespack.zip", :filename => "#{@ps.setcode}_salespack.zip", :type => 'application/octet-stream'.freeze)
 end

 
protected
 def create_authorized?
  return current_user.has_role("admin")
 end

 def update_authorized?
  return current_user.has_role("admin")
 end

 def delete_authorized?
  return current_user.has_role("admin") 
 end

end
