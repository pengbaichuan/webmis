class ProductsetsController < ApplicationController
 require 'importer.rb'
 before_filter :authorize
 authorize_resource :except => [:remove_tag,:favorite]
 
#Roles for manage: product manager
 
 
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search
  
  def show
    @record = Productset.find(params[:id])
    @hdrs = @record.headrs
    @page_title = 'PDS: ' + @record.name.to_s
  end

  def handle
    @record = Productset.find(params[:id])
    @hdrs = @record.headrs
    @page_title = 'Handle PDS: ' + @record.name.to_s
  end

  def index
    if params[:reset]
      params.delete :search
      params.delete :reset

      params.delete :sort
      params.delete :direction
      params.delete :product_line
      params[:removed] = false
    end
    conditions_string_ary = []
    conditions_param_values = []

    @favorites = Productset.tagged_with("favorite", :on => :favorites, :owned_by => current_user)

    @page_title = 'PDS: ' + "Productsets"
    conditions_string_ary << "1=1"

    if params[:removed].blank?
      conditions_string_ary << "and (productsets.removed_yn = 0 or productsets.removed_yn is null)"
    end   

    unless params[:search].blank?
      conditions_string_ary << "and (productsets.setcode LIKE ? 
                                         OR productsets.name LIKE ?
                                         OR productsets.data_set_description LIKE ?
                                         OR productsets.output_format LIKE ?
                                         OR productsets.area_name LIKE ?
                                         OR productsets.predecessor LIKE ?
                                         OR productsets.nds_name LIKE ?
                                         OR productsets.customer_name LIKE ?
                                         OR productsets.firsttiername LIKE ?
                                         OR productsets.remarks LIKE ?
                                         OR products.volumeid LIKE ?
                                         OR company_abbrs.company_name LIKE ?
                                         OR company_abbrs.ms_abbr_3 LIKE ?
                                         OR product_lines.name LIKE ?
                                         OR product_lines.abbr LIKE ?)"
      15.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
    end

    unless params[:product_line].blank?
       conditions_string_ary << "and (products.product_line_id = ? or product_lines.parent_id = ?)"
       2.times do
         conditions_param_values <<  params[:product_line]
       end
    end

    conditions = conditions_string_ary.join(" ")
    @productsets = Productset.eager_load(:products,:company_abbr,:product_lines).where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])
    generate_id_sequence_cookie(@productsets,params[:page])
    if request.xhr?
      render(:partial => "productset", :collection => @productsets)
    end
  end

  def print
    @record = Productset.find(params[:id])
    
    render :action => 'show', :layout => 'print'
  end

  def create_order
    redirect_to :action => 'index'
  end

  def new
    @productset = Productset.new
  end

  def get_datareleases
    if (params[:did] && params[:did] != "")
      @dr = Datarelease.where(:company_abbr_id => params[:did])
    else
      @dr = []
    end
    render :partial => "datarelease_select"
  end

  def create_old
    do_create
    respond_to do |type|
      type.html do
        if params[:iframe]=='true' # was this an iframe post ?
          responds_to_parent do
            if successful?
              render :action => 'create.rjs', :layout => false
            else
              render :action => 'form_messages.rjs', :layout => false
            end
          end
        else
          if successful?
            if params[:commit] == "Add product"
              redirect_to :controller => 'products' , :action => 'new', :layout => true, :link_id => @record.id
            elsif params[:commit] == "Add existing product"
              redirect_to :controller => 'products' , :action => 'index', :layout => true, :link_id => @record.id
            else
              flash[:info] = "Product was successfully created"
              return_to_main
            end
          else
            render(:action => 'create_form', :layout => true)
          end
        end
      end
      type.js do
        render :action => 'create.rjs', :layout => false
      end
      type.xml { render :xml => response_object.to_xml, :content_type => Mime::XML, :status => response_status }
      type.json { render :text => response_object.to_json, :content_type => Mime::JSON, :status => response_status }
      type.yaml { render :text => response_object.to_yaml, :content_type => Mime::YAML, :status => response_status }
    end
  end

  def create
    @productset = Productset.new
    notice = ""
    params[:productset][:conversiontool_name] = params[:productset][:conversiontool_name] + "_" + params[:subbundle] if params[:subbundle] && params[:subbundle].size > 0
    
    succesful = @productset.update_attributes(strong_params)

    respond_to do |format|
      if succesful
        flash[:notice] = notice + 'Productset was successfully created, now add your products.'
        format.html  { redirect_to :action => 'edit', :id => @productset.id.to_s }
        format.xml  { render :xml => @productset, :status => :created, :location => @productset }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @productset.errors, :status => :unprocessable_entity }
      end
    end
   
  end

  def destroy_link
    ic = Includedproduct.find(params[:link_id])
    ic.destroy
    flash[:notice] = 'Product unlinked.'
    redirect_to :action => 'edit', :id => params[:id] 
  end

  def update
    
    if params[:commit] == "Add product"
      redirect_to :controller => 'products' , :action => 'new', :layout => true, :link_id => params[:id]
    elsif params[:commit] == "Add existing product"
      redirect_to :controller => 'products' , :action => 'index', :layout => true, :link_id => params[:id]
    else
      @productset = Productset.find(params[:id])
      @productset.setchanges = "Updated core attributes"
      succesful =  @productset.update_attributes(strong_params)

      respond_to do |type|
        type.html do
          if params[:iframe]=='true' # was this an iframe post ?
            responds_to_parent do
              if successful?
                flash[:notice] = "Productset was successfully updated"
                render :action => 'update.rjs', :layout => false
              else
                render :action => 'form_messages.rjs', :layout => false
              end
            end
          else # just a regular post
            if succesful
              flash[:notice] = "Productset and product was successfully updated."
              if params[:commit] == "Add product"
                redirect_to :controller => 'sproducts' , :action => 'new', :layout => true, :link_id => @record.id
              elsif params[:commit] == "Add existing product"
                redirect_to :controller => 'sproducts' , :action => 'index', :layout => true, :link_id => @record.id
              else
                flash[:notice] = "Productset was successfully updated"
                redirect_to :action => 'edit'
                #return_to_main
              end
            else
              render(:action => 'edit')
            end
          end
        end
        
        #type.js do
        #  render :action => 'update.rjs', :layout => false
        #end
        type.xml { render :xml => response_object.to_xml, :content_type => Mime::XML, :status => response_status }
        type.json { render :text => response_object.to_json, :content_type => Mime::JSON, :status => response_status }
        type.yaml { render :text => response_object.to_yaml, :content_type => Mime::YAML, :status => response_status }
      end
    end
  end

  def edit
    @productset = Productset.find(params[:id])
    @page_title = "Edit Productset " + @productset.id.to_s
    if @productset.conversiontool_name
      if @productset.conversiontool_name.split("_")[1]
        params[:subbundle] =  @productset.conversiontool_name.split("_")[1]
        @productset.conversiontool_name = @productset.conversiontool_name.split("_")[0]
       

        
      end
    end

    #
  end

  def disable
    @productset = Productset.find(params[:id])
    @products_in_set = Productset.find(@productset).products
    multiple = Array.new
    multi = 0
    
    if @products_in_set.empty?
      @productset.removed_yn = 1
      @productset.save
      flash.now[:notice] = "Productset: #{@productset.name}, is succesfully removed from the database."
      
    else
    @products_in_set.each do |pis|
      obj_product = Product.new
      c = obj_product.amount_multiple_product_usage(pis)
      if ( c >= 2)
        
        multi = 1
        Includedproduct.where(product_id:pis.volumeid).each do |l|
          if ( l.productset_id != nil ) 
            @pst =  Productset.find(l.productset_id)
            if ( l.productset_id != @productset.id && !@pst.nil?)
              multiple.push("#{pis.volumeid} in #{@pst.name}  (#{l.productset_id})")
            else
              @productset.setchanges = pis.volumeid + " was removed from productset"
              l.productset_id = nil
              l.save!              
            end
          end
        end

      else
                
        pis.removed_yn = 1
        @productset = Productset.find(params[:id])
        @productset.removed_yn = 1
        flash.now[:notice] = "Productset and included product(s) are succesfully removed from the database."
        @productset.save!
        pis.save
        
      end
      
      if (multi == 1)
        @productset.removed_yn = 1
        flash.now[:notice] = "Productset was succesfully removed from the database. </br > 
                          The following product(s) were available in another productset, 
                          so they are not removed: </br >
                          #{multiple.inspect.to_s.gsub(',','<br />').gsub(/[\[\]]/,'')}"
        @productset.save!
      end
      
    end
    end
    redirect_to :back
  end

  def delete_linked_product
    i = Includedproduct.find(params[:iid])
    #Productset.transaction do
    ps = Productset.find(params[:id])
    ps.setchanges = i.product.volumeid + " was removed from productset"
    i.productset_id = nil 
    i.save
    ps.save
    #end
    redirect_to :action => 'edit' , :id => params[:id]
  end

  def pdf_all
    begin
      @ps = Productset.find(params[:id])
      @files = []
      uniquedir = "/tmp/tempruby_" + DateTime.now.hash.to_s + (rand*100000000000).to_i.to_s + "/"
      FileUtils.mkdir_p uniquedir
      filename = uniquedir + "pset_" + @ps.id.to_s + ".pdf"
    
      File.open(filename, "w") do |f|
        if @ps.type == "CountryConversionProductset"
          f.write(@ps.pdf_nc)
        else
          f.write(@ps.pdf)
        end
      end
      @files << filename

      @ps.document_versions.each do |doc|
        puts " documents found - "
        if doc.content_type == "application/pdf"
          filename = uniquedir + "product_" + @ps.id.to_s + "-addon-" + doc.id.to_s + ".pdf"
          File.open(filename, "w") do |f|
            f.write(doc.binary_data)
            @files << filename
          end
        end
      end


      @ps.products.each do |product|
        filename = uniquedir + "product_" + product.id.to_s + ".pdf"
        File.open(filename, "w") do |f|
          f.write(product.pdf)
          @files << filename
        end
        puts "checking for documents - " + product.id.to_s
        product.document_versions.each do |doc|
          puts " documents found - "
          if doc.content_type == "application/pdf"
            filename = uniquedir + "product_" + product.id.to_s + "-addon-" + doc.id.to_s + ".pdf"
            File.open(filename, "w") do |f|
              f.write(doc.binary_data)
              @files << filename
            end
          end
        end
      end
    
      b = ""
      @files.collect{|x| b= b + x + " "}
      puts "pdftk #{b} cat output #{uniquedir}combined.pdf"
      `pdftk #{b} cat output #{uniquedir}combined.pdf`
      send_file "#{uniquedir}combined.pdf", :disposition => "inline",:filename => "#{@ps.setcode.gsub(' ','_')}-V#{@ps.major.to_s}-#{@ps.minor.to_s}-consolidated.pdf", :type => "application/pdf"
    rescue => error
      render :text => "Details for Productset are not available"
    end
  end

  def export_pdf
    @ps = Productset.find(params[:id])
    filename = "/tmp/bla.pdf"

    File.open(filename, "w") do |f|
      f.write(@ps.pdf)
    end
    send_data @ps.pdf.render, :disposition => "inline",:filename => "#{@ps.setcode.gsub(' ','_')}-V#{@ps.major.to_s}-#{@ps.minor.to_s}.pdf", :type => "application/pdf"
  
  
  
  end

  def export_pdf_nc
    @ps = Productset.find(params[:id])
    filename = "/tmp/bla.pdf"
    File.open(filename, "w") do |f|
      f.write(@ps.pdf_nc)
    end
    send_data @ps.pdf_nc.render,:disposition => "inline", :filename => "#{@ps.setcode.gsub(' ','_')}-V#{@ps.major.to_s}-#{@ps.minor.to_s}.pdf", :type => "application/pdf"
  end

  def export

    @ps = Productset.find(params[:id])
    result = render_to_string :action => :ppf, :layout => false
    FileUtils.mkdir_p '/tmp/sessions/'
    FileUtils.mkdir_p '/tmp/sessions/' + session.session_id
    FileUtils.cp "public/templates/template.ods", "/tmp/sessions/#{session.session_id}/data.ods"
    Zip::ZipFile.open("/tmp/sessions/#{session.session_id}/data.ods") {
      |zipfile|
      zipfile.file.open("content.xml","w") {|f| f.puts result}
    }

    send_file("/tmp/sessions/#{session.session_id}/data.ods", :filename =>"#{@ps.setcode}.ods", :type =>'application/octet-stream'.freeze)

  end

  def update_table
    if  session["as:productsets"] && session["as:productsets"]["list_filter"] && session["as:productsets"]["list_filter"]["textfilter"].values.to_s.size > 0
      @filter_active =  session["as:productsets"]["list_filter"]["textfilter"]
    else
      @filter_active =nil
    end
    super
  end
 
  def cloneit
    ps = Productset.find(params[:id])
    psnew = ps.dup
    psnew.name = "#CLONED#" + ps.name.to_s
    psnew.status = 5
    psnew.minor = 0
    psnew.minor = 0
    psnew.save
    ps.includedproducts.each do |product|
      nip = Includedproduct.new
      pnew = product.product.dup
      pnew.volumeid = "#CLONED#" + pnew.volumeid
      pnew.save
      nip.product_id = pnew.id
      nip.productset_id = psnew.id
      nip.save
      psnew.includedproducts << nip
    end

    flash[:notice] = "Productset succesfully cloned with name : " + "#CLONED#" + ps.name.to_s
    redirect_to :action => 'edit', :id => psnew.id
  end

  def delete

    ps = Productset.find(params[:id])
    ps.removed_yn = 1
    ps.save

    flash[:notice] = "Productset was succesfully removed"
    redirect_to :action => 'index'
  end

  def clone_product
    p = Product.find(params[:id])
    pnew = p.dup
    pnew.name = "#CLONED#" + p.name.to_s
    pnew.volumeid = "#CLONED#" + p.volumeid.to_s
    pnew.save
    nip = Includedproduct.new
    nip.product_id = pnew.id
    nip.productset_id = params[:psid]
    nip.save
    flash[:info] = "Productset succesfully cloned"
    redirect_to :action => 'edit' , :id => params[:psid]
  end

  def show_tmc
    ps = Productset.find(params[:id])
    if !ps.tmc_file.blank?
    send_file("/data/newgroups/Product_Management/Product_Definitions/Individual_Sheets/#{ps.tmc_file}", :filename => "#{ps.tmc_file}", :type => 'application/octet-stream'.freeze)
    end
  end

  def show_nds
    ps = Productset.find(params[:id])
    send_file("/data/newgroups/Product_Management/Product_Definitions/Individual_Sheets/FCS_#{ps.nds_name.gsub(" ","_")}.xls", :filename => "FCS_#{ps.nds_name.gsub(" ","_")}.xls", :type => 'application/octet-stream'.freeze)
  end

  def salespack
    sheetloc = "/data/newgroups/Product_Management/Product_Definitions/Individual_Sheets"
    @ps = Productset.find(params[:id])
    nds_file = "FCS_" + @ps.nds_name.gsub(" ", "_") + ".xls"
    FileUtils.mkdir_p '/tmp/sessions/'
    FileUtils.mkdir_p '/tmp/sessions/' + session.session_id
    if File.exists?("/tmp/sessions/#{session.session_id}/salespack.zip")
      FileUtils.rm "/tmp/sessions/#{session.session_id}/salespack.zip"
    end

    @files = Hash.new
    puts "FCS :" + nds_file

    Zip::ZipFile.open("/tmp/sessions/#{session.session_id}/salespack.zip", Zip::ZipFile::CREATE) {
      |zipfile|
      if File.exists?(sheetloc + "/" + nds_file)
        zipfile.add("#{nds_file}", sheetloc + "/" + nds_file)
        @files["Navigation database Specification"] = nds_file
      end
      if @ps.tmc_file && File.exists?("#{sheetloc}/#{@ps.tmc_file}")
        zipfile.add("#{@ps.tmc_file}", "#{sheetloc}/#{@ps.tmc_file}")
        @files["TMC Specification"] = @ps.tmc_file
      end

      @ps.products.each do |pr|
        if pr.nds_name
          nds_file = "FCS_" + pr.nds_name.gsub(" ", "_") + ".xls"
          if File.exists?(sheetloc + "/" + nds_file)
            zipfile.add("#{nds_file}", sheetloc + "/" + nds_file)
            @files["Navigation database Specification #{pr.volume_id}"] = nds_file
          end
        end
      end

      index = render_to_string :action => 'index_salespack', :layout => false
      zipfile.file.open("index.html","w") {|f| f.puts index}
      zipfile.file.open("ppf.pdf","w") {|f| f.puts @ps.pdf}
    }
    send_file("/tmp/sessions/#{session.session_id}/salespack.zip", :filename => "#{@ps.setcode}_salespack.zip", :type => 'application/octet-stream'.freeze)
  end

  def start_import
    begin
      Productset.import(params[:cat_id])
      render :text => "<p style='color:green;'><b>Import was successful.</b></p>" + ENV['import_warning']
      
    rescue => e
      render :text => "<p style='color:red;'><b>Import failed.</b></p><br />" + e.message.to_s  + '<br />' + e.backtrace.to_s
    end
    ENV['import_warning'] = ''
  end

  def show_import_category_name
    @product_categories = ProductCategory.find(params[:cat_id])
    render :text => "<strong>#{@product_categories.abbreviation} </strong><small>- #{@product_categories.description}</i></small>"
  end

  def show_import
   @product_categories = ProductCategory.where(:active => true)
  end

  def destroy_link_document
    succes = false
    Includedproductsetdocument.transaction do
      pinfo = Includedproductsetdocument.find(params[:id])
      succes = pinfo.destroy
      p = Productset.find(pinfo.productset_id)
      p.setchanges = "Document was removed from productset"
      p.save
    

      if succes
        flash[:notice] = "Document was sucessfully removed"
      else
        flash[:error] = "Document was not removed"
      end

    end

    redirect_to(:action => 'edit', :id => params[:productset_id])
  end
  
  def get_datareleases_by_company_abbr_id
    @data_rel = Array.new
    @data_rel << ""
    if (params[:did] && params[:did] != "")
      @dr = DataRelease.where('production = 1 and data_releases.name is not null and company_abbr_id = ' + params[:did]).order(:name)
    else
      @dr = []
    end
    render :partial => "datarelease_name_select"
  end

  def favorite
    @productset = Productset.find(params[:id])
    current_user.tag(@productset, :with => "favorite", :on => :favorites)
    redirect_to :back, :notice => "Productset  #{@productset.name} added to your favorites." 
  end

  def remove_tag
   @productset = Productset.find(params[:id])
   current_user.tag(@productset, :with => "", :on => :favorites)
   redirect_to :back, :notice => "Productset removed from your favorites."
  end

  def clone_set
    @productset = Productset.find(params[:id])
    if @new_productset = @productset.clone_set
      flash[:info] = "Productset and included products succesfully cloned."
      redirect_to(:action => 'edit', :id => @new_productset.id)
    else
      flash[:error] = "Clone failed"
      redirect_to :back
    end
  end
  
  private  
  def sort_column  
    params[:sort] || "productsets.id"  
  end  
    
  def sort_direction  
    params[:direction] || "desc"  
  end
  
  def search
    params[:search] || ""
  end
 
end
