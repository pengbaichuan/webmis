class ProjectsController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search

  # GET /projects
  # GET /projects.json
  def index
    if params[:reset]
      params.delete :removed
      params.delete :search
      params.delete :reset
      params.delete :sort
      params.delete :direction
      params.delete :removed
    end

    if params[:removed] == '1'
      conditions_string_ary = ["1=1"]
    else
      conditions_string_ary = ["projects.status < #{Project::STATUS_REMOVED}"]
    end

    conditions_param_values = []

    unless params[:search].blank?
      q = params[:search].strip
      conditions_string_ary << "(projects.id = ? OR
                                 projects.name LIKE ? OR
                                 projects.description LIKE ?)"
      conditions_param_values << q.to_i # for id
      2.times do
        conditions_param_values << "%#{q}%"
      end
    end

    conditions = conditions_string_ary.join(' AND ')

    @projects = Project.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])
    generate_id_sequence_cookie(@projects,params[:page])

    if request.xhr?
      render(:partial => "project", :collection => @projects)
    end
  end

  # GET /projects/1
  # GET /projects/1.json
  def show
    @project = Project.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @project }
    end
  end

  def handle
    @project = Project.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @project }
    end
  end

  # GET /projects/new
  # GET /projects/new.json
  def new
    @project = Project.new
    @project.status = Project::STATUS_ENTRY

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @project }
    end
  end

  # GET /projects/1/edit
  def edit
    @project = Project.find(params[:id])
    @project_members = @project.users
  end

  # POST /projects
  # POST /projects.json
  def create
    @project = Project.new(strong_params)
    @project.status = Project::STATUS_ENTRY
    @project.environment_id = Environment.development_environment_id
    

    respond_to do |format|
      if @project.save
        member_roles = []
        member_roles = params[:member_roles] if !params[:member_roles].blank?
        RolesUser.update_user_roles(@project.id,current_user,member_roles)
        ConfigParameter.create_new_project_set(@project.id)
        format.html { redirect_to @project, notice: 'Project was successfully created.' }
        format.json { render json: @project, status: :created, location: @project }
      else
        format.html { render action: "new" }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /projects/1
  # PUT /projects/1.json
  def update
    @project = Project.find(params[:id])

    respond_to do |format|
      if @project.update_attributes(strong_params)
        member_roles = []
        member_roles = params[:member_roles] if !params[:member_roles].blank?
        RolesUser.update_user_roles(@project.id,current_user,member_roles)

        format.html { redirect_to @project, notice: 'Project was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    @project = Project.find(params[:id])
    @project.destroy

    respond_to do |format|
      format.html { redirect_to projects_url }
      format.json { head :no_content }
    end
  end

  def change_status
    @project = Project.find(params[:id])

    if @project.allow_status?(params[:status])
      @project.status = params[:status]
      @project.save
      flash[:notice] = "Project was successfully updated with status #{@project.status_str}."
    else
      flash[:error]  = "Not allowed to change the status to #{Project.statusarray[params[:status]]}."
    end

    respond_to do |format|
      format.html { redirect_to handle_project_path(@project)}
      format.json { render json: @project }
    end
  end

private
  def sort_column
    params[:sort] || "projects.name"
  end

  def sort_direction
    params[:direction] || "asc"
  end

  def search
    params[:search] || ""
  end
  
end