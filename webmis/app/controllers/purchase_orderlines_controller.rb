class PurchaseOrderlinesController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"

  # GET /purchase_orderlines
  # GET /purchase_orderlines.json
  def index
    @purchase_orderlines = PurchaseOrderline.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @purchase_orderlines }
    end
  end

  # GET /purchase_orderlines/1
  # GET /purchase_orderlines/1.json
  def show
    @purchase_orderline = PurchaseOrderline.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @purchase_orderline }
    end
  end

  # GET /purchase_orderlines/new
  # GET /purchase_orderlines/new.json
  def new
    @purchase_orderline = PurchaseOrderline.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @purchase_orderline }
    end
  end

  # GET /purchase_orderlines/1/edit
  def edit
    @purchase_orderline = PurchaseOrderline.find(params[:id])
  end

  # POST /purchase_orderlines
  # POST /purchase_orderlines.json
  def create
    @purchase_orderline = PurchaseOrderline.new(strong_params)

    respond_to do |format|
      if @purchase_orderline.save
        format.html { redirect_to @purchase_orderline, notice: 'Purchase orderline was successfully created.' }
        format.json { render json: @purchase_orderline, status: :created, location: @purchase_orderline }
      else
        format.html { render action: "new" }
        format.json { render json: @purchase_orderline.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /purchase_orderlines/1
  # PUT /purchase_orderlines/1.json
  def update
    @purchase_orderline = PurchaseOrderline.find(params[:id])

    respond_to do |format|
      if @purchase_orderline.update_attributes(strong_params)
        format.html { redirect_to @purchase_orderline, notice: 'Purchase orderline was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @purchase_orderline.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /purchase_orderlines/1
  # DELETE /purchase_orderlines/1.json
  def destroy
    @purchase_orderline = PurchaseOrderline.find(params[:id])
    @purchase_orderline.destroy

    respond_to do |format|
      format.html { redirect_to purchase_orderlines_url }
      format.json { head :no_content }
    end
  end
end
