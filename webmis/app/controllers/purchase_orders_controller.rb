class PurchaseOrdersController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search
  # GET /purchase_orders
  # GET /purchase_orders.json
  def index
    if params[:reset]
      params.delete :search
      params.delete :reset
      params.delete :sort
      params.delete :direction
    end

    conditions = '1=1 '
    conditions_string_ary = []
    conditions_param_values = []

    unless params[:search].blank?
      conditions_string_ary << " and (purchase_orders.purchase_reference LIKE ?)"
      1.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end

      external_contacts = ExternalContact.where(["first_name LIKE ? OR last_name LIKE ?"]+["\%#{params[:search].strip}\%","\%#{params[:search].strip}\%"])
      external_contacts.collect{|ec|ec.id}.each do |ec_id|
        conditions_string_ary << " or (purchase_orders.external_contact_id = ?)"
        conditions_param_values << "#{ec_id}"
      end
    end

    conditions = conditions + conditions_string_ary.join(" ")
    @purchase_orders = PurchaseOrder.available.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])

    if request.xhr?
      render(:partial => "reception", :collection => @purchase_orders)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @purchase_orders }
      end
    end
  end

  # GET /purchase_orders/1
  # GET /purchase_orders/1.json
  def show
    @purchase_order = PurchaseOrder.find(params[:id])
    ts = @purchase_order.date_ordered.strftime('%Y%m%d')

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @purchase_order }
      format.pdf { render :pdf => "purchase_order_#{@purchase_order.purchase_reference}_#{ts}.pdf",
          :margin => { :bottom => 20 },
          :footer => { :html => { :template => '/pdf_footer', :formats => [:html] }},
          :encoding => "utf8"}
    end
  end

  # GET /purchase_orders/new
  # GET /purchase_orders/new.json
  def new
    @purchase_order = PurchaseOrder.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @purchase_order }
    end
  end

  # GET /purchase_orders/1/edit
  def edit
    @purchase_order = PurchaseOrder.find(params[:id])
  end

  # POST /purchase_orders
  # POST /purchase_orders.json
  def create
    @purchase_order = PurchaseOrder.new(strong_params)

    respond_to do |format|
      if @purchase_order.save
        format.html { redirect_to @purchase_order, notice: 'Purchase order was successfully created.' }
        format.json { render json: @purchase_order, status: :created, location: @purchase_order }
      else
        format.html { render action: "new" }
        format.json { render json: @purchase_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /purchase_orders/1
  # PUT /purchase_orders/1.json
  def update
    @purchase_order = PurchaseOrder.find(params[:id])

    respond_to do |format|
      if @purchase_order.update_attributes(strong_params)
        format.html { redirect_to @purchase_order, notice: 'Purchase order was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @purchase_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /purchase_orders/1
  # DELETE /purchase_orders/1.json
  def destroy
    @purchase_order = PurchaseOrder.find(params[:id])
    @purchase_order.destroy

    respond_to do |format|
      format.html { redirect_to purchase_orders_url }
      format.json { head :no_content }
    end
  end

  def change_status
    @purchase_order = PurchaseOrder.find(params[:id])
    @purchase_order.status  = params[:status]

    if @purchase_order.status == 30 # Settled
      @purchase_order.date_paid = Date.today
    end
    if @purchase_order.status == 999 # Deleted
      @purchase_order.remarks += "-- Deleted by #{current_user.full_name} --"
      @purchase_order.deleted = true

       @purchase_order.included_article_license_purchase_orderlines.each do |rel_table|
         rel_table.destroy # Relations needs to be removed.
       end
    end

    if  @purchase_order.save
      flash[:notice] = "Purchase order is changed to status #{@purchase_order.statusstr}"
    else
      flash[:error] =  "Purchase order fails to chance status, and remains in status #{@purchase_order.statusstr}"
    end
    respond_to do |format|
      format.html { redirect_to :back }
      format.json { render json: @purchase_order.errors, status: :unprocessable_entity }
    end
  end


  private
  def sort_column
    params[:sort] || "purchase_orders.purchase_reference"
  end  

  def sort_direction
    params[:direction] || "desc"
  end

  def search
    params[:search] || ""
  end
end
