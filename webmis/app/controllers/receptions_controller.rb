require 'will_paginate/array'
class ReceptionsController < ApplicationController
  before_filter :authorize
  authorize_resource param_method: :strong_params
  layout "application"
  helper_method :sort_column, :sort_direction, :search
 
  def index    
    @favorites = Reception.tagged_with("favorite", :on => :favorites, :owned_by => current_user)
     if params[:reset]
      params.delete :inactive
      params.delete :search
      params.delete :reset
      params.delete :sort
      params.delete :direction
      params.delete :column_search_field
    end

    conditions_string_ary = []
    conditions_param_values = []
    conditions_string_ary << "((receptions.status = 'COMPLETED') OR (receptions.status = 'INPROGRESS' and requested_by IS NULL))"
    unless params[:search].blank?
      q = params[:search].strip
      conditions_string_ary << "(receptions.id = ?
                                OR receptions.purpose LIKE ?
                                OR receptions.storage_location LIKE ?
                                OR receptions.remarks LIKE ?
                                OR receptions.product_id LIKE ?
                                OR receptions.references LIKE ?
                                OR regions.name LIKE ?
                                OR data_types.name LIKE ?
                                OR company_abbrs.company_name LIKE ?
                                OR data_releases.name LIKE ?
                                OR data_releases.release_code LIKE ?
                                OR data_releases.aliases LIKE ?
                                OR tags.name = ?)"
      conditions_param_values << q.to_i # only for id
      11.times do
        conditions_param_values << "%#{q}%"
      end
      conditions_param_values << q # for tags
    end

    if !column_search_query.empty?
      conditions_string_ary << "#{column_search_query['query']}"
      conditions_param_values +=  column_search_query["values"]
    end
    conditions = conditions_string_ary.join( ' AND ' )
    @receptions = Reception.includes([{:data_release => :company_abbr},:region,:data_type,:tags]).where([conditions] + conditions_param_values).references(:data_releases, :company_abbrs, :regions,:data_types,:tags ).order(sort_column + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])
    generate_id_sequence_cookie(@receptions,params[:page])
    
    if request.xhr?
      render(:partial => "reception", :collection => @receptions)
    end
  end

  def requests   
if params[:reset]
      params.delete :inactive
      params.delete :search
      params.delete :reset
      params.delete :sort
      params.delete :direction
      params.delete :column_search_field
    end

    conditions_string_ary = []
    conditions_param_values = []
    conditions_string_ary << "((receptions.status = 'COMPLETED') OR (receptions.status = 'INPROGRESS'))"
    unless params[:search].blank?
      q = params[:search].strip
      conditions_string_ary << "(receptions.id = ?
                                OR receptions.purpose LIKE ?
                                OR receptions.storage_location LIKE ?
                                OR receptions.remarks LIKE ?
                                OR receptions.product_id LIKE ?
                                OR receptions.references LIKE ?
                                OR regions.name LIKE ?
                                OR data_types.name LIKE ?
                                OR company_abbrs.company_name LIKE ?
                                OR data_releases.name LIKE ?
                                OR data_releases.release_code LIKE ?
                                OR data_releases.aliases LIKE ?
                                OR tags.name = ?)"
      conditions_param_values << q.to_i # only for id
      11.times do
        conditions_param_values << "%#{q}%"
      end
      conditions_param_values << q # for tags
    end

    if !column_search_query.empty?
      conditions_string_ary << "#{column_search_query['query']}"
      conditions_param_values +=  column_search_query["values"]
    end
    conditions = conditions_string_ary.join( ' AND ' )
    @receptions = Reception.requests.includes([{:data_release => :company_abbr},:region,:data_type,:tags]).where([conditions] + conditions_param_values).references(:data_releases, :company_abbrs, :regions,:data_types,:tags ).order(sort_column + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])
    generate_id_sequence_cookie(@receptions,params[:page])

    if request.xhr?
      render(:partial => "reception", :collection => @receptions)
    end

  end

  def get_datareleases
    if (params[:sid] && params[:sid] != "")
      @dr = DataRelease.where('data_releases.name is not null and production = 1 and company_abbr_id = ' + params[:sid]).order('updated_at DESC')
    else
      @dr = []
    end
    render :partial => "datarelease_select"
  end 

  def get_areas
    if (params[:id] && params[:id] != "")
	    dr = DataRelease.find(params[:id]) 
	    @areas  = Region.all
    else
      @areas = []
    end
    render :partial => "area_select"
  end 

  def new
    @parents = []
    @reception = Reception.new
    if params[:parent_id]
      @parent_reception  = Reception.find(params[:parent_id])
      @reception.product_id = @parent_reception.product_id
      @reception.area_id = @parent_reception.area_id
      @reception.datasource_id = @parent_reception.datasource_id
      @reception.data_release_id = @parent_reception.data_release_id
      @reception.purpose = @parent_reception.purpose
      @reception.parent_id = @parent_reception.id
    else
      if !params[:data_set_id].blank?
        data_set = DataSet.find(params[:data_set_id])
        @reception.area_id = data_set.region.id
        @reception.data_type_id = data_set.data_type_id
        @reception.data_release_id = data_set.data_release_id
        @reception.purpose = "production"
        @reception.filling_id = data_set.filling_id
        @reception.data_set_id = data_set.id
        @reception.data_set_version = data_set.version
      else
        
      end
    end
    
    @reception.data_medium_id = DataMedium.where('name like "ftp"').first.id if DataMedium.where('name like "ftp"').first
    @reception.reception_date = Time.now.strftime("%d-%m-%Y")
    @reception.removed_yn = false
    @reception.can_be_removed_yn = false

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @reception }
    end
  end

  def new_request
    @reception = Reception.new
    @reception.status = "REQUESTED"
    @reception.purpose = "production"
    @reception.priority = "Medium"
    @reception.removed_yn = false
    @reception.can_be_removed_yn = false
    @parents = []
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @reception }
    end
  end

  def edit
    @reception = Reception.find(params[:id])
    authorize! :edit, @reception
    if params["result_files"]
      @reception.storage_location = params["result_files"]
      @reception.remarks = @reception.remarks.to_s + "\n"  + Time.now.strftime("%Y%m%d") + ': Conversion result updated.'
    end
     
    
    @parents = @reception.parents
    if @reception.status == "REQUESTED"
      @reception.status = "INPROGRESS"
      @reception.save
    end

    begin
      @a = Reception.where("id in (select MIN(id) from receptions where id > "+@reception.id.to_s+")").first
      if @a
        @next = @a.id
        @result_next = true
      end
    rescue => error
      @result_next = false
    end

    begin
      @b = Reception.where( "id in (select MAX(id) from receptions where id < "+@reception.id.to_s+")").first
      if @b
        @previous = @b.id
        @result_previous = true
      end
    rescue => error
      @result_previous = false
    end
  end

  def set_inprogress
    @reception = Reception.find(params[:id])
    authorize! :set_inprogress, @reception
    if @reception.status.to_s ==  "REQUESTED"
      @reception.status = "INPROGRESS"
      @reception.save
      flash[:notice] = "Status was successfully updated "
    else
      flash[:error] = "Status update failed, status already at #{@reception.status.to_s}"
    end
    #    if params[:popup]
    #        redirect_to :action => 'show_request', :id => params[:id], :layout => false, :popup => true
    #    else
    #        redirect_to :action => 'show_request', :id => params[:id]
    #    end
    
    respond_to do |format|
      if params[:popup]
        format.html { render :action => "show_request", :layout => false, :id => params[:id] , :popup => true}
      else
        format.html { redirect_to :action => "show_request", :id => params[:id]}
      end
      format.xml  { render :xml => @reception}
    end
  end

  def reject
    @reception = Reception.find(params[:id])
    authorize! :reject, @reception
    if @reception.status != "COMPLETED"
      @reception.status = "REJECTED"
      @reception.rejection_remark = params[:reception][:rejection_remark]
      @reception.save
      flash[:notice] = "Reception request #{params[:id]} is rejected"
      Postoffice.reject_reception(@reception.id).deliver_now
    end
    redirect_to({:action => 'requests'},{:notice => "Reception request #{params[:id]} is rejected."})
  end

  def create
    @parents = []  
    @inbound_order = InboundOrder.new
    Reception.transaction do
      if !params[:reception][:data_release_id] && request.path.scan(/\.xml$/)[0]
        dr  = DataRelease.new
        dr.name = params[:reception][:release_name]
        dr.release_code =params[:reception][:release_name]
        params[:reception].delete(:release_name)
        dr.company_abbr_id = params[:reception][:company_abbr_id]
        params[:reception].delete(:company_abbr_id)
        dr.save
        params[:reception][:data_release_id] = dr.id
      end

      @reception = Reception.new(strong_params)
      @reception.status = "INPROGRESS"

      authorize! :create, @reception
      if params[:parent_link_id]
        parent_rec = ParentReception.new
        parent_rec.parent_reception_id = params[:parent_link_id]
        parent_rec.type = "Child"
        @reception.parent_receptions << parent_rec
      end

      respond_to do |format|
        update_order(@reception,@inbound_order)
        @reception.inbound_order_id = @inbound_order.id
        if @reception.save && @inbound_order.save
          create_lines(@reception)
          @reception.referenceid = @reception.id

          @reception.tag_list = "#{params[:free_tags]}"
          @reception.save
          @reception.reload
          DesignTag.add_design_tags(@reception)

          if params[:send_mail]
            format.html { redirect_to :action => :mail, :id => @reception.id }
          else
            flash[:notice] = 'Reception was successfully created.'
            format.html { redirect_to(receptions_url) }
            format.xml  { render :xml => @reception, :status => :created, :location => @reception }
          end
        else
          format.html { render :action => "new" }
          format.xml  { render :xml => @reception.errors, :status => :unprocessable_entity }
        end
      end
    end
  end

  def create_request
    @reception = Reception.new(strong_params)
    @parents = []
    relating = false
    
    if params[:commit] == 'Relate previous receptions'
      relating = true
      @reception.area_id = Region.find_all_for_module("WebMIS").first.id if !@reception.area_id
      @reception.data_medium_id = DataMedium.first if !@reception.data_medium_id
      @reception.data_release_id = DataRelease.where("(production = 1) and (data_releases.name is not null)").last if !@reception.data_release_id
    end
    
    respond_to do |format|
      @reception.requested_by = current_user.first_name + " " + current_user.last_name
      @reception.status = 'REQUESTED'
      if @reception.save
        @reception.referenceid = @reception.id
        if params[:relations]
          params[:relations].each do |relation|
            relation_id = relation[0]
            relation_type = relation[1]
            parent = @reception.parents.new
            parent.parent_reception_id = relation_id
            parent.reception_id = @reception.id
            parent.type = relation_type
            parent.save
          end
        end
        @reception.save
        
        if !relating 
          Postoffice.reception_request(@reception.id).deliver
          flash[:notice] = 'Reception request was succesfully created.'
          format.html { redirect_to(:action => 'requests') }
          format.xml  { render :xml => @reception, :status => :created, :location => @reception }
        else
          format.html { redirect_to(:action => 'edit_request', :id => @reception.id, :relate => 1) }
          format.xml  { render :xml => @reception, :status => :created, :location => @reception }
        end  
      else
        format.html { render :action => "new_request" }
        format.xml  { render :xml => @reception.errors, :status => :unprocessable_entity }
      end
    end
  end

  def update
    @reception = Reception.find(params[:id])
    @reception.assign_attributes(strong_params)
    @reception.status = "INPROGRESS"
    
    if @reception.removed_yn_changed? && @reception.removed_yn?
      @reception.removed_by_username = current_user.user_name
    end
    
    if @reception.can_be_removed_yn_changed? && @reception.can_be_removed_yn?
      @reception.removal_mark_by_username = current_user.user_name
    end
    
    Reception.transaction do
      if (@reception.inbound_order_id)
        @inbound_order = InboundOrder.find(@reception.inbound_order_id)
        update_order(@reception,@inbound_order)
      end
      respond_to do |format|
        @reception.reception_lines.each do |line|
          line.destroy
        end
        if !@reception.storage_location.blank? && @reception.save
          create_lines(@reception)
          
          @reception.tag_list = "#{params[:free_tags]}"
          @reception.reception_date = Time.now.strftime("%d-%m-%Y")
          @reception.save
          @reception.reload
          DesignTag.add_design_tags(@reception)

          if params[:send_mail]
            format.html { redirect_to :action => :mail, :id => @reception.id}
          else
            format.html { redirect_to(receptions_url,{:notice => "Reception was successfully updated."}) }
          end
          format.xml  { head :ok }
        else
          if @reception.storage_location.blank?
            flash[:error] = 'storage location cannot be empty!'
            format.html { redirect_to(:back) }
          else
          format.html { render :action => "edit" }
          end
          format.xml  { render :xml => @reception.errors, :status => :unprocessable_entity }
        end
      end
    end
  end

  def destroy
    @reception = Reception.find(params[:id])
    @reception.destroy

    respond_to do |format|
      format.html { redirect_to(receptions_url) }
      format.xml  { head :ok }
    end
  end

  def create_lines(reception)
    linearr = Array.new
    if reception.storage_location
      linearr = reception.storage_location.split("\n")
    end
    linearr.each do |line|
      split = line.scan(/(.*)\/(.*)$/)
      if split[0]
        r = ReceptionLine.new
        r.reception_id = reception.id
        r.path = split[0][0].chomp.strip
        r.file = split[0][1].chomp.strip
        r.save
      end
    end
  end

  def update_order(reception,inbound_order) 
    inbound_order.orderref = params[:reception][:referenceid]
    inbound_order.data_release_id = @reception.data_release_id
    inbound_order.save
  end

  def mail
    resend = false
    r = Reception.find(params[:id])
    if params[:rs] || r.send_status
      resend = true
    end
    if r.storage_location && r.storage_location.size > 0
      r.status = "COMPLETED"
    else
      nomail = true
    end

    # if !r.send_status
    r.send_status = 1
    r.senddate = Time.now
    r.user_name = current_user.first_name + " " + current_user.last_name
    r.save

    addresses = Postoffice.confirm_reception(params[:id],resend).deliver_now

    if (r.data_intake_requested == true) || (r.data_intake_requested == 1)
      Postoffice.data_intake_request(r.id).deliver_now
    end
    if r.validation_requested == 1
      Postoffice.validation_request(r.id).deliver_now
    end
    r.sendto =  addresses.to_s.scan(/([A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4})/i).uniq.join(',') # extract email addresses from mail object
    r.save
    if nomail
      redirect_to({ :action => 'index'}, {:flash => {:error => "No files registered. Notification mail not sent."}} )
    else
      redirect_to({ :action => 'index'}, {:flash => {:notice => "Notification mail(s) are sent."}} )
    end
  end
 
  def show
    @reception = Reception.find(params[:id])
    region_name = ''
    region_name = @reception.region.name.to_s if @reception.region
    @page_title = "Receptions " + @reception.id.to_s + " " + region_name
    @parents = @reception.related
    @rel_conversions = @reception.used_in_conversions.collect {|c| [c.id,c.productid] }
    @favorite = false
    if @reception.owner_tags_on(current_user, :favorites).size > 0
      @favorite = true
    end
    
    if params[:tag_list]
      @reception.update_attributes
    end

    respond_to do |format|
      if params[:popup]
        format.html { render :action => "show", :layout => "popup" }
      else
        format.html { render :action => "show"}
      end
      format.xml  { render :xml => @reception}
    end
  end

  def show_request
    @reception = Reception.find(params[:id])
    @dir_advice = @reception.storage_location_path
    
    respond_to do |format|
      if params[:popup]
        format.html { render :action => "show_request", :layout => 'popup' }
      else
        format.html { render :action => "show_request" }
      end
      format.xml  { render :xml => @reception}
    end
  end
  
  def reject_request
    @reception = Reception.find(params[:id])
    
    respond_to do |format|
      format.html { render :action => "reject_request" }
      format.xml  { render :xml => @reception}
    end
  end

  def edit_request
    @reception = Reception.find(params[:id])
    @parents = @reception.parents

    respond_to do |format|
      format.html { render :action => "edit_request" }
      format.xml  { render :xml => @reception}
    end
  end

  def update_request
    @reception = Reception.find(params[:id])
    if @reception.update_attributes(strong_params)
      if !@reception.parents.empty?
        @reception.parents.each do |r|
          r.destroy
        end
      end
      if params[:relations]
        params[:relations].each do |relation|
          relation_id = relation[0]
          relation_type = relation[1]
          parent = @reception.parents.new
          parent.parent_reception_id = relation_id
          parent.reception_id = @reception.id
          parent.type = relation_type
          parent.save
        end
      end
      
      Postoffice.reception_request(@reception.id).deliver
      flash[:notice] = 'Reception request was updated and mails were sent'
      if params[:relate]
        flash[:notice] = 'Reception request with relations is succesfully created.<br>Request is sent by e-mail.'
      end
      
      redirect_to :action => 'requests'
    else
      render :action => "edit_request", :id => params[:id]
    end
  end

  def add_parent
    @reception = Reception.find(params[:id])
    @parent = Reception.where("id = #{params[:parent_id]}").first
    if @parent
      pr = ParentReception.where("reception_id = #{params[:id]} and parent_reception_id = #{params[:parent_id]}").first
      if !pr && params[:parent_id] != params[:id]
        parent_rec = ParentReception.new
        parent_rec.reception_id = params[:id]
        parent_rec.parent_reception_id = params[:parent_id]
        parent_rec.type = params[:rel_type]
        @reception.parent_receptions << parent_rec
        @reception.save
      else
        if params[:parent_id] == params[:id]
          @message = "You can't relate to this reception."
        else
          @message = "Relation already exists."
        end
      end
    else
      @message = "Reception not found"
    end
    @parents = @reception.parents
    if params[:full] 
      if !params[:relate]
        if params[:return_action]
          redirect_to :action => params[:return_action], :id => params[:id]
        else
          redirect_to :action => :edit, :id => params[:id]
        end
      else
        if params[:return_action]
          redirect_to :action => params[:return_action], :id => params[:id], :relate => "1"
        else
          redirect_to :action => :edit, :id => params[:id], :relate => "1"
        end
      end
    else  
      # render :nothing => true, :layout => false
      flash.now[:error] = @message if @message
      render :partial => 'show_parent_receptions'
    end
  end

  def delete_parent
    @reception = Reception.find(params[:id])
    p = ParentReception.where("reception_id = #{params[:id]} and parent_reception_id = #{params[:parent_id]}").first
    recid = p.reception_id
    p.destroy
    @parents = Reception.find(recid).parents
    render :partial => 'show_parent_receptions'
  end

  def can_be_removed
    logger.info 'REMOVAL MARK: by ' + current_user.first_name + " " + current_user.last_name
    logger.info '--------------------------------------------'
    @reception = Reception.find(params[:id])
    @reception.can_be_removed_yn = true
    @reception.removed_yn = false if @reception.removed_yn == nil
    @reception.removal_mark_by_username = current_user.user_name
    
    if @reception.save       
      flash[:notice] = "Reception #{@reception.id.to_s} succesfully marked for removal."
    else
      if @reception.persistent_yn
        flash[:error] = "Reception #{@reception.id.to_s} is marked as \"persistent\" and not allowed to be removed."
      else
        flash[:error] = "Marking not possible"
      end
    end
    redirect_to :back  
  end

  def mark_as_removed
    @reception = Reception.find(params[:id])
    if @reception.removal_allowed?
      begin
        @reception.removed_yn = true
        @reception.removed_by_username = current_user.user_name
        @reception.save
        flash[:notice] = "Reception #{@reception.id.to_s} succesfully marked as removed."
      rescue => e
        flash[:error] = "Removal failed: #{e.message}"
      end
    else
      msg_a = ''
      msg_b = ''
      if @reception.can_be_removed_yn == false
        msg_a = "Reception #{@reception.id.to_s} is not marked for removal and therefore not allowed to be removed.<br />"
      end
      if @reception.persistent_yn
        msg_b = "Reception is marked as \"persistent\" and not allowed to be removed."
      end
      flash[:error] = msg_a + msg_b
    end
    redirect_to :back  
  end

  def index_can_be_removed
    @receptions = Reception.where(:can_be_removed_yn => true, :removed_yn => false, :status => 'COMPLETED')
  end
  
  def update_removal
    @rms = params[:rm]
    @receptions = []
    
    @rms.each do |id|
      @receptions << Reception.find(id[0])
    end    

    @collection = []
    @receptions.each do |r|
      scripthash = Hash.new
      scripthash["remark_header"] = "# " + r.id.to_s + " " + r.data_release.company_abbr.ms_abbr_3 + " " + r.region.name if r.region && r.data_release
      scripthash["removal_dir"] = r.storage_location.split("\n").collect {|s|s.scan(/\A.*_\d{8}\//i)}.flatten
      @collection << scripthash
    end
    

    @receptions.each do |r|
      r.removed_yn = true
      r.removed_by_username = current_user.user_name
      r.save
    end
  end
  
  def remove_tag
    @reception = Reception.find(params[:id])
    current_user.tag(@reception, :with => "", :on => :favorites)
    @favorite = false
    redirect_to :back, :notice => "Reception #{@reception.id} removed from your favorites."
  end
  
  def favorite
    @reception = Reception.find(params[:id])
    current_user.tag(@reception, :with => "favorite", :on => :favorites)
    redirect_to :back, :notice => "Reception #{@reception.id} added to your favorites."   
  end

  def receptions_for_select
    conditions = '1=1'
    conditions_string_ary = []
    conditions_param_values = []

    unless params[:q].blank?
      params[:q].strip.split('+').each do |q|
        conditions_string_ary << " and (receptions.id LIKE ?
              or receptions.status LIKE ?
               or receptions.region_name LIKE ?
                or data_releases.name LIKE ?
                 or data_types.name LIKE ?
                  or company_abbrs.company_name LIKE ?
                   or receptions.purpose LIKE ?
                    or receptions.storage_location LIKE ?
                     or receptions.remarks LIKE ?)"
        9.times do
          conditions_param_values << "\%#{q}\%"
        end
      end
    end
    conditions = conditions + conditions_string_ary.join(" ")
    @receptions_for_select2 = Reception.available.eager_load([{:data_release => :company_abbr},:region,:data_type]).where([conditions] + conditions_param_values).paginate(:per_page => params[:page_limit], :page => params[:page])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @receptions_for_select2.to_json(:include => {:data_release => {:include => :company_abbr},:region => {},:data_type =>{}}) }
    end

  end

  def filling_for_data_type
    @pre_selected_filling_id = nil
    if !params[:id].blank?
      @reception = Reception.find(params[:id])
    else
      @reception = nil
    end
    if !params[:data_type_val].blank?
      @datatype_fillings = DataType.find(params[:data_type_val]).fillings.active.order(:name).collect {|f| [ "#{f.name}","#{f.id}" ] }
    else
      @datatype_fillings = []
    end
    if !params[:pre_filling_id].blank?
      @pre_selected_filling_id = params[:pre_filling_id]
    end
  end

  def render_file_tree
    @reception = Reception.find(params[:id])
    if @reception.storage_location && !@reception.storage_location.split.empty?
      @path = @reception.storage_location.split.first.scan(/^.*\d\/data/i).first
    end
    render_file_browse_modal(@path)
  end

  
  private  
  def sort_column  
    params[:sort] || "receptions.reception_date"  
  end  
    
  def sort_direction  
    params[:direction] || "desc"  
  end
  
  def search
    params[:search] || ""
  end
  
end
