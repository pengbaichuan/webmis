class RegionAliasesController < ApplicationController

  before_filter :authorize
  authorize_resource
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search

  # index not used as this is handled by the regions controller
  def index
    if params[:reset]
      params.delete :q
      params.delete :reset
      
      params[:sort] = 'name'
      params[:direction] = 'asc'
    end
    conditions = "1=1 "
    conditions_string_ary = []
    conditions_param_values = []
    
    unless params[:q].blank?
      conditions_string_ary << " and (regions.name LIKE ? OR regions.country_iso_code LIKE ? or regions.country_iso3_code LIKE ? OR regions.area_code LIKE ?)"
      4.times do
        conditions_param_values << "\%#{params[:q].strip}\%"
      end
    end
    conditions = conditions + conditions_string_ary.join(" ")
    @region_aliases = RegionAlias.includes(:region).where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])
    if @region_aliases.empty? && Region.find_by_name(params[:q].strip)
      @region_aliases = Region.find_by_name(params[:q].strip).region_aliases.includes(:region).paginate(:per_page => 50, :page => params[:page])
    end

    if request.xhr?
      flash.keep[:notice]
      render(:partial => "region_alias", :collection => @region_aliases)
    end      
  end

  def show
    @region_alias = RegionAlias.find(params[:id])
  end

  def new
    @region_alias = RegionAlias.new
    @region_alias.isactive = true
  end


  def edit
    @region_alias = RegionAlias.find(params[:id])
  end

  def create
    @region_alias = RegionAlias.new
    @region_alias.display_type_id = AreaType.where(:name => 'RegionAlias').last.id

    region = Region.find(params[:region_alias][:region_id]) unless params[:region_alias][:region_id].blank?
    @region_alias.region_id = region ? region.id : nil

    @region_alias.name = params[:region_alias][:name]
    @region_alias.isactive = params[:region_alias][:isactive] if params[:region_alias][:isactive]

    respond_to do |format|
      if @region_alias.save
        # the 'method app_module_ids=' must be placed after the save as it will created and save child objects of @region_alias, so @region_alias must also be a saved entity
        @region_alias.app_module_ids=[AppModule.where(:abbr => 'MIS').last.id]
        format.html { redirect_to @region_alias, :notice => 'Region alias was successfully created.' }
        format.xml  { render :xml => @region_alias, :status => :created, :location => @region_alias }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @region_alias.errors, :status => :unprocessable_entity }
      end
    end    
  end

  def update
    @region_alias = RegionAlias.find(params[:id])
    @region_alias.display_type_id = AreaType.where(:name => 'RegionAlias').last.id

    region = Region.find(params[:region_alias][:region_id]) unless params[:region_alias][:region_id].blank?
    @region_alias.region_id = region ? region.id : nil

    @region_alias.name = params[:region_alias][:name]
    @region_alias.isactive = params[:region_alias][:isactive] if params[:region_alias][:isactive]

    respond_to do |format|
      if @region_alias.save
         @region_alias.app_module_ids = [AppModule.where(:abbr => 'MIS').last.id]
        format.html { redirect_to @region_alias, :notice => 'Region alias was successfully updated.' }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @region_alias.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @region_alias = RegionAlias.find(params[:id])
    @region_alias.destroy
    flash[:notice] = "RegionAlias succesfully deleted!"
    respond_to do |format|
      format.html { redirect_to(RegionAliass_url) }
      format.xml  { head :ok }
    end
  end
  
  
  private 
  def sort_column
    params[:sort] || "name"
  end
  
  def sort_direction
    params[:direction] || "asc"
  end
  
  def search
    params[:search] || ""
  end  
end
