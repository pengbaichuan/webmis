class RegionsController < ApplicationController
  before_filter :authorize, :except => [:get_region_id, :dakota_region_ids, :dakota_names, :test_areas]
  authorize_resource :except => [:get_region_id, :dakota_region_ids, :dakota_names, :test_areas]

  layout "webmis"
  helper_method :sort_column, :sort_direction, :search

  def index
    conditions = "(regions.isactive = 1)"
    @inactive = false
    @test_areas_only = false

    if params[:reset]
      params.delete :q
      params.delete :ruby_type
      params.delete :reset

      if params[:inactive] == "1"
        params.delete :inactive
        @inactive = false
      end

      if params[:test_areas_only] == "1"
        params.delete :test_areas_only
        @test_areas_only = false
      end

      params[:sort] = 'regions.name'
      params[:direction] = 'asc'
    end

    conditions_string_ary = []
    conditions_param_values = []

    if ( params[:inactive] && params[:inactive] == "1" )
      @inactive = true
      conditions = "(1=1)"
    end

    ruby_type = params[:ruby_type]
    unless ruby_type.blank?
      conditions_string_ary << " and (ruby_type = ?)"
      conditions_param_values << ruby_type.gsub(' ','_').camelize
    end

    unless params[:q].blank?
      conditions_string_ary << " and (regions.name LIKE ? OR
                                      continent_code LIKE ? OR
                                      country_iso_code LIKE ? OR
                                      country_iso3_code LIKE ? OR
                                      city_iso_code LIKE ? OR
                                      state_code LIKE ? OR
                                      area_code LIKE ? OR
                                      test_area_configurations.name LIKE ? OR
                                      regions.dakota_id = #{params[:q].strip.to_i})"
      8.times do
        conditions_param_values << "\%#{params[:q].strip}\%"
      end
    end

    if ( params[:test_areas_only] && params[:test_areas_only] == "1" )
      @test_areas_only = true
      conditions_string_ary << ( "AND ( test_area_configurations.region_id = regions.id )" )
    end

    conditions = conditions + conditions_string_ary.join(" ")
    @regions = Region.eager_load(:test_area_configurations).where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 75, :page => params[:page])
    generate_id_sequence_cookie(@regions,params[:page])

    if request.xhr?
      flash.keep[:notice]
      render(:partial => "region", :collection => @regions)
    end
  end

  def dakota_region_ids_file
    send_data Region.dakota_region_ids_file, :filename => 'REGION_IDS.TXT'
  end

  def dakota_names_file
    send_data Region.dakota_names_file, :filename => 'NAMES.TXT'
  end

  def test_area_configurations
    @region = Region.find(params[:id])
    @test_area_configurations = @region.test_area_configurations.order("product_line_id,min_x,min_y")
  end

  def render_tac_frm
    @region = Region.find(params[:id])
    if params[:test_area_configuration_id].blank?
      @test_area_configuration = TestAreaConfiguration.new(:region_id => @region.id,:nr_of_test_areas_x=>1,:nr_of_test_areas_y=>1)
    else
      @test_area_configuration = @region.test_area_configurations.where(:id => params[:test_area_configuration_id]).last
    end
  end

  def create_test_area_configuration
    @region = Region.find(params[:id])
    params.delete("action")
    params.delete("controller")
    params.delete("id")
    begin
      @test_area_configuration = TestAreaConfiguration.new(params)
      @test_area_configuration.region_id = @region.id
      @test_area_configuration.save!
    rescue => e
      @errors = e.message
    end
  end

  def update_test_area_configuration
    @region = Region.find(params[:id])
    params.delete("action")
    params.delete("controller")
    params.delete("id")
    begin
      @test_area_configuration = TestAreaConfiguration.find(params[:test_area_configuration_id])
      params.delete("test_area_configuration_id")
      @test_area_configuration.assign_attributes(params)
      @test_area_configuration.region_id = @region.id
      @test_area_configuration.save!
    rescue => e
      @errors = e.message
    end
  end

  def delete_test_area_configuration
    @region = Region.find(params[:id])
    @test_area_configuration = TestAreaConfiguration.find(params[:test_area_configuration_id])
    @test_area_configuration.destroy
    flash[:notice] = "#{@test_area_configuration.name} removed."
    respond_to do |format|
      format.html { redirect_to test_area_configurations_region_path(@region) }
      format.json { head :no_content }
    end
  end

  def download_test_areas_exact_script
    @test_area_configuration = TestAreaConfiguration.find(params[:test_area_configuration_id])
    send_data @test_area_configuration.generate_exact_script,
      :filename => @test_area_configuration.file_name("scr")
  end

  def render_tac_generation
    @region = Region.find(params[:id])
    @test_area_configuration = TestAreaConfiguration.find(params[:test_area_configuration_id])
  end

#merged from backoffice
  def get_region_id
    # Designed based on CVENV method cv_privatelib
    region_id = '-1'
    if !params[:areacode].blank?
      region = Region.find_by_region_code(params[:areacode].strip)
      if region && !region.dakota_id.blank?
        region_id = region.dakota_id.to_s
      end
    end
    render :text => region_id
  end

  def dakota_region_ids
    render :text => Region.dakota_region_ids
  end

  def dakota_names
    render :text => Region.dakota_names
  end

  def test_areas
    txt = ""
    region = Region.find_by_region_code(params[:region_code].strip)
    if !params[:target_platform_name].blank? && region
      tp = TargetPlatform.find_by_name(params[:target_platform_name].strip)
      txt = region.test_area_configurations_string(tp.id) if tp
    else
      txt = region.test_area_configurations_string if region
    end
    txt = "NO TEST AREAS FOUND FOR: #{params[:region_code]} #{params[:target_platform_name]}" if txt == ""
    render :text => txt + "\n"
  end

  private
    def sort_column
      params[:sort] || "regions.name"
    end

    def sort_direction
      params[:direction] || "asc"
    end

    def search
      params[:q] || ""
    end
  
end
