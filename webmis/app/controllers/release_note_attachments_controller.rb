class ReleaseNoteAttachmentsController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search
  
  # GET /release_note_attachments
  # GET /release_note_attachments.json
  def index
    if params[:reset]
      params.delete :search
      params.delete :search_product_category
      params.delete :reset
      
      params.delete :sort
      params.delete :direction
    end

    conditions_string_ary = ["1=1"]
    conditions_param_values = []

    
    s_product_category = params[:search_product_category]
    unless s_product_category.blank?
      conditions_string_ary << ' (release_note_attachments_product_categories.product_category_id = ?) AND
                                 (release_note_attachments_product_categories.removed_yn = 0 OR
                                  release_note_attachments_product_categories.removed_yn IS NULL)'
      conditions_param_values << s_product_category
    end
    
    unless params[:search].blank?
      q = params[:search].strip
      conditions_string_ary << "(release_note_attachments.id = ? OR
                                 release_note_attachments.title LIKE ?)"
      conditions_param_values << q.to_i # for id
      1.times do
        conditions_param_values << "\%#{q}\%"
      end
    end

    conditions = conditions_string_ary.join(' AND ')
    @release_note_attachments = ReleaseNoteAttachment.active.includes(:release_note_attachments_product_categories).references(:release_note_attachments_product_categories).where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])

    if request.xhr?
      render(:partial => "release_note_attachment", :collection => @release_note_attachments)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @release_note_attachments }
      end
    end
  end

  # GET /release_note_attachments/1
  # GET /release_note_attachments/1.json
  def show
    @release_note_attachment = ReleaseNoteAttachment.find(params[:id])
    @product_categories = @release_note_attachment.release_note_attachments_product_categories.active

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @release_note_attachment }
    end
  end

  # GET /release_note_attachments/new
  # GET /release_note_attachments/new.json
  def new
    @release_note_attachment = ReleaseNoteAttachment.new
    @release_note_attachment.active_yn = true

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @release_note_attachment }
    end
  end

  # GET /release_note_attachments/1/edit
  def edit
    @release_note_attachment = ReleaseNoteAttachment.find(params[:id])
    @product_categories = @release_note_attachment.release_note_attachments_product_categories.active
  end

  # POST /release_note_attachments
  # POST /release_note_attachments.json
  def create
    @release_note_attachment = ReleaseNoteAttachment.new(strong_params)

    respond_to do |format|
      if @release_note_attachment.save
        format.html { redirect_to :action => :edit, :id => @release_note_attachment.id, notice: 'Release note attachment was successfully created.' }
        format.json { render json: @release_note_attachment, status: :created, location: @release_note_attachment }
      else
        format.html { render action: "new" }
        format.json { render json: @release_note_attachment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /release_note_attachments/1
  # PUT /release_note_attachments/1.json
  def update
    @release_note_attachment = ReleaseNoteAttachment.find(params[:id])

    respond_to do |format|
      if @release_note_attachment.update_attributes(strong_params)
        format.html { redirect_to @release_note_attachment, notice: 'Release note attachment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @release_note_attachment.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update_line
    @release_note_attachment = ReleaseNoteAttachment.find(params[:id])
    rna_pc = ReleaseNoteAttachmentsProductCategory.find(params[:pcid].join)
    rna_pc.mandatory_yn = params[:mandatory_yn]
    destroyit = false
    if params[:product_category] == ''
      rna_pc.removed_yn = true
      detroyit = true
    else
      rna_pc.product_category_id = params[:product_category]
    end
    
    if rna_pc.save
      if destroyit
        rna_pc.destroy
      end
    else
      @msg = 'Double entry for product category'
    end

    @product_categories = @release_note_attachment.release_note_attachments_product_categories.active
    

      render :layout => false

  end
  
  def new_line
    @release_note_attachment = ReleaseNoteAttachment.find(params[:id])
    rna_pc = ReleaseNoteAttachmentsProductCategory.new
    rna_pc.mandatory_yn = true
    rna_pc.release_note_attachment_id = @release_note_attachment.id
    rna_pc.product_category_id = params[:product_category]
    if rna_pc.save
      #nothing
    else
      @msg = 'Double entry for product category'
    end
    @product_categories = @release_note_attachment.release_note_attachments_product_categories.active
    render :layout => false
  end

  # DELETE /release_note_attachments/1
  # DELETE /release_note_attachments/1.json
  def destroy
    @release_note_attachment = ReleaseNoteAttachment.find(params[:id])
    @release_note_attachment.destroy

    respond_to do |format|
      format.html { redirect_to release_note_attachments_url }
      format.json { head :no_content }
    end
  end
  
  def remove
    @release_note_attachment = ReleaseNoteAttachment.find(params[:id])
    @release_note_attachment.removed_yn = 1
    @release_note_attachment.save
    
    respond_to do |format|
      format.html { redirect_to release_note_attachments_url,notice: 'Release note attachment was successfully removed.' }
      format.json { head :no_content }
    end  
  end
  
  
  private  
  def sort_column  
    params[:sort] || "title"  
  end  
    
  def sort_direction  
    params[:direction] || "asc"  
  end
  
  def search
    params[:search] || ""
  end
  
end
