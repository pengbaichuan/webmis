class ReleaseNoteAttachmentsProductCategoriesController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"
  
  # GET /release_note_attachments_product_categories
  # GET /release_note_attachments_product_categories.json
  def index
    @release_note_attachments_product_categories = ReleaseNoteAttachmentsProductCategory.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @release_note_attachments_product_categories }
    end
  end

  # GET /release_note_attachments_product_categories/1
  # GET /release_note_attachments_product_categories/1.json
  def show
    @release_note_attachments_product_category = ReleaseNoteAttachmentsProductCategory.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @release_note_attachments_product_category }
    end
  end

  # GET /release_note_attachments_product_categories/new
  # GET /release_note_attachments_product_categories/new.json
  def new
    @release_note_attachments_product_category = ReleaseNoteAttachmentsProductCategory.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @release_note_attachments_product_category }
    end
  end

  # GET /release_note_attachments_product_categories/1/edit
  def edit
    @release_note_attachments_product_category = ReleaseNoteAttachmentsProductCategory.find(params[:id])
  end

  # POST /release_note_attachments_product_categories
  # POST /release_note_attachments_product_categories.json
  def create
    @release_note_attachments_product_category = ReleaseNoteAttachmentsProductCategory.new(strong_params)

    respond_to do |format|
      if @release_note_attachments_product_category.save
        format.html { redirect_to @release_note_attachments_product_category, notice: 'Release note attachments product category was successfully created.' }
        format.json { render json: @release_note_attachments_product_category, status: :created, location: @release_note_attachments_product_category }
      else
        format.html { render action: "new" }
        format.json { render json: @release_note_attachments_product_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /release_note_attachments_product_categories/1
  # PUT /release_note_attachments_product_categories/1.json
  def update
    @release_note_attachments_product_category = ReleaseNoteAttachmentsProductCategory.find(params[:id])

    respond_to do |format|
      if @release_note_attachments_product_category.update_attributes(strong_params)
        format.html { redirect_to @release_note_attachments_product_category, notice: 'Release note attachments product category was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @release_note_attachments_product_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /release_note_attachments_product_categories/1
  # DELETE /release_note_attachments_product_categories/1.json
  def destroy
    @release_note_attachments_product_category = ReleaseNoteAttachmentsProductCategory.find(params[:id])
    @release_note_attachments_product_category.destroy

    respond_to do |format|
      format.html { redirect_to release_note_attachments_product_categories_url }
      format.json { head :no_content }
    end
  end
end
