class RoleActionsController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search

  # GET /role_actions
  # GET /role_actions.json
  def index
    if params[:reset]
      params.delete :q
      params.delete :reset

      params[:sort] = 'controller_name'
      params[:direction] = 'asc'
    end
    conditions = "1=1 "
    conditions_string_ary = []
    conditions_param_values = []

    unless params[:q].blank?
      conditions_string_ary << " and (role_actions.controller_name LIKE ? OR role_actions.action_name LIKE ?)"
      2.times do
        conditions_param_values << "\%#{params[:q].strip}\%"
      end
    end
    conditions = conditions + conditions_string_ary.join(" ")

    @role_actions = RoleAction.where([conditions] + conditions_param_values).group(:controller_name).order(sort_column + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])
    if request.xhr?
      render(:partial => "role_action", :collection => @role_actions,:params => params)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @role_actions }
      end
    end
  end

  # GET /role_actions/1
  # GET /role_actions/1.json
  def show
    @role_action = RoleAction.find(params[:id])
    @role_actions = RoleAction.where(:controller_name => @role_action.controller_name).group(:action_name)

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @role_action }
    end
  end

  # GET /role_actions/new
  # GET /role_actions/new.json
  def new
    @role_action = RoleAction.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @role_action }
    end
  end

  # GET /role_actions/1/edit
  def edit
    @role_action = RoleAction.find(params[:id])
    @role_actions = RoleAction.where(:controller_name => @role_action.controller_name).group(:action_name)
  end

  # POST /role_actions
  # POST /role_actions.json
  def create
    @role_action = RoleAction.new(strong_params)
    @role_actions = RoleAction.where(:controller_name => @role_action.controller_name).group(:action_name)

    respond_to do |format|
      if @role_action.save
        format.html { redirect_to :id => @role_action.id,:action => "edit", notice: 'Role action was successfully created.' }
        format.json { render json: @role_action, status: :created, location: @role_action }
      else
        format.html { render action: "new" }
        format.json { render json: @role_action.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /role_actions/1
  # PUT /role_actions/1.json
  def update
    @role_action = RoleAction.find(params[:id])

    params['ra-sel'].each do |k,v|
      old = RoleAction.find(k.split('_').first)
      old.same_action_name.collect{|ra|RoleAction.find(ra).destroy}
      v.each do |role_id|
        role_action = RoleAction.new
        role_action.role_id = role_id
        role_action.controller_name = old.controller_name
        role_action.action_name = old.action_name
        role_action.save
      end
    end
    @role_action = RoleAction.where(:controller_name => @role_action.controller_name,:action_name => @role_action.action_name).last

    respond_to do |format|
      if !@role_action.nil?
        format.html { redirect_to :id => @role_action.id,:action => "edit", notice: 'Role action was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @role_action.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /role_actions/1
  # DELETE /role_actions/1.json
  def destroy
    @role_action = RoleAction.find(params[:id])
    @role_action.destroy

    respond_to do |format|
      format.html { redirect_to role_actions_url }
      format.json { head :no_content }
    end
  end

  def new_line    
    params[:role_ids].each do |role_id|
      role_action = RoleAction.new
      role_action.role_id = role_id
      role_action.controller_name = params[:controller_name]
      role_action.action_name = params[:action_name]
      role_action.save
    end
    @role_actions = RoleAction.where(:controller_name => params[:controller_name]).group(:action_name)
    @role_action = RoleAction.where(:controller_name => params[:controller_name]).first
    respond_to do |format|
      format.js { render :layout => false }
    end  
  end

  def destroy_line
    ids = params[:ids].gsub('ra-destroy-','').split('_')
    ids.each do |id|
      RoleAction.find(id).destroy
    end
    @role_actions = RoleAction.where(:controller_name => params[:controller_name]).group(:action_name)
    @role_action = RoleAction.where(:controller_name => params[:controller_name]).first
    respond_to do |format|
      format.js { render :layout => false }
    end  
  end

  private
  def sort_column
    params[:sort] || "controller_name"
  end

  def sort_direction
    params[:direction] || "asc"
  end

  def search
    params[:search] || ""
  end
end
