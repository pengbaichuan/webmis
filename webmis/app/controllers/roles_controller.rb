class RolesController < ApplicationController
  before_filter :authorize
  authorize_resource param_method: :strong_params
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search
  
  # GET /Roles
  # GET /Roles.xml
  def index
    if params[:reset]
      params.delete :search
      params.delete :reset
      params.delete :sort
      params.delete :direction
    end

    conditions_string_ary = ["1=1"]
    conditions_param_values = []

    unless params[:search].blank?
      q = params[:search].strip
      conditions_string_ary << "(roles.id = ? OR
                                 roles.title LIKE ? OR
                                 users.user_name LIKE ? OR
                                 users.first_name LIKE ? OR
                                 users.last_name LIKE ?)"
      conditions_param_values << q.to_i # for id
      4.times do
        conditions_param_values << "%#{q}%"
      end
    end

    conditions = conditions_string_ary.join(' AND ')
    @roles = Role.includes(:users).where([conditions] + conditions_param_values).references(:users).order(sort_column + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])
    generate_id_sequence_cookie(@roles,params[:page])

    if request.xhr?
      render(:partial => "role", :collection => @roles)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @roles }
      end
    end
  end

  # GET /Roles/1
  # GET /Roles/1.xml
  def show
    @role = Role.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @role }
    end
  end

  # GET /Roles/new
  # GET /Roles/new.xml
  def new
    @role = Role.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @role }
    end
  end

  # GET /Roles/1/edit
  def edit
    @role = Role.find(params[:id])
  end

  # POST /Roles
  # POST /Roles.xml
  def create
    @role = Role.new(strong_params)

    respond_to do |format|
      if @role.save
        flash[:notice] = 'Role was successfully created.'
        format.html { redirect_to(@role) }
        format.xml  { render :xml => @role, :status => :created, :location => @role }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @role.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /Roles/1
  # PUT /Roles/1.xml
  def update
    @role = Role.find(params[:id])

    respond_to do |format|
      if @role.update_attributes(strong_params)
        flash[:notice] = 'Role was successfully updated.'
        format.html { redirect_to(@role) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @role.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /Roles/1
  # DELETE /Roles/1.xml
  def destroy
    @role = Role.find(params[:id])
    @role.destroy

    respond_to do |format|
      format.html { redirect_to(Roles_url) }
      format.xml  { head :ok }
    end
  end

private
  def sort_column
    !params[:sort].blank? ? params[:sort] : "roles.title"
  end

  def sort_direction
    !params[:direction].blank? ? params[:direction] : "asc"
  end

  def search
    params[:search] || ""
  end
end
