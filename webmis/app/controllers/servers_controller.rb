class ServersController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search

  # GET /servers
  # GET /servers.xml
  def index
    if params[:reset]
      params.delete :search
      params.delete :active
      params.delete :scheduling_allowed_yn
      params.delete :down
      params.delete :reset

      params[:projects] = 0
      params.delete :sort
      params.delete :direction
    end

    conditions = '1=1'
    conditions_string_ary = []
    conditions_param_values = []

    unless params[:active].blank?
      conditions_string_ary << " and (servers.active = ?)"
      conditions_param_values << params[:active]
    end

    unless params[:scheduling_allowed_yn].blank?
      conditions_string_ary << " and (servers.scheduling_allowed_yn = ?)"
      conditions_param_values << params[:scheduling_allowed_yn]
    end

    unless params[:down].blank?
      conditions_string_ary << " and (servers.down = ?)"
      conditions_param_values << params[:down]
    end

    unless params[:search].blank?
      conditions_string_ary << " and (servers.server_id LIKE ? OR servers.server_remark LIKE ? or servers.diskcapacity LIKE ? or servers.processor LIKE ? or servers.additionalprocess LIKE ? or projects.name LIKE ?)" 
      6.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
    end
    conditions = conditions + conditions_string_ary.join(" ")

    if params[:projects].to_i == 1
      # all servers allowed for scheduling by the current project
      @servers = Server.allocated_to(session[:project_id]).joins(:owned_by).where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(per_page: 75, page: params[:page])
    elsif params[:projects].to_i == 2
      @servers = Server.joins(:owned_by).where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(per_page: 75, page: params[:page])
    else
      @servers = Server.owned_by(session[:project_id]).joins(:owned_by).where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(per_page: 75, page: params[:page])
    end

    generate_id_sequence_cookie(@servers, params[:page])

    if request.xhr?
      flash.keep[:notice]
      render(partial: "server", collection: @servers)
    end      
  end

  # GET /servers/1
  # GET /servers/1.xml
  def show
    @server = Server.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @server }
    end
  end

  # GET /servers/new
  # GET /servers/new.xml
  def new
    @server = Server.new
    @server.owned_by = Project.default

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @server }
    end
  end

  # GET /servers/1/edit
  def edit
    @server = Server.find(params[:id])
  end

  # POST /servers
  # POST /servers.xml
  def create
    @server = Server.new(strong_params)
    @server.projects = []
    if params[:project_ids].class == Array
      params[:project_ids].each do |project_id|
        @server.projects << Project.find(project_id)
      end
    end

    respond_to do |format|
      if @server.save
        @server.set_job_types(params[:job_types] || [])
        flash[:notice] = 'Server was successfully created.'
        format.html { redirect_to(@server) }
        format.xml  { render :xml => @server, status: :created, location: @server }
      else
        format.html { render action: "new" }
        format.xml  { render xml: @server.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /servers/1
  # PUT /servers/1.xml
  def update
    @server = Server.find(params[:id])
    @server.projects = []
    if params[:project_ids].class == Array
      params[:project_ids].each do |project_id|
        @server.projects << Project.find(project_id)
      end
    end
    @server.set_job_types(params[:job_types] || [])

    respond_to do |format|
      if @server.update_attributes(strong_params)
        flash[:notice] = 'Server was successfully updated.'
        format.html { redirect_to(@server) }
        format.xml  { head :ok }
      else
        format.html { render action: "edit" }
        format.xml  { render xml: @server.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /servers/1
  # DELETE /servers/1.xml
  def destroy
    @server = Server.find(params[:id])
    @server.destroy

    respond_to do |format|
      format.html { redirect_to servers_url, notice: "Server deleted." }
      format.xml  { head :ok }
    end
  end

  def overview
    # default for the visible servers is 'allowed for scheduling'
    params[:project_id] = 1 if !params[:project_id]
    params[:project_id] = params[:project_id].to_i
    params[:projects] = 1 unless params[:projects]

    if params[:conv_id]
      c  = Conversion.find(params[:conv_id].to_s.to_i)
      c.server_name = params[:server_id]
      c.status = 13
      c.status_remark = params[:status_remark]
      c.save
    end

    if params[:projects] == "0"
      # only show the servers owned by the current project
      @servers = Server.owned_by(params[:project_id]).where('active =1').order('server_id')
    elsif params[:projects] == "2"
      # show all servers
      @servers = Server.where('active = 1').order('server_id')
    else
      # the default, params[:projects] == 1, show the servers allowed for scheduling by the current project
      @servers = Server.allocated_to(params[:project_id]).where('active = 1').order('server_id')
    end

    @schedule_servers = Server.allocated_to(params[:project_id]).where('active = 1 and scheduling_allowed_yn = 1').order('server_id')
    @active_unassigned_conversions = Conversion.active_unassigned_conversions(params[:project_id])

    @active_assigned_conversions = Conversion.running
    @queued_jobs = Job.queued

    @active_unassigned_jobs = Job.in_queue.where(project_id:params[:project_id])
    @active_assigned_jobs = Job.on_server + Job.assigned
    @waiting_on_bug_tracker_jobs = Job.waiting_on_bug_tracker.where(project_id:params[:project_id])
  end

  def job_execute
    @job = Job.find(params[:jid])
    @job.perform
    redirect_to action: "overview", :project_id => session[:project_id]
  end

  def job_reset
    @job = Job.find(params[:jid])
    @job.kill
    @job.save
    redirect_to action: "overview", :project_id => session[:project_id]
  end

  def crashed_job_default_action
    @job = Job.find(params[:jid])

    @job.bug_tracker_id = params[:bug_tracker_id]
    unless params[:job_fail_reason].blank?
      @job.fail_category = params[:job_fail_reason]
      @job.fail_category_logged_by_user_id = current_user.id
      @job.fail_category_logged_on = Time.now
    end

    if @job.class == ConversionJob
      if !@job.allow_cleanup?
        # don't save job as there is already a cleanup running
        flash[:warn] = 'Cleanup already in progress or no longer allowed.'
      elsif params[:move_to_crash_dir] == 'true' && !params[:bug_tracker_id].blank?
        begin
          @job.log_action("Moved working directory to #{ConfigParameter.get('conversion_job_crash_dir',session[:project_id])}#{params[:bug_tracker_id]}.",current_user)
          @job.move_to_crashed
          flash[:notice] = 'Moving to crash directory'
        rescue Exception => e
          flash[:error] = e.message
          puts e.message
          puts e.backtrace
        end
      else
        @job.clear_working_dir
        @job.save!
        flash[:notice] = 'Working directory cleared'
      end
    else # @job_class != ConversionJob
      # No default action for a crashed job of this type specified, just clear the working directory
      @job.clear_working_dir
      @job.save!
    end
  end

  def reset
    @conversion = Conversion.find(params[:cid])
    @conversion.server_name = nil
    @conversion.status = 10
    @conversion.save
    redirect_to :action => "overview", :project_id => session[:project_id]
  end

  def update_serverprocess
    server = Server.find(params[:id])
    server.additionalprocess = params[:addprocess]
    server.save
    redirect_to :action => "overview", :project_id => session[:project_id]
  end
  
  def job_assign
    server = Server.find(params[:id])
    job = Job.find(params[:jid])
    job.run_server = server.server_name
    job.save
    # the Job::STATUS_QUEUED should be put in the queue for the server, the others should be started right away
    job.perform unless job.status == Job::STATUS_QUEUED

    redirect_to action: "overview", :project_id => session[:project_id]
  end

  def assign_to_server
    server = Server.find(params[:id])
    conversion = Conversion.find(params[:cid])
    if conversion.production_orderline
      conversion.production_orderline.update_bp_message("Running #{server.server_id}",current_user)
    end
    conversion.server_name = server.server_id
    conversion.status = Conversion::STATUS_CONVERSION
    conversion.save
    
    redirect_to :action => "overview", :project_id => session[:project_id]
  end

  def toggle_shutdown
    @server = Server.find(params[:sid].gsub(/[^\d]/,''))
    if @server.down
      @server.down = false
    else
      @server.down = true
      @server.scheduling_allowed_yn = false
    end

    Server.transaction do
      if @server.save
        release_jobs(@server.server_id) if @server.down
      end
    end

    render :nothing => true
  end

  def toggle_schedule_on_off
    @server = Server.find(params[:sid].gsub(/[^\d]/,''))
    if @server.scheduling_allowed_yn == true
      @server.scheduling_allowed_yn = false
    else
      @server.scheduling_allowed_yn = true
    end
    Server.transaction do
      if @server.save
        release_jobs(@server.server_id) if @server.scheduling_allowed_yn == false
      end
    end

    render :nothing => true
  end

  def refresh_overview    
  end

  private
  def sort_column
    params[:sort] || "server_id"
  end

  def sort_direction
    params[:direction] || "asc"
  end

  def search
    params[:search] || ""
  end

  def release_jobs(server_id)
    @jobs = Job.assigned.where(:run_server => server_id)
    @jobs.each do |job|
      job.run_server = nil
      next if job.save
      job.autoschedule
    end
  end

end

