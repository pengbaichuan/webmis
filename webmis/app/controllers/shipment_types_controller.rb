class ShipmentTypesController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"
  # GET /shipment_types
  # GET /shipment_types.xml
  def index
    @shipment_types = ShipmentType.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @shipment_types }
    end
  end

  # GET /shipment_types/1
  # GET /shipment_types/1.xml
  def show
    @shipment_type = ShipmentType.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @shipment_type }
    end
  end

  # GET /shipment_types/new
  # GET /shipment_types/new.xml
  def new
    @shipment_type = ShipmentType.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @shipment_type }
    end
  end

  # GET /shipment_types/1/edit
  def edit
    @shipment_type = ShipmentType.find(params[:id])
  end

  # POST /shipment_types
  # POST /shipment_types.xml
  def create
    @shipment_type = ShipmentType.new(strong_params)

    respond_to do |format|
      if @shipment_type.save
        flash[:notice] = 'ShipmentType was successfully created.'
        format.html { redirect_to(@shipment_type) }
        format.xml  { render :xml => @shipment_type, :status => :created, :location => @shipment_type }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @shipment_type.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /shipment_types/1
  # PUT /shipment_types/1.xml
  def update
    @shipment_type = ShipmentType.find(params[:id])

    respond_to do |format|
      if @shipment_type.update_attributes(strong_params)
        flash[:notice] = 'ShipmentType was successfully updated.'
        format.html { redirect_to(@shipment_type) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @shipment_type.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /shipment_types/1
  # DELETE /shipment_types/1.xml
  def destroy
    @shipment_type = ShipmentType.find(params[:id])
    @shipment_type.destroy

    respond_to do |format|
      format.html { redirect_to(shipment_types_url) }
      format.xml  { head :ok }
    end
  end
end
