class ShippingJobsController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout 'webmis'
  helper_method :sort_column, :sort_direction, :search

  # GET /shipping_jobs
  # GET /shipping_jobs.json
  def index
    if params[:reset]
      params.delete :search_status
      params.delete :search
      params.delete :sort
      params.delete :direction

      params.delete :sort
      params.delete :direction

      # when from jobs/tagged_failed_jobs_report
      params.delete :date_range_a
      params.delete :date_range_b
      params.delete :fail_category
    end

    conditions_string_ary = ['1=1']
    conditions_param_values = []

    status = params[:search_status]
    unless status.blank?
      conditions_string_ary << '(jobs.status = ?)'
      conditions_param_values << status
    end

    # when from jobs/tagged_failed_jobs_report
    unless params[:fail_category].blank?
      range_end = Date.today.strftime('%d-%m-%Y')
      unless params[:date_range_a].blank?
        range_begin = params[:date_range_a]
        if !params[:date_range_b].blank?
          range_end = params[:date_range_b]
        end
      else
        range_begin = ShippingJob.fail_reason_tagged.first.fail_category_logged_on.strftime('%d-%m-%Y')
      end

      unless params[:date_range_b].blank?
        if params[:date_range_a].blank?
          range_end = Date.today.strftime('%d-%m-%Y')
        end
      end
      conditions_string_ary << "(jobs.fail_category_logged_on BETWEEN '#{DateTime.parse(range_begin)}' AND '#{DateTime.parse(range_end)}')"
      if params[:fail_category] != 'all'
        conditions_string_ary << '(jobs.fail_category = ?)'
        conditions_param_values << params[:fail_category]
      end
    end

    unless params[:search].blank?
      q = params[:search].strip
      conditions_string_ary << '(jobs.id = ? OR
                                 outbound_orders.filename LIKE ? OR
                                 jobs.run_server LIKE ?)'
      conditions_param_values << q.to_i # for id
      2.times do
        conditions_param_values << "%#{q}%"
      end
    end

    conditions = conditions_string_ary.join(' AND ')
    @shipping_jobs = ShippingJob.includes(:outbound_order).references(:outbound_orders).where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(per_page: 30, page: params[:page])

    if request.xhr?
      render partial: 'shipping_job', collection: @shipping_jobs
    else
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @shipping_jobs }
      end
    end
  end

  def failures
    if params[:sort].blank?
      params[:sort] = sort_column
      params[:direction] = sort_direction
    end
    @shipping_jobs = ShippingJob.failures.includes(:outbound_order).order(sort_column + ' ' + sort_direction)
    @shipping_jobs = @shipping_jobs.paginate(per_page: 30, page: params[:page])

    if request.xhr?
      render(partial: 'failed_shipping_job', collection: @shipping_jobs)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @shipping_jobs }
      end
    end
  end

  def tagging_fail_reason
    @success_rows = []
    params[:ids].each do |job_id|
      job = ShippingJob.find(job_id)
      job.fail_category = params[:fail_reason]
      job.fail_category_logged_by_user_id = current_user.id
      job.fail_category_logged_on = Time.now
      if job.save
        @success_rows << job.id
      end
    end
    render layout: false
  end


  # GET /shipping_jobs/1
  # GET /shipping_jobs/1.json
  def show
    @shipping_job = ShippingJob.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @shipping_job }
    end
  end


  # GET /shipping_jobs/new
  def new
    @shipping_job = ShippingJob.new( project_id: session[:project_id] )

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @shipping_job }
    end
  end

  # GET /shipping_jobs/1/edit
  def edit
    @shipping_job = ShippingJob.find(params[:id])
  end

  # POST /shipping_jobs
  # POST /shipping_jobs.json
  def create
    @shipping_job = ShippingJob.new(strong_params('shipping_job'))

    respond_to do |format|
      if @shipping_job.save
        format.html { redirect_to @shipping_job, notice: 'Shipping job was successfully created.' }
        format.json { render :show, status: :created, location: @shipping_job }
      else
        format.html { render :new }
        format.json { render json: @shipping_job.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /shipping_jobs/1
  # PATCH/PUT /shipping_jobs/1.json
  def update
    begin
      ShippingJob.transaction do
        @shipping_job = ShippingJob.find(params[:id])
        server_name = params[:shipping_job][:run_server].blank? ? '-' : params[:shipping_job][:run_server]

        if params[:commit] == 'Execute'
          @shipping_job.stdout_log = "Job is manually executed on server '#{server_name}'."
          @shipping_job.set_status(Job::STATUS_MANUAL)
          perform_now = true
        elsif params[:commit] == 'Schedule'
          @shipping_job.stdout_log = params[:shipping_job][:run_server].blank? ? 'Job is manually queued.' : "Job is manually queued on server '#{server_name}'."
          @shipping_job.set_status(Job::STATUS_QUEUED)
          perform_now = false
        elsif params[:commit] == 'Save'
          @shipping_job.stdout_log = 'Job is saved.'
          @shipping_job.set_status(Job::STATUS_MANUAL)
          perform_now = false
        else
          raise "Unknown submit operation '#{params[:commit]}'."
        end
        @shipping_job.fail_reason = ''
        @shipping_job.log_scheduler("Job manually set to #{@shipping_job.status_str} for server #{server_name}.", current_user)
        @shipping_job.log_action(@shipping_job.stdout_log, current_user)

        respond_to do |format|
          if @shipping_job.update(strong_params('shipping_job'))

            @shipping_job.perform if perform_now

            format.html { redirect_to @shipping_job, notice: 'Shipping job was successfully updated.' }
            format.json { render :show, status: :ok, location: @shipping_job }
          else
            flash[:error] = 'This job was changed while you were editing it. Please try again.'
            format.html { render :edit }
            format.json { render json: @shipping_job.errors, status: :unprocessable_entity }
          end
        end
      end
    end
  end

  # DELETE /shipping_jobs/1
  # DELETE /shipping_jobs/1.json
  def destroy
    @shipping_job = ShippingJob.find(params[:id])
    @shipping_job.destroy
    
    respond_to do |format|
      format.html { redirect_to shipping_jobs_url, notice: 'Shipping job was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def clean_working_dir
    @shipping_job = ShippingJob.find(params[:id])
    @shipping_job.clear_working_dir
    redirect_to action: 'show'
  end

  def cancel
    begin
      @shipping_job = ShippingJob.find(params[:id])
      @shipping_job.lock_version = params[:lock_version]
      @shipping_job.cancel(current_user.full_name)
    rescue ActiveRecord::StaleObjectError => e
      puts e.message
      puts e.backtrace
      flash[:error] = 'This job was changed while you were cancelling it.'
    end
    redirect_to action: 'show'
  end

  def edit_new_copy
    @shipping_job_org = ShippingJob.find(params[:id])
    @shipping_job_org.lock_version = params[:lock_version] unless params[:lock_version].blank?
    if @shipping_job_org.allow_reschedule?
      begin
        ShippingJob.transaction do
          @shipping_job = @shipping_job_org.reschedule(current_user, manual_yn = true)
          redirect_to action: 'edit', id: @shipping_job.id
        end
      rescue ActiveRecord::StaleObjectError
        flash[:error] = 'This job was changed while you were editing it. Please try again.'
        redirect_to action: 'show', id: @shipping_job_org.id
      rescue => e
        flash[:error] = "Error rescheduling: #{e.message}"
        redirect_to action: 'show', id: @shipping_job_org.id
      end
    else
      if @shipping_job_org.busy?
        flash[:error] = 'Editing a new copy of this job is not allowed yet. Please wait till move action is finished.'
      else
        flash[:error] = 'Editing a new copy of this job is no longer allowed. The production orderline it belongs to probably just finished.'
      end
      redirect_to action: 'show', id: current_job.id
    end
  end

  def regenerate
    @shipping_job_org = ShippingJob.find(params[:id])
    @shipping_job_org.lock_version = params[:lock_version] unless params[:lock_version].blank?
    if @shipping_job_org.allow_reschedule?
      begin
        @shipping_job = @shipping_job_org.regenerate(current_user)
      rescue ActiveRecord::StaleObjectError
        flash[:error] = 'This job was changed while you were editing it. Please try again.'
        redirect_to action: 'show', id: @shipping_job_org.id
      rescue => e
        flash[:error] = "Error regenerating : #{e.message}"
        redirect_to action: 'show', id: @shipping_job_org.id
      end
      redirect_to action: 'show', id: @shipping_job.id
    else
      flash[:error] = 'Rescheduling is no longer allowed. The outbound order this job belongs to probably just finished.'
      redirect_to action: 'show', id: @shipping_job_org.id
    end
  end

  def reschedule
    @shipping_job_org = ShippingJob.find(params[:id])
    @shipping_job_org.lock_version = params[:lock_version] unless params[:lock_version].blank?
    if @shipping_job_org.allow_reschedule?
      begin
        @shipping_job = @shipping_job_org.reschedule(current_user)
      rescue ActiveRecord::StaleObjectError
        flash[:error] = 'This job was changed while you were editing it. Please try again.'
        redirect_to action: 'show', id: @shipping_job_org.id
      rescue => e
        flash[:error] = "Error rescheduling : #{e.message}"
        redirect_to action: 'show', id: @shipping_job_org.id
      end
      redirect_to action: 'show', id: @shipping_job.id
    else
      flash[:error] = 'Rescheduling is no longer allowed. The outbound order this belongs to probably just finished.'
      redirect_to action: 'show', id: @shipping_job_org.id
    end
  end

  private
  # Never trust parameters from the scary internet, only allow the white list through.
  def sort_column
    params[:sort] || 'jobs.id'
  end

  def sort_direction
    params[:direction] || 'desc'
  end

  def search
    params[:search] || ''
  end
end
