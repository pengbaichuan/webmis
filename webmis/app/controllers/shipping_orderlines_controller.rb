class ShippingOrderlinesController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"

  class MissingField < StandardError
  end

  rescue_from MissingField, :with => :missing_field
  helper_method :sort_column, :sort_direction, :search
  def missing_field
    flash[:notice] = "Required field is empty."
    redirect_to :back
  end

  # GET /shipping_orderlines
  # GET /shipping_orderlines.xml
  def index
    conditions = '1=1'
     if params[:reset]
      params.delete :search
      params.delete :reset

      params.delete :sort
      params.delete :direction
      params[:removed] = false
    end

    if params[:search] && params[:search] != ""
      q = params[:search].strip
      conditions = "#{conditions} and (shipping_orderlines.extern_reference like ?
                                           or shipping_orderlines.call_off_reference like ?
                                            or articles.customer_reference like ?
                                             or customer_depots.label like ?
                                              or shipping_orders.po_reference like ?
                                               or transport_reference like ?
                                                or invoice_reference like ?
                                                 or customer_depots.name like ?)"
    end
    @shipping_orderlines = ShippingOrderline.eager_load(:ordering_customer_depot,[:shipping_order,:article]).where(conditions,"%#{q}%",
      "%#{q}%",
       "%#{q}%",
        "%#{q}%",
          "%#{q}%",
            "%#{q}%",
             "%#{q}%",
              "%#{q}%").order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])

    generate_id_sequence_cookie(@shipping_orderlines,params[:page])

    if request.xhr?
     render(:partial => "shipping_orderline", :collection => @shipping_orderlines)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @shipping_orderlines }
      end
    end
  end

  # GET /shipping_orderlines/1
  # GET /shipping_orderlines/1.xml
  def unpaid_shipped_list
    conditions = 'shipping_orderlines.status=60'
    if params[:reset]
      params.delete :search
      params.delete :reset

      params[:sort] = 'shipping_orderlines.payment_due_date'
      params[:direction] = 'desc'
      params[:removed] = false
    end

    if params[:search] && params[:search] != ""
      q = params[:search].strip
      conditions = "#{conditions} and (shipping_orderlines.extern_reference like ?
                                           or shipping_orderlines.call_off_reference like ?
                                            or articles.customer_reference like ?
                                             or customer_depots.label like ?
                                              or shipping_orders.po_reference like ?
                                               or transport_reference like ?
                                                or invoice_reference like ?
                                                 or customer_depots.name like ?)"
    end
    @shipping_orderlines = ShippingOrderline.joins(:ordering_customer_depots).eager_load([:shipping_order,:article]).where(conditions,"%#{q}%",
      "%#{q}%",
       "%#{q}%",
        "%#{q}%",
          "%#{q}%",
            "%#{q}%",
             "%#{q}%",
              "%#{q}%").order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])


    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @shipping_orderlines }
    end
  end

  def process_unpaid_to_paid
    message = ""
    if params[:unpaid_orderline]
      params[:unpaid_orderline].each do |line|
         @shipping_orderline = ShippingOrderline.find(line[0])
         message = message + "#{@shipping_orderline.extern_reference} set from #{@shipping_orderline.status_str} to Paid.<br>"
         @shipping_orderline.status = 70
         @shipping_orderline.save
      end
    end
    flash[:notice] = message
    redirect_to :action => "unpaid_shipped_list",:controller => "shipping_orderlines"

  end

  def paid
    @shipping_orderline = ShippingOrderline.find(params[:id])
    respond_to do |format|
        @shipping_orderline.status = 70
        @shipping_orderline.save
        flash[:notice] = "#{@shipping_orderline.extern_reference} set to paid."
        format.html { redirect_to :back }
        format.xml  { head :ok }
    end
  end

  def uninvoiced_shipped_list
    conditions = 'shipping_orderlines.status=40 and shipping_orderlines.invoice_id is null'
    if params[:reset]
      params.delete :search
      params.delete :reset

      params[:sort] = 'shipping_orderlines.updated_at'
      params[:direction] = 'desc'
      params[:removed] = false
    end

    if params[:search] && params[:search] != ""
      q = params[:search].strip
      conditions = "#{conditions} and (shipping_orderlines.extern_reference like ?
                                           or shipping_orderlines.call_off_reference like ?
                                            or articles.customer_reference like ?
                                             or customer_depots.label like ?
                                              or shipping_orders.po_reference like ?
                                               or transport_reference like ?
                                                or invoice_reference like ?
                                                 or customer_depots.name like ?)"
    end
    @shipping_orderlines = ShippingOrderline.joins(:ordering_customer_depots).eager_load([:shipping_order,:article]).where(conditions,"%#{q}%",
      "%#{q}%",
      "%#{q}%",
      "%#{q}%",
      "%#{q}%",
      "%#{q}%",
      "%#{q}%",
      "%#{q}%").order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])

    if request.xhr?
      render(:partial => "shipping_orderline_uninvoiced", :collection => @shipping_orderlines)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @shipping_orderlines }
      end
    end
  end

  def call_of_list
    @t = Time.now + 14.day

    @shipping_orderlines = ShippingOrderline.allocated

    respond_to do |format|
      format.html # call_of_list.html.erb
      format.xml  { render :xml => @shipping_orderlines }
    end
  end

  def show
    @shipping_orderline = ShippingOrderline.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @shipping_orderline }
    end
  end

  # GET /shipping_orderlines/new
  # GET /shipping_orderlines/new.xml
  def new
    @shipping_orderline = ShippingOrderline.new
    @shipping_orderline.shipping_order_id = params['shipping_order_id']
    @shipping_order = ShippingOrder.find(@shipping_orderline.shipping_order_id)
    @shipping_orderline.article_id = ShippingOrder.find(params['shipping_order_id']).article_id
    @shipping_orderline.status = 0

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @shipping_orderline }
    end
  end

  def new_retroaction
    @shipping_orderline = ShippingOrderline.new
    @shipping_orders = ShippingOrder.order('id DESC')
    @shipping_orders_select = []
    @shipping_orders.each do |so|
      non_stockable_str = ""
      non_stockable_str = "(non stockable)" if so.article.non_stockable
      label = "#{so.po_reference} | #{so.shipping_customer_depot.label} | #{so.article.customer_reference} #{non_stockable_str}"
      @shipping_orders_select << [label,so.id]
    end
  end

  def start_retroaction
    if params[:shipping_order_id] != ""
    @shipping_orderline = ShippingOrderline.new
    @shipping_order = ShippingOrder.find(params[:shipping_order_id])
    @shipping_orderline.shipping_order_id = @shipping_order.id
    @shipping_orderline.article_id = @shipping_order.article_id
    @shipping_orderline.delivery_date = Time.now

    @shipping_orderline.status = 10
    end
    render :partial => "start_retroaction"
  end

  def perform_retroaction
    if params[:shipping_orderline][:amount_requested].blank? || params[:shipping_orderline][:transport_reference].blank? || params[:shipping_orderline][:extern_reference].blank?
      raise MissingField
    end

    d = Time.now
    ds = d.strftime("%y%W%m%d")
    em = ''
    begin
      ShippingOrderline.transaction do
        @shipping_orderline = ShippingOrderline.new(strong_params)
        @shipping_orderline.call_off_reference = "#{@shipping_orderline.shipping_order_id}SO#{@shipping_orderline.id}#{ds}"
        @shipping_orderline.status = 10
        @shipping_orderline.amount = @shipping_orderline.amount_requested
        if @shipping_orderline.save && @shipping_orderline.allocate
          # @shipping_orderline.allocate
          @shipping_orderline.status = 40
        end
      end
    rescue => e
      @shipping_orderline.destroy
      em = e.message
    end
    respond_to do |format|
      if !@shipping_orderline.destroyed? && @shipping_orderline.save

        format.html {  redirect_to :action => "edit",:controller => "shipping_orders", :id => @shipping_orderline.shipping_order.id }
        format.xml  { render :xml => @shipping_orderline, :status => :created, :location => @shipping_orderline }
      else
        flash[:error] = "Cannot create shipping orderline" + '<br />' +em
        @shipping_orders = ShippingOrder.order('id DESC')
        @shipping_orders_select = []
        @shipping_orders.each do |so|
          non_stockable_str = ""
          non_stockable_str = "(non stockable)" if so.article.non_stockable
          label = "#{so.po_reference} | #{so.shipping_customer_depot.label} | #{so.article.customer_reference} #{non_stockable_str}"
          @shipping_orders_select << [label,so.id]
        end
        format.html { render :action => "new_retroaction" }
        format.xml  { render :xml => @shipping_orderline.errors, :status => :unprocessable_entity }
      end
    end

  end

  # GET /shipping_orderlines/1/edit
  def edit
    @shipping_orderline = ShippingOrderline.find(params[:id])
  end

  # POST /shipping_orderlines
  # POST /shipping_orderlines.xml
  def create
    @shipping_orderline = ShippingOrderline.new(strong_params)
    @shipping_orderline.status = 10
    respond_to do |format|
      if @shipping_orderline.save
        format.html {  redirect_to :action => "edit",:controller => "shipping_orders", :id => @shipping_orderline.shipping_order.id }
        format.xml  { render :xml => @shipping_orderline, :status => :created, :location => @shipping_orderline }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @shipping_orderline.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /shipping_orderlines/1
  # PUT /shipping_orderlines/1.xml
  def update
    @shipping_orderline = ShippingOrderline.find(params[:id])

    respond_to do |format|
      if @shipping_orderline.update_attributes(strong_params)
        format.html { redirect_to :action => "edit",:controller => "shipping_orders", :id => @shipping_orderline.shipping_order.id }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @shipping_orderline.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /shipping_orderlines/1
  # DELETE /shipping_orderlines/1.xml
  def destroy_me
    @shipping_orderline = ShippingOrderline.find(params[:id])

    if @shipping_orderline.status < 30

        if @shipping_orderline.destroy
          flash[:notice] = "#{@shipping_orderline.extern_reference} is deleted."
          flash.keep
        end
    end
    flash[:notice] = "#{@shipping_orderline.extern_reference} has not the correct status for removal."
          redirect_to :action => "edit",:controller => "shipping_orders", :id => params[:shipping_order_id]


  end

  def allocate
    if params[:return_to]
     @return_to = params[:return_to]
    end
    @shipping_orderline = ShippingOrderline.find(params[:id])
    @shipping_orderline.shipping_order_id = params[:shipping_order_id]
    d = Time.now
    ds = d.strftime("%y%W%m%d")
    @shipping_orderline.call_off_reference = "#{@shipping_orderline.shipping_order_id}SO#{@shipping_orderline.id}#{ds}"
  end

  def allocation
    @shipping_orderline = ShippingOrderline.find(params[:id])

    respond_to do |format|
      if @shipping_orderline.update_attributes(strong_params)
        @shipping_orderline.allocate
        if params[:return_to]
          format.html { redirect_to :action => params[:return_to],:controller => "shipping_orderlines", :id => @shipping_orderline.shipping_order.id }
        else
            format.html { redirect_to :action => "edit",:controller => "shipping_orders", :id => @shipping_orderline.shipping_order.id }
        end
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @shipping_orderline.errors, :status => :unprocessable_entity }
      end
    end
  end

  def deallocate
    @shipping_orderline = ShippingOrderline.find(params[:id])
    @shipping_orderline.call_off_reference = nil
    @shipping_orderline.save
    @shipping_orderline.deallocate

    redirect_to :action => "edit",:controller => "shipping_orders", :id => @shipping_orderline.shipping_order.id
  end

  def ship
    @shipping_orderline = ShippingOrderline.find(params[:id])
    @shipping_orderline.shipping_order_id = params[:shipping_order_id]
  end

  def shipping
    @shipping_orderline = ShippingOrderline.find(params[:id])

    respond_to do |format|
      if @shipping_orderline.update_attributes(strong_params)
        @shipping_orderline.status = 40
        @shipping_orderline.save

        format.html { redirect_to :action => "edit",:controller => "shipping_orders", :id => @shipping_orderline.shipping_order.id }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @shipping_orderline.errors, :status => :unprocessable_entity }
      end
    end
  end

  def invoice
    @shipping_orderline = ShippingOrderline.find(params[:id])

    params[:suffix] = "WM"
    asset = AssetType.find_by_name("Invoice")
    @nextnumber = asset.get_next_number(params)
    @shipping_orderline.invoice_reference = @nextnumber
    asset = Asset.new
    asset.ref_id = @nextnumber
    asset.requested_by = current_user.first_name + " " + current_user.last_name
    asset.remarks = "Generated via shipping order #{params[:id]}."
    asset.assettype = "Invoice"
    asset.title = "#{@shipping_orderline.shipping_order.po_reference}_#{@shipping_orderline.shipping_order.ordering_customer_depot.name}"
    asset.suffix = params[:suffix]
    asset.save

  end

  def create_invoice
    @shipping_orderline = ShippingOrderline.find(params[:id])
    respond_to do |format|
    if @shipping_orderline.update_attributes(strong_params)
      @shipping_orderline.status = 60 #invoiced

      if @shipping_orderline.shipping_order.invoicing_customer_depot.payment_term.to_i > 0
        term_days = @shipping_orderline.shipping_order.invoicing_customer_depot.payment_term
      else
        term_days = 30
      end
      @shipping_orderline.payment_due_date = (Date.current + term_days.days)


      flash[:notice] = "Shipping order set to status invoiced.<br>Now you are able print the invoice."
      @shipping_orderline.save

      format.html { redirect_to :back }
      format.xml  { head :ok }
    else
      format.html { render :action => "invoice" }
      format.xml  { render :xml => @shipping_orderline.errors, :status => :unprocessable_entity }

    end
    end
  end

  def invoice_from_list
    @shipping_orderline = ShippingOrderline.find(params[:id])
    @shipping_orderline.status = 60
    @shipping_orderline.save
    message = "#{@shipping_orderline.extern_reference} set to Invoiced.<br>"
    flash[:notice] = message
    redirect_to :back
  end

  def html_to_pdf_invoice
    @shipping_orderline = ShippingOrderline.find(params[:id])
    @shipping_order = ShippingOrder.find(@shipping_orderline.shipping_order_id)
    @wstock = WarehouseStock.find_by_shipping_orderline_id(@shipping_orderline.id)
    @cu = current_user
    @mapscape = CustomerDepot.where('label like "%mapscape%" and city like "eindhoven"').first

    @s_total_i = (@shipping_orderline.amount * @shipping_orderline.article.price)

      @percentage_i = 0
      @percentage_i = @shipping_order.shipping_customer_depot.vat_percentage if !@shipping_order.shipping_customer_depot.vat_percentage.nil?

      @vat_i = (@s_total_i / 100 * @percentage_i)
      @vat_desc =  @shipping_order.shipping_customer_depot.vat_statement

    @total_i = (@s_total_i + @vat_i)

    @imagepath = 'http://' + ENV["host"] + "/webmis/images/mapscapelogo4docs.png" # Needed for working with PDFKit
    t = Time.now
    ts = t.strftime('%Y%m%d')

    respond_to do |format|
          format.html
          format.pdf do
            render :pdf => "#{@shipping_orderline.invoice_reference}_#{ts}",
              :margin => { :bottom => 20 },
              :footer => { :html => { :template => '/pdf_footer', :formats => [:html] }},
              :encoding => "utf8"
          end
    end
  end

  def html_to_pdf_call_of
    @shipping_orderline = ShippingOrderline.find(params[:id])
    @shipping_order = ShippingOrder.find(@shipping_orderline.shipping_order_id)
    @wstock = WarehouseStock.find_by_shipping_orderline_id(@shipping_orderline.id)

    @cu = current_user
    @imagepath = 'http://' + ENV["host"] + "/webmis/images/mapscapelogo4docs.png" # Needed for working with PDFKit

    html = render :layout => false
    kit = PDFKit.new(html, :page_size => 'A4', :margin_top    => '0.5in', :disable_smart_shrinking => false,
      :footer_html => 'http://' + ENV["host"] + "/webmis/pdf_footer.html")

    kit.stylesheets << Rails.root.join('public','stylesheets','pdf.css').to_s
    t = Time.now
    ts = t.strftime('%Y%m%d')
    send_data(kit.to_pdf, :filename => "#{@shipping_orderline.call_off_reference}_#{ts}.pdf", :type => 'application/pdf')
    flash[:notice] = "Stock order #{@shipping_orderline.id} printed."
  end

private
  def sort_column
    params[:sort] || "shipping_orderlines.delivery_date"
  end

  def sort_direction
    params[:direction] || "desc"
  end

  def search
    params[:search] || ""
  end

end
