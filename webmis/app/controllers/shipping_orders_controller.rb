class ShippingOrdersController < ApplicationController
  before_filter :authorize
  authorize_resource

  layout 'application'
  helper_method :sort_column, :sort_direction, :search

  # GET /shipping_orders
  # GET /shipping_orders.xml
  def index
    @search_opts = {}
    if params[:reset]
      params.delete :search
      params.delete :reset
      params.delete :sort
      params.delete :direction
      params.delete :column_search_field
    end

    conditions_string_ary = []
    conditions_param_values = []

    unless params[:search].blank?
      q = params[:search].strip
      conditions_string_ary << '(shipping_orders.id = ?
                                 OR shipping_orders.po_reference like ?
                                 OR articles.customer_reference like ?
                                 OR customer_depots.label like ?
                                 OR customer_depots.name like ?
                                 OR shipping_customer_depots_shipping_orders.label like ?
                                 OR shipping_customer_depots_shipping_orders.name like ?
                                 OR invoicing_customer_depots_shipping_orders.label like ?
                                 OR invoicing_customer_depots_shipping_orders.name like ?)'
      conditions_param_values << 1.to_i # only for id
      8.times do
        conditions_param_values << "\%#{q}\%"
      end
    end

    unless column_search_query.empty?
      conditions_string_ary << "#{column_search_query['query']}"
      conditions_param_values += column_search_query['values']
    end
    conditions = conditions_string_ary.join(' AND ')

    @shipping_orders = ShippingOrder.eager_load([:ordering_customer_depot, :shipping_customer_depot, :invoicing_customer_depot, :article, :ordering_customer_depot]).where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction)

    if params[:uninvoiced]
      @shipping_orders = @shipping_orders.select{|so| so.shipping_orderlines.where(status: ShippingOrder::STATUS_SHIPPED, invoice_id: nil).sum(:amount) >0}
    end

    @shipping_orders = @shipping_orders.paginate(per_page: 100, page: params[:page])
    generate_id_sequence_cookie(@shipping_orders, params[:page])

    if request.xhr?
      render(partial: 'shipping_order', collection: @shipping_orders)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render xml: @shipping_orders }
      end
    end
  end

  # GET /shipping_orders/1
  # GET /shipping_orders/1.xml
  def show
    @shipping_order = ShippingOrder.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render xml: @shipping_order }
    end
  end

  # GET /shipping_orders/new
  # GET /shipping_orders/new.xml
  def new
    @shipping_order = ShippingOrder.new(status: ShippingOrder::STATUS_DRAFT)

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render xml: @shipping_order }
    end
  end

  # GET /shipping_orders/1/edit
  def edit
    @shipping_order = ShippingOrder.find(params[:id])
  end

  # POST /shipping_orders
  # POST /shipping_orders.xml
  def create
    @shipping_order = ShippingOrder.new(strong_params)
    @shipping_order.attachment_org = params[:shipping_order][:attachment_org].read if params[:shipping_order][:attachment_org]
    @shipping_order.initiated_by_user_id = current_user.id
    @shipping_order.status = ShippingOrder::STATUS_RECEIVED

    respond_to do |format|
      if @shipping_order.save
        flash[:notice] = 'Shipping order was successfully created.'
        format.html { redirect_to @shipping_order }
        format.xml  { render xml: @shipping_order, status: :created, location: @shipping_order }
      else
        format.html { render action: 'new' }
        format.xml  { render xml: @shipping_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /shipping_orders/1
  # PUT /shipping_orders/1.xml
  def update
    @shipping_order = ShippingOrder.find(params[:id])

    respond_to do |format|
      if @shipping_order.update_attributes(strong_params)
        @shipping_order.attachment_org = params[:shipping_order][:attachment_org].read if params[:shipping_order][:attachment_org]
        @shipping_order.save!
        flash[:notice] = 'Shipping order was successfully updated.'
        format.html { redirect_to @shipping_order }
        format.xml  { head :ok }
      else
        format.html { render action: 'edit' }
        format.xml  { render xml: @shipping_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /shipping_orders/1
  # DELETE /shipping_orders/1.xml
  def destroy
    @shipping_order = ShippingOrder.find(params[:id])
    @shipping_order.destroy

    respond_to do |format|
      format.html { redirect_to shipping_orders_url }
      format.xml  { head :ok }
    end
  end

  def cancel
    @shipping_order = ShippingOrder.find(params[:id])
    @shipping_order.cancel
    flash[:notice] = 'Shipping order cancelled.'
    redirect_to :back
  end

 private
  def sort_column
    params[:sort] || 'shipping_orders.id'
  end

  def sort_direction
    params[:direction] || 'desc'
  end

  def search
    params[:search] || ''
  end
end
