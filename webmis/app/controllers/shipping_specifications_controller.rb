class ShippingSpecificationsController < ApplicationController
  before_filter :authorize
  authorize_resource

  layout "webmis"
  helper_method :sort_column, :sort_direction, :search

  def index
    conditions = '1=1'
    if params[:reset]
      params.delete :search
       params.delete :reset

      params[:sort] = 'shipping_specifications.id'
      params[:direction] = 'desc'
    end

    conditions_string_ary = []
    conditions_param_values = []
    search = params[:search].to_s.strip
    if !search.blank?
      conditions_string_ary << " and shipping_specifications.id like ? or
          shipping_specifications.name like ? or
          shipping_specifications.shipment_type like ? or
          shipping_specifications.courier like ? or 
          distribution_lists.name like ? or
          packing_specifications.name like ? or
          file_transfer_accounts.user_name like ? "
      7.times do
        conditions_param_values << "\%#{search}\%"
      end
    end

    conditions = conditions + conditions_string_ary.join(" ")

    @shipping_specifications = ShippingSpecification.where([conditions]+conditions_param_values).joins(' left join packing_specifications
                                            on packing_specifications.id = shipping_specifications.packing_specification_id
                                              left join distribution_lists
                                                on distribution_lists.id = shipping_specifications.distribution_list_id
                                                  left join file_transfer_accounts
                                                    on file_transfer_accounts.id = shipping_specifications.file_transfer_account_id').order(sort_column + 
                                                                ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])


    respond_to do |format|
      if !search.blank?
        flash.now[:notice] = @shipping_specifications.total_entries.to_s + " records found."
      end
      format.html # index.html.erb
      format.json { render json: @shipping_specifications }
    end
  end

  # GET /shipping_specifications/1
  # GET /shipping_specifications/1.json
  def show
    @shipping_specification = ShippingSpecification.find(params[:id])
    contact = ExternalContact.find(@shipping_specification.external_contact_id) if @shipping_specification.external_contact_id
    if contact
      @contact_person = contact.first_name ? contact.first_name + " " + contact.last_name : contact.last_name
    else
      @contact_person = "-"
    end
  end

  # GET /shipping_specifications/new
  # GET /shipping_specifications/new.json
  def new
    @shipping_specification = ShippingSpecification.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @shipping_specification }
    end
  end

  # GET /shipping_specifications/1/edit
  def edit
    @shipping_specification = ShippingSpecification.find(params[:id])
  end

  # POST /shipping_specifications
  # POST /shipping_specifications.json
  def create
    @shipping_specification = ShippingSpecification.new(strong_params)
    respond_to do |format|
      if @shipping_specification.save
        format.html { redirect_to @shipping_specification, :notice => 'New shipping specification created.' }
        format.json { render json: @shipping_specification, :status => :created, :location => @shipping_specification }
      else
        format.html { render :action => "new" }
        format.json { render :json => @shipping_specification.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /shipping_specifications/1
  # PUT /shipping_specifications/1.json
  def update
    @shipping_specification = ShippingSpecification.find(params[:id])
    respond_to do |format|
      if @shipping_specification.update_attributes(strong_params)
        format.html { redirect_to @shipping_specification, :notice => 'Shipping specification was successfully updated.' }
        format.json { head :no_content }
      else
        flash.now[:alert] = "Shipping specification could not be saved."
        format.html { render action: "edit" }
        format.json { render json: @shipping_specification.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /shipping_specifications/1
  # DELETE /shipping_specifications/1.json
  def destroy
    @shipping_specification = ShippingSpecification.find(params[:id])
    @shipping_specification.destroy

    respond_to do |format|
      format.html { redirect_to shipping_specifications_url, notice: "Shipping specification deleted." }
      format.json { head :no_content }
    end
  end

  private
  def sort_column
    params[:sort] || "shipping_specifications.id"
  end

  def sort_direction
    params[:direction] || "desc"
  end

  def search
    params[:search] || ""
  end

end
