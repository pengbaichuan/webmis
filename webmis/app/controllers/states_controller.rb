class StatesController < ApplicationController

  before_filter :authorize
  authorize_resource  
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search

  # index not used as this is handled by the regions controller
  def index
    if params[:reset]
      params.delete :q
      params.delete :reset

      params[:sort] = 'name'
      params[:direction] = 'asc'
    end

    conditions = "1=1 "
    conditions_string_ary = []
    conditions_param_values = []

    unless params[:q].blank?
      conditions_string_ary << " and (name LIKE ? OR state_code LIKE ?)"
      2.times do
        conditions_param_values << "\%#{params[:q].strip}\%"
      end
    end
    conditions = conditions + conditions_string_ary.join(" ")
    @states = State.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])

    if request.xhr?
      flash.keep[:notice]
      render(:partial => "state", :collection => @states)
    end 
  end

  def show
    @state = State.find(params[:id])
  end

  def new
    @state = State.new
    @state.isactive = true
  end

  def edit
    @state = State.find(params[:id])
  end

  def create
    @state = State.new(strong_params)
    @state.type = 'State'
    @state.display_type_id = AreaType.where(:name => 'State').last.id

    respond_to do |format|
      if @state.save
        # the 'method app_module_ids=' must be placed after the save as it will created and save child objects of @state, so @state must also be a saved entity
        @state.app_module_ids = [AppModule.where(:abbr => 'MIS').last.id]
        format.html { redirect_to @state, :notice => "State succesfully saved."	 }
        format.json { render json: @state, status: :created, location: @state }
      else
        format.html { render action: "new" }
        format.json { render json: @state.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @state = State.find(params[:id])
    @state.app_module_ids = [AppModule.where(:abbr => 'MIS').last.id]

    respond_to do |format|
      if @state.update_attributes(strong_params)
        format.html { redirect_to(@state, :notice => 'State was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @state.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @state = State.find(params[:id])
    if @state.destroy
	   flash[:notice] = "State succesfully deleted!"
	   redirect_to_index
    else
	   flash[:error] =  "Database error! Please contact an administrator!"
      redirect_to_index
    end
  end

  def redirect_to_index
    if params[:q]
	    redirect_to :action => :index, :q => params[:q]
    else
      redirect_to :action => :index
    end
  end

  def generate_dakota_id
    render :text => Region.generate_dakota_id
  end

  private
  def sort_column
    params[:sort] || "name"
  end

  def sort_direction
    params[:direction] || "asc"
  end

  def search
    params[:search] || ""
  end

end
