class StaticReleaseNoteTitlesController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search 
  # GET /static_release_note_titles GET /static_release_note_titles.xml
  def index
    conditions = '1=1'
    if params[:reset]
      params.delete :search
      params.delete :reset
      
      params.delete :sort
      params.delete :direction
      params[:removed] = false
    end
    
    if params[:search] && params[:search] != ""
      q = params[:search].strip
      conditions = "#{conditions} and (static_release_note_titles.title like ?
                                           or product_categories.abbreviation like ?)"    
    end

    @static_release_note_titles = StaticReleaseNoteTitle.includes(:product_category).where(conditions,"%#{q}%",
      "%#{q}%").references(:product_categories).order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])
    
    if request.xhr?
      render(:partial => "static_release_note_title", :collection => @static_release_note_titles)
    else
      respond_to do |format|    
        format.html # index.html.erb
        format.xml  { render :xml => @static_release_note_titles }
      end
    end
  end

  # GET /static_release_note_titles/1
  # GET /static_release_note_titles/1.xml
  def show
    @static_release_note_title = StaticReleaseNoteTitle.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @static_release_note_title }
    end
  end

  # GET /static_release_note_titles/new
  # GET /static_release_note_titles/new.xml
  def new
    @static_release_note_title = StaticReleaseNoteTitle.new
    @static_release_note_title.status = 10

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @static_release_note_title }
    end
  end

  # GET /static_release_note_titles/1/edit
  def edit
    @static_release_note_title = StaticReleaseNoteTitle.find(params[:id])
  end

  # POST /static_release_note_titles
  # POST /static_release_note_titles.xml
  def create
    @static_release_note_title = StaticReleaseNoteTitle.new(strong_params)

    respond_to do |format|
      if @static_release_note_title.save
        format.html { redirect_to(:action => :index, :notice => 'StaticReleaseNoteTitle was successfully created.') }
        format.xml  { render :xml => @static_release_note_title, :status => :created, :location => @static_release_note_title }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @static_release_note_title.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /static_release_note_titles/1
  # PUT /static_release_note_titles/1.xml
  def update
    @static_release_note_title = StaticReleaseNoteTitle.find(params[:id])

    respond_to do |format|
      if @static_release_note_title.update_attributes(strong_params)
        format.html { redirect_to(:action => :index, :notice => 'StaticReleaseNoteTitle was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @static_release_note_title.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /static_release_note_titles/1
  # DELETE /static_release_note_titles/1.xml
  
  
private  
  def sort_column  
    params[:sort] || 'static_release_note_titles.title'
  end  
    
  def sort_direction  
    params[:direction] || "asc"  
  end
  
  def search
    params[:search] || ""
  end  
end
