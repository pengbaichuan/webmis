class StockController < ApplicationController
  before_filter :authorize
  authorize_resource

  # GET /stock
  # GET /stock.xml
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search

  def index
    conditions = "stock.status = 'ARCHIVED' "
    if params[:reset]
      params.delete :search
      params.delete :reset
      params.delete :oldest_archive

      params[:sort] = 'medium_number'
      params[:direction] = 'desc'
    end

    if params[:search] && params[:search] != ""
      q = params[:search].strip
      conditions = "#{conditions} and (stock.product_ref like ? or
                   stock.medium_number like  ? or
                   company_abbrs.company_name like ? or
                   data_releases.name like ? or
                   commercial_name like ? or
                   company_abbrs.company_name like ? or
                   stock.region_name like ? or
                   stock.product_name like ? or
                   stock.databasename like ? or
                   stock.location like ?)"
    end

    if (params[:loctype])
      session[:loctype] = params[:loctype]
    end

    if session[:loctype] && session[:loctype] != ''
      conditions = conditions + " and location_type = '#{session[:loctype]}'"
    end

    if params[:setcode] && params[:setcode].strip != ''

      ps = Productset.find(params[:setcode])
      products = ps.includedproducts
      s = "(1=0 "
      products.each do |product|
        s = s + " OR product_ref like '" + product.product.volumeid.to_s + "%%'"
      end
      s = s + ")"

      if s.size > 5
        conditions = conditions + " and #{s} "
      end

    end

    if params[:oldest_archive] && !params[:oldest_archive].blank?
      conditions = "1=1 and (stock.location LIKE \"%%ARCHIVE%%\")"
      params[:sort] = 'created_at'
      params[:direction] = 'asc'
      @stock = Stock.where(conditions).order(sort_column + ' ' + sort_direction).paginate(:per_page => 600, :page => params[:page])

    elsif conditions ==  "stock.stockstatus <> 'ARCHIVED' "
      @stock = Stock.order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])
    else
      @stock = Stock.eager_load([:data_release,:company_abbr, :conversion_databases]).where(conditions,"%#{q}%",
       "%#{q}%",
        "%#{q}%",
         "%#{q}%",
          "%#{q}%",
           "%#{q}%",
            "%#{q}%",
             "%#{q}%",
              "%#{q}%",
               "%#{q}%").order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])
    end

    if request.xhr?
      render(:partial => "stock", :collection => @stock)
    else
        respond_to do |format|
          format.html # index.html.erb
          format.xml  { render :xml => @stock }
        end
    end
  end

  def requests
    conditions = "stock.status <> 'ARCHIVED' "
    if params[:reset]
      params.delete :search
      params.delete :reset
      params.delete :setcode

      params[:sort] = 'stock.updated_at'
      params[:direction] = 'desc'
    end

    if params[:search] && params[:search] != ""
      q = params[:search].strip
      conditions = "#{conditions} and (stock.product_ref like ? or
                   stock.medium_number like  ? or
                   company_abbrs.company_name like ? or
                   data_releases.name like ? or
                   commercial_name like ? or
                   company_abbrs.company_name like ? or
                   stock.region_name like ? or
                   stock.product_name like ? or
                   stock.databasename like ? or
                   stock.location like ?)"
    end

    if (params[:loctype])
      session[:loctype] = params[:loctype]
    end

    if session[:loctype] && session[:loctype] != ''
      conditions = conditions + " and location_type = '#{session[:loctype]}'"
    end

    if params[:setcode] && params[:setcode].strip != ''

      ps = Productset.find(params[:setcode])
      products = ps.includedproducts
      s = "(1=0 "
      products.each do |product|
        s = s + " OR product_ref like '" + product.product.volumeid.to_s + "%%'"
      end
      s = s + ")"

      if s.size > 5
        conditions = conditions + " and #{s} "
      end

    end

    if conditions ==  "stock.stockstatus <> 'ARCHIVED' "
      @stock = Stock.order(sort_column + ' ' + sort_direction).paginate(:per_page => 20, :page => params[:page])
    else
      @stock = Stock.eager_load([:data_release,:company_abbr]).where(conditions,"%#{q}%",
       "%#{q}%",
        "%#{q}%",
         "%#{q}%",
          "%#{q}%",
           "%#{q}%",
            "%#{q}%",
             "%#{q}%",
              "%#{q}%",
               "%#{q}%").order(sort_column + ' ' + sort_direction).paginate(:per_page => 20, :page => params[:page])
    end

    if request.xhr?
      render(:partial => "request", :collection => @stock)
    else
        respond_to do |format|
          format.html # index.html.erb
          format.xml  { render :xml => @stock }
        end
    end
  end
  # GET /stock/1
  # GET /stock/1.xml

  def show
    @stock = Stock.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @stock }
    end
  end

  def show_request
    @stock = Stock.find(params[:id])
    if @stock.conversion_databases.size > 0
        @dbfilename = @stock.conversion_databases.available.collect {|x| x.filename }.uniq.join
    else
        @dbfilename = @stock.databasename
    end

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @stock }
    end
  end
  # GET /stock/new
  # GET /stock/new.xml

  def new
    @stock = Stock.new

    @ds = -1

    if (params[:cid])
      #prepare stockrecord from conversion
      c = Conversion.find(params[:cid])
      @stock.prepare_from_conversion(c)
      @supplier_abbr_id = c.data_release.company_abbr.id
      @ds = c.data_release_id
    end

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @stock }
    end
  end

  def new_request
    @stock = Stock.new
    init_stock(@stock)
    @ds = -1
    @cvs = []
    @datareleases = []
    @purpose = [ "production", "test"]
    @amount = Array.new
    @amount << "see distr.list"
    @amount = @amount + (1..20).to_a
    @copies = @stock.nr_of_copies_request
    @stock.requested_delivery_date = DateTime.now


    if params[:conversion_id]
      cv = Conversion.find(params[:conversion_id])
      @cid = params[:conversion_id]
      @stock.prepare_from_conversion(cv)
      @copies = @stock.nr_of_copies_request
      @datareleases = DataRelease.where("company_abbr_id = #{@stock.supplier_id}")
      @cvs << cv


    end
    if !params[:cid].blank? || !params[:production_orderline_id].blank?
      conversion_id = params[:cid] if !params[:cid].blank?
      conversion_id = ProductionOrderline.find(params[:production_orderline_id]).chosen_conversion_id if !params[:production_orderline_id].blank?
      #prepare stock record from conversion
      c = Conversion.find(conversion_id)
      @cid = conversion_id
      @stock.prepare_from_conversion(c)
      @copies = @stock.nr_of_copies_request
      @datareleases = DataRelease.where("company_abbr_id = #{@stock.supplier_id}")
      @supplier_abbr_id = c.data_release.company_abbr.id
      @ds = c.data_release_id
      @stock.end_customer_name = ''
      @stock.commercial_name = Product.find_by_volumeid(c.productid).name unless c.productid.blank?
    end


    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @stock }
    end
  end

  def create_request
    @amount = (1..20).to_a
    @amount << "see distr.list"
    @purpose = [ "production", "test"]

    Stock.transaction do
      @stock = Stock.new(params[:stock])
      @stock.status = "REQUESTED"
      @stock.requested_delivery_date = Date.new(params[:stock][:requested_delivery_date][:year].to_i,
           params[:stock][:requested_delivery_date][:month].to_i,
           params[:stock][:requested_delivery_date][:day].to_i)

      @stock.supplier_name = CompanyAbbr.find(@stock.supplier_id).name
      @stock.requested_by = current_user.first_name + " " + current_user.last_name

      @stock.nr_of_copies_request = 0 if (@stock.nr_of_copies_request == "see distr.list")

      if params[:stock][:recognition_file] == "" || params[:stock][:cd_type] == ""
            flash[:error] = "Please choose recognition-file and medium type"
            form_errror = true
      end

      @cvs = []
      @datareleases = DataRelease.where("company_abbr_id = #{@stock.supplier_id}")
      if !params[:production_orderline_id].blank?
        @stock.production_orderline_id = params[:production_orderline_id]
      end
      respond_to do |format|
        if @stock.save && !form_errror
          Postoffice.medium_request(@stock.id).deliver
          flash[:notice] = 'Media request was succesfully requested'

          if @stock.conversion
            # When production order is available go to next step
            if  @production_orderline = @stock.conversion.production_orderline
                @stock.conversion.production_orderline.transition(current_user)
                @production_orderline.update_bp_message("Media requested")
             end
          end



          # When coming from other controller

          if ((params[:return_to_controller] && params[:return_to_action]) && (params[:return_to_controller].to_s != "" && params[:return_to_action].to_s != ""))
            if params[:return_to_id].to_s == ""
              format.html { redirect_to :action => params[:return_to_action].to_s,
              :controller => params[:return_to_controller].to_s }
            else
              ret_id = params[:return_to_id].to_s
              format.html { redirect_to :action => params[:return_to_action].to_s,
              :controller => params[:return_to_controller].to_s, :id => ret_id }
            end
            format.xml  { render :xml => @stock, :status => :created, :location => @stock }
          end
          format.html { redirect_to :action => :requests }


        else
          @ds = -1
          if @stock.data_release_id
            @ds = DataRelease.find(@stock.data_release_id).company_abbr.id
          end
          if params[:stock][:conversion_id] && params[:production_orderline_id].blank?
            format.html { redirect_to :action => 'new_request',
              :cid => params[:stock][:conversion_id],
            :return_to_controller => params[:return_to_controller].to_s,
            :return_to_id => params[:return_to_id].to_s,
            :return_to_action => params[:return_to_action].to_s
            }
          elsif !params[:production_orderline_id].blank?
             format.html { redirect_to :action => 'new_request', :return_to_controller => params[:return_to_controller].to_s,
            :return_to_id => params[:return_to_id].to_s,
            :return_to_action => params[:return_to_action].to_s,:production_orderline_id => params[:production_orderline_id]

             }
          else
            format.html { render :action => "new_request"}
          end


        end

      end
    end
  end
  # GET /stock/1/edit

  def edit_request
    @editing = "yep"
    @stock = Stock.find(params[:id])
    #cv = Conversion.find(@stock.conversion_id)
    @sabbr = DataRelease.where(id:@stock.data_release_id)
    @ds = -1
    @cvs = []
    @datareleases = []
    @purpose = [ "production", "test"]
    @amount = (1..20).to_a
    @amount << "see distr.list"
    @copies = @stock.nr_of_copies_request
    @copies = "see distr.list" if (@stock.nr_of_copies_request == 0)
    @cvs = Conversion.select("id,status").where("status = #{Conversion::STATUS_FINISHED}")
  end
  # POST /stock
  # POST /stock.xml

  def edit
    @stock = Stock.find(params[:id])
    @stock.production_orderline_id = params[:production_orderline_id]
    @ds = -1
    @a = Stock.where("id in (select MIN(id) from stock where id > "+@stock.id.to_s+")").first
    if @stock.data_release_id
      @ds = DataRelease.find(@stock.data_release_id).company_abbr.id
      @supplier_abbr_id = @ds
    end
    begin
      if @a
        @next = @a.id
        @result_next = true
      end
    rescue => error
      @result_next = false
    end

    begin
      @b = Stock.where("id in (select MAX(id) from stock where id < "+@stock.id.to_s+")").first
      if @b
        @previous = @b.id
        @result_previous = true
      end
    rescue => error
      @result_previous = false
    end
  end
  # POST /stock
  # POST /stock.xml

  def html2pdf_medium_request
    @stock = Stock.find(params[:id])
    if @stock.conversion_databases.size > 0
        @dbfilename = @stock.conversion_databases.available.collect {|x| x.filename }.uniq.join
    else
        @dbfilename = @stock.databasename
    end

    flash.now[:notice] = "medium result #{@stock.id} printed."

    respond_to do |format|
          format.html
          format.pdf do
            render :pdf => "medium_request_#{@stock.id}",
              :margin => { :bottom => 20 },
              :footer => { :html => { :template => '/pdf_footer.html' }},
              :encoding => "utf8"
          end
    end
  end
    #redirect_to params.merge({:action => 'edit_request', :id => params[:id]})
    #redirect_to params.merge({:action => 'requests', :id => '1'})

  def create
    Stock.transaction do
      @stock = Stock.new(strong_params)

      respond_to do |format|
        if @stock.save
          if @stock.location.scan(/ARCHIVE/).size >3
            @stock.location_type = "ARCHIVE"
          else
            @stock.location_type = "BOX"
          end
          update_location(params[:stock][:location], @stock.id)
          @stock.status = 'ARCHIVED' if params[:stock][:location]
          flash[:notice] = 'Stock was successfully created.'
          format.html { redirect_to(:action => :index) }
          format.xml  { render :xml => @stock, :status => :created, :location => @stock }
        else
          @ds = -1
          if @stock.data_release_id
            @ds = DataRelease.find(@stock.data_release_id).company_abbr.id
          end
          format.html { render :action => "new" }
          format.xml  { render :xml => @stock.errors, :status => :unprocessable_entity }
        end
      end
    end
  end
  # PUT /stock/1
  # PUT /stock/1.xml

  def update
    @stock = Stock.find(params[:id])
          @stock.nr_of_copies_request = 0 if (@stock.nr_of_copies_request == "see distr.list")

    if params[:stock][:requested_delivery_date]
    @stock.requested_delivery_date = Date.new(params[:stock][:requested_delivery_date][:year].to_i,
           params[:stock][:requested_delivery_date][:month].to_i,
           params[:stock][:requested_delivery_date][:day].to_i)
           params[:stock].delete(:requested_delivery_date)
    end
    if (params[:stock][:medium_number])
            if ((params[:stock][:medium_number].to_s != '')&&(@stock.status == 'REQUESTED'))
              @stock.status = "CREATED"
              trans_status = 'to_created'

            end
            @stock.actual_delivery_date = Date.now if ((params[:stock][:medium_number].to_s != '')&&(@stock.status == 'REQUESTED'))
            @stock.status = "REQUESTED" if ((params[:stock][:medium_number].to_s == '')&&(@stock.status == 'CREATED'))
            @stock.status = "REQUESTED" if ((params[:stock][:medium_number].to_s == '')&&(@stock.status == 'VALIDATED'))
            @stock.status = 'ARCHIVED' if params[:stock][:location]
            if ((params[:stock][:medium_number].to_s == '')&&(@stock.status == 'REJECTED'))
              @stock.status = "REQUESTED"
              @stock.status_remark = ""
            end
    end
    respond_to do |format|
      Stock.transaction do
        if @stock.update_attributes(strong_params)

          update_location(params[:stock][:location], @stock.id)
          if @stock.save
            flash[:notice] = 'Archive was successfully updated.'
            if ((trans_status) || (trans_status == 'to_created'))
              Postoffice.medium_result(@stock.id).deliver
              if @stock.conversion.production_orderline
                @stock.conversion.production_orderline.update_bp_message("#{@stock.conversion.production_orderline.bp_message}|master created")
              end
            else
              Postoffice.medium_request(@stock.id).deliver if (@stock.status == 'REQUESTED')
            end
          end
            if (params[:prev_action] == "edit_request")
            format.html { redirect_to(:action => 'requests' ) }
          else
            if ((params[:return_to_controller] && params[:return_to_action]) && (params[:return_to_controller].to_s != "" && params[:return_to_action].to_s != ""))
            if params[:return_to_id].to_s == ""
              format.html { redirect_to :action => params[:return_to_action].to_s,
              :controller => params[:return_to_controller].to_s }
            else
              ret_id = params[:return_to_id].to_s
              format.html { redirect_to :action => params[:return_to_action].to_s,
              :controller => params[:return_to_controller].to_s, :id => ret_id }
            end
            else
            format.html { redirect_to(:action => 'index' ) }
            end
          end
          format.xml  { head :ok }
        else
          @ds = -1
          if @stock.data_release_id
            @ds = DataRelease.find(@stock.data_release_id).company_abbr.id
          end

          format.html { render :action => "edit" }
          format.xml  { render :xml => @stock.errors, :status => :unprocessable_entity }
        end

      end
    end

  end
  # DELETE /stock/1
  # DELETE /stock/1.xml

  def medium_validated
    @stock = Stock.find(params[:id])
    @stock.status = 'VALIDATED'
     if @stock.save
          flash[:notice] = 'Status of medium is successfully updated.'
            Postoffice.medium_valid_result(@stock.id).deliver
            if @stock.conversion.production_orderline
              @stock.conversion.production_orderline.update_bp_message("#{@stock.conversion.production_orderline.bp_message}|master validated")
            end
    end
    respond_to do |format|
    flash[:notice] = 'Status of medium is successfully updated.'
    format.html { redirect_to(:action => :requests) }
    end
     end

  def destroy

    @stock = Stock.find(params[:id])
    loc = Location.where(cd_no:@stock.id)
    location_code = ""
    loc.each do |dloc|
      if dloc.locationtype == "ARCHIVE"
          dloc.cd_no = nil
          location_code = dloc.location_code
          dloc.save
      end
    end
    @stock.destroy

    respond_to do |format|
      flash[:notice] = "Record was succesfully deleted.<br />Archive location #{location_code} is released."
      format.html { redirect_to(:action => :index) }
      format.xml  { head :ok }
    end
  end

  def pdf

    t = Table(:column_names => %w[col1 col2 col3 col4],
      :data => [%w[lorum ipsum blah blah]] * 20)

    TableRenderer.render_pdf( :file => "bar.pdf",
      :report_title => "Sample Table Report",
      :data => t )
    pdf = LabelGenerator.render_pdf(:data => user_addresses)
    send_data pdf, :type => "application/pdf",
      :filename => "mailing_labels.pdf"
  end

  def update_location(loc_id, cd)
    locids = []
    l = nil
    if loc_id
      locids = loc_id.split("|")
    end


    if locids.size == 2
      #RELEASE ORIGINAL LOCATION
      orgloc = Location.where(cd_no:cd)
      orgloc.each do |oloc|
        if oloc.locationtype == "ARCHIVE"
          oloc.cd_no = nil
          oloc.save
        end
      end

      l = Location.where("location_code = '#{locids[1]}' and locationtype = '#{locids[0]}'").first
    end

    if l && l.locationtype == "ARCHIVE"
      #RESERVE LOCATION FOR CD
      l.cd_no = cd
      l.reference = cd
      l.save
    end
  end

  def docs_overview
    @stockids = []
    @stockids = (params[:stockid].select {|k,v| v == "1"}).flatten if params[:stockid]
    @stockids.delete("1")
    if !session[:selected]
      session[:selected] = []
      session[:stockids] = []
    end
    stocks = []
    stocks = Stock.find(@stockids)
    stocks.each do |stock|
      session[:selected] << stock.medium_number
      session[:stockids] << stock.id
    end
    session[:stockids] = session[:stockids].uniq
    #@contacts = Array.new
    @contacts = ExternalContact.find_all_dist
    @id = params[:id].to_s
    if params[:commit] == "Add more"
      redirect_to :controller => :stock , :action => :documents
    end
    @stockids = session[:stockids]
  end

  def select_contacts
    c = Company.find(params[:id])
    @contacts = c.external_contacts
    render :layout => false
  end

  def get_datareleases
    if (params[:sid] && params[:sid] != "")
      @dr = DataRelease.where('company_abbr_id = ' + params[:sid])
    else
      @dr = []
    end
    render :partial => "datarelease_select"
  end

  def documents
    conditions = ["stock.status = 'ARCHIVED'"]
    if params[:c]
      cond = params[:c]
      conditions = ["stock.status = 'ARCHIVED' and (stock.medium_number like ?
        or company_abbrs.company_name like ?
        or data_releases.name like ?
        or commercial_name like ?
        or company_abbrs.company_name like ?
        or stock.product_name like ?
        or stock.databasename like ?
        or stock.location like ?
        or stock.product_ref like ?)",cond,cond,cond,cond,cond,cond,cond,cond,cond]
    end

    if (params[:loctype])
      session[:loctype] = params[:loctype]
    end
    if session[:loctype] && session[:loctype] != ''
      conditions.push(session[:loctype])
      conditions[0] = conditions.at(0).to_s + " and location_type = ?"
    end

    if params[:setcode] && params[:setcode].strip != ''
      ps = Productset.find(params[:setcode])
      @ps = ps
      products = ps.includedproducts
      s = "("
      rs = ""
      if products


      products.each do |product|
        s = s + "'" + product.product.volumeid.to_s + "',"
        rs = rs + " or product_ref like '#{product.product.volumeid.to_s}%'"
      end
      s = s[0,s.size-1]
      s = s + ")"
      if s.size > 2
        conditions[0] = conditions.at(0).to_s + " and product_ref in #{s} " + " and stock.status = 'ARCHIVED' "
        conditions[0] = conditions.at(0).to_s + rs + " and stock.status = 'ARCHIVED' "
      end
    end
    end

    if conditions ==  ""
      @stock = Stock.paginate(:all , :page => params[:page])
    else
      @stock = Stock.includes([:data_release,:company_abbr]).where(conditions).order("product_ref DESC").paginate(:page => params[:page])
    end
    if params[:commit] != "Print"
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @stock }
      end
    else
      @contacts = ExternalContact.find_all_dist
      render  :action => 'docs_overview'

    end
  end

  def clear_selected
    session[:selected] = nil
    session[:stockids] = nil
    redirect_to :action => 'documents'
  end

  def get_address
    if params[:id] && params[:id].size >= 1
      @external_contact = ExternalContact.find(params[:id])
    end
    render :layout => false
  end

  def cyclecountreport
    count = 0
    csv_string = FasterCSV.generate do |csv|
      csv << ["creation_date" , "commercial_name",  "product_id", "medium_number", "location_name","databasename","supplier_name", "data_release", "shipped"]
      Stock.all.each do |stock|
        shipped = "NO"
        count = count + 1
        found = OutboundOrderline.where("cd_no = '#{stock.medium_number}'").count
        shipped = "YES" if found > 0
        loc = stock.location.to_s

        if stock.location && stock.location.scan(/ARCHIVE\|([0-9]{1,2})$/)[0]
          loc = "ARCHIVE|" + sprintf("%03d",stock.location.scan(/ARCHIVE\|([0-9]{1,2})$/)[0][0].to_i)
        end
        if stock.data_release
         csv << [stock.created_at.strftime('%d-%m-%y'),stock.commercial_name, stock.product_id, stock.medium_number, loc,stock.databasename, stock.supplier_name, stock.data_release.name, shipped]
        else
         csv << [stock.created_at.strftime('%d-%m-%y'),stock.commercial_name, stock.product_id, stock.medium_number, loc,stock.databasename, stock.supplier_name, 'UNKNOWN', shipped]
        end
      end
    end

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @stock }
      format.csv { send_data(csv_string, :type => 'application/excel', :filename => 'voorraad.csv', :disposition => 'inline')}
    end
  end

  def get_details
    @cvs = []
    @datareleases = []
    @cvs = Product.find_completed_conversions(params[:product_id])
    @stock = Stock.new
    @stock.requested_delivery_date = Date.today + 2.days
    @purpose = [ "production", "test"]
    @amount = ['see distr. list'] + (1..20).to_a


    # get relevant PDS data
    product = Product.find_by_volumeid(params[:product_id])
    if product
      sets = product.productsets
      if sets.size == 1
        @stock.end_customer_name = sets[0].customer_name
        @stock.supplier_id = product.main_dataset.company_abbr_id if product.main_dataset
        @stock.commercial_name = product.name
      end
    end

    if (params[:conversion_id] && params[:conversion_id].size > 0) || (params[:cid] && params[:cid].size > 0)
      cv = Conversion.find(params[:conversion_id]) if params[:conversion_id]
      cv = Conversion.find(params[:cid]) if params[:cid]
      @stock.prepare_from_conversion(cv)
      @copies = @stock.nr_of_copies_request
      @datareleases = DataRelease.where("company_abbr_id = #{@stock.supplier_id}")
      @stock.conversion_id = cv.id
    else
      @product_location = Stock.new
    end
    render :layout => false
  end

  def clone_request
    @source_stock = Stock.find(params[:id])
    @stock = Stock.new
    @stock.commercial_name = @source_stock.commercial_name
    @stock.supplier_id = @source_stock.supplier_id
    @stock.supplier_name = @source_stock.supplier_name
    @stock.cd_type = @source_stock.cd_type
    @stock.product_id = @source_stock.product_id
    @stock.data_release_id = @source_stock.data_release_id
    @stock.product_ref = @source_stock.product_ref
    @stock.tpd_phase_2_description = @source_stock.tpd_phase_2_description
    @stock.databasename = @source_stock.databasename
    @stock.region_name = @source_stock.region_name
    @stock.region_id = @source_stock.region_id
    @stock.end_customer_name = @source_stock.end_customer_name
    @stock.recognition_file = @source_stock.recognition_file
    @stock.nr_of_copies_request = @source_stock.nr_of_copies_request
    @stock.purpose = @source_stock.purpose
    @stock.conversion_id = @source_stock.conversion_id
    @stock.production_order_id = @source_stock.production_order_id
    @stock.requested_by = current_user.first_name + " " + current_user.last_name
    @stock.status = "REQUESTED"
    @stock.medium_number = ''
    @stock.requested_delivery_date = DateTime.now
    @stock.actual_delivery_date = nil
    @stock.save

    @editing = "yep"
    @sabbr = DataRelease.where(id:@stock.data_release_id)
    @ds = -1
    @cvs = []
    @datareleases = []
    @purpose = [ "production", "test"]
    @amount = (1..20).to_a
    @amount << "see distr.list"
    @copies = @stock.nr_of_copies_request
    @copies = "see distr.list" if (@stock.nr_of_copies_request == 0)
    @cvs = Conversion.where(:status => Conversion::STATUS_FINISHED)


    respond_to do |format|
      flash[:notice] = "Record was succesfully cloned."
          format.html { render :action => "edit_request" }
          format.xml  { render :xml => @stock.errors, :status => :unprocessable_entity }
    end
  end

  def borrowed
    @borrowed_grid = Stock.where("borrby not like ''" ).order('location')
    @stock = Stock.where("borrby not like ''")

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @stock , :layout => false}
    end
  end

  def reject_medium
    @stock = Stock.find(params[:id])

  end

  def perform_medium_rejection
    if params[:stock][:status_remark] && !params[:stock][:status_remark].blank?
      @stock = Stock.find(params[:id])

      remark = params[:stock][:status_remark] + "\nRejected by " + current_user.first_name + " " + current_user.last_name
      @stock.status_remark = remark
      @stock.status = 'REJECTED'
      if @stock.save
            if @stock.conversion
              if @stock.conversion.production_orderline
                pol = @stock.conversion.production_orderline
                pol.bp_name = "CREATE MEDIA"
                pol.bp_seqnr = 60
                pol.save
              end
            end
          respond_to do |format|
              flash[:notice] = 'Status of medium is successfully updated.<br /><i>Please notify the production manager.</i>'
              format.html { redirect_to(:action => :requests) }
              format.xml  { head :ok }
          end
      end
    else
        flash[:error] = "Please provide a reason for rejection."
        redirect_to :action => 'reject_medium', :id => params[:id]
    end

  end

  def move2box
    @stock = Stock.find(params[:id])
    new_location = "BOX|#{Location.where(:locationtype => "BOX").order('location_code desc').collect {|x|x.location_code.to_i}.sort.last.to_s}"
    update_location(new_location, @stock.id)
    @stock.location = new_location
    @stock.save
    redirect_to :action => 'index', :oldest_archive => true

  end


protected

  def init_stock(stock)
    stock.requested_delivery_date = Date.today + 2.days

    return stock
  end

private
  def sort_column
    params[:sort] || "stock.updated_at"
  end

  def sort_direction
    params[:direction] || "desc"
  end

  def search
    params[:search] || ""
  end

end
