class StockDepotsController < ApplicationController
  before_filter :authorize
  authorize_resource
  
  layout 'webmis'
        
  # GET /stock_depots
  # GET /stock_depots.xml
  def index
    @stock_depots = StockDepot.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @stock_depots }
    end
  end

  # GET /stock_depots/1
  # GET /stock_depots/1.xml
  def show
    @stock_depot = StockDepot.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @stock_depot }
    end
  end

  # GET /stock_depots/new
  # GET /stock_depots/new.xml
  def new
    @stock_depot = StockDepot.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @stock_depot }
    end
  end

  # GET /stock_depots/1/edit
  def edit
    @stock_depot = StockDepot.find(params[:id])
  end

  # POST /stock_depots
  # POST /stock_depots.xml
  def create
    @stock_depot = StockDepot.new(strong_params)

    respond_to do |format|
      if @stock_depot.save
        format.html { redirect_to(@stock_depot, :notice => 'StockDepot was successfully created.') }
        format.xml  { render :xml => @stock_depot, :status => :created, :location => @stock_depot }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @stock_depot.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /stock_depots/1
  # PUT /stock_depots/1.xml
  def update
    @stock_depot = StockDepot.find(params[:id])

    respond_to do |format|
      if @stock_depot.update_attributes(strong_params)
        format.html { redirect_to(@stock_depot, :notice => 'StockDepot was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @stock_depot.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /stock_depots/1
  # DELETE /stock_depots/1.xml
  def destroy
    @stock_depot = StockDepot.find(params[:id])
    @stock_depot.destroy

    respond_to do |format|
      format.html { redirect_to(stock_depots_url) }
      format.xml  { head :ok }
    end
  end
end
