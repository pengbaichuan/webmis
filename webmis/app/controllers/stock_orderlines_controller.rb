class StockOrderlinesController < ApplicationController
    before_filter :authorize
    authorize_resource
    layout 'webmis'

#  access_control :DEFAULT => 'Logistics Manager', [:index,:show] => 'all',
#     :destroy => 'Logistics Admin'
        
  # GET /stock_orderlines
  # GET /stock_orderlines.xml
  def index
    @stock_orderlines = StockOrderline.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @stock_orderlines }
    end
  end

  # GET /stock_orderlines/1
  # GET /stock_orderlines/1.xml
  def show
    @stock_orderline = StockOrderline.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @stock_orderline }
    end
  end

  # GET /stock_orderlines/new
  # GET /stock_orderlines/new.xml
  def new
    @stock_orderline = StockOrderline.new
    @stock_order = StockOrder.find(params[:stock_order_id])
    @stock_orderline.stock_order_id = params[:stock_order_id]
    @stock_orderline.status = 0
    

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @stock_orderline }
    end
  end

  # GET /stock_orderlines/1/edit
  def edit
    @stock_orderline = StockOrderline.find(params[:id])
    @stock_order = @stock_orderline.stock_order
  end
  
  def confirm
    @stock_orderline = StockOrderline.find(params[:id])
    @stock_order = @stock_orderline.stock_order
  end

  # POST /stock_orderlines
  # POST /stock_orderlines.xml
  def create
    StockOrderline.transaction do
        @stock_orderline = StockOrderline.new(strong_params)

        respond_to do |format|
          if @stock_orderline.save
            ws = WarehouseStock.create_or_update_from_orderline(@stock_orderline)
            ws.save
            format.html { redirect_to(:controller => 'stock_orders', :id => @stock_orderline.stock_order_id, :action => 'edit', :notice => 'Line was successfully created.') }
            format.xml  { render :xml => @stock_orderline, :status => :created, :location => @stock_orderline }
          else
            format.html { render :action => "new" }
            format.xml  { render :xml => @stock_orderline.errors, :status => :unprocessable_entity }
          end
        end
    end
  end

  # PUT /stock_orderlines/1
  # PUT /stock_orderlines/1.xml
  def update
    @stock_orderline = StockOrderline.find(params[:id])
    if params[:commit] == 'Confirm'
      if params[:stock_orderline][:stock_reference] == ''
        unvalid = true
        message = 'reference is required!'
      end
      
      if params[:stock_orderline][:amount_confirmed] == ''
        unvalid = true
        message = 'Quantity Confirmed is required!'
      end
      
      if params[:stock_orderline][:amount_confirmed].to_i < @stock_orderline.amount_order.to_i && params[:stock_orderline][:amount_confirmed].to_i != 0
        @stock_orderline.status = 50
      end
    end
    StockOrderline.transaction do 
      respond_to do |format|
        if @stock_orderline.update_attributes(strong_params)
          ws = WarehouseStock.create_or_update_from_orderline(@stock_orderline)
          ws.save
          if unvalid
            format.html { redirect_to(:controller => 'stock_orders', :id => @stock_orderline.stock_order_id, :action => 'confirm', :notice => message) }
          else
            format.html { redirect_to(:controller => 'stock_orders', :id => @stock_orderline.stock_order_id, :action => 'edit', :notice => 'Line was successfully updated.') }
          end          
          format.xml  { head :ok }
        else
          format.html { render :action => "edit" }
          format.xml  { render :xml => @stock_orderline.errors, :status => :unprocessable_entity }
        end
      end
    end
  end
  
  
  # DELETE /stock_orderlines/1
  # DELETE /stock_orderlines/1.xml
  def confirmed
  @stock_orderline = StockOrderline.find(params[:id])
    if params[:commit] == 'Confirm'
      if params[:stock_orderline][:stock_reference] == ''
        unvalid = true
        flash[:error] = 'reference is required!'
      else
        @stock_orderline.stock_reference = params[:stock_orderline][:stock_reference]
      end
      
      if params[:stock_orderline][:amount_confirmed] == ''
        unvalid = true
        flash[:error] = 'Quantity Confirmed is required!'
      else
        @stock_orderline.amount_confirmed = params[:stock_orderline][:amount_confirmed]
      end
      
      @stock_orderline.remarks = params[:stock_orderline][:remarks]
      if @stock_orderline.save
        ok = true
      end
      if params[:stock_orderline][:amount_confirmed].to_i < @stock_orderline.amount_order.to_i && params[:stock_orderline][:amount_confirmed].to_i != 0
        @stock_orderline.status = 50 #partly confirmed
        @stock_orderline.save
      else
        @stock_orderline.stock_order.close
        @stock_orderline.status = 60 #confirmed
        
      end
    end
    
    
    
    StockOrderline.transaction do 
      respond_to do |format|
        if @stock_orderline.status != 60
            ws = WarehouseStock.create_or_update_from_orderline(@stock_orderline)
            ws.save
        end
        if  ok
          if unvalid
            format.html { redirect_to(:controller => 'stock_orderlines', :id => @stock_orderline.stock_order_id, :action => 'confirm') }
            flash.now[:error]
          else
            flash[:notice] = 'Line was successfully confirmed.'
            format.html { redirect_to(:controller => 'stock_orders', :id => @stock_orderline.stock_order_id, :action => 'edit') }
          end          
          format.xml  { head :ok }
        else          
          format.html { render :action => "edit" }
          format.xml  { render :xml => @stock_orderline.errors, :status => :unprocessable_entity }
        end   
          
      end
    end
  end
  
  def destroy
    @stock_orderline = StockOrderline.find(params[:id])
    @stock_orderline.destroy

    respond_to do |format|
      format.html { redirect_to(stock_orderlines_url) }
      format.xml  { head :ok }
    end
  end
  
  def order
    @stock_orderline = StockOrderline.find(params[:id])
    @stock_orderline.status = 30 #Ordered
    @stock_orderline.save

    StockOrderline.transaction do 
      respond_to do |format|
        if @stock_orderline.save && @stock_orderline.amount_order > 0
          ws = WarehouseStock.create_or_update_from_orderline(@stock_orderline)
          ws.save
            flash[:notice] = 'Line was successfully ordered.'
            format.html { redirect_to(:controller => 'stock_orders', :id => @stock_orderline.stock_order_id, :action => 'edit') }
             
          format.xml  { head :ok }
        else
          format.html { render :action => "edit" }
          format.xml  { render :xml => @stock_orderline.errors, :status => :unprocessable_entity }
        end
      end
    end    
  end
end
