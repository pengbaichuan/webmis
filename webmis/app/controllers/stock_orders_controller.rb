class StockOrdersController < ApplicationController
    before_filter :authorize
    authorize_resource
    layout 'webmis'

#        before_filter :authorize
#        access_control :DEFAULT => 'Logistics Manager', [:index,:show] => 'all',
#          :destroy => 'Logistics Admin'
        
  # GET /stock_orders
  # GET /stock_orders.xml
  def index
    @stock_orders = StockOrder.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @stock_orders }
    end
  end

  # GET /stock_orders/1
  # GET /stock_orders/1.xml
  def show
    @stock_order = StockOrder.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @stock_order }
    end
  end

  # GET /stock_orders/new
  # GET /stock_orders/new.xml
  def new
    @stock_order = StockOrder.new
    @stock_order.date_ordered = Time.now
    @stock_order.status = 0

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @stock_order }
    end
  end

  # GET /stock_orders/1/edit
  def edit
    @stock_order = StockOrder.find(params[:id])
  end

  # POST /stock_orders
  # POST /stock_orders.xml
  def create
    @stock_order = StockOrder.new(strong_params)
    @stock_order.depot_id = '1'

    respond_to do |format|
      if @stock_order.save
        format.html { redirect_to(:controller => :stock_orderlines, :action => 'new', :stock_order_id => @stock_order.id, :notice => 'stock order header was successfully saved.') }
        format.xml  { render :xml => @stock_order, :status => :created, :location => @stock_order }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @stock_order.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /stock_orders/1
  # PUT /stock_orders/1.xml
  def update
    @stock_order = StockOrder.find(params[:id])

    respond_to do |format|
      if @stock_order.update_attributes(strong_params)
        format.html { redirect_to(@stock_order, :notice => 'StockOrder was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @stock_order.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /stock_orders/1
  # DELETE /stock_orders/1.xml
  def destroy
    @stock_order = StockOrder.find(params[:id])
    @stock_order.destroy

    respond_to do |format|
      format.html { redirect_to(stock_orders_url) }
      format.xml  { head :ok }
    end
  end
  
  def close
    order = StockOrder.find(params[:id])
    order.close
  end
  
  def html2pdf_stock_order
   @stock_order = StockOrder.find(params[:id])
    if @stock_order.stock_orderlines.size > 0
        @lines = @stock_order.stock_orderlines
    else
        @lines = []
    end
    
    @cu = current_user

    t = Time.now
    ts = t.strftime('%Y%m%d')
  
    flash[:notice] = "Stock order #{@stock_order.id} printed."
    respond_to do |format|
          format.html
          format.pdf do
            render :pdf => "PO_#{@stock_order.purchase_reference}_#{ts}.pdf",
              :margin => { :bottom => 20 },
              :footer => { :html => { :template => '/pdf_footer', :formats => [:html] }},
              :encoding => "utf8" 
          end
    end    

  end
end
