class TestPlansController < ApplicationController
  before_filter :authorize
  authorize_resource
  
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search
  
  def index
    if params[:reset]
      params.delete :search
      params.delete :search_status
      params.delete :obsolete
      params.delete :reset

      params[:sort] = 'test_plans.name'
      params[:direction] = 'asc'
    end

    conditions_string_ary = []
    conditions_param_values = []

    if params[:obsolete].blank?
      conditions_string_ary << '(status IS NULL OR status != "obsolete")'
    else
      conditions_string_ary << '(1=1)'
    end

    unless params[:search_status].blank?
      q_status = params[:search_status].strip
      conditions_string_ary << 'AND (status = ?)'
      conditions_param_values << q_status
    end

    unless params[:search].blank?
      q_search = params[:search].strip
      conditions_string_ary << "AND (test_plans.id = #{q_search.to_i}
                                OR test_plans.name LIKE ?
                                OR product_lines.name LIKE ?
                                OR test_plans.plan_details LIKE ?)"
      3.times do
        conditions_param_values << "\%#{q_search}\%"
      end
    end
    conditions = conditions_string_ary.join(" ")
    
    @test_plans = TestPlan.includes(:product_line).where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 40, :page => params[:page])
    generate_id_sequence_cookie(@test_plans,params[:page])

    if request.xhr?
      render(:partial => "test_plan", :collection => @test_plans)
    end
  end
  
  def show
    @test_plan = TestPlan.find(params[:id])
    
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @test_plan }
    end
  end
  
  def handle
    @test_plan = TestPlan.find(params[:id])
    
    respond_to do |format|
      format.html # maintain.html.erb
      format.xml  { render :xml => @test_plan }
    end
  end

  def approved
    @test_plan = TestPlan.find(params[:id]).use_latest_baseline
    respond_to do |format|
      format.html { render Rails.application.routes.recognize_path(request.referrer)} # latest.html.erb
    end
  end
  
  def new
    @test_plan = TestPlan.new
    @test_plan.status = HandleVersioning::STATUS_NEW
    @plan_details = []
  end
  
  def edit
    @test_plan = TestPlan.find(params[:id])
    @plan_details = JSON.parse(@test_plan.plan_details)
  end
  
  def create
    @test_plan = TestPlan.new(params[:test_plan])
    @test_plan.status = HandleVersioning.statusarray[HandleVersioning::STATUS_DRAFT]
    @test_plan.updated_by = current_user.user_name
    respond_to do |format|
      if @test_plan.save 
        flash[:notice] = 'Test plan was successfully created.'
        format.html { redirect_to(handle_test_plan_path(@test_plan)) }
        format.xml  { render :xml => @test_plan, :status => :created, :location => @test_plan }
      else
        @plan_details = []
        @test_plan.status = HandleVersioning.statusarray[HandleVersioning::STATUS_NEW]
        format.html { render :action => "new" }
        format.xml  { render :xml => @test_plan.errors, :status => :unprocessable_entity }
      end
    end    
  end
  
  def update
    @test_plan = TestPlan.find(params[:id])
    @test_plan.updated_by = current_user.user_name
    @test_plan.assign_attributes(params[:test_plan])
    if @test_plan.plan_details_changed?
      @test_plan.status = HandleVersioning.statusarray[HandleVersioning::STATUS_DRAFT]
    end
    respond_to do |format|
      if @test_plan.save
        flash[:notice] = 'Test plan was successfully updated.'
        format.html { redirect_to(handle_test_plan_path(@test_plan)) }
        format.xml  { head :ok }
      else
        @plan_details = JSON.parse(@test_plan.plan_details)
        format.html { render :action => "edit" }
        format.xml  { render :xml => @test_plan.errors, :status => :unprocessable_entity }
      end
    end    
  end

  def delete_parameter
    if params[:id]
      @test_plan = TestPlan.find(params[:id])
    else
      @test_plan = TestPlan.new
    end
    @test_plan.plan_details = params[:details]
    @plan_details = @test_plan.delete_parameter(params[:test_case_name],params[:parameter_name],false)
    render :partial => "test_cases_form" ,:object => @plan_details
  end

  def add_update_parameter
    if params[:id]
      @test_plan = TestPlan.find(params[:id])
    else
      @test_plan = TestPlan.new
    end
    @test_plan.plan_details = params[:details]
    if params[:old_parameter_name] && (params[:old_parameter_name] !=  params[:parameter_name])
      details = @test_plan.delete_parameter(params[:test_case_name],params[:old_parameter_name],false)
      @test_plan.plan_details = details.to_json
    end
    
    @plan_details = @test_plan.add_update_parameter(params[:test_case_name],params[:parameter_name],params[:parameter_value],false)
    render :partial => "test_cases_form" ,:object => @plan_details
  end

  def delete_test_case
    if params[:id]
      @test_plan = TestPlan.find(params[:id])
    else
      @test_plan = TestPlan.new
    end
    @test_plan.plan_details = params[:details]
    @plan_details = @test_plan.delete_test_case(params[:test_case_name],false)
    render :partial => "test_cases_form" ,:object => @plan_details
  end

  def create_test_case
    if params[:id]
      @test_plan = TestPlan.find(params[:id])
    else
      @test_plan = TestPlan.new
    end
    @test_plan.plan_details = params[:details]
    @plan_details = @test_plan.add_test_case(params[:test_case_name],false)
    render :partial => "test_cases_form" ,:object => @plan_details
  end

  def update_test_case
    if params[:id]
      @test_plan = TestPlan.find(params[:id])
    else
      @test_plan = TestPlan.new
    end
    @test_plan.plan_details = params[:details]
    @plan_details = @test_plan.update_test_case(params[:test_case_name],params[:new_test_case_name],false)
    render :partial => "test_cases_form" ,:object => @plan_details
  end

  def clone
    @org_test_plan = TestPlan.find(params[:id])
    @test_plan = @org_test_plan.clone
    @test_plan.updated_by = current_user.user_name
     respond_to do |format|
     if @test_plan.save
        flash[:notice] = 'Test plan was successfully duplicated.'
        format.html { redirect_to(@test_plan) }
        format.xml  { head :ok }
      else
        format.html { redirect_to :back }
        format.xml  { render :xml => @test_plan.errors, :status => :unprocessable_entity }
      end
    end
  end

  def change_status
    @test_plan = TestPlan.find(params[:id])
    @test_plan.status = params[:status]
    respond_to do |format|
     if @test_plan.save
        flash[:notice] = 'Test plan status was successfully changed.'
        format.html { redirect_to(handle_test_plan_path(@test_plan)) }
        format.xml  { head :ok }
      else
        format.html { redirect_to :back }
        format.xml  { render :xml => @test_plan.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  private
  def sort_column  
    params[:sort] || "test_plans.name"  
  end  
  
  def sort_direction  
    params[:direction] || "asc"  
  end
  
  def search
    params[:search] || ""
  end 
end
