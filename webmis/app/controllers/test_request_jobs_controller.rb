class TestRequestJobsController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search
  # GET /test_request_jobs
  # GET /test_request_jobs.json
  def index
    if params[:reset]
      params.delete :search_status
      params.delete :search
      params.delete :sort
      params.delete :direction

      params[:sort] = sort_column
      params[:direction] = sort_direction
    end

    if params[:sort].blank?
      params[:sort] = sort_column
      params[:direction] = sort_direction
    end

    conditions_string_ary = ["test_requests.project_id = #{session[:project_id]}"]
    conditions_param_values = []

    status = params[:search_status]
    unless status.blank?
      conditions_string_ary << " and (jobs.status = ?)"
      conditions_param_values << status
    end

    conditions = conditions_string_ary.join(" ")

    sql_join = "JOIN test_requests ON test_requests.id = jobs.reference_id"
    
    @test_request_jobs = TestRequestJob.joins(sql_join).where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction)

    unless params[:search].blank?
      search = params[:search].strip
      matched_test_request_jobs = []
      @test_request_jobs.each do |test_request_job|
        if (test_request_job.id.to_s.include?(search))  ||
           (test_request_job.status_str.to_s.include?(search)) ||
           (test_request_job.test_case_name.to_s.include?(search)) ||
           (test_request_job.run_server.to_s.include?(search)) ||
           (test_request_job.start_time && test_request_job.start_time.localtime.strftime("%d %b '%y at %T").include?(search)) ||
           (test_request_job.finish_time && test_request_job.finish_time.localtime.strftime("%d %b '%y at %T").include?(search))
          matched_test_request_jobs << test_request_job
        end
      end
      @test_request_jobs = matched_test_request_jobs
    end

    @test_request_jobs = @test_request_jobs.paginate(per_page: 50, page: params[:page])
    generate_id_sequence_cookie(@test_request_jobs,params[:page])

    if request.xhr?
      render(:partial => "test_request_job", :collection => @test_request_jobs)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @test_request_jobs }
      end
    end
  end

  def failures
    if params[:sort].blank?
      params[:sort] = sort_column
      params[:direction] = sort_direction
    end

    sql_join = "JOIN test_requests ON test_requests.id = jobs.reference_id"

    @test_request_jobs = TestRequestJob.joins(sql_join).order(sort_column + ' ' + sort_direction)
    @test_request_jobs = @test_request_jobs.failures.where("test_requests.project_id = #{session[:project_id]}").paginate(per_page: 30, page: params[:page])

    if request.xhr?
      render(:partial => "failed_test_request_job", :collection => @test_request_jobs)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @test_request_jobs }
      end
    end
  end

  def tagging_fail_reason
    @success_rows = []
    params[:ids].each do |job_id|
      if is_int?(job_id)
        job = TestRequestJob.find(job_id)
        job.fail_category = params[:fail_reason]
        job.fail_category_logged_by_user_id = current_user.id
        job.fail_category_logged_on = Time.now
        job.log_action("Test request job tagged with fail-reason '#{params[:fail_reason]}'.",current_user)
        if job.save
          @success_rows << job.id
        end
      end
    end
    render :layout => false
  end

  # GET /test_request_jobs/1
  # GET /test_request_jobs/1.json
  def show
    @test_request_job = TestRequestJob.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @test_request_job }
    end
  end

  # GET /test_request_jobs/new
  # GET /test_request_jobs/new.json
  def new
    @test_request_job = TestRequestJob.new(:project_id => session[:project_id])

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @test_request_job }
    end
  end

  # GET /test_request_jobs/1/edit
  def edit
    @test_request_job = TestRequestJob.find(params[:id])
    @script = @test_request_job.job_script
  end

  # POST /test_request_jobs
  # POST /test_request_jobs.json
  def create
    @test_request_job = TestRequestJob.new(strong_params)

    respond_to do |format|
      if @test_request_job.save
        format.html { redirect_to @test_request_job, notice: 'Test request job was successfully created.' }
        format.json { render json: @test_request_job, status: :created, location: @test_request_job }
      else
        format.html { render action: "new" }
        format.json { render json: @test_request_job.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /test_request_jobs/1
  # PUT /test_request_jobs/1.json
  def update
    begin
      TestRequestJob.transaction do
        @test_request_job = TestRequestJob.find(params[:id])
        server_name = params[:test_request_job][:run_server].blank? ? '-' : params[:test_request_job][:run_server]

        if params[:commit] == "Execute"
          @test_request_job.stdout_log = "Job is manually executed on server '#{server_name}'."
          @test_request_job.set_status(Job::STATUS_MANUAL)
          perform_now = true
        elsif params[:commit] == "Schedule"
          @test_request_job.stdout_log = "Job is manually queued" + (server_name.blank? ? '.' : " on server '#{server_name}'.")
          @test_request_job.set_status(Job::STATUS_QUEUED)
          perform_now = false
        elsif params[:commit] == "Save"
          @test_request_job.stdout_log = "Job is saved."
          @test_request_job.set_status(Job::STATUS_MANUAL)
          perform_now = false
        else
          raise "Unknown submit operation '#{params[:commit]}'."
        end
        @test_request_job.fail_reason = ''
        @test_request_job.log_scheduler("Job manually set to #{@test_request_job.status_str} for server #{server_name}.", current_user)
        @test_request_job.log_action(@test_request_job.stdout_log, current_user)

        if @test_request_job.job_script && params[:test_request_job][:run_script] != @test_request_job.job_script.to_json
          # script has been changed by the user
          @test_request_job.job_script = JobScript.new(params[:test_request_job][:run_script])
          @test_request_job.job_script.updated_by = current_user.full_name
        end

        respond_to do |format|
          if @test_request_job.update_attributes!(strong_params)
            @test_request_job.perform if perform_now

            format.html { redirect_to @test_request_job, notice: 'Test request job was successfully updated.' }
            format.json { head :no_content }
          else
            flash[:error] = "Error updating job"
            format.html { render action: "edit" }
            format.json { render json: @test_request_job.errors, status: :unprocessable_entity }
          end
        end
      end
    rescue ActiveRecord::StaleObjectError
      flash[:error] = "This job was changed while you were editing it. Please try again."
      redirect_to :action => 'edit'
    end
  end

  # DELETE /test_request_jobs/1
  # DELETE /test_request_jobs/1.json
  def destroy
    @test_request_job = TestRequestJob.find(params[:id])
    @test_request_job.destroy

    respond_to do |format|
      format.html { redirect_to test_request_jobs_url }
      format.json { head :no_content }
    end
  end

  def clean_working_dir
    @test_request_job = TestRequestJob.find(params[:id])
    @test_request_job.clear_working_dir
    redirect_to :action => "show"
  end

  def cancel
    begin
      @test_request_job = TestRequestJob.find(params[:id])
      @test_request_job.lock_version = params[:lock_version]
      @test_request_job.cancel(current_user.full_name)
      Postoffice.test_request_job_status_change(@test_request_job).deliver_now
    rescue ActiveRecord::StaleObjectError
      flash[:error] = "This job was changed while you were cancelling it."
    end
    redirect_to :action => "show"
  end

  def edit_new_copy
    @test_request_job_org = TestRequestJob.find(params[:id])
    @test_request_job_org.lock_version = params[:lock_version] unless params[:lock_version].blank?
    if @test_request_job_org.allow_reschedule?
      begin
        TestRequestJob.transaction do
          @test_request_job = @test_request_job_org.reschedule(current_user, manual_yn = true)
          redirect_to :action => 'edit', :id => @test_request_job.id
        end
      rescue ActiveRecord::StaleObjectError
        flash[:error] = 'This job was changed while you were editing it. Please try again.'
        redirect_to :action => 'show', :id => @test_request_job_org.id
      rescue => e
        flash[:error] = "Error rescheduling: #{e.message}"
        redirect_to :action => 'show', :id => @test_request_job_org.id
      end
    else
      if @test_request_job_org.busy?
        flash[:error] = 'Editing a new copy of this job is not allowed yet. Please wait till move action is finished.'
      else
        flash[:error] = 'Editing a new copy of this job is no longer allowed. The test request it belongs to probably just finished.'
      end
      redirect_to :action => 'show', :id => @test_request_job.id
    end
  end

  def reschedule
    @test_request_job_org = TestRequestJob.find(params[:id])
    @test_request_job_org.lock_version = params[:lock_version] unless params[:lock_version].blank?
    if @test_request_job_org.allow_reschedule?
      begin
        @test_request_job = @test_request_job_org.reschedule(current_user)
      rescue ActiveRecord::StaleObjectError
        flash[:error] = 'This job was changed while you were editing it. Please try again.'
        redirect_to :action => 'show', :id => @test_request_job_org.id
      rescue => e
        flash[:error] = "Error rescheduling: #{e.message}"
        redirect_to :action => 'show', :id => @test_request_job_org.id
      end
      redirect_to :action => 'show', :id => @test_request_job.id
    else
      flash[:error] = 'Rescheduling is no longer allowed. The test request this belongs to probably just finished.'
      redirect_to :action => 'show', :id => @test_request_job_org.id
    end
  end

  def generated_script
    script = JobScript.new(params[:script])
    test_request_job = TestRequestJob.find(params[:id])
    script.updated_by = current_user.full_name unless params[:script] == test_request_job.job_script.to_json
    render :text => script.to_script("TEST_REQUEST_JOB_ID", params[:id])
  end

  private
  def sort_column
    params[:sort] || "jobs.id"
  end

  def sort_direction
    params[:direction] || "desc"
  end

  def search
    params[:search] || ""
  end
end
