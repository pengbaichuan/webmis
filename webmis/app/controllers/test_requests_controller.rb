class TestRequestsController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "application"
  helper_method :sort_column, :sort_direction, :search

  def index
    @search_opts = {:inactive => "0"}
    if params[:reset]
      params.delete :inactive
      params.delete :search
      params.delete :reset
      params.delete :sort
      params.delete :direction
      params.delete :column_search_field
    end

    conditions_string_ary = ["( test_requests.project_id = #{session[:project_id]} )"]
    conditions_param_values = []

    if params[:inactive]
      @search_opts = {:inactive => "1"}
    else
      conditions_string_ary << "(test_requests.status != 'FINISHED' AND test_requests.status != 'CANCELED')"
    end

    @roles = []
    current_user.roles.uniq.collect {|r|@roles << r.title}

    if params["me_myself_and_i"] == '1'
      conditions_string_ary << "( test_requests.assigned_to_user_id = #{current_user.id} )"
    end

    if params[:search] && params[:search] != ""
      q = params[:search].strip
      conditions_string_ary << "( test_requests.product_id LIKE ?
                                 OR test_types.name like ?
                                 OR conversion_databases.path_and_file LIKE ?
                                 OR products.name LIKE ? )"

      4.times do
        conditions_param_values << "\%#{q}\%"
      end
    end

    if !column_search_query.empty?
      conditions_string_ary << "#{column_search_query['query']}"
      conditions_param_values += column_search_query["values"]
    end
    conditions = conditions_string_ary.join(" AND ")

    @test_requests = TestRequest.joins("LEFT JOIN products on products.volumeid = test_requests.product_id").eager_load([:region,:conversion,:test_type,:test_db,:assigned_user]).where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).uniq.paginate(:per_page => 50, :page => params[:page])
    generate_id_sequence_cookie(@test_requests,params[:page])

    if params["me_myself_and_i"] == '1' && @test_requests.total_entries == 0
      flash.now[:warning] = 'No test requests assigned to you.'
    end


    if request.xhr?
      render(:partial => "test_request", :collection => @test_requests)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @test_requests }
      end
    end
  end

  # GET /test_requests/1
  # GET /test_requests/1.xml
  def show
    @test_request = TestRequest.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @test_request }
    end
  end

  def handle
    @test_request = TestRequest.find(params[:id])
    @test_request.actual_start_date = Date.today() if !@test_request.actual_start_date
    @test_request.actual_finish_date = Date.today() if !@test_request.actual_finish_date

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @test_request }
    end
  end

  def change_status
    @test_request = TestRequest.find(params[:id])
    @test_request.status = params[:status]

    if params[:status] == TestRequest::STATUS_RUNNING
      @test_request.actual_start_date = Date.today
      @test_request.actual_finish_date = nil
      @test_request.result_remarks = ''
      @test_request.save!
      flash[:notice] = 'Test marked as started.'
      if @test_request.production_orderline
        @test_request.production_orderline.transition(current_user)
        @test_request.production_orderline.update_bp_message('Test running', current_user)
      end
    end

    if params[:status] == TestRequest::STATUS_FINISHED
      TestRequest.transaction do
        @test_request.actual_finish_date = Date.today
        @test_request.save!
        @test_request.reload
        if @test_request.test_request_jobs.size > 0
            @test_request.cleanup
        end
      end

      Postoffice.result_test_request(params[:id]).deliver_now
      flash[:notice] = 'Result is send to requester.'
      if @test_request.production_orderline
        #@test_request.production_orderline.transition(current_user)
        @test_request.production_orderline.update_bp_message('Test finished',current_user)
      end
    end

    if params[:status] == TestRequest::STATUS_CANCELLED
      @test_request.actual_finish_date = Date.today
      cu = current_user.first_name.to_s + ' ' + current_user.last_name.to_s
      @test_request.result_remarks = 'Canceled by ' + cu  + ' on ' + Date.today.strftime('%d-%m-%Y') + '. <br />' + @test_request.result_remarks.to_s
      @test_request.save!
      @test_request.reload
      if @test_request.test_request_jobs.size > 0
         @test_request.terminate(current_user)
      end
      Postoffice.result_test_request(params[:id]).deliver_now
      flash[:notice] = 'Cancellation is send to requester.'
    end

    if params[:status] == TestRequest::STATUS_REQUESTED
      @test_request.save!
      Postoffice.test_request(params[:id]).deliver_now
      flash[:notice] = 'Request is sent to assigned tester.'
    end

    redirect_to :action => 'handle', :id => params[:id]

  end

  # GET /test_requests/new
  # GET /test_requests/new.xml
  def new
    @test_request = TestRequest.new(:project_id => session[:project_id], :start_date => Date.today, :finish_date => Date.today)
    @test_request.requested_by_user_id = current_user.id

    if params['remote'] && !params[:cid].blank?
      # When from conversion
      @test_request.test_db_conversion_databases_id = ConversionDatabase.available.where(:conversion_id => params[:cid]).first.id
      conversion = Conversion.find(params[:cid])
      @test_request.product_id = conversion.productid.to_s
      @test_request.production_orderline_id  = conversion.production_orderline_id.to_s
    elsif params[:production_orderline_id]
      # When from production order
      production_orderline = ProductionOrderline.find(params[:production_orderline_id])
      @test_request.test_db_conversion_databases_id =  ConversionDatabase.available.where(:conversion_id => production_orderline.chosen_conversion_id).first.id if production_orderline.chosen_conversion
      conversion = production_orderline.chosen_conversion
      @test_request.product_id = production_orderline.product_id
      @test_request.production_orderline_id  = params[:production_orderline_id]
    end
    
    respond_to do |format|
      if @test_request.test_db_conversion_databases_id.blank? && !params[:production_orderline_id].blank? && !params[:return_to_id].blank?
        flash[:error] = 'Test database is not available (yet).'
        format.html { redirect_to(:controller => params[:return_to_controller],:action => params[:return_to_action],:id => params[:return_to_id] ,:notice => 'Test database is not available (yet).') }
      else
        format.html # new.html.erb
      end
      format.xml  { render :xml => @test_request }
    end
  end

  def databases_by_product_id
    #TODO moet weg
    @testdbs = ConversionDatabase.available.where(:product_id => params['pid'])
    #TODO integreren
    @conversion_databases_for_test = ConversionDatabase.available.where(:conversion_id => params[:cid])
    render :partial => "databases_by_product_id"
  end

  # GET /test_requests/1/edit
  def edit
    @test_request = TestRequest.find(params[:id])
    if @test_request.use_scheduler
      editing_not_allowed(@test_request,"Editing of test request with job support is not allowed.")
    end
  end

  # POST /test_requests
  # POST /test_requests.xml
  def create
    @test_request = TestRequest.new(strong_params)
    @test_request.project_id = session[:project_id]
    @test_request.status = 'CREATED'

    # Normalize date from calender UI
    params[:test_request][:start_date] = DateTime.strptime(params[:test_request][:start_date],"%d-%m-%Y") if params[:test_request][:start_date]
    params[:test_request][:finish_date] = DateTime.strptime(params[:test_request][:finish_date],"%d-%m-%Y") if params[:test_request][:finish_date]
    params[:test_request][:actual_start_date] = DateTime.strptime(params[:test_request][:actual_start_date],"%d-%m-%Y") if params[:test_request][:actual_start_date]
    params[:test_request][:actual_finish_date] = DateTime.strptime(params[:test_request][:actual_finish_date],"%d-%m-%Y") if params[:test_request][:actual_finish_date]

    if @test_request.test_plan
      @test_request.run_parallel = true
      @test_request.use_scheduler = true
    end

    respond_to do |format|
      TestRequest.transaction do
        if @test_request.save
          if !@test_request.test_plan_id.blank?
            @test_request.generate_jobs
          end
          if params[:return_to_controller].blank? && params[:return_to_action].blank? && params[:return_to_id].blank?
            format.html { redirect_to(@test_request, :notice => 'TestRequest was successfully created.') }
            format.xml  { render :xml => @test_request, :status => :created, :location => @test_request }
          else
            @test_request.status = 'REQUESTED'
            if @test_request.save
              Postoffice.test_request(@test_request.id).deliver_now
              flash[:notice] = "TestRequest was successfully created and sent to #{@test_request.assigned_user.full_name}."
              if @test_request.production_orderline
                @test_request.production_orderline.transition(@test_request.assigned_user)
                @test_request.production_orderline.update_bp_message("Test requested",@test_request.assigned_user)
              end
            end
            format.html { redirect_to(:controller => params[:return_to_controller],:action => params[:return_to_action],:id => params[:return_to_id] ,:notice => 'TestRequest was successfully created.') }
          end
        else
          format.html { render :action => "new" }
          format.xml  { render :xml => @test_request.errors, :status => :unprocessable_entity }
        end
      end
    end
  end

  # PUT /test_requests/1
  # PUT /test_requests/1.xml
  def update
    @test_request = TestRequest.find(params[:id])
    # Normalize date from calender UI
    params[:test_request][:start_date] = DateTime.strptime(params[:test_request][:start_date],"%d-%m-%Y") if params[:test_request][:start_date]
    params[:test_request][:finish_date] = DateTime.strptime(params[:test_request][:finish_date],"%d-%m-%Y") if params[:test_request][:finish_date]
    params[:test_request][:actual_start_date] = DateTime.strptime(params[:test_request][:actual_start_date],"%d-%m-%Y") if params[:test_request][:actual_start_date]
    params[:test_request][:actual_finish_date] = DateTime.strptime(params[:test_request][:actual_finish_date],"%d-%m-%Y") if params[:test_request][:actual_finish_date]

    respond_to do |format|
      TestRequest.transaction do
        if @test_request.update_attributes(strong_params)
          @test_request.reload

          if @test_request.status == "CANCELED" && @test_request.test_request_jobs.size > 0
            @test_request.cancel
          end

          if @test_request.status == "FINISHED" && @test_request.test_request_jobs.size > 0
            @test_request.cleanup
          end
          
          Postoffice.test_request(@test_request.id,true).deliver_now
          if params[:test_request][:result_remarks].blank?
            format.html { redirect_to(@test_request, :notice => 'TestRequest was successfully updated.') }
          else
            format.html { redirect_to(handle_test_request_path(@test_request), :notice => 'TestRequest was successfully updated.') }
          end
          format.xml  { head :ok }
        else
          format.html { render :action => "edit" }
          format.xml  { render :xml => @test_request.errors, :status => :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /test_requests/1
  # DELETE /test_requests/1.xml
  def destroy
    @test_request = TestRequest.find(params[:id])
    @test_request.destroy

    respond_to do |format|
      format.html { redirect_to(test_requests_url) }
      format.xml  { head :ok }
    end
  end

  def conversion_databases_for_select
    # Infinity scroll for select2
    conditions = '1=1'
    conditions_string_ary = []
    conditions_param_values = []

    unless params[:q].blank?
      params[:q].strip.split('+').each do |q|
        conditions_string_ary << " and (conversion_databases.conversion_id like ?
              or conversion_databases.product_id LIKE ?
               or conversion_databases.filename LIKE ?
                or conversion_databases.db_type LIKE ?)"
        4.times do
          conditions_param_values << "\%#{q}\%"
        end
      end
    end
    conditions = conditions + conditions_string_ary.join(" ")
    @conversion_databases_for_select2 = ConversionDatabase.order(:id,:conversion_id).where([conditions] + conditions_param_values).paginate(:per_page => params[:page_limit], :page => params[:page])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: "{\"total\":#{@conversion_databases_for_select2.total_entries}, \"results\":#{@conversion_databases_for_select2.to_json}}"}
    end
  end

  def render_file_tree
    @conversion_database = ConversionDatabase.find(params[:conversion_database_id])
    @path = @conversion_database.path.to_s
    render_file_browse_modal(@path)
  end

  private
  def sort_column
    params[:sort] || "test_requests.id"
  end

  def sort_direction
    params[:direction] || "desc"
  end

  def search
    params[:search] || ""
  end
end
