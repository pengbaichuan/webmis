class TestToolReleasesController < ApplicationController
  before_filter :authorize
  authorize_resource
  helper_method :sort_column, :sort_direction, :search

  # GET /test_tool_releases
  # GET /test_tool_releases.json
  def index

    @include_inactive = false
    if params[:reset]
      params.delete :search
      params.delete :reset

      params.delete :sort
      params.delete :direction
      params[:inclinactive] = '0'
      @include_inactive = false
    end
    conditions = 'test_tool_releases.active_yn != 0 '
    conditions_string_ary = []
    conditions_param_values = []

    if params[:inclinactive] == '1'
      @include_inactive = true
      conditions = '1=1 '
    end

    unless params[:search].blank?
      conditions_string_ary << "AND (test_tool_releases.release_name LIKE ?)"
      1.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
    end
    conditions = conditions + conditions_string_ary.join(" ")

    @test_tool_releases = TestToolRelease.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])
    generate_id_sequence_cookie(@test_tool_releases,params[:page])

    if request.xhr?
      render(:partial => "test_tool_release", :collection => @test_tool_releases)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json => @test_tool_releases }
      end
    end
  end

  # GET /test_tool_releases/1
  # GET /test_tool_releases/1.json
  def show
    @test_tool_release = TestToolRelease.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @test_tool_release }
    end
  end

  # GET /test_tool_releases/new
  # GET /test_tool_releases/new.json
  def new
    @test_tool_release = TestToolRelease.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @test_tool_release }
    end
  end

  # GET /test_tool_releases/1/edit
  def edit
    @test_tool_release = TestToolRelease.find(params[:id])
  end

  # POST /test_tool_releases
  # POST /test_tool_releases.json
  def create
    @test_tool_release = TestToolRelease.new(strong_params)

    respond_to do |format|
      if @test_tool_release.save
        format.html { redirect_to @test_tool_release, notice: 'Test tool release was successfully created.' }
        format.json { render json: @test_tool_release, status: :created, location: @test_tool_release }
      else
        format.html { render action: "new" }
        format.json { render json: @test_tool_release.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /test_tool_releases/1
  # PUT /test_tool_releases/1.json
  def update
    @test_tool_release = TestToolRelease.find(params[:id])

    respond_to do |format|
      if @test_tool_release.update_attributes(strong_params)
        format.html { redirect_to @test_tool_release, notice: 'Test tool release was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @test_tool_release.errors, status: :unprocessable_entity }
      end
    end
  end

  def sync
    TestToolRelease.sync_with_filesystem
    params.delete("_method")  # we don't want to pass around the action and 'post' method, just the regular parameters
    params.delete("action")
    respond_to do |format|
      format.html { redirect_to test_tool_releases_url(params), :notice => "Active tools synchronized with the file system." }
      format.xml  { head :ok }
    end
  end


  private
  def sort_column
    params[:sort] || "test_tool_releases.id"
  end

  def sort_direction
    params[:direction] || "desc"
  end

  def search
    params[:search] || ""
  end
end
