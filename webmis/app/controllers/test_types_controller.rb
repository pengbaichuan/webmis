class TestTypesController < ApplicationController
  before_filter :authorize
  authorize_resource param_method: :strong_params
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search

  # GET /test_types
  # GET /test_types.xml
#  before_filter :authorize
#  access_control :DEFAULT => 'admin',
#    :index => 'operations engineer | readonly | admin',
#    :show  => 'operations engineer | readonly | admin' 
  
  layout "webmis"
  
  def index
    conditions = "(test_types.removed_yn = 0)"
    @deleted = false

    if params[:reset]
      params.delete :search
      params.delete :reset

      if params[:removed] == "1"
        @deleted = true
      end

      params[:sort] = 'name'
      params[:direction] = 'asc'
    end

    if ( params[:removed] == "1" && params[:search] )
      @deleted = true
      conditions = "(1=1)"
    end
    conditions_string_ary = []
    conditions_param_values = []

    unless params[:search].blank?
      conditions_string_ary << "AND (test_types.id = #{params[:search].to_i} OR
                                 test_types.name LIKE ? OR
                                 test_types.description LIKE ?)"
      2.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
    end

    conditions = conditions + conditions_string_ary.join(" ")
    @test_types = TestType.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])
    generate_id_sequence_cookie(@test_types,params[:page])

    if request.xhr?
      flash.keep[:notice]
      render(:partial => "test_type", :collection => @test_types)
    end
  end

  # GET /test_types/1
  # GET /test_types/1.xml
  
  def show
    @test_type = TestType.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @test_type }
    end
  end

  def handle
    @test_type = TestType.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @test_type }
    end
  end

  # GET /test_types/new
  # GET /test_types/new.xml
  def new
    @test_type = TestType.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @test_type }
    end
  end

  # GET /test_types/1/edit
  def edit
    @test_type = TestType.find(params[:id])
  end

  # POST /test_types
  # POST /test_types.xml
  def create
    @test_type = TestType.new(strong_params)
    @test_type.active_yn = true
    @test_type.removed_yn = false
    
    respond_to do |format|
      if @test_type.save
        format.html { redirect_to(@test_type, :notice => 'TestType was successfully created.') }
        format.xml  { render :xml => @test_type, :status => :created, :location => @test_type }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @test_type.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /test_types/1
  # PUT /test_types/1.xml
  def update
    @test_type = TestType.find(params[:id])

    respond_to do |format|
      if @test_type.update_attributes(strong_params)
        format.html { redirect_to(@test_type, :notice => 'TestType was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @test_type.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /test_types/1
  # DELETE /test_types/1.xml
  def destroy
    @test_type = TestType.find(params[:id])
    @test_type.destroy

    respond_to do |format|
      format.html { redirect_to(test_types_url) }
      format.xml  { head :ok }
    end
  end

def remove
    @test_type = TestType.find(params[:id])
    @test_type.removed_yn = true
    @test_type.active_yn = false

    respond_to do |format|
      if @test_type.save
        format.html { redirect_to handle_test_type_path(@test_type), notice: 'Test type was successfully removed.' }
        format.json { head :no_content }
      else
        format.html { render action: "handle" }
        format.json { render json: @test_type.errors, status: :unprocessable_entity }
      end
    end
  end

  def restore
    @test_type = TestType.find(params[:id])
    @test_type.removed_yn = false
    @test_type.active_yn = true

    respond_to do |format|
      if @test_type.save
        format.html { redirect_to handle_test_type_path(@test_type), notice: 'Test type was successfully restored.' }
        format.json { head :no_content }
      else
        format.html { render action: "handle" }
        format.json { render json: @test_type.errors, status: :unprocessable_entity }
      end
    end
  end

  def activate
    @test_type = TestType.find(params[:id])
    @test_type.active_yn = true
    @test_type.removed_yn = false

    respond_to do |format|
      if @test_type.save
        format.html { redirect_to handle_test_type_path(@test_type), notice: 'Test type was successfully activated.' }
        format.json { head :no_content }
      else
        format.html { render action: "handle" }
        format.json { render json: @test_type.errors, status: :unprocessable_entity }
      end
    end
  end

  def deactivate
    @test_type = TestType.find(params[:id])
    @test_type.active_yn = false

    respond_to do |format|
      if @test_type.save
        format.html { redirect_to handle_test_type_path(@test_type), notice: 'Test type was successfully deactivated.' }
        format.json { head :no_content }
      else
        format.html { render action: "handle" }
        format.json { render json: @test_type.errors, status: :unprocessable_entity }
      end
    end
  end


  private
  def sort_column
    params[:sort] || "name"
  end

  def sort_direction
    params[:direction] || "asc"
  end

  def search
    params[:search] || ""
  end 
end
