class TextBlocksController < ApplicationController

  before_filter :authorize
  authorize_resource
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search

  # GET /text_blocks
  # GET /text_blocks.json
  def index
    if params[:reset]
      params.delete :removed
      params.delete :search
      params.delete :reset
      params.delete :sort
      params.delete :direction
      params.delete :column_search_field
    end

    conditions_string_ary = []
    conditions_param_values = []

    if params[:removed]
      @removed = true
      conditions_string_ary << "1=1"
    else
      conditions_string_ary << "(text_blocks.status != '#{TextBlock::STATUS_OBSOLETE}')"
    end

    unless params[:search].blank?
      q = params[:search].strip
      conditions_string_ary << "(text_blocks.id = ?
                                OR text_blocks.name LIKE ?
                                OR text_blocks.content LIKE ?)"
      conditions_param_values << q.to_i # only for id
      2.times do
        conditions_param_values << "%#{q}%"
      end
    end

    if !column_search_query.empty?
      conditions_string_ary << "#{column_search_query['query']}"
      conditions_param_values +=  column_search_query["values"]
    end
    conditions = conditions_string_ary.join( ' AND ' )

    @text_blocks = TextBlock.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])
    generate_id_sequence_cookie(@text_blocks,params[:page])

    if request.xhr?
      render(:partial => "text_block", :collection => @text_blocks)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @text_blocks }
      end
    end
  end

  # GET /text_blocks/1
  # GET /text_blocks/1.json
  def show
    @text_block = TextBlock.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @text_block }
    end
  end

  # GET /text_blocks/1/handle
  def handle
    @text_block = TextBlock.find(params[:id])

    respond_to do |format|
      format.html # handle.html.erb
      format.json { render json: @text_block }
    end
  end

  # GET /text_blocks/new
  # GET /text_blocks/new.json
  def new
    @text_block = TextBlock.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @text_block }
    end
  end

  # GET /text_blocks/1/edit
  def edit
    @text_block = TextBlock.find(params[:id])
  end

  # POST /text_blocks
  # POST /text_blocks.json
  def create
    @text_block = TextBlock.new(strong_params)

    respond_to do |format|
      if @text_block.save
        format.html { redirect_to @text_block, notice: 'Text block was successfully created.' }
        format.json { render json: @text_block, status: :created, location: @text_block }
      else
        format.html { render action: "new" }
        format.json { render json: @text_block.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /text_blocks/1
  # PUT /text_blocks/1.json
  def update
    @text_block = TextBlock.find(params[:id])

    respond_to do |format|
      if @text_block.update_attributes(strong_params)
        format.html { redirect_to @text_block, notice: 'Text block was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @text_block.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /text_blocks/1
  # DELETE /text_blocks/1.json
  def destroy
    @text_block = TextBlock.find(params[:id])
    @text_block.destroy

    respond_to do |format|
      format.html { redirect_to text_blocks_url }
      format.json { head :no_content }
    end
  end

  def change_status
    @text_block = TextBlock.find(params[:id])
    respond_to do |format|
      if @text_block.change_status(params[:status])
        format.html { redirect_to :back, notice: "text block was successfully changed to #{@text_block.status_string}." }
        format.json { render json: @text_block, status: :created, location: @text_block }
      else
        format.html { render action: "show" }
        format.json { render json: @text_block.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  def sort_column
    params[:sort] || "text_blocks.name"
  end

  def sort_direction
    params[:direction] || "asc"
  end

  def search
    params[:search] || ""
  end
end
