class UpdateRegionsController < ApplicationController

  before_filter :authorize
  authorize_resource
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search

  # index not used as this is handled by the regions controller
  def index
    if params[:reset]
      params.delete :q
      params.delete :reset
      
      params[:sort] = 'name'
      params[:direction] = 'asc'
    end
    conditions = "1=1 "
    conditions_string_ary = []
    conditions_param_values = []
    
    unless params[:q].blank?
      conditions_string_ary << " and (regions.name LIKE ?)"
      1.times do
        conditions_param_values << "\%#{params[:q].strip}\%"
      end
    end
    conditions = conditions + conditions_string_ary.join(" ")
    @update_regions = UpdateRegion.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 50, :page => params[:page])
    
    if request.xhr?
      flash.keep[:notice]
      render(:partial => "update_region", :collection => @update_regions)
    end      
  end

  def show
    @update_region = UpdateRegion.find(params[:id])
  end

  def new
    @update_region = UpdateRegion.new
    @update_region.isactive = true
  end


  def edit
    @update_region = UpdateRegion.find(params[:id])
  end

  def create
    @update_region = UpdateRegion.new(strong_params)
    @update_region.display_type_id = AreaType.where(:name => 'UpdateRegion').last.id

    respond_to do |format|
      if @update_region.save
        @update_region.app_module_ids = [AppModule.where(:abbr => 'MIS').last.id]
        format.html { redirect_to(@update_region, :notice => 'UpdateRegion was successfully created.') }
        format.xml  { render :xml => @update_region, :status => :created, :location => @update_region }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @update_region.errors, :status => :unprocessable_entity }
      end
    end    
  end

  def update
    @update_region = UpdateRegion.find(params[:id])
    @update_region.app_module_ids = [AppModule.where( :abbr => 'MIS').last.id]
    @update_region.display_type_id = AreaType.where(:name => 'UpdateRegion').last.id

    respond_to do |format|
      if @update_region.update_attributes(strong_params)
        format.html { redirect_to(@update_region, :notice => 'UpdateRegion was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @update_region.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @update_region = UpdateRegion.find(params[:id])
    @update_region.destroy
    flash[:notice] = "UpdateRegion succesfully deleted!"
    respond_to do |format|
      format.html { redirect_to(UpdateRegions_url) }
      format.xml  { head :ok }
    end
  end

  def generate_dakota_id
    render :text => Region.generate_dakota_id
  end
  
  
  private 
  def sort_column
    params[:sort] || "name"
  end
  
  def sort_direction
    params[:direction] || "asc"
  end
  
  def search
    params[:search] || ""
  end  
end
