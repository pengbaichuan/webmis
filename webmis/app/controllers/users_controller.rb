class UsersController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout 'application'
  helper_method :sort_column, :sort_direction, :search
  
  def index
    @search_opts = {:inactive => "0"}
    if params[:reset]
      params.delete :inactive
      params.delete :search
      params.delete :reset
      params.delete :sort
      params.delete :direction
      params.delete :column_search_field
    end

    conditions_string_ary = []
    conditions_param_values = []

    if params[:inactive]
      @search_opts = {:inactive => "1"}
      conditions_string_ary << "1=1"
    else
      conditions_string_ary << "(job_title != 'Exit' OR job_title IS NULL)"
    end
    
    if params[:search] && params[:search] != ""
      q = params[:search].strip
      conditions_string_ary << "(id = ?
                    OR user_name LIKE ?
                    OR first_name LIKE ?
                    OR last_name  LIKE ?
                    OR job_title LIKE ?
                    OR email LIKE ?)"
      conditions_param_values << q.to_i # only for id
      5.times do
        conditions_param_values << "%#{q}%"
      end
    end

    if !column_search_query.empty?
      conditions_string_ary << "#{column_search_query['query']}"
      conditions_param_values +=  column_search_query["values"]
    end
    conditions = conditions_string_ary.join( ' AND ' )

    @users = User.where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction).paginate(:per_page => 100, :page => params[:page])
    generate_id_sequence_cookie(@users,params[:page])
    
    if request.xhr?
     render(:partial => "user", :collection => @users)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.xml  { render :xml => @users }
      end
    end
  end

  def show
    @user = User.find(params[:id])
  end

  def new
    @roles_user = RolesUser.new
    @user = User.find(params[:id])
  end

  def new_event
    @user_mail = UserMail.new
    @user = User.find(params[:id])
  end

  def edit
    @user = User.find(params[:id])
    @numberofroles = 0
    if @user.roles_users
    	@roles = @user.roles_users
      @numberofroles = @user.roles_users.length
    end
    @numberofevents= @user.user_mails.length
    if @user.user_mails
	    @user_mails = @user.user_mails
    end
  end

  def create
    @user = User.new(strong_params)
    @user.save
    redirect_to :action => :index
  end

  def update
    @user = User.find(params[:id])
    respond_to do |format|
      if @user.update_attributes(strong_params)
        flash[:notice] = 'User was successfully updated.'
        format.html { redirect_to(@user) }
        format.xml  { head :ok }
      else
        format.html { render action: "edit" }
        format.xml  { render xml: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy
    redirect_to :action => :index
  end

  def delete_user_role
    @user = User.find(params[:id])
    RolesUser.transaction do
      begin
        @roles_user = RolesUser.where(role_id:params[:rid],user_id:@user.id)
        @roles_user.each do |ru|
          ru.destroy
        end
        render :partial => "roles_select", :object => @user
      rescue => e
        render :text => "ERROR: #{e.message}"
      end
    end
  end

  def add_user_role
    @user = User.find(params[:id])
    RolesUser.transaction do
      begin
        @roles_user = RolesUser.new({:role_id => params[:rid],:user_id => @user.id,:module => "MIS"})
        @roles_user.save!
        render :partial => "roles_select", :object => @user
      rescue => e
        render :text => "ERROR: #{e.message}"
      end
    end
  end

  def delete_user_mail
    @user = User.find(params[:id])
    UserMail.transaction do
      begin
        user_mail = UserMail.where(mail_event_id:params[:umid],user_id:@user.id)
        user_mail.each do |mc|
          mc.mail_conditions.each do |umc|
            umc.destroy
          end
          mc.destroy
        end     
        render :partial => "mail_events_select", :object => @user
      rescue => e
        render :text => "ERROR: #{e.message}"
      end
    end
  end

  def add_user_mail
    @user = User.find(params[:id])
    UserMail.transaction do
      begin
        user_mail = UserMail.new({:mail_event_id => params[:umid],:user_id => @user.id})
        user_mail.save!
        render :partial => "mail_events_select", :object => @user
      rescue => e
        render :text => "ERROR: #{e.message}"
      end
    end
  end


  def save_new_role
    @roles_user = RolesUser.new(params[:roles_user])
    @roles_user.user_id = params[:user_id]
    @roles_user.save
    redirect_to :action => 'edit', :id => @roles_user.user_id
  end

  def save_new_event
    @user_mail = UserMail.new(params[:user_mail])
    @user_mail.user_id = params[:user_id]
    @user_mail.save
    redirect_to :action => 'edit', :id => @user_mail.user_id
  end
  
  def edit_user_mail_event
    @user = User.find(params[:id])
    @user_mail = UserMail.find_by_mail_event_id_and_user_id(params[:umid],@user.id)

    @user_mail_conditions =  UserMailCondition.new
    @user_conditions = UserMailCondition.where(users_mail_id:@user_mail.id)
    @mail_conditions = MailCondition.where(:field => "DataType").collect {|c| [c.field_value]}
  end
  
  def save_user_mail_event
    @user_mail = UserMail.find(params[:user_mail_id])
    @field_values = Array.new
    params.each { |key, val| @field_values << val if (key.to_s == val.to_s)}
    @old_user_mail_conditions = UserMailCondition.where(:users_mail_id => params[:user_mail_id])
    @old_user_mail_conditions.each {|d| d.destroy}
    @field_values.each do |f|
      @user_mail_conditions = UserMailCondition.new
      @user_mail_conditions.users_mail_id = params[:user_mail_id]
      @user_mail_conditions.mail_conditions_id = MailCondition.find_by_field_value("#{f}").id
      @user_mail_conditions.save
    end
           
    flash[:notice] = "Conditions for mail-event \"#{@user_mail.mail_event.name}\" succesfully updated."
    redirect_to :action => 'edit', :id => @user_mail.user_id
  end
  
  def generate_users_ldap
    message = User.sync_ldap_users
    if message != ""
      flash[:notice] = message.gsub('\n','<br />')
    else
      flash[:notice] = "No new users found."
    end
    redirect_to :action => :index
  end
  
  def exit_user
    @user = User.find(params[:id])
    @user.job_title = 'Exit'
    @user.save
    
    flash[:notice] = "Succesfully disabled #{@user.full_name}."
    redirect_to :action => :index
  end
  
  def user_call
    begin
      @user = User.find(params[:id])
      @user.user_call(current_user)
      render :text => "Established connection."
    rescue => e
      render :text => "ERROR: #{e.message}"
    end
  end
  
  private  
  def sort_column  
    params[:sort] || "user_name"  
  end  
    
  def sort_direction  
    params[:direction] || "asc"  
  end
  
  def search
    params[:search] || ""
  end  

end
