class ValidationErrorsController < ApplicationController
  # the CRUD operations for ValidationError are mainly included because they were trivial
  # and it might ease development/debugging in some cases. The WebMIS U.I. does not contain
  # a link to them.

  before_filter :authorize
  authorize_resource
  layout 'webmis'
  
  # GET /validation_errors
  # GET /validation_errors.json
  def index
    @validation_errors = ValidationError.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @validation_errors }
    end
  end

  # GET /validation_errors/1
  # GET /validation_errors/1.json
  def show
    @validation_error = ValidationError.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @validation_error }
    end
  end

  # GET /validation_errors/new
  # GET /validation_errors/new.json
  def new
    @validation_error = ValidationError.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @validation_error }
    end
  end

  # GET /validation_errors/1/edit
  def edit
    @validation_error = ValidationError.find(params[:id])
  end

  # POST /validation_errors
  # POST /validation_errors.json
  def create
    @validation_error = ValidationError.new(strong_params)

    respond_to do |format|
      if @validation_error.save
        format.html { redirect_to @validation_error, notice: 'Validation error was successfully created.' }
        format.json { render json: @validation_error, status: :created, location: @validation_error }
      else
        format.html { render action: "new" }
        format.json { render json: @validation_error.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /validation_errors/1
  # PUT /validation_errors/1.json
  def update
    @validation_error = ValidationError.find(params[:id])

    respond_to do |format|
      if @validation_error.update_attributes(strong_params)
        format.html { redirect_to @validation_error, notice: 'Validation error was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @validation_error.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /validation_errors/1
  # DELETE /validation_errors/1.json
  def destroy
    @validation_error = ValidationError.find(params[:id])
    @validation_error.destroy

    respond_to do |format|
      format.html { redirect_to validation_errors_url }
      format.json { head :no_content }
    end
  end
end
