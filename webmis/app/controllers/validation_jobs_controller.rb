class ValidationJobsController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"
  helper_method :sort_column, :sort_direction, :search
  # GET /validation_jobs
  # GET /validation_jobs.json
  def index
    if params[:reset]
      params.delete :search_status
      params.delete :search
      params.delete :sort
      params.delete :direction

      # when from jobs/tagged_failed_jobs_report
      params.delete :date_range_a
      params.delete :date_range_b
      params.delete :fail_category

      params[:sort] = sort_column
      params[:direction] = sort_direction
    end

    if params[:sort].blank?
      params[:sort] = sort_column
      params[:direction] = sort_direction
    end

    conditions_string_ary = ["production_orders.project_id = #{session[:project_id]}"]
    conditions_param_values = []

    status = params[:search_status]
    unless status.blank?
      conditions_string_ary << " and (jobs.status = ?)"
      conditions_param_values << status
    end

    conditions = conditions_string_ary.join(" ")

    sql_join = 
          "JOIN conversions ON conversions.id = conversion_databases.conversion_id
          JOIN production_orderlines on production_orderlines.id = conversions.production_orderline_id
          JOIN production_orders on production_orderlines.production_order_id = production_orders.id"

    # when from jobs/tagged_failed_jobs_report
    unless params[:fail_category].blank?
      range_end = Date.today.strftime("%d-%m-%Y")
      unless params[:date_range_a].blank?
        range_begin = params[:date_range_a]
        if !params[:date_range_b].blank?
          range_end = params[:date_range_b]
        end
      else
        range_begin = ValidationJob.fail_reason_tagged.first.fail_category_logged_on.strftime("%d-%m-%Y")
      end

      unless params[:date_range_b].blank?
        if params[:date_range_a].blank?
          range_end = Date.today.strftime("%d-%m-%Y")
        end
      end
      conditions += " AND (jobs.fail_category_logged_on BETWEEN '#{DateTime.parse(range_begin)}' AND '#{DateTime.parse(range_end)}')"
      if params[:fail_category] != 'all'
        conditions += " AND (jobs.fail_category = ?)"
        conditions_param_values << params[:fail_category]
      end
    end



    unless params[:search].blank?
      search = params[:search].strip
      conditions += " AND (jobs.id LIKE ? or conversion_databases.id LIKE ? or conversion_databases.path_and_file LIKE ? or jobs.run_server LIKE ?) "
      4.times do
        conditions_param_values << "%#{search}%"
      end
    end

    
    @validation_jobs = ValidationJob.joins(sql_join).eager_load(:conversion_database).where([conditions] + conditions_param_values).order(sort_column + ' ' + sort_direction)
    @validation_jobs = @validation_jobs.paginate(per_page: 50, page: params[:page])
    generate_id_sequence_cookie(@validation_jobs,params[:page])

    if request.xhr?
      render(:partial => "validation_job", :collection => @validation_jobs)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @validation_jobs }
      end
    end
  end

  def failures
    if params[:sort].blank?
      params[:sort] = sort_column
      params[:direction] = sort_direction
    end

    sql_join = "JOIN conversion_databases ON conversion_databases.id = jobs.reference_id
          JOIN conversions ON conversions.id = conversion_databases.conversion_id
          JOIN production_orderlines on production_orderlines.id = conversions.production_orderline_id
          JOIN production_orders on production_orderlines.production_order_id = production_orders.id"

    @validation_jobs = ValidationJob.joins(sql_join).includes(:conversion_database).order(sort_column + ' ' + sort_direction)
    @validation_jobs = @validation_jobs.failures.where("production_orders.project_id = #{session[:project_id]}").paginate(per_page: 30, page: params[:page])

    if request.xhr?
      render(:partial => "failed_validation_job", :collection => @validation_jobs)
    else
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @validation_jobs }
      end
    end
  end

  def tagging_fail_reason
    @success_rows = []
    params[:ids].each do |job_id|
      if is_int?(job_id)
        job = ValidationJob.find(job_id)
        job.fail_category = params[:fail_reason]
        job.fail_category_logged_by_user_id = current_user.id
        job.fail_category_logged_on = Time.now
        job.log_action("Validation job tagged with fail-reason '#{params[:fail_reason]}'.",current_user)
        if job.save
          @success_rows << job.id
        end
      end
    end
    render :layout => false
  end

  # GET /validation_jobs/1
  # GET /validation_jobs/1.json
  def show
    @validation_job = ValidationJob.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @validation_job }
    end
  end

  # GET /validation_jobs/new
  # GET /validation_jobs/new.json
  def new
    @validation_job = ValidationJob.new(:project_id => session[:project_id])

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @validation_job }
    end
  end

  # GET /validation_jobs/1/edit
  def edit
    @validation_job = ValidationJob.find(params[:id])
    @script = @validation_job.job_script
  end

  # POST /validation_jobs
  # POST /validation_jobs.json
  def create
    @validation_job = ValidationJob.new(params[:validation_job])

    respond_to do |format|
      if @validation_job.save
        format.html { redirect_to @validation_job, notice: 'Validation job was successfully created.' }
        format.json { render json: @validation_job, status: :created, location: @validation_job }
      else
        format.html { render action: "new" }
        format.json { render json: @validation_job.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /validation_jobs/1
  # PUT /validation_jobs/1.json
  def update
    begin
      ValidationJob.transaction do
        @validation_job = ValidationJob.find(params[:id])
        server_name = params[:validation_job][:run_server].blank? ? '-' : params[:validation_job][:run_server]

        if params[:commit] == "Execute"
          @validation_job.stdout_log = "Job is manually executed on server '#{server_name}'."
          @validation_job.set_status(Job::STATUS_MANUAL)
          perform_now = true
        elsif params[:commit] == "Schedule"
          @validation_job.stdout_log = "Job is manually queued" + (server_name.blank? ? '.' : " on server '#{server_name}'.")
          @validation_job.set_status(Job::STATUS_QUEUED)
          perform_now = false
        elsif params[:commit] == "Save"
          @validation_job.stdout_log = "Job is saved."
          @validation_job.set_status(Job::STATUS_MANUAL)
          perform_now = false
        else
          raise "Unknown submit operation '#{params[:commit]}'."
        end
        @validation_job.fail_reason = ''
        @validation_job.log_scheduler("Job manually set to #{@validation_job.status_str} for server '#{server_name}.", current_user)
        @validation_job.log_action(@validation_job.stdout_log, current_user)

        if @validation_job.job_script && params[:validation_job][:run_script] != @validation_job.job_script.to_json
          # script has been changed by the user
          @validation_job.job_script = JobScript.new(params[:validation_job][:run_script])
          @validation_job.job_script.updated_by = current_user.full_name
        end

        respond_to do |format|
          if @validation_job.update_attributes!(strong_params)

            @validation_job.perform if perform_now

            format.html { redirect_to @validation_job, notice: 'Validation job was successfully updated.' }
            format.json { head :no_content }
          else
            flash[:error] = "Error updating job"
            format.html { render action: "edit" }
            format.json { render json: @validation_job.errors, status: :unprocessable_entity }
          end
        end
      end
    rescue ActiveRecord::StaleObjectError
      flash[:error] = "This job was changed while you were editing it. Please try again."
      redirect_to :action => 'edit'
    end
  end

  # DELETE /validation_jobs/1
  # DELETE /validation_jobs/1.json
  def destroy
    @validation_job = ValidationJob.find(params[:id])
    @validation_job.destroy

    respond_to do |format|
      format.html { redirect_to validation_jobs_url }
      format.json { head :no_content }
    end
  end

  def clean_working_dir
    @validation_job = ValidationJob.find(params[:id])
    @validation_job.clear_working_dir
    redirect_to :action => "show"
  end

  def cancel
    begin
      @validation_job = ValidationJob.find(params[:id])
      @validation_job.lock_version = params[:lock_version]
      @validation_job.cancel(current_user.full_name)
    
    rescue ActiveRecord::StaleObjectError
      flash[:error] = "This job was changed while you were cancelling it."
    end
    redirect_to :action => "show"
  end

  def edit_new_copy
    @validation_job_org = ValidationJob.find(params[:id])
    @validation_job_org.lock_version = params[:lock_version] unless params[:lock_version].blank?
    if @validation_job_org.allow_reschedule?
      begin
        ValidationJob.transaction do
          @validation_job = @validation_job_org.reschedule(current_user, manual_yn = true)
           redirect_to :action => 'edit', :id => @validation_job.id
        end
      rescue ActiveRecord::StaleObjectError
        flash[:error] = 'This job was changed while you were editing it. Please try again.'
        redirect_to :action => 'show', :id => @validation_job_org.id
      rescue => e
        flash[:error] = "Error rescheduling: #{e.message}"
        redirect_to :action => 'show', :id => @validation_job_org.id
      end
    else
      if @validation_job_org.busy?
        flash[:error] = 'Editing a new copy of this job is not allowed yet. Please wait till move action is finished.'
      else
        flash[:error] = 'Editing a new copy of this job is no longer allowed. The conversion database it belong to probably just changed.'
      end
      redirect_to :action => 'show', :id => @validation_job_org.id
    end
  end

  def reschedule
    @validation_job_org = ValidationJob.find(params[:id])
    @validation_job_org.lock_version = params[:lock_version] unless params[:lock_version].blank?
    if @validation_job_org.allow_reschedule?
      begin
        @validation_job = @validation_job_org.reschedule(current_user)
      rescue ActiveRecord::StaleObjectError
        flash[:error] = 'This job was changed while you were editing it. Please try again.'
        redirect_to :action => 'show', :id => @validation_job_org.id
      rescue => e
        flash[:error] = "Error rescheduling: #{e.message}"
        redirect_to :action => 'show', :id => @validation_job_org.id
      end
      redirect_to :action => 'show', :id => @validation_job.id
    else
      flash[:error] = 'Rescheduling is no longer allowed. The conversion database this belongs to probably just changed.'
      redirect_to :action => 'show', :id => @validation_job_org.id
    end
  end

  def generated_script
    script = JobScript.new(params[:script])
    validation_job = ValidationJob.find(params[:id])
    script.updated_by = current_user.full_name unless params[:script] == validation_job.job_script.to_json
    render :text => script.to_script("VALIDATION_JOB_ID", params[:id])
  end

  private
  def sort_column
    params[:sort] || "jobs.id"
  end

  def sort_direction
    params[:direction] || "desc"
  end

  def search
    params[:search] || ""
  end
end
