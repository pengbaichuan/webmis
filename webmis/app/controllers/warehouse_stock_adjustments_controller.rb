class WarehouseStockAdjustmentsController < ApplicationController
  before_filter :authorize
  authorize_resource
  layout "webmis"

  # GET /warehouse_stock_adjustments
  # GET /warehouse_stock_adjustments.json  
  def index
    redirect_to :action => 'onhand'
  end
  
  def onhand
    @warehouse_stock_adjustments = WarehouseStockAdjustment.all
    @article = '0'
    @stock_order = '0'
    conditions = "1=1"
    if !params['article_id'].blank?
        @article = params['article_id']
        conditions_a = "article_id='#{@article}'"
        conditions = conditions_a
    else params['article_id'] = ''
    end
    
    
    if !params['stock_order_id'].blank?
        @stock_order = params['stock_order_id']
        conditions_s = "stock_order_id='#{@stock_order}'"
        if conditions_a
            conditions = "#{conditions_a} and #{conditions_s}"
        else
            conditions = conditions_s
        end
    end
    
     @warehouse_stock_adjustments = WarehouseStockAdjustment.where(conditions)
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @warehouse_stock_adjustments }
    end
  end

  # GET /warehouse_stock_adjustments/1
  # GET /warehouse_stock_adjustments/1.json
  def show
    @warehouse_stock_adjustment = WarehouseStockAdjustment.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @warehouse_stock_adjustment }
    end
  end

  # GET /warehouse_stock_adjustments/new
  # GET /warehouse_stock_adjustments/new.json
  def new
    @warehouse_stock_adjustment = WarehouseStockAdjustment.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @warehouse_stock_adjustment }
    end
  end

  

  # POST /warehouse_stock_adjustments
  # POST /warehouse_stock_adjustments.json
  def create
    @warehouse_stock_adjustment = WarehouseStockAdjustment.new(strong_params)

    respond_to do |format|
      
        begin
          WarehouseStockAdjustment.transaction do
            if @warehouse_stock_adjustment.save
              @warehouse_stock_adjustment.adjust
              format.html { redirect_to @warehouse_stock_adjustment, notice: 'Warehouse stock adjustment was successfully created.' }
              format.json { render json: @warehouse_stock_adjustment, status: :created, location: @warehouse_stock_adjustment }
            else
              format.html { render action: "new" }
              format.json { render json: @warehouse_stock_adjustment.errors, status: :unprocessable_entity }
            end
          end
        rescue => e
          @warehouse_stock_adjustment.errors[:base] << e.message
          format.html { render action: "new" }
          format.json { render json: @warehouse_stock_adjustment.errors, status: :unprocessable_entity }
        end
    end
  end

 

  # DELETE /warehouse_stock_adjustments/1
  # DELETE /warehouse_stock_adjustments/1.json
  def destroy
    @warehouse_stock_adjustment = WarehouseStockAdjustment.find(params[:id])
    @warehouse_stock_adjustment.destroy

    respond_to do |format|
      format.html { redirect_to warehouse_stock_adjustments_url }
      format.json { head :no_content }
    end
  end
end
