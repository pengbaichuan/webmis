class WarehouseStockController < ApplicationController
    before_filter :authorize
    authorize_resource
    layout 'webmis'
#    
#    access_control :DEFAULT => 'Logistics Admin', [:index] => 'all'
    
  
  # GET /warehouse_stock
  # GET /warehouse_stock.xml
    def onhand
    @amount_forecast = ShippingOrderline.where(:status => '10').sum(:amount)
    @article = '0'
    @stock_order = '0'
    conditions = ''

      if params['article_id'] != ''
        @article = params['article_id']
        conditions_a = "article_id='#{@article}'"
        conditions = conditions_a
        @amount_forecast = ShippingOrderline.where(:status => '10', :article_id => @article).sum(:amount)
      else params['article_id'] = ''
      end
      
      if params['stock_order_id'] != ''
        @stock_order = params['stock_order_id']
        conditions_s = "stock_order_id='#{@stock_order}'"
        if conditions_a
            conditions = "#{conditions_a} and #{conditions_s}"
        else
            conditions = conditions_s
        end
      end
    if params['stock_order_id'] != ''
      @sum_by_article_id = WarehouseStock.where(conditions).group('article_id').collect{|s|[s.article_id,
          WarehouseStock.where("article_id='#{s.article_id}'and stock_order_id = '#{s.stock_order_id}' and status = 20").sum("quantity")]}
    else
      @sum_by_article_id = WarehouseStock.where(conditions).group('article_id').collect{|s|[s.article_id,
          WarehouseStock.where("article_id='#{s.article_id}'and status = 20").sum("quantity")]}
    end
    
    @filter_result = WarehouseStock.where(conditions).group('article_id,stock_order_id').collect{|s|[s.article_id,
        WarehouseStock.where("article_id='#{s.article_id}' and stock_order_id = '#{s.stock_order_id}' and status = 20").sum("quantity"),
      s.stock_order.purchase_reference,StockOrderline.where(:stock_order_id => s.stock_order.id).sum(:amount_order)]}

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @warehouse_stock }
    end
  end
  
  def index
    @amount_forecast = ShippingOrderline.where(:status => '10').sum(:amount)
    @article = '0'
    @stock_order = '0'
    conditions = ''

      if params['article_id'] != ''
        @article = params['article_id']
        conditions_a = "article_id='#{@article}'"
        conditions = conditions_a
        @amount_forecast = ShippingOrderline.where(:status => '10', :article_id => @article).sum(:amount)
      else params['article_id'] = ''
      end
      
      if params['stock_order_id'] != ''
        @stock_order = params['stock_order_id']
        conditions_s = "stock_order_id='#{@stock_order}'"
        if conditions_a
            conditions = "#{conditions_a} and #{conditions_s}"
        else
            conditions = conditions_s
        end
      end
    if params['stock_order_id'] != ''
      @sum_by_article_id = WarehouseStock.where(conditions).group('article_id').collect{|s|[s.article_id,
          WarehouseStock.where("article_id='#{s.article_id}'and stock_order_id = '#{s.stock_order_id}' and status = 20").sum("quantity")]}
    else
      @sum_by_article_id = WarehouseStock.where(conditions).group('article_id').collect{|s|[s.article_id,
          WarehouseStock.where("article_id='#{s.article_id}'and status = 20").sum("quantity")]}
    end
    
    @filter_result = WarehouseStock.where(conditions).group('article_id,stock_order_id').collect{|s|[s.article_id,
        WarehouseStock.where("article_id='#{s.article_id}' and stock_order_id = '#{s.stock_order_id}' and status = 20").sum("quantity"),
      s.stock_order.purchase_reference]}

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @warehouse_stock }
    end
  end
  
  def listing
    @warehouse_grid = initialize_grid(WarehouseStock)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @warehouse_stock }
    end
  end

  # GET /warehouse_stock/1
  # GET /warehouse_stock/1.xml
  def show
    @warehouse_stock = WarehouseStock.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @warehouse_stock }
    end
  end

  # GET /warehouse_stock/new
  # GET /warehouse_stock/new.xml
  def new
    @warehouse_stock = WarehouseStock.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @warehouse_stock }
    end
  end

  # GET /warehouse_stock/1/edit
  def edit
    @warehouse_stock = WarehouseStock.find(params[:id])
  end

  # POST /warehouse_stock
  # POST /warehouse_stock.xml
  def xcreate
    @warehouse_stock = WarehouseStock.new(strong_params)

    respond_to do |format|
      if @warehouse_stock.save
        format.html { redirect_to(:action => :index, :notice => 'WarehouseStock was successfully created.') }
        format.xml  { render :xml => @warehouse_stock, :status => :created, :location => @warehouse_stock }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @warehouse_stock.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /warehouse_stock/1
  # PUT /warehouse_stock/1.xml
  def update
    @warehouse_stock = WarehouseStock.find(params[:id])

    respond_to do |format|
      if @warehouse_stock.update_attributes(strong_params)
        format.html { redirect_to(:action => :index, :notice => 'WarehouseStock was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @warehouse_stock.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /warehouse_stock/1
  # DELETE /warehouse_stock/1.xml
  def destroy_stock
    @warehouse_stock = WarehouseStock.find(params[:id])
    @warehouse_stock.destroy

    respond_to do |format|
      format.html { redirect_to(:action => :index) }
      format.xml  { head :ok }
    end
  end
end
