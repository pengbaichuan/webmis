module ApplicationHelper

  def column_sort(column, title = nil, search_fields = false, select_values = [],search_column_overrule = nil, width_overule = nil)
    if sort_direction == "desc"
      html_class = "glyphicon glyphicon-sort-by-alphabet-alt" # twitter-bootstrap css
    else # default is ascending
      html_class = "glyphicon glyphicon-arrow-up" # twitter-bootstrap css
    end

    title ||= column.titleize
    css_class = (column == sort_column) ? "#{html_class}" : nil

    if search_column_overrule.nil?
      search_field_name = column.parameterize
    else
      search_field_name = search_column_overrule.parameterize
    end
    direction = (column == sort_column && sort_direction == "asc") ? "desc" : "asc"

    if params["column_search_field"]
      field_val = params["column_search_field"]["#{search_field_name}"]
    else
      field_val = ''
    end
    
    search_js = ''
    if search_fields
      if select_values.empty?
        search_html = text_field_tag("column_search_field[#{search_field_name}]",field_val,{:class => "column_search form-control"})
      else
        search_html = hidden_field_tag("column_search_field[#{search_field_name}]",field_val)
        search_html += select_tag("column_select_field[#{search_field_name}]",options_for_select(select_values,field_val),{:include_blank => true,:class => "column_search form-control"})
        search_js = '<script type="text/javascript">'
        search_js += "$('#column_select_field_#{search_field_name}').parent().parent().css( 'min-width', '#{width_overule}%' );" if !width_overule.nil?
        search_js += '$("[id^=column_select_field_]").select2({allowClear: true})</script>'
      end
    else
      search_html = ''
    end

    if search_fields
    (raw '<div class="input-group hidden-sm hidden-xs"><div class="input-group-addon">') + link_to(' ' + title, params.merge(:sort => column, :direction => direction))+ (raw "<span class='#{css_class} text-primary'><\/span>") + (raw '</div>') + search_html+ (raw '</div>') + raw(search_js)
    else
      link_to(' ' + title, params.merge(:sort => column, :direction => direction))+ (raw "<span class='#{css_class} text-primary'><\/span>")
    end
  end

  def sortable(column, title = nil, search_fields = false, select_values = [],search_column_overrule = nil)
    if sort_direction == "desc"
      html_class = "icon-circle-arrow-down" # twitter-bootstrap css
    else # default is ascending
      html_class = "icon-circle-arrow-up" # twitter-bootstrap css
    end

    title ||= column.titleize
    css_class = (column == sort_column) ? "#{html_class}" : nil
    if search_column_overrule.nil?
      search_field_name = column.parameterize
    else
      search_field_name = search_column_overrule.parameterize
    end
    direction = (column == sort_column && sort_direction == "asc") ? "desc" : "asc"

    if params["column_search_field"]
      field_val = params["column_search_field"]["#{search_field_name}"]
    else
      field_val = ''
    end

    if search_fields
      if select_values.empty?
        search_html = text_field_tag("column_search_field[#{search_field_name}]",field_val,{:class => "column_search form-control"})
      else
        search_html = hidden_field_tag("column_search_field[#{search_field_name}]",field_val)
        search_html += select_tag("column_select_field[#{search_field_name}]",options_for_select(select_values,field_val),{:include_blank => true,:class => "column_search"})
      end
    else
      search_html = ''
    end

    link_to(' ' + title, params.merge(:sort => column, :direction => direction), {:class => css_class})  + (raw '<br />') + search_html

  end

  def pageless(total_pages, url=nil, container=nil, params=nil)
    par = Hash.new
    if sort_direction
      par['direction'] = sort_direction
    end
    if sort_column
      par['sort'] = sort_column
    end
    if search
      par['search'] = search
    end
    if params
      # avoid overwrite
      params.delete('page')
      params.delete('sort')
      params.delete('search')
      params.delete('direction')
      par.merge!(params)
    end
    opts = {
      :totalPages => total_pages,
      :url        => url,
      :loaderMsg  => 'loading',
      :loaderImage => image_path("endless-loader.gif"),
      :params => par

    }

    container && opts[:container] ||= container

    javascript_tag("$('#endless').pageless(#{opts.to_json});")
  end

  def history_class(old, new)
    case
    when (!old || (old.class.method_defined?(:empty?) && old.empty?)) && (!new || (new.class.method_defined?(:empty?) && new.empty?))
      "equal"
    when (!old || (old.class.method_defined?(:empty?) && old.empty?))
      "added"
    when (!new || (old.class.method_defined?(:empty?) && new.empty?))
      "removed"
    when old == new
      "equal"
    else
      "changed"
    end
  end

  def history_string(old, new)
    case
    when !old && !new
      ""
    when old && (!new || (new.class.method_defined?(:empty?) && new.empty?))
      old
    when !old || (old.class.method_defined?(:empty?) && old.empty?) || old == new
      new
    else
      "#{old} => #{new}"
    end
  end

  def amount_to_currency(amount, unit)
    number_to_currency(amount, unit: unit, separator: ",", delimiter: ".", format: "%u %n")
  end

  def release_version
    if Rails.env != 'production'
      Rails.env.humanize
    else
      rfile = "#{Rails.root}/ReleaseName.txt"
      if File.exist?(rfile)
        File.read(rfile).humanize
      else
        "WebMIS"
      end
    end
  end

  def bootstrap_boolean(bool,title=nil)
    bool = true if bool == 1
    bool = false if bool == 0
    bool = false if bool.nil?

    if !title
      title='true or false'
    end
    if bool == false
      content_tag(:i, :class => 'icon-minus glyphicon glyphicon-minus',:title => title)do
        '&nbsp;'.html_safe
      end
    else
      content_tag(:i, :class => 'icon-ok glyphicon glyphicon-ok',:title => title)do
        '&nbsp;'.html_safe
      end
    end
  end

  def current_dashboards
    User.find(session[:user_id]).dashboard_grids
  end

  def refresh_widget(id,time)
    return javascript_tag( "function doPoll#{id}(){
               $.get('" +url_for(:action => :render_widget, :controller => :dashboard_widgets, :id => id.to_s)+ "', function( data ) {
                 $('#widget-id-#{id.to_s}').replaceWith(data); setTimeout(doPoll#{id}," + time + ");
               });}
               setTimeout(function() {
                 doPoll#{id}();},#{time});")
  end

  def hash_to_html(hash,html_class=nil)
    hclass = ''
    if html_class
      hclass = " class='#{html_class}'"
    end
    [
      '<ul '+hclass+'>',
      hash.map { |k, v| ["<li><strong>#{k}</strong>", (v.class.name == 'Hash') ? hash_to_html(v) : "<span> #{v}</span></li>"] },
      '</ul>'
    ].join
  end

  def next_sequence_id(direction="next")
    follow_up_sequence_id = nil
    cookie_name = "id_sequence_#{controller.controller_name}"
    sequence_array = []
    if cookies[cookie_name]
      sequence_array = cookies[cookie_name].split(',')
    end

    if params[:id] && !sequence_array.empty?
      current_sequence_index = sequence_array.index(params[:id])
      if direction == 'next'
        if sequence_array.last != params[:id]
          if current_sequence_index
            follow_up_sequence_id = sequence_array[current_sequence_index + 1]
          else
            follow_up_sequence_id = sequence_array.first
          end
        end
      else
        if sequence_array.first != params[:id]
          if current_sequence_index
            follow_up_sequence_id = sequence_array[current_sequence_index - 1]
          end
        end
      end
    end
    return follow_up_sequence_id
  end

  def next_prev_button(direction="next",action=nil)
    record_id =  next_sequence_id(direction)
    if direction == "next"
      icon_class = " icon-step-forward glyphicon glyphicon-step-forward"
    else
      icon_class = " icon-step-backward glyphicon glyphicon-step-backward"
    end
    if record_id
      if !action
        controller_action = controller.action_name
      else
        controller_action = action
      end
      link_to({:action => controller_action, :id => record_id, :controller => controller.controller_name},{:class => 'btn btn-default',:title => "#{controller.controller_name.singularize} with id #{record_id}"}) do
        content_tag(:i, "", :class => icon_class).html_safe
      end
    else
      content_tag(:button,"",:class => 'btn disabled',:title => "No record(s) available") do
        content_tag(:i, "", :class => icon_class).html_safe
      end
    end
  end

  def env_production?
    if session[:environment] == "production"
      return true
    else
      return false
    end
  end

  def alert_class_for(flash_type)
    # map flash-type to css class
    {
      :success => 'alert-success',
      :error => 'alert-danger',
      :alert => 'alert-warning',
      :notice => 'alert-info'
    }[flash_type.to_sym] || flash_type.to_s
  end

  def job_status_to_bootstrap_panel_css(status)
    panel_css = 'panel '
    case status
    when Job::STATUS_HOLD,Job::STATUS_FAILED
      return panel_css + "panel-danger"
    when Job::STATUS_COMPLETE
      return panel_css + "panel-success"
    when Job::STATUS_MANUAL, Job::STATUS_HOLD
      return panel_css + "panel-info"
    when Job::STATUS_CANCELLED
      return panel_css + "panel-warning"
    when Job::STATUS_PROCESSING
      return panel_css + "panel-primary"
    else
      return panel_css + "panel-default"
    end
  end

end
