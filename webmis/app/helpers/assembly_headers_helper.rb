module AssemblyHeadersHelper
  def conversion_database_icon_class(status)
    case status
    when ConversionDatabase::STATUS_HAS_ERRORS
      iconclass = 'glyphicon-warning-sign'
    when ConversionDatabase::STATUS_HAS_WARNINGS
      iconclass = 'glyphicon-info-sign'
    when ConversionDatabase::STATUS_VALIDATING
      iconclass = 'glyphicon-search'
    when ConversionDatabase::STATUS_ACCEPTED
      iconclass = 'glyphicon-warning-sign'
    when ConversionDatabase::STATUS_NOT_VALIDATED
      iconclass = 'glyphicon-question-sign'
    when ConversionDatabase::STATUS_REJECTED
      iconclass = 'glyphicon-warning-sign'
    else
      iconclass = 'glyphicon-ok'
    end
    return iconclass
  end
end
