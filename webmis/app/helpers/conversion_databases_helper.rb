module ConversionDatabasesHelper
  def conversion_database_action_link(context)
    case context.status
    when Job::STATUS_FAILED
      btn_class = 'btn-danger'
    when Job::STATUS_PROCESSING
      btn_class = 'btn-primary'
    when Job::STATUS_COMPLETE
      btn_class = 'btn-success'
    when Job::STATUS_QUEUED
      btn_class = 'btn-info'
    when Job::STATUS_HOLD, Job::STATUS_MANUAL
      btn_class = 'btn-inverse'
    when Job::STATUS_PLANNED
      btn_class = 'btn-warning'
    else # Job::STATUS_CANCELLED, Job::STATUS_RESCHEDULED
      btn_class = '',nil
    end

    action_link_html = "<a class='btn btn-mini dropdown-toggle #{btn_class}' data-toggle='dropdown' href='#'>"
    action_link_html = action_link_html + 'Action'
    action_link_html = action_link_html + "<span class='caret'></span></a>"
    return action_link_html.html_safe
  end
end
