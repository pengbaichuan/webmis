module ConversionsHelper
 
  def show_info(id)
    @conversion = Conversion.find(id)
    
    info = "n.a."
    if @conversion
      @database = ConversionDatabase.present.where(:conversion_id => @conversion.id)
      @results = @database.collect {|x|x.path_and_file}
      if @results.empty? && @conversion.reception_reference.to_s != ""
            r = Reception.where(:id => @conversion.reception_reference).last
            if r
              @results = r.storage_location
            end
      end
       
        info = "<h1>#{@conversion.id}</h1><table class='noborder'>"
        info = info + "<tr><td>In:</td><td>#{@conversion.input_format}</td></tr>"
        info = info + "<tr><td>Out:</td><td>#{@conversion.output_format}</td></tr>"
        info = info + "<tr><td>Production Order:</td><td>#{@conversion.production_orderline.production_order.name}</td></tr>" if @conversion.production_orderline_id
        info = info + "<tr><td>Product-id:</td><td>#{@conversion.productid}</td></tr>"
        info = info + "<tr><td>Supplier:</td><td>#{CompanyAbbr.find(@conversion.data_release.company_abbr.id.to_s).company_name}</td></tr>" if @conversion.data_release
        info = info + "<tr><td>Release:</td><td>#{@conversion.data_release.name}</td></tr>"
        info = info + "<tr><td>Area:</td><td>#{Region.find(@conversion.region_id).name}</td></tr>" if @conversion.region_id
        info = info + "<tr><td>Additional info:</td><td><pre>#{@conversion.additional_info}</pre></td></tr>"
        info = info + "</table><hr>"
        
        info = info +  "<h1>result(s) #{@conversion.status_str}</h1><table class='noborder'>"
        info = info + "<tr><td>REASON:</td><td><pre>#{@conversion.rejection_remark}</pre></td></tr>" if @conversion.rejection_remark != ""
        info = info + "<tr><td>Converter:</td><td>#{@conversion.converter}</td></tr>"
        info = info + "<tr><td>Outcome:</td><td>#{@conversion.outcome}</td></tr>"
        info = info + "<tr><td>Result files:</td><td><pre>#{@results}</pre></td></tr>"
        info = info + "<tr><td>Reception-id:</td><td><pre>#{@conversion.reception_reference}</pre></td></tr>" if @conversion.reception_reference.to_s != ""
        info = info + "<tr><td>Remarks:</td><td><pre>#{@conversion.remarks}</pre></td></tr>" if @conversion.remarks != ""
        info = info + "</table>"
        
    end
    return info
  end
  
  def conversion_action_link(context)
    case context.status
    when Conversion::STATUS_CONVERSION
      btn_class = 'btn-primary'
    when Conversion::STATUS_FINISHED
      btn_class = 'btn-success'
    when Conversion::STATUS_ENTRY, Conversion::STATUS_REQUESTED, Conversion::STATUS_REQUESTED_TPD
      btn_class = 'btn-info'
    when Conversion::STATUS_CANCELLED, Conversion::STATUS_REJECTED
      btn_class = 'btn-warning'
    else
      btn_class = '',nil
    end

    action_link_html = "<a class='btn btn-mini dropdown-toggle #{btn_class}' data-toggle='dropdown' href='#'>"
    action_link_html = action_link_html + "Action"
    action_link_html = action_link_html + "<span class='caret'></span></a>"
    return action_link_html.html_safe
  end

  def report_cols(input_report_data, conversion)
    # the conversions in input_report_data are already grouped by input->output
    # this method puts each of those group in the right column for display
    # col0: process data/receptions to dhive conversions
    # col1: rdf->rdb->dhive and dhive->pdhive conversions
    # col2: dhive/pdive -> nds conversion
    # col3: nds->nds conversions
    # col4: the group which contains conversion, the final conversions step
    cols = [{},{},{},{},{}]
    input_report_data[:conversion_data].each do |key, conversions|
      from = key.split('|')[0]
      to = key.split('|')[1]
      if conversions[conversion.id]
        # the final conversion should always be in the final column
        cols.last[key] = {conversion.id => conversions[conversion.id]}
        conversions.delete(conversion.id)
      end
      next if conversions.empty?
      if from == 'RDF' || from == 'RDB' || to == 'PDhive'
        cols[1][key] = Hash[conversions.sort]
      elsif to == 'dHive'
        cols[0][key] = Hash[conversions.sort]
      elsif from == 'NDS'
        cols[3][key] = Hash[conversions.sort]
      else
        cols[2][key] = Hash[conversions.sort]
      end
    end
    return cols
  end

end
