module DefaultContentReleaseNotesHelper
  
    def show_content_info(id)
    @content = DefaultContentReleaseNote.find(id)
    @default_content_release_note = DefaultContentReleaseNote.find(id)
    
    info = "n.a."
    if @content
      info = "<b>#{@default_content_release_note.title}</b>
             <pre>#{@default_content_release_note.content}</pre>"        
    end
    return info
  end
  
end
