module HighLevelBomsHelper
  
  def show_bom_info(id)
  @bom = HighLevelBom.find(id)
    
       
  info = "
  <table>
  <tr>
    <td><b>Internal BOM reference:</b></td>
    <td>#{@bom.internal_reference}</td>
    <td><b>Revision:</b></td>
    <td>#{@bom.revision}</td>
  </tr>
  <tr>
    <td><b>Active:</b></td>
    <td>#{@bom.active}</td>
    <td><b>Deleted:</b></td>
    <td>#{@bom.deleted}</td>
  </tr>
  <tr>
    <td><b>Created by:</b></td>
    <td>#{@bom.create_user.full_name}</td>
    <td><b>Status:</b></td>
    <td>#{@bom.status_string}</td>
  </tr>
  </table>"
  
  info = info + "
  <table class='noborder'>
  <tr>
    <td><b>Product:</b></td>
    <td>#{@bom.product.volumeid}&nbsp;#{@bom.product.name}</td>
  </tr>
  <tr>
    <td><b>Master product:</b></td>
    <td>#{@bom.product_location_id}&nbsp;#{ProductLocation.find(@bom.product_location_id).storage_path}</td>
  </tr>
  <tr>
    <td><b>Inlay reception ID:</b></td>
    <td>#{@bom.inlay.id} - #{@bom.inlay.data_release.company_abbr.name.to_s} #{@bom.inlay.region_name.to_s} #{@bom.inlay.updated_at.strftime('%d-%m-%Y')}</td>
  </tr>
  <tr>
    <td><b>Label reception ID:</b></td>
    <td>#{@bom.label.id} - #{@bom.label.data_release.company_abbr.name.to_s} #{@bom.label.region_name.to_s} #{@bom.label.updated_at.strftime('%d-%m-%Y')}</td>
  </tr>
</table>"
        
    
  return info
  end
  
end
