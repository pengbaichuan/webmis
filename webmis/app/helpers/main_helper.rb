module MainHelper

  def app_action_controllers_hash
    returnhash = {}
    Rails.application.routes.routes.map do |route|
      if returnhash[route.defaults[:controller]].nil?
        returnhash[route.defaults[:controller]] = [] << route.defaults[:action]
      else
        returnhash[route.defaults[:controller]] =  returnhash[route.defaults[:controller]] << route.defaults[:action]
      end
    end
    return returnhash
  end
end
