# encoding: UTF-8
module Wice
  module GridViewHelper
    # patch on wicegrid plugin "wice_grid_view_helpers.rb"
    def grid_html(grid, options, rendering, reuse_last_column_for_filter_buttons) #:nodoc:
      table_html_attrs, header_tr_html_attrs = options[:table_html_attrs], options[:header_tr_html_attrs]

      cycle_class = nil
      sorting_dependant_row_cycling = options[:sorting_dependant_row_cycling]

      content = GridOutputBuffer.new
      # Ruby 1.9.1
      content.force_encoding('UTF-8') if content.respond_to?(:force_encoding)

      content << %!<div class="wice_grid_container" id="#{grid.name}"><div id="#{grid.name}_title">!
      content << content_tag(:h3, grid.saved_query.name) if grid.saved_query
      content << "</div><table #{tag_options(table_html_attrs, true)}>"
      content << "<thead>"

      no_filters_at_all = (options[:show_filters] == :no || rendering.no_filter_needed?) ? true: false

      if no_filters_at_all
        no_rightmost_column = no_filter_row = no_filters_at_all
      else
        no_rightmost_column = no_filter_row = (options[:show_filters] == :no || rendering.no_filter_needed_in_main_table?) ? true: false
      end

      no_rightmost_column = true if reuse_last_column_for_filter_buttons

      pagination_panel_content_html, pagination_panel_content_js = nil, nil
      if options[:upper_pagination_panel]
        content << rendering.pagination_panel(no_rightmost_column, options[:hide_csv_button]) do
          pagination_panel_content_html, pagination_panel_content_js =
          pagination_panel_content(grid, options[:extra_request_parameters], options[:allow_showing_all_records])
          pagination_panel_content_html
        end
      end

      title_row_attrs = header_tr_html_attrs.clone
      title_row_attrs.add_or_append_class_value!('wice_grid_title_row', true)

      content << %!<tr #{tag_options(title_row_attrs, true)}>!

      filter_row_id = grid.name + '_filter_row'

      # first row of column labels with sorting links

      filter_shown = if options[:show_filters] == :when_filtered
        grid.filtering_on?
      elsif options[:show_filters] == :always
        true
      end

      cached_javascript = []

      rendering.each_column_aware_of_one_last_one(:in_html) do |column, last|

        column_name = column.column_name
        if column_name.is_a? Array
          column_name, js = column_name
          cached_javascript << js
        end

        if column.attribute_name && column.allow_ordering

          css_class = grid.filtered_by?(column) ? 'active_filter' : nil

          direction = 'asc'
          link_style = nil
          if grid.ordered_by?(column)
            css_class = css_class.nil? ? 'sorted' : css_class + ' sorted'
            link_style = grid.order_direction
            direction = 'desc' if grid.order_direction == 'asc'
          end

          col_link = link_to(
          column_name,
          rendering.column_link(column, direction, params, options[:extra_request_parameters]),
          :class => link_style)
          content << content_tag(:th, col_link, Hash.make_hash(:class, css_class))
          column.css_class = css_class
        else
          if reuse_last_column_for_filter_buttons && last
            content << content_tag(:th,
            hide_show_icon(filter_row_id, grid, filter_shown, no_filter_row, options[:show_filters], rendering),
            :class => 'hide_show_icon'
            )
          else
            content << content_tag(:th, column_name)
          end
        end
      end

      content << content_tag(:th,
      hide_show_icon(filter_row_id, grid, filter_shown, no_filter_row, options[:show_filters], rendering),
      :class => 'hide_show_icon'
      ) unless no_rightmost_column

      content << '</tr>'
      # rendering first row end


      unless no_filters_at_all # there are filters, we don't know where, in the table or detached
        if no_filter_row # they are all detached
          content.stubborn_output_mode = true
          rendering.each_column(:in_html) do |column|
            if column.filter_shown?
              filter_html_code, filter_js_code = column.render_filter
              filter_html_code = filter_html_code.html_safe_if_necessary
              cached_javascript << filter_js_code
              content.add_filter(column.detach_with_id, filter_html_code)
            end
          end

        else # some filters are present in the table

          filter_row_attrs = header_tr_html_attrs.clone
          filter_row_attrs.add_or_append_class_value!('wice_grid_filter_row', true)
          filter_row_attrs['id'] = filter_row_id

          content << %!<tr #{tag_options(filter_row_attrs, true)} !
          content << 'style="display:none"' unless filter_shown
          content << '>'

          rendering.each_column_aware_of_one_last_one(:in_html) do |column, last|
            if column.filter_shown?

              filter_html_code, filter_js_code = column.render_filter
              filter_html_code = filter_html_code.html_safe_if_necessary
              cached_javascript << filter_js_code
              if column.detach_with_id
                content.stubborn_output_mode = true
                content << content_tag(:th, '', Hash.make_hash(:class, column.css_class))
                content.add_filter(column.detach_with_id, filter_html_code)
              else
                content << content_tag(:th, filter_html_code, Hash.make_hash(:class, column.css_class))
              end
            else
              if reuse_last_column_for_filter_buttons && last
                content << content_tag(:th,
                reset_submit_buttons(options, grid, rendering),
                Hash.make_hash(:class, column.css_class).add_or_append_class_value!('filter_icons')
                )
              else
                content << content_tag(:th, '', Hash.make_hash(:class, column.css_class))
              end
            end
          end
          unless no_rightmost_column
            content << content_tag(:th, reset_submit_buttons(options, grid, rendering), :class => 'filter_icons' )
          end
          content << '</tr>'
        end
      end

      rendering.each_column(:in_html) do |column|
        unless column.css_class.blank?
          column.td_html_attrs.add_or_append_class_value!(column.css_class)
        end
      end

      content << '</thead><tfoot>'
      content << rendering.pagination_panel(no_rightmost_column, options[:hide_csv_button]) do
        if pagination_panel_content_html
          pagination_panel_content_html
        else
          pagination_panel_content_html, pagination_panel_content_js =
          pagination_panel_content(grid, options[:extra_request_parameters], options[:allow_showing_all_records])
          pagination_panel_content_html
        end
      end

      content << '</tfoot><tbody>'
      cached_javascript << pagination_panel_content_js

      # rendering  rows
      cell_value_of_the_ordered_column = nil
      previous_cell_value_of_the_ordered_column = nil

      grid.each do |ar| # rows

        before_row_output = if rendering.before_row_handler
          call_block_as_erb_or_ruby(rendering, rendering.before_row_handler, ar)
        else
          nil
        end

        after_row_output = if rendering.after_row_handler
          call_block_as_erb_or_ruby(rendering, rendering.after_row_handler, ar)
        else
          nil
        end

        row_content = ''
        rendering.each_column(:in_html) do |column|
          cell_block = column.cell_rendering_block

          opts = column.td_html_attrs.clone

          column_block_output = if column.class == Wice::ActionViewColumn
            cell_block.call(ar, params)
          else
            call_block_as_erb_or_ruby(rendering, cell_block, ar)
          end

          if column_block_output.kind_of?(Array)

            unless column_block_output.size == 2
              raise WiceGridArgumentError.new('When WiceGrid column block returns an array it is expected to contain 2 elements only - '+
              'the first is the contents of the table cell and the second is a hash containing HTML attributes for the <td> tag.')
            end

            column_block_output, additional_opts = column_block_output

            unless additional_opts.is_a?(Hash)
              raise WiceGridArgumentError.new('When WiceGrid column block returns an array its second element is expected to be a ' +
              "hash containing HTML attributes for the <td> tag. The returned value is #{additional_opts.inspect}. Read documentation.")
            end

            additional_css_class = nil
            if additional_opts.has_key?(:class)
              additional_css_class = additional_opts[:class]
              additional_opts.delete(:class)
            elsif additional_opts.has_key?('class')
              additional_css_class = additional_opts['class']
              additional_opts.delete('class')
            end
            opts.merge!(additional_opts)
            opts.add_or_append_class_value!(additional_css_class) unless additional_css_class.blank?
          end

          if sorting_dependant_row_cycling && column.attribute_name && grid.ordered_by?(column)
            cell_value_of_the_ordered_column = column_block_output
          end
          row_content += content_tag(:td, column_block_output, opts)
        end

        row_attributes = rendering.get_row_attributes(ar)

        if sorting_dependant_row_cycling
          cycle_class = cycle('odd ui-widget-content', 'even', :name => grid.name) if cell_value_of_the_ordered_column != previous_cell_value_of_the_ordered_column
          previous_cell_value_of_the_ordered_column = cell_value_of_the_ordered_column
        else
          cycle_class = cycle('odd ui-widget-content', 'even', :name => grid.name)
        end

        row_attributes.add_or_append_class_value!(cycle_class)

        content << before_row_output if before_row_output
        content << "<tr #{tag_options(row_attributes)}>#{row_content}"
        content << content_tag(:td, '') unless no_rightmost_column
        content << '</tr>'
        content << after_row_output if after_row_output
      end

      content << '</tbody></table></div>'

      base_link_for_filter, base_link_for_show_all_records = rendering.base_link_for_filter(controller, options[:extra_request_parameters])

      link_for_export      = rendering.link_for_export(controller, 'csv', options[:extra_request_parameters])

      parameter_name_for_query_loading = {grid.name => {:q => ''}}.to_query
      parameter_name_for_focus = {grid.name => {:foc => ''}}.to_query

      prototype_and_js_version_check = if ENV['RAILS_ENV'] == 'development'
        %$ if (typeof(WiceGridProcessor) == "undefined"){\n$ +
          %$   alert('wice_grid.js not loaded, WiceGrid cannot proceed! ' +\n$ +
          %$     'Please make sure that you include WiceGrid javascript in your page. ' +\n$ +
          %$     'Use <%= include_wice_grid_assets %> or <%= include_wice_grid_assets(:include_calendar => true) %> ' +\n$ +
          %$     'for WiceGrid javascripts and assets.')\n$ +
        %$ } else if ((typeof(WiceGridProcessor._version) == "undefined") || ( WiceGridProcessor._version != "0.4.3")) {\n$ +
          %$    alert("wice_grid.js in your /public is outdated, please run\\n ./script/generate wice_grid_assets_jquery\\n$ +
          %$ or\\n ./script/generate wice_grid_assets_prototype\\nto update it.");\n$ +
        %$ }\n$
      else
        ''
      end

      if rendering.show_hide_button_present
        cached_javascript << JsAdaptor.show_hide_button_initialization(grid.name, filter_row_id)
      end

      if rendering.reset_button_present
        cached_javascript << JsAdaptor.reset_button_initialization(grid.name, reset_grid_javascript(grid))
      end

      if rendering.submit_button_present
        cached_javascript << JsAdaptor.submit_button_initialization(grid.name, submit_grid_javascript(grid))
      end

      if rendering.contains_a_text_input?
        cached_javascript << JsAdaptor.enter_key_event_registration(grid.name)
      end

      if rendering.csv_export_icon_present
        cached_javascript << JsAdaptor.csv_export_icon_initialization(grid.name)
      end

      if rendering.contains_auto_reloading_selects
        cached_javascript << JsAdaptor.auto_reloading_selects_event_initialization(grid.name)
      end

      if rendering.contains_auto_reloading_inputs
        cached_javascript << JsAdaptor.auto_reloading_inputs_event_initialization(grid.name)
      end

      if rendering.contains_auto_reloading_inputs_with_negation_checkboxes
        cached_javascript << JsAdaptor.auto_reloading_inputs_with_negation_checkboxes_event_initialization(grid.name)
      end

      if rendering.contains_auto_reloading_calendars
        cached_javascript << JsAdaptor.auto_reloading_calendar_event_initialization(grid.name)
      end

      if rendering.element_to_focus
        cached_javascript << JsAdaptor.focus_element(rendering.element_to_focus)
      end

      content << javascript_tag(
      JsAdaptor.dom_loaded +
      %/ #{prototype_and_js_version_check}\n/ +
      %/ window['#{grid.name}'] = new WiceGridProcessor('#{grid.name}', '#{base_link_for_filter}',\n/ +
      %/  '#{base_link_for_show_all_records}', '#{link_for_export}', '#{parameter_name_for_query_loading}',\n/ +
      %/ '#{parameter_name_for_focus}', '#{ENV['RAILS_ENV']}');\n/ +
      if no_filters_at_all
        ''
      else
        rendering.select_for(:in_html) do |vc|
          vc.attribute_name and not vc.no_filter
        end.collect{|column| column.yield_javascript}.join("\n")
      end +
      "\n" + cached_javascript.compact.join('') +
      '})'
      )

      if content.stubborn_output_mode
        grid.output_buffer = content
      else
        # this will serve as a flag that the grid helper has already processed the grid but in a normal mode,
        # not in the mode with detached filters.
        grid.output_buffer = true
      end
      return content
    end

  end
end

