module ProductionOrderlinesHelper
  def shipment_request_button_style(production_orderline)
    procc_arr = production_orderline.define_pre_process_for_manual_shipment_registration
    btnclass = ''
    icon_class = ''
    case procc_arr[0]
    when 'register_cd','register_file'
      btnclass = 'btn-warning'
      icon_class = 'icon-folder-close'
    when 'ship_cd','ship_file'
      btnclass = 'btn-info'
      icon_class = 'icon-road'
    else
      btnclass = 'btn-danger'
      icon_class = 'icon-ban-circle'
    end

    label = procc_arr[2]
    title = procc_arr[3]

    return {"btnclass" => btnclass,"label" => label,"title" => title,"iconclass" => icon_class}
  end

  def shipment_request_link_params(production_orderline,bulk=false)
    procc_arr = production_orderline.define_pre_process_for_manual_shipment_registration
    paramhash = {}
    case procc_arr[0]
    when 'register_cd'
      paramhash = {:action => 'edit',
        :controller => :stock,
        :id => procc_arr[1],
        :production_orderline_id => production_orderline.id,
        :return_to_controller => :production_orderlines,
        :return_to_action => :shipment_index}
    when 'register_file'
      paramhash = {:action => 'new',
        :controller => :product_locations,
        :conversion_id => procc_arr[1],
        :return_to_id => production_orderline.id,
        :return_to_controller => :production_orderlines,
        :return_to_action => :show_shipment_request,
        :production_orderline_id => production_orderline.id}
    when 'ship_file','ship_cd'
      paramhash = {:controller => :outbound_orders,
        :action => 'create_from_order',
        :production_order_line_id => production_orderline.id,
        :return_to_id => production_orderline.id,
        :return_to_controller => :production_orderlines,
        :return_to_action => :show_shipment_request }
        if bulk && production_orderline.production_order.production_orderlines.where(:bp_name => 'SHIPMENT REQUESTED').size > 1
          paramhash['multiline'] = '1'
        end
    end
    return paramhash
  end
end
