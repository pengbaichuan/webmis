module ProductsHelper

  def volumeid_form_column(record, input_name)
    input :record, :volumeid, :onkeypress =>"showIcon('img_#{input_name}');", :class => "name-input text-input"
  end

  def name_form_column(record, input_name)
    input :record, :name, :onkeypress =>"showIcon('img_#{input_name}');", :class => "name-input text-input"
  end

  def detailedcoverage_form_column(record, input_name)
    input :record, :detailedcoverage, :onkeypress =>"showIcon('img_#{input_name}');", :class => "name-input text-input"
  end

  def hwncoverage_form_column(record, input_name)
    input :record, :hwncoverage, :onkeypress =>"showIcon('img_#{input_name}');", :class => "name-input text-input"
  end

  def tmclocation_form_column(record, input_name)
    input :record, :tmclocation, :onkeypress =>"showIcon('img_#{input_name}');", :class => "name-input text-input"
  end

  def tmcevent_form_column(record, input_name)
    input :record, :tmcevent, :onkeypress =>"showIcon('img_#{input_name}');", :class => "name-input text-input"
  end

  def thirdpartydata1_form_column(record, input_name)
    text_area :record, :thirdpartydata1, :onkeypress =>"showIcon('img_#{input_name}');", :class => "name-input text-input"
  end

  def thirdpartydata2_form_column(record, input_name)
    text_area :record, :thirdpartydata2, :onkeypress =>"showIcon('img_#{input_name}');", :class => "name-input text-input"
  end

  def remarks_form_column(record, input_name)
    text_area :record, :remarks, :onkeypress =>"showIcon('img_#{input_name}');", :class => "name-input text-input"
  end


  def detailedcoverage(record)
    record.detailedcoverage.gsub("\n","<br/>")
  end

  def hwncoverage(record)
    record.hwncoverage.gsub("\n","<br/>")
  end

  def thirdpartydata2(record)
    record.thirdpartdata2.gsub("\n","<br/>")
  end

  def nds_name_column(record)
    if record.nds_name && record.nds_name.strip != 'n.a.'
      link_to record.nds_name, :action => 'show_nds', :id => record.id
    else
      'n.a.'
    end
  end
end
