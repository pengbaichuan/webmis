module ProductsetsHelper

def status_column(record)
 Productset.statushash[record.status.to_i]
end 

def customer_name_form_column(record, input_name)
  input :record, :customer_name, :onkeypress =>"showIcon('img_#{input_name}');", :class => "name-input text-input"
end

def product_line_name_form_column(record, input_name)
  input :record, :product_line_name, :onkeypress =>"showIcon('img_#{input_name}');", :class => "name-input text-input"
end

def name_form_column(record, input_name)
  input :record, :name, :onkeypress =>"showIcon('img_#{input_name}');", :class => "name-input text-input"
end

def setcode_form_column(record, input_name)
  input :record, :setcode, :onkeypress =>"showIcon('img_#{input_name}');", :class => "name-input text-input"
end

def firsttiername_form_column(record, input_name)
  input :record, :firsttiername, :onkeypress =>"showIcon('img_#{input_name}');", :class => "name-input text-input"
end

def description_form_column(record, input_name)
  input :record, :description,:onkeypress =>"showIcon('img_#{input_name}');", :class => "name-input text-input"
end

def tmc_file_form_column(record, input_name)
  input :record, :tmc_file,:onkeypress =>"showIcon('img_#{input_name}');", :class => "name-input text-input"
end

def tpd_file_form_column(record, input_name)
  input :record, :tpd_file,:onkeypress =>"showIcon('img_#{input_name}');", :class => "name-input text-input"
end

def nds_name_form_column(record, input_name)
  input :record, :nds_name,:onkeypress =>"showIcon('img_#{input_name}');", :class => "name-input text-input"
end

def remarks_form_column(record, input_name)
  text_area :record, :remarks,:onkeypress =>"showIcon('img_#{input_name}');", :class => "name-input text-input"
end

def approval_text_form_column(record, input_name)
  input :record, :approval_text,:onkeypress =>"showIcon('img_#{input_name}');", :class => "name-input text-input"
end

def predecessor_form_column(record, input_name)
  input :record, :predecessor,:onkeypress =>"showIcon('img_#{input_name}');", :class => "name-input text-input"
end

def remarks_column(record)
  record.remarks.to_s.gsub("\n","<br/>")
end

def nds_name_column(record)
  if record.nds_name
   link_to record.nds_name, :action => 'show_nds', :id => record.id
  else
   ''
  end
end


end
