module ReceptionsHelper
  
  def show_info(obj)
    result = '<div>'
    result = result + '<p>'+obj.remarks+'</p>'
    result = result + '</div>'
    return result
  end
  
  def free_tags
    datatypes = DataType.where('isactive = 1').order('name').collect {|c| c.name.to_s }
    others = Reception.tag_counts.collect{|t|t.name}
    filling = Filling.active.collect {|f|f.name}
    tmerge = others.concat(filling).sort.uniq
    return datatypes.concat(tmerge).sort.uniq
  end
  
end
