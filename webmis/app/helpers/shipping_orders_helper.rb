module ShippingOrdersHelper
  
  def show_shipping_order_info(id)
   @shipping_order =  ShippingOrder.find(id)
       
  info = "<div style='width: 600px'>
  <div><b>
        #{@shipping_order.id}&nbsp;
        #{@shipping_order.ordering_customer_depot.name}&nbsp;
        #{@shipping_order.source}&nbsp;#{@shipping_order.po_reference}</b>
  </div>
  <div>
    <table class='noborder'>
      <tr>
        <td><b>Status:</b></td>
        <td>#{@shipping_order.status_str} </td>
      </tr>
      <tr>
        <td><b>Article:</b></td>
        <td>#{@shipping_order.article.name} </td>
      </tr>
      <tr>
        <td><b>Initiated by:</b></td>
        <td>#{@shipping_order.init_user.full_name} </td>
      </tr>
    </table>
    <hr><br>
<table class='noborder'>
  <th>Status</th>
  <th>Ext. Reference (LAB)</th>
  <th>Req. Delivery Date</th>
  <th>Stock Order</th>
  <th>Call off Reference</th>
   <th>Transport Reference</th>
  <th>Remarks</th>
  <th>Amount</th>"
  
    
  @shipping_order.shipping_orderlines.each do |l| 
  info = info +
   "<tr>
    <td>#{l.status_str} </td>
    <td>#{l.extern_reference}</td>
    <td>#{l.delivery_date} </td>
    <td>#{StockOrder.find(l.stock_order_id).purchase_reference if l.stock_order_id}</td>
    <td>#{l.call_off_reference}</td>
    <td>#{l.transport_reference}</td>
    <td><i>#{l.remarks} </i></td>
    <td align='right'>#{l.amount} </td>
  </tr>"   
  end
  info = info + "
  <tr>
    <td colspan='7' align='right'>&nbsp;</td>
    <td><hr></td>
  </tr>
  <tr>
    <td colspan='7' align='right'><b>Total:</b></td>
    <td align='right'> #{@shipping_order.shipping_orderlines.sum(:amount)} </td>
  </tr>
</table>
  
  </div>  
    
  
</div>"
            
  return info
  end
  
end