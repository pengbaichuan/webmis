ThinkingSphinx::Index.define :AllocationException, :with => :active_record do

    indexes id, :sortable => true
    indexes exception_type, :sortable => true
    indexes scope, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
