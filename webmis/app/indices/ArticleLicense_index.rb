ThinkingSphinx::Index.define :ArticleLicense, :with => :active_record do

    indexes id, :sortable => true
    indexes label, :sortable => true
    indexes description, :sortable => true
    indexes external_contact_id, :sortable => true
    indexes currency, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
