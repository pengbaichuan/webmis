ThinkingSphinx::Index.define :Article, :with => :active_record do

    indexes id, :sortable => true
    indexes customer_reference, :sortable => true
    indexes name, :sortable => true
    indexes description, :sortable => true
    indexes supplier_reference, :sortable => true
    indexes uom_weight, :sortable => true
    indexes uom_volume, :sortable => true
    indexes currency, :sortable => true
    indexes purchase_currency, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
