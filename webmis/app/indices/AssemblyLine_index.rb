ThinkingSphinx::Index.define :AssemblyLine, :with => :active_record do

    indexes id, :sortable => true
    indexes reference_id, :sortable => true
    indexes area, :sortable => true
    indexes file_name, :sortable => true
    indexes data_release_name, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
