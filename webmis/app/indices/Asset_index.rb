ThinkingSphinx::Index.define :Asset, :with => :active_record do

    indexes id, :sortable => true
    indexes ref_id, :sortable => true
    indexes requested_by, :sortable => true
    indexes remarks, :sortable => true
    indexes assettype, :sortable => true
    indexes title, :sortable => true
    indexes suffix, :sortable => true
    indexes role, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
