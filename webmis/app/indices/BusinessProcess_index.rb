ThinkingSphinx::Index.define :BusinessProcess, :with => :active_record do

    indexes id, :sortable => true
    indexes name, :sortable => true
    indexes description, :sortable => true
    indexes header_link_controller, :sortable => true
    indexes header_link_action, :sortable => true
    indexes line_link_controller, :sortable => true
    indexes line_link_action, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
