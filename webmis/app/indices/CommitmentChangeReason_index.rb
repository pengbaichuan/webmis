ThinkingSphinx::Index.define :CommitmentChangeReason, :with => :active_record do

    indexes id, :sortable => true
    indexes reason, :sortable => true
    indexes description, :sortable => true
    indexes commitment_type, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
