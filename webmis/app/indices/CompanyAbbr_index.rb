ThinkingSphinx::Index.define :CompanyAbbr, :with => :active_record do

    indexes id, :sortable => true
    indexes company_id, :sortable => true
    indexes company_name, :sortable => true
    indexes abbr, :sortable => true
    indexes relation_type, :sortable => true
    indexes ms_abbr_2, :sortable => true
    indexes ms_abbr_3, :sortable => true
    indexes copyright_name, :sortable => true
    indexes updated_by, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
