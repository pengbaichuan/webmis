ThinkingSphinx::Index.define :Company, :with => :active_record do

    indexes id, :sortable => true
    indexes name, :sortable => true
    indexes shipping_address_postalcode, :sortable => true
    indexes shipping_address_street, :sortable => true
    indexes shipping_address_city, :sortable => true
    indexes shipping_address_state, :sortable => true
    indexes shipping_address_country, :sortable => true
    indexes billing_address_postalcode, :sortable => true
    indexes billing_address_street, :sortable => true
    indexes billing_address_city, :sortable => true
    indexes billing_address_state, :sortable => true
    indexes billing_address_country, :sortable => true
    indexes phone_office, :sortable => true
    indexes phone_fax, :sortable => true
    indexes external_id, :sortable => true

  
  has 
  set_property :delta => true
end
