ThinkingSphinx::Index.define :ConfigParameter, :with => :active_record do

    indexes id, :sortable => true
    indexes cfg_name, :sortable => true
    indexes cfg_type, :sortable => true
    indexes cfg_value, :sortable => true
    indexes description, :sortable => true
    indexes cfg_group, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
