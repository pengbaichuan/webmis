ThinkingSphinx::Index.define :ContentReleaseNote, :with => :active_record do

    indexes id, :sortable => true
    indexes title, :sortable => true
    indexes content, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
