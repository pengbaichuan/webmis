ThinkingSphinx::Index.define :ConversionDatabase, :with => :active_record do

    indexes id, :sortable => true
    indexes db_type, :sortable => true
    indexes path_and_file, :sortable => true
    indexes filename, :sortable => true
    indexes path, :sortable => true
    indexes remarks, :sortable => true
    indexes product_id, :sortable => true
    indexes group_value, :sortable => true
    indexes removal_remark, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
