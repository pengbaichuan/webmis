ThinkingSphinx::Index.define :Conversion, :with => :active_record do

    indexes id, :sortable => true
    indexes request_by, :sortable => true
    indexes priority, :sortable => true
    indexes supplier_id, :sortable => true
    indexes product, :sortable => true
    indexes additional_info, :sortable => true
    indexes request_data, :sortable => true
    indexes converter, :sortable => true
    indexes data_files, :sortable => true
    indexes reference_numbers, :sortable => true
    indexes remarks, :sortable => true
    indexes outcome, :sortable => true
    indexes cvtoolenv, :sortable => true
    indexes database_name, :sortable => true
    indexes productid, :sortable => true
    indexes reception_reference, :sortable => true
    indexes purpose, :sortable => true
    indexes legacy_type, :sortable => true
    indexes ca_brn, :sortable => true
    indexes tpd_products, :sortable => true
    indexes tpd_rejected, :sortable => true
    indexes tpd_rejection_remark, :sortable => true
    indexes tpd_result_sender, :sortable => true
    indexes region_name, :sortable => true
    indexes template_filename, :sortable => true
    indexes server_name, :sortable => true
    indexes output_format, :sortable => true
    indexes input_format, :sortable => true
    indexes result_files, :sortable => true
    indexes end_product_yn, :sortable => true
    indexes status_remark, :sortable => true
    indexes rejection_remark, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
