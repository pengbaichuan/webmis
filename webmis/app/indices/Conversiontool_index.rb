ThinkingSphinx::Index.define :Conversiontool, :with => :active_record do

    indexes id, :sortable => true
    indexes code, :sortable => true
    indexes description, :sortable => true
    indexes release, :sortable => true
    indexes tool_type, :sortable => true
    indexes major_release, :sortable => true
    indexes remarks, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
