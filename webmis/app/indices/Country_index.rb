ThinkingSphinx::Index.define :Country, :with => :active_record do

    indexes id, :sortable => true
    indexes name, :sortable => true
    indexes description, :sortable => true
    indexes continent_code, :sortable => true
    indexes country_iso_code, :sortable => true
    indexes country_iso3_code, :sortable => true
    indexes country_num_code, :sortable => true
    indexes city_iso_code, :sortable => true
    indexes city_ascii_name, :sortable => true
    indexes state_code, :sortable => true
    indexes area_code, :sortable => true
    indexes supplier_id, :sortable => true
    indexes type, :sortable => true
    indexes ruby_type, :sortable => true

  
  has 
  set_property :delta => true
end
