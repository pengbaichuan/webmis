ThinkingSphinx::Index.define :CustomerContentValue, :with => :active_record do

    indexes id, :sortable => true
    indexes customer_version, :sortable => true
    indexes value, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
