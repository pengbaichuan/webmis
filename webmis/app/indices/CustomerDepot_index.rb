ThinkingSphinx::Index.define :CustomerDepot, :with => :active_record do

    indexes id, :sortable => true
    indexes name, :sortable => true
    indexes street_name, :sortable => true
    indexes house_number, :sortable => true
    indexes postal_code, :sortable => true
    indexes city, :sortable => true
    indexes country, :sortable => true
    indexes unloading_point, :sortable => true
    indexes billing_country, :sortable => true
    indexes contact_name, :sortable => true
    indexes constact_salutation, :sortable => true
    indexes contact_phone, :sortable => true
    indexes contact_email, :sortable => true
    indexes label, :sortable => true
    indexes vat_identification, :sortable => true
    indexes customer_code, :sortable => true
    indexes supplier_code, :sortable => true
    indexes vat_statement, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
