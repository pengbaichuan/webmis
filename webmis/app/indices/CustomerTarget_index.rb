ThinkingSphinx::Index.define :CustomerTarget, :with => :active_record do

    indexes id, :sortable => true
    indexes label, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
