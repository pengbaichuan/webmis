ThinkingSphinx::Index.define :DataMedium, :with => :active_record do

    indexes id, :sortable => true
    indexes name, :sortable => true
    indexes unit_of_measure, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
