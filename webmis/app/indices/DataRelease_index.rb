ThinkingSphinx::Index.define :DataRelease, :with => :active_record do

    indexes id, :sortable => true
    indexes name, :sortable => true
    indexes release_code, :sortable => true
    indexes aliases, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
