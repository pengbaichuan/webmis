ThinkingSphinx::Index.define :DataSetGroup, :with => :active_record do

    indexes id, :sortable => true
    indexes company_abbr, :sortable => true
    indexes data_release, :sortable => true
    indexes region_code, :sortable => true
    indexes data_type, :sortable => true
    indexes filling, :sortable => true
    indexes suffix, :sortable => true
    indexes description, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
