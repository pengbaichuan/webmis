ThinkingSphinx::Index.define :DataSet, :with => :active_record do

    indexes id, :sortable => true
    indexes name, :sortable => true
    indexes description, :sortable => true
    indexes remarks, :sortable => true
    indexes picked_reception_ids, :sortable => true
    indexes updated_by, :sortable => true
    indexes review_result, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
