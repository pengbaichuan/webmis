ThinkingSphinx::Index.define :DefaultContentReleaseNote, :with => :active_record do

    indexes id, :sortable => true
    indexes title, :sortable => true
    indexes content, :sortable => true

  
  has 
  set_property :delta => true
end
