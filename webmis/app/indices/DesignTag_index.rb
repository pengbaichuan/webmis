ThinkingSphinx::Index.define :DesignTag, :with => :active_record do

    indexes id, :sortable => true
    indexes name, :sortable => true
    indexes description, :sortable => true
    indexes tag_type, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
