ThinkingSphinx::Index.define :DistributionListMember, :with => :active_record do

    indexes id, :sortable => true
    indexes external_contact_id, :sortable => true
    indexes transfer_method, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
