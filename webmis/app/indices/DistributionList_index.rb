ThinkingSphinx::Index.define :DistributionList, :with => :active_record do

    indexes id, :sortable => true
    indexes name, :sortable => true
    indexes remarks, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
