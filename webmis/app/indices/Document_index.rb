ThinkingSphinx::Index.define :Document, :with => :active_record do

    indexes id, :sortable => true
    indexes content_type, :sortable => true
    indexes filename, :sortable => true
    indexes description, :sortable => true
    indexes category, :sortable => true
    indexes sync_filepath, :sortable => true
    indexes checksum, :sortable => true
    indexes document_object_id, :sortable => true
    indexes package_directory, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
