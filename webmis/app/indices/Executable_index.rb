ThinkingSphinx::Index.define :Executable, :with => :active_record do

    indexes id, :sortable => true
    indexes name, :sortable => true
    indexes description, :sortable => true
    indexes input, :sortable => true
    indexes output, :sortable => true
    indexes parameters, :sortable => true
    indexes rules, :sortable => true
    indexes source_data_def_type, :sortable => true
    indexes updated_by, :sortable => true
    indexes metalogic, :sortable => true
    indexes subprocs, :sortable => true
    indexes bug_tracker_id, :sortable => true
    indexes comment, :sortable => true
    indexes status, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
