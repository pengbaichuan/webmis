ThinkingSphinx::Index.define :ExternalContact, :with => :active_record do

    indexes id, :sortable => true
    indexes salutation, :sortable => true
    indexes title, :sortable => true
    indexes first_name, :sortable => true
    indexes last_name, :sortable => true
    indexes primary_address_street, :sortable => true
    indexes primary_address_postalcode, :sortable => true
    indexes primary_address_city, :sortable => true
    indexes primary_address_state, :sortable => true
    indexes primary_address_country, :sortable => true
    indexes phone_work, :sortable => true
    indexes phone_mobile, :sortable => true
    indexes email, :sortable => true
    indexes external_id, :sortable => true

  
  has 
  set_property :delta => true
end
