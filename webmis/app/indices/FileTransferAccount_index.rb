ThinkingSphinx::Index.define :FileTransferAccount, :with => :active_record do

    indexes id, :sortable => true
    indexes transfer_type, :sortable => true
    indexes user_name, :sortable => true
    indexes password, :sortable => true
    indexes remarks, :sortable => true
    indexes address, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
