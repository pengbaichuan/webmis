ThinkingSphinx::Index.define :HighLevelBom, :with => :active_record do

    indexes id, :sortable => true
    indexes revision, :sortable => true
    indexes internal_reference, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
