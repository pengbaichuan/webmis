ThinkingSphinx::Index.define :InboundOrderline, :with => :active_record do

    indexes id, :sortable => true
    indexes drfmsid, :sortable => true
    indexes mediatype, :sortable => true
    indexes area, :sortable => true
    indexes dbrelease, :sortable => true
    indexes deliveryformat, :sortable => true
    indexes dbversion, :sortable => true
    indexes data_release_name, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
