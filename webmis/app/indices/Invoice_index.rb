ThinkingSphinx::Index.define :Invoice, :with => :active_record do

    indexes id, :sortable => true
    indexes reference, :sortable => true
    indexes remarks, :sortable => true
    indexes currency, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
