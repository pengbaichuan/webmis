ThinkingSphinx::Index.define :License, :with => :active_record do

    indexes id, :sortable => true
    indexes licensenumber, :sortable => true
    indexes purpose, :sortable => true
    indexes active_yn, :sortable => true
    indexes comment, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
