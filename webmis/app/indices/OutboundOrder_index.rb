ThinkingSphinx::Index.define :OutboundOrder, :with => :active_record do

    indexes id, :sortable => true
    indexes company_id, :sortable => true
    indexes contact_person, :sortable => true
    indexes shipto_name, :sortable => true
    indexes shipto_postalcode, :sortable => true
    indexes shipto_streetname, :sortable => true
    indexes shipto_country, :sortable => true
    indexes send_by, :sortable => true
    indexes map_no, :sortable => true
    indexes cd_no, :sortable => true
    indexes type, :sortable => true
    indexes extra, :sortable => true
    indexes amount, :sortable => true
    indexes filename, :sortable => true
    indexes from_name, :sortable => true
    indexes remarks, :sortable => true
    indexes courier, :sortable => true
    indexes shipto_state, :sortable => true
    indexes shipto_city, :sortable => true
    indexes shipto_id, :sortable => true
    indexes contact_id, :sortable => true
    indexes sftp_subdirectory, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
