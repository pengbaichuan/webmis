ThinkingSphinx::Index.define :OutboundOrderline, :with => :active_record do

    indexes id, :sortable => true
    indexes map_no, :sortable => true
    indexes cd_no, :sortable => true
    indexes product_id, :sortable => true
    indexes filename, :sortable => true
    indexes product_name, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
