ThinkingSphinx::Index.define :PackingSpecification, :with => :active_record do

    indexes id, :sortable => true
    indexes name, :sortable => true
    indexes description, :sortable => true
    indexes run_script, :sortable => true
    indexes updated_by, :sortable => true
    indexes bug_tracker_id, :sortable => true
    indexes comment, :sortable => true
    indexes status, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
