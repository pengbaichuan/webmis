ThinkingSphinx::Index.define :ParameterSetting, :with => :active_record do

    indexes id, :sortable => true
    indexes parameterset, :sortable => true
    indexes updated_by, :sortable => true
    indexes status, :sortable => true
    indexes bug_tracker_id, :sortable => true
    indexes comment, :sortable => true
    indexes name, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
