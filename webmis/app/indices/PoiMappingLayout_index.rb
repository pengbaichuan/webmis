ThinkingSphinx::Index.define :PoiMappingLayout, :with => :active_record do

    indexes id, :sortable => true
    indexes definition_version, :sortable => true
    indexes description, :sortable => true
    indexes import_hash, :sortable => true
    indexes source_filename, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
