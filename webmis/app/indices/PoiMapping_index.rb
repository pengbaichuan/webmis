ThinkingSphinx::Index.define :PoiMapping, :with => :active_record do

    indexes id, :sortable => true
    indexes map_or_filter_type, :sortable => true
    indexes map_or_filter_value, :sortable => true
    indexes logical_type, :sortable => true
    indexes input_description, :sortable => true
    indexes output_description, :sortable => true
    indexes import_hash, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
