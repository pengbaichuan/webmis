ThinkingSphinx::Index.define :ProcessDataElement, :with => :active_record do

    indexes id, :sortable => true
    indexes remark, :sortable => true
    indexes split_by_value, :sortable => true
    indexes directory, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
