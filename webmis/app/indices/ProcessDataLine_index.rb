ThinkingSphinx::Index.define :ProcessDataLine, :with => :active_record do

    indexes id, :sortable => true
    indexes log_forward, :sortable => true
    indexes log_backward, :sortable => true
    indexes cr_reference, :sortable => true
    indexes change_version, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
