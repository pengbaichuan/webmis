ThinkingSphinx::Index.define :ProcessDataSpecification, :with => :active_record do

    indexes id, :sortable => true
    indexes name, :sortable => true
    indexes directory, :sortable => true
    indexes script_tag, :sortable => true
    indexes script_parameter, :sortable => true
    indexes description, :sortable => true
    indexes split_by_key, :sortable => true
    indexes filling, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
