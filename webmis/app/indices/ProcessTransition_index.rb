ThinkingSphinx::Index.define :ProcessTransition, :with => :active_record do

    indexes id, :sortable => true
    indexes validation, :sortable => true
    indexes from_level, :sortable => true
    indexes to_level, :sortable => true
    indexes action_label, :sortable => true
    indexes cascade_to_level, :sortable => true
    indexes cascade_type, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
