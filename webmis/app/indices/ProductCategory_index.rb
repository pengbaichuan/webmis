ThinkingSphinx::Index.define :ProductCategory, :with => :active_record do

    indexes id, :sortable => true
    indexes abbreviation, :sortable => true
    indexes description, :sortable => true
    indexes pds_import_file, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
