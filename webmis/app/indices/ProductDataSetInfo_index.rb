ThinkingSphinx::Index.define :ProductDataSetInfo, :with => :active_record do

    indexes id, :sortable => true
    indexes description, :sortable => true
    indexes remarks, :sortable => true
    indexes area_name, :sortable => true
    indexes name, :sortable => true
    indexes data_release, :sortable => true
    indexes data_info, :sortable => true
    indexes filling, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
