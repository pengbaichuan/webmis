ThinkingSphinx::Index.define :ProductDesign, :with => :active_record do

    indexes id, :sortable => true
    indexes name, :sortable => true
    indexes description, :sortable => true
    indexes model, :sortable => true
    indexes updated_by, :sortable => true
    indexes bug_tracker_id, :sortable => true
    indexes comment, :sortable => true
    indexes status, :sortable => true
    indexes design_type, :sortable => true
    indexes product_design_md5, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
