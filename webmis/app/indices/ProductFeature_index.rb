ThinkingSphinx::Index.define :ProductFeature, :with => :active_record do

    indexes id, :sortable => true
    indexes name, :sortable => true
    indexes abbr_code, :sortable => true
    indexes description, :sortable => true
    indexes system_translation, :sortable => true
    indexes translation_type, :sortable => true
    indexes updated_by, :sortable => true
    indexes bug_tracker_id, :sortable => true
    indexes comment, :sortable => true
    indexes status, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
