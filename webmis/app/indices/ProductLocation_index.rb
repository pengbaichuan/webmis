ThinkingSphinx::Index.define :ProductLocation, :with => :active_record do

    indexes id, :sortable => true
    indexes product_id, :sortable => true
    indexes product_format, :sortable => true
    indexes area, :sortable => true
    indexes supplier, :sortable => true
    indexes data_release, :sortable => true
    indexes data_path, :sortable => true
    indexes conversiontool, :sortable => true
    indexes storage_path, :sortable => true
    indexes remarks, :sortable => true
    indexes product_type, :sortable => true
    indexes internal_storage_path, :sortable => true
    indexes checksum_storage_file, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
