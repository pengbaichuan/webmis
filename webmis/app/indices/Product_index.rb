ThinkingSphinx::Index.define :Product, :with => :active_record do

    indexes id, :sortable => true
    indexes name, :sortable => true
    indexes customer_name, :sortable => true
    indexes customer_id, :sortable => true
    indexes volumeid, :sortable => true
    indexes detailedcoverage, :sortable => true
    indexes hwncoverage, :sortable => true
    indexes thirdpartydata1, :sortable => true
    indexes thirdpartydata2, :sortable => true
    indexes tmclocation, :sortable => true
    indexes tmcevent, :sortable => true
    indexes remarks, :sortable => true
    indexes zip, :sortable => true
    indexes qxs, :sortable => true
    indexes speedlimits, :sortable => true
    indexes namerotations, :sortable => true
    indexes medium_name, :sortable => true
    indexes nds_name, :sortable => true
    indexes tpd_file, :sortable => true
    indexes test_requirement, :sortable => true
    indexes conversion_settings, :sortable => true
    indexes last_change, :sortable => true
    indexes copyright_name, :sortable => true
    indexes created_from, :sortable => true
    indexes file_detect_regex, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
