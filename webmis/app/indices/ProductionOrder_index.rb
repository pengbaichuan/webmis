ThinkingSphinx::Index.define :ProductionOrder, :with => :active_record do

    indexes id, :sortable => true
    indexes name, :sortable => true
    indexes requestor, :sortable => true
    indexes owner, :sortable => true
    indexes comments, :sortable => true
    indexes test_requirement, :sortable => true
    indexes datarelease_name, :sortable => true
    indexes conversiontool_name, :sortable => true
    indexes supplier_name, :sortable => true
    indexes region_name, :sortable => true
    indexes bp_failreason, :sortable => true
    indexes bp_name, :sortable => true
    indexes bp_status, :sortable => true
    indexes process_transitions_snapshot, :sortable => true
    indexes test_strategy, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
