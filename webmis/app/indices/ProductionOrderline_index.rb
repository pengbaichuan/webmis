ThinkingSphinx::Index.define :ProductionOrderline, :with => :active_record do

    indexes id, :sortable => true
    indexes product_id, :sortable => true
    indexes bp_name, :sortable => true
    indexes remarks, :sortable => true
    indexes owner, :sortable => true
    indexes test_requirement, :sortable => true
    indexes product_accepted, :sortable => true
    indexes customer_feedback, :sortable => true
    indexes product_design, :sortable => true
    indexes bp_message, :sortable => true
    indexes created_from, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
