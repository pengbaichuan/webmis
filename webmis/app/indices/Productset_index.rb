ThinkingSphinx::Index.define :Productset, :with => :active_record do

    indexes id, :sortable => true
    indexes name, :sortable => true
    indexes supplier_id, :sortable => true
    indexes customer_id, :sortable => true
    indexes firsttier_id, :sortable => true
    indexes customer_name, :sortable => true
    indexes firsttiername, :sortable => true
    indexes state, :sortable => true
    indexes setchanges, :sortable => true
    indexes remarks, :sortable => true
    indexes internal_name, :sortable => true
    indexes description, :sortable => true
    indexes owner, :sortable => true
    indexes datarelease_name, :sortable => true
    indexes platform_name, :sortable => true
    indexes ciq, :sortable => true
    indexes nds_name, :sortable => true
    indexes area_name, :sortable => true
    indexes shippingmethod_name, :sortable => true
    indexes underconstructiondate, :sortable => true
    indexes conversiontool_name, :sortable => true
    indexes medium_name, :sortable => true
    indexes approval_text, :sortable => true
    indexes status, :sortable => true
    indexes supplier_name, :sortable => true
    indexes tpdspec, :sortable => true
    indexes predecessor, :sortable => true
    indexes setcode, :sortable => true
    indexes tmc_file, :sortable => true
    indexes tmc_info_name, :sortable => true
    indexes tpd_file, :sortable => true
    indexes datasource_extrainfo, :sortable => true
    indexes test_requirement, :sortable => true
    indexes settype, :sortable => true
    indexes areaabbr, :sortable => true
    indexes contabbr, :sortable => true
    indexes output_format, :sortable => true
    indexes output_format_extrainfo, :sortable => true
    indexes conversion_settings, :sortable => true
    indexes set_release_major, :sortable => true
    indexes set_release_minor, :sortable => true
    indexes data_set_description, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
