ThinkingSphinx::Index.define :PurchaseOrder, :with => :active_record do

    indexes id, :sortable => true
    indexes purchase_reference, :sortable => true
    indexes remarks, :sortable => true
    indexes external_contact_id, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
