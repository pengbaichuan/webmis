ThinkingSphinx::Index.define :PurchaseOrderline, :with => :active_record do

    indexes id, :sortable => true
    indexes item_description, :sortable => true
    indexes item_reference, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
