ThinkingSphinx::Index.define :Reception, :with => :active_record do

    indexes id, :sortable => true
    indexes dataversion, :sortable => true
    indexes data_type_id, :sortable => true
    indexes referenceid, :sortable => true
    indexes purpose, :sortable => true
    indexes remarks, :sortable => true
    indexes storage_location, :sortable => true
    indexes area_type, :sortable => true
    indexes userid, :sortable => true
    indexes user_name, :sortable => true
    indexes conversiontool, :sortable => true
    indexes region_name, :sortable => true
    indexes status, :sortable => true
    indexes priority, :sortable => true
    indexes data_intake_result_text, :sortable => true
    indexes validation_result_text, :sortable => true
    indexes validated_by, :sortable => true
    indexes data_intake_by, :sortable => true
    indexes ruleset, :sortable => true
    indexes source_location, :sortable => true
    indexes rejection_remark, :sortable => true
    indexes request_remarks, :sortable => true
    indexes requested_by, :sortable => true
    indexes references, :sortable => true
    indexes product_id, :sortable => true
    indexes removal_mark_by_username, :sortable => true
    indexes removed_by_username, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
