ThinkingSphinx::Index.define :ReleaseNoteAttachment, :with => :active_record do

    indexes id, :sortable => true
    indexes title, :sortable => true
    indexes package_directory, :sortable => true
    indexes description, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
