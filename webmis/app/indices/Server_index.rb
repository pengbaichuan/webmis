ThinkingSphinx::Index.define :Server, :with => :active_record do

    indexes id, :sortable => true
    indexes server_id, :sortable => true
    indexes server_remark, :sortable => true
    indexes diskcapacity, :sortable => true
    indexes processor, :sortable => true
    indexes additionalprocess, :sortable => true
    indexes virtual_host, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
