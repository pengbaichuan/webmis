ThinkingSphinx::Index.define :ShippingOrder, :with => :active_record do

    indexes id, :sortable => true
    indexes po_reference, :sortable => true
    indexes source, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
