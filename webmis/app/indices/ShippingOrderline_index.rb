ThinkingSphinx::Index.define :ShippingOrderline, :with => :active_record do

    indexes id, :sortable => true
    indexes call_off_reference, :sortable => true
    indexes remarks, :sortable => true
    indexes extern_reference, :sortable => true
    indexes transport_reference, :sortable => true
    indexes invoice_reference, :sortable => true
    indexes invoice_remarks, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
