ThinkingSphinx::Index.define :ShippingSpecification, :with => :active_record do

    indexes id, :sortable => true
    indexes name, :sortable => true
    indexes external_contact_id, :sortable => true
    indexes shipment_type, :sortable => true
    indexes courier, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
