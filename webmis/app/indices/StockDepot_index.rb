ThinkingSphinx::Index.define :StockDepot, :with => :active_record do

    indexes id, :sortable => true
    indexes name, :sortable => true
    indexes street_name, :sortable => true
    indexes postal_code, :sortable => true
    indexes city_name, :sortable => true
    indexes country_name, :sortable => true
    indexes contact_name, :sortable => true
    indexes contact_email, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
