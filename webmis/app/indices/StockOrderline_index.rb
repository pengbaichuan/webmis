ThinkingSphinx::Index.define :StockOrderline, :with => :active_record do

    indexes id, :sortable => true
    indexes stock_reference, :sortable => true
    indexes remarks, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
