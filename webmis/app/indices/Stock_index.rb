ThinkingSphinx::Index.define :Stock, :with => :active_record do

    indexes id, :sortable => true
    indexes commercial_name, :sortable => true
    indexes key_type, :sortable => true
    indexes supplier_name, :sortable => true
    indexes cd_type, :sortable => true
    indexes product_name, :sortable => true
    indexes product_ref, :sortable => true
    indexes brn, :sortable => true
    indexes tpd_phase_1_description, :sortable => true
    indexes tpd_phase_2_description, :sortable => true
    indexes tpd_iba_description, :sortable => true
    indexes cd_template_name, :sortable => true
    indexes databasename, :sortable => true
    indexes extra, :sortable => true
    indexes location, :sortable => true
    indexes rds_tmcfile, :sortable => true
    indexes medium_number, :sortable => true
    indexes region_name, :sortable => true
    indexes location_type, :sortable => true
    indexes borrby, :sortable => true
    indexes borron, :sortable => true
    indexes borrback, :sortable => true
    indexes requested_by, :sortable => true
    indexes end_customer_name, :sortable => true
    indexes recognition_file, :sortable => true
    indexes purpose, :sortable => true
    indexes request_notes, :sortable => true
    indexes status, :sortable => true
    indexes ciq, :sortable => true
    indexes status_remark, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
