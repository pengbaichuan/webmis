ThinkingSphinx::Index.define :TestPlan, :with => :active_record do

    indexes id, :sortable => true
    indexes name, :sortable => true
    indexes plan_details, :sortable => true
    indexes status, :sortable => true
    indexes updated_by, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
