ThinkingSphinx::Index.define :TestRequestJob, :with => :active_record do

    indexes id, :sortable => true
    indexes conversion_id, :sortable => true
    indexes run_server, :sortable => true
    indexes run_command, :sortable => true
    indexes run_script, :sortable => true
    indexes run_pid, :sortable => true
    indexes working_dir, :sortable => true
    indexes fail_reason, :sortable => true
    indexes type, :sortable => true
    indexes bug_tracker_id, :sortable => true
    indexes group_value, :sortable => true
    indexes action_log, :sortable => true
    indexes fail_category, :sortable => true
    indexes scheduler_log, :sortable => true
    indexes test_case_name, :sortable => true
    indexes reserved_path, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
