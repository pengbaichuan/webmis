ThinkingSphinx::Index.define :TestRequest, :with => :active_record do

    indexes id, :sortable => true
    indexes status, :sortable => true
    indexes product_id, :sortable => true
    indexes request_remarks, :sortable => true
    indexes result_remarks, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
