ThinkingSphinx::Index.define :TestToolRelease, :with => :active_record do

    indexes id, :sortable => true
    indexes release_name, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
