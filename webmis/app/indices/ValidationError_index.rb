ThinkingSphinx::Index.define :ValidationError, :with => :active_record do

    indexes id, :sortable => true
    indexes name, :sortable => true
    indexes ticket_nr, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
