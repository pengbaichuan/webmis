ThinkingSphinx::Index.define :WarehouseStockStatus, :with => :active_record do

    indexes id, :sortable => true
    indexes remarks, :sortable => true

  
  has created_at, updated_at 
  set_property :delta => true
end
