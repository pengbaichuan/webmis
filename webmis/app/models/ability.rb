class Ability
  include CanCan::Ability

  def initialize(user)
    if user
      if user.has_role?(["admin"])
        can :manage, :all
      else
        can :read, :all # default when controller not configured in table role_actions
        RoleAction.feed_ability.each do |controller,action_roles|
          model = Object.const_get controller.classify
          action_roles.each do |ar|
            ar.each do |action,roles|
              can action.to_sym, [model] if user.has_role?(roles)
            end
          end
        end
      end
    else
      return false
    end
  end
  
end