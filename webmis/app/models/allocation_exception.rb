class AllocationException < ActiveRecord::Base
  acts_as_ordered_taggable_on :tags

  scope :available, -> {where(:removed_yn => [false,nil])}

  def set_tagmodel(model=nil)
    ProductDesign.store_tag_model(self,model)
  end

  def tagmodel
    ProductDesign.tagmodel(self)
  end

  def self.add_exception(model, coverage=nil, scope="CONVERSION", exception_type="N.A.")
    ex = AllocationException.new
    ex.scope = scope
    ex.set_tagmodel(model)
    ex.set_tag_list_on(:context_coverage, coverage) if coverage
    ex.removed_yn = false
    ex.save
  end

  def self.find_by_tagmodel_and_coverage(results, model, coverage)
     ProductDesign.find_by_tagmodel_and_coverage(results, model, coverage)
  end

end
