class ApiKey < ActiveRecord::Base
  acts_as_taggable
belongs_to :user
 def self.new
   apikey = super
   apikey.key = Digest::SHA1.hexdigest("--#{Time.now.to_s}--mapscape--")[0,25]
   return apikey
 end

end
