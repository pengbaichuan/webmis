class Area < Region

  # belongs_to :supplier
  belongs_to :company_abbr, :class_name => "CompanyAbbr", :foreign_key =>  "supplier_id"

  validates :name ,:presence => true, :uniqueness => true
  validates :area_code ,:presence => true
  validate :region_code_must_be_uniq
  validate :name_must_be_uniq

  def region_code_must_be_uniq
    if !self.area_code.blank?
      m = Region.active.find_all_by_region_code(self.area_code,false)
      if ( m.size == 1 && m.last.id != self.id ) || ( m.size > 1 )
        errors.add(:area_code, "is already in use by #{m.collect{|r|"#{r.ruby_type}: #{r.id} #{r.name}"}.join(" | ")}")
      end
    end
  end

  def name_must_be_uniq
    if !self.name.blank?
      n = Region.active.where("name = ?" ,self.name)
      if ( n.size == 1 && n.last.id != self.id ) || ( n.size > 1 )
        errors.add(:name, "is already in use by #{n.collect{|r|"#{r.ruby_type}: #{r.id} #{r.name}"}.join(" | ")}")
      end
    end
  end

end
