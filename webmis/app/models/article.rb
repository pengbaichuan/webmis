class Article < ActiveRecord::Base
  belongs_to :high_level_bom
  belongs_to :create_user, :class_name => "User", :foreign_key =>  "created_by_user_id"
  has_many :stock_orderlines
  has_many :article_licenses, :through => :high_level_bom

  validates :high_level_bom_id, :presence => true
  validates :customer_reference, :presence => true
  validates :price, :presence => true
  validates :currency, :presence => true
end
