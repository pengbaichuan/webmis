class ArticleLicense < ActiveRecord::Base
  belongs_to :external_contact
  belongs_to :stock_depot
  belongs_to :high_level_bom
  has_many :included_article_license_purchase_orderlines

  def self.active
    where({:active_yn => true}).order(:label)
  end

end
