class AssemblyHeader < ActiveRecord::Base
 
 validates_uniqueness_of :product_id, :name, :allow_blank => true
 validates :output_format,:presence => true
 
 has_many :assembly_lines,  :dependent => :destroy
 has_many :conversions, :foreign_key => "assembly_id", :dependent => :nullify
 belongs_to :product, :primary_key => :volumeid, :foreign_key => :product_id
 
 validates :name,:presence => true, :if => Proc.new{|f| f.product_id.blank? }
 validates :product_id,:presence => true, :if => Proc.new{|f| f.name.blank? }
 
 scope :test, -> {where(['(product_id = ? OR product_id IS NULL) AND (name IS NOT NULL AND name != ?)', '','']).order(:name)}

  def export_for_conversion
    refs, data_files = ""
    lines_per_reference = {}

    commentref =  self.product_id.blank? ? "" : "\# Product: " + self.product_id + "\n"
    commentref += self.name.blank? ? "" : "\# Assembly name: " + self.name.to_s + "\n\#\n"

    lines = self.assembly_lines.order("substring_index(file_name,'/',7),reverse(substring_index(reverse(file_name),\"/\",1))")

    lines.each do |line|
      if !lines_per_reference[line.reference_id]
        refs += line.reference_id + "\n"
        clean_line = line.file_name.split(' ')[0]
        cd = ConversionDatabase.available.where(:path_and_file => clean_line)
        if cd.empty?
          lines_per_reference[line.reference_id] = "\# conversion database not available\n"
        elsif cd.last.db_type.blank? || DataType.find_by_name(cd.last.db_type).nil? || !DataType.find_by_name(cd.last.db_type).is_intermediate
          lines_per_reference[line.reference_id] = "\# datatype unknown\n"
        elsif cd.last.tagmodel.nil? || cd.last.tagmodel.empty?
          lines_per_reference[line.reference_id] = "\# filling not specified\n"
        else
          tagmodel=cd.last.tagmodel
          filling = tagmodel["filling"] ? tagmodel["filling"].join(' ') : ''
          if tagmodel['core']
            input_datatype = tagmodel['core'].first.split('|')[0]
            input_datatype = tagmodel['core'].first.split('-')[0] if (input_datatype.size == tagmodel['core'].first.size) # no pipes available, handle legacy case
          else
            input_datatype = ""
          end
          lines_per_reference[line.reference_id] = "\# #{input_datatype.upcase} #{filling.upcase}\n"
        end
      end
      lines_per_reference[line.reference_id] += "01 #{line.file_name}\n"
    end
    data_files = commentref + lines_per_reference.values.join
    return [refs,data_files]
  end
end
