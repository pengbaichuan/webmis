class AssemblyLine < ActiveRecord::Base
  
  belongs_to :data_release
  belongs_to :assembly_header

  comma do
    reference_id
    area
    company_abbr_id
    file_name
  end

  def not_uniq?
    lines = self.assembly_header.assembly_lines.collect{|l|l.file_name if l.id != self.id}
    return lines.include?(self.file_name)
  end

  def available?
    if self.source_type
      if self.source_obj
        case self.source_obj.class.name
        when "ProcessDataElement"
          return self.source_obj.available?(self.file_name)
        else
          return self.source_obj.available?
        end
      else
        return false
      end
    else
      return false
    end
  end

  def filling
    f = ""
    if self.source_obj && self.source_obj.class.name == "ConversionDatabase"
      f = self.source_obj.filling_and_core.join(" ")
    end
    return f
  end

  def data_type_and_filling
    if self.source_type
      if self.source_obj
        case self.source_obj.class.name
        when "ProcessDataElement"
          df = self.source_obj.data_set.data_type.name
          df += self.source_obj.data_set.filling ? "#{ - self.source_obj.data_set.filling.name}" : ''
          return df
        when "ConversionDatabase"
          df = self.source_obj.db_type
          df += self.source_obj.filling_and_core.empty? ? '' : " - #{self.source_obj.filling_and_core.join(" ")}"
          return df
        when "Reception"
          df = self.source_obj.data_type.name
          df += self.source_obj.filling ? "#{ - self.source_obj.filling.name}" : ''
        else
          return ''
        end
      else
        return ''
      end
    else
      return ''
    end
  end

  def source_obj
    return self.source_type.constantize.find(self.source_id) if self.source_type && self.source_id
  end
end
