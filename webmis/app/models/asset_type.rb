class AssetType < ActiveRecord::Base
  def next_serial params
    key = params[:id]+params[:rangetype].to_s
    s = SeqInt.find_by_name(key)
    if s 
      s.current_value = s.current_value + 1
      s.save
    else
      s = SeqInt.new
      s.name = key
      # check if range applicable
      range = AssetTypeNumberRange.find_by_name(params[:rangetype]) if params[:rangetype]
      if range 
        s.current_value = range.start_range
      else
        s.current_value = 1
      end
      s.save
    end
    return s.current_value
  end

 def get_next_number params
   return eval(gencode)
 end

end
