class BundleContent < ActiveRecord::Base
  belongs_to :executable
  belongs_to :cvtool_bundle
  belongs_to :conversiontool
end
