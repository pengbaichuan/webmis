class BusinessProcess < ActiveRecord::Base
  validates :name, :presence => true
  scope :core_bp, -> {where(:core_bp_yn => true).order(:name)}

  def system_support_yn
    normalized_name = self.name.to_s.downcase.gsub(" ", "_")
    return Conversion.new.respond_to?(normalized_name) || ProductionOrder.new.respond_to?(normalized_name) || ProductionOrderline.new.respond_to?(normalized_name)
  end
end
