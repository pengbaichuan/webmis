class Callsession < ActiveRecord::Base
	belongs_to :scenarios
	belongs_to :user

	has_many :calls
	belongs_to :scenario
end
