class Category < ActiveRecord::Base
  has_many :category_attributes
  has_many :point_attributes, :through => :category_attributes
  acts_as_tree :order => "feature_name"

  def parent
    if self.parent_id
      return Category.where("id = #{self.parent_id}").first
    else
      return nil
    end
  end

  def has_parent?
    return !(self.parent_id == nil)
  end

  def add_default_attributes
    PointAttribute.all.each do |x|
      ca = CategoryAttribute.new
      ca.category_id = self.id
      ca.point_attribute_id = x.id
      ca.save
    end
  end

end
