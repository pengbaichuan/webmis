class Checklog
  attr_accessor :loglevel, :only_check_yn, :result, :attributes, :output, :sub_checklog

  LOGLEVEL_NONE = 1
  LOGLEVEL_ERROR = 10
  LOGLEVEL_WARNING = 20
  LOGLEVEL_INFO = 50
  LOGLEVEL_DEBUG = 100
  LOGLEVEL_BACKTRACE = 200

  @@loglevel_array = Array.new
  @@loglevel_array[LOGLEVEL_NONE]      = 'None'
  @@loglevel_array[LOGLEVEL_ERROR]     = 'Error'
  @@loglevel_array[LOGLEVEL_WARNING]   = 'Warning'
  @@loglevel_array[LOGLEVEL_INFO]      = 'Info'
  @@loglevel_array[LOGLEVEL_DEBUG]     = 'Debug'
  @@loglevel_array[LOGLEVEL_BACKTRACE] = "Backtrace"

  STATUS_OK        = 'OK'
  STATUS_ERROR     = 'Error'
  STATUS_EXCEPTION = 'Exception'

  def loglevel_str
    return @@loglevel_array[loglevel]
  end

  def self.loglevel_prefix(loglevel)
    return @@loglevel_array[loglevel].upcase
  end

  def self.loglevel_array
    return @@loglevel_array
  end

  def self.loglevel_hash
    s_hash = Hash.new
    @@loglevel_array.each do |s|
      # loglevel_none is only for private use, not a log level accessable to the user
      s_hash[s] = @@loglevel_array.index(s) unless s.nil? || s == @@loglevel_array[LOGLEVEL_NONE]
    end
    return s_hash.sort_by { |k,v| v }
  end

  def initialize(only_check, loglvl = LOGLEVEL_INFO)
    @loglevel = loglvl
    @only_check_yn = only_check
    @result = STATUS_OK
    @attributes = []
    @output = []
    @sub_checklog = {}
  end

  def add_attribute(name, value, remark, loglvl)
    self.attributes << ChecklogAttribute.new(name, value, remark, loglvl)
    self.loglevel = loglvl if self.loglevel > loglvl
  end

  def add_attributes(attributes, remark = "", loglvl = LOGLEVEL_INFO)
    attributes.each do |name, value|
      self.add_attribute(name, value, remark, loglvl)
    end
  end

  def add_output_line(line, loglvl, to_console_yn = false)
    self.output << ChecklogOutputLine.new(line, loglvl)
    self.loglevel = loglvl if self.loglevel > loglvl
    puts line if to_console_yn
  end

  def add_error_line(line, to_console_yn = false)
    self.add_output_line("ERROR: " + line, LOGLEVEL_ERROR, to_console_yn)
  end

  def add_warning_line(line, to_console_yn = false)
    self.add_output_line("WARNING: " + line, LOGLEVEL_WARNING, to_console_yn)
  end

  def add_info_line(line, to_console_yn = false)
    self.add_output_line("INFO: " + line, LOGLEVEL_INFO, to_console_yn)
  end

  def add_debug_line(line, to_console_yn = false)
    self.add_output_line(line, LOGLEVEL_DEBUG, to_console_yn)
  end

  def add_backtrace_line(line, to_console_yn = false)
    self.add_output_line(line, LOGLEVEL_BACKTRACE, to_console_yn)
  end

  def update_result(new_result, loglvl = LOGLEVEL_INFO)
    # only update the result if the current result value is still 'STATUS_OK'
    self.result = new_result if self.result == STATUS_OK
    self.loglevel = loglvl if self.loglevel > loglvl
  end

  def new_sub(key, loglvl = nil)
    unless sublog = self.sub_checklog[key]
      # the sub checklog does not exist yet
      sublog = Checklog.new(self.only_check_yn, loglvl || self.loglevel)
      self.sub_checklog[key] = sublog
    end
    return sublog
  end

  def add_sub(key, new_sublog)
    if old_sublog = self.sub_checklog[key]
      # two sublogs for the same object, merge them into new_sublog
      # and let the subchecklog for this key point to new_sublog
      new_sublog.update_result(old_sublog.result, old_sublog.loglevel)
      new_sublog.attributes = old_sublog.attributes + new_sublog.attributes
      new_sublog.output = old_sublog.output + new_sublog.output
    end
    self.sub_checklog[key] = new_sublog
    new_sublog.only_check_yn = self.only_check_yn
  end

  def errors_or_warnings?
    bool = false
    self.output.each do |line|
      if [Checklog::LOGLEVEL_ERROR,Checklog::LOGLEVEL_WARNING].include?(line.loglevel)
        bool = true
        break
      end
      self.sub_checklog.each do |key, sub|
          bool = bool || sub.errors_or_warnings?
      end
    end
    bool
  end

  def to_s
    result  = "STATUS: " + self.loglevel_str + "\r\n"
    result += "ATTRIBUTES:\r\n"
    self.attributes.each do |attribute|
      result += attribute.attr_name.to_s + ": " + attribute.attr_value.to_s + (attribute.attr_remark.blank? ? "" : "(#{attribute.attr_remark})") + "\r\n"
    end
    result += "OUTPUT:"
    self.output.each do |outputline|
      result += outputline.line + "\r\n"
    end
    self.sub_checklog.each do |key, sub|
      result += "BEGIN SUB CHECKLOG " + key + "\r\n"
      result += sub.to_s
      result += "END SUB CHECKLOG " + key + "\r\n"
    end
    return result
  end




end