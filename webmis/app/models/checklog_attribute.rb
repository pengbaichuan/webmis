class ChecklogAttribute
  attr_accessor :attr_name, :attr_value, :attr_remark, :loglevel

  def initialize(name, value, remark, loglvl)
    @attr_name, @attr_value, @attr_remark, @loglevel  = name, value, remark, loglvl
  end
end