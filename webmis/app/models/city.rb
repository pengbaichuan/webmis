class City < Region
  validates :city_iso_code , :uniqueness => true,:presence => true
  validates :name , :uniqueness => true,:presence => true
end
