class CommitmentChangeReason < ActiveRecord::Base
  has_many :commitment_dates
  validates :reason, :commitment_type, :presence => true

  def self.commitment_types_select
    [[ProductionOrder::COMMITMENT_BASELINE.titleize, ProductionOrder::COMMITMENT_BASELINE],
     [ProductionOrder::COMMITMENT_MODIFIED.titleize, ProductionOrder::COMMITMENT_MODIFIED]
    ]
  end

  def self.initial(commitment_type)
    self.where(:commitment_type => commitment_type).first
  end
end