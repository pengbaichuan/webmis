class CommitmentDate < ActiveRecord::Base
  belongs_to :production_order
  belongs_to :commitment_change_reason

  validates :commitment_change_reason_id, :commitment_type, :presence => true

  def self.commitment_types_select
    [[ProductionOrder::COMMITMENT_BASELINE.titleize, ProductionOrder::COMMITMENT_BASELINE],
     [ProductionOrder::COMMITMENT_MODIFIED.titleize, ProductionOrder::COMMITMENT_MODIFIED]
    ]
  end
end