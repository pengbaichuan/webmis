class Company < ActiveRecord::Base
  validates :name , :uniqueness => true,:presence => true
  has_many :external_contacts

  STATUS_ACTIVE   = 100
  STATUS_OBSOLETE = 999

  @@statusarray = Array.new
  @@statusarray[STATUS_ACTIVE]     = 'Active'
  @@statusarray[STATUS_OBSOLETE]   = 'Removed'

  scope :active, -> {where(:status => STATUS_ACTIVE)}

  def status_string
    return @@statusarray[self.status] if self.status
  end

  def change_status(status)
    case status
    when "#{STATUS_ACTIVE}"
      if self.status != status
        self.status = status
      end
    when "#{STATUS_OBSOLETE}"
      if self.status != status && self.remove_allowed?
        self.status = status
      end
    end
    self.save
  end

  def remove_allowed?
    self.status != STATUS_OBSOLETE
  end
end

