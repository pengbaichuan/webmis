class CompanyAbbr < ActiveRecord::Base
  version_fu
  belongs_to :company
  has_many :data_releases
  has_many :receptions, :through => :data_releases
  has_many :file_transfer_accounts, :foreign_key => :company_abbrs_id
  has_many :productsets
  has_many :product_lines
  has_many :product_designs
  
  scope :active, -> {where(:is_active => true).order(:ms_abbr_3)}
  
  validates :ms_abbr_3, :presence => true
  validates_uniqueness_of :ms_abbr_3
  validates :dakota_id, :presence => true
  validates :company_name, :presence => true
  acts_as_taggable
  before_save :update_supplier_tags
  
  comma do
    id
    company_name
    ms_abbr_3
    copyright_name
    is_active
  end

  def update_supplier_tags
    DataSet.transaction do
      if self.company_name != self.company_name_was
        data_sets = DataSet.where(company_abbr_id:self.id)
        data_sets.each do |ds|
          ds.update_tag_list
          ds.save!
        end
      end
    end
  end

  def self.find_all_for_module(app_module)
    CompanyAbbr.all.order(:company_name)
  end

  def name
    company_name
  end
  
  def self.select_tag
    self.find_all_for_module('MIS').collect{|c|[c.company_name,c.id] if c.is_active == true}.compact
  end

  def dakota_supplier_id
    if !self.dakota_id.blank?
      return "#{self.ms_abbr_3.downcase}, #{self.company_name.strip}, #{self.dakota_id}"
    end
  end

  def self.dakota_supplier_ids
    csvcontent = ""
    suppliers = CompanyAbbr.where("dakota_id is not null OR dakota_id != ''").order(:dakota_id)
    suppliers.each do |supplier|
      csvcontent += "#{supplier.dakota_supplier_id}\n"
    end
    return csvcontent
  end

  def self.dakota_supplier_ids_file
    csvcontent = "# Supplier\n# code, name, number\n# Last line, should be an empty line !\n# Don't use tab's!\n\n"
    csvcontent += CompanyAbbr.dakota_supplier_ids
    csvcontent += "\n"
  end

  def self.generate_dakota_id
    return CompanyAbbr.where("dakota_id is not null OR dakota_id != ''").order(:dakota_id).last.dakota_id + 1
  end

end
