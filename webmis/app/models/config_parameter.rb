class ConfigParameter < ActiveRecord::Base
  version_fu 
  after_initialize :assign_defaults, :if => Proc.new{|c| c.cfg_group.blank? }
  validates :cfg_name, :presence => true, :uniqueness => {:scope => :project_id}
  validates :cfg_type, :presence => true
  validates :cfg_value,:presence => true

  belongs_to :project
  has_one :environment, :through => :project
  
  @@cfg_types = Array.new
  @@cfg_types[0] = 'string' #[0] should be 'string' as default
  @@cfg_types[1] = 'regex'
  @@cfg_types[2] = 'json'
  @@cfg_types[3] = 'array'
  @@cfg_types[4] = 'eval'
  @@cfg_types[5] = 'boolean'

  before_save :defaults

  def defaults
    self.project_id = Project.default.id if self.project_id.blank?
  end
  
  def assign_defaults
    self.cfg_type = @@cfg_types[0] if self.cfg_type.blank?
  end
  
  def self.config_types
    return @@cfg_types
  end
  
  def group
    @group = []
    if self.cfg_group.blank?
      @group = ConfigParameter.where(:cfg_group => self.cfg_group)
    end
    return @group
  end
  
  def self.get(name,project_id='')
    default_project_id = Project.default.id
    if project_id.blank?
      project_id = ConfigParameter.get_project_id
    end
    hit = ConfigParameter.where(:cfg_name => name, :project_id => project_id).limit(1)
    if hit.empty?
      # parameter not set for project, so use production value
      hit = ConfigParameter.where(:cfg_name => name, :project_id => default_project_id).limit(1)
    end
    if !hit.empty?
      if hit.first.cfg_type == 'boolean'
        case hit.first.cfg_value
        when (/(true|t|yes|y|1)$/i)
          return true
        when (/(false|f|no|n|0)$/i)
          return false
        else
          return false
        end 
      else
        return hit.first.cfg_value 
      end
    end
  end

  def self.development_env_allowed_cfg_names
    allowed_names = %w[age_removal_conversion_databases_in_months
                       conversion_job_crash_dir
                       conversion_job_intermediate_dir
                       conversion_job_result_dir
                       default_database_expiry_in_days
                       project_color_profile
                       enforce_review_procedure
                      ]
    return allowed_names
  end

  def self.get_project_id
   project_id = Thread.current[:project_id]
   if !project_id
     project_id = Project.default.id
     if Rails.env != 'test'
       puts "No project.id given and no Thread.current[:project_id] found, so used default_project_name configured in ConfigParameter."
     end
   else
     puts "Thread.current[:project_id] USED !" unless Rails.env == 'test'
   end
   return project_id
  end
  
  def self.create_new_project_set(project_id)
    ConfigParameter.transaction do
      ConfigParameter.development_env_allowed_cfg_names.each do |cfg_name|
        new_cfg_par = ConfigParameter.where(:cfg_name=>cfg_name,:project_id=>Project.default.id).last.dup
        new_cfg_par.project_id = project_id
        new_cfg_par.cfg_value = ENV['default_database_expiry_in_days'] if cfg_name == 'default_database_expiry_in_days'
        new_cfg_par.cfg_value = "blue" if cfg_name == 'project_color_profile'
        new_cfg_par.save!
      end
    end
  end

end
