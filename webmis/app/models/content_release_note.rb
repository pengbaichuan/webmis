class ContentReleaseNote < ActiveRecord::Base
  belongs_to :product_release_note
  validates :product_release_note_id,:title, :presence => true
  
  scope :deleted, -> {where(:status => 100)}
  default_scope {where("status < 100").order('rank, subrank asc')}
  
  def self.include_deleted_in
   ContentReleaseNote.with_exclusive_scope { yield }
  end
  
  @@statusarray     = Array.new
  @@statusarray[0]   = 'Draft'
  @@statusarray[10]  = 'Review'
  @@statusarray[30]  = 'Approved'
  @@statusarray[100] = 'Removed'

  def self.status_hash
    s_hash = Hash.new
    @@statusarray.each do |s|
      if !s.nil?
        s_hash[s] = @@statusarray.index(s)
      end
    end
    return s_hash.sort_by { |k,v| v }
  end
  
  def status_string
    @@statusarray[self.status]
  end
  
  def self.prepare_default_content(product_cat_id,rel_note_id)
    customer_content_item = nil
    @items = DefaultContentReleaseNote.where(:product_category_id => product_cat_id, :status => 10)
    @items.each do |i|
      if i.use_customer_content_yn
        customer_content_item = i
      else
        c = ContentReleaseNote.new
        c.title = i.title
        c.content = i.content
        c.product_release_note_id = rel_note_id
        c.user_id = i.user_id
        c.status = i.status
        c.start_on_new_page = i.start_on_new_page
        c.product_list = i.product_list
        c.addition_required_yn = i.addition_required_yn
        c.rank = i.rank
        c.subrank = i.subrank
        c.allow_jira_import_yn =  i.allow_jira_import_yn
  #      cr = ContentReleaseNote.where("rank <= 0 and product_release_note_id = #{rel_note_id}").last
  #      if !cr.nil?
  #        c.rank = cr.rank - 1 # on top
  #      else
  #        c.rank = 0
  #      end  
        c.save
      end
    end

    # insert customer content in the release notes
    if customer_content_item
      product_release_note = ProductReleaseNote.find(rel_note_id)

      # determine the production orderlines with customer content to be included in the release notes
      included_production_orderlines = []
      if product_release_note.production_order
        product_release_note.production_order.production_orderlines.where("bp_name !=  'CANCELLED'").each do |po|
          included_production_orderlines << po if po.customer_contents_for_release_notes?
        end
      elsif product_release_note.production_orderline.customer_contents_for_release_notes?
        included_production_orderlines << product_release_note.production_orderline
      end

      rank = customer_content_item.rank
      rank = rank.next if customer_content_item.subrank.to_i > 0

      if included_production_orderlines.size > 1 && customer_content_item.rank
        # make room to insert the customer contents in the release notes at the given position.
        ContentReleaseNote.where("product_release_note_id = '#{rel_note_id}' AND rank > #{customer_content_item.rank}").each do |c|
          c.rank += customer_content_item.rank - 1
          c.save
        end
      elsif included_production_orderlines.size == 0 && customer_content_item.rank
        # no customer contents, renumber the elements coming after it.
        ContentReleaseNote.where("product_release_note_id = '#{rel_note_id}' AND rank > #{customer_content_item.rank}").each do |c|
          c.rank = c.rank.pred
          c.save
        end
      end

      # insert the customer content, each included_production_orderline has its own rank, the content itself uses the subranks.
      included_production_orderlines.each do |orderline|
        c = ContentReleaseNote.new
        c.title = customer_content_item.title + ' ' + orderline.product.name
        c.content = customer_content_item.content
        c.product_release_note_id = rel_note_id
        c.user_id = customer_content_item.user_id
        c.status = customer_content_item.status
        c.start_on_new_page = customer_content_item.start_on_new_page
        c.product_list = customer_content_item.product_list
        c.addition_required_yn = customer_content_item.addition_required_yn
        c.rank = rank
        c.subrank = nil
        c.save

        # loop through the customer content for that production_order itself
        subrank = 1
        orderline.customer_contents.each do |customer_content|
          if customer_content.customer_targets.where(output_target: CustomerContentDefinition::OUTPUT_TARGET_RELEASE_NOTES).size > 0
            c = ContentReleaseNote.new
            c.title = customer_content.customer_targets.where(output_target: CustomerContentDefinition::OUTPUT_TARGET_RELEASE_NOTES).first.label
            customer_content.generate_if_empty
            c.content = customer_content.value
            c.product_release_note_id = rel_note_id
            c.user_id = customer_content_item.user_id
            c.status = customer_content_item.status
            c.start_on_new_page = false
            c.product_list = false
            c.addition_required_yn = false
            c.content_type = customer_content.customer_content_specification.content_type
            c.rank = rank
            c.subrank = subrank
            c.save
            subrank = subrank.next
          end
        end
        rank = rank.next

      end

    end  
      
    # Mandatory attachments
    ProductCategory.find(product_cat_id).release_note_attachments_product_categories.each do |rnapc|
      rna = ReleaseNoteAttachment.find(rnapc.release_note_attachment_id)
      rdoc = Document.new
      rdoc.document_object_id = rna.title
      rdoc.description = rna.description
      rdoc.package_directory = rna.package_directory
      note = ProductReleaseNote.find(rel_note_id)
      # only one of the next two will be set
      rdoc.production_order_id = note.production_order_id
      rdoc.production_orderline_id = note.production_orderline_id
      rdoc.mandatory_yn = rnapc.mandatory_yn
      rdoc.save      
    end
  end

  def ranking(direction="down")
    if direction == "up"
      subs = ContentReleaseNote.where("product_release_note_id = #{self.product_release_note_id} and rank = #{self.rank} and subrank is not null").order(:subrank)
      hit = ContentReleaseNote.where("product_release_note_id = #{self.product_release_note_id} and rank < #{self.rank}").order(:rank).last
      if hit
        mani = ContentReleaseNote.where("product_release_note_id = #{self.product_release_note_id} and rank = #{hit.rank}")
        mani.each do |m|
          m.rank = (self.rank)
          m.save
        end
      end
      self.rank = (self.rank - 1)
      self.save   
      subs.each do |s|
        s.rank = (self.rank)
        s.save
      end
    else
      subs = ContentReleaseNote.where("product_release_note_id = #{self.product_release_note_id} and rank = #{self.rank} and subrank is not null").order(:subrank)
      hit = ContentReleaseNote.where("product_release_note_id = #{self.product_release_note_id} and rank > #{self.rank}").order(:rank).first
      if hit
        mani = ContentReleaseNote.where("product_release_note_id = #{self.product_release_note_id} and rank = #{hit.rank}")
        mani.each do |m|
          m.rank = (self.rank)
          m.save
        end
      end
      self.rank = (self.rank)
      self.save
      subs.each do |s|
        s.rank = (self.rank)
        s.save
      end          
    end
    
    r = 0
    ContentReleaseNote.where("product_release_note_id = #{self.product_release_note_id}").order('rank, subrank asc').each do |s|
      if s.subrank.nil?
        r = r + 1
        s.rank = r 
      else
        s.rank = r
      end
      s.save  
    end    
#    if direction == "up"
#      cr = ContentReleaseNote.where("rank > #{self.rank} and product_release_note_id = #{self.product_release_note_id}").first
#      if !cr.nil?
#        new_rank = (cr.rank + 1)
#      else
#        new_rank = (self.rank + 1)
#      end
#      self.rank = new_rank
#    else
#      cr = ContentReleaseNote.where("rank < #{self.rank} and product_release_note_id = #{self.product_release_note_id}").first
#      if !cr.nil?
#        new_rank = (cr.rank - 1)
#      else
#        new_rank = (self.rank - 1)
#      end
#      self.rank = new_rank
#    end
#    self.subrank = nil
self.save
end
  
  def subranking(direction="right")
    if direction == "left"
      subs = ContentReleaseNote.where("product_release_note_id = #{self.product_release_note_id} and rank = #{self.rank} and subrank is not null").order(:subrank)
      self.subrank = nil
      hit = ContentReleaseNote.where("product_release_note_id = #{self.product_release_note_id} and rank > #{self.rank}").order('rank asc').first
      if hit
        mani = ContentReleaseNote.where("product_release_note_id = #{self.product_release_note_id} and rank = #{hit.rank}")
        mani.each do |m|
          m.rank = (self.rank)
          m.save
        end
      end
      self.rank = (self.rank + 1)
      self.save
      sr = 0
      subs.each do |s|
        s.subrank = (sr + 1)
        s.save
        sr = (sr + 1)
      end
    else
      hit = ContentReleaseNote.where("product_release_note_id = #{self.product_release_note_id} and rank < #{self.rank}").order('rank asc').last
      if hit
        self.rank = hit.rank
        subhit = ContentReleaseNote.where("product_release_note_id = #{self.product_release_note_id} and rank = #{hit.rank}").order(:subrank).last
        if subhit
          if !subhit.subrank.nil?
            self.subrank = (subhit.subrank + 1)
          else
            self.subrank = 1
          end
        end        
      end
      self.subrank = 1 if self.subrank.nil?
      self.save      
    end    
    
    
#    if direction == "left"
#      self.subrank = nil
#      cr = ContentReleaseNote.where("rank > #{self.rank} and product_release_note_id = #{self.product_release_note_id}").first
#      if !cr.nil?
#        self.rank = (cr.rank + 1)
#      else
#        self.rank = (self.rank + 1)
#      end
#    else
#      if self.subrank == nil
#       cr = ContentReleaseNote.where("rank < #{self.rank} and product_release_note_id = #{self.product_release_note_id}").last
#       if !cr.nil?
#         self.rank = cr.rank
#         if cr.subrank
#          self.subrank = (cr.subrank + 1)
#         else
#           self.subrank = 0
#         end
#       end
#      else
#        cr = ContentReleaseNote.where("rank < #{self.rank} and product_release_note_id = #{self.product_release_note_id}").last
#         if !cr.nil?
#           self.rank = cr.rank
#           csr = ContentReleaseNote.where("rank = #{self.rank} and product_release_note_id = #{self.product_release_note_id}").first
#           if !csr.nil? && csr.subrank
#             self.subrank = csr.subrank + 1
#           end
#         end
#       end
#      end
   self.save
  end
  
  def subordering(direction="down")
    
  end
  
end
