class Conversion < ActiveRecord::Base
  require 'find'
  HUMANIZED_ATTRIBUTES = {
    :data_files => "Input data",
    :conversiontool_id => 'CV-Tool',
    :cvtool_bundle_id => 'CV-Bundle',
    :region_id => 'Area'
  }
  
  acts_as_taggable
  belongs_to :supplier
  belongs_to :datasource
  belongs_to :data_release
  belongs_to :company_abbr
  belongs_to :production_orderline
  belongs_to :conversiontool
  belongs_to :cvtool_bundle
  belongs_to :region
  has_many   :product_locations
  has_many   :conversion_jobs
  has_many   :conversion_databases
  has_many   :stock
  has_many   :test_requests, :through => :conversion_databases
  has_many   :validation_jobs, :through => :conversion_databases
  has_many   :validation_errors, :through => :conversion_databases
  belongs_to :request_user, :class_name => "User", :foreign_key =>  "request_user_id"
  belongs_to :replaced_by, :class_name => "Conversion", :foreign_key => "replaced_by_id"
  belongs_to :conversion_environment
  belongs_to :assembly, :class_name => "AssemblyHeader", :foreign_key => "assembly_id"
  belongs_to :force_create, :class_name => "ProductionOrder", :foreign_key => "force_create_id"

  STATUS_ENTRY                  =   0
  STATUS_REQUESTED              =  10
  STATUS_CONVERSION             =  13
  STATUS_REQUESTED_TPD          =  15
  STATUS_FINISHED               =  20
  STATUS_CANCELLED              =  30
  STATUS_REJECTED               = 100

  @@status_array = Array.new
  @@status_array[STATUS_ENTRY]                  = 'Entry'
  @@status_array[STATUS_REQUESTED]              = 'Requested'
  @@status_array[STATUS_CONVERSION]             = 'Conversion'
  @@status_array[STATUS_REQUESTED_TPD]          = 'Requested TPD'
  @@status_array[STATUS_FINISHED]               = 'Finished'
  @@status_array[STATUS_CANCELLED]              = 'Cancelled'
  @@status_array[STATUS_REJECTED]               = 'Rejected'

  @@outcome_values = ["No result","Crashed","Unusable","Not OK","Correct", "Partly compiled"]

  scope :available, -> {where("conversions.status <> #{STATUS_CANCELLED} or conversions.production_orderline_id is not null")}
  scope :current, -> {where("(conversions.status <> #{STATUS_CANCELLED} or conversions.production_orderline_id is not null) and replaced_by_id is null")}
  scope :running, -> {where("(conversions.status = #{STATUS_CONVERSION} or (conversions.status < #{STATUS_FINISHED} and conversions.server_name is not null))")}
  scope :still_open, -> {where("conversions.status < #{STATUS_FINISHED}")}
  scope :finished_available, -> {eager_load(:conversion_databases).where("conversions.status = #{STATUS_FINISHED} AND 
                                                                         ( conversion_databases.file_system_status = #{ConversionDatabase::FILE_SYSTEM_STATUS_AVAILABLE} OR
                                                                           conversion_databases.file_system_status = #{ConversionDatabase::FILE_SYSTEM_STATUS_PERSISTENT})")}

  # the checks on production_orderline_id and product_design is to except the conversions which are part of
  # the automatic workflow as these use the conversion_job for scheduling
  scope :in_cue, -> {joins('LEFT OUTER JOIN production_orderlines on conversions.production_orderline_id = production_orderlines.id
                            LEFT OUTER JOIN production_orders on production_orderlines.production_order_id = production_orders.id
                            LEFT OUTER JOIN ordertypes on production_orders.ordertype_id = ordertypes.id').includes("conversion_databases").where("
  (conversions.status > #{STATUS_ENTRY} and conversions.status < #{STATUS_REQUESTED_TPD}) and
  (conversions.database_name is null or conversions.database_name = '') and
  (conversions.reception_reference is null or conversions.reception_reference = '') and
  (conversions.server_name is null) and
  (conversions.production_orderline_id is null or production_orderlines.product_design is null or ordertypes.job_support_yn = 0)").order('priority')}

  validates :data_release_id,:presence => true, :if => :data_needed?
  validates :region_id, :presence => true, :if => :data_needed?
  validates :conversiontool_id,  :presence => true, :if => :data_needed?
  validates :cvtool_bundle_id, :presence => true, :if => :data_needed?
  validates :input_format,:presence => true, :if => :data_needed?
  validates :output_format,:presence => true, :if => :data_needed?
  validates :data_files,:presence => true, :if => :data_needed?
  validates :result_files, :presence => true, :if => :result_needed?
  
  alias_attribute :conv_type, :type

  attr_accessor :checklog

  after_initialize :defaults

  def defaults
    if self.new_record?
      self.status ||= STATUS_ENTRY
    end
  end

  def before_save
    if (region_id)
      r = Region.find(region_id)
      self.region_name = r.name
    end

    if self.size_bytes && self.size_bytes > 0
      self.size_mb = self.size_bytes / 1024 / 1024
    end

    if (!(self.database_name && self.database_name.strip.size >= 1))
      self.conversion_date = nil
    end
  end

  def data_needed?
    !(self.job_support? || self.end_status?)
  end

  def result_needed?
    !self.job_support? && self.status == STATUS_FINISHED && self.outcome != "No result" && self.outcome != "Crashed"
  end

  def status_str
    return @@status_array[self.status || STATUS_CANCELLED]
  end

  def self.status_array
    return @@status_array
  end
  
  def self.status_hash
    # for an option list of all possible statuses
    s_hash = Hash.new
    @@status_array.each{ |s| s_hash[s] = @@status_array.index(s) unless s.nil? }
    return s_hash.sort_by { |k,v| v }
  end

  def status_hash
    # for an option list of all valid statuses for the current conversion
    s_hash = Hash.new
    @@status_array.each{ |s| s_hash[s] = @@status_array.index(s) unless (s.nil?  || !self.allow_status?(@@status_array.index(s))) }
    return s_hash.sort_by { |k,v| v }
  end

  def allow_status_change?(new_status)
    result = case status
               when STATUS_ENTRY
                 self.conversion_jobs.empty? && [STATUS_REQUESTED, STATUS_REQUESTED_TPD, STATUS_FINISHED, STATUS_CANCELLED, STATUS_REJECTED].include?(new_status)
               when STATUS_REQUESTED
                 self.conversion_jobs.empty? && [STATUS_CONVERSION, STATUS_FINISHED, STATUS_CANCELLED, STATUS_REJECTED].include?(new_status)
               when STATUS_REQUESTED_TPD
                 self.conversion_jobs.empty? && [STATUS_FINISHED, STATUS_CANCELLED, STATUS_REJECTED].include?(new_status)
               when STATUS_CONVERSION
                 [STATUS_FINISHED, STATUS_CANCELLED, STATUS_REJECTED, STATUS_REQUESTED_TPD].include?(new_status)
               when STATUS_FINISHED, STATUS_CANCELLED, STATUS_REJECTED
                 false # end status, no status change allowed
               else
                 true # undefined begin status, allow everything to get in a defined state
             end
    return result
  end

  def allow_status?(new_status)
    self.status == new_status ||allow_status_change?(new_status)
  end

  def set_status(new_status)
    result = allow_status_change?(new_status)
    self.status = new_status if result
  end

  def end_status?
    if self.status.nil? # old garbage records
      return true
    else
      return self.status >= STATUS_FINISHED
    end
  end

  def expired?
    return status == STATUS_CANCELLED || status == STATUS_REJECTED || (self.production_orderline && self.production_orderline.expired?)
  end

  def running?
    return status == STATUS_CONVERSION
  end

  def outcome_removed?
    return end_status? && self.conversion_databases.available.empty?
  end

  def self.outcome_select_hash
    outcome_options = {}
      @@outcome_values.collect{ |h| outcome_options[h] = h }
    return outcome_options
  end

  def default_selection_outcome
    if [STATUS_REQUESTED_TPD].include?(self.status) || !self.conversion_databases.available.empty?
      return @@outcome_values[4]
    else
      return @@outcome_values[0]
    end
  end

  def checklog
    @checklog || @checklog = Checklog.new(only_check_yn = false, Checklog::LOGLEVEL_DEBUG)
  end

  def create_reception(endproduct)
    r = Reception.new
    r.product_id = self.productid

    parents = {}
    self.source_receptions.each do |reception|
      if !parents[reception.id]
        pr = ParentReception.new
        pr.parent_reception_id =  reception.id
        pr.type = "Child"
        r.parent_receptions << pr
        parents[reception.id] = "IN"
      end
    end

    r.reception_date  = Time.now
    # r.priority = "NORMAL"
    dm = DataMedium.find_by_name("LOCAL")
    r.data_medium_id  =  dm.id if dm

    if endproduct
      ca  = CompanyAbbr.find_by_company_name("Mapscape")
      r.datasource_id = ca.id if ca
    else
      r.datasource_id = self.datasource_id
    end

    if endproduct
      if self.productid && self.productid.scan(/(^MP)/i)[0] #Mapscape Product-id prefix
        dr = DataRelease.where(datasource_id: r.datasource_id).first
        r.data_release_id =  dr.id if dr

        autorelease = self.productid
        if  autorelease.scan(/(\d{2})(\d)/)[0]
          match = autorelease.scan(/(\d{2})(\d)/)[0]
          release_name = "20" + match[0].to_s + "Q" + match[1].to_s
          ca  = CompanyAbbr.find_by_company_name("Mapscape")


          dr = DataRelease.where("company_abbr_id = #{ca.id.to_s} and name = '#{release_name}'").first
          if dr
            r.data_release_id = dr.id
          else
            ca  = CompanyAbbr.find_by_company_name("Mapscape")
            dr = DataRelease.new
            dr.datasource_id = ca.id
            dr.company_abbr_id = ca.id
            dr.name = release_name
            dr.save!
            r.data_release_id = dr.id
          end
        end
      else
        r.data_release_id =  self.data_release_id
      end
    else
      r.data_release_id =  self.data_release_id
    end
    r.conversiontool = self.conversion_tool_release

    if self.purpose == 'Production'
      r.purpose = "production"
    else
      r.purpose = "Test"
    end

    r.storage_location = self.result_files
    r.area_id = self.region_id
    dt = DataType.find_by_name(self.output_format)
    r.data_type_id = dt.id
    r.status = "INPROGRESS"
    r.data_intake_requested = 0
    r.validation_requested = 0
    r.remarks = self.remarks
    return r
  end

  def deliver_next_mail
    case status
      when STATUS_ENTRY
        Postoffice.conversion_request(self.id).deliver
        set_status(STATUS_REQUESTED)
      when STATUS_REQUESTED, STATUS_CONVERSION
        if self.tpd
          Postoffice.tpd_request(self.id).deliver
          set_status(STATUS_REQUESTED_TPD)
        else
          set_status(STATUS_FINISHED)
        end
      when STATUS_REQUESTED_TPD
        Postoffice.tpd_result(self.id).deliver
        set_status(STATUS_FINISHED)
      else
        # do nothing
    end
    save!
  end

  def source_receptions
    reception_array = []
    ref_nrs = reference_numbers
    if ref_nrs
      ref_array = ref_nrs.split("\r")
      ref_array.each do |ref|
        ref = ref.strip.chomp
        sourcereception = Reception.find_by_referenceid(ref)
        if sourcereception
          reception_array << sourcereception
        end
      end
    end
    return reception_array
  end
  
  def define_input_source(references,data, purpose)
    # Define source of inputdata Conversion || Reception || ProcessData
    dataline_size = data.split("\r\n").size
    if references.size > dataline_size
      raise "Number of reference lines (#{references.size.to_s}) should at least be equal to number of input data lines (#{dataline_size.to_s})"
    end

    @refs = references
    conv_match = false
    found_but_removed = false
    rcpt_match = false
    proc_match = false
    data.split("\r\n").each do |dfile|
      found_but_removed = false
      dfile = dfile.gsub(/(\#.*)/,'').gsub(/(^[0-9]* )/,'').gsub(/ .*$/,'')
      next if dfile.size == 0
      conv_match = false
      rcpt_match = false
      proc_match = false
      @refs.each do |ref|
        idx = references.index("#{ref}")
        refclean =  ref.scan(/^(conversion-id-|reception-id-|data_set-id-|UNKNOWN-ID-)(.*)/) # else already known
        if refclean[0] && refclean[0][1]
          ref = refclean[0][1]
        end
        # Look in conversion databases
        @match = ConversionDatabase.where(:conversion_id => ref.strip.to_i)
          @match.each do |f|
          if !dfile.to_s.strip.scan("#{f.path_and_file.to_s.strip}").empty?
            @refs[idx] = "conversion-id-" + ref.to_s
            conv_match = true
            found_but_removed = f.unavailable?
            break unless found_but_removed
          end
        end

        # Look in receptions
        if (!conv_match) || found_but_removed
          if @match = Reception.where(:id => ref.strip.to_i, :status => 'COMPLETED').first
            if @match && @match.storage_location
              if @match.storage_location.to_s.strip.match(dfile)
                @refs[idx] = "reception-id-"  + ref.to_s
                rcpt_match = true
                found_but_removed = !@match.available?
              end
            end
          end
        end

        # Look in DataSet
        if !conv_match && !rcpt_match
          if @match = DataSet.where(:id => ref.strip.to_i).first
            if @match && !@match.process_data_elements.active.where(:directory => dfile.to_s.strip).empty?
              @refs[idx] = "data_set-id-"  + ref.to_s
              proc_match = true
              found_but_removed = (@match.status == DataSet::STATUS_REMOVED && !@match.process_data_elements.where(:directory => dfile.to_s.strip).last.available?)
            end
          end
        end
      end
      # No match found
      if !conv_match && !rcpt_match && !proc_match && purpose.to_s == "Production"
        raise "Input file #{dfile.to_s} is not found with provided reference data, please check reference data and input data"
      elsif found_but_removed && self.status <= STATUS_REQUESTED
        raise "Input file #{dfile.to_s} is found with provided reference data but it was REMOVED, please check reference data and input data"
      end
    end
    
    @refs.each do |ref_full|
      if !ref_full.match(/reception-|^conversion-|^data_set-/)
        raise "Reference data #{ref_full} was specified by no related input data was found for it"
      end
    end
    return @refs
  end

  def output_data
    return self.conversion_databases.collect{|x| x.path_and_file}
  end

  def unknown_input_data(input_data = [])
    # return those input lines which cannot be matched to (a subdirectory or file of) input_data
    # also remove input lines beginning with '#' by adding that to the input_data
    input_data << '#'
    return self.data_files.split(/\r?\n/).reject{|line| index = input_data.index{|i| line.sub(/^\d\d?\s+/,'').start_with?(i) || i.start_with?(line.sub(/^\d\d?\s+/,''))}}
  end

  def input_conversion_data(conversion_map = Hash.new)
    # conversion_map contains the conversion mapping which will be shown
    # in the conversion report. In that conversions are grouped by their key
    # so each key points to a set of conversions which all have that input and
    # output format.
    key = "#{self.input_format}|#{self.output_format}"
    # conversion_data contains all data to be shown in the conversion report 
    # about this particular conversion
    conversion_data = 
      {
      :self => self,
      :conversions => [],
      :data_set => [],
      :receptions => [],
      :unknown => [],
      :unknown_input_data => []
    }
    conversion_map[key] = Hash.new unless conversion_map[key]
    # input_data are all databases which are the output of the used
    # conversions and conversion_databases and all input files of the
    # used receptions and process data. It represents all paths made
    # available as input by the used conversions, conversion databases,
    # process data and receptions.
    input_data = []

    self.reference_numbers.to_s.split("\n").each do |line|
      # reference_numbers contains one reference per line
      # for each line try to determine what type of reference it is
      if id = /conversion-id-(?<id>\d+)/.match(line) {|data| data[:id].to_i}
        if conv = Conversion.find(id)
          conversion_data[:conversions] << conv
          conv.input_conversion_data.each do |k,v|
            if conversion_map[k]
              conversion_map[k].merge!(v)
            else
              conversion_map[k] = v
            end
            input_data += conv.output_data
          end
        else
          conversion_data[:unknown] << "Conversion '#{id}'c not found"
        end
      elsif id = /data_set-id-(?<id>\d+)/.match(line) {|data| data[:id].to_i}
        if pd = DataSet.find(id)
          conversion_data[:data_set] << pd
          input_data << pd.generate_directory
        else
          conversion_data[:unknown] << "Data set '#{id}' not found"
        end
      elsif id = /reception-id-(?<id>\d+)/.match(line) {|data| data[:id].to_i}
        if rec = Reception.find(id)
          conversion_data[:receptions] << rec
          input_data += rec.storage_location.split(/\r?\n/) if rec.storage_location
        else
          conversion_data[:unknown] << "Reception '#{id}' not found"
        end
      elsif id = /conversion_database-id-(?<id>\d+)/.match(line) {|data| data[:id].to_i}
        cd = ConversionDatabase.find(id)
        if cd && cd.conversion
          # if a conversion database is specified use the conversion which generated it
          # for the report, but only take this conversion database as available output
          conversion_data[:conversions] << cd.conversion
          cd.conversion.input_conversion_data.each do |k,v|
            if conversion_map[k]
              conversion_map[k].merge!(v)
            else
              conversion_map[k] = v
            end
          end
          input_data << cd.path_and_file
        elsif cd
          conversion_data[:unknown] << "Conv.db. '#{id}' has no conversion"
        else
          conversion_data[:unknown] << "Conv.db. '#{id}' not found"
        end
      else
        conversion_data[:unknown] << line.strip
      end
    end
    conversion_data[:conversions].sort_by!{|x| x.id}.uniq!
    conversion_data[:data_set].sort_by!{|x| x.id}
    conversion_data[:receptions].sort_by!{|x| x.id}
    conversion_data[:unknown].sort!
    conversion_data[:unknown_input_data] = unknown_input_data(input_data.uniq)

    conversion_map[key][self.id] = conversion_data

    return conversion_map
  end

  def input_report_data
    data_set = []
    receptions = []
    unknown = []
    # reverse the ordered list of hash keys returned by self.input_conversion_data
    # so we get the first conversions in front
    conversion_data = Hash[self.input_conversion_data.to_a.reverse]

    # aggregate all inputs so we can show them together
    conversion_data.values.each do |value|
      value.each do |conversion_id, data|
        data_set += data[:data_set]
        receptions += data[:receptions]
        unknown += data[:unknown]
      end
    end

    result = {
      :conversion_data => conversion_data,
      :data_set => data_set.uniq.sort_by{|x| x.id},
      :receptions => receptions.uniq.sort_by{|x| x.id},
      :unknown => unknown.uniq.sort
    }
    return result
  end

  def allocate_bundle
    # TODO: remove the method allocate_bundle and the Business Process 'ALLOCATE BUNDLE'
    # dummy method. The bundle is already assigned but this method is still in the defined work flow
    # once the allocate bundle business process is no longer in a work flow and has been removed itself
    # this method can be removed.
    true
  end
    
  def select_datarelease
    return if self.data_release_id    # data release already assigned
    # identify release from tags - if not available -> NIL
    design = JSON.parse(self.production_orderline.product_design)
    step = design["conversion_steps"][self.design_sequence]
    # assume intermediate database are logged with same supplier
    if design["input_datatypes"][0]["supplier"]
    else
      if design["conversion_steps"][self.design_sequence]["connections"][0]["exclude"]
        raise "Required data package excluded by user, can't create a conversion."
      else
        raise "Can't find a supplier in conversion step for conversion #{self.id}."
      end
    end
    
    supplier = design["input_datatypes"][0]["supplier"]
    cid = CompanyAbbr.find_by_ms_abbr_3(supplier).id

    releases = DataRelease.where(company_abbr_id:cid)
    all_releases_found = true
    step["input"].each do |input|
      # check if release_id was already assigned
      next if input["data_release_id"]
      
      release_found = false
      tags = []
      if input["tags"]
        tags = input["tags"]
      else
        input["tags"] = []
      end
        
      if tags
          
        if tags.size == 0 
          # fallback to global tags
          design["input_datatypes"].each do |inputtype|
            if inputtype["type"] == input["format"]
              if inputtype["tags"]
               
                tags = inputtype["tags"]
                break
              end
            end
          end
        end
        releases.each do |release|
          if tags.index(release.name)
            
            self.data_release_id = release.id if input == step["input"][0]
            input["supplier"] = supplier
            input["data_release_name"] = release.name
            input["data_release_id"] = release.id
            input["tags"] = input["tags"] + tags
            input["tags"].uniq!
            self.data_release_id = release.id
            release_found = true
            break # assume first is correct
          end 
        end
        all_releases_found = all_releases_found && release_found
      else
        all_releases_found = false
      end
      if all_releases_found
       
        line = self.production_orderline
        line.product_design = design.to_json
        line.save!
        save!
        return true
      else
        raise "NOT ALL REQUIRED DATARELEASES WERE FOUND (tags: #{tags.inspect})"
      end
    end
  end

  def allocate_data
    branch = self.conversiontool.get_branch
    pol = self.production_orderline
    result = ProductDesign.allocate_files(JSON.parse(pol.product_design), {:strategy => 'best match', :force_create_id => force_create_id}, self.design_sequence, pol, branch)
    
    pol.product_design = result[0].to_json
    pol.save!
  end

  def add_metadata
    fulldesign = JSON.parse(self.production_orderline.product_design)
    stepdesign = fulldesign["conversion_steps"][self.design_sequence]
    
    exe = Executable.find_by_name(stepdesign["name"])
    add_metadata_by_exe(exe,fulldesign) if exe.metalogic.to_s.size > 1

    if stepdesign["pre_improvement_steps"]
      stepdesign["pre_improvement_steps"].each do |exehash|
        exe = Executable.find_by_name(exehash["name"])
        add_metadata_by_exe(exe, fulldesign) if exe.metalogic.to_s.size > 1
      end
    end

    if stepdesign["post_improvement_steps"]
      stepdesign["post_improvement_steps"].each do |exehash|
        exe = Executable.find_by_name(exehash["name"])
        add_metadata_by_exe(exe, fulldesign) if exe.metalogic.to_s.size > 1
      end
    end

    ProductionOrderline.transaction do
      self.production_orderline.product_design = fulldesign.to_json
      self.production_orderline.save!
      resources = self.allocated_files_tagged
      self.data_files = resources[0].join("\r\n")
      self.reference_numbers = resources[1].join("\r\n")
      save!
    end
    self.checklog.add_debug_line("Metadata added successfully.")
  end
    
  def add_metadata_by_exe(exe,design)
    # pickup tagging logic
    @merge = false
    @merge = true if self.design_json["name"] == "DHMerge"

    if exe.metalogic.to_s.size > 1  
      resources = design["conversion_steps"][self.design_sequence]["connections"]
      resources.each do |input|
        next if input["exclude"]
        input["source_spec"]["datasets"].each do |dataset|
          if dataset["file_resources"]
            @resourcearray = dataset["file_resources"]
            @production_orderline = self.production_orderline
            @input = input
            @unknown_region_codes = []
          
            if exe.metalogic.size > 0
              begin
                eval(exe.metalogic)
              rescue SyntaxError => e
                raise "Syntax error in Tagging Logic for " + exe.name + " "  + e.message
              end
            else
              raise "NO TAGGING LOGIC FOUND"
            end

            dataset["file_resources"].delete_if {|file| file["tags"] && file["tags"].index("DELETE")}
            dataset["file_resources"] = @resourcearray
            if !@unknown_region_codes.empty?
              self.checklog.add_info_line("Found regioncodes in source data which are not known by the system: #{@unknown_region_codes.join(", ")}")
            end
          end
        end
      end
    else
      puts "No metalogic found for : " + exe.name.to_s
    end
  end
             
  def tag
    result= @file
    result["tags"]  = [] 
    if @file["file_path"].match(/xml$/)
      if result["file_path"].match(/.*reference_frame.*xml$/)
        result["tags"] << "mich_ref"
      else
        result["tags"] = [] if !result["tags"]
        result["tags"] << "michelin"
      end
    else
      result["tags"] << "DELETE"
    end
    
    result
  end

  def create_assemblies_from_design
    groups = group_resources
    groupkeys = groups.keys
    count = 0
    AssemblyHeader.transaction do
      groupkeys.each do |key|
        if self.productid
          name =  self.productid
        else
          name = self.production_orderline.production_order.name
        end
      
        if groups.size > 1
          name =  name + " " + key.join(" ") if key != ["OTHER"]
          header = AssemblyHeader.find_or_create_by_name(name)
        else
          header = AssemblyHeader.find_or_create_by_product_id(name)
          header.product_id = self.productid if self.product_id
        end
      
        header.output_format = self.output_format
        header.name = name
        header.assembly_lines.destroy_all
        groups[key].each do |file|
          line = AssemblyLine.new
          line.file_name = file["file_path"]
          line.file_name = line.file_name + " " + file["tags"].join(" ") if file["tags"]
          if file["conversion_database_id"]
            line.reference_id = file["conversion_database_id"]
            cd = ConversionDatabase.find(file["conversion_database_id"])
            #main supplier
            release = DataRelease.find_by_tagmodel(cd.tagmodel)
            line.data_release_id = release.id
            line.company_abbr_id = release.company_abbr_id
            line.data_release_name = release.name
          end
          if file["process_data_element_id"]
            line.reference_id = file["process_data_element_id"]
            elem = ProcessDataElement.find(file["process_data_element_id"])
            release = DataRelease.find_by_supplier_and_release_name(elem.data_set.company_abbr.company_name, elem.data_set.data_release.name)
            line.data_release_id = release.id
            line.data_release_name = elem.data_set.data_release.name
          end
          header.assembly_lines << line
        end
        header.save!
        count = count + 1
      end
    end
    return count
  end
    
  def allocated_files_tagged
    files = []
    ids = []
    step = JSON.parse(self.production_orderline.product_design)["conversion_steps"][self.design_sequence]
    step["connections"].each do |input|
      next if input["exclude"]
      input["source_spec"]["datasets"].each do |dataset|
        dataset["file_resources"].each do |file|
          filestring  = file["file_path"]
          filestring += ' ' + file['tags'].join(' ') if file['tags']
          files << filestring
          if file["conversion_database_id"]
            ids << "conversion_database-id-" + file["conversion_database_id"].to_s
          end
          if file["process_data_element_id"]
            ds = ProcessDataElement.find(file["process_data_element_id"]).data_set
            ids << "data_set-id-" + ds.id.to_s
          end
        end
      end
    end
    if ids.uniq.size == 0 && files.size > 0 
      raise "No Id's found for files"
    end
    return [files,ids.uniq]
  end

  def design_parameterset_id
    parameter_setting_id = nil
    if self.production_orderline && self.production_orderline.product_design && self.production_orderline.production_order.ordertype && self.production_orderline.production_order.ordertype.job_support_yn
      designhash = JSON.parse(self.production_orderline.product_design)
    end

    pd = ProductDesign.find_by_name(designhash["name"])
    parameter_setting_id = pd.parameter_setting_id if pd

    return parameter_setting_id
  end
  
  def design_json
    if self.production_orderline && self.production_orderline.product_design && self.production_orderline.production_order.ordertype && self.production_orderline.production_order.ordertype.job_support_yn
      JSON.parse(self.production_orderline.product_design)["conversion_steps"][self.design_sequence]
    else
      return Hash.new
    end
  end
  
  def product_line
    p = JSON.parse(self.production_orderline.product_design)["product_line"]
    raise "Product line NOT FOUND" if !p
    return p
  end
  
  def executable
    exe = design_json["name"]
    exeobj = Executable.find_by_name(exe)
    return exeobj
  end

  def is_a_number?(s)
    s.to_s.match(/\A[+-]?\d+?(\.\d+)?\Z/) == nil ? false : true
  end

  def parameter_setting_to_hash(params_pf, parameter_setting, supported_parameter_names, supported_parameter_names_in_bundle, product_line)
    use_test_versions_yn = self.production_orderline.production_order.use_test_versions_yn
    return params_pf if !parameter_setting
    if parameter_setting.parent_id
      ps = ParameterSetting.find_correct_version(parameter_setting.parent_id, use_test_versions_yn, self.checklog)
      params_pf = parameter_setting_to_hash(params_pf, ps, supported_parameter_names, supported_parameter_names_in_bundle, pl)
    end
    
    ps = ParameterSetting.find_correct_version(parameter_setting.id, use_test_versions_yn, self.checklog)
    params = ps ? JSON.parse(ps.parameterset) : []
    params.each do |param|
      if supported_parameter_names_in_bundle.include?(param["name"])
        params_pf[param["name"]] = param["value"]
      elsif supported_parameter_names.include?(param["name"])
        self.checklog.add_info_line("Parameter setting '#{param['name']}' is defined for one of the executables, but this definition is not in the selected bundle.")
        # self.checklog.add_error_line("Parameter setting '#{param[:name]}' is defined for one of the
        # executables, but this definition is not in the selected bundle, choose another one.")
        # raise "Parameter setting not in this bundle" unless use_test_versions_yn
        # use_test_versions_yn is true, so continue and include this parameter anyway
        params_pf[param["name"]] = param["value"]
      end
    end

    return params_pf
  end

  def parameters
    params_pf = Hash.new
    use_test_versions_yn = self.production_orderline.production_order.use_test_versions_yn

    supported_parameter_names = []
    supported_parameter_names_in_bundle = []

    # find the list of executables included in this conversion
    exemain = design_json["name"]
    exeobj = executable
    if exeobj.subprocs && exeobj.subprocs.size > 0
      exemain = exeobj.subprocs
    end

    executables = [exeobj]
    if pre_steps = design_json['pre_improvement_steps']
      executables = executables + pre_steps.collect{|e|Executable.find_by_name(e['name'])}
    end
    if post_steps = design_json['post_improvement_steps']
      executables = executables + post_steps.collect{|e|Executable.find_by_name(e['name'])}
    end

    # find the parameter names supported by the executables in this conversion
    executables.each do |exe|
      # customer content parameters currently overwrite the same parameters from the parameter_setting.
      supported_parameter_names += exe.param_names
      supported_parameter_names_in_bundle += param_names_in_bundle(exe, use_test_versions_yn)
    end
    
    # find the parameters in the parameter_setting of the platform which must be included in this conversion
    if design_json['dedicated_parameterset_id']
      self.checklog.add_info_line("Using dedicated parameterset #{design_json['dedicated_parameterset_id']}")
      psdedicated = ParameterSetting.find(design_json['dedicated_parameterset_id'])
      if !psdedicated
        raise "Dedicated parameterset not found : #{design_json['dedicated_parameterset_id']}"
      end
      ps = ParameterSetting.find_correct_version(psdedicated.id, use_test_versions_yn, self.checklog)
    else
      param_id = design_parameterset_id
      if param_id 
        ps = ParameterSetting.find_correct_version(param_id, use_test_versions_yn, self.checklog)
      end
    end
    if ps    
      params_pf = parameter_setting_to_hash(params_pf, ps, supported_parameter_names, supported_parameter_names_in_bundle, pl)
    end
    # find the feature specific parameters which must be included in this conversion
    design_line = JSON.parse(self.production_orderline.product_design)
    features = []
    features = features + design_line["features"] if design_line["features"]

    
    if design_json["used_product_design_md5"] && !design_json['dedicated_parameterset_id']
      # imported design!
      json_imported_design_revision = JSON.parse(ProductDesignVersion.where("product_design_md5 = '#{design_json["used_product_design_md5"]}'")[-1].model)
      pl_imported = ProductLine.find(json_imported_design_revision["product_line"]["id"])
      if pl_imported && pl_imported.parameter_setting(self.production_orderline.production_order.project_id)
        ps = ParameterSetting.find_correct_version(pl_imported.parameter_setting(self.production_orderline.production_order.project_id).id, use_test_versions_yn, self.checklog)
        params_pf = parameter_setting_to_hash(params_pf, ps, supported_parameter_names, supported_parameter_names_in_bundle,pl_imported)
      end
      if json_imported_design_revision["features"] && json_imported_design_revision["features"].size > 0
        # add features (and with that parameters of the imported design)
        features = features + json_imported_design_revision["features"]
      end
    end
    

    if features && !design_json['dedicated_parameterset_id']
      features.each do |f|
        feature = ProductFeature.find_correct_version_by_name(f["name"], use_test_versions_yn, self.checklog)
        trans = feature.system_translation if feature
        
        if !trans or trans.to_s.size < 1
          next
        end
        translation = JSON.parse(trans) if trans
        if translation && translation["parts"]
          translation["parts"].each do |part|
            if part["type"] == "PARAMETER"
              if supported_parameter_names_in_bundle.include?(part["parameter_name"])
                params_pf[part["parameter_name"]] = part["parameter_value"]
              elsif supported_parameter_names.include?(part["parameter_name"])
                # self.checklog.add_error_line("Parameter '#{part["parameter_name"]}' of feature
                # '#{f["name"]}' is defined for one of the executables, but this definition is not in the
                # selected bundle, choose another one.")
                self.checklog.add_info_line("Parameter '#{part["parameter_name"]}' of feature '#{f["name"]}' is defined for one of the executables, but this definition is not in the selected bundle.")
                # raise "Product feature parameter not in this bundle" unless use_test_versions_yn
                params_pf[part["parameter_name"]] = part["parameter_value"]
              end
            end
          end
        end
      end
    end



    # find the parameters defined in the customer_content which must be included in this conversion
    self.production_orderline.customer_contents.each do |customer_content|
      # only take the customer_content parameters into account with output_target
      # CustomerContentDefinition::OUTPUT_TARGET_CONVERSION_DB
      targets = customer_content.customer_targets.where(:output_target => CustomerContentDefinition::OUTPUT_TARGET_CONVERSION_DB)
      if targets.size > 0
        target_name = targets.last.label

        # only include the parameter in this script if the executable supports it.
        if supported_parameter_names_in_bundle.include?(target_name)
          # try to fill the customer_content value if it has not been set yet
          customer_content.generate_if_empty

          # only include the customer_content if it has a value now
          params_pf[target_name] = customer_content.value_string unless customer_content.value.to_s.blank?
        elsif supported_parameter_names.include?(target_name)
          # self.checklog.add_error_line("Customer content '#{target_name}' is defined for one of the
          # executables, but this definition is not in the selected bundle, choose another one.")
          self.checklog.add_info_line("Customer content '#{target_name}' is defined for one of the executables, but this definition is not in the selected bundle.")
          # raise "Customer content not in this bundle" unless self.use_test_versions_yn

          customer_content.generate_if_empty
          params_pf[target_name] = customer_content.value_string unless customer_content.value.to_s.blank?
        end
      end
    end

    return params_pf
  end

  def param_names_in_bundle(exe, use_test_versions_yn = false)
    names = []
    bundle_content = BundleContent.where({:conversiontool_id => self.conversiontool_id, :cvtool_bundle_id => self.cvtool_bundle_id, :executable_id => exe.id}).last
    if bundle_content
      if bundle_content.parameters && bundle_content.parameters.size > 3
        paramhash = JSON.parse(bundle_content.parameters)
        paramlist = paramhash.instance_of?(Array) ? paramhash : paramhash["parameters"]
        names = paramlist.map{ |p| p["name"] }
      end
    elsif BundleContent.where({:conversiontool_id => self.conversiontool_id, :cvtool_bundle_id => self.cvtool_bundle_id}).empty?
      self.checklog.add_info_line("No toolspec imported for '#{self.conversiontool.code if self.conversiontool}/#{self.cvtool_bundle.name if self.cvtool_bundle}'.")
      # self.checklog.add_error_line("No toolspec imported for '#{self.conversiontool.code if
      # self.conversiontool}/#{self.cvtool_bundle.name if self.cvtool_bundle}', please import the toolspec, or
      # choose another bundle.") raise "Toolspec not imported." unless use_test_versions_yn
    else
      self.checklog.add_info_line("Executable '#{exe.name}' not found in the imported toolspec for '#{self.conversiontool.code if self.conversiontool}/#{self.cvtool_bundle.name if self.cvtool_bundle}'.")
      # self.checklog.add_error_line("Executable '#{exe.name}' not found in the imported toolspec for
      # '#{self.conversiontool.code if self.conversiontool}/#{self.cvtool_bundle.name if self.cvtool_bundle}',
      # please choose a bundle that does include this executable.") raise "Executable toolspec not imported."
      # unless use_test_versions_yn
    end
    return names
  end

  def create_pml_file(data_set_pml=nil)
    if production_orderline.product
      pml= production_orderline.product.poi_mapping_layout
    else
      pml= data_set_pml
    end
    if !pml
      raise "No Poi Mapping File found, check product configuration"
    end
    basedir = File.join(ConfigParameter.get('default_WebMisScripts_dir'), 'PML')
    Server.ssh_localhost("mkdir -p #{basedir}") unless Dir.exists?(basedir)
    poi_map_location = pml.create_file(basedir)

    return poi_map_location
  end

  def dakota_header(sourcetags)
    used_params = []
    used_param_values = []
    headersizefilesize = 0
    # get relevant Product line parameters
    exemain = design_json["name"]
    exeobj = executable
    
    if exeobj.subprocs && exeobj.subprocs.size > 0
      exemain = exeobj.subprocs
    end
    params_pf = parameters

    dakscript = JobScript.new
    dakscript.set_type(JobScript::TYPE_DAKOTA)
    dakscript.production_orderline_id = self.production_orderline_id.to_s
    dakscript.product_id = self.production_orderline.product_id.to_s if self.production_orderline
    dakscript.conversion_id = self.id

    param_text = "parameter"

    params_pf.each do |name, value|
      next if ( used_param_values.index([name,value]) || value.to_s.blank? ) # Unset parameter result is superfluous param+
      if used_params.index(name)
        param_text = "param+"
      else
        param_text = "parameter"
      end
      used_params << name

      if is_a_number?(value.to_s)
        dakscript.parameters << [param_text, name.to_s, value.to_s]
      else
        dakscript.parameters << [param_text, name.to_s,"\"#{value.to_s}\""]
      end
      used_param_values << [name, value]

    end
    poi_file = nil
    haspoimapper = false
    design_json["connections"].each do |input|
      next if input["exclude"]
      if input["target_spec"]["allocate"]
        # must be parameter sidefile
        input["source_spec"]["datasets"].each do |dataset|
          if dataset["file_resources"]
            dataset["file_resources"].each do |file|
              next if ( used_param_values.index([input["target_spec"]["name"],file["file_path"]]) || file["file_path"].to_s.blank? )

              if file["process_data_element_id"]
                data_set = ProcessDataElement.find(file["process_data_element_id"]).data_set
                sourcetags << data_set.sourcetag
              end

              if used_params.index(input["target_spec"]["name"])
                param_text = "param+"
              else
                param_text = "parameter"
              end

              used_params << input["target_spec"]["name"]
              dakscript.parameters << [param_text, input["target_spec"]["name"],  ' "' + file["file_path"] + '"']
              used_param_values << [input["target_spec"]["name"], file["file_path"]]
              headersizefilesize = headersizefilesize + File.size(file["file_path"]) if File.exists?(file["file_path"])
            end
          end
        end
      end
    end


    if design_json["pre_improvement_steps"]
      design_json["pre_improvement_steps"].each do |pstep|
        exe = pstep["name"]
        if exe == "DHPOIMap"
          haspoimapper = true
        end

        dakscript.pre_improvement_steps << exe.to_s
      end
    end

    dakscript.main_executable = exemain
    if exemain == "DHPOIMap"
      haspoimapper = true
    end

    if design_json["post_improvement_steps"]
      design_json["post_improvement_steps"].each do |pstep|
        exe = pstep["name"]
        if exe == "DHPOIMap"
          haspoimapper = true
        end
        dakscript.post_improvement_steps << exe.to_s
      end
    end

    # only initialize number_of_update_regions if we have an executable which makes use of NDS_NUMPROCS (only 'NDH2NDS' at the moment of writing)
    if exemain == "NDH2NDS" || dakscript.pre_improvement_steps.include?("NDH2NDS") || dakscript.post_improvement_steps.include?("NDH2NDS")
      dakscript.number_of_update_regions = 0
    else
      dakscript.number_of_update_regions = nil
    end

    if haspoimapper
      # find attached poi_mapping_layout via file_resources
      data_set_pml = nil
      design_json["connections"].each do |connection|
        connection["source_spec"]["datasets"].each do |dataset|
          dataset["file_resources"].each do |file|
            next if !file["process_data_element_id"]
            data_set = ProcessDataElement.find(file["process_data_element_id"]).data_set
            data_set_pml = data_set.poi_mapping_layout if data_set && data_set.poi_mapping_layout
          end
        end
      end
      poi_file = create_pml_file(data_set_pml)
      dakscript.parameters << ["parameter", "POI_mapping_layout_file", "\"#{poi_file}\""]
    end
    
    seq = 1
    
    dakscript.conversiontool_code = self.conversiontool.code.to_s
    dakscript.cvtool_bundle_name = self.cvtool_bundle.name.to_s
    #  dakstring = dakstring  + self.data_files.map{|x| "#{{seq} #{x}"}
    
    return [dakscript, headersizefilesize]
  end

  def source_conversion_databases
    dbs = []
    if design_json && design_json["connections"]
      design_json["connections"].each do |connection|
        connection["source_spec"]["datasets"].each do |dataset|
          dataset["file_resources"].uniq.each do |file|
            if file["conversion_database_id"]
              dbs << file["conversion_database_id"]
            end
          end
        end
      end
    else
      # use manual stuff and try to parse
      self.data_files.split("\r\n").each do |file|
        clean_file = file.split(" ")[0]
        cd = ConversionDatabase.where(:path_and_file => clean_file).first
        if cd
          dbs << cd.id
        end
      end
    end

    return ConversionDatabase.find(dbs)
  end
  
  def group_resources
    file_resources = {}
    file_for_all_regions = []
    design_json["connections"].each do |connection|
      next if connection["target_spec"]["allocate"] # header parameter input
      connection["source_spec"]["datasets"].each do |dataset|
        dataset["file_resources"].uniq.each do |file|
          raise "File not found ::" + file["file_path"] if !File.exists?(file["file_path"])
          file["size"] = File.size(file["file_path"])
          if file['include_in_all_regions']
            file['include_in_all_regions'] = false
            # file_for_all_regions[["ALL_REGIONS"]] = [] if !file_for_all_regions[["ALL_REGIONS"]]
            file_for_all_regions << file
          end
        end
      end
    end

    
    design_json["connections"].each do |connection|
      next if connection["target_spec"]["allocate"] # header parameter input
      connection["source_spec"]["datasets"].each do |dataset|
        dataset["file_resources"].uniq.each do |file|
          next if file['include_in_all_regions']
          if dataset["coverage_required"] && dataset["coverage_required"].size > 0
            ignore_check = Executable.find_by_name(design_json["name"]).ignore_ds_regions_yn
            some_coverage_in_file_needed = false
            if !ignore_check
              file["coverage"].each do |coverage|
                if dataset["coverage_required"].index(coverage)
                  some_coverage_in_file_needed = true
                end
              end
              if !some_coverage_in_file_needed
                self.checklog.add_debug_line("coverage not needed - skipping #{file["coverage"]}/ #{dataset['coverage_required']}", true)
                next
              end
            end
          end
          raise "File not found ::" + file["file_path"] if !File.exists?(file["file_path"])
          file["size"] = File.size(file["file_path"])

          if file["group_by"]
            idtags = []
                    
            file["group_by"].each do |groups|
              file[groups].each do |group|
                idtags <<  group
              end
            end
            file_resources[idtags] = [] if !file_resources[idtags]
            file_resources[idtags] << file
            file_for_all_regions.uniq.each do |f|
              file_resources[idtags] << f
            end
          else
            file_resources[["OTHER"]] = [] if !file_resources[["OTHER"]]
            file_resources[["OTHER"]] << file
          end
        end
      end
    end
    
    return file_resources
  end

  def dakota_scripts
    dakota_scripts = []
    sourcetags = []
    file_ds_region = {}
    h = dakota_header(sourcetags)
    header = h[0]
    headerfilesize = h[1]
    groups = group_resources
    groups.each do |group, files|
      dakscript = header.dup
      indirect_process_data_elements = []
      sourcetags = []
      process_data_elements = []
      data_set_ids = []
      # count update regions/tags
      utags = []
      files.each{|x| utags << x["tags"].to_s}
      num_updateregions = utags.uniq.size + 1

      # only set number_of_update_regions if the attribute has been initialized
      # (as it should only be used if it has been initialized and ignored otherwise)
      dakscript.number_of_update_regions = num_updateregions if dakscript.number_of_update_regions
      seq = 1
      coverage = []
      subregions = []
      totalsize = headerfilesize
      files.sort{|x,y| y["file_path"] <=> x["file_path"] }.uniq.each do |file|
        totalsize = totalsize + file["size"]
        tag_str_arr = []
        if file["tags"]
          file["tags"].each do |tag|
            if tag.match(/\s/)
              tag_str_arr << "\"#{tag}\""
            else
              tag_str_arr << tag
            end
          end
        end
        dakscript.data << [seq.to_s, file["file_path"]] + tag_str_arr
        if file["realpath"]
          dakscript.data << ["#", "SYMLINK: #{file["file_path"]} to #{file["realpath"]}"]
        end
        # seq = seq +1 lookup information on process data element required for supplier filtering
        if file["process_data_element_id"]
          sourcetags << ProcessDataElement.find(file["process_data_element_id"]).data_set.sourcetag
          process_data_elements << ProcessDataElement.find(file["process_data_element_id"])
          data_set_ids = process_data_elements.collect{|pde|pde.data_set_id}.uniq
        end

        coverage = coverage + file["coverage"] if file["coverage"]
        subregions = subregions + file["subregion"] if file["subregion"]
        id_data_sets = []
        if file['conversion_database_id']
          # Find the process data of the previous conversion
          cd = ConversionDatabase.find(file['conversion_database_id'])
          data_set_ids = cd.used_data_sets
          if data_set_ids.first
            id_data_sets = data_set_ids.map{|k,v|v['id']}.compact.uniq.flatten
          end
          indirect_process_data_elements = indirect_process_data_elements +  cd.conversion_job.process_data_elements.collect{|x|x.id}
        end

        if self.is_frontend? && !id_data_sets.empty?
          data_sets = DataSet.find(id_data_sets)
          data_sets.each do |ds|
            if ds
              
              if ds.status != DataSet::STATUS_PRODUCTION && self.production_orderline.production_order.project == Project.default
                raise "DS #{ds.id} \"#{ds.label(true)}\" is not at status PRODUCTION"
              end

              # Add regional parameters when process data has a data set definition for this region
              file["coverage"].each do |region_code|
                #file_ds_region[dsd.id] = file_ds_region[dsd.id] << region_code if !ignore_dsd_regions_chk
                region = Region.find_by_region_code(region_code)
                if region && ds.has_region?(region.id)
                  region_param_json = ds.parameter_settings_by_region_id(region.id)
                  if region_param_json != ''
                    JSON.parse(region_param_json).each do |p|
                      dakscript.parameters  << [ 'parameter',p['parameter_name'],p['parameter_value'] ]
                    end
                  end
                end
              end
            end
          end
        end

      end
      process_data_element_ids = []
      process_data_element_ids = process_data_elements.collect{|pde|pde.id}.uniq if !process_data_elements.empty?
      dakota_scripts << {:group => group, :indirect_process_data_elements => indirect_process_data_elements, :script => dakscript, :coverage => coverage.uniq, :subregion => subregions.uniq, :cores => dakscript.number_of_update_regions, :sourcetags => sourcetags.uniq, :process_data_elements => process_data_element_ids, :totalfilesize => totalsize}
    end

    subregions = {}
    dakota_scripts.each do |v|
      if v[:subregion]
        if subregions[v[:coverage]]
          subregions[v[:coverage]] = subregions[v[:coverage]] + v[:subregion]
        else
          subregions[v[:coverage]] =  v[:subregion]
        end
      else
        subregions[v[:coverage]] = ["NONE"]
      end
    end

    dakota_scripts.each do |v|
      if v[:coverage]
        v[:subregion_amount] = subregions[v[:coverage]].size

        ignore_ds_regions_chk = false
        exe_names = [v[:script].main_executable]
        exe_names += v[:script].pre_improvement_steps if !v[:script].pre_improvement_steps.empty?
        exe_names += v[:script].post_improvement_steps if !v[:script].post_improvement_steps.empty?
        
        exe_names.each do |exe_name|
          exe = Executable.find_by_name(exe_name)
          if exe && exe.ignore_ds_regions_yn
            ignore_ds_regions_chk = true
          end
        end
        if !ignore_ds_regions_chk
          if v[:process_data_elements].empty?
            if !v[:indirect_process_data_elements].empty?
              v[:indirect_process_data_elements].each do |el|
                ds = ProcessDataElement.find(el).data_set
                if ds
                  file_ds_region[ds.id] = [] if !file_ds_region[ds.id]
                  v[:coverage].each do |cvrg|
                    file_ds_region[ds.id] = file_ds_region[ds.id] << cvrg
                  end
                end
              end
            end
          else
            v[:process_data_elements].each do |el|
              ds = ProcessDataElement.find(el).data_set
              if ds
                file_ds_region[ds.id] = [] if !file_ds_region[ds.id]
                v[:coverage].each do |cvrg|
                  file_ds_region[ds.id] = file_ds_region[ds.id] << cvrg
                end
              end
            end
          end
        end
      end
    end

    missing_region_codes = []
    ds_name = ''
    file_ds_region.each do |ds_id,regions|
      ds = DataSet.find(ds_id)
      ds_name = ds.name
      ds.regions.order(:name).each do |region|
        if !regions.include?(region.region_code.downcase)
         missing_region_codes << region.region_code if region.ignore_ds_regions_yn == false
        end
      end
    end
    if !missing_region_codes.empty?
      raise "Following regions are defined in Data set \"#{ds_name}\" but not found in source data: #{missing_region_codes.join(', ')}"
    end
    return dakota_scripts
  end

  def filter_tagmodel(script,output_tag_model_for_job)
    if script[:sourcetags] && script[:sourcetags].size > 0
      output_tag_model_for_job["filling"].each do |filling|
        # only perform AND operation if sourcetag is part of output_tag_model to prevent empty array
        if (output_tag_model_for_job[filling] - script[:sourcetags]).size < output_tag_model_for_job[filling].size
          # use and operator to filter sourcetags (supplier/release) not found in the source files of this
          # jobs
          output_tag_model_for_job[filling] = output_tag_model_for_job[filling] & script[:sourcetags]
        end

        # Filter coverage for particular filling
        if script[:coverage]
          if ( output_tag_model_for_job[filling] && output_tag_model_for_job["#{filling}_coverage"] ) && !output_tag_model_for_job["#{filling}_coverage"].empty? && output_tag_model_for_job["#{filling}_coverage"].size > 1
            filtered_filling_coverage = []
            output_tag_model_for_job["#{filling}_coverage"].each do |org_filling_coverage|
              if script[:coverage].include?(org_filling_coverage)
                filtered_filling_coverage << org_filling_coverage
              end
            end
            if filtered_filling_coverage.empty?
              raise "Coverage for #{filling} is not included in script-coverage #{script[:coverage].join('|')}"
            else
              output_tag_model_for_job["#{filling}_coverage"] = filtered_filling_coverage
            end
          end
        end

      end
    end
    return(output_tag_model_for_job)
  end

  def create_conversion_jobs(regenerate_job=nil)
    messages = []
    ConversionJob.transaction do
      dakota_scripts.each do |script|
        # build up tagmodel
        filesize = script[:totalfilesize]
        raise "Tag model not found for conversion " + self.id.to_s if !design_json["output_tag_model"]

        tagmodel = design_json["output_tag_model"]
        tagmodel["subregion"] = script[:subregion] if script[:subregion] && script[:subregion].size > 0

        tagmodel = filter_tagmodel(script,tagmodel)
        tagmodel["branch"] = self.conversiontool.get_branch
        # tagmodel["bundle_release"] = self.conversiontool.code

        cj = nil
        database = nil
        found = false
        jobs = ConversionJob.where(project_id:self.project_id).order("id desc")
        jobs = jobs.find_by_tagmodel_and_coverage(jobs, tagmodel, script[:coverage])
        jobs.each do |j|
          cj = j #if j.conversion.id == self.id
          if cj && !cj.allow_rerun?  # job already found
            cj.conversion_databases.available.each do |cd|
              if cj.conversion.output_format == self.output_format # same output format needed
                database = cd
                if !self.production_orderline.conversion_databases.find_by_id(self.id)
                  self.production_orderline.conversion_databases << cd
                  self.production_orderline.save!
                end
                found = true
                break
              end
            end
            break if found
            if cj.conversion_databases.size == 0
              if cj.conversion.production_orderline.production_order.bp_name != ProductionOrder::BP_NAME_CANCELLED &&
                  cj.conversion.output_format == self.output_format && !cj.end_status?  # same output format needed
                if cj.conversion_id != self.id.to_s && (self.force_create_id.nil? || cj.conversion.force_create_id == self.force_create_id)
                  raise "Similar Job already queued for conversion #{self.id} with job id : #{cj.id.to_s} at status #{cj.status_str}"
                else
                  found = true
                  break
                end
              end
            end
          end
        end
        if found && !regenerate_job
          if cj.conversion.id != self.id
            message =  "Existing result found " + script[:coverage].join.upcase
            message = message + " Job Id: " + cj.id.to_s if cj
            message = message + " Db Id: " + database.id.to_s if database
            messages << message
            self.production_orderline.chosen_conversion_id = cj.conversion.id
            self.production_orderline.save!
            if self.force_create_id && self.force_create_id != cj.conversion.force_create_id
              self.checklog.add_debug_line("Forced New Job creation - Job already found at status #{cj.status_str} (id: #{cj.id})", false)
            else
              self.checklog.add_info_line("New Job not created - Job already found at status #{cj.status_str} (id: #{cj.id})", false)
              self.checklog.add_debug_line("Related tag model " + tagmodel.inspect + " " + script[:coverage].to_s, false)
              next
            end
          else
            self.checklog.add_info_line("Found existing job '#{cj.id}' within the same conversion.", false)
            cj.checklog = self.checklog.new_sub("Conversion job #{cj.id}", Checklog::LOGLEVEL_INFO)
            next # never create same job again within same conversion
          end
        end

        begin
          available_cds = ConversionDatabase.available
          cds = available_cds.find_by_tagmodel_and_coverage(available_cds, tagmodel, script[:coverage])
        rescue => e
          puts e.message + " for  conversion " + self.id.to_s  + "coverage tags :: " + script[:coverage].size.to_s
        end
        founddb = false
        # always create new job for now
        if cds.size > 0
          cds.each do |db|
            next unless db.project_id == self.project_id
            next if self.force_create_id && self.force_create_id != db.conversion.force_create_id

            cd = db
            if cd.conversion.output_format == self.output_format && !regenerate_job
              self.checklog.add_info_line("Conversion Database already found for " + tagmodel.inspect + "  coverage : " + script[:coverage].to_s + " -> database id " + cd.id.to_s, true)

              message = "Existing result found " + script[:coverage].join(" ").upcase
              message = message + " Db Id: " + cd.id.to_s
              messages <<  message
              # set chosen conversion id
              self.production_orderline.chosen_conversion_id = cd.conversion.id
              self.production_orderline.save!
              founddb = true
              break
            end
          end
        end

        if !founddb
          self.checklog.add_debug_line("Conversion Database not found, creating conversion job.")
          cj = ConversionJob.new(:project_id => self.project_id)
          # filter tagmodel if needed
          #
          # design_json["output_tag_model"]
          output_tag_model_for_job = design_json["output_tag_model"]
          tagmodel["subregion"] = script[:subregion] if script[:subregion] && script[:subregion].size > 0

          output_tag_model_for_job = filter_tagmodel(script,output_tag_model_for_job)
          output_tag_model_for_job["subregion"] = script[:subregion] if script[:subregion] && script[:subregion].size > 0
          output_tag_model_for_job["branch"] = self.conversiontool.code.split("_")[0].to_s
          output_tag_model_for_job["bundle_release"] = self.conversiontool.code
          ProductDesign.store_tag_model(cj,output_tag_model_for_job)
          cj.status = ConversionJob::STATUS_PLANNED
          self.conversion_jobs << cj
          save
          cj.checklog = self.checklog.new_sub("Conversion job #{cj.id}", Checklog::LOGLEVEL_INFO)

          region_code = Region.active.find_by_name("World").region_code
          if self.region_id && (!script[:coverage] || script[:coverage].empty?)
            region_code = self.region.region_code.to_s
          end
          if script[:coverage] && !script[:coverage].empty?
            region = Region.active.find_by_region_code(script[:coverage][0])
            if region && region.parent && region.class.name != "Continent"
              region_code = region.parent.region_code.to_s
            else
              region_code = region.region_code.to_s if region
            end
          end



          # take some defaults to get up and running
          company_code = "unk"
          company_code = self.data_release.company_abbr.ms_abbr_3.to_s.downcase if self.data_release
          data_release_code = "XX-XX"
          data_release_code = self.data_release.release_code.to_s if self.data_release

          if script[:sourcetags] && script[:sourcetags].size > 0
            # extract release from source tags - should be more specified than conversion level information
            sourcetags = script[:sourcetags][0].split("|")
            company =  CompanyAbbr.find_by_ms_abbr_3(sourcetags[1])
            if !company
              company =  CompanyAbbr.find_by_company_name(sourcetags[1])
            end
            release = nil
            release = company.data_releases.where(:name => sourcetags[2]) if company
            if release
              data_release_code = release[0].release_code.to_s.downcase
              company_code = release[0].company_abbr.ms_abbr_3.to_s.downcase
            end
          end

          cvtool_char =  self.conversiontool ? self.conversiontool.branch_indicator : ''

          # OEM code in database name when not intermediate
          oem_code = ''
          data_type= DataType.active.find_by_name(self.output_format)
          if data_type && !data_type.is_intermediate
            product_line = ProductLine.find(JSON.parse(self.production_orderline.product_design)["product_line"]["id"])
            oem = product_line && product_line.company_abbr ? product_line.company_abbr.ms_abbr_3.downcase : ''
            oem_code = "_#{oem}" if !oem.blank?
          end

          tag_list_string = region_code.downcase + "_" + company_code + oem_code + "_" + data_release_code+ "_" + Date.today.strftime("%Y%m%d") + cvtool_char
          if script[:coverage] && ( script[:coverage].size <= 1 ) && script[:coverage][0] && ( script[:coverage][0].downcase != region_code.downcase )
            tag_list_string = tag_list_string + "_" +   script[:coverage][0].downcase
          end

          if script[:subregion] && script[:subregion].size > 0
            tag_list_string = tag_list_string + "_" +  script[:subregion][0].downcase
          end

          cj.conversion_environment_id = self.conversion_environment_id if self.conversion_environment
          uniq_tags = tag_list_string.split("_").map{|x|x.to_s.downcase}.uniq
          cj.tag_list = output_tag_model_for_job["filling"].join(",") + "," + uniq_tags.join(",")

          cj.set_tag_list_on(:context_coverage, script[:coverage]) if script[:coverage] && script[:coverage].size > 0
          if script[:subregion] && script[:subregion].size > 0
            cj.set_tag_list_on(:context_subregion, script[:subregion])
          end
          
          cj.set_tag_list_on(:context_labelsupplier, company_code)
          cj.set_tag_list_on(:context_labelrelease, data_release_code)


          cj.job_script = script[:script]
          cj.job_script.tag_list_string = tag_list_string
          cj.working_dir = "w_################-" + tag_list_string  # work-dir without path
          # scheduling info
          exeobj = executable
          filesize_mb = ((filesize.to_f / 2**20)+0.5).round(0).to_i  # filesize in MB, rounded
          if exeobj.schedule_manual
            cj.schedule_manual = exeobj.schedule_manual
            cj.log_action("Schedule only manual due to setup of executable #{exeobj.name}.")
          else
            cj.schedule_manual = false
          end
          cj.schedule_cores = exeobj.schedule_cores ? exeobj.schedule_cores : 1
          if script[:cores] && script[:cores] > 1
            cj.schedule_cores = script[:cores]
          end
          schedule_memory_base = exeobj.schedule_memory_base ? exeobj.schedule_memory_base : 0
          schedule_memory_times_input = exeobj.schedule_memory_times_input ? exeobj.schedule_memory_times_input : 0
          schedule_diskspace_base = exeobj.schedule_diskspace_base ? exeobj.schedule_diskspace_base : 0
          schedule_diskspace_times_input = exeobj.schedule_diskspace_times_input ? exeobj.schedule_diskspace_times_input : 0
          cj.schedule_memory = schedule_memory_base + (schedule_memory_times_input * filesize_mb)
          cj.schedule_diskspace = schedule_diskspace_base + (schedule_diskspace_times_input * filesize_mb)
          cj.schedule_allow_virtual = exeobj.schedule_allow_virtual ? exeobj.schedule_allow_virtual : false
          cj.schedule_exclusive = exeobj.schedule_exclusive ? exeobj.schedule_exclusive : false

          if script[:subregion]
            cj.group_by = "subregion"
            cj.group_value = script[:coverage].join("-")
            cj.group_amount = script[:subregion_amount]
          end

          if !regenerate_job
            ConversionJob.transaction do
              if cj.save!
                ConversionJobProcessDataElement.create_by_job_and_process_data_elements(cj.id,script[:process_data_elements])
                ConversionJobProcessDataElement.create_by_job_and_process_data_elements(cj.id,script[:indirect_process_data_elements], true)
              end
            end
          else
            if found &&  cj.tag_list.sort == regenerate_job.tag_list.sort
              ConversionJob.transaction do
                if cj.save!
                  ConversionJobProcessDataElement.create_by_job_and_process_data_elements(cj.id,script[:process_data_elements])
                  ConversionJobProcessDataElement.create_by_job_and_process_data_elements(cj.id,script[:indirect_process_data_elements], true)
                end
              end
              return cj
            end
          end

          cj.reload
          # Cancel job when process data is created based on DS and not in white list
          cj.conversion_job_process_data_elements.each do |cj_pde|
            ds = cj_pde.process_data_element.data_set
            if ds
              region = Region.find_by_region_code(cj.tagmodel['coverage'][0])
              if !ds.has_region?(region.id,'latest_approved',self.project_id)
                cj.status = ConversionJob::STATUS_CANCELLED
                cj.log_action("Skipped for processing according data set definition #{ds.id } #{ds.name} by cancelling conversion job.")
                cj.save!
                cj.checklog.add_attributes({"SKIPPED FOR PROCESSING"=>"Not in coverage of DS: #{ds.id } #{ds.name}"})
                break
              end
            end
          end

          cj.checklog.update_result(cj.status_str)
          cj.checklog.add_attributes({
              "Conversion environment" => cj.conversion_environment ? cj.conversion_environment.name : nil,
              "Tag list" => cj.tag_list,
              "Run script" => cj.job_script ? cj.job_script.to_script("CONVERSION_JOB_ID", cj.id) : cj.run_script,
              "Working dir" => cj.working_dir,
              "Schedule manual" => cj.schedule_manual,
              "Schedule cores" => cj.schedule_cores,
              "Schedule memory" => cj.schedule_memory,
              "Schedule diskspace" => cj.schedule_diskspace,
              "Schedule allow virtual" => cj.schedule_allow_virtual,
              "Schedule exclusive" => cj.schedule_exclusive
            })
        end
      end
    end
    if regenerate_job
      raise "Not possible to regenerate job, no matches found."
    else
      return messages
    end
  end
  
  def get_idtags_for_connection(connection)
    # this function builds a unique tag list - might need to be revisited to agree on additional tags
    tags = []
    tags << connection["source_spec"]["originating_format"] if connection["source_spec"]["originating_format"].to_s.upcase
    tags << connection["source_spec"]["format"] if connection["source_spec"]["format"].to_s.upcase
    tags << connection["source_spec"]["supplier"] if connection["source_spec"]["supplier"].to_s.upcase
    tags << connection["source_spec"]["release"] if connection["source_spec"]["release"].to_s.upcase
    tags << connection["source_spec"]["area"] if connection["source_spec"]["area"].to_s.upcase
    return tags
  end
  
  def monitor_conversion
    return 'Conversion not in progress' unless self.status == STATUS_CONVERSION
    pol = self.production_orderline

    if self.conversion_jobs.size == 0
      Conversion.transaction do
        pol.bp_message = "SKIPPED: Conversion #{pol.chosen_conversion_id} has same credentials."
        pol.save!
        set_status(STATUS_CANCELLED)
        self.rejection_remark = 'All jobs already created, conversion set to removed.'
        self.production_orderline_id = nil
        save!
      end
      return pol.bp_message
    end

    # self.conversion_jobs.size > 0
    complete = 0
    failed = 0
    queued = 0
    hold = 0
    active = 0
    manual = 0
    total = 0
    db_complete = 0
    db_val_active = 0
    db_val_errors = 0
    db_val_warnings = 0
    db_val_unresolved = 0
    db_total = 0
    self.conversion_jobs.each do |cj|
      total += 1 unless cj.rescheduled?

      case cj.status
        when ConversionJob::STATUS_MANUAL
          manual += 1
        when ConversionJob::STATUS_QUEUED
          queued += 1
        when ConversionJob::STATUS_HOLD
          hold += 1
        when ConversionJob::STATUS_FAILED
          failed += 1
        when ConversionJob::STATUS_PROCESSING
          active += 1
        when ConversionJob::STATUS_COMPLETE, ConversionJob::STATUS_CANCELLED
          complete += 1
        else
          # don't count ConversionJob::STATUS_RESCHEDULED
      end
    end

    self.conversion_databases.each do |cd|
      db_total += 1
      db_val_unresolved += 1 unless cd.validation_errors.unresolved.empty?
      case cd.status
        when ConversionDatabase::STATUS_READY_FOR_VALIDATION, ConversionDatabase::STATUS_VALIDATING
          db_val_active += 1
        when ConversionDatabase::STATUS_HAS_ERRORS
          db_val_errors += 1
        when ConversionDatabase::STATUS_HAS_WARNINGS
          db_val_warnings += 1
        else # end status
          db_complete += 1
      end
    end

    if complete < total || db_complete < db_total || db_val_unresolved > 0
      # conversion is still running
      msg = ["TOTAL: #{total.to_s}"]
      msg << "COMPLETED: #{complete.to_s}" if complete > 0
      msg << "ACTIVE: #{active.to_s}" if active > 0
      msg << "FAILED: #{failed.to_s}" if failed > 0
      msg << "VALIDATION PROBLEMS: #{db_val_errors.to_s}" if db_val_errors > 0
      msg << "VALIDATION WARNINGS: #{db_val_warnings.to_s}" if db_val_warnings > 0
      msg << "VALIDATION ERRORS UNRESOLVED: #{db_val_unresolved.to_s}" if db_val_unresolved > 0
      msg << "ON HOLD: #{hold.to_s}" if hold > 0
      msg << "QUEUED: #{queued.to_s}" if queued > 0
      msg << "VALIDATING: #{db_val_active.to_s}" if db_val_active > 0
      msg << "MANUAL: #{manual.to_s}" if manual > 0
      raise IsActiveRuntimeError.new("#{msg.join(' ')}")
    end

    # conversion (including any automatic conversion database validations) is finished
    Conversion.transaction do
      self.set_status(STATUS_FINISHED)
      save!
      if pol
        pol.chosen_conversion_id = self.id
        pol.bp_message = "#{complete}/#{total} jobs completed"
        pol.save!
      end
    end

    return "#{complete}/#{total} jobs completed"
  end
  
  def queue_conversion
    messages = []
    Conversion.transaction do
      self.set_status(STATUS_CONVERSION)
      messages = messages + create_conversion_jobs
      self.conversion_jobs.where(:status => ConversionJob::STATUS_PLANNED).each do |cj|
        cj.checklog = self.checklog.new_sub("Conversion job #{cj.id}", Checklog::LOGLEVEL_INFO)
        cj.queue
      end
      save!
    end
    self.checklog.add_debug_line("Finished 'queue_conversion.")   
    return messages
  end

  def reject(user = 'system')
    Conversion.transaction do
      set_status(STATUS_REJECTED)
      self.conversion_jobs.each{ |j| j.terminate(user) }  # if conversion_jobs is not empty, job_support? is true
      save!
    end
  end

  def terminate(user = 'system', dbs_to_remove = nil)
    Conversion.transaction do
      set_status(STATUS_CANCELLED)
      if self.job_support?
        self.conversion_jobs.each{ |j| j.terminate(user, dbs_to_remove) }
      else
        self.conversion_databases.each{ |cd| cd.terminate(user) if dbs_to_remove.nil? || dbs_to_remove.index(cd.id) }
      end
      save!
    end
  end

  def cancel(name_user = nil, dbs_to_remove = nil)
    Conversion.transaction do
      if self.job_support? || self.conversion_databases.available.empty?
        set_status(STATUS_CANCELLED)
        self.conversion_jobs.each{ |j| j.cancel(name_user, dbs_to_remove) }
        save!
      end
    end
  end

  def expire(user = 'system')
    Conversion.transaction do
      cancel(user) unless self.end_status?
      msg = "#{Date.today.strftime('%d-%m-%Y')} Database expired."
      self.conversion_databases.available.each{ |cd| cd.mark_for_removal(user, msg) unless cd.file_system_status == ConversionDatabase::FILE_SYSTEM_STATUS_PERSISTENT }
      save!
    end
  end
  
  def conversion_tool_release
    cvtool = ''
    cvtool += self.conversiontool.code if self.conversiontool
    cvtool += '/' + self.cvtool_bundle.name  if self.cvtool_bundle
    return cvtool
  end

  def job_support?
    self.production_orderline.production_order.ordertype.job_support_yn rescue false
  end

  def is_frontend?
    self.production_orderline && self.production_orderline.production_order.ordertype.is_frontend
  end

  def allow_edit?
    !self.job_support? && ( self.production_orderline.nil? || !self.end_status? )
  end

  def allow_edit_result?
    self.allow_edit? && self.status != STATUS_REJECTED && self.status != STATUS_ENTRY
  end

  def reset_result(user='system')
    if self.production_orderline_id.blank? && !self.outcome_persistent_yn && self.conversion_databases.persistent.empty?
      Conversion.transaction do
        self.conversion_databases.each do |conversion_database|
          conversion_database.set_status(ConversionDatabase::STATUS_CANCELLED)
          conversion_database.set_file_system_status(ConversionDatabase::FILE_SYSTEM_STATUS_REMOVED)
          conversion_database.remarks = "#{conversion_database.remarks}\n Flagged as removed by reset result of conversion-id #{self.id} by user #{user}."
          conversion_database.save!
        end
        self.status = STATUS_REQUESTED
        self.outcome = default_selection_outcome
        self.server_name = nil
        self.size_bytes = nil
        self.size_mb = nil
        return self.save
      end
    end
    return false
  end

  def handle_validation_errors(error_name, error_ok, accept_validation_error, selected_db_ids, user, ticket_nr = nil)
    self.conversion_databases.each do |db|
      # only look at the databases with the given validation error
      if validation_error = db.validation_errors.unhandled.find_by_name(error_name)
        if error_ok && accept_validation_error
          # accept the validation error in the conversion database
          validation_error.resolved_yn = true
          validation_error.ticket_nr = ticket_nr unless ticket_nr.blank?
          validation_error.save!
          db.handle_validation_error_accepted(error_name, user)
        else
          # investigate the validation error, ticket_nr better not be null or else this will have no effect.
          validation_error.ticket_nr = ticket_nr
          validation_error.save!
          db.handle_validation_error_investigation(error_name, user) unless error_ok
        end
      end

      if ticket_nr && selected_db_ids.include?(db.id.to_s)
        # copy the selected validation results to the crashdir for further analysis
        db.copy_to_crashdir(ticket_nr)
      end
    end
  end

  def toggle_outcome_persistence(overrule=nil)
    if overrule.nil?
      if self.outcome_persistent_yn
        self.outcome_persistent_yn = false
      else
        self.outcome_persistent_yn = true
      end
    else
      self.outcome_persistent_yn = overrule
    end
    Conversion.transaction do
      self.conversion_databases.each do |cd|
        cd.toggle_persistence(self.outcome_persistent_yn)
      end
      self.save!
    end
  end

  def unmark_for_removal(user = nil)
    Conversion.transaction do
      self.conversion_databases.each do |cd|
        cd.unmark_for_removal(user)
        cd.save!
      end
    end
  end

  def create_test_request_allowed?
    return true if !self.productid = false
    return false
  end

  def test_plan
    if !self.productid.blank?
      product = Product.find_by_volumeid(self.productid)
      if product.product_line
        tp = TestPlan.where("product_line_id = #{product.product_line_id}").last
        if !tp
          tp = TestPlan.default
        end
      else
        tp = Testplan.default
      end
    else
      tp = Testplan.default
    end
    return tp
  end

  def create_test_request
    TestRequest.transaction do
      if create_test_request_allowed?
        self.conversion_databases.each do |db|
            db.create_test_request
        end
      end
    end
  end

  def create_patch_request(request_user_obj, conversion_database_id)
    new_conversion = self.dup
    new_conversion.status = STATUS_ENTRY
    new_conversion.converter = ""
    new_conversion.request_by = request_user_obj.full_name
    new_conversion.request_user_id = request_user_obj.id
    new_conversion.request_date = Date.today
    new_conversion.input_format = self.output_format
    new_conversion.data_files = ConversionDatabase.find(conversion_database_id).path_and_file
    new_conversion.reference_numbers = "conversion-id-#{self.id}"
    new_conversion.remarks = ""
    new_conversion.additional_info = "PATCH REQUEST ON OUTCOME OF CONVERSION-#{self.id}."
    new_conversion.server_name = nil
    new_conversion.production_orderline_id = nil
    new_conversion.result_files = nil
    new_conversion.cloned_from_id = self.id
    new_conversion.replaced_by_id = nil
    new_conversion.patch_request_yn = true
    new_conversion.save!
    return new_conversion
  end

  def self.patch_request_by_month(yyyy,mm)
    rdate = Date.new(yyyy,mm,1)
    Conversion.where(:request_date => rdate.beginning_of_month..rdate.end_of_month,:patch_request_yn => true)
  end

  def self.patch_request_hash_by_year(yyyy)
    m = 0
    r = {}
    12.times do
      m = m + 1
      r[Date::MONTHNAMES[m]] = patch_request_by_month(yyyy,m)
    end
    return r
  end

  def self.patch_request_totals_hash_by_year_cvtool_bundle(yyyy)
    m = 0
    h = {}
    12.times do
      m = m + 1
      h.merge!(patch_request_by_month(yyyy,m).group(:cvtool_bundle_id).size){|k, oldval, newval| newval + oldval}
    end
    r = {}
    h.each do |k,v|
      r[CvtoolBundle.find(k).name] = v
    end
    return r
  end

  def project_id
    if self.production_orderline
      return self.production_orderline.production_order.project_id
    else
      return Project.default.id
    end
  end

  def duplicate(user_obj=nil,without_production_orderline=false,include_tpd=true)
    conversion = self.dup
    conversion.status = STATUS_ENTRY
    conversion.outcome = default_selection_outcome
    conversion.cloned_from_id = self.id
    conversion.request_date = Time.now
    conversion.conversion_date = nil
    conversion.converter = nil
    conversion.remarks = nil
    conversion.cvtoolenv = ""
    conversion.size_bytes = nil
    conversion.size_mb = nil
    conversion.database_name = ""
    conversion.converter = ""
    conversion.additional_info = "Cloned from #{self.id}.\n" + self.additional_info
    conversion.conversion_date = nil
    conversion.reception_reference = nil
    conversion.created_at = Time.now
    conversion.server_name = nil
    conversion.result_files = nil
    conversion.status_remark = nil
    conversion.rejection_remark = nil
    conversion.priority = "Normal"
    conversion.outcome_persistent_yn = false
    if user_obj
      conversion.request_by = user_obj.full_name
      conversion.request_user_id = user_obj.id
    end
    if without_production_orderline
      conversion.production_orderline_id = nil
      conversion.purpose = "test"
      conversion.productid = nil
    end
    if !include_tpd
      conversion.tpd_products = nil
      conversion.tpd_rejected = nil
      conversion.tpd_rejection_remark = nil
      conversion.tpd_result_sender = nil
    end
    return conversion
  end

  def last_conversion?
    result = false
    steps = JSON.parse(self.production_orderline.product_design)["conversion_steps"].size
    if self.design_sequence >= steps - 1
      result = true
    end
    return result
  end

  def label
    return "#{self.input_format}2#{self.output_format} #{self.region.region_code if self.region} #{self.data_release.company_abbr.ms_abbr_3 if self.data_release} #{self.data_release.name if self.data_release}"
  end

  def self.active_unassigned_conversions(project_id=Project.default.id)
    ra = []
    Conversion.in_cue.each do |conversion|
      ra << conversion if conversion.project_id == project_id
    end
    return ra
  end

  def map_outcome_to_status(outcome,new_tpd_products='')
    case outcome
    when *['No result','']
      self.set_status(Conversion::STATUS_CONVERSION)  unless self.server_name.blank?
    when 'Not OK'
      # Do nothing
    when 'Partly compiled'
      self.set_status(Conversion::STATUS_CONVERSION)
    when *['Crashed','Unusable','']
      self.set_status(Conversion::STATUS_FINISHED) # Will only work if !reject because otherwise the status is already in an end-status
    when 'Correct'
      if self.tpd && new_tpd_products.blank? && self.tpd_products.blank?
        self.set_status(Conversion::STATUS_REQUESTED_TPD)
      else
        self.set_status(Conversion::STATUS_FINISHED)
      end
    else
      if !self.conversion_databases.available.empty?
        if self.tpd && new_tpd_products.blank? && self.tpd_products.blank?
          self.set_status(Conversion::STATUS_REQUESTED_TPD)
        else
          self.set_status(Conversion::STATUS_FINISHED)
        end
      end
    end
  end

  def deliver_manual_update_mail(outcome,org_status)
    message = ''
    case self.status
    when Conversion::STATUS_CONVERSION
      if outcome == 'Not OK'
        Postoffice.conversion_result(self.id,false).deliver_now
      end
    when Conversion::STATUS_REQUESTED_TPD
      Postoffice.conversion_result(self.id,false).deliver_now
      Postoffice.tpd_request(self.id).deliver_now
    when Conversion::STATUS_FINISHED
      case org_status
      when Conversion::STATUS_REQUESTED_TPD
        if self.tpd_products
          Postoffice.tpd_result(self.id).deliver_now
        else
          message += "TPD requested.<br />"
          Postoffice.tpd_request(self.id).deliver_now
        end
      when Conversion::STATUS_FINISHED
        Postoffice.conversion_result(self.id,true).deliver_now # Resend
        Postoffice.tpd_result(self.id).deliver_now if !self.tpd_products.blank?
      else
        Postoffice.conversion_result(self.id,false).deliver_now
      end
    when Conversion::STATUS_REJECTED
      message += "Request is rejected"
      Postoffice.reject_conversion_request(self.id).deliver_now
    end
  end

  def allow_production_orderline_transition?
    # avoid distorted order flow
    ( self.production_orderline && ["REQUEST CONVERSION","SEND CONVERSION REQUEST","CONVERSION"].include?(self.production_orderline.bp_name) )
  end

  def assembly_select_option_list
    # determine assembly option_list
    assembly_option_list = []
    begin
      # check whether the conversion assembly is in the test scope. If not an exception will be thrown, so
      # instead of an if, we use a rescue
      AssemblyHeader.test.find(self.assembly_id)
    rescue
      assembly_option_list << [self.assembly.name, self.assembly.id] if self.assembly
    end
    assembly_option_list += AssemblyHeader.test.collect{|a|[a.name,a.id]}
  end

  def self.map_attributes_from_product_id(product_id)
    attr_h = {}
    product = Product.find_by_real_volumeid("#{product_id}")
    assembly = AssemblyHeader.find_by(:product_id => product_id)
    company_abbr = product.productsets.last.company_abbr if product && !product.productsets.empty?
    area_name = product.productsets.last.area_name if product && !product.productsets.empty?
    region = Region.find_by(name: area_name) if area_name
    region = product.coverage.regions.active.last.parent if product && !region && product.coverage && !product.coverage.regions.empty?
    region = product.coverage.regions.active.last if product && !region && product.coverage && !product.coverage.regions.empty?
    product_output = product.productsets.last.output_format.split(' ')[0].strip if product && !product.productsets.empty?
    data_type_out = DataType.find_by(name: product_output) if product_output
    data_type_in = DataType.find_by(name: 'dhive')

    pdi = ProductDataSetInfo.find_by(:product_id => Product.last.id,:major_dataset => true)
    pdi = ProductDataSetInfo.find_by(:product_id => Product.last.id) if pdi.nil?
    data_release = DataRelease.where(name: pdi.data_release).last if pdi
    attr_h[:data_release_id] = data_release ? data_release.id : nil
    attr_h[:additional_info] = product.productsets.empty? ? nil : product.productsets.collect{|ps|ps.remarks}.join("\n\n")
    attr_h[:datasource_id] = company_abbr ? company_abbr.id : nil
    attr_h[:purpose] = "Production"
    attr_h[:assembly_id] = assembly ? assembly.id : nil
    attr_h[:region_id] = region ? region.id : nil
    attr_h[:output_format] = data_type_out ? data_type_out.name : nil
    attr_h[:input_format] = data_type_in ? data_type_in.name : nil
    attr_h[:reference_numbers] = nil
    attr_h[:data_files] = nil
    if assembly
      assembly_import = assembly.export_for_conversion
      attr_h[:data_files] = assembly_import[1].blank? ? nil : assembly_import[1]
      attr_h[:reference_numbers]= assembly_import[0].blank? ? nil : assembly_import[0]
    end
    return attr_h
  end
  
end


