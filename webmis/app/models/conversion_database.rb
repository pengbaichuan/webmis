class ConversionDatabase < ActiveRecord::Base
  
  acts_as_ordered_taggable_on :tags
  # Example: ConversionDatabase.includes(:base_tags).where("tags.name = 'LTEF2DH'")
  belongs_to :conversion
  belongs_to :conversion_job
  belongs_to :production_orderline
  belongs_to :region
  has_many :production_orderline_conversion_databases
  has_many :production_orderlines, :through => :production_orderline_conversion_databases
  has_many :test_requests ,:foreign_key => "test_db_conversion_databases_id"
  has_many :included_database_test_requests
  has_many :validation_jobs, :foreign_key => :reference_id, :dependent => :destroy
  has_many :validation_errors

  after_initialize :defaults

  STATUS_READY_FOR_VALIDATION   =   10
  STATUS_VALIDATING             =   20
  STATUS_HAS_ERRORS             =   50
  STATUS_HAS_WARNINGS           =   70
  STATUS_REJECTED               =  200
  STATUS_ACCEPTED               =  300
  STATUS_CANCELLED              =  800
  STATUS_NOT_VALIDATED          = 1000

  FILE_SYSTEM_STATUS_AVAILABLE          =   10
  FILE_SYSTEM_STATUS_PERSISTENT         =  100
  FILE_SYSTEM_STATUS_MARKED_FOR_REMOVAL =  900
  FILE_SYSTEM_STATUS_REMOVED            = 1000

 

  @@status_array = Array.new
  @@status_array[STATUS_READY_FOR_VALIDATION]  = "Ready for validation"
  @@status_array[STATUS_VALIDATING]            = "Validating"
  @@status_array[STATUS_HAS_ERRORS]            = "Has errors"
  @@status_array[STATUS_HAS_WARNINGS]          = "Has Warnings"
  @@status_array[STATUS_REJECTED]              = "Rejected"
  @@status_array[STATUS_ACCEPTED]              = "Accepted"
  @@status_array[STATUS_CANCELLED]             = "Cancelled"
  @@status_array[STATUS_NOT_VALIDATED]         = "Not validated"

  @@file_system_status_array = Array.new
  @@file_system_status_array[FILE_SYSTEM_STATUS_AVAILABLE]           = 'Available'
  @@file_system_status_array[FILE_SYSTEM_STATUS_PERSISTENT]          = 'Persistent'
  @@file_system_status_array[FILE_SYSTEM_STATUS_MARKED_FOR_REMOVAL]  = 'Marked for removal'
  @@file_system_status_array[FILE_SYSTEM_STATUS_REMOVED]             = 'Removed'

  scope :present, -> { where(:file_system_status => [FILE_SYSTEM_STATUS_AVAILABLE, FILE_SYSTEM_STATUS_PERSISTENT, FILE_SYSTEM_STATUS_MARKED_FOR_REMOVAL]).order('conversion_id DESC') }
  scope :available, -> { where("file_system_status <= #{FILE_SYSTEM_STATUS_PERSISTENT}").order('conversion_id DESC') }
  scope :available_no_intermediate, -> { where("file_system_status <= #{FILE_SYSTEM_STATUS_PERSISTENT} AND db_type != 'dHive' AND db_type != 'RDB'").order('conversion_id DESC') }
  scope :marked_for_removal, -> { where(:file_system_status => FILE_SYSTEM_STATUS_MARKED_FOR_REMOVAL).order('conversion_id DESC') }
  scope :outcome_removed, -> { where(:file_system_status => FILE_SYSTEM_STATUS_REMOVED).order('conversion_id DESC') }
  scope :persistent, -> { where(:file_system_status => FILE_SYSTEM_STATUS_PERSISTENT).order('conversion_id DESC') }
  scope :outstanding, -> { where("conversion_databases.status < #{STATUS_REJECTED}")}
  def defaults
    if self.new_record?
      self.status ||= STATUS_READY_FOR_VALIDATION
      self.file_system_status ||= FILE_SYSTEM_STATUS_AVAILABLE
    end
  end

  def self.totals(project_id, purpose="all")
    return_hash = ConversionDatabase.joins("inner join conversions on conversion_databases.conversion_id = conversions.id inner join jobs on conversion_databases.conversion_job_id = jobs.id").where("conversions.status <= #{Conversion::STATUS_FINISHED} AND jobs.project_id = ?", project_id).group("conversion_databases.status").size
    
    case purpose
    when "auto_que"
      return_hash.keep_if{|key,value| [STATUS_HAS_ERRORS].include?(key)}
    end

    return return_hash
  end

  def status_str
    return  @@status_array[status]
  end

  def file_system_status_str
    return @@file_system_status_array[file_system_status]
  end

  def self.status_array
    return @@status_array
  end

  def self.file_system_status_array
    return @@file_system_status_array
  end

  def self.status_hash
    # for an option list of all possible statuses
    s_hash = Hash.new
    @@status_array.each{ |s| s_hash[s] = @@status_array.index(s) unless s.nil? }
    s_hash.sort_by { |_, v| v }
  end

  def status_hash
    # for an option list of all valid statuses for the current conversion database
    s_hash = Hash.new
    @@status_array.each{ |s| s_hash[s] = @@status_array.index(s) unless (s.nil? || !self.allow_status?(@@status_array.index(s))) }
    s_hash.sort_by { |_, v| v}
  end

  def self.file_system_status_hash
    s_hash = Hash.new
    @@file_system_status_array.each{ |s| s_hash[s] = @@file_system_status_array.index(s) unless s.nil? }
    s_hash.sort_by { |_, v| v }
  end

  def file_system_status_hash
    s_hash = Hash.new
    @@file_system_status_array.each{ |s| s_hash[s] = @@file_system_status_array.index(s) unless (s.nil? || !self.allow_file_system_status?(@@file_system_status_array.index(s))) }
    s_hash.sort_by { |_, v| v }
  end

  def before_save
    @coverage_str = self.coverage
  end

  def allow_status_change?(new_status)
    result = case status
               when STATUS_READY_FOR_VALIDATION
                 status != new_status # begin status, there is no strict work flow so allow every other status for now
               when STATUS_VALIDATING
                 validation_jobs_finished? && [STATUS_HAS_ERRORS, STATUS_HAS_WARNINGS, STATUS_REJECTED, STATUS_ACCEPTED, STATUS_CANCELLED, STATUS_NOT_VALIDATED].include?(new_status)
               when STATUS_HAS_ERRORS, STATUS_HAS_WARNINGS
                 # validation result known, change to an end-status
                 [STATUS_REJECTED, STATUS_ACCEPTED, STATUS_CANCELLED, STATUS_NOT_VALIDATED].include?(new_status)
               when STATUS_REJECTED, STATUS_ACCEPTED, STATUS_CANCELLED, STATUS_NOT_VALIDATED
                 false # end status, no status change allowed
               else
                 true # undefined begin status, allow everything to get in a defined status again.
             end
    return result
  end

  def allow_status?(new_status)
    self.status == new_status || allow_status_change?(new_status)
  end

  def set_status(new_status)
    result = allow_status_change?(new_status)
    self.status = new_status if result
    return result
  end

  def end_status?
    return [STATUS_REJECTED, STATUS_ACCEPTED, STATUS_CANCELLED, STATUS_NOT_VALIDATED].include?(self.status)
  end

  def validation_jobs_finished?
    return self.validation_jobs.outstanding.empty?
  end

  def allow_file_system_status_change?(new_file_system_status)
    return file_system_status != new_file_system_status && allow_file_system_status?(new_file_system_status)
  end

  def allow_file_system_status?(new_file_system_status)
    end_state = self.end_status?     # you can only change some file_system_statuses if conversion_database is in an end status
    expired = self.conversion.nil? || self.conversion.expired?
    result = case new_file_system_status
               when FILE_SYSTEM_STATUS_AVAILABLE
                 !expired
               when FILE_SYSTEM_STATUS_PERSISTENT
                 end_state
               when FILE_SYSTEM_STATUS_MARKED_FOR_REMOVAL
                 self.status > STATUS_VALIDATING && (expired || file_system_status != FILE_SYSTEM_STATUS_PERSISTENT)
               when FILE_SYSTEM_STATUS_REMOVED
                 end_state && file_system_status != FILE_SYSTEM_STATUS_PERSISTENT   # no check on reserved_resources on job level for now as this status is supposed to represent the actual file system state
             end
    return result
  end

  def set_file_system_status(new_file_system_status)
    self.file_system_status = new_file_system_status if allow_file_system_status_change?(new_file_system_status)
    return self.file_system_status == new_file_system_status
  end

  def available?
    file_system_status == FILE_SYSTEM_STATUS_AVAILABLE || file_system_status == FILE_SYSTEM_STATUS_PERSISTENT
  end

  def unavailable?
    !available?
  end

  def allow_mark_for_removal?
    ![FILE_SYSTEM_STATUS_MARKED_FOR_REMOVAL, FILE_SYSTEM_STATUS_REMOVED, FILE_SYSTEM_STATUS_PERSISTENT].include?(self.file_system_status)
  end

  def allow_unmark_for_removal?
    self.file_system_status == FILE_SYSTEM_STATUS_MARKED_FOR_REMOVAL
  end

  def shortest
    if self.filename.to_s == ""
      if self.path.to_s == ""
        return self.path_and_file
      else
        return self.path
      end
    else
      return self.filename
    end
  end
  
  def used_in_conversions
    result = Array.new
    Conversion.where("conversions.reference_numbers like '%#{self.conversion_id}'").each do |cv|
      if cv.data_files.to_s.scan(/#{self.path_and_file}/i)
        result << cv
      end  
    end
    return result
  end
  
  def test_requested?
    result = false
    TestRequest.where(test_db_conversion_databases_id:self.id).each do |t|
      if t.status == "CANCELED"
        result = false
        break
      else
        result = true
      end      
    end
    return result
  end

  def schema_version
    DhiveMetadata.set_db(path_and_file)
    version = DhiveMetadata.where("MetaTag = 'DH_VERSION'")
    return version.first.MetaValue
  end
  
  def store_version
    set_tag_list_on(:schema_version, schema_version)
    save
  end

  def self.update_internal_metadata
    cds = ConversionDatabase.where("db_type = 'dHive'")
    count = 0
    failed_count = 0
    cds.each do |database|
      begin
        database.store_metadata
        count = count + 1
      rescue => e
        puts e.message
        puts e.backtrace
        failed_count = failed_count + 1
      end
    end
  end

  def self.find_by_tagmodel(model)
    results = ConversionDatabase.available.order("created_at desc")
    model.each do |k,v|
      next if k.to_s == "coverage"
      next if k.to_s.match(/coverage_required$/)
      results = results.tagged_with(v, :on => "context_" + k.gsub(" ","_"))
    end
    
    return results
  end

  def self.find_by_tagmodel_and_coverage(results, model, coverage)
    ProductDesign.find_by_tagmodel_and_coverage(results, model, coverage)
  end

  def self.status_dbs_on_filesystem
    not_found = []
    should_be_removed = []
    can_be_removed = []
    valid = []

    Project.all.each do |pj|
      base_paths = []
      base_paths << File.join(ConfigParameter.get("conversion_job_result_dir", pj.id.to_s), "/**/o_*")
      base_paths << File.join(ConfigParameter.get("conversion_job_intermediate_dir", pj.id.to_s), "/**/o_*")
      base_paths << File.join(ConfigParameter.get("conversion_job_crash_dir", pj.id.to_s), "/**/o_*")

      base_paths.each do |base_path|
        Dir.glob(base_path).each do |x|
          cd = ConversionDatabase.where("path_and_file like '#{x}%'").first
          if !cd
            not_found << x
          elsif cd.removed_yn
            should_be_removed << cd
          elsif cd.can_be_removed_yn
            can_be_removed << cd
          else
            valid << cd
          end
        end
      end
    end
    return {:valid => valid,  :not_found => not_found, :should_be_removed => should_be_removed, :can_be_removed => can_be_removed}
  end

  def self.remove_all_obsolete!
    raise "NOT ALLOWED TO RUN ON DEV ENVIRONMENTS" if Rails.env == "development" || Rails.env == "test"
    results = status_dbs_on_filesystem
    results[:not_found].each do |x|
      begin
        remove_not_found(x)
      rescue => e
        puts e.message
      end
    end
    results[:should_be_removed].each do |x|
      begin
        x.remove
      rescue => e
        puts e.message
      end
    end
    results[:can_be_removed].each do |x|
      # only delete if order expired
      if x.conversion.production_orderline && x.conversion.production_orderline.production_order.bp_name == ProductionOrder::BP_NAME_DELETED
        begin
          x.remove
        rescue => e
          puts e.message
        end
      end
    end
  end

  def self.owned_by_webmis(path)
    uid = File.stat(path).uid
    if Etc.getpwuid(uid).name == ENV["prod_user"]
      return true
    else
      return false
    end
  end

  def self.remove_not_found(path)
    if !owned_by_webmis(path)
      raise "path #{path} not owned by webmis - not removed"
    end
    res = Server.ssh_to_server("localhost", "rm -rf #{path}")
  end

  def store_metadata
    metatags = []
    begin
      begin
        self.size_bytes = File.size(path_and_file)
        save!
      rescue => e
        metatags << "FILESIZE_FAILREASON"
        set_tag_list_on(:META_FAILREASON, e.message)
      end
        
      begin
        if self.conversion_job.tagmodel
          ProductDesign.store_tag_model(self,self.conversion_job.tagmodel)
          self.region = Region.find_by_region_code(self.conversion_job.tagmodel['coverage'].to_s)
          self.save!
        end
      rescue => e
        puts "Error storing tagmodel"
        puts e.message
        puts e.backtrace
      end


      # DhiveMetadata.set_db(path_and_file)
      # meta = DhiveMetadata.all
      # meta.each do |element|
      #   if element.MetaValue && element.MetaValue.strip != "" && element.MetaTag && element.MetaTag.strip != ""
      #     tag = "DHMETA_" + element.MetaTag.gsub(" ","_")
      #     metatags << tag
      #     set_tag_list_on(tag, element.MetaValue) if element.MetaTag != "DAKOTA_CONVERT" &&
      #       element.MetaTag != "STATFILE"
      #     save!
      #   end
      # end
      # set_tag_list_on(:internal_metatags, metatags)
    rescue => e
      metatags << "META_FAILREASON"
      self.reload
      set_tag_list_on(:META_FAILREASON, e.message)
      set_tag_list_on(:internal_metatags, metatags)
      save!
    end
  end
  
  def coverage
    self.region ? self.region.name : nil  
  end

  def data_supplier
    mytagmodel = self.tagmodel(['core'])['core']
    if mytagmodel && !mytagmodel.empty?
      core = mytagmodel.last.split('|')
      if core.size <= 1
        core = mytagmodel.last.split('-')
      end
      cabbr = CompanyAbbr.find_by_ms_abbr_3(core[1])
      if !cabbr
        cabbr = CompanyAbbr.find_by_company_name(core[1])
      end
      return cabbr.company_name if cabbr
    else
      company_name = self.conversion.data_release.company_abbr.company_name if self.conversion.data_release
      return company_name if company_name
    end
  end

  def data_release
    mytagmodel = self.tagmodel(['core'])['core']
    if mytagmodel && !mytagmodel.empty?
      core = mytagmodel.last.split('|')
      if core.size <= 1
        core = mytagmodel.last.split('-')
      end
      cabbr = CompanyAbbr.find_by_company_name(self.data_supplier)
      unless cabbr.nil?
        dr = DataRelease.where(:company_abbr_id => cabbr.id,:name => core[2])
        return dr.first.name if dr
      else
        raise "No CompanyAbbr found for '#{self.data_supplier}'."
      end
    else
      dr = self.conversion.data_release.name if self.conversion.data_release
      return dr if dr
    end    
  end

  def tagmodel(context_group=[])
    org_model = ProductDesign.tagmodel(self,context_group)
    # confusing for users with IQ >= 100, so remove from model
    org_model.delete_if {|k, v| k.match(/.*coverage_required/) }
  end

  def coverage_label
    ProductDesign.coverage_label_by_tagmodel(self)
  end

  def filling_and_core
    # used by the assembly editor to show the filling/datatype of a database
    model = self.tagmodel
    results = []
    if model['filling']
      model['filling'].each do |fil|
        results << fil
        if fil == "core" && model[fil]
          results << "(#{model[fil].to_s.split(/[\-|]/)[0]})"
        end
      end
    end
    return results
  end

  def nds_size_statistics_hash
    rtrn_hash = Hash.new
    if !self.path.blank? && Dir["#{self.path}"] && self.db_type == 'NDS' && self.path_and_file.match(/(iso|zip|NDS)\z/)
      if self.path_and_file.match(/\.zip\z/ix)
        fsresult = `zipinfo #{self.path_and_file} | grep ".NDS" | cut -d ' ' -f 5-1000`
        # 16384 bx defN 13-Oct-30 09:02 maps/EEC/EEC_WLD/OVERALL.NDS
      elsif self.path_and_file.match(/\.NDS\z/ix)
        fsresult = `find #{self.path} -name "*.NDS" | xargs du -b`
        # 106315776 /data/output/nds/prod/nok/eur/eur/eur_nok_12Q4_20130221d/PRODUCT/E5/SPEECH.NDS
      elsif self.path_and_file.match(/\.iso\z/ix)
        region = ''
        fsresult = ''
        fs_sub_result = `isoinfo -l -i #{self.path_and_file}`

        if !fs_sub_result.to_s.blank?
          fs_sub_result.each_line do |l|
            region = l if l.match('Directory')
            if l.match(/\.NDS/)
              fsresult += l.scan(/(\d{2,})/).first.join('') + ' ' + "#{region.strip + l.scan(/\w*.\.NDS/).join('').strip}".scan(/\/.*/).join('') + "\n"
              # 278528 /UPDATE1/UR5/PRODUCT/E4/ROUTING.NDS
            end
          end
        end
      end

      fsresult.split("\n").each do |result|
        if result.match(/\//)
          building_block = result.split('/').last.gsub('.NDS','')
          region = result.split('/')[(result.split('/').size - 2)]
          size = result.strip.match(/\A\d*/)[0]
          
          rtrn_hash[region] = {} if  rtrn_hash[region].nil? && !region.blank?
          rtrn_hash[region][building_block] = size if !building_block.blank? && !size.blank? && !rtrn_hash[region].nil?
        end
      end
    end
    return rtrn_hash
  end

  def nds_size_statistics
    stats = self.nds_size_statistics_hash
    if stats.empty?
      self.conversion.source_conversion_databases.each do |db|
        source_stats = db.nds_size_statistics_hash
        if !source_stats.empty?
          source_stats["conversion_database_id"] = db.id
          return source_stats
        end
      end
    else
      stats["conversion_database_id"] = self.id
      return stats
    end
    return stats # empty hash
  end

  def self.cleanup
    # used for temporary cleanup
    #nolog
    #reserved_resources = ConversionJob.reserved_resources
    #ConversionDatabase.present.where("id > 23177 and id < 24600").each do |db|
    #  db.remove(reserved_resources)
    #end
  end

  def remove_all_marked_for_removal!
    dbs = ConversionDatabase.where("can_be_removed_yn = true and removed_yn = false")
    remove_multiple(dbs)
  end

  def self.remove_multiple(dbs)
    reserved_resources = ConversionJob.reserved_resources
    all_removed = true
    dbs.each do |db|
      begin
        db.remove(reserved_resources)
      rescue => e
        db.removal_remark = e.message
        all_removed = false
        db.save!
      end
    end
    return all_removed
  end

  def remove(reserved_resources=nil)
    # for now only to be used from console because of security concerns
    if !ConversionDatabase.owned_by_webmis(self.path_and_file)
      raise "database path #{self.path_and_file} not owned by webmis - not removed"
    end

    if self.path_and_file && self.path_and_file.to_s.size > 5 # preemptive check
      if !self.conversion_job
        raise "Conversiondatabase #{self.id} NOT DELETED because it's manually generated"
        return
      end
      unless self.allow_file_system_status_change?(FILE_SYSTEM_STATUS_REMOVED)
        raise "Conversiondatabase #{self.id} with status #{self.status_str} NOT DELETED because the file_system change from #{self.file_system_status_str} to #{@@file_system_status[FILE_SYSTEM_STATUS_REMOVED]}is not allowed"
        return
      end
      if File.directory?(path_and_file.to_s)
        cds = ConversionDatabase.where("id <> #{self.id} and path_and_file like '#{self.path_and_file}%'").available
        if cds.size > 0
          raise "Conversiondatabase #{self.id} DIR (#{self.path_and_file}) NOT removed, #{cds.size.to_s} other databases found in for same dir"
        else
          if ConfigParameter.get("conversion_job_result_dir",self.project_id.to_s).size > self.path_and_file.size - 10
            raise "Short result directory #{self.id.to_s} / #{self.path_and_file} NOT removed"
          else
            index = nil
            index = reserved_resources.index{|x|x["file_path"] == self.path_and_file} if reserved_resources
            if index
              raise "Resource reserved #{self.id.to_s} / #{self.path_and_file} NOT removed "
            else
              result = Server.ssh_to_server("localhost", "chmod -R 777 #{self.path_and_file} && chgrp -R conversion #{self.path_and_file} && rm -rf #{self.path_and_file}")
              self.file_system_status = FILE_SYSTEM_STATUS_REMOVED
              self.removal_remark = "REMOVED BY SYSTEM"
              self.save!
            end
          end
        end
      elsif File.file?(self.path_and_file.to_s)
        # its a file
        # check if other conversion databases in directory
        dir = File.dirname("#{self.path_and_file}")
        cds = ConversionDatabase.where("id <> #{self.id} and path_and_file like '#{dir}%'").available
        if cds.size > 0
          s = Server.ssh_to_server("localhost", "rm #{self.path_and_file}")
          self.file_system_status = FILE_SYSTEM_STATUS_REMOVED
          self.save!
        else
          if ConfigParameter.get("conversion_job_result_dir",self.project_id.to_s).size > dir.size - 10
            raise "Short result directory #{self.id.to_s} / #{dir} not removed"
          else
            index = nil
            index = reserved_resources.index{|x|x["file_path"] == self.path_and_file} if reserved_resources
            if index
              raise "Resource reserved for conversion: #{self.id.to_s} / #{self.path_and_file} not removed "
            else
              mes =  "No other Conversiondatabases found in dir - removing FULL DIR #{self.id} #{dir}"
              s = Server.ssh_to_server("localhost", "chmod -R 777 #{dir} && chgrp -R conversion #{dir} && rm -rf #{dir}")
              self.file_system_status = FILE_SYSTEM_STATUS_REMOVED
              self.removal_remark = mes
              self.save!
            end
          end
        end
      else
        raise "UNKNOWN RESULT  #{self.id.to_s} / #{self.path_and_file} not removed (resource probably deleted outside system)"
      end
    else
      self.removal_remark =  "Empty path/file:#{self.id.to_s} - #{self.path_and_file.to_s} - flagging conversion database as removed"
      self.file_system_status = FILE_SYSTEM_STATUS_REMOVED
      self.save!
    end
  end

  def handle_automatic_validation
    ConversionDatabase.transaction do
      # return unless the conversion database can go the validating state
      return unless allow_status_change?(STATUS_VALIDATING)

      data_type = DataType.find_by_name(self.db_type)
      # return unless we have a data_type for which automatic validation is activated.
      unless data_type && data_type.latest_approved && data_type.latest_approved.automatic_validation_yn
        self.set_status(STATUS_NOT_VALIDATED)
        self.save!
        return
      end

      # use the latest approved version
      dt = data_type.latest_approved

      # create ValidationJob
      job = ValidationJob.new
      job.project_id = self.project_id
      job.job_script = JobScript.new()
      job.job_script.set_type(JobScript::TYPE_DAKOTA)
      job.save!
      self.set_status(STATUS_VALIDATING)
      self.validation_jobs << job
      self.save!

      job.job_script.conversion_id = self.id.to_s
      job.job_script.production_orderline_id = self.production_orderline_id.to_s if self.production_orderline_id
      job.job_script.product_id = self.production_orderline.product_id.to_s if self.production_orderline && self.production_orderline.product_id

      job.job_script.main_executable = dt.validation_executable
      tag_list_str = self.filename.split(".")[0]

      if self.conversion_job && self.conversion_job.job_script
        # the database was created by a generated conversion job
        # use the same bundle for the validation as for the conversion
        job.job_script.conversiontool_code = self.conversion_job.job_script.conversiontool_code
        job.job_script.cvtool_bundle_name = self.conversion_job.job_script.cvtool_bundle_name
      else
        # don't know with what bundle the database was actually created
        # assume the bundle specified in the conversion is correct
        job.job_script.conversiontool_code = self.conversion.conversiontool.code
        job.job_script.cvtool_bundle_name = self.conversion.cvtool_bundle.name.to_s
      end
      job.job_script.tag_list_string = tag_list_str
      job.job_script.data << ["1", self.path_and_file]
      job.schedule_manual = dt.schedule_manual || false
      job.log_action("Schedule only manual due to setup of executable #{dt.name}.") if job.schedule_manual
      job.schedule_cores = dt.schedule_cores || 1
      filesize_mb = (self.size_bytes.to_f / 2**20).round(0).to_i  # filesize in MB, rounded
      schedule_memory_base = dt.schedule_memory_base || 0
      schedule_memory_times_input = dt.schedule_memory_times_input || 0
      job.schedule_memory = schedule_memory_base + (schedule_memory_times_input * filesize_mb)
      schedule_diskspace_base = dt.schedule_diskspace_base || 0
      schedule_diskspace_times_input = dt.schedule_diskspace_times_input || 0
      job.schedule_diskspace = schedule_diskspace_base + (schedule_diskspace_times_input * filesize_mb)
      job.schedule_allow_virtual = dt.schedule_allow_virtual || false
      job.schedule_exclusive = dt.schedule_exclusive || false
      job.working_dir = "w_################-" + tag_list_str

      # Copy available context-tags of original conversion_job
      if self.conversion_job
        conversion_job = self.conversion_job
        job.set_tag_list_on(:context_coverage, conversion_job.tag_list_on(:context_coverage)) if !conversion_job.tag_list_on(:context_coverage).empty?
        job.set_tag_list_on(:context_subregion, conversion_job.tag_list_on(:context_subregion)) if !conversion_job.tag_list_on(:context_subregion).empty?
        job.set_tag_list_on(:context_subregion, conversion_job.tag_list_on(:context_subregion)) if !conversion_job.tag_list_on(:context_subregion).empty?
        job.set_tag_list_on(:context_labelsupplier, conversion_job.tag_list_on(:context_labelsupplier)) if !conversion_job.tag_list_on(:context_labelsupplier).empty?
        job.set_tag_list_on(:context_labelrelease, conversion_job.tag_list_on(:context_labelrelease)) if !conversion_job.tag_list_on(:context_labelrelease).empty?
      end
      job.save!
      # queue validation job
      job.queue
      return job
    end
  end

  def copy_to_crashdir(ticket_nr)
    src = self.path
    dest = File.join(ConfigParameter.get("conversion_job_crash_dir",self.project_id.to_s), ticket_nr.to_s)
    Delayed::Worker.logger.debug("Conversion database #{self.id.to_s} Rsync copy start " + src + " to " + dest)
    rsynccmd = "mkdir -p #{dest} && rsync --partial -arv #{src} #{dest} 2>&1; echo exitcode:$?"
    rres = Server.ssh_localhost(rsynccmd).chomp
    Delayed::Worker.logger.debug(rres)
    exitcode = rres.scan(/exitcode:(.*)/).flatten[0]
    if exitcode.to_i != 0
      resultstr = "Conversion database #{self.id.to_s} FAILURE: Rsync copy FAILED " + src + " to " + dest + " at " + DateTime.now.to_s
      Delayed::Worker.logger.error(resultstr)
      raise resultstr
    else
      Delayed::Worker.logger.info("Conversion database ${self.id.to_s} SUCCESS: Rsync copy completed " + src + " to " + dest + " at " + DateTime.now.to_s)
    end
    # there is no po post_method
  end

  def detail_hash
    h = Hash.new
    h['id'] = self.id
    h['status'] = self.status_str
    h['path'] = self.path_and_file
    h['data_supplier'] = self.data_supplier
    h['data_release']  = self.data_release
    h['coverage'] =      self.coverage
    h['bundle_release'] = self.conversion.conversion_tool_release
    h['conversion_id'] = self.conversion.id
    h['data_type'] = self.db_type
    h['conversion_job_id'] = self.conversion_job_id
    h['product_id'] = self.product_id

    h['conversion_script'] = ''
    h['conversion_script'] = self.conversion_job.job_script.to_script("CONVERSION_JOB_ID", self.conversion_job.id) if self.conversion_job
    h['removed'] = (self.file_system_status == FILE_SYSTEM_STATUS_REMOVED).to_s
    h['removal_candidate'] = (self.file_system_status == FILE_SYSTEM_STATUS_MARKED_FOR_REMOVAL).to_s
    return h
  end

  def handle_validation_error_accepted(error_name, user)
    self.remarks += "\nValidation error '#{error_name}' accepted by '#{user.full_name}'."
    if self.validation_errors.unresolved.empty? && self.set_status(STATUS_ACCEPTED)
      self.remarks += "\nStatus conversion database set to #{self.status_str}."
    end
    self.save!
  end

  def handle_validation_error_rejected(error_name, user)
    self.remarks += "\nValidation error '#{error_name}' rejected by '#{user.full_name}'."
    if self.set_status(STATUS_REJECTED)
      self.mark_for_removal(user)
    end
    self.save!
  end

  def handle_validation_error_investigation(error_name, user)
    # mark the database for removal but don't change the status just yet
    self.remarks += "\nValidation error '#{error_name}' rejected by '#{user.full_name}'."
    self.mark_for_removal(user)
    self.save!
  end

  def mark_for_removal(user = nil, remarks = nil)
    return if [FILE_SYSTEM_STATUS_MARKED_FOR_REMOVAL, FILE_SYSTEM_STATUS_REMOVED].include?(self.file_system_status)
    if set_file_system_status(FILE_SYSTEM_STATUS_MARKED_FOR_REMOVAL)
      user_name = user ? user.user_name : ''
      remarks ||= "#{Date.today.strftime("%d-%m-%Y")} #{user_name}: Marked for removal."
      self.remarks = "#{self.remarks}\n#{remarks}"
    else
      raise "can't mark '#{self.file_system_status_str}' conversion database #{self.id} #{self.shortest} with status '#{self.status_str}' for removal."
    end
  end

  def unmark_for_removal(user = nil)
    return unless self.file_system_status == FILE_SYSTEM_STATUS_MARKED_FOR_REMOVAL

    if set_file_system_status(FILE_SYSTEM_STATUS_AVAILABLE)
      user_name = user ? user.user_name : ''
      self.remarks ||= ""
      self.remarks += "\n#{Date.today.strftime('%d-%m-%Y')} #{user_name}: Unmarked for removal."
    else
      raise "Can't unmark '#{self.file_system_status_str}' conversion database #{self.id} #{self.shortest} with status '#{self.status_str}' for removal."
    end
  end

  def used_data_sets
    return_hash = Hash.new{|h,k| h[k]=Hash.new(&h.default_proc) }
    if self.conversion_job
      self.conversion_job.conversion_job_process_data_elements.each do |job_element|
        element = job_element.process_data_element
        data_set = element.data_set
        
        return_hash["data_set_#{data_set.id}"]["id"] = data_set.id
        return_hash["data_set_#{data_set.id}"]["version"] = job_element.data_set_version
        return_hash["data_set_#{data_set.id}"]["name"] = data_set.name
        #return_hash["data_set_#{data_set.id}"]["items"]["data_set_#{data_set.id}"]["id"] = data_set.id
        return_hash["data_set_#{data_set.id}"]["elements"]["process_data_element_#{element.id}"]["id"] = element.id
      end
    end
    return return_hash
  end

  def toggle_persistence(persistent = nil)
    return if self.unavailable?  # you can only toggle between available and persistent file_system_status
    persistent = self.file_system_status != FILE_SYSTEM_STATUS_PERSISTENT  if persistent.nil?
    if persistent
      raise "Could not set conversion database #{self.id} #{self.shortest} to #{FILE_SYSTEM_STATUS_PERSISTENT}" unless set_file_system_status(FILE_SYSTEM_STATUS_PERSISTENT)
    elsif !set_file_system_status(FILE_SYSTEM_STATUS_AVAILABLE) &&
      !set_file_system_status(FILE_SYSTEM_STATUS_MARKED_FOR_REMOVAL)  # Available not a valid new file_system_status, use mark for removal instead
      raise "Could not remove the #{FILE_SYSTEM_STATUS_PERSISTENT} flag from conversion database #{self.id} #{self.shortest}"
    end
    self.save!
  end

  def cancel(user = 'system')
    ConversionDatabase.transaction do
      if set_status(STATUS_CANCELLED)
        set_file_system_status(FILE_SYSTEM_STATUS_MARKED_FOR_REMOVAL)
      end
      self.validation_jobs.each{ |j| j.cancel(user) }
      save!
    end
  end

  def terminate(user = 'system')
    ConversionDatabase.transaction do
      set_status(STATUS_CANCELLED)
      set_file_system_status(FILE_SYSTEM_STATUS_MARKED_FOR_REMOVAL) unless self.file_system_status == FILE_SYSTEM_STATUS_PERSISTENT
      self.validation_jobs.each{ |j| j.terminate(user) }
      save!
    end
  end

  def self.find_by_process_data_item(data_set_id,data_set_version=nil)
    conversion_databases = []
    job_ids = []
    DataSet.find(data_set_id).process_data_elements.each do |el|
      if data_set_version
        job_ids = el.conversion_job_process_data_elements.where(:data_set_version => data_set_version).collect{|job_element|job_element.conversion_job.id}
      else
        job_ids = el.conversion_job_process_data_elements.collect{|job_element|job_element.conversion_job.id}
      end
    end

    ConversionJob.find(job_ids).each do |job|
      if !job.conversion_databases.empty?
        conversion_databases = conversion_databases + job.conversion_databases
      end
    end
    return conversion_databases
  end

  def project_id
    return self.conversion_job ? self.conversion_job.project_id : Project.default.id
  end

  def output_directory_path
    if !self.path_and_file.blank?
      expr = '.*\/o_\d{8}.*\/'
      if match = self.path_and_file.match(/#{expr}/)
        return match[0]
      end
    end
  end

  def self.manual_removal_script(ids)
    content = "#!/bin/sh\n"
    content = content + "# Removal script generated by WebMIS\n"
    content = content + "# Please check lines before execute!\n"
    content = content + "# " + Time.now.to_s + "\n\n"
    conversion_databases = ConversionDatabase.find(ids)
    conversion_databases.each do |db|
      content = content + "# conversion-id-#{db.conversion_id} conversion-db-#{db.id}\n"
      if db.output_directory_path
        content = content + "rm -rf #{db.output_directory_path.strip}\n"
      else
        content = content + "# WARNING: COULD NOT FIND O_DIR FOR rm -rf #{db.path_and_file.strip}\n"
      end
    end
    return content
  end

  def suitable_reference_database
    match= nil
    #manual order case
    base_query_string =  " conversions.input_format = '#{self.conversion.input_format}' AND "
    base_query_string += " conversions.output_format = '#{self.conversion.output_format}' AND "
    base_query_string += " (conversion_databases.status = #{STATUS_ACCEPTED} "
    base_query_string += "  or conversion_databases.status = #{STATUS_NOT_VALIDATED}) AND "
    base_query_string += " conversion_databases.file_system_status != #{FILE_SYSTEM_STATUS_REMOVED} AND "
    base_query_string += " conversions.status = #{Conversion::STATUS_FINISHED} AND "
    base_query_string += " conversion_databases.file_system_status < #{FILE_SYSTEM_STATUS_MARKED_FOR_REMOVAL} AND "
    base_query_string += " conversion_databases.id <> #{self.id} "

    if !self.conversion.productid
      #half product
      #find same dhive with different release
      tagmodel = self.tagmodel
      if tagmodel["filling"].index("core")
        use_filling = "core"
      else
        use_filling = tagmodel["filling"][0]
      end
      core_data_tags = tagmodel[use_filling][0]
      release = core_data_tags.split("|")[-1]
      dr = DataRelease.find_by_name(release)
      if dr.previous_data_release
        prev_name = dr.previous_data_release.name.downcase.to_s
        core_data_tags.gsub!(release,prev_name)
      else
        #no previous release of source data - just try to find ref db for current release
      end

      tagmodel[use_filling][0] = core_data_tags
      match = ConversionDatabase.includes(:conversion)
      match.where(base_query_string).order("id desc")

      match = ConversionDatabase.find_by_tagmodel_and_coverage(match, tagmodel, tagmodel["coverage"])
    else
      #check if previous manual conversion available
      match = ConversionDatabase.includes(:conversion)
      query_string = base_query_string + " AND conversions.production_orderline_id = #{self.conversion.production_orderline_id}"
      match = match.where(query_string).order("id desc").limit(10)
    end
    
    if match.size == 0
      if self.conversion.productid.blank?
        raise "Cannot find suitable ref database for conversion database #{self.id} #{self.tagmodel}"
      end
      #look at predecessor
      match = ConversionDatabase.includes(:conversion)
      prev_prod = Product.where("volumeid = '#{self.conversion.productid}'").first.predecessor

      if prev_prod
        prev_prodid = prev_prod.volumeid
      else
        raise "Predecessor product not found for product #{self.conversion.productid}"
      end

      query_string = base_query_string + " AND conversions.productid = '#{prev_prodid}'"
      match = match.where(query_string).order("id desc")

    end

    return match.first
  end

  def create_test_request
    ConversionDatabase.transaction do
      testplan = TestPlan.default
      if !self.conversion.productid.blank?
        p = Product.find_by_volumeid(self.conversion.productid)
        tpl = p.product_line
        tp = TestPlan.find_by_product_line_id(tpl.id) if tpl
        if tp
          testplan = tp
        end
      end

      @test_request = TestRequest.new
      @test_request.test_plan_id = testplan.id
      @test_request.project_id = self.conversion.project_id
      @test_request.production_orderline_id = self.conversion.production_orderline_id
      @test_request.test_type_id = TestType.find_by_name("Data Validation").id
      @test_request.requested_by_user_id = self.conversion.request_user_id
      @test_request.assigned_to_user_id = self.conversion.request_user_id
      @test_request.test_db_conversion_databases_id = self.id
      @test_request.status = 'REQUESTED'
      @test_request.product_id = self.conversion.productid
      @test_request.start_date = DateTime.now
      @test_request.finish_date = nil
      @test_request.actual_start_date = nil
      @test_request.actual_finish_date = nil
      @test_request.request_remarks = "Auto generated from conversion #{self.conversion.id}"
      @test_request.result_remarks = ''
      @test_request.removed_yn = false
      @test_request.test_plan_id  = testplan.id
      @test_request.run_parallel  = false
      @test_request.use_scheduler = false
      @test_request.run_locally   = nil
      @test_request.test_tool_release_id = nil

      if @test_request.test_plan_id
        @test_request.run_parallel = true
        @test_request.use_scheduler = true
      end
      refid = suitable_reference_database.id
      @test_request.create_test_ref_databases([self.id],'test')
      @test_request.create_test_ref_databases([refid],'ref')
      @test_request.ref_db_conversion_databases_id = refid
      @test_request.generate_jobs
      @test_request.save!
    end
  end

  def self.create_from_manual_conversion_request(conversion_obj,result_db)
    n = ConversionDatabase.new()
    n.conversion_id = conversion_obj.id
    n.status = STATUS_NOT_VALIDATED
    n.db_type = conversion_obj.output_format
    n.path_and_file = result_db.html_safe
    n.filename = result_db.split('/').last
    n.path = result_db.gsub(n.filename,'')
    n.product_id = conversion_obj.productid.to_s
    n.size_bytes = conversion_obj.size_bytes
    n.region = conversion_obj.region
    n.set_file_system_status(FILE_SYSTEM_STATUS_PERSISTENT) if conversion_obj.outcome_persistent_yn
    n.save!
    n.store_metadata if conversion_obj.output_format == "dHive"
  end

  def remove_from_manual_conversion_request
    self.remarks = "replaced-with-conversion-request-update."
    self.set_file_system_status(FILE_SYSTEM_STATUS_REMOVED)
    self.save!
  end

  handle_asynchronously :copy_to_crashdir
end
