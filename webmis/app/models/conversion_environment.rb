class ConversionEnvironment < ActiveRecord::Base
  validates :name, :presence => true
  has_many :conversion_jobs
  has_many :conversions

  scope :active, -> { where(:active_yn => true) }

  def self.collection_for_select
    ConversionEnvironment.active.order('name DESC').collect{|cve|[cve.name,cve.id]}.insert(0,['Latest (default)',nil])
  end
end
