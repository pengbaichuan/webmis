class ConversionJob < Job
  acts_as_ordered_taggable_on :tags
  belongs_to :conversion_environment
  belongs_to :conversion
  has_one :production_orderline, :through => :conversion
  has_one :production_order, :through => :production_orderline
  has_many :conversion_databases
  has_many :conversion_job_process_data_elements, :dependent => :destroy
  has_many :process_data_elements, :through => :conversion_job_process_data_elements

  comma :tagged_failed_jobs_report do
    type 'Job type'
    fail_category 'Category'
    fail_category_logged_by_user :full_name => 'Tagged by'
    fail_category_logged_on 'Date tagged'
    bug_tracker_id
    fail_reason
    start_time
    finish_time
    conversion :id => 'Conversion'
    run_server
    action_log
    id
    coverage_label "Coverage"
    conversion "Data type" do |conversion|conversion.output_format end
    conversion "Conversion tool" do |conversion|conversion.conversiontool.code end
    conversion "Conversion bundle" do |conversion|conversion.cvtool_bundle.name end
    conversion "Data release" do |conversion|conversion.data_release.name if conversion.data_release end
    conversion "Supplier" do |conversion|conversion.data_release.company_abbr.company_name if conversion.data_release end
    production_order :id => "Production order id"
    production_order :name => "Production order name"
    production_orderline "Product ID" do |production_orderline|production_orderline.product.volumeid if production_orderline.product end
    production_orderline "Product Name" do |production_orderline|production_orderline.product.name if production_orderline.product end
  end

  def product
    conversion.production_orderline.product
  end

  alias_method :old_set_status, :set_status
  def set_status(new_status)
    result = old_set_status(new_status)
    if result && new_status == STATUS_FAILED
      Postoffice.conversion_job_status_change(self).deliver_now
    end
    result
  end


  def set_status_when_move_failed(new_status)
    old_set_status(new_status)
    # always send the email as the problem might have been caused by a job update race condition
    # we just cannot be sure there is no garbage left on the file system in that case
    Postoffice.conversion_job_file_move_failed(self.id).deliver
  end

  def duplicate(user=nil)
    ConversionJob.transaction do 
      conversion_job_clone = self.dup
      conversion_job_clone.original_job_id = self.id
      conversion_job_clone.run_command = nil
      conversion_job_clone.exitcode = nil
      conversion_job_clone.fail_reason = nil
      conversion_job_clone.run_server = nil
      conversion_job_clone.run_pid = nil
      conversion_job_clone.stdout_log = ''
      conversion_job_clone.start_time = nil
      conversion_job_clone.finish_time = nil
      conversion_job_clone.status = STATUS_PLANNED
      conversion_job_clone.lock_version = 1
      conversion_job_clone.file_system_status = FILE_SYSTEM_STATUS_FREE
      conversion_job_clone.action_log = nil
      conversion_job_clone.scheduler_log = nil
      conversion_job_clone.fail_category = nil
      conversion_job_clone.fail_category_logged_by_user_id = nil
      conversion_job_clone.fail_category_logged_on = nil
      conversion_job_clone.result_dir_size_bytes = nil
      conversion_job_clone.working_dir = self.working_dir.gsub!(/^w_.*-/, "w_################-")
      conversion_job_clone.version = nil
      ProductDesign.store_tag_model(conversion_job_clone, self.tagmodel)
      conversion_job_clone.log_action("Cloned from conversion job #{self.id}",user)
      if conversion_job_clone.save!
        self.conversion_job_process_data_elements.each do |cjpde|
          cjpde_clone = cjpde.dup
          cjpde_clone.conversion_job_id = conversion_job_clone.id
          cjpde_clone.save!
        end
      end
      conversion_job_clone.reload
      conversion_job_clone.tag_list = self.tag_list

      # dirty workaround, the following two fields are not copied for some obscure reason
      # TODO: find out why
      conversion_job_clone.set_tag_list_on(:context_labelrelease, self.tag_list_on(:context_labelrelease))
      conversion_job_clone.set_tag_list_on(:context_labelsupplier, self.tag_list_on(:context_labelsupplier))
      
      return conversion_job_clone
    end
  end

  def regenerate(current_user, conversiontool_id, cvtool_bundle_id, manual_yn = false)
    return unless conversion.running?

    ConversionJob.transaction do
      job_cancel(current_user, rescheduled_yn = true)  # will do nothing if job is already in an end-status
      self.conversion_databases.each{ |cd| cd.terminate(current_user) }

      conversion_job_clone = self.duplicate
      if conversion_job_clone.job_script
        conversion_job_clone.job_script.updated_by = current_user.user_name
        conversion_job_clone.job_script.cvtool_bundle_name = CvtoolBundle.find(cvtool_bundle_id).name
        conversion_job_clone.job_script.conversiontool_code = Conversiontool.find(conversiontool_id).code
      else
        # script not generated, check whether the tool bundle was changed.
        unless self.conversion && self.conversion.cvtool_bundle_id == cvtool_bundle_id && self.conversion.conversiontool_id = conversiontool_id
          # chosen tool bundle differs from the conversion one, as we can't generate the script
          # just put the job on manual for now and leave it to the user to adapt the script.
          manual_yn = true
          conversion_job_clone.log_action("Tool bundle changed, job must be edited manually before proceeding", current_user)
        end
      end

      if manual_yn
        conversion_job_clone.set_status(STATUS_MANUAL)
      else
       conversion_job_clone.queue
      end
      conversion_job_clone.save!

      return conversion_job_clone
    end
  end

  def simulate_dakota?
    conversion.production_orderline.production_order.use_dakota_simulator_yn
  end

  def dakota_simulator_path
     File.expand_path("#{ConfigParameter.get("dakota_simulator_dir",self.project_id.to_s)}/daksim")
  end

  def perform 
    begin
      return unless allow_status_change?(STATUS_PROCESSING) && self.file_system_status == FILE_SYSTEM_STATUS_FREE
      clear_scripts
      cmd = simulate_dakota? ? dakota_simulator_path : 'daklite'
      daklite_opts = self.conversion_environment ? "-r #{self.conversion_environment.name}" : ''
      cid= File.expand_path("#{ConfigParameter.get("dakota_scripts_dir",self.project_id.to_s)}/dakota_#{id.to_s}")
      self.start_time = DateTime.now
      self.finish_time = nil
      self.run_command = "#{cmd} -f #{cid} #{daklite_opts}".strip
      self.stdout_log = ""
      self.fail_reason = ""
      self.exitcode = nil
      save!
       
      working_dir_prefix = Time.now.strftime("%Y%m%d-%H%M%S") + "-#{self.id}"
      if self.job_script
        self.job_script.working_dir_prefix = working_dir_prefix
        running_script = self.job_script.to_script("CONVERSION_JOB_ID", self.id)
        self.working_dir = "w_#{working_dir_prefix}-#{self.job_script.tag_list_string}"
      else
        # legacy method of handling the script, still in place so the old conversion jobs
        # don't break. Can be removed at a later date when these jobs are no longer relevant
        running_script = "mapinfo CONVERSION_JOB_ID \"#{self.id}\""+ "\n" + self.run_script
        running_script = "strict DAKOTA_PREFIX_WORKDIR " + working_dir_prefix + "\n" + running_script
        running_script = "# AUTO-GENERATED DAKOTA SCRIPT - EXPORTED BY WEBMIS\n" + running_script
        self.working_dir = self.working_dir.gsub(/^w_.*-/,"w_" + working_dir_prefix + "-")
      end
      File.open(cid, 'w') {|f| f.write(running_script.to_s.gsub("--->","")) }

      #write execscript
      s = "#! /bin/bash\r\n"
      s = s + "echo PID: $$ > #{cid}.log || exit\n"
      s = s + "export MY_DAKOTAMON_SERVER=" + '"' + "http://staging.dakotamon" + '"' + "\n" unless Rails.env == 'production'
      s = s + "export PROCESSID=$$\n"
      s = s + "#{self.run_command} >> #{cid}.log\n"
      s = s + "echo $? >  #{cid.to_s}.done\n"
      File.open(cid + ".exec", 'w') {|f| f.write(s) }
      execstring = "source /data/users/#{ENV['prod_user']}/.profile;nohup sh #{cid}.exec > #{cid.to_s}.log 2>&1 &"
      ssh(execstring)
      # get pid
      pidexec= "cat  #{cid}.log | grep '^PID' | cut -d' ' -f2"
      pid= ssh(pidexec).chomp

      begin
        self.run_pid = pid
        self.set_status(STATUS_PROCESSING)
        self.set_file_system_status(FILE_SYSTEM_STATUS_ON_SERVER)
        self.log_action("Started #{self.status_str} conversion job.")
        save!
      rescue ActiveRecord::StaleObjectError => e
        # kill needed
        kill_linux_process(pid)
        clear_working_dir
        clear_scripts
        puts "Failed setting status to PROCESSING for job " + self.id.to_s + " .. killing stated linux process " + childpid.to_s + " on " + self.run_server.to_s
        puts e.message
        puts e.backtrace
      end
    rescue Exception => e
      puts e.message
      puts e.backtrace
      self.stdout_log = stdout_log + "------------\r\n" + e.message + "\r\n======\r\n" + e.backtrace.join("\r\n")
      self.fail_reason = e.message + "\r\n======\r\n" + e.backtrace.join("\r\n")
      ############################ cd.process_location = working_dir
      clear_scripts
      self.set_status(STATUS_FAILED)
      self.set_file_system_status(FILE_SYSTEM_STATUS_FREE)
      self.log_action("Conversion job #{self.status_str}. #{e.message}")
    ensure
      save!
    end
  end

  def automonitor
    return unless status == STATUS_PROCESSING || file_system_status == FILE_SYSTEM_STATUS_MOVING
    # we currently just check whether the .done file exists
    cid = File.expand_path("#{ConfigParameter.get("dakota_scripts_dir",self.project_id.to_s)}/dakota_#{id.to_s}")
    final_dir = self.db_result_dir


    return unless File.file?("#{cid.to_s}.done") # conversion process is still running

    begin
      Job.transaction do
        if self.file_system_status == FILE_SYSTEM_STATUS_ON_SERVER
          self.result_dir_size_bytes = get_dir_size(locate_output_dir)
          self.set_file_system_status(FILE_SYSTEM_STATUS_RESERVED, final_dir)
          self.save!
        end
      end
    rescue => e
      self.reload
      self.fail_reason = e.message
      self.save!
      raise e
    end

    begin
      suffix = ""
      contents = File.read("#{cid.to_s}.done")
      standardoutlog = File.read("#{cid.to_s}.log") if File.exists?("#{cid.to_s}.log")
      taillog = ""
      if standardoutlog.size > 10000
        taillog = standardoutlog[standardoutlog.size-10000..-1]
      else
        taillog = standardoutlog
      end
      
      self.stdout_log = taillog
      m = standardoutlog.match(/The new working dir is: (.*)/)
      source_path = m[1].gsub(/\/w_/, "/o_") if m
      if contents.to_i == 0
        # success
        if self.allow_file_system_status_change?(FILE_SYSTEM_STATUS_MOVING)
          output_dir = self.working_dir.gsub(/^w_/, "o_")
          suffix = output_dir.scan(/^(o_.*)$/).flatten.join("")
          ConversionJob.transaction do
            self.exitcode = 0
            raise "Invalid directory suffix for conversion job " + self.id.to_s if !suffix || suffix.size == 0
            self.stdout_log =  stdout_log.to_s + "MOVING FILES FROM " + source_path.to_s + " TO FINAL DIR " + final_dir.to_s + "\r\n"
            self.set_file_system_status(FILE_SYSTEM_STATUS_MOVING, final_dir)
            self.log_action("#{self.file_system_status_str} output to #{final_dir}.")
            move("#{cid.to_s}.log", File.join(final_dir, suffix))
            # move script files
            self.stored_scriptfiles.each do |script|
              move(script, File.join(final_dir, suffix))
            end
            save! # if stale object exception ->
          end
          rsyncmove(source_path, File.join(final_dir,suffix), "post_move")
        end
      else
        self.exitcode = contents.to_i
        self.stdout_log = taillog
        self.finish_time = DateTime.now
        if self.locate_working_dir.blank?
          fail_msg = "Dakota scripts failed, no working directory found."
          self.set_file_system_status(FILE_SYSTEM_STATUS_FREE)
        else
          fail_msg = "Dakota scripts failed."
        end
        self.set_status(STATUS_FAILED)
        self.fail_reason = fail_msg
        self.log_action("Job #{self.status_str}. " + fail_msg)
      end
    rescue Exception => e
      puts e.message
      puts e.backtrace
      self.stdout_log = stdout_log + "------------\r\n" + e.message + "\r\n======\r\n" + e.backtrace.join("\r\n")
      self.fail_reason = e.message + "\r\n======\r\n" + e.backtrace.join("\r\n")
      self.set_status(STATUS_FAILED)
      self.finish_time = DateTime.now
    ensure
      save!
    end
  end

  def post_crash_move
    # don't worry about interrupted move_to_crashed calls. Maintenance of the created crash directories fall
    # outside the responsibility of WebMIS and should be handled by the server. Just make sure the directories
    # on the server are cleared.
    ConversionJob.transaction do
      clear_dir(locate_output_dir)
      self.set_file_system_status(FILE_SYSTEM_STATUS_FREE)
      save!
    end
  end

 def move_to_crashed
    # you can only move something to crash if there is something to move and the conversion scripts of this job are finished
    return if self.run_server.to_s.blank? || self.file_system_status == FILE_SYSTEM_STATUS_FREE || self.status > STATUS_FAILED
    old_working_dir = locate_working_dir
    if !old_working_dir.blank?
      dest_dir = File.join(ConfigParameter.get("conversion_job_crash_dir",self.project_id.to_s),self.bug_tracker_id)
      Job.transaction do
        if file_system_status == FILE_SYSTEM_STATUS_ON_SERVER
          final_result_dir = File.join(dest_dir, self.working_dir)
          self.result_dir_size_bytes = get_dir_size(old_working_dir)
          self.set_file_system_status(FILE_SYSTEM_STATUS_RESERVED, final_result_dir)
        end
        self.log_action("Moving to crash directory #{File.join(dest_dir,self.working_dir).to_s}")        # move script files
        self.save!
        self.stored_scriptfiles.each do |script|
          move(script, dest_dir)
        end
        self.set_file_system_status(FILE_SYSTEM_STATUS_MOVING)
        self.save!
        rsyncmove(old_working_dir, File.join(dest_dir,self.working_dir), "post_crash_move")
      end
    else
      clear_dir(locate_output_dir)
      self.set_file_system_status(FILE_SYSTEM_STATUS_FREE)
      self.log_action('Working directory blank or not found on the file system, nothing moved to the crash directory.')
      self.save
    end
  end


  def clear_scripts
    cid= File.expand_path("#{ConfigParameter.get("dakota_scripts_dir",self.project_id.to_s)}/dakota_#{id.to_s}")
    FileUtils.rm_f("#{cid}")
    FileUtils.rm_f("#{cid}.done")
    FileUtils.rm_f("#{cid}.exec")
    FileUtils.rm_f("#{cid}.log")
  end

  def cancel(user = 'system', dbs_to_remove = nil)
    Job.transaction do
      job_cancel(user)
      # whatever the status of the conversion_job, always mark its non-persistent databases ready for removal
      self.conversion_databases.each do |cd|
        if dbs_to_remove && dbs_to_remove.index(cd.id)
          # if the databases have been explicitly selected, even mark persistent databases as ready for removal
          cd.terminate
        else
          cd.cancel
        end
      end
    end
  end

  def terminate(user = 'system', dbs_to_remove = nil)
    Job.transaction do
      job_cancel(user)
      self.conversion_databases.each{|cd| cd.terminate if dbs_to_remove.nil? || dbs_to_remove.index(cd.id)}
    end
  end

  def reschedule(user_ref = 'system', manual_yn = false)
    return unless allow_reschedule?

    Job.transaction do
      job_cancel(user_ref, rescheduled_yn = true)
      self.conversion_databases.each{|cd| cd.terminate(user_ref)}

      user = (user_ref.class == User ? user_ref : nil)
      new_job = duplicate(user)
      if manual_yn
        new_job.set_status(STATUS_MANUAL)
      else
        new_job.queue
      end
      new_job.save!

      return new_job
    end
  end

  def move(src, dest)
    cmd = "mkdir -p #{dest} && cp -rf #{src} #{dest} && rm -rf #{src} 2>&1"
    return ssh(cmd).chomp
  end

  def db_result_dir
    # move to output if last step in production design - else move to /data/ops
    design_hash = self.conversion.production_orderline.design_hash
    if (!DataType.find_by_name(self.conversion.output_format).is_intermediate)
      result_dir = ConfigParameter.get("conversion_job_result_dir",self.project_id.to_s)
    else
      result_dir = ConfigParameter.get("conversion_job_intermediate_dir",self.project_id.to_s)
    end
    return File.join(result_dir,self.output_format_dir)
  end
  
  def output_format_dir
    # add output format
    output = self.conversion.output_format.to_s.downcase

    # prefer region top-level continent, otherwise the region itself
    region = nil

    if self.tagmodel['coverage'] && !self.tagmodel['coverage'].empty?
      region_rec = Region.find_by_region_code(self.tagmodel['coverage'].first) # returns nil if not found
      if region_rec
        region = region_rec.region_code
        if region_rec.parent
          if region_rec.parent.type == 'Continent'
            region = region_rec.parent.region_code
          end
          # dig deeper
          if region_rec.parent.parent && region_rec.parent.parent.type == 'Continent'
            region = region_rec.parent.parent.region_code
          end
        end
      end
    end

    # fallback when no region can be determined by job attributes
    if !region && self.conversion.region
      region = self.conversion.region.region_code
    end

    # ensure a region
    if !region
      region = Region.find_by_name('world').region_code # returns nil if not found
      region =  "wld" if !region # could not find any region, so make it
    end
    
    dir = File.join(output,region.to_s.downcase)
    return dir
  end

  def locate_working_dir
    wdir = ''
    if !self.working_dir.blank?
      wdir = ssh("find /volumes1/dakota -type d -name #{self.working_dir} 2</dev/null").strip
    end
    return wdir
  end

  def locate_output_dir
    return nil if working_dir.to_s.blank?
    output_dir = working_dir.gsub(/^w_/, "o_")
    return ssh("find /volumes1/dakota -type d -name #{output_dir} 2</dev/null").strip
  end

  def tail_z_log(a=500)
    logfile = ssh("ls -d1 #{self.locate_working_dir}\/z*.log* 2</dev/null").strip
    if !logfile.blank?
      return  ssh("tail -#{a.to_s} #{logfile}")
    else
      if (self.conversion_databases.size > 0) && (!File.split(self.conversion_databases.last.path_and_file)[0] != '.')
        zlogfile = ssh("ls -d1 #{File.split(self.conversion_databases.last.path_and_file)[0]}\/z*.log* 2</dev/null").strip
        if !zlogfile.blank?
          return  ssh("zcat #{zlogfile}| tail -#{a.to_s}")
        end
      end
      return "Location z-log file cannot be determined."
    end
  end

  def terminal(server,user=nil)
    if user
      self.log_action("Terminal activated.",user)
    else
      self.log_action("Terminal activated by unknown user!")
    end
    self.save
    #cmd = 'export DISPLAY=:0;gnome-terminal'
    dir = locate_working_dir
    cmd = "export DISPLAY=:0;gnome-terminal -e \"bash -c \\\"ssh #{run_server} -t 'cd #{dir} ;ls; bash' ; exec bash\\\"\""
    #[]
    #cmd = "export DISPLAY=:0;gnome-terminal -e \"bash -c \\\"ls #{dir} ; cd #{dir} ; ssh #{run_server} ; exec bash\\\"\""
    s = Server.ssh_to_server(server, cmd)
    
    puts s
    return s
  end

  def add_conversion_database(db_type, path, size, production_orderline = nil)
    production_orderline = self.conversion.production_orderline if !production_orderline
    cd = ConversionDatabase.new
    conv = self.conversion
    conv.conversion_databases << cd
    conv.save!
    self.conversion_databases << cd
    production_orderline.conversion_databases << cd
    production_orderline.save!
    cd.db_type = db_type
    cd.group_amount = self.group_amount
    cd.group_by = self.group_by
    cd.group_value = self.group_value
    cd.path_and_file = path
    cd.filename = File.basename(path)
    cd.path = File.dirname(path)
    cd.stdout_log = self.stdout_log
    cd.product_id = conversion.productid
    cd.size_bytes = size
    cd.toggle_persistence(self.conversion.outcome_persistent_yn)
    
    begin
     cd.store_metadata
     if self.tag_list_on(:coverage)
       cd.set_tag_list_on(:coverage, self.tag_list_on(:coverage))
     end
    rescue Exception => e
     puts e.message
     puts e.backtrace
     self.stdout_log = stdout_log + "------------\r\n" + e.message + "\r\n======\r\n" + e.backtrace.join("\r\n")
    end
    cd.tag_list = self.tag_list

    self.log_action("Conversion database #{cd.id} #{cd.shortest} created.")
    
    if self.status == STATUS_COMPLETE
      # handle the database validation, creating validation jobs if necessary
      cd.handle_automatic_validation

      begin
        test_strategy = self.conversion.production_orderline.production_order.test_strategy
        if self.conversion.production_orderline.production_order.test_strategy
          if test_strategy && test_strategy == "All"
            cd.create_test_request
            self.log_action("Automated TestJobs were successfully created.")
          elsif test_strategy && test_strategy == "Final Result" && self.conversion.last_conversion?
            cd.create_test_request
            self.log_action("Last conversion. Automated TestJobs were successfully created.")
          end
        else
          self.log_action("Automated TestJobs were not requested. No action.")
        end
      rescue => e
        puts e.message
        puts e.backtrace
        self.log_action("Automated TestJobs creation failed. Reason: #{e.message}")
      end
    else
      # job did not finish successfully, mark the database for removal as we can't be sure it is a correct one
      # set the status just to NOT_VALIDATED as the user might decide to make use of the database after all
      cd.set_status(ConversionDatabase::STATUS_NOT_VALIDATED)
      cd.set_file_system_status(ConversionDatabase::FILE_SYSTEM_STATUS_MARKED_FOR_REMOVAL)
    end
    cd.save!
  end

  def tagmodel(context_group=[])
    ProductDesign.tagmodel(self,context_group)
  end

  def self.find_by_tagmodel_and_coverage(results, model, coverage)
    ProductDesign.find_by_tagmodel_and_coverage(results, model, coverage)
  end

  def coverage_label
     ProductDesign.coverage_label_by_tagmodel(self)
  end

  def allow_rerun?
    # careful, used in the job generation in conversion to determine if a found job
    # should be used or run again
    result = false
    result = true if self.status == STATUS_CANCELLED || self.status == STATUS_RESCHEDULED
    return result
  end
  
  def self.reserved_resources
    resources = []
    ConversionJob.outstanding.each do |job|
      if job.conversion.design_json && job.conversion.design_json["connections"] then
        job.conversion.design_json["connections"].each do |connection|
          connection["source_spec"]["datasets"].each do |ds|
            resources = resources + ds["file_resources"] if ds["file_resources"]
          end
        end
      end
    end
    return resources.uniq
  end

  def self.reserved_resources_per_job
    resources_job = []
    ConversionJob.outstanding.each do |job|
      job.conversion.design_json["connections"].each do |connection|
        resources = []
        connection["source_spec"]["datasets"].each do |ds|
          resources = resources + ds["file_resources"] if ds["file_resources"]
        end
      end
      resources_job << {job.id => resources.uniq}
    end
    return resources_job
  end

  def finalize_result
    ConversionJob.transaction do
      self.reload    # to avoid a stale object

      # we cannot really terminate a delayed move action currently, but if the the file_system_status is no
      # longer 'Moving' it is assumed the move should be considered as terminated, just register the results
      # but mark them for removal, so the system can try to clean things up.
      final_dir = self.db_result_dir
      # cleanup for daksim or when no CleanUp executable is used
      clear_dir(locate_working_dir)
      self.set_file_system_status(FILE_SYSTEM_STATUS_FREE)
      self.set_status(STATUS_COMPLETE)      # don't test on its success, the status should just be set to complete if possible
      self.log_action("Moving result completed. Job #{self.status_str}.")
      self.stdout_log ||= ""
      self.stdout_log += "MOVING OF RESULTS WAS COMPLETED\r\n"
      self.finish_time = DateTime.now

      path_dir = File.join(final_dir, self.output_dir)
      filesize = 0
      Find.find(path_dir) do |f|
        filesize += File.stat(f).size
      end

      # Create a conversion_database record for each matching datatype from executable-identification
      identifications = self.conversion.design_json["output"]
      cd_created = false
      Find.find(path_dir) do |file|
        identifications.uniq.each do |i|
          if i["identification"].to_s != '' && (/(#{Regexp.escape(i["identification"])})/).match(file)
            data_type = DataType.find_by_name(i['datatype'].to_s)
            size = File.stat(file).size
            add_conversion_database(data_type.name, file, size)
            cd_created = true
          end
        end
        register_derived_product(file) if !cd_created
      end
      save!
      set_permissions_outcome_directory

      # cleanup for daksim or when no Cleanup executable is used
      clear_dir(locate_working_dir)

      # When no match on executable-identification
      add_conversion_database(conversion.output_format, File.join(final_dir,self.output_dir), filesize) unless cd_created
      save!
    end
  end

  def post_move(counter=0,emsg=nil)
    retry_counter = counter
    if retry_counter > 5
      # the post move failed 6 times but the scheduled_job is already finished. So just put the job to failed
      # and report a success back to the scheduled_job.
      clear_dir(locate_working_dir)
      self.set_status_when_move_failed(STATUS_FAILED)
      self.set_file_system_status(FILE_SYSTEM_STATUS_FREE)
      self.stdout_log = "" unless self.stdout_log
      self.stdout_log += "ERROR IN POST_MOVE OPERATION: The results of the job were successfully moved, but in the administration after the move something went 6 times wrong:\r\n"
      self.stdout_log += emsg.to_s
      save!
      return
    end

    begin
      finalize_result
    rescue => e
      retry_counter += 1
      sleep 10 # allow NFS to sync, not necessary for 'test' as that one runs on the local file system
      fmsg =  e.message + "\r\n"
      fmsg += e.backtrace.join("\r\n")
      post_move(retry_counter,fmsg)
    end
  end

  def register_derived_product(file)
    # check if file is derived product
    if product
      result = product.file_for_derived_product(file)
      if result
        product_content_item = result[0]
        tag = result[1]
        ConversionJob.transaction do
          derived_product = product.create_derived_product(product_content_item, tag)
          # create production orderline for derived_product
          ol = self.production_orderline.production_order.production_orderlines.where("product_id ='#{derived_product.volumeid}'").first
          if !ol
            ol = self.production_orderline.dup
            ol.parent_linenr = self.production_orderline.id
            ol.product_id = derived_product.volumeid
            ol.created_from = "PARENT"
            ol.bp_message = "Derived product"
            ol.bp_name = "" # status not relevant as derived products will not participate in workflow
            ol.bp_status = ""
            ol.save!
          end
          size = File.stat(file).size
          # need to attach to other orderline
          add_conversion_database("NDS", file, size, ol)
        end
      end
    end
  end

  def stored_scriptfiles
    base = File.expand_path("#{ConfigParameter.get("dakota_scripts_dir",self.project_id.to_s)}/dakota_#{self.id.to_s}")
    if File.exists?(base)
      return Dir.glob(base+'*')
    else
      # maybe moved to conversion database?
      if !self.conversion_databases.empty?
        db = self.conversion_databases.last
        if !db.path_and_file.empty?
          outputdir = db.path_and_file.scan(/.*o_\d{8}.*\//).join()
          base = File.expand_path("#{outputdir}/dakota_#{self.id.to_s}")
          if File.exists?(base)
            return Dir.glob(base+'*')
          end
        end
      end
    end
    return [] # returns empty array when no files found
  end

  def estimate_duration
    duration = 0 # Default / Unknown
    prematch = ConversionJob.includes(:conversion).where("jobs.start_time is not null and jobs.finish_time is not null AND conversions.input_format = '#{self.conversion.input_format}' AND conversions.output_format = '#{self.conversion.output_format}' AND jobs.status = #{STATUS_COMPLETE}").references(:conversions).tagged_with(self.tag_list_on(:context_labelsupplier), :on => :context_labelsupplier).tagged_with(self.tag_list_on(:context_coverage), :on => :context_coverage)

    if self.tag_list_on(:context_subregion).empty?
      match = prematch
    else
      match = prematch.tagged_with(self.tag_list_on(:context_subregion), :on => :context_subregion)
    end

    if !match.empty?
      durations = match.collect{|j|((j.finish_time - j.start_time) / 1.minute).round}
      average_duration = (durations.inject(0.0) { |sum, el| sum + el } / durations.size).round
      duration = average_duration
    end
    return duration
  end

  def label
    return_arr = []
    return_arr << self.tagmodel['coverage'].join(' ') if self.tagmodel['coverage']
    return_arr << self.tagmodel['core'].join('').gsub('|',' ') if self.tagmodel['core']
    return_arr << self.tagmodel['filling'].join(' ') if self.tagmodel['filling']
    return_arr << self.tagmodel['bundle release'].join(' ') if self.tagmodel['bundle release']
    return return_arr.join(' ')
  end

  def output_dir
    output_dir = self.working_dir.gsub(/^w_/, "o_")
    suffix = output_dir.scan(/^(o_.*)$/).flatten.join("")
    return suffix
  end

  def set_permissions_outcome_directory
    path_dir = File.join(self.db_result_dir, self.output_dir)
    of_dt = DataType.find_by_name(self.conversion.output_format)

    if of_dt && of_dt.is_intermediate
      mod_cmd = "chmod -R 755 #{path_dir}"
      grp_cmd = "chgrp -R 'conversion' #{path_dir}"
    else
      mod_cmd = "chmod -R 555 #{path_dir}"
      grp_cmd = "chgrp -R 'Domain Users' #{path_dir}"
    end
    
    cmd = [grp_cmd,mod_cmd].join(' ; ')
    Server.ssh_localhost(cmd)
  end

  alias_method :old_allow_reschedule?, :allow_reschedule?
  def allow_reschedule?
    return old_allow_reschedule? && self.conversion.running?
  end

  def self.find_all_by_project_id(project_id)
    return self.where("jobs.project_id = #{project_id}")
  end
  
  handle_asynchronously :rsyncmove
end # end Class

