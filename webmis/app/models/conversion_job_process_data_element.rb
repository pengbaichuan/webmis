class ConversionJobProcessDataElement < ActiveRecord::Base
  belongs_to :conversion_job
  belongs_to :process_data_element

  def self.create_by_job_and_process_data_elements(job_id,elements=[], indirect=false)
    elements.each do |el|
      pd_element = ProcessDataElement.find(el)
      data_set_version = 0
      if pd_element
        data_set_version = pd_element.data_set.version
      end
      cjpde = ConversionJobProcessDataElement.new(:conversion_job_id => job_id,:process_data_element_id => el,:data_set_version => data_set_version, :indirect => indirect)
      cjpde.save!
    end
  end

end
