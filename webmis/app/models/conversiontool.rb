class Conversiontool < ActiveRecord::Base
  validates :code, :presence => true
  validates_uniqueness_of :code
  validates :remarks, :presence => true, :if => :not_released?
  scope :active, -> {where('isactive = 1').order('code desc') }
  has_many :conversions
  has_many :cvtool_bundle_releases
  has_many :cvtool_bundles, :through => :cvtool_bundle_releases

  def self.find_active
    Conversiontool.where('isactive = 1').order('created_at desc')
  end

  def get_branch
    return self.code.split("_")[0].to_s
  end

  def branch_indicator
    if self.code.match(/^(\d*)_\d{8}/)
      return "_#{self.get_branch}" # 0914 => _0914
    else
      return self.get_branch[0] # dev => d
    end
  end

  def not_released?
     if self.prod_released_yn == false
       return true
     elseif self.prod_released_yn.nil?
       return true
     else
       return false
     end
  end

  def self.select_tag
    self.active.order("created_at desc").collect{|ct|[ct.code[0],[ct.code,ct.id]]}.sort_by{|c| c[0]}.collect{|cvt|cvt[1]}
  end

  def sync_with_filesystem(synchronize_bundle_yn = false, new_cvtool_bundles_active = true)
    bundle_list_path = File.join(ConfigParameter.get('cvtool_release_directory'), 'Bundles', self.code, 'bundle_list.csv') # no project_id dependency
    if File.exists?(bundle_list_path)
      found_cvtool_bundle_names = []
      CSV.foreach(bundle_list_path, :headers => true) do |row|
        cvtool_bundle_name = row[1]
        cvtool_bundle_description = row[2]
        found_cvtool_bundle_names << cvtool_bundle_name
        cvtool_bundle = self.cvtool_bundles.find_by_name(cvtool_bundle_name)
        unless cvtool_bundle
          cvtool_bundle = CvtoolBundle.find_by_name(cvtool_bundle_name)
          cvtool_bundle = CvtoolBundle.new(:name => cvtool_bundle_name, :active_yn => true, :remarks => cvtool_bundle_description) unless cvtool_bundle
          self.cvtool_bundles << cvtool_bundle
        end

        if synchronize_bundle_yn
          toolspec_name = row[0] + ".toolspec"
          toolspec_path = File.join(ConfigParameter.get('cvtool_release_directory'), 'Bundles', self.code, cvtool_bundle_name, 'docs', toolspec_name)
          if File.exists?(toolspec_path)
            bundles = self.code.gsub(/_\d{8}(_\d)?$/,'')
            release = self.code[bundles.length+1,10]
            contents = File.read(toolspec_path)
            ToolSpec.synchronize_bundle(bundles, cvtool_bundle_name, release, contents, mail_exception = true)
          end
        end
      end

      self.cvtool_bundle_releases.each do |cvtool_bundle_release|
        cvtool_bundle_release.active_yn = false unless found_cvtool_bundle_names.include?(cvtool_bundle_release.cvtool_bundle.name)
        cvtool_bundle_release.save!
      end

    else
      self.isactive = false
      save!
    end

  end

  def self.sync_with_filesystem(found_conversiontools_active = true,synchronize_bundle = true)
    bundledir = File.join(ConfigParameter.get('cvtool_release_directory'), 'Bundles') # no project_id dependency
    include_prefixes = ConfigParameter.get('include_conversion_tool_prefix').downcase.split(',') # no project_id dependency

    Dir.chdir(bundledir) do
      # conversiontool release directories have the format <name>_yyyymmdd[_n]
      conversiontool_names = Dir.glob("*_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]{,_[0-9]}")

      # set all conversiontools for which no directory was found in bundledir to inactive
      Conversiontool.active.each do |conversiontool|
        conversiontool.isactive = conversiontool_names.include?(conversiontool.code)
        conversiontool.save!
      end

      conversiontool_names.each do |dirname|
        conversiontool = Conversiontool.find_by_code(dirname)
        prefix = dirname.gsub(/_\d{8}(_\d)?$/,'').downcase
        if include_prefixes.include?(prefix)
          if conversiontool
            conversiontool.isactive = true if found_conversiontools_active
            synchronize_bundle_yn = false
          else
            conversiontool = Conversiontool.new(:code => dirname, :isactive => found_conversiontools_active , :remarks => "Created by system (Conversiontool.sync_with_filesystem).")
            if synchronize_bundle
              synchronize_bundle_yn = true
            else
              synchronize_bundle_yn = false
            end
          end
          conversiontool.save!
          conversiontool.sync_with_filesystem(synchronize_bundle_yn)
        end
      end
    end
  end

end
