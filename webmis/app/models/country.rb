class Country < Region
  validates :country_iso3_code ,:uniqueness => true,:presence => true
  validates :country_iso_code , :uniqueness => true,:presence => true
  validates :name , :uniqueness => true,:presence => true
end
