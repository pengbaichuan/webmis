class Coverage < ActiveRecord::Base
  has_many :datasources
  has_many :coverages_regions, :dependent => :destroy
  has_many :regions, :through => :coverages_regions

  has_many :products_as_primary_coverage , :class_name => 'Product' ,:foreign_key => "primary_coverage_id"
  has_many :products_as_secondary_coverage,:class_name => 'Product' ,:foreign_key => "secondary_coverage_id"

  def swap_temp_status(tmp_id)
    self.status = 0 #tmp
    self.save
    c = Coverage.find(tmp_id)
    c.status = nil
    c.save   
  end

  def duplicate
    Coverage.transaction do
      c_dup = self.dup
      c_dup.remarks = "Cloned from #{self.id}."
      c_dup.save
      self.coverages_regions.each do |c_region|
        c_dup_region = c_region.dup
        c_dup_region.coverage_id = c_dup.id
        c_dup_region.save
      end
      return c_dup
    end
  end

  def add_region(region_id)
    new_region = CoveragesRegion.new({:region_id => region_id,:coverage_id => self.id})
    new_region.save
  end

  def region_included?(region_code)
    if self.regions.empty?
      # No regions in coverage
      return false
    else
      if Region.find_by_region_code(region_code)
        self.regions.each do |coverage_region|
          if region_code.upcase == coverage_region.region_code.upcase
            # Match
            return true
          end
        end
        # Requested region not in coverage
        return false
      else
        # Requested region not available in table
        return false
      end
    end
  end

end
