class CoveragesRegion < ActiveRecord::Base
  belongs_to :coverage
  belongs_to :region
end
