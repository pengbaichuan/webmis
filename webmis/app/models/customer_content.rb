class CustomerContent < ActiveRecord::Base
  belongs_to :customer_content_specification
  belongs_to :product
  belongs_to :production_orderline
  has_many :customer_targets, :dependent => :destroy

  validates :customer_content_specification_id, :presence => true
  validates :value, :customer_content_valid => true

  def find_target(output_target)
     self.customer_targets.where(:output_target => output_target).first rescue nil
  end

  def generate
    if self.customer_content_specification.generated?
      begin
        self.value = self.instance_eval(self.customer_content_specification.generation_code) 
      rescue Exception => exc
        puts "ERROR: Generation failed for customer_content #{self.id}."
        puts exc
        self.value = nil
      end
    elsif !self.customer_content_specification.customer_content_values.empty?
      # suggestion, if we have a list of possible values, just take the last one as default
      self.value = self.customer_content_specification.customer_content_values.last.value
    end
  end

  def generate_if_empty
    if value.to_s.blank?
      self.value = generate
      save unless value.to_s.blank?
    end
  end

  def value_string
    # placeholder function to format the value string, for now it just returns value as a string
    self.value.to_s
  end

  def allow_editing?
    if self.customer_content_specification.for_product || self.production_orderline
      return self.customer_content_specification.editable?
    else
      return false
    end
  end
end
