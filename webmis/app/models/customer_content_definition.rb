class CustomerContentDefinition < ActiveRecord::Base
  OUTPUT_TARGET_NONE          =  0
  OUTPUT_TARGET_CONVERSION_DB = 20
  OUTPUT_TARGET_RELEASE_NOTES = 60
  OUTPUT_TARGET_XML           = 80

  belongs_to :customer_content_specification
  belongs_to :product_line
  has_many :customer_targets, :dependent => :nullify

  validates :customer_content_specification_id, :presence => true
  validates :product_line, :presence => true

  @@outputarray = Array.new
  @@outputarray[OUTPUT_TARGET_NONE]           = "None"
  @@outputarray[OUTPUT_TARGET_CONVERSION_DB]  = "Conversion database"
  @@outputarray[OUTPUT_TARGET_RELEASE_NOTES]  = "Release notes"
  @@outputarray[OUTPUT_TARGET_XML]            = "XML"

  def self.outputarray
    @@outputarray
  end

  def self.outputhash
    o_hash = Hash.new
    @@outputarray.each do |o|
      if !o.nil?
        o_hash[o] = @@outputarray.index(o)
      end
    end
    return o_hash.sort_by { |k,v| v }
  end

  def output_string
    @@outputarray[self.output_target]
  end

end
