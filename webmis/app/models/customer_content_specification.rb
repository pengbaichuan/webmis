class CustomerContentSpecification < ActiveRecord::Base
  version_fu
  # numbering should be alphabetically to ease sorting
  TYPE_HASH       =  30
  TYPE_STRING     =  60
  TYPE_TEXT       =  80

  has_many :customer_content_definitions
  has_many :product_lines, through: :customer_content_definitions
  has_many :customer_content_values
  has_many :customer_contents

  scope :active, -> { where(:is_active => true).order(:name)}
  include HandleVersioning

  @@typearray                 = Array.new
  @@typearray[TYPE_HASH]      = "Hash"
  @@typearray[TYPE_STRING]    = "String"
  @@typearray[TYPE_TEXT]      = "Text"

  def self.typearray
    @@typearray
  end

  @@attrs_requiring_review = [:generation_code, :validation_code]

  def self.attrs_requiring_review
    return @@attrs_requiring_review
  end

  def self.typehash
    t_hash = Hash.new
    @@typearray.each do |t|
      if !t.nil?
        t_hash[t] = @@typearray.index(t)
      end
    end
    return t_hash.sort_by { |k,v| v }
  end

  def self.active?
    self.where(is_active: true)
  end

  def self.manual?
    self.where(manual: true)
  end

  def self.for_product?
    self.where(for_product: true)
  end

  def generated?
    !self.generation_code.to_s.blank?
  end

  def editable?
    self.manual || self.customer_content_values.size > 0
  end

  def hash?
    self.content_type == TYPE_HASH
  end

  def edit_hash?
    self.manual && self.hash?
  end

  def string?
    self.content_type == TYPE_STRING
  end

  def edit_string?
    self.manual && self.string?
  end

  def text?
    self.content_type == TYPE_TEXT
  end

  def edit_text?
    self.manual && self.text?
  end

  def type_string
    self.content_type ? @@typearray[self.content_type] : ""
  end

  def use_selection_list?
    return !self.manual && self.customer_content_values.size > 0
  end

  def editable?
    return self.manual || self.customer_content_values.size > 0
  end

  def show_content_in_index?
    return[TYPE_STRING].include?(self.content_type)
  end
end