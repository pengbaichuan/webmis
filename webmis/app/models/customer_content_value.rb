class CustomerContentValue < ActiveRecord::Base
  belongs_to :customer_content_specification
  validates :customer_content_specification_id, :presence => true
  validates :value, :customer_content_valid => true
end
