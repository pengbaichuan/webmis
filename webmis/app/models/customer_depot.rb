class CustomerDepot < ActiveRecord::Base
  belongs_to :shipping_order 
  has_many :shipping_orders
  belongs_to :payment_statement, :class_name => "TextBlock", :foreign_key => "payment_statement_text_block_id"
  
  scope :orderers, ->  { where(:ordering_yn => true).order(:name)  }
  scope :shippers, ->  { where(:shipping_yn => true).order(:name)  }
  scope :invoicers,->  { where(:invoicing_yn => true).order(:name) }
  
  message = ': Shipping role is enabled, so at least one value should be available in VAT percentage or statement.'
  validates :vat_percentage,:presence => {:message => message}, :if => ("vat_statement.nil?  && shipping_yn == true")
  validates :vat_statement, :presence => {:message => message}, :if => ("vat_percentage.nil? && shipping_yn == true")
  validates :vat_identification, :presence => true, :if => ("shipping_yn == true")

end
