class CustomerTarget < ActiveRecord::Base
  belongs_to :customer_content
  belongs_to :customer_content_definition

  validates :customer_content_id, presence: true
  validates :output_target, presence: true
end
