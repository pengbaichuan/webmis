class CvtoolBundle < ActiveRecord::Base 
  validates :name, :presence => true

  has_many :cvtool_bundle_releases
  has_many :conversiontools, :through => :cvtool_bundle_releases
  has_many :conversions

  scope :active, -> { where(:active_yn => true) }

  def self.select_tag
    self.active.select("cvtool_bundles.id,cvtool_bundles.name").includes("cvtool_bundle_releases").order(:name).collect {|c|[c.name,c.id]}.uniq
  end

  def self.select_cvtool_bundles(selected_conversiontool_id, production_orderline = nil)
    used_cv_tooling = production_orderline ? production_orderline.used_cv_tooling : []
    if selected_conversiontool_id 
      conversiontool_id = selected_conversiontool_id
    else
      # use the conversiontool_id as provided by the production_orderline
      conversiontool_id = used_cv_tooling[0][1] if used_cv_tooling[0]
    end

    selection_list = self.joins("INNER JOIN cvtool_bundle_releases on cvtool_bundle_releases.cvtool_bundle_id = cvtool_bundles.id").where(
        "cvtool_bundle_releases.conversiontool_id = #{conversiontool_id.to_i} AND " +
        "cvtool_bundle_releases.active_yn = 1 AND " +
        "cvtool_bundles.active_yn = 1").order(:name).collect{|c| [c.name.to_s, c.id]}
    if used_cv_tooling[1] && used_cv_tooling[0] && (selected_conversiontool_id.nil? || selected_conversiontool_id == used_cv_tooling[0][1])
      # the conversiontool has not changed, make sure it is also possible to select the original cvtool_bundle
      selection_list << used_cv_tooling[1]
      selection_list.uniq!
    end
    if selection_list.empty?
      return CvtoolBundle.select_tag
    else
      return selection_list
    end
  end

end
