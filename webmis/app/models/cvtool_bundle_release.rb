class CvtoolBundleRelease < ActiveRecord::Base
  validates :conversiontool_id, :presence => true
  validates :cvtool_bundle_id, :presence => true
  scope :active, -> { where(:active_yn => true) }

  belongs_to :conversiontool
  belongs_to :cvtool_bundle
end