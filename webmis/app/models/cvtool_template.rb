class CvtoolTemplate < ActiveRecord::Base
 
validates :name, :presence => true
validates :filename, :presence => true, :if => :template
validates :template, :presence => true, :if => :filename
validates_uniqueness_of :name

def self.find_active
    CvtoolTemplate.where('isactive = 1').order('created_at desc')
end

end
