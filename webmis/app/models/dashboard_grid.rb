class DashboardGrid < ActiveRecord::Base
  belongs_to :user
  has_many :dashboard_grid_widgets
  has_many :dashboard_widgets,  :through => :dashboard_grid_widgets

  validates :name, :presence => true, :uniqueness => :true
  validates :user_id, :presence => true

end
