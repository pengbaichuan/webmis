class DashboardWidget < ActiveRecord::Base
  has_many :dashboard_grid_widgets
  has_many  :dashboard_grids, :through => :dashboard_grid_widgets

  before_save :default_values
  validates :name,:presence => true, :uniqueness => true
  validates :width_css_class,:presence => true
  validates :refresh_time, :presence => true, :numericality => true

  def default_values
    self.refresh_time ||= 60000 # 1 minute
    self.width_css_class ||= 'span4'
  end

end
