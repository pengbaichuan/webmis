class DataRelease < ActiveRecord::Base
  belongs_to :company_abbr
  has_many :receptions
  has_many :product_designs
  validates :company_abbr, :presence => true
  belongs_to :previous_data_release, :class_name => "DataRelease", :foreign_key => 'prev_data_release_id'


scope :production, -> { where('production is true and (active is null or active = true)').order("name desc") }

after_initialize :default_values
  
def datasource
 return company_abbr
end

def generate_release_code
  if !self.name.blank?
    return self.name.gsub(/[Qq]/,"_q").gsub('.','_').downcase
  else
    return ''
  end
end

def self.find_by_tagmodel(tagmodel)
  key = "core"
  if !tagmodel["core"]
      key  = tagmodel["filling"][0]
  end
  ids = tagmodel[key][0].split("|")
  #mainsupp code
  supplier_abbrevation = ids[1]
  release_name = ids[2]
  rel = DataRelease.includes("company_abbr").where("data_releases.name = ? and company_abbrs.ms_abbr_3 = ?", release_name, supplier_abbrevation)
  return rel[0]
end

def self.find_by_supplier_and_release_name(supplier_name, release_name)
  rel = DataRelease.includes("company_abbr").where("data_releases.name = ? and company_abbrs.company_name = ?", release_name, supplier_name)
  return rel[0]
end

def previous_data_release_candidates(abbr_id=self.company_abbr_id)
  return DataRelease.production.where("company_abbr_id = ?",[abbr_id])
end

private
  def default_values
    # active flag is not used, so equalize it with the production flag
    self.production = true if self.production.nil?
    self.active = self.production
  end

end
