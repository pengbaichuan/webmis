class DataSet < ActiveRecord::Base
  acts_as_ordered_taggable_on :tags
  version_fu

  belongs_to :data_type
  belongs_to :company_abbr
  belongs_to :data_release
  belongs_to :filling
  belongs_to :region
  belongs_to :coverage
  belongs_to :poi_mapping_layout
  has_many :regions, :through => :coverage
  has_many :direct_receptions, class_name: 'Reception'
  has_many :product_data_set_infos
  has_many :products, :through => :product_data_set_infos
  has_many :data_set_group_lines
  has_many :data_set_groups, :through => :data_set_group_lines
  has_many :extract_jobs, :foreign_key => :reference_id
  has_many :process_data_elements
  has_many :receptions, through: :process_data_elements
  has_many :conversion_jobs, :through => :process_data_elements
  has_many :conversions, :through => :conversion_jobs
  has_many :production_orderlines, :through => :conversions
  has_many :production_orders,  :through => :production_orderlines
  has_many :conversion_databases, :through => :conversion_jobs
  has_many :data_set_status_logs

  after_initialize :default_values
  before_save :update_tag_list
  after_save :auto_data_set_status_log
  
  validates :data_type_id, :presence => true
  validates :company_abbr_id, :presence => true
  validates :region_id, :presence => true
  validates :data_release_id, :presence => true
  
  STATUS_DRAFT                  =   0
  STATUS_WAITING_DELIVERY       = 100
  STATUS_IN_PROGRESS            = 200
  STATUS_REVIEW                 = 300
  STATUS_TEST                   = 400
  STATUS_PRODUCTION             = 500
  STATUS_REMOVAL_CANDIDATE      = 600
  STATUS_REMOVED                = 999

  attr_accessor :checklog

  validates :data_release_id, uniqueness: {:scope => [:data_type_id,:company_abbr_id,:region_id,:filling_id], :message => ",Data type, Supplier, Coverage and Filling combination already taken", :if => "status != #{STATUS_REMOVED}"}

  scope :production, -> { where(:status => STATUS_PRODUCTION ) }
  scope :preproduction, -> { where("status = #{STATUS_PRODUCTION} OR status = #{STATUS_TEST}") }
  scope :active, -> { where("status < #{STATUS_REMOVAL_CANDIDATE}") }
  
  @@statusarray = Array.new
  @@statusarray[STATUS_DRAFT]                  = 'Draft'
  @@statusarray[STATUS_WAITING_DELIVERY]       = 'Awaiting delivery'
  @@statusarray[STATUS_IN_PROGRESS]            = 'In progress'
  @@statusarray[STATUS_REVIEW]                 = 'Review'
  @@statusarray[STATUS_TEST]                   = 'Test'
  @@statusarray[STATUS_PRODUCTION]             = 'Production'
  @@statusarray[STATUS_REMOVAL_CANDIDATE]      = 'Removal candidate'
  @@statusarray[STATUS_REMOVED]                = 'Removed'

  def create_new_version?
    # override the create_new_version? method of version_fu
    self.status_changed? && ( self.status == STATUS_REVIEW || self.status == STATUS_DRAFT )
  end

  def status_string
    return @@statusarray[self.status]
  end

  def self.status_string(status)
    return @@statusarray[status]
  end

  def self.status_array
    @@statusarray.collect{|v|[v,@@statusarray.index(v)]if v}.compact
  end

  def status_allowed?(to_status)
    if self.status != to_status

      case to_status
      when STATUS_PRODUCTION
        if !self.regions.empty? && self.status >= STATUS_TEST
          return true
        end
      when STATUS_REMOVED
        return true
      when STATUS_WAITING_DELIVERY
        if !self.regions.empty? && self.status == STATUS_DRAFT
          return true
        end
      else
        return false
      end
    end
  end

  def map_to_product_data_set_info
    # returns object with nested associations, not saved!
    pdsi = ProductDataSetInfo.new
    pdsi.description = self.description
    pdsi.datatype_id = self.data_type_id
    pdsi.area_name = self.region.name
    pdsi.name = self.name
    pdsi.company_abbr_id = self.company_abbr_id
    pdsi.data_release = self.data_release.name
    pdsi.data_info = self.data_type.name
    pdsi.filling = self.filling.name if self.filling
    pdsi.major_dataset = false
    pdsi.data_set_id = self.id
    pdsi.data_set_version = self.version

    if self.coverage
      pdsi.coverage = self.coverage.dup
      self.coverage.coverages_regions.each do |region|
        pdsi.coverage.coverages_regions << region.dup
      end
    end

    return pdsi
  end

  def processable_regions(version=nil)
    if self.coverage
      if version
        old = self.versions.find_by_version(version)
        v_coverage = Coverage.find(old.coverage_id) if old.coverage_id
        if v_coverage
          return v_coverage.coverages_regions.where('do_not_process is null or do_not_process = 0')
        end
      else
        return self.coverage.coverages_regions.where('do_not_process is null or do_not_process = 0')
      end
    else
      return [] # empty
    end
  end

  def create_or_find_coverage_region(region_ids)
    if self.coverage && self.coverage.regions.collect {|r|r.id}.sort == region_ids.sort
        # No changes in region coverage
        return self.coverage
    else
      if !region_ids.empty?
        begin
          Coverage.transaction do
            coverage = Coverage.new
            coverage.name = "DS #{self.id} v.#{self.version}"
            coverage.remarks = "Initiated with Data Set #{self.id} #{self.name} v.#{self.version}"
            coverage.save!
            region_ids.each do |region_id|
              coverage.add_region(region_id)
            end
            return coverage
          end
        rescue => e
          log.error(e)
        end
      else
        # create coverage without regions
        if !self.coverage
          begin
            Coverage.transaction do
              coverage = Coverage.new
              coverage.name = "DS #{self.id} v.#{self.version}"
              coverage.remarks = "Initiated with Data Set #{self.id} #{self.name} v.#{self.version}"
              coverage.save!
              coverage.add_region(self.region.id)
              return coverage
            end
          rescue => e
            log.error(e)
          end

        end
        
      end
    end
  end

  def has_region?(region_id,version=nil,project_id=nil)
    if self.region_id == region_id
      return true
    end


    if version == 'latest_approved'
      # no need to find a version
      if ( Project.default?(project_id) && self.status == DataSet::STATUS_PRODUCTION ) || (!Project.default?(project_id) && (self.status == DataSet::STATUS_PRODUCTION || self.status == DataSet::STATUS_TEST))
        version = nil
      end
    end

    region = Region.find(region_id)
    region_aliases = Region.find_all_by_region_code(region.region_code, true).collect{|r|r.id}

    match = nil
    if version
      if version != 'latest_approved'
        versioned = self.versions.find_by_version(version)
      else
        if Project.default?(project_id)
          versioned = self.versions.where(:status => DataSet::STATUS_PRODUCTION).last
        else
          versioned = self.versions.where("status = #{DataSet::STATUS_PRODUCTION} OR status = #{DataSet::STATUS_TEST}").last
        end
      end
      v_coverage = Coverage.find(versioned.coverage_id) if versioned && versioned.coverage_id
      if v_coverage
        match = v_coverage.coverages_regions.where(region_id:region_aliases)
      end
    end
    if self.coverage && !version
      match = self.coverage.coverages_regions.where(region_id:region_aliases)
    end

    return false if !match || match.empty?
    match.each do |coverages_region|
    if coverages_region
      if !coverages_region.do_not_process
        return true
      else
        return false
      end
    else
      return false
    end
    end
  end

  def parameter_settings_by_region_id(region_id)
    region = Region.find(region_id)
    region_aliases = Region.find_all_by_region_code(region.region_code, true).collect{|r|r.id}
    
    if self.coverage
      coverages_regions = self.coverage.coverages_regions.where(region_id:region_aliases)
      return '' if coverages_regions.empty?

      coverages_regions.each do |coverages_region|
        if coverages_region && coverages_region.parameter_settings.to_s != '' && coverages_region.parameter_settings.to_s != '[]'
          return coverages_region.parameter_settings.to_s
        else
          return ''
        end
      end
    else
      return ''
    end
  end

  def do_not_process_value_by_region_id(region_id)
    if self.coverage
      coverages_region = self.coverage.coverages_regions.find_by_region_id(region_id)
      if coverages_region
        return coverages_region.do_not_process
      else
        return "false"
      end
    else
      return "false"
    end
  end

  def label(tag_only=false)
    data_type =  self.data_type ? self.data_type.name : '-'
    filling = self.filling ? " #{self.filling.name}" : ''
    supplier_code = self.company_abbr ? self.company_abbr.ms_abbr_3 : '-'
    release = self.data_release ? self.data_release.name : '-'
    region_code = self.region ? self.region.region_code : '-'
    name = self.name ? "\"#{self.name}\"" : ''
    name = '' if tag_only
    return "#{supplier_code} #{release} #{region_code} #{data_type} #{filling} #{name}".strip
  end

  def sourcetag
    data_type =  self.data_type ? self.data_type.name : '-'
    supplier_code = self.company_abbr ? self.company_abbr.ms_abbr_3 : '-'
    release = self.data_release ? self.data_release.name : '-'
    "#{data_type.downcase}|#{supplier_code.downcase}|#{release.downcase}"
  end

  def self.find_or_create_by_core_attributes(data_type_name,supplier_name,release_name,region_code,filling_name=nil,only_production=false,create=false)
    filling = []
    result = nil
    data_type = DataType.find_by_name(data_type_name)
    company_abbr = CompanyAbbr.find_by_company_name(supplier_name)
    data_release = DataRelease.find_by_name(release_name)
    region = Region.find_by_region_code(region_code)
    filling = Filling.find_by_name(filling_name) if filling_name

    if data_type && company_abbr && data_release && region && filling
      found = DataSet.where(:data_type_id => data_type.id,:company_abbr_id => company_abbr.id,:data_release_id => data_release.id,:region_id => region.id)
      if filling_name && !found.empty?
        match = found.where(:filling_id => filling.id)
      else
        match = found.where("filling_id is null or filling_id = ''")
      end
      exact_match = match.where("status < #{STATUS_REMOVED}")
      if only_production
        result = exact_match.production.last
      else
        result = exact_match.last
      end
      if !result && create 
        filling_id_val = nil
        filling_id_val = filling.id if filling
        new_set = DataSet.create_ds_and_associated_data_release(:data_type_id => data_type.id,:company_abbr_id => company_abbr.id,:data_release_id => data_release.id,:region_id => region.id,:filling_id => filling_id_val)
        result = new_set if new_set
      end
    end
    return result
  end

  def self.create_ds_and_associated_data_release(data_type_name,supplier_name,release_name,region_code,filling_name=nil)
    data_type = DataType.find_by_name(data_type_name)
    company_abbr = CompanyAbbr.find_by_company_name(supplier_name)
    data_release = DataRelease.find_by_name(release_name)
    region = Region.find_by_region_code(region_code)
    filling = Filling.find_by_name(filling_name) if filling_name

    if data_type && company_abbr && region && release_name.strip != ''
      DataSet.transaction do
        if !data_release
          new_data_release = DataRelease.new(:name => release_name.strip,:company_abbr_id=>company_abbr.id,:active=>true,:production=>true)
          new_data_release.release_code = new_data_release.generate_release_code
          new_data_release.save
          data_release = new_data_release
        end
        if ( filling_name && filling ) || ( filling_name == nil )
          name_ar = [data_type.name,company_abbr.ms_abbr_3,data_release.name,region.region_code]
          name_ar << filling_name.gsub(' ','-') if filling_name
          ds_name = name_ar.join('_').upcase
          ds = DataSet.new(:name => ds_name,:data_type_id => data_type.id,:company_abbr_id => company_abbr.id,:data_release_id => data_release.id,:region_id => region.id)
          ds.filling_id = filling.id if filling
          ds.save!
          return ds
        end
      end
    else
      return nil
    end
  end

  def self.processable_proccess_data_element_region?(process_data_element_id,region_id)
    # Can be called from Executable tagging logic eval
    ds = ProcessDataElement.find(process_data_element_id).data_set
    if !ds
      # No DS so process it
      return true
    else
      allowed_regions = self.processable_regions(self.version)
      if allowed_regions.collect{|r|r.region.id}.include?(region_id)
        # In list so process it
        return true
      else
        # Not in coverage so don't process it
        return false
      end
    end
  end

  def duplicate(dr_id=nil)
    DataSet.transaction do
      new_coverage = self.coverage.duplicate
      new_ds = self.dup
      new_ds.data_release_id = dr_id if dr_id
      new_ds.remarks = "Cloned from Data set definition #{self.id} #{self.label(true)}.\n" + self.remarks
      new_ds.status = STATUS_DRAFT
      new_ds.coverage_id = new_coverage.id
      new_ds.name = new_ds.label(true)
      new_ds.save!
      return new_ds
    end
  end

  def create_or_add_to_group(return_message=false)
    if self.data_set_groups.empty?
      r_message = 'create_or_add_to_group'
      new_group = false
      add_line = nil
      ds_g = DataSetGroup.new({:data_release => self.data_release.name,
          :company_abbr => self.company_abbr.ms_abbr_3,
          :region_code => self.region.region_code, :data_type => self.data_type.name,
        })
      if self.filling
        ds_g.filling = self.filling.name
      else
        ds_g.filling = ''
      end
      q = DataSetGroup.where(ds_g.attributes.delete_if {|k, v| !v })
      
      ds_g.description = "Generated based on DS #{self.id}\"#{self.name}\" v.#{self.version}."

      ds_g_line = DataSetGroupLine.new(:data_set_id => self.id)

      if q.empty?
        new_group = true
      else
        add_line = q.last.id
      end

      # save group and/or line
      begin
        DataSetGroup.transaction do
          if new_group
            ds_g.save!
            ds_g_line.data_set_group_id = ds_g.id
            r_message = "Created and added to new data set group #{ds_g.name}"
          else
            if add_line
              ds_g_line.data_set_group_id = add_line
              r_message = "Added to existing data set group #{ds_g_line.data_set_group.name}"
            end
          end
          ds_g_line.save!
        end
      rescue => e
        if return_message
          r_message = e.message
        else
          raise e.message
        end
      end
    else
      r_message = "Already related to data set definition group(s) #{self.data_set_groups.collect{|g|g.name}.join(' / ')}"
    end
    return r_message if return_message
  end

  def update_tag_list
    filling_name = []
    filling_name = [self.filling.name] if self.filling
    self.tag_list = [self.company_abbr.company_name,self.region.name,self.data_type.name,self.data_release.name] + filling_name
  end

  def ssh_localhost(cmd)
    stdout = ""
    user = ENV['prod_user']
    server = 'localhost'
    Net::SSH.start( server , user) do |session|
      session.exec!(cmd) do |channel, stream, data|
        stdout << data if stream == :stdout
        stdout << data if stream == :stderr
      end
      session.close
      return stdout
    end
  end

  def generate_directory
    # Set default values
    supplier_dir,release_dir,data_type_dir,region_dir = 4.times.collect {|_|"unknown"}

    # Supplier / Release
    supplier_dir = self.company_abbr.ms_abbr_3.downcase if self.company_abbr
    if self.data_release && !self.data_release.release_code.blank?
      release_dir = self.data_release.release_code
    elsif self.data_release
        release_dir = self.data_release.generate_release_code
    end
    unless self.filling.blank?
      filling_suffix = "_#{self.filling.name.parameterize.underscore}"
    else
      filling_suffix = ''
    end
    # Region
    region_dir = self.region.region_code.downcase if self.region && self.region.region_code
    # Data type
    data_type_dir = self.data_type.name.downcase if self.data_type && !self.data_type.name.blank?
    return File.join(ConfigParameter.get('process_data_directory',Project.default.id),data_type_dir,region_dir,supplier_dir,release_dir + filling_suffix).downcase.gsub('//','/').strip
  end

  def create_directory_structure
    msg = []
      cmd = "mkdir -p #{self.generate_directory.strip}"
      ssh_localhost(cmd)
      msg << ssh_localhost("ls -d1 #{self.generate_directory.strip}")
        self.process_data_elements.active.each do |el|
          element_dir = el.full_directory
          el_cmd = "mkdir -p #{element_dir.strip}"
          ssh_localhost(el_cmd)
          msg << ssh_localhost("ls -d1 #{element_dir.strip}")
        end
      cmd = "chmod -R 775 #{self.generate_directory.strip}"
      ssh_localhost(cmd)
      return msg.join()
  end

  def validate_element_file_structure(checkonly=false)
    emsg = []
    proceed = true
    content = true

    # has element(s)?
    if self.process_data_elements.size == 0
      emsg << "No elements available!"
      proceed = false
    end

    if proceed
      if File.directory?(self.generate_directory)
        file_dirs = Dir[File.join(self.generate_directory,'*')]
        # additional meta files does not count as data
        file_dirs.delete_if {|f|f.to_s.match(/__trash/)}
        unless file_dirs.empty?
        else
          emsg << "Directory '#{self.generate_directory}' has no filled element directories!"
          proceed = false
        end
      else
        emsg << "Directory '#{self.generate_directory}' does not exist!"
        proceed = false
      end
    end

    if proceed
      # unique elements within data set?
      eldirs = self.process_data_elements.active.collect{|el|el.full_directory}
      unique =  eldirs.uniq
      msgstr = ''
      if !unique.empty?
        nonuniqarr = unique.map { | e | [eldirs.count(e), e] }.
          select { | c, x | c > 1 }.
          sort.reverse.
          map { | c, e | "'#{e}' #{c}" }
        if !nonuniqarr.empty?
          msgstr = nonuniqarr.compact.join('') + " times specified."
        end
        if !msgstr.blank?
          msgstr = "Element with " + msgstr
          emsg << msgstr
          proceed = false
        end
      end
    end

    self.process_data_elements.active.each do |el|
      # element directory exist?
      msgstr = ''
      if !File.directory?(el.full_directory)
        msgstr = msgstr + "'#{el.full_directory}' does not exist or is not readable."
        emsg << msgstr
        proceed = false
      end

      # element directory empty?
      if proceed == true && Dir[File.join(el.full_directory,'*')].delete_if {|f|f.to_s.match(/\.reception/)}.empty?
        msgstr = msgstr + "'#{el.full_directory}' is empty or content is not readable."
        emsg << msgstr
        content = false
        proceed = false
      end

      # double rdb-files?
      if content == true && (el.process_data_specification.name.downcase == "rdb" || el.process_data_specification.name.downcase == "rdf")
        files = Dir.glob(File.join(el.full_directory,'*'))
        if files.size > 1
          msgstr = msgstr + "More then one RDB-file stored. (#{files.join(',')})"
          emsg << msgstr
          proceed = false
        end
      end
    end
    
    if proceed
      return true
    else
      return false if checkonly
      raise emsg.uniq.join("\r\n")
    end
  end

  def report_validate_element_file_structure
    begin
      self.validate_element_file_structure
    rescue RuntimeError => e
      return e.message
    end
    return "No problems in elements and file structure detected."
  end

  def self.reviewers
    project_id = Project.default.id
    role = Role.find_by_title("data handler")
    ru = []
    ru = RolesUser.where(role_id:role.id,project_id:project_id) if role
    if ru.empty?
      return []
    else
      return ru.collect {|rus|rus.user}
    end
  end

  def self.reviewers_email_addresses
    self.reviewers.collect{|r|r.email}.join(',')
  end

  def reviewer_name
    if self.review_user_id.blank?
      return ''
    else
      return User.find(self.review_user_id).full_name
    end
  end

  def request_review(request_user_id,review_user_id)
    DataSet.transaction do
      req_user = User.find(request_user_id)
      rev_user = User.find(review_user_id)
      self.status = STATUS_REVIEW
      self.review_request_user_id = request_user_id
      self.review_user_id = review_user_id
      self.review_result = nil
      self.save!
      log_msg = "Requested a review to #{rev_user.user_name}."
      log_status({:user => req_user.user_name,:remarks => log_msg})
      Postoffice.data_set_review_request(self,req_user,rev_user).deliver_now
    end
  end

  def result_review(approved=true,remark='')
    req_user = User.find(self.review_request_user_id)
    rev_user = User.find(self.review_user_id)
    DataSet.transaction do
      self.review_request_user_id = nil
      self.review_user_id = nil
      self.review_result = remark
      self.status = STATUS_IN_PROGRESS
      self.status = STATUS_TEST if approved
      self.save!
      if approved
        msg = "Approved"
      else
        msg = "Disapproved"
      end
      log_msg = "#{msg} data set after review.\n#{remark}"
      log_status({:user => rev_user.user_name,:remarks => log_msg})
      Postoffice.data_set_review_result(self,req_user,rev_user,approved).deliver
    end
  end

  def auto_data_set_status_log
    if self.tag_list_changed?
      list = self.tag_list_change
      log_status({:level => "DATA SET #{self.id}", :user => self.updated_by, :remarks => "Tags changed from '#{list.shift}' to '#{list.join(', ')}'."})
    end
  end

  def log_status(args = {})
    args[:level] ||= 'DATA SET'
    args[:user] ||= self.updated_by
    args[:version] ||= self.version
    args[:remarks] ||= "n.a."
    args[:status] ||= self.status

    os = DataSetStatusLog.new
    os.data_set_id = self.id
    os.level = args[:level]
    os.user = args[:user]
    os.version = args[:version]
    os.remarks = args[:remarks]
    os.status = args[:status]
    os.save
  end

  def create_extract_job(reception_id,files=nil)
    # Ensure directory
    self.create_directory_structure

    reception = Reception.find(reception_id)
    if !files
      # all files from reception
      files = reception.storage_location.to_s.split
    end

    picked = []
    picked = self.picked_reception_ids.split(',') if self.picked_reception_ids
    picked << reception_id
    self.picked_reception_ids = picked.uniq.join(',')
    self.save!

    job = ExtractJob.new
    job.status = Job::STATUS_QUEUED
    job.project_id = Project.default.id
    job.reference_id = self.id
    job.schedule_allow_virtual = true
    job.working_dir = "__src_#{reception_id}_#{reception.core_tags.join('_').downcase}"
    target_dir = File.join(self.generate_directory,job.working_dir)
    # TODO migrate run_script to job_script
    job.run_script = "#Copy data from reception #{reception_id} to data set #{self.id}: #{self.label}\n"
    job.run_script = job.run_script + "mkdir -p #{target_dir}\n"
    job.run_script = job.run_script + "cd #{target_dir}\n"
    # exit 1 if target is not available
    job.run_script = job.run_script + "if [ $? == 1 ]; then\n"
    job.run_script = job.run_script + "exit 1\nfi\n"
    files.each do |file|
      if !file.match("\\*") # Exclude trailing wildcards
        job.run_script = job.run_script + "cp -r #{file} .\n"
      end
    end
    job.run_script = job.run_script + "chmod -R 775  #{target_dir}\n"
    job.save!
  end

  def sync_elements(user_obj=nil)
      username = "unknown user"
      username = user_obj.full_name if user_obj
      trigger_change = false
      if File.directory?(self.generate_directory)
        file_dirs = Dir[File.join(self.generate_directory,'*')]
        unless file_dirs.empty?
          file_dirs.each do |dir|
            spec_dir = File.basename(dir)
            # check if specification exist
            next if spec_dir == '__trash'
            unless process_data_specification = ProcessDataSpecification.find_by_directory(spec_dir)
              raise "#{spec_dir} is not defined as process data specification!"
            end

            reception_id = ''
            reception_meta_files = Dir[File.join(dir,'*.reception')]
            if !reception_meta_files.empty?
              reception_id = File.basename(reception_meta_files.last, '.reception')
              # cleanup reception file
              reception_meta_files.each do |rfile|
                ssh_cmd = "rm -rf #{rfile}"
                self.ssh_localhost(ssh_cmd)
              end
            end

            unless process_data_specification.split_by_key.blank?
              # split_by_values are only regions for now
              dirs_incl_region = Dir[File.join(dir,'*')].delete_if {|f|f.to_s.match(/\.reception/)}
              if dirs_incl_region.empty?
                raise "#{spec_dir} needs to be splitted by '#{process_data_specification.split_by_key}'."
              else
                dirs_incl_region.each do |dir_subregion|
                  split_by_value = File.basename(dir_subregion)
                  if Region.find_by_region_code(split_by_value) == nil
                    raise "#{split_by_value} is not defined as region!"
                  end
                  # check and skip if directory already specified
                  existing_element = self.process_data_elements.active.find_by_directory(dir_subregion)
                  if existing_element
                    # element is already registered, so skip
                    if !reception_id.blank?
                      existing_element.reception_id = reception_id
                      existing_element.save!
                    end
                    next
                  else
                    if Dir[File.join(dir_subregion,'*')].empty?
                      path = Dir[File.join(dir_subregion,'*')]
                      raise "#{dir_subregion} has no content!"
                    end
                    # create new element by subregion
                    ProcessDataElement.transaction do
                      element = self.process_data_elements.new
                      element.split_by_value = split_by_value.upcase
                      element.process_data_specification_id = process_data_specification.id
                      element.directory = element.generate_directory
                      element.reception_id = self.picked_reception_ids.split(',').first if !self.picked_reception_ids.blank?
                      element.reception_id = reception_id if !reception_id.blank?
                      element.remark = "Created with file sync."
                      element.save!
                      trigger_change = true

                    log = ProcessDataLine.new
                    log.data_set_id = self.id
                    log.log_forward = "Added #{element.process_data_specification.name} #{split_by_value.upcase} from reception #{element.reception_id}."
                    log.user_id = user_obj.id if user_obj
                    log.process_data_element_id = element.id
                    log.save!
                    end
                  end
                end
              end
            else
              # check and skip if directory already specified
              trailing_slash = process_data_specification.directory_level_yn ? "/" : ""
              existing_element = self.process_data_elements.active.find_by_directory(dir.gsub(self.generate_directory,'')+trailing_slash)
              if existing_element
                # element is already registered, so skip
                next
              else
                if Dir[File.join(dir,'*')].empty?
                  raise "#{dir} has no content!"
                end
                # create new element
                ProcessDataElement.transaction do
                  element = self.process_data_elements.new
                  element.process_data_specification_id = process_data_specification.id
                  element.directory = element.generate_directory
                  element.reception_id = self.picked_reception_ids.split(',').first if !self.picked_reception_ids.blank?
                  element.reception_id = reception_id if !reception_id.blank?
                  element.remark = "Created with file sync. by #{username}"
                  element.save!
                  trigger_change = true

                  log = ProcessDataLine.new
                  log.data_set_id = self.id
                  log.log_forward = "Added #{element.process_data_specification.name} from reception #{element.reception_id}."
                  log.user_id = user_obj.id if user_obj
                  log.process_data_element_id = element.id
                  log.save!
                end
              end
            end
          end

          # remove unused elements (non existing directories)
          self.process_data_elements.active.each do |element|
            if !File.directory?(element.full_directory)
              trigger_change = true
              element.remove_and_sync(user_obj)
            end
            # Check for duplicate elements within item and remove the last one.
            chk_dupicates = self.process_data_elements.active.where(directory:element.directory)
              chk_dupicates.last.destroy if chk_dupicates.size > 1
          end
        else
          raise "Item directory '#{self.generate_directory}' has no filled element directories!"
        end
      else
        raise "Item directory '#{self.generate_directory}' does not exist!"
      end
  if trigger_change
    self.status = STATUS_IN_PROGRESS
    self.save
  end
  return true
  end

  def involved_receptions
    @reception_ids = []
    @reception_ids << self.process_data_elements.active.collect {|de|de.reception_id if de.removed_yn == false && !de.reception_id.blank?}.uniq.compact

    if @reception_ids.reject(&:empty?).size > 0
      @receptions = Reception.find(@reception_ids)
    else
      @receptions = []
    end
    return @receptions + self.direct_receptions
  end

  def self.allocate_data(tags)
     DataSet.tagged_with(tags)
  end

  def allow_request_review?
    self.status < STATUS_PRODUCTION && self.status >= STATUS_IN_PROGRESS
  end

  def allow_review?
    if ( self.status == STATUS_IN_PROGRESS || self.status == STATUS_REVIEW ) && !self.review_user_id.blank? && !self.review_request_user_id.blank?
      return true
    else
      return false
    end
  end

  def allow_production_order?
    if self.status == STATUS_TEST || self.status == STATUS_PRODUCTION
      return true
    else
      return false
    end
  end

  def allow_edit?
    jobs = self.conversion_jobs.outstanding
    if jobs.empty? && !in_use? && self.status != STATUS_REVIEW
      return true
    else
      return false
    end
  end

  def allow_remove?
    if self.status == STATUS_REMOVAL_CANDIDATE
      return true
    else
      return false
    end
  end

  def allow_removal_candidate?
    if self.status < STATUS_REMOVAL_CANDIDATE && allow_edit?
      return true
    else
      return false
    end
  end

  def allow_reception?
    self.status < STATUS_REMOVAL_CANDIDATE
  end

  def in_use?(msg=false)
    production_orders = self.production_orders.active.uniq
    if production_orders.empty?
      return '' if msg
      return false
    else
      return "Active production orders using this data set:\n" + production_orders.collect{|po|[po.id,po.name].join(':')}.join("\n") if msg
      return true
    end
  end

  private
    def default_values
      self.status ||= STATUS_DRAFT
    end



end
