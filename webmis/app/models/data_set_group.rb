class DataSetGroup < ActiveRecord::Base
  validates :company_abbr, :presence => true
  validates :data_release, :presence => true
  validates :region_code,  :presence => true
  
  has_many :data_set_group_lines
  has_many :data_sets, :through => :data_set_group_lines

  STATUS_ACTIVE                 = 500
  STATUS_REMOVED                = 999

  validates :data_release, uniqueness: { :scope => [:company_abbr, :data_type, :filling, :region_code, :suffix],
    :if => :active?, :message => "/ Supplier / Coverage / Data type combination already in use." }
  scope :active, ->  { where(:status => STATUS_ACTIVE)  }
  scope :removed, -> { where(:status => STATUS_REMOVED) }
  
  @@statusarray = Array.new
  @@statusarray[STATUS_ACTIVE]             = 'Active'
  @@statusarray[STATUS_REMOVED]            = 'Removed'
  
  after_initialize :default_values
  
  def status_string
    return @@statusarray[self.status]
  end

  def self.status_array
    @@statusarray.collect{|v|[v,@@statusarray.index(v)]if v}.compact
  end

  def active?
    self.status == STATUS_ACTIVE
  end

  def code
    code_a = []
    code_a << self.company_abbr if !self.company_abbr.blank?
    code_a << self.data_release if !self.data_release.blank? 
    code_a << self.region_code  if !self.region_code.blank?
    if !self.data_type.blank?
      if self.filling.blank?
        code_a << self.data_type
      else
        code_a << "#{self.data_type}[#{self.filling}]"
      end
    end
    code_a << self.suffix  if !self.suffix.blank?
    return !code_a.empty? ? code_a.join('|') : ""
  end

  def name
    self.code.gsub('|',' ').gsub('[',' - ').gsub(']','')
  end
  
  def self.find_by_code(p_company_abbr,p_region_code,p_data_release='latest',p_data_type='*',p_filling=nil,p_suffix=nil)
    conditions = "1=1"
    conditions_string_ary = []
    
    conditions_string_ary << conditions
    conditions_string_ary << "data_set_groups.company_abbr = '#{p_company_abbr}'"
    conditions_string_ary << "data_set_groups.region_code  = '#{p_region_code}'"
    if p_data_release == 'latest'
      # latest already returned by using last ordered on data_release
    else
      conditions_string_ary << "data_set_groups.data_release = '#{p_data_release}'"
    end
    if p_data_type
      conditions_string_ary << "data_set_groups.data_type    = '#{p_data_type}'"
    end
    if p_filling
      conditions_string_ary << "data_set_groups.filling      = '#{p_filling}'"
    end
    if p_suffix
      conditions_string_ary << "data_set_groups.suffix       = '#{p_suffix}'"
    end
    conditions = conditions_string_ary.join(' AND ')
    selection =  DataSetGroup.active.where("#{conditions}").order(:data_release)
    if selection && selection.size > 1 && p_data_type == '*'
      filtered_selection = selection.where( "data_set_groups.data_type IS NULL OR data_set_groups.data_type = ''" )
      if filtered_selection
        return filtered_selection.last
      end
    else
      return selection.last
    end
  end

  def sync_lines(ds_ids=[])
    new_list = ds_ids.sort

    DataSetGroupLine.transaction do
      self.data_sets.each do |ds|
        # Remove unwanted
        if !new_list.include?(ds.id)
          match = self.data_set_group_lines.where(:data_set_id => ds.id)
          match.each do |dsgl|
            dsgl.destroy
          end
        end
        # Keep existing
        if new_list.include?(ds.id)
          new_list.delete_if {|id| id == ds.id }
        end
      end
      # Create remaining as new line
      new_list.each do |ds_id|
        new_line = DataSetGroupLine.new(:data_set_id => ds_id,:data_set_group_id => self.id)
        new_line.save!
      end
    end

  end

  def duplicate
    new_group = self.dup
    DataSetGroupLine.transaction do
      new_group.description = "Cloned from data set group \"#{self.name}\"."
      uniqstr = Digest::SHA1.hexdigest(Time.now.to_s)
      new_group.suffix = new_group.suffix.to_s + "_CLONE_#{uniqstr}" # makes group unique
      new_group.status = STATUS_ACTIVE
      new_group.save!
      self.data_set_group_lines.each do |line|
        new_line = line.dup
        new_line.data_set_group_id = new_group.id
        new_line.save!
      end
    end
    return new_group
  end


  def production_order_allowed?(project_id)
    all_data_sets_size = self.data_sets.size
    if project_id == Project.default.id
      if all_data_sets_size == self.data_sets.where(:status => DataSet::STATUS_PRODUCTION).size
        return true
      end
    else
      if all_data_sets_size == self.data_sets.where("data_sets.status = #{DataSet::STATUS_PRODUCTION} OR data_sets.status = #{DataSet::STATUS_TEST}").size
        return true
      end
    end
    return false
  end
  
  private
    def default_values
      self.status ||= STATUS_ACTIVE
    end  
end
