class DataSetGroupLine < ActiveRecord::Base 
  belongs_to :data_set
  belongs_to :data_set_group
  
  validates :data_set_id, :presence => true
  validates :data_set_group_id, :presence => true
end
