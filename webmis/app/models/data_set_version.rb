class DataSetVersion < ActiveRecord::Base
  belongs_to :data_type
  belongs_to :company_abbr
  belongs_to :data_release
  belongs_to :filling
  belongs_to :region
  belongs_to :coverage
  has_many :regions, :through => :coverage
end
