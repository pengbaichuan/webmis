class DataType < ActiveRecord::Base
  version_fu

  after_initialize :defaults
  has_many :product_data_set_infos
  has_many :process_data_specifications

  has_many :data_type_fillings, :dependent => :destroy
  has_many :fillings, :through => :data_type_fillings

  scope :active, -> { where('(isactive is null or isactive = true) AND (status != "obsolete")').order(:name) }
  include HandleVersioning
  validates :status, :workflow => true
  validates :name, :presence => true, :uniqueness => true
  validates :validation_executable, :presence => true, :if => :automatic_validation?

  def automatic_validation?
    return @automatic_validation_yn
  end

#  for now no required review as we are still developing
  @@attrs_requiring_review = []

  def self.attrs_requiring_review
    return @@attrs_requiring_review
  end

  def defaults
    self.isactive = true if new_record?
  end

  def meta_string
    "design_id=#{design_id.to_s}|type=" + name.to_s + "|description=" + description.to_s
  end
  
  def design_id
    "DT_#{id}" 
  end
  
  def self.select_tag
    self.active.collect {|dt|[dt.name,dt.id]}
  end

  def update_filling(old_filling_ids,new_filling_ids)
    # Remove filling
    old_filling_ids.each do |ofi|
      if !new_filling_ids.include?(ofi.to_s)
        self.data_type_fillings.where(:filling_id => ofi ).last.destroy
      end
    end
    
    # Add new filling
    new_filling_ids.each do |nfi|
      if !old_filling_ids.include?(nfi.to_i)
        new_data_type_filling = DataTypeFilling.new(:filling_id => nfi,:data_type_id => self.id)
        new_data_type_filling.save
      end
    end
    
  end
  
  
end
