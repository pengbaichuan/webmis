class DataTypeFilling < ActiveRecord::Base
  belongs_to :data_type
  belongs_to :filling

  validates :data_type_id, :uniqueness => { :scope => :filling_id, :message => "relation already exist" } , :presence => true
  validates :filling_id, :presence => true

end
