class Datasource < ActiveRecord::Base
  version_fu 
  has_many :datasource_internalcontacts
  has_many :contracts
  has_many :users, :through => :datasource_internalcontacts
  belongs_to :supplier
  has_many :data_releases
  has_many :external_roles
  has_many :datasource_types
  has_many :conversions
  has_many :internet_datasources
  has_one :coverage
  has_many :datasource_modules
  has_many :app_modules, :through => :datasource_modules

  validates :name, :presence => true
  validates :abbr, :uniqueness => true, :allow_blank => true

  def app_module_ids
    self.app_modules.map(&:id)
  end

  def app_module_ids=(new_ids)
    ids = (new_ids || []).reject(&:blank?)
    old_ids = self.app_module_ids
    self.save
    self.transaction do
      DatasourceModule.destroy_all({ :app_module_id => old_ids - new_ids, :datasource_id => self.id })
      (new_ids - old_ids).each do |app_module_id|
        self.datasource_modules.create!(:app_module_id => app_module_id)
      end
    end
  end

  def self.find_all_for_module(app_module)
    Datasource.joins("join datasource_modules on datasource_modules.datasource_id = datasources.id
                      join app_modules on datasource_modules.app_module_id = app_modules.id").where("app_modules.name = '#{app_module}'").order("datasources.name")
  end
end
