class DatasourceInternalcontact < ActiveRecord::Base
  belongs_to :datasource
  belongs_to :user

  validates :datasource_id, uniqueness: {scope: :user_id}
 end
