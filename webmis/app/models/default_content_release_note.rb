class DefaultContentReleaseNote < ActiveRecord::Base
  belongs_to :product_category
  belongs_to :user
  validates  :title, :product_category_id, :presence => true
  default_scope -> {where('status < 100')}
  scope :active, -> {where(:status => 10)}
  
  def self.include_deleted_in
   DefaultContentReleaseNote.with_exclusive_scope { yield }
  end  
  
  @@statusarray     = Array.new
  @@statusarray[0]   = 'Draft'
  @@statusarray[10]  = 'Active'
  @@statusarray[30]  = 'Inactive'
  @@statusarray[100] = 'Removed'

  def self.status_hash
    s_hash = Hash.new
    @@statusarray.each do |s|
      if !s.nil?
        s_hash[s] = @@statusarray.index(s)
      end
    end
    return s_hash.sort_by { |k,v| v }
  end
  
  def status_string
    @@statusarray[self.status]
  end  
end
