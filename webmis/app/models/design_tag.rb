class DesignTag < ActiveRecord::Base
  acts_as_tree
  validates :name, :uniqueness => {:scope => :parent_id}

  scope :data_types, -> {where({:tag_type => 'DataType'})}
  scope :suppliers, -> {where({:tag_type => 'Supplier'})}
  scope :data_releases, -> {where({:tag_type => 'DataRelease'})}
  scope :others, -> {where({:tag_type => 'Other'})}
  scope :regions, -> {where({:tag_type => 'Region'})}
  scope :filling, -> {where({:tag_type => 'Filling'})}

  @@datatypes =  { "Other" => "Not specified additional tags",
    "DataType" => 'Type of Data like MNT',
    "DataRelease" => "Release of input data",
    "Supplier" => "Data supplier",
    "Region" => "Countries or States",
    "Filling" => "Content filling of a datatype like GDF-ECC"}

  def self.data_types_select(desc=false)
    if desc
      @@datatypes.collect{|k,v|[k,v]}
    else
      @@datatypes.keys.sort.collect{|k|[k,k]}
    end
  end

  def self.tagstrings
    leaves = DesignTag.all.order(:parent_id).collect{|x|x.self_and_ancestors}
    leavestrings = []
    leaves.each do |leave|
      leavetrace = ""
      lastid = -1
      leave.each do |elem|
        if leavetrace.size > 0
          leavetrace = elem.name.to_s + " - " + leavetrace
        else
          leavetrace = elem.name.to_s
        end
        if elem.description.to_s.size > 0
          leavetrace =  elem.description.to_s   + "/" + leavetrace
        end
        lastid = elem.id if lastid == -1
      end
     
      leavestrings << [leavetrace,lastid.to_s]
    end
    return leavestrings
  end
  
  def self.leavestrings
    leaves = self.leaves.collect{|x|x.self_and_ancestors}
    leavestrings = []
    leaves.each do |leave|
      lastid = -1

      leavetrace = ""
      leave.each do |elem|
        leavetrace = elem.name + "-" + leavetrace
        lastid = elem.id if lastid == -1
      end
      leavetrace = leavetrace[0..-2]
      leavestrings << [leavetrace, lastid.to_s]
    end
    return leavestrings
  end
  
  def self.add_design_tags(reception)
    supplier = reception.data_release.company_abbr.ms_abbr_3
    datatype = reception.data_type.name
    release_name  = reception.data_release.name
      
    # Sync DesignTag with Reception
    # reception.tags.collect{|t|DesignTag.find_or_create_by_name(t.name)}
    reception.tags.collect{|t|DesignTag.find_by_name(t.name)}
    reception.tag_list = [supplier, datatype, release_name] + reception.tags.collect{|t|t.name}
    reception.save
      
    # unique across tags
    dt_supplier = DesignTag.find_by_name(supplier)
    if !dt_supplier
      dt_supplier = DesignTag.create(:name => supplier, :description => reception.data_release.company_abbr.company_name, :tag_type => 'Supplier')
    end
      
    dt_datatype = DesignTag.find_or_create_by_path [supplier, datatype]
    dt_datatype.description =  reception.data_type.description
    dt_datatype.tag_type =  'DataType'
    dt_datatype.save
      
    release = DesignTag.find_or_create_by_path [supplier, datatype, release_name]
    release.description = ''
    release.tag_type = 'DataRelease'
    release.save
  end  
  
  def self.import_releases
    receptions = Reception.all.order('id desc')
    
    receptions.each do |reception|
      supplier = reception.data_release.company_abbr.ms_abbr_3
      datatype = reception.data_type.name
      release_name  = reception.data_release.name
      # release = DesignTag.find_or_create_by_path [supplier, datatype, release_name]
      
      # unique across tags
      dt_supplier = DesignTag.find_by_name(supplier)
      if !dt_supplier
        dt_supplier = DesignTag.create(:name => supplier, :description => reception.data_release.company_abbr.company_name, :tag_type => 'Supplier')
      end
      
      dt_datatype = DesignTag.find_or_create_by_path [supplier, datatype]
      dt_datatype.description =  reception.data_type.description
      dt_datatype.tag_type =  'DataType'
      dt_datatype.save
      
      
      release = DesignTag.find_or_create_by_path [supplier, datatype, release_name]
      release.description = '' 
      release.tag_type = 'DataRelease'
      release.save
      
      
      
      # import description!!!!!!!!!!!!!!! here use create instead of by path supplier =
      # DesignTag.find_or_create_by_path
      
    end
    
  end
  
  def self.product_lines
    DesignTag.find_by_name("Platform").leaves
  end
  
  
    
  def short_string
    lastid = -1
    leave = self.ancestors
    leavetrace = ""
    leave.each do |elem|
      leavetrace = elem.name + "-" + leavetrace
      lastid = elem.id if lastid == -1
    end
    leavetrace = leavetrace[0..-2]
  end
  
  def short_string_and_self
    lastid = -1
    leave = self.ancestors
    leavetrace = name
    leave.each do |elem|
      leavetrace = elem.name + "-" + leavetrace
      lastid = elem.id if lastid == -1
    end
    leavetrace = leavetrace[0..-1]
  end
  
  def self_and_ancestor_strings
    res =[]
    self.self_and_ancestors.each do |item|
      res << item.name
    end
    
    return res.reverse
  end
  
  def short_string_and_self_spaced
    lastid = -1
    leave = self.ancestors
    leavetrace = name
    leave.each do |elem|
      leavetrace = elem.name + "-" + leavetrace
      lastid = elem.id if lastid == -1
    end
    leavetrace = leavetrace[0..-1]
  end
  
  def extended_string
    # description to be populated
    leave = self.ancestors
    leavetrace = ""
    leave.each do |elem|
      if leavetrace.size > 0
        leavetrace = elem.name.to_s + " - " + leavetrace
      else
        leavetrace = elem.name.to_s
      end
      if elem.description.to_s.size > 0
        leavetrace =  elem.description.to_s   + "/" + leavetrace
      end
    end
    return leavetrace
  end
  
  def meta_string
    if parent
      "design_id=#{design_id}|type=#{name}|supplier=#{parent.name}|description=#{description}"
    else
      "design_id=#{design_id}|type=#{name}|description=#{description}"
    end
  end
  
  def human_readable_string
    data_type.name + " " + data_type.description + " (" +  company_abbr.name + ")" 
  end
  
  def design_id
    "DESIGNTAG_#{id}"
  end

  def extended_description
    if parent
      name.to_s + " " + description.to_s + " (" +  parent.name.to_s + "/" + parent.description.to_s + ")"
    else
      name.to_s + " " + description.to_s
    end
  end
  
  def self.find_by_supplier_and_datatype(sup,dt)
    dt = DesignTag.find_by_path([sup,dt])
      
  end
  
  
  def  self.import_bundles
    # get cvtool releases
    cvtools = Pathname.glob("/data/id/release/Bundles/*/").map { |i| i.basename.to_s } 
    cvtools.each do |tool|
      dt_datatype = DesignTag.find_or_create_by_path (['Bundle'] + tool.split("_"))
      dt_datatype.description =  tool
      dt_datatype.tag_type =  'CVTOOL'
      dt_datatype.save
      
      bundles = Pathname.glob("/data/id/release/Bundles/#{tool}/*/").map { |i| i.basename.to_s } 
      bundles.each do |bundle|
        
        dt_datatype = DesignTag.find_or_create_by_path (['Bundle'] + tool.split("_") +  bundle.split("_"))
        dt_datatype.description =  bundle
        dt_datatype.tag_type =  'CVTOOL_BUNDLE'
        dt_datatype.save
        
      end
    end
  end
     
   
end
