class DhiveMetadata < ActiveRecord::Base
  #establish_connection 'dhive'
  #set_table_name "dh_metadata"

  def self.set_db(result_db)
     DhiveMetadata.establish_connection(
        :adapter  => "sqlite3",
        :database => result_db,
        :encoding => 'utf8',
        :timeout => 10000
      )
  end

  def get_version
    d = DhiveMetadata.where("MetaTag = 'DH_VERSION'").first
    return d.MetaValue
  end
  


end
