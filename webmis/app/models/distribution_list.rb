class DistributionList < ActiveRecord::Base
  has_many :distribution_list_members
  has_many :production_orders
  belongs_to :product_category
  has_many :outbound_orders
  has_many :external_contacts, :through => :distribution_list_members
  scope :active, -> {where(:active_yn => true)}

  def electronic_delivery_email_list
    self.distribution_list_members.where(:electronic_delivery_yn => true).collect{|dlm|dlm.external_contact.email if dlm.external_contact}.join(',')
  end

  def amount_hard_copies
    a = distribution_list_members.collect{|x|x.hard_copy_amount if !x.hard_copy_amount.nil? }
    if !a.empty?
      return a.compact.sum
    else
      return 0
    end
  end
end
