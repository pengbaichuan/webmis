class DistributionListMember < ActiveRecord::Base
  belongs_to :distribution_list
  belongs_to :external_contact
  has_many :outbound_orders
  
  validates :external_contact_id, :presence => true
  validates :hard_copy_amount, :presence => true, :if => :hard_copy_delivery?
  
  def hard_copy_delivery?
    self.hard_copy_delivery_yn == true
  end
  
  
end
