class Document < ActiveRecord::Base
  version_fu

  def data_file=(input_data)
    self.filename = input_data.original_filename
    self.content_type = input_data.content_type.chomp
    self.binary_data = input_data.read
    digest = Digest::MD5.new
    digest << self.binary_data
    self.checksum = digest.hexdigest
  end

  def get_latest_version
    DocumentVersion.where("document_id = #{self.id} and version = #{self.version}").first
  end

  def get_version(version)
    DocumentVersion.where("document_id = #{self.id} and version = #{version}").first
  end

  def find_related_products
    Includeddocument.where("document_id = #{self.id}")
  end

  def find_related_productsets
    Includedproductsetdocument.where("document_id = #{self.id}")
  end

end
