class Environment < ActiveRecord::Base
  has_many :projects

  def self.development_environment_id
    Environment.find_or_create_by(name: 'development').id
  end
end
