class Executable < ActiveRecord::Base
  version_fu
  attr_accessor :altid, :input_hash, :output_hash, :parameters_hash
  has_many :bundle_contents
  has_many :toolspec_featuregroups
  has_many :toolspec_parameters, :through => :toolspec_featuregroups

  scope :improvements, -> {where(is_improvement_process:true)}
  scope :compilers, -> {where(is_improvement_process:[false,nil])}

  include HandleVersioning
  validates :status, :workflow => true
  validates :name, :uniqueness => true
  after_find :load_hash
  
  def load_hash
    if self.input && self.input.size > 0
      @input_hash = JSON.parse(self.input)
    end
    
    if self.output && self.output.size > 0
      @output_hash = JSON.parse(self.output)
    end

    if self.parameters && self.parameters.size > 0
      @parameters_hash = JSON.parse(self.parameters)
    end
  end


#  for now no required review as we are still developing
#  @@attrs_requiring_review = [:name, :input, :output, :parameters]
  @@attrs_requiring_review = []

  def self.attrs_requiring_review
    return @@attrs_requiring_review
  end

  def self.find_by_outputformat(format)
    exes = []
    executables = Executable.all

    executables.each do |exe|
      if  JSON.parse(exe.output).index(format)
        exes <<  exe
      end
    end
    return exes
  end

  def featuregroups_for_bundle(conversiontool, cvtool_bundle)
      self.toolspec_featuregroups.where("conversiontool_id = #{conversiontool.id} and cvtool_bundle_id = #{cvtool_bundle.id}")
  end

  def parameters_for_bundle(conversiontool, cvtool_bundle)
    self.toolspec_parameters.where("conversiontool_id = #{conversiontool.id} and cvtool_bundle_id = #{cvtool_bundle.id}")
  end
  
  def parameters_superset
    self.toolspec_parameters.select("toolspec_parameters.name, toolspec_parameters.description, toolspec_parameters.datatype, toolspec_parameters.default,toolspec_parameters.mandatory").all.collect{|x|x.attributes}.uniq
  end



  # generate a list which can be used in a select box
  # the label is the executable name, the value the parameters JSON
  def self.parameters_option_list
    option_list = []
    self.where("status != 'obsolete' and name is not null").sort_by{|r| r.name}.each do |inst|
      option_list << [inst.name, inst.to_json]
    end
    option_list
  end

  def design_id
    "EXE_" + name
  end

  def param_names
    #list all parameter names
    names = []
    if (self.parameters && self.parameters.size > 3)
      paramhash = JSON.parse(self.parameters)
      listexes = paramhash.instance_of?(Array) ? paramhash : paramhash["parameters"]
      listexes.each do |param|
        names << param["name"]
      end
    end
    return names
  end

  def self.param_list
    #list of all parameters

    parameters = []

    Executable.all.each do |exe|
      if !exe.obsolete? && exe.parameters && exe.parameters.size > 3
        paramhash = JSON.parse(exe.parameters)
        listexes = paramhash.instance_of?(Array) ? paramhash : paramhash["parameters"]
        listexes.each do |param|
          param["exe"] = exe.name
          param["label"] = exe.name + " - " + param["name"]
          param["executable_id"] = exe.id
          param["version"] = exe.version
        end
        parameters += listexes
      end
    end

    return parameters.uniq.sort{|x,y|x["label"] <=> y["label"]}
  end

  def meta_string
     "design_id=#{design_id}|name=#{name}"
  end

  def input_formats
    if !@input_formats
      @input_formats = JSON.parse(input)
    end
    return @input_formats
  end

  def output_formats
    if !@output_formats
      @output_formats = JSON.parse(output)
    end
    return @output_formats
  end

  def isimprovementprocess?
    if output_formats == input_formats
      return true
    else
      return false
    end
  end

  def validate_version_string

  end

  def preprocessors
   preds = []
   exes = Executable.all
   exes.each do |exe|
     input_formats.each do |iformat|
      if exe.output_formats.index(iformat)
        preds << exe
      end
     end
   end
   return preds
  end

  def postprocessors
   posts = []
   exes = Executable.all
   exes.each do |exe|
     input_formats.each do |iformat|
      posts << exe if exe.input_formats.index(iformat)
     end
   end
   return posts
  end

  def can_process_format?(input)
   if input_formats.index(input)
     return true
   else
     return false
   end
  end

  def can_generate_format?(output)
   if output_formats.index(output)
     return true
   else
     return false
   end
  end

  def self.tags(context={"format" => "RDB", "file" => "/data/ops2/backoffice/prod/rdb/eur/o_20141205-120215-50178-eur_her_2014_q2_20141205d/eur_her_2014_q2_20141205d-zlib.db"} )
    result = []

    if context["format"] == "RDB"
      begin     
        result = `sq3dh #{context["file"]} "select distinct region_code,description from clp_cfg_regions r, clp_cfg_region_def d ON r.region_id = d.region_id where ((region_code like '___' and admin_type = 1111) or (region_code like '___D' and admin_type = 1111) or (region_code like '___-__' and admin_type = 1112))"`.split("\n").map{|x|x.split("|")}
        newresults = []
        unknown_region_codes = []
        result.each do |code|
          known_region = Region.find_by_region_code(code[0])
          if known_region && known_region.ruby_type != 'UpdateRegion'
            newresults << [code[1], code[0]]
          else
            unknown_region_codes << code[0]
          end
        end
      
      rescue => e
        puts e.message
        puts e.backtrace
        raise  "TAGS CANT BE ACCESSED IN RDB - " + e.message
      end
    end
    return [ newresults, unknown_region_codes ]
  end

  def self.find_by_parameter(parameter)
    exes = Executable.where("parameters like '%#{parameter}%'")
    return exes
  end

  def parameter_for_resource(input_tags)
    param_for_resource = nil
    parameters = JSON.parse(self.parameters)["parameters"]
    parameters.each do |param|
      if param["allocate"] && param["allocate_tags"].index(input_tags)
        param_for_resource = param
      end
    end

    return param_for_resource
  end

  def synchronize_with_toolspec(cvtool_bundle, conversiontool, tool_name, tool_release, release_name, executable_contents, contents = nil)
    # find the previous import if it exists, and create a new bundle_contents otherwise.
    exec_search_str = "`executable_id`='#{self.id}'"
    bundle_search_str = cvtool_bundle && cvtool_bundle.id ? "`cvtool_bundle_id`='#{cvtool_bundle.id}'" : "`cvtool_bundle_id` is null"
    conversiontool_search_str = conversiontool && conversiontool.id ? "`conversiontool_id`='#{conversiontool.id}'" : "`conversiontool_id` is null"
    tool_name_search_str = tool_name ? "`tool_name`='#{tool_name}'" : "`tool_name` is null"
    tool_release_search_str = release_name ? "`tool_release`='#{release_name}'" : "`tool_release` is null"
    bundle_content = BundleContent.where([exec_search_str, bundle_search_str, conversiontool_search_str, tool_name_search_str, tool_release_search_str].join(" AND ")).first
    if bundle_content.nil?
      bundle_content = BundleContent.new(:name => self.name, :executable_id => self.id)
      bundle_content.cvtool_bundle_id = cvtool_bundle.id if cvtool_bundle
      bundle_content.conversiontool_id = conversiontool.id if conversiontool
      bundle_content.tool_name = tool_name if tool_name
      bundle_content.tool_release = release_name if release_name
    end
    bundle_content.toolspec_contents = executable_contents.to_json
    # note that if bundle_content is not null, the toolspec has already been imported before. You still want to import the executable as you might have made some 
    # manual changes which should be reversed. The bundle_content does not have to be imported again, but it is easier to just always import it again.
    # although the bundle_content should not change in that case, we don't currently validate that assumption.
    self.comment = "updated by toolspec importer (#{release_name})"

    # new toolspec import
    self.is_improvement_process = executable_contents["is_improvement_process"]
    bundle_content.is_improvement_process = executable_contents["is_improvement_process"]

    bundle_content.description = executable_contents["description"] unless executable_contents["description"].blank?
    self.description = executable_contents["description"] unless executable_contents["description"].blank?
    
    # handle the input array
    bundle_content.input = executable_contents["input"].to_json
    self.input = merge_datatypes(self.input_formats, executable_contents["input"]).to_json

    #handle the output array
    bundle_content.output = executable_contents["output"].to_json
    self.output = merge_datatypes(self.output_formats, executable_contents["output"]).to_json

    # handle the parameters array
    bundle_content.parameters = executable_contents["parameters"].to_json
    executable_parameters = executable_contents["parameters"]
    if executable_parameters && executable_parameters.size > 0
      parameters = self.parameters.blank? ? [] : JSON.parse(self.parameters)
      parameters = parameters["parameters"] unless parameters.instance_of?(Array)

      # transform the current parameters array into a hash for easy searching
      parameters_hash = {}
      parameters.each do |parameter|
        parameters_hash[parameter["name"]] = parameter
      end

      #build a new parameters array
      parameters_new = []
      executable_parameters.each do |parameter|

        new_parameter = parameter
        if parameters_hash.has_key?(new_parameter["name"])
          old_parameter = parameters_hash[new_parameter["name"]]
          new_parameter['allocate'] = old_parameter['allocate']
          new_parameter['allocate_tags'] = old_parameter['allocate_tags']
          new_parameter['visibility'] = old_parameter['visibility']

          parameters_hash.delete(new_parameter["name"])
        else
          # new parameter
          if new_parameter["data_type"] == 'path'
            new_parameter['allocate'] = true
          else
            new_parameter['allocate'] = false
          end
          new_parameter['allocate_tags'] = ""
        end
        parameters_new << new_parameter
      end

      #import the remaining parameters
      parameters_hash.each{ |_, old_parameter| parameters_new << old_parameter }

      # store the new parameters in JSON format in the executable
      self.parameters = { 'parameters' => parameters_new }.to_json
    end
    
    # don't create a new version if only the updated_by and comment attributes have changed
    self.versioned_columns = self.versioned_columns - ['updated_by', 'comment']
    self.updated_by = 'toolspec importer'
    bundle_content.save!
    self.save!
  end

:private
  def merge_datatypes(original_datatypes, new_datatypes)
    if new_datatypes.nil? || new_datatypes.empty?
      return original_datatypes
    else
      original_datatypes.each do |datatype|
        if datatype.instance_of?(String)
          e_datatype = datatype.to_s.upcase
        else
          e_datatype = datatype["datatype"].to_s.upcase
        end
        new_datatypes.delete_if{|e| e.map{|x,y| {x => y.to_s.upcase}}.index{|x| x.has_value?(e_datatype)}}
      end

      new_datatypes.each do |formathash|
        # validate format
        dt = DataType.find_by_name(formathash["datatype"])
        if dt
          dt = dt.approved
        else
          dt_a = DataType.where("description like '%#{formathash["datatype"]}%'")
          dt = dt_a[0].use_latest_baseline if dt_a[0]
        end
        if dt && dt.status == 'approved'
          original_datatypes << {"datatype" => dt.name, "id" => dt.id, "version" => dt.version, "mandatory" => false, "script_tag" => ''}
        else
          self.comment = "ERROR, datatype '#{formathash}' not approved or available. " + self.comment
          self.status = "draft" if self.status = 'approved'
        end
      end
    end
    return original_datatypes.uniq
  end


end
