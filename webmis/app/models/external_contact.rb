class ExternalContact < ActiveRecord::Base

  belongs_to :company
  has_many :distribution_list_members
  has_many :distribution_lists, :through => :distribution_list_members
  validates :last_name ,  :presence => true

  STATUS_ACTIVE   = 100
  STATUS_OBSOLETE = 999

  @@statusarray = Array.new
  @@statusarray[STATUS_ACTIVE]     = 'Active'
  @@statusarray[STATUS_OBSOLETE]   = 'Removed'
  
  scope :active, -> {where(:status => STATUS_ACTIVE) }

  def status_string
    return @@statusarray[self.status] if self.status
  end

  def change_status(status)
    case status
    when "#{STATUS_ACTIVE}"
      if self.status != status
        self.status = status
      end
    when "#{STATUS_OBSOLETE}"
      if self.status != status && self.remove_allowed?
        self.status = status
      end
    end
    self.save
  end

  def self.find_all_dist(nameorder="last")
    if nameorder != "last"
      order = "last_name"
    else
      order = "first_name"
    end
    return ExternalContact.active.where("email != '' AND email IS NOT NULL").order(order)
  end

  def full_name(incl_salutation = false)
    full_name  = ''
    full_name += "#{self.title}" if incl_salutation && !self.title.blank?
    full_name += "#{self.salutation} " if incl_salutation && !self.salutation.blank? && self.title.blank?
    full_name += "#{self.first_name} " if !self.first_name.blank?
    full_name += "#{self.last_name} "  if !self.last_name.blank?
    if full_name == ''
      full_name = '-'
    end
    return full_name.strip
  end

  def self.salutations
    ["","Mr.","Mrs.","Miss","Ms.","Dr.","Prof.","Rev."]
  end

  def remove_allowed?
    self.status != STATUS_OBSOLETE
  end

end