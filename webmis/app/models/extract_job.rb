class ExtractJob < Job
  belongs_to :data_set, :foreign_key => :reference_id

  def perform
    reception_to_process_data
  end

  def reception_to_process_data
    begin
      # puts "Executing extract job perform for  " + self.id.to_s + " lock version : " + self.lock_version.to_s
      return unless allow_status_change?(STATUS_PROCESSING) && self.file_system_status == FILE_SYSTEM_STATUS_FREE
      clear_scripts

      scriptfile = File.expand_path("#{ConfigParameter.get("dakota_scripts_dir", self.project_id.to_s)}/extract_#{id.to_s}")
      File.open(scriptfile, 'w') {|f| f.write(self.run_script) }
      self.run_command = "bash #{scriptfile}"

      self.start_time = DateTime.now
      self.finish_time = nil

      execscr = "#! /bin/bash\r\n"
      execscr = execscr + "echo PID: $$ > #{scriptfile}.log\n"
      execscr = execscr + "export PROCESSID=$$\n"
      execscr = execscr + "#{self.run_command} >> #{scriptfile}.log\n"
      execscr = execscr + "echo $? >  #{scriptfile.to_s}.done\n"
      File.open(scriptfile + ".exec", 'w') {|f| f.write(execscr) }
      execstring = "source /data/users/#{ENV['prod_user']}/.profile;nohup sh #{scriptfile}.exec > #{scriptfile.to_s}.log 2>&1 &"
      ssh(execstring)

      # get pid
      pidexec= "cat  #{scriptfile}.log | grep '^PID' | cut -d' ' -f2"
      pid= ssh(pidexec).chomp

      begin
        self.run_pid = pid
        self.set_status(STATUS_PROCESSING)
        self.log_action("#{self.status_str} extract job.")
        save!
      rescue ActiveRecord::StaleObjectError => e
        # kill needed
        kill_linux_process(pid)
        clear_scripts
        puts "Failed setting status to PROCESSING for job " + self.id.to_s + " .. killing stated linux process " + pid.to_s + " on " + self.run_server.to_s
        puts e.message
        puts e.backtrace
      end
    rescue Exception => e
      puts e.message
      puts e.backtrace
      self.stdout_log = stdout_log + "------------\r\n" + e.message + "\r\n======\r\n" + e.backtrace.join("\r\n")
      self.fail_reason = e.message + "\r\n======\r\n" + e.backtrace.join("\r\n")
      clear_scripts
      self.set_status(STATUS_FAILED)
      self.set_file_system_status(FILE_SYSTEM_STATUS_FREE)
      self.log_action("Extract job #{self.status_str}. #{e.message}")
    ensure
      save!
    end
  end

  def automonitor
    return unless status == STATUS_PROCESSING  # only monitor running jobs
    scriptfile = File.expand_path("#{ConfigParameter.get("dakota_scripts_dir", self.project_id.to_s)}/extract_#{id.to_s}")
    return unless File.file?("#{scriptfile.to_s}.done") # still copying
    begin
      contents = File.read("#{scriptfile.to_s}.done")
      self.stdout_log = File.read("#{scriptfile.to_s}.log")
      if contents.to_i == 0
        # success
        self.exitcode = contents.to_i
        self.set_status(STATUS_COMPLETE)
        self.finish_time = DateTime.now
        self.stdout_log = self.stdout_log + "\r\nCOPY COMPLETED\r\n"
        self.save!
        # move job script files
        self.stored_scriptfiles.each do |script|
          dest = File.join(self.data_set.generate_directory,self.working_dir)
          move(script, dest)
        end
      else
        # failure
        self.exitcode = contents.to_i
        self.set_status(STATUS_FAILED)
        self.finish_time = DateTime.now
        self.stdout_log = self.stdout_log + "\r\nCOPY FAILED\r\n"
        clear_scripts
        self.save!
      end
    rescue Exception => e
      puts e.message
      puts e.backtrace
      self.stdout_log = self.stdout_log + "------------\r\n" + e.message + "\r\n======\r\n" + e.backtrace.join("\r\n")
      self.fail_reason = e.message + "\r\n======\r\n" + e.backtrace.join("\r\n")
      self.status = STATUS_FAILED
      self.finish_time = DateTime.now
      clear_scripts
    ensure
      self.save!
    end
  end

  def clear_scripts
    eid = File.expand_path("#{ConfigParameter.get("dakota_scripts_dir",self.project_id.to_s)}/extract_#{id.to_s}")
    FileUtils.rm_f("#{eid}")
    FileUtils.rm_f("#{eid}.done")
    FileUtils.rm_f("#{eid}.exec")
    FileUtils.rm_f("#{eid}.log")
  end

  def terminate(user='system')
    # for an extract job the terminate and cancel methods are equivalent
    cancel(user)
  end

  def cancel(user='system')
    if status == STATUS_PROCESSING
      kill_linux_process(run_pid) unless run_pid.to_s.blank?
      self.fail_reason = self.fail_reason.to_s + "\r\n======\r\nKilled by user #{user}.\r\n"
      self.stdout_log = self.stdout_log + "\r\n------------\r\nKilled by user #{user}.\r\n"
      self.finish_time = DateTime.now
    end
    set_status(STATUS_CANCELLED)
    self.log_action("Job cancelled", User.find_by_full_name(user))
    save!
  end

  def show_in_process_data_item?
    show = false

    if self.status != STATUS_CANCELLED
      show = true
      if self.status == STATUS_COMPLETE && !self.working_dir_exist?
        show = false
      end
    end
    return show
  end

  def working_dir_exist?
    return Dir.exist?(File.join(self.data_set.generate_directory,self.working_dir))
  end

  def stored_scriptfiles
    base = File.expand_path("#{ConfigParameter.get("dakota_scripts_dir", self.project_id.to_s)}/extract_#{id.to_s}")
    if File.exists?(base)
      return Dir.glob(base+'*')
    else
      return [] # returns empty array when no files found
    end
  end

end
