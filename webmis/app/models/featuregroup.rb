# Class that stores a featuregroup, tools and improvement processes are also stored as featuregroup with the additional isTool/isImprovProc bool set
class Featuregroup
    # general members
    @name
    @childfeaturegroups
    @parameters
    @description
    @@tocRef
    @@depth

    # tool members
    @isTool
    @input
    @output

    # Improv Proc memebers
    @isImprovProc

    # Constructor
    # name              Name of the featuregroup
    # description       Description of the featuregroup
    def initialize(name, description)
        @name = name
        @description = description
        @parameters = Array.new
        @childfeaturegroups = Array.new
        @isTool = false
        @isImprovProc = false
    end

    def name
      @name
    end

    def parameters
      @parameters
    end

    def childfeaturegroups
      @childfeaturegroups
    end

    def description
      @description
    end

    def input
      @input
    end

    def output
      @output
    end

    def children
      @childfeaturegroups
    end

    # Static method to set the Table Of Contents object reference
    def self.setToc(toc)
        @@tocRef = toc
    end

    # Static method to set the depth. This is used to keep track of the nesting of featuregroups
    def self.initDepth()
        @@depth = 0
    end
    # Mark featuregroup as tool
    # input     Contents of input element (from toolspec)
    # output    Contents of output element (from toolspec)
    def setTool(input, output)
        @isTool = true
        @input = input
        @output = output
    end

    # Mark featuregroup as improvement process
    def setImprovProc
        @isImprovProc = true
    end

    def is_improvement_process
      @isImprovProc
    end

    # Method that removes featuregroups that are children of the current feature group if they are empty
    def removeEmptyObjects
        @childfeaturegroups.each do |childfeatgroup|
            childfeatgroup.removeEmptyObjects
            if (childfeatgroup.isEmpty?)
                @childfeaturegroups.delete(childfeatgroup)
            end
        end
    end

    # Checks if the featuregroup contains any featuregroups or parameters
    def isEmpty?
        return (@childfeaturegroups.length + @parameters.length == 0)
    end

    # Append a featuregroup
    # featuregroup      Featuregroup to append as child to this featuregroup
    def addFeaturegroup(featuregroup)
        @childfeaturegroups.push(featuregroup)
    end

    # Append a parameter
    # parameter      Parameter to append as child to this featuregroup
    def addParameter(parameter)
        @parameters.push(parameter)
    end

    # Print only the children, and not itself (used for root of document)
    # writer                    Writer object
    # appendix                  Container for appendix data
    # configurationHash         Hash of parsed csv data
    def printChildrenToPDF(writer, appendix, configurationHash)
        # print parameters
        @parameters.each do |parameter|
            parameter.printToPDF(writer, appendix, configurationHash)
        end

        # print child feature groups
        @childfeaturegroups.each do |featuregroup|
            featuregroup.printToPDF(writer, appendix, configurationHash)
        end
    end

    # Print the feature group and all its children to the pdf
    # writer            Writer interface
    # appendix          The appendix hash
    # configurationHash The hash containing parsed configuration files
    def printToPDF(writer, appendix, configurationHash)
        # print header
        if (@isTool)
            writer.font_size 16
            @@tocRef.add_a_subcontent_page("Tool: #{@name}" , writer)
        elsif(@isImprovProc)
            writer.font_size 12
            writer.text "<b>" + @name + "</b>", :inline_format => true
        else
            writer.font_size 13
            # Generate new (sub)subsection
            if (@@depth > 0)
                @@tocRef.add_a_subsubcontent_page(@name, writer)
            else
                @@tocRef.add_a_subcontent_page(@name, writer)
            end
            @@depth += 1
        end

        writer.font_size 10

        # print description
        list = parseHTMLtablestring(@description)
        list.each do |element|
            if (element.is_a? String)
                # support html breaks
                element.gsub!("<br></br>", "\n")
                # Call writer.text on each seperate line
                element.split("\n").each do |line|
                    writer.text line, :inline_format => true
                end
            else
                # print table
                writer.table(element, :cell_style => {:inline_format => true}, :header => true)
                writer.move_down 5
            end
        end
        writer.move_down 5
        writer.indent_plus 20

        # print parameters
        @parameters.each do |parameter|
            parameter.printToPDF(writer, appendix, configurationHash)
        end

        # print child feature groups
        @childfeaturegroups.each do |featuregroup|
            featuregroup.printToPDF(writer, appendix, configurationHash)
        end

        if (!@isTool)
            @@depth -= 1
        end
        writer.indent_min 20
    end

    # Sort the parameters of this featuregroup and call sort on all featuregroups
    def sort
        # sort child feature groups
        @childfeaturegroups.each do |featuregroup|
            featuregroup.sort
        end

        # sort parameters
        @parameters.sort! do |l,r|
            l.name <=> r.name
        end
    end
end