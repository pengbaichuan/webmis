# To change this template, choose Tools | Templates
# and open the template in the editor.

class FileResourceUtil

  def self.add_file_resource_to_list(list,filespec)
    path_and_file = filespec["file_path"]
    if File.exists?(path_and_file) && File.lstat(path_and_file).symlink?
      realpath = File.realpath(path_and_file)
      filespec["realpath"] = realpath
    end
    list << filespec
  end
  
end
