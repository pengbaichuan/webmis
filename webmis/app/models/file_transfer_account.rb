class FileTransferAccount < ActiveRecord::Base
  belongs_to :company_abbrs
  has_many :outbound_orders
  has_many :shipping_specifications


  def transfer_types
    @transfer_types = [ "FTP", "HTTP", "SFTP", "HTTP", "Other" ]
  end

  def self.mapscape_ftp
    return CompanyAbbr.where("company_name LIKE 'Mapscape'").first.file_transfer_accounts
  end

end