class Filling < ActiveRecord::Base
  has_many :data_type_fillings, :dependent => :destroy
  has_many :data_types, :through => :data_type_fillings

  validates :name ,:presence => true, :uniqueness => true
  after_initialize :default_attribute_values

  scope :active, -> {where({ :active_yn => true, :removed_yn => false }).order(:name)}

  def default_attribute_values
    if new_record?
      self.active_yn  = true
      self.removed_yn = false
    end
  end
  
end
