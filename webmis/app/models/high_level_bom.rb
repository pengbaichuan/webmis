class HighLevelBom < ActiveRecord::Base 
  belongs_to :product
  belongs_to :product_location
  belongs_to :article
  has_many :article_licenses
  belongs_to :inlay, :class_name => "Reception",  :foreign_key =>  "inlay_reception_id"
  belongs_to :label, :class_name => "Reception",  :foreign_key =>  "label_reception_id"
  belongs_to :create_user, :class_name => "User", :foreign_key =>  "created_by_user_id"
  
  validates :internal_reference, :presence => true
  validates :product_id, :presence => true
  
  validate do |bom|
    if bom.status == 60
        bom.errors.add_to_base("No inlay available!")   if bom.inlay_reception_id.blank?
        bom.errors.add_to_base("No label available!")   if bom.label_reception_id.blank?
        bom.errors.add_to_base("No product available!") if bom.product_location_id.blank?
    end
  end
  
  @@statusarray     = Array.new
  @@statusarray[0]  = 'Draft'
  @@statusarray[60] = 'Released'
  
  def self.released
    where({:status => 60, :active => true, :deleted => false}).order(:internal_reference)
  end

  def articles
    Article.where(:high_level_bom_id => self.id)
  end  
  
def self.status_hash
  s_hash = Hash.new
  @@statusarray.each do |s|
    if !s.nil?
      s_hash[s] = @@statusarray.index(s)
    end
  end
  return s_hash.sort_by { |k,v| v }
end

def status_string
   @@statusarray[self.status] 
end  
   
end
