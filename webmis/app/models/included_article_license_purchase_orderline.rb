class IncludedArticleLicensePurchaseOrderline < ActiveRecord::Base
  belongs_to :article_license
  belongs_to :purchase_orderline
  belongs_to :shipping_orderline
  has_one :article, :through => :shipping_orderline
end
