class IncludedDatabaseTestRequest < ActiveRecord::Base
  belongs_to :conversion_database
  belongs_to :test_request
  has_one :conversion, :through => :conversion_database
  has_one :region, :through => :conversion  
end
