class Includedproduct < ActiveRecord::Base

  belongs_to :productset
  belongs_to :product
  attr_accessor :old_productset_id

  after_create :update_parent_version
  before_destroy :update_parent_version_destroy

  has_many :product_data_set_infos, :through => :products

  def show_ps
    return @old_productset_id
  end

  def update_parent_version
    if self.productset
      set = self.productset
      set.addchanges = "\nAdded product #{self.product.volumeid} to productset"
      set.setchanges = "\nAdded product #{self.product.volumeid} to productset"
      # #set.save
    end
  end

  def update_parent_version_destroy
    if self.productset
      set = self.productset
      set.addchanges = "\nRemoved product #{self.product.volumeid} from productset"
      set.setchanges = "\nRemoved product #{self.product.volumeid} from productset"
      set.save
  
    end
  end

  def productset_id=(id)
    @old_productset_id = self.productset_id
    super

  end

  def parts
    parts = Includedproduct.where("link_product_id = #{self.id}")
    return parts
  end

  def parent_product
    if !link_product_id
      return nil
    else
      return Includedproduct.find(:link_product_id).product
    end
  end

end

