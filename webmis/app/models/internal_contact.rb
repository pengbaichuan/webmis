class InternalContact < ActiveRecord::Base
  has_many :roles
  has_many :datasources, :through => :roles

  validates :first_name, :last_name, :presence => true
end
