class InternetDatasource < ActiveRecord::Base

  require 'net/http'
  require 'uri'
  require 'digest/sha2'
  
  version_fu #acts_as_versioned

  belongs_to   :datasource
  belongs_to :ping_event

  has_many :points
  validates :language_id, :presence => true

  def self.pingall
    InternetDatasource.all.each {|ids|
      PingEvent.new(
        :internet_datasource_id => ids.id,
        :updated                => Time.now,
        :is_online              => ids.is_active?
      ).save
    }
  end

  def is_active?
    !!Net::HTTP.get(URI.parse(location)) rescue false
  end

  def cat
    if self.category
      cat = nil
      begin
        cat = Category.find(self.category)
      rescue
      end
      return cat
    else
      return nil
    end
  end


  def getStatistics
    ActiveRecord::Base.connection.execute("select coverages.name,
                                                  count(*),
                                                  categories.feature_name
                                             from coverages,
                                                  points,
                                                  categories,
                                                  datasources,
                                                  internet_datasources
                                            where datasources.coverage_id            = coverages.id
                                              and internet_datasources.datasource_id = datasources.id
                                              and internet_datasources.id            = points.internet_datasource_id
                                              and points.category                    = categories.svf_code
                                         group by coverages.name,
                                                  categories.feature_name")
  end

  def running_here?
    `ps -eaf`.match(%r<script/runjob\s*#{self.id}\b>)
  end

  def script_available
    return script && script.size > 0
  end

  def release
    # releases this datasource (create datarelease record en reception record)
    # dump data to DHIVE POI database.
    isources = InternetDatasource.where("status = 200")

    begin
      isources.each do |source|
        # create reception record
    
        export_new_points
      end
      isource.status = 300
      isource.save
    rescue e
      isource.status = -100
      isource.save
    end


  end

  def export_points(filter, incremental)
    # find latest release
    latest_release = DataRelease.where("datasource_id = #{self.datasource_id}").order('created_at desc').first
    last_release_date = DateTime.new
    if latest_release
      last_release_date = DateTime.new
    end

    conditions= "internet_datasource_id = #{self.id}"
    if incremental
      conditions = conditions + " and (created_at > '#{last_release_date}' or updated_at > '#{last_release_date}')"
    end
    if filter
      conditions = conditions + " and " + filter
    end  
    
    pagesize = 100
    offset = 0
    amount = Point.where(conditions).count
    pages = amount / pagesize +1
    c = 0


    while c <= pages
      points = Point.where(conditions).limit(offset)
      c = c + 1
      offset = offset + pagesize



      ##and released_at is null
      drecid = nil
      dsupid = nil
      relcode = nil
      release_desc =
        if points.size > 0
        # create release
        DataRelease.transaction do
          dr = DataRelease.where("datasource_id = #{self.datasource.id} and name = 'QL-" + Date.today.cwyear.to_s + "-" + Date.today.cweek.to_s + "'" ).first
          if !dr
            dr  = DataRelease.new
            release_desc = "Webscraper Release Week " + Date.today.cwyear.to_s + "-" + Date.today.cweek.to_s + Date.today.cweek.to_s
            dr.name = release_desc
            relcode =  "QL-" + Date.today.cwyear.to_s + "-" + Date.today.cweek.to_s
            dr.release_code = relcode
            dr.datasource_id = self.datasource.id
            dr.company_abbr_id = 87
            dr.save
          end

          rec = Reception.new
          rec.purpose = "POITRACKING"
          rec.datasource_id = self.datasource.id
          rec.data_release_id = dr.id
          dm = DataMedium.find_by_name('FTP')
          rec.data_medium_id = dm.id if dm
          reg = Region.find_by_name("Europe")
          rec.area_id = reg.id if reg

          dt = DataType.find_by_name("POI")

          rec.data_type_id = dt.id
          rec.referenceid = dr.release_code + "/" +  DateTime.now.strftime('%m-%d-%Y %H:%M')
          rec.reception_date = Date.today
          rec.purpose = "test"
          rec.remarks = "Automated reception process for Webscraper data"
          rec.storage_location = "TBD"
          rec.region_name = "Europe"
          rec.removed_yn = 1
          rec.userid = "johan.hendrikx"
          rec.user_name = "Johan Hendrikx"
          recsave = rec.save!

          dsup = DhiveSupplier.where("id = #{self.id}").first
          if !dsup
            dsup = DhiveSupplier.new
            dsup.Id = self.id
            dsup.Name = self.datasource.name
            dsup.Abbreviation = self.datasource.name
            dsup.save
          end
          dsupid = dsup.Id

          #create reception entry is Dhive_database
          drec = DhiveReception.new
          drecid = rec.id
          drec.Id = drecid
          drec.SupplierId = dsupid
          drec.Release = relcode
          drec.ReceptionTimeStamp = rec.created_at
          drec.Description = release_desc
          drec.save
        end
      end
      DhivePoi.logger.level = Logger::INFO
      #DhivePoi.transaction do
      DhivePoi.transaction do
        # DhiveAttribute.transaction do

        #puts "Point saving started"
        points.each do |point|
          begin
            dhive_poi = point.to_dhive
            dhive_poi.sanitize
            dhive_poi.SupplierId = dsupid
            dhive_poi.ReceptionId = drecid
            dhive_poi.save
            #all pva's created and saved and committed as well
            minpva = 9999999999999
            dhive_poi.dhive_attributes.each do |att|
              minpva = att.id.to_i if att.id.to_i < minpva
            end
            dhive_poi.FirstPVAId = minpva
            dhive_poi.save

            #dhive_attribute.new
            #point.released_at = Time.now
            #point.save
            
          rescue => e
            #puts e.message
            #puts e.backtrace
          end
        end
      end
      #puts "Finished exporting page #{c} of #{pages} for #{dsupid} ::: #{offset} - #{c}"
    end # while pages not finished
    #end
  end

  def self.export_all(limit=10)
    `cp db/dhive.org db/dhive_test.sq3`
    DhivePoi.init_spatial
    #puts "Dhive prepared"
    sources = InternetDatasource.where("script <> ''  and script is not null").order('id desc').limit(limit)
    sources.each do |source|
      begin
        source.export_new_points
        #puts "Source " + source.location.to_s +  " exported"
      rescue => e
        #puts e.message
        #puts e.backtrace
        #log.error(e)
      end
    end
  end


  def self.export_incremental
    `cp db/dhive.org db/dhive.sq3`
    DhivePoi.init_spatial
    # puts "Dhive prepared"
    sources = InternetDatasource.where("script <> ''  and script is not null").order('id desc').limit(10)
    sources.each do |source|
      begin
        source.export_points(nil,true)
        # puts "Source " + source.location.to_s +  " exported"
      rescue => e
        # puts e.message
        # puts e.backtrace
        # log.error(e)
      end
    end
  end

  def self.export(params={})
    db_name = "dhive.sq3"
    if params[:db_name]
      db_name = params[:db_name]
    end
    db_name = db_name + "_" + Time.now.to_i.to_s
    #`cp db/dhive.org /tmp/#{db_name}`

    `#{ENV['dhive_model_path']}/DHModelCreate /tmp/#{db_name}`
    #puts "Dhive Model created"

    DhiveBase.establish_connection(:adapter => 'sqlite3' , :database => "/tmp/#{db_name}", :timeout => 5000)
    #puts "Dhive Base connected"
    #DhiveSupplier.establish_connection(:adapter => 'sqlite3' , :database => "/tmp/#{db_name}", :timeout => 5000)
    #DhiveReception.establish_connection(:adapter => 'sqlite3' , :database => "/tmp/#{db_name}", :timeout => 5000)
    #DhiveAttribute.establish_connection(:adapter => 'sqlite3' , :database => "/tmp/#{db_name}", :timeout => 5000)
    DhivePoi.init_spatial
    #puts "Spatial initialized"

    
    filter = "1 = 1"
    limit = nil
    if params[:conditions]
      filter = params[:conditions]
    end

    filtersource = ""
    if params[:sourceconditions]
      filtersource = params[:sourceconditions] + " AND "
    end
    filtersource = filtersource + " id > 91 and script <> ''  and script is not null "

    if params[:limit]
      limit = params[:limit]
    end

    #puts "Dhive prepared"
    if limit
      sources = InternetDatasource.where(filtersource).order('id desc').limit(limit)
    else
      sources = InternetDatasource.where(filtersource).order('id desc')
    end
    #puts "sources found " + sources.size.to_s
    sources.each do |source|
      begin
        source.export_points(filter,false)
        #puts "Source " + source.location.to_s +  " exported"
      rescue => e
        #puts e.message
        #puts e.backtrace
        #log.error(e)
      end
    end
    return db_name
  end

  def export(params={})
    db_name = "dhive.sq3"
    `cp db/dhive.org db/#{db_name}`
    db_name = db_name + "_" + Time.now.to_i.to_s + "_" + id.to_s
    `cp db/dhive.org /tmp/#{db_name}`
    DhiveBase.establish_connection(:adapter => 'sqlite3' , :database => "/tmp/#{db_name}", :timeout => 5000)
    DhivePoi.init_spatial
    filter = "1 = 1"
    #puts "Dhive prepared for single"
    export_points(filter,false)
    #puts "Source " + location.to_s +  " exported"
    return "/tmp/#{db_name}"
  end

  def self.timestamp
    DateTime.now.strftime('%Y%m%dT%H%M%S')
  end

  def fast_export
    s = "internet_datasource_id = " + self.id.to_s
    InternetDatasource.fast_export(s)
  end


  def self.fast_export(conditions)
    page_size = 100000
    total = Point.where("internet_datasource_id is not null and " + conditions).count
    pages = total/page_size

    maphash =
      {
      'BrandName' => 'brandname',
      'PlaceName' => 'city_name',
      'InputCategory' => 'category',
      'CreationTimeStamp' => 'checked_at',
      'LastUpdatedTimeStamp' => 'updated_at',
      'PermanentId' => 'id',
      'InputCategoryType' => "INTERNET",
      'Name' => 'name_of_facility',
      'StreetName' => 'street_name',
      'HouseNumber' => 'house_number',
      'PostalCode' => 'postal_code',
      'PhoneNumber' => 'phone_number',
      'Fax' => 'fax_number',
      'Internet' => 'deeplink',
      'Email' => 'email_address',
      'SupplierId' => 'internet_datasource_id',
      'ISOCountrycode' => 'country_iso3_code',
      'InputGeomWGS84' => 'coord'
    }
    #maphash = maphash.invert
    dump = "/tmp/points.db-#{timestamp}"
    `cp db/dhive.org #{dump}`
    sqlite = ActiveRecord::Base.sqlite3_connection(:database => dump,
      :adapter  => "sqlite3")

    # don't log inserts
    ActiveRecord::Base.logger.level = Logger::INFO
    coder = HTMLEntities.new
    

    sqlite.transaction {
      ## use separate SQLite connection
      #require 'active_record/connection_adapters/sqlite3_adapter'
      sqlite = ActiveRecord::Base.sqlite3_connection(:database => dump,
        :adapter  => "sqlite3")
      sqlite.instance_variable_get(:@connection).enable_load_extension(1)
      sqlite.instance_variable_get(:@connection).execute("SELECT load_extension('libspatialite.so')")
      columns = DhivePoi.columns
      iid = -1
      for page in 0..pages do
        #puts "#{timestamp}\t#{page * page_size}/#{total}\t#{(page * page_size) / total.to_f}"

        Point.connection.select_all("select *
                                   from points
                                  where internet_datasource_id is not null and #{conditions}
                                  order by internet_datasource_id
                                  limit #{page_size}
                                 offset #{page_size * page}").each do |p|

          if p["internet_datasource_id"] != iid
            iid = p["internet_datasource_id"]
            #puts "Adding datarelease:" + iid.to_s
            idatasource = InternetDatasource.find(iid)
            dr = DataRelease.where("datasource_id = #{idatasource.id} and name = 'QL-" + Date.today.cwyear.to_s + "-" + Date.today.cweek.to_s + "'" ).first
            if !dr
              dr  = DataRelease.new
              release_desc = "Webscraper Release Week " + Date.today.cwyear.to_s + "-" + Date.today.cweek.to_s + Date.today.cweek.to_s
              dr.name = release_desc
              relcode =  "QL-" + Date.today.cwyear.to_s + "-" + Date.today.cweek.to_s
              dr.release_code = relcode
              dr.datasource_id = idatasource.id
              dr.company_abbr_id = 87
              dr.save
            end

            rec = Reception.new
            rec.purpose = "POITRACKING"
            rec.datasource_id = idatasource.id
            rec.data_release_id = dr.id
            dm = DataMedium.find_by_name('FTP')
            rec.data_medium_id = dm.id if dm
            reg = Region.find_by_name("Europe")
            rec.area_id = reg.id if reg

            dt = DataType.find_by_name("POI")

            rec.data_type_id = dt.id
            rec.referenceid = dr.release_code + "/" +  DateTime.now.strftime('%m-%d-%Y %H:%M')
            rec.reception_date = Date.today
            rec.purpose = "test"
            rec.remarks = "Automated reception process for Webscraper data"
            rec.storage_location = "TBD"
            rec.region_name = "Europe"
            rec.removed_yn = 1
            rec.userid = "johan.hendrikx"
            rec.user_name = "Johan Hendrikx"
            rec.save

            iid = p["internet_datasource_id"]
            columnssup = DhiveSupplier.columns
            dsup = DhiveSupplier.new
            dsup.Id = idatasource.id
            name = "UNKNOWN"
            name = idatasource.datasource.name if idatasource.datasource
            dsup.Name = name
            dsup.Abbreviation = name
            dsupid = dsup.Id
            sqlite.execute("insert into DH_SUP values(#{columnssup.map {|c| sqlite.quote(dsup.send(c.name), c) }.join(', ')})")
            

            #create reception entry is Dhive_database
            columnsrcp = DhiveReception.columns
            drec = DhiveReception.new
            drecid = rec.id
            drec.Id = drecid
            drec.SupplierId = dsupid
            drec.Release = relcode
            drec.ReceptionTimeStamp = rec.created_at
            drec.Description = release_desc
            sqlite.execute("insert into DH_RCP values(#{columnsrcp.map {|c| sqlite.quote(drec.send(c.name), c) }.join(', ')})")

          end
          p = self.sanitize_data(p,coder)


          insert = "insert into DH_POI values(#{columns.map {|c| sqlite.quote(p[maphash[c.name]], c) }.join(', ')})"
          sqlite.execute(insert)
        end
      end
    }
  end

  def self.sanitize_data(p,coder)
    p["name_of_facility"] = coder.decode(p["name_of_facility"]) if p["name_of_facility"]
    p["street_name"] = coder.decode(p["street_name"]) if p["street_name"]
    p["house_number"] = coder.decode(p["house_number"]) if p["house_number"]
    p["postal_code"] = coder.decode(p["postal_code"]) if p["postal_code"]
    p["city_name"] = coder.decode(p["city_name"]) if p["city_name"]
    p["country_name"] = coder.decode(p["country_name"]) if p["country_name"]
    p["phone_number"] = coder.decode(p["phone_number"]) if p["phone_number"]
    if p["absolute_xcoordinate"] && p["absolute_ycoordinate"]
      p["coord"] = GeoRuby::SimpleFeatures::Point.from_x_y(p["absolute_xcoordinate"],p["absolute_ycoordinate"])
    else
      p["coord"] = GeoRuby::SimpleFeatures::Point.from_x_y(0,0)
    end
     
    
    return p
  end

  def self.export_csv(internet_datasource_id,file_name)
    require 'fastercsv'
    details_columns =  YAML::load(Point.where("internet_datasource_id = #{internet_datasource_id}").first.details).keys
    export_columns = ["name_of_facility","street_name","house_number","postal_code","city_name","deeplink","country_name","country_code","phone_number","category","iso_language_code","absolute_xcoordinate","absolute_ycoordinate"]

    FasterCSV.open("#{file_name}", "w") do |csv|
	    #dumping headers
	    csv << export_columns + details_columns
      points = Point.where("internet_datasource_id = #{internet_datasource_id}").select(export_columns.to_csv + ",details")
      
	    points.each do |point|
        main_data = export_columns.map{|x| point.attributes[x]}
        details =  YAML::load(point.details)
        main_data = main_data + details_columns.map{|x| details[x]}
        csv << main_data
	    end
	    # ...
    end

  end


  
end

