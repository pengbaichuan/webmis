class Invoice < ActiveRecord::Base
  belongs_to :shipping_order
  has_many :invoices, :foreign_key => "credit_invoice_parent_id"
  has_many :shipping_orderlines, :dependent => :nullify, :inverse_of => :invoice

  belongs_to :credit_invoice_parent, :class_name => "Invoice"
  has_one :credit, :foreign_key => "credit_invoice_parent_id",:class_name => "Invoice"
  belongs_to :parent ,:foreign_key => "credit_invoice_parent_id",:class_name => "Invoice"

  
  validates :shipping_order_id, :presence => true
  validates :reference, :uniqueness => true
  validates_associated :shipping_orderlines
  validate  :not_already_invoiced_validation

  @@statusarray     = Array.new
  @@statusarray[0]   = 'Draft'
  @@statusarray[50]  = 'Invoiced'
  @@statusarray[60]  = 'Paid'

  def all_shipping_orderlines
    if !self.credit_invoice_parent_id.blank?
      return ShippingOrderline.where("invoice_id = #{self.id} OR invoice_id = #{self.credit_invoice_parent_id}")
    else
      return self.shipping_orderlines
    end
  end

  def status_string
    @@statusarray[self.status]
  end

  def not_already_invoiced_validation
    self.shipping_orderlines.each do |so|
      if so.status != 40
        errors.add(:shipping_orderine_ids, "shipping_orderline '#{so.id} has the wrong status '#{so.status_str}' ")
        puts "***** shipping_orderline '#{so.id} has the wrong status '#{so.status_string}' "
        return false
      end
      if so.invoice_id_was && so.invoice_id_was != self.id
        errors.add(:shipping_orderline_ids, "shipping orderline '#{so.id}' already assigned to another invoice (#{so.invoice_id_was}).}")
        puts "***** shipping orderline '#{so.id}' already assigned to another invoice (#{so.invoice_id_was}).}"
        return false
      end
    end
  end

  def credit_invoice
    credit_invoice = self.dup
    credit_invoice.credit_invoice_parent_id = self.id
    if self.reference.match(/\AMI-/i)
      credit_invoice.reference = self.reference.to_s.gsub('MI-','MC-')
    else
      credit_invoice.reference = "MC-#{self.reference}"
    end
    credit_invoice.total = self.total * -1 if self.total && self.total.to_i != 0
    credit_invoice.vat = self.vat * -1 if self.vat && self.vat.to_i != 0
    credit_invoice.transaction do 
      credit_invoice.save
      return credit_invoice.id
        # TODO Create invoice fro paid license fees
        # Not possible now, because Invoice model now only based on shipping_orderlines
        # self.shipping_orderlines.each do |orderline|
          # orderline.purchase_orderlines.each do |purchaseline|
            # create invoice based on fee in purchaseline
          # end
        # end
    end

  end

end
