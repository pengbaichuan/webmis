# To change this template, choose Tools | Templates
# and open the template in the editor.

class JiraIssue
  @@adapter = JiraRestAdapter.new(ConfigParameter.get("jira_url"), ConfigParameter.get("jira_login"), ConfigParameter.get("jira_password"))
  attr_accessor :contents

  def self.adapter
    @@adapter
  end


  def initialize(content=nil)
    self.contents = content
  end

  def self.projects
    projects = @@adapter.get_enum_projects
    projects.collect{|x|x["key"]}
  end

  def self.statuses
    projects = @@adapter.get_enum_statuses
    projects.collect{|x|x["name"]}.sort
  end


  def self.regions(project_id)
    @@adapter.get_enum_regions(project_id)

  end

  def self.fixversions(project_id)
    fixversions = @@adapter.get_enum_versions(project_id)
    fixversions.collect{|x|x["name"]}
  end

  def self.find(id)
    issue = JiraIssue.new
    issue.contents = @@adapter.get_issue(id)
    return issue
  end

  def self.where(jql, opts={})
    results = @@adapter.get_issues_by_jql(URI.escape(jql), opts)
    results.map!{|x|JiraIssue.new(x)}
    results
  end

  def get_value(fieldname)
    @@adapter.get_value(self.contents, fieldname)
  end


  def method_missing(m, *args, &block)
    if !m.match(/=$/)
     if self.contents[m.to_s]
       return self.contents[m.to_s]
     else
      return self.contents["fields"][m.to_s]
     end
    else
      attribute = m.to_s.gsub(/=$/,"")
      if self.contents["fields"][attribute]
        self.contents["fields"][attribute] = args[0]
      else
        self.contents[attribute] = args[0]
      end
    end
  end

  def save
    @@adapter.save(self.contents, self.contents["fields"].keys)
  end

end
