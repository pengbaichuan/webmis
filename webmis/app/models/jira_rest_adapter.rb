# To change this template, choose Tools | Templates
# and open the template in the editor.
require 'rest_client'
require 'json'
require 'net/http/post/multipart'
require 'stringio'

class JiraRestAdapter
  attr_accessor :filter
  attr_accessor :endpoint
  attr_accessor :login
  attr_accessor :password
  attr_accessor :driver
  attr_accessor :update_array
  attr_accessor :jira_url


  #ok rest
  def initialize(endpoint= "https://jira.mapscape.nl/",login="BugSyncBMW",password="Mapscape2014",http_login=nil,http_password=nil)
    @jira_url = File.join("https://#{login}:#{password}@#{endpoint.gsub("https://","")}", "rest/api/latest/")
    #@jira_url = File.join("http://#{login}:#{password}@#{endpoint.gsub("http://","")}", "rest/api/latest/")
    @endpoint = endpoint
    @login = login
    @password = password
  end

  #ok rest
  def get_enum_priorities
    full_url = File.join(@jira_url, "priority")
    priorities= JSON.parse(RestClient.get(full_url))
    return priorities
  end

  #ok rest
  def download_attachment(attachment)
    id = attachment["id"]
    filename = attachment["filename"]
    endpoint = nil
    #temporary fix to workaround certicate issues
    if @endpoint == "http://jira.mapscape.nl"
      endpoint = "https://jira.mapscape.nl"
    else
      endpoint = @endpoint
    end

    serverdetails = URI.parse("#{endpoint}/login.jsp")
    http = nil
    if endpoint.match(/https/)
      http = Net::HTTP.new(serverdetails.host, 443)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    else
      port = endpoint.scan(/:([0-9]*)$/).flatten.join.strip
      if port.strip.size == 0
        port = 80
      else
        port = port.to_i
      end
      http = Net::HTTP.new(serverdetails.host, port)
    end

    resp,data = http.post serverdetails.path ,"os_username=#{login}&os_password=#{password}&os_destination=%2Fsecure%2F"
    cookie = resp.response['set-cookie']
    headers = {
      'Cookie' => cookie
    }
    resp,data = http.get("#{@endpoint}/secure/attachment/#{id}/#{filename}",headers)
    return data
  end

  #rest ok
  def get_enum_statuses
    full_url = File.join(@jira_url, "status")
    json = RestClient::Request.execute(:url => full_url, :method => :get, :verify_ssl => false)
    statuses =  JSON.parse(json)
    return statuses
  end


 def get_enum_fields
    full_url = File.join(@jira_url, "field")
    projects =  JSON.parse(RestClient::Request.execute(:url => full_url, :method => :get, :verify_ssl => false))
    return projects
  end


  def get_enum_projects
    full_url = File.join(@jira_url, "project")
    projects =  JSON.parse(RestClient::Request.execute(:url => full_url, :method => :get, :verify_ssl => false))
    return projects
  end


  def get_enum_versions(projectid)
    return [] if projectid.blank?
    full_url = File.join(@jira_url, "project", projectid, "versions")
    projects =  JSON.parse(RestClient::Request.execute(:url => full_url, :method => :get, :verify_ssl => false))
    return projects
  end

  def get_enum_regions(projectid)
    return [] if projectid.blank?
    full_url = File.join(@jira_url, "issue", "createmeta?projectKeys=#{projectid}&issuetypeName=CR,PR&expand=projects.issuetypes.fields")
    fields =  JSON.parse(RestClient::Request.execute(:url => full_url, :method => :get, :verify_ssl => false))
    fields = fields["projects"][0]["issuetypes"].delete_if{|x|x["name"] != "PR"}[0]["fields"]
    enum = fields.to_a.delete_if{|x|x[1]["name"] != "Product Region"}
    if enum.flatten[1]
      values= enum.flatten[1]["allowedValues"].collect{|x|x["value"]}
    else
      values = []
    end
    return values
  end


  #rest_ok
  def get_status_string(status_id)
    status = get_enum_statuses
    status.each do |s|
      if s["id"].to_i == status_id.to_i
        return s["name"]
      end
    end
    raise "Status not found for #{status_id}"
  end


  def get_user(id)
    #full_url = File.join(@jira_url, "user", id.to_s)
    #user = JSON.parse(RestClient.get(full_url))
    #return user
    raise "Not available on Jira Rest Interface"
  end

  #rest ok
  def get_notes(issue)
    notes = []
    if issue["fields"] && issue["fields"]["comment"]
      notes = issue["fields"]["comment"]["comments"]
    end
    return notes
  end

  def get_issues_by_jql(jql, opts={})
    optsarr = []
    opts.each do |k,v|
      optsarr << "#{k}=#{v}"
    end

    optsstring = optsarr.join("&")
    optsstring = "&" + optsstring if optsarr.size > 0
    full_url = @jira_url + "search?expand=schema,editmeta#{optsstring}&jql=#{jql}"
    issues = JSON.parse(RestClient::Request.execute(:url => full_url, :method => :get, :verify_ssl => false))
    return issues["issues"]
  end

  #rest ok
  def get_issues(filter_id="10874")
    jql= 'jql=filter='+filter_id
    full_url = @jira_url + "search?expand=schema,editmeta&maxResults=1000&#{jql}"
    issues = JSON.parse(RestClient::Request.execute(:url => full_url, :method => :get, :verify_ssl => false))
    return issues["issues"]
  end

  # rest ok
  def get_value(source_issue, field_name)
    field_value = get_field(source_issue, field_name)
    return field_value
  end

  #rest ok
  def get_field(issue, field_name)
    value = nil
    if issue.keys.index(field_name)
      value = issue[field_name]
    elsif issue["fields"].keys.index(field_name)
      value = issue["fields"][field_name]
    else
      cfield = locate_customfield(field_name)
      if cfield
        return issue["fields"][cfield["id"]]
      end
    end
    return value
  end
  

  #ok rest
  def locate_customfield name
    custom_field = nil
    full_url = File.join(@jira_url, "field")
    @custom_fields = JSON.parse(RestClient::Request.execute(:url => full_url, :method => :get, :verify_ssl => false)) if !@custom_fields
    @custom_fields.each do |cfield|
      if cfield["name"] == name
        custom_field = cfield
        break
      end
    end
    return custom_field
  end


  def set_field(issue, field_name,field_value)
    issue["fields"] = {} if !issue["fields"]
    if field_name ==  "project"
      issue["fields"][field_name] = {"key" => field_value}
    elsif field_name == "issuetype"
      issue["fields"][field_name] = {"name" => field_value}
    else
      issue["fields"][field_name] = field_value
    end
  end

  #ok rest
  def get_attachments(issue)
    attachments = []
    full_url = File.join(@jira_url, "issue/#{issue['key']}?expand=schema,editmeta")
    details = JSON.parse(RestClient.get(full_url))
    if details["fields"]["attachment"]
      attachments = details["fields"]["attachment"]
    end
    return attachments
  end

  #ok rest
  def new_issue
    {}
  end

  #ok rest
  def save_attachment(id,attachment)
    issue = get_issue(id)
    alreadyin = false
    if issue["fields"]["attachment"]
      issue["fields"]["attachment"].each do |in_attachment|
        alreadyin = true if attachment.id == in_attachment["id"] || attachment.filename == in_attachment["filename"]
      end
    end
    if !alreadyin
      data = attachment.data.clone
      url = URI.parse(File.join(@jira_url, "issue", id, "attachments"))#.gsub("https://","http://"))
      req = Net::HTTP::Post::Multipart.new url.path,
        "file" => UploadIO.new(StringIO.new(data), "application/zip", attachment.filename)
      req.add_field('X-Atlassian-Token', 'nocheck')

      http_s = Net::HTTP.new(url.host, url.port)
      http_s.use_ssl = true
      http_s.verify_mode = OpenSSL::SSL::VERIFY_NONE
      resp,data = http_s.post "/login.jsp" ,"os_username=#{login}&os_password=#{password}&os_destination=%2Fsecure%2F"
      cookie = resp.response['set-cookie']
      req.add_field('Cookie', cookie)
      res = http_s.start do |http|
        http.request(req)
      end
    end
  end

  #rest ok
  def new_note
    {}
  end

  #rest ok
  def new_attachment
    a = Attachment.new
    return a
  end
  
  #rest ok
  def save_note(issue_id,note)
    issue = get_issue(issue_id)
    notes = []

    if issue["fields"]["comment"] && issue["fields"]["comment"]['comments']
      notes = issue["fields"]["comment"]["comments"]
    end
    noteid = check_note_exists(notes,note)
    note["updated"] = DateTime.now if !note["updated"]
    if noteid.to_i  < 0
      if note["body"].to_s.size > 0
        note["body"] = note["body"].to_s + "\n\n" + "Ref: #{note['id']} by #{note['author']} at #{note['updated']}"
        full_url = File.join(@jira_url, "issue/#{issue['key']}","comment")
        RestClient.post(full_url,note.to_json, {"Content-Type" => "application/json", "Accept-Encoding" => 'deflate'})
      else
        puts "empty note found ... " + note.inspect
      end
    else
      puts "Existing note found , skipping"
    end
  end

  #rest ok
  def check_note_exists(notes,note)
    id = -1
    ref = note["id"].to_s
    if ref && ref.size > 0
      notes.each do |jiranote|
        if jiranote["body"].scan(/Ref: #{ref}/).to_s.size > 0
          id = jiranote["id"]
          break
        end
      end
    end
    return id
  end


  def save(issue,fields=nil)
    searchstr = nil
    searchstr = issue["key"] if issue["key"]

    issue = issue.deep_dup

    issue.keys.each do|key|
      if key != "fields"
        issue.delete(key)
      else
        issue["fields"].keys.each do |field|
          if !fields.index(field)
            issue["fields"].delete(field)
          end
        end
      end
    end

    existing = {}
    jql= URI.encode("jql=project=#{issue["fields"]["project"]["key"]} AND key = \"#{searchstr}\"")
    full_url = @jira_url + "search?expand=schema,editmeta&maxResults=2&#{jql}"
    
    #existing = JSON.parse(RestClient.get(full_url, {"Content-Type" => "application/json", "Accept-Encoding" => 'deflate'}))if !searchstr.blank?
    existing = JSON.parse(RestClient::Request.execute(:url => full_url, :method => :get, :verify_ssl => false)) if !searchstr.blank?


    existing = existing["issues"]
    if existing && existing.size > 1
      raise "More then one issue found for " + issue["fields"]["summary"].to_s +  " / " + searchstr + " " + existing.collect{|x|x["key"].to_s}.join(" ")
    end

    org_issue = nil
    id = nil
    action = "NONE"
    if existing && existing[0] && existing[0]["key"].size > 0
      org_issue = get_issue(existing[0]["key"])
      #if diff(org_issue,issue,fields)
        issue["key"] = org_issue["key"]
        issue["id"] = org_issue["id"]
        full_url = File.join(@jira_url, "issue", issue["key"])
        RestClient::Request.execute(:url => full_url, :method => :get, :verify_ssl => false)
        full_url.gsub!("https://" , "http://")
        result = RestClient.put(full_url, issue.to_json, {"Content-Type" => "application/json", "Accept-Encoding" => 'deflate'})

        if result
          id = existing[0]["key"]
          action = "UPDATE"
        end
    else
      @current_issue = issue
      full_url = File.join(@jira_url, "issue")
      result = JSON.parse(RestClient::Request.execute(:method => :post, :url => full_url, :payload => issue.to_json, :verify_ssl => false, :headers => {"Content-Type" => "application/json", "Accept-Encoding" => 'deflate'}))
      id = result["key"]
      action = 'CREATE'
    end
    return [id,action]
  end

  #ok rest
  def get_issue(id)
    full_url = @jira_url + "issue/#{id}?expand=schema,editmeta"
    issue = JSON.parse(RestClient::Request.execute(:url => full_url, :method => :get, :verify_ssl => false))
    return issue
  end

  def diff(issue_source,issue_dest,fields)
    change = false
    fields.each do |ivar|
      change2 = false
      source_value = get_field(issue_source,ivar)
      dest_value =  get_field(issue_dest,ivar)

      if source_value.instance_of(Hash)
        if dest_value.instance_of(Hash)
          dest_value.keys do |key|
            dest_value.delete(key) if !source_value[key]
          end
        end
      end

      change2 = true if !(source_value.to_s.gsub(/\r\n/,"\n").eql? dest_value.to_s.gsub(/\r\n/,"\n")) #.gsub(/\r\n/,"\n"))
      #end
      # Added length to both strings to check for non-printable/bad-printable characters.
      change = change2 || change
    end
    return change
  end

  def direct_update(issue,updatevalues)
    update_array = []
    updatevalues.keys.each do |key|
      found = false
      row = Jira4R::V2::RemoteFieldValue.new
      if issue.respond_to?(key)
        row.id = key
        found = true
      else
        cfield = locate_customfield(key)
        if cfield
          row.id = cfield.id
          found = true
        end
      end
      if found
        row.values = updatevalues[key].to_s
        update_array << row
      else
        puts "Field not found :: "  + key.inspect
      end
    end
    @driver.updateIssue(issue.key, update_array)
  end

end
