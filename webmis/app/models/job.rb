class Job < ActiveRecord::Base
  version_fu
  attr_accessor :checklog, :job_script

  before_save :before_save
  after_initialize :defaults
  belongs_to :fail_category_logged_by_user, :class_name => "User", :foreign_key =>  "fail_category_logged_by_user_id"
  belongs_to :original_job , :class_name => "Job"
  belongs_to :project

  # begin counting ,don't change order without checking depended status calculations
  STATUS_PLANNED           =  10
  STATUS_QUEUED            =  20
  STATUS_HOLD              =  30
  STATUS_PROCESSING        =  40
  STATUS_MANUAL            =  50
  STATUS_FAILED            = 180
  STATUS_CANCELLED         = 190
  STATUS_COMPLETE          = 200
  STATUS_RESCHEDULED       = 300

  FILE_SYSTEM_STATUS_FREE      =  10
  FILE_SYSTEM_STATUS_ON_SERVER = 100
  FILE_SYSTEM_STATUS_RESERVED  = 500
  FILE_SYSTEM_STATUS_MOVING    = 600

  scope :outstanding, -> {where("jobs.status < #{STATUS_CANCELLED}")}
  scope :processing, -> {where("jobs.status = #{STATUS_PROCESSING}")}
  scope :planned, -> {where("jobs.status = #{STATUS_QUEUED} or jobs.status = #{STATUS_HOLD}")}
  scope :on_server, -> {where("jobs.file_system_status >= #{FILE_SYSTEM_STATUS_ON_SERVER}")}
  scope :in_queue, -> {where("jobs.status = #{STATUS_MANUAL} or jobs.status = #{STATUS_HOLD}")}
  scope :queued, -> {where("jobs.status = #{STATUS_QUEUED} or jobs.status = #{STATUS_HOLD}")}
  scope :assigned, -> {where("(jobs.status = #{STATUS_QUEUED} OR jobs.status = #{STATUS_HOLD} OR jobs.status = #{STATUS_MANUAL}) AND jobs.run_server IS NOT NULL AND jobs.run_server != ''")}
  scope :active, -> {where("jobs.status < #{STATUS_RESCHEDULED}")}
  scope :failures, -> {where("(jobs.fail_category IS NULL OR jobs.fail_category = '') AND (jobs.status = #{STATUS_FAILED} OR jobs.status = #{STATUS_CANCELLED})")}
  scope :fail_reason_tagged, -> {where("jobs.fail_category IS NOT NULL AND jobs.fail_category != ''")}
  scope :waiting_on_bug_tracker, -> {where("jobs.status = #{STATUS_FAILED} AND jobs.bug_tracker_id is not null")}
  scope :reserve_space_failed, -> {where("jobs.status = #{STATUS_PROCESSING} and jobs.fail_reason <> ''")}
  scope :failures_on_server, -> {("(jobs.fail_category IS NULL OR jobs.fail_category = '') AND (jobs.status = #{STATUS_FAILED} OR jobs.status = #{STATUS_CANCELLED}) AND jobs.file_system_status != #{FILE_SYSTEM_STATUS_FREE}")}
  
  @@status_array       = Array.new
  @@status_array[STATUS_PLANNED]           = 'PLANNED'
  @@status_array[STATUS_QUEUED]            = 'QUEUED'
  @@status_array[STATUS_HOLD]              = 'HOLD'
  @@status_array[STATUS_PROCESSING]        = 'PROCESSING'
  @@status_array[STATUS_MANUAL]            = 'MANUAL'
  @@status_array[STATUS_FAILED]            = 'FAILED'
  @@status_array[STATUS_CANCELLED]         = 'CANCELLED'
  @@status_array[STATUS_COMPLETE]          = 'COMPLETE'
  @@status_array[STATUS_RESCHEDULED]       = 'RESCHEDULED'

  @@file_system_status_array = Array.new
  @@file_system_status_array[FILE_SYSTEM_STATUS_FREE]       = 'Server free'
  @@file_system_status_array[FILE_SYSTEM_STATUS_ON_SERVER]  = 'On server'
  @@file_system_status_array[FILE_SYSTEM_STATUS_RESERVED]   = 'Reserved on file system'
  @@file_system_status_array[FILE_SYSTEM_STATUS_MOVING]     = 'Moving'

  def defaults
    # read_attribute because of a rails bug https://rails.lighthouseapp.com/projects/8994/tickets/3165-activerecordmissingattributeerror-after-update-to-rails-v-234
    if read_attribute(:run_script) && JobScript.valid_json?(self.run_script)
      self.job_script = JobScript.new(self.run_script)
    else
      self.job_script = nil
    end
    self.status ||= STATUS_PLANNED  if self.new_record?
    self.file_system_status ||= FILE_SYSTEM_STATUS_FREE if self.new_record?
  end

  def before_save
    if self.job_script
      self.run_script = self.job_script.to_json
    end 
    self.fix_bug_tracker_id
  end

  def checklog
    @checklog || @checklog = Checklog.new(only_check_yn = false, Checklog::LOGLEVEL_DEBUG)
  end

  def status_str
    @@status_array[status]
  end

  def file_system_status_str
    @@file_system_status_array[file_system_status]
  end

  def self.status_array
    @@status_array
  end

  def self.file_system_status_array
    @@file_system_status_array
  end

  def self.status_hash
    # for an option list of all possible statuses
    s_hash = Hash.new
    @@status_array.each{ |s| s_hash[s] = @@status_array.index(s) unless s.nil? }
    s_hash.sort_by { |k,v| v }
  end

  def status_hash
    # for an option list of all valid statuses for the current job
    s_hash = Hash.new
    @@status_array.each{ |s| s_hash[s] = @@status_array.index(s) unless (s.nil? || !self.allow_status?(@@status_array.index(s))) }
    s_hash.sort_by { |k,v| v }
  end

  def self.file_system_status_hash
    # for an option list of all possible file_system_statuses
    s_hash = Hash.new
    @@file_system_status_array.each{ |s| s_hash[s] = @@file_system_status_array.index(s) unless s.nil? }
    s_hash.sort_by { |k,v| v }
  end

  def file_system_status_hash
    # for an option list of all valid file_system_statuses for the current job
    s_hash = Hash.new
    @@file_system_status_array.each{ |s| s_hash[s] = @@file_system_status_array.index(s) unless (s.nil? || !self.allow_file_system_status?(@@file_system_status_array.index(s))) }
    s_hash.sort_by { |k,v| v }
  end

  def allow_status_change?(new_status)
    result = case self.status
               when STATUS_PLANNED
                 [STATUS_QUEUED, STATUS_PROCESSING, STATUS_MANUAL, STATUS_FAILED, STATUS_CANCELLED, STATUS_COMPLETE].include?(new_status)
               when STATUS_QUEUED
                 [STATUS_HOLD, STATUS_PROCESSING, STATUS_MANUAL, STATUS_FAILED, STATUS_CANCELLED, STATUS_COMPLETE].include?(new_status)
               when STATUS_HOLD
                 [STATUS_QUEUED, STATUS_PROCESSING, STATUS_MANUAL, STATUS_FAILED, STATUS_CANCELLED, STATUS_COMPLETE].include?(new_status)
               when STATUS_MANUAL
                 [STATUS_QUEUED, STATUS_PROCESSING, STATUS_FAILED, STATUS_CANCELLED, STATUS_COMPLETE].include?(new_status)
               when STATUS_PROCESSING
                 [STATUS_FAILED, STATUS_CANCELLED, STATUS_COMPLETE, STATUS_RESCHEDULED].include?(new_status)
               when STATUS_FAILED
                 [STATUS_CANCELLED, STATUS_COMPLETE, STATUS_RESCHEDULED].include?(new_status)
               when STATUS_CANCELLED, STATUS_COMPLETE, STATUS_RESCHEDULED
                 false    # end status, no status change allowed
               else
                 true     # undefined begin status, allow everything to get in a defined state.
             end
    result
  end

  def allow_status?(new_status)
    self.status == new_status || allow_status_change?(new_status)
  end

  def set_status(new_status)
    result = allow_status_change?(new_status)
    self.status = new_status if result
    result
  end

  def allow_file_system_status_change?(new_file_system_status)
    result = case new_file_system_status
               when FILE_SYSTEM_STATUS_FREE
                 true       # always possible to mark the server as free
               when FILE_SYSTEM_STATUS_ON_SERVER
                 !self.run_server.blank?   # as long as we have a server, it is possible to mark its file system as occupied
               when FILE_SYSTEM_STATUS_RESERVED
                 !self.run_server.blank? && [FILE_SYSTEM_STATUS_ON_SERVER].include?(self.file_system_status)
               when FILE_SYSTEM_STATUS_MOVING
                 !self.run_server.blank? && [FILE_SYSTEM_STATUS_RESERVED].include?(self.file_system_status)
               else
                 true # undefined begin file_system_status, allow everything to get in a defined state
             end
    result &&= !(self.file_system_status == FILE_SYSTEM_STATUS_FREE && self.end_status?) # treat FILE_SYSTEM_STATUS_FREE as an end file system status once the job is in an end status
    result
  end

  def allow_file_system_status?(new_file_system_status)
    self.file_system_status == new_file_system_status || allow_file_system_status_change?(new_file_system_status)
  end

  def set_file_system_status(new_file_system_status, path = nil)
    result = allow_file_system_status_change?(new_file_system_status)
    if result
      if [FILE_SYSTEM_STATUS_FREE, FILE_SYSTEM_STATUS_ON_SERVER].include?(new_file_system_status) && !self.reserved_path.blank?
        SpaceReserved.unreserve_space(self.reserved_path, self.result_dir_size_bytes)
        self.reserved_path = nil
      elsif [FILE_SYSTEM_STATUS_RESERVED, FILE_SYSTEM_STATUS_MOVING].include?(new_file_system_status) && self.reserved_path.blank? && !path.blank?
        SpaceReserved.reserve_space(path, self.result_dir_size_bytes)  # will generate an exception on failure
        self.reserved_path = path
      end
      self.file_system_status = new_file_system_status
    end
    return result
  end

  def self.totals(project_id, purpose="all")
    return_hash = {}
    Job.where(:project_id => project_id).group(:type,:status).size.each do |a|
      # #[["ConversionJob", 0], 2]
      if return_hash[a[0][0]].nil?
        return_hash[a[0][0]] = {@@status_array[a[0][1]]=>a[1]} if !@@status_array[a[0][1]].nil?
      else
        return_hash[a[0][0]] = return_hash[a[0][0]].merge({@@status_array[a[0][1]]=>a[1]})
      end
    end

    case purpose
    when "auto_que"
      failed_move = ConversionJob.processing.where("project_id = #{project_id} and fail_reason != ''").size
      if failed_move > 0
         return_hash["ConversionJob"]["DISK_FULL"] = failed_move
      end
      return_hash.each do |a|
        exclude_status = [STATUS_MANUAL, STATUS_CANCELLED, STATUS_COMPLETE, STATUS_RESCHEDULED, STATUS_PROCESSING]
        exclude_status.each do |stat|
          a[1].delete_if {|k,v| k == @@status_array[stat] }
        end
        
        if a[1].empty?
          return_hash.delete(a[0])
        end
        # Exclude ExtractJob
        # return_hash.delete("ExtractJob")
      end
    end

    return return_hash
  end

  def queue
    return unless self.status == STATUS_PLANNED

    unless self.set_status(self.schedule_manual ? STATUS_MANUAL : STATUS_QUEUED)
      # should normally not occur
      self.checklog.add_warning_line("Could not change the status #{self.status_str} to #{self.schedule_manual ? @@status_array[STATUS_MANUAL] : @@status_array[STATUS_QUEUED]}.")
      self.log_scheduler("Failed to set the status to #{self.schedule_manual ? @@status_array[STATUS_MANUAL] : @@status_array[STATUS_QUEUED]}, job.set_status() unexpectedly returned false.")
      self.save!
      return
    end
    self.checklog.add_debug_line("Job set to #{self.status_str}.")
    self.log_scheduler("Job set to status #{self.status_str} because the manual flag of the job #{self.schedule_manual ? 'has been set' : 'is not set'}.")
    self.estimated_duration = estimate_duration
    checklog_estimate = "#{self.estimated_duration} minutes"
    if self.estimated_duration == 0
      checklog_estimate = "No referring Jobs found."
    end
    self.checklog.add_attributes({"Status" => self.status_str, "Estimated duration" => checklog_estimate})
    self.checklog.update_result(self.status_str)
    self.log_action("Status changed to #{self.status_str}.")
    self.save!
  end

  def autoschedule
    return unless self.status == STATUS_QUEUED || self.status == STATUS_HOLD # only queued need to be scheduled
    Job.transaction do
      set_hold = set_manual = false
      if !self.run_server.blank?
        self.log_scheduler("Searching for an active server with name '#{self.run_server}'.")
        servers = Server.where("server_id LIKE ? AND active = 1", "\%#{self.run_server}\%")
        if servers.size != 1
          # we can only auto schedule the job if the server is properly specified.
          self.add_log_scheduler("Specified server '#{self.run_server}' not found as an active server.")
          set_hold = true
        end
      elsif schedule_manual
        # error, you cannot automatically perform a manual schedule, set the status to manual
        self.log_scheduler("Job specified as a schedule manual job.")
        set_manual = true
      else
        self.log_scheduler("Searching for active servers with scheduling allowed" +
                            (schedule_cores.to_i > 0 ? ", at least " + schedule_cores.to_s + " cores" : "") +
                            (schedule_diskspace.to_i > 0 ? ", at least " + schedule_diskspace.to_s + " MB diskspace": "") +
                            (schedule_memory.to_i > 0 ? ", at least " + schedule_memory.to_s + " MB memory": "") +
                            (schedule_allow_virtual ? "" : ", which are not a virtual host") +
                            " and which are available for project #{self.project.name} and job type #{self.class.name}.")
        servers = find_potential_servers
        self.add_log_scheduler("Found #{servers.count} potential servers.")
        set_hold = servers.empty?
      end

      if set_manual
        if set_status(STATUS_MANUAL)
          self.add_log_scheduler("Status changed to #{self.status_str}.")
        else
          self.add_log_scheduler("Could not change status to #{@@status_array[STATUS_MANUAL]}.")
        end
        save!
        return
      elsif set_hold
        if set_status(STATUS_HOLD)
          self.add_log_scheduler("Status changed to #{self.status_str}.")
        elsif self.status == STATUS_HOLD
          self.add_log_scheduler("Status kept at #{self.status_str}.")
        else
          self.add_log_scheduler("Could not change status to #{@@status_array[STATUS_HOLD]}.")
        end
        save!
        return
      end

      set_status(STATUS_QUEUED)  if self.status == STATUS_HOLD

      available_servers = []
      servers.each do |server|
        self.add_log_scheduler("Checking server '#{server.server_id}'.")
        healthy = true
        # jobs queued on a specific server have priority over jobs not queued on that server
        if run_server.blank?
          jobs_on_server = Job.where("run_server = '" + server.server_name + "'" +
              " AND status >= #{STATUS_QUEUED} AND status <= #{STATUS_MANUAL}")
        else
          jobs_on_server = Job.where("run_server = '"+ server.server_name + "'" +
              " AND status >= #{STATUS_PROCESSING} AND status <= #{STATUS_MANUAL}")
        end
        failed_and_finished_jobs = Job.on_server.where("run_server = '#{server.server_name}'").order("finish_time desc")
        self.add_log_scheduler("Found #{jobs_on_server.count} jobs on this server#{run_server.blank? ? '' : ' including manually queued ones'}.")
        jobs_on_server << self  # make sure the job to be scheduled is in the jobs_on_server list

        server_max_jobs = server.max_jobs.to_i
        if jobs_on_server.size <= 1 && !server.down
          # the server has no running jobs
          self.add_log_scheduler("Server #{server.server_id} has no running jobs and is not down, checking its health.")

          if !server.healthy?
            # disable server for scheduling
            healthy = false
            server.down = true
            server.server_remark = server.server_remark + server.healthy?("message").to_s
            server.save
            self.add_log_scheduler("Server health check failed, marking server #{server.server_id} as down.")
          end
          
        end

        if healthy == true && !server.down
          # initialize possible nil values
          server_cores = server.cores ? server.cores : 0
          server_diskspace = server.diskcapacity ? server.diskcapacity.to_i : 0
          server_memory = server.memory ? server.memory : 0

          used_cores = jobs_on_server.inject(0){|sum,job| sum + job.schedule_cores.to_i}
          used_diskspace = (jobs_on_server + failed_and_finished_jobs).inject(0){|sum,job| sum + job.schedule_diskspace.to_i}
          used_memory = jobs_on_server.inject(0){|sum,job| sum + job.schedule_memory.to_i}
          self.add_log_scheduler(schedule_exclusive  ? "Job should be scheduled exclusive." : "Job is not exclusive to a server.")
          self.add_log_scheduler("There are #{jobs_on_server.size - 1} other jobs of a maximum of #{server_max_jobs} running on the server.")
          self.add_log_scheduler(server_max_jobs >= jobs_on_server.size ? "Max jobs not exceeded." : "Server already at its max jobs.")
          self.add_log_scheduler("#{used_cores} of #{server_cores} cores will be used. " + (server_cores >= used_cores ? "Max cores not exceeded." : "All cores of the server already in use."))
          self.add_log_scheduler("#{used_diskspace}MB of #{server_diskspace}MB diskspace will be allocated. " + (server_diskspace > used_diskspace ? "Server has enough diskspace available" : "All diskspace of the server is already allocated."))
          self.add_log_scheduler("#{used_memory}MB of #{server_memory}MB memory will be allocated. " + (server_memory > used_memory ? "Server has enough memory available." : "All memory of the server is already allocated."))
          if (!schedule_exclusive || jobs_on_server.size <= 1) &&
              server_max_jobs >= jobs_on_server.size && server_cores >= used_cores && server_diskspace >= used_diskspace && server_memory >= used_memory
            # the server can handle this job
            available_servers << server
            self.add_log_scheduler("Server '#{server.server_id}' is available for scheduling of this job.")
          end
        end
        
      end

      self.add_log_scheduler("Found #{available_servers.size} servers available for scheduling.")
      if available_servers.size > 0
        # we now have a list of servers which can be used. In the future we probably want some fancy selection
        # mechanism to spread the load and keep the heavy servers available for the heavy jobs. For now just
        # takes the first one
        self.run_server = available_servers[0].server_name
        self.log_action("Server '#{self.run_server}'' assigned.")
        self.add_log_scheduler("Taking the first available server '#{self.run_server}'.")
        save!
        perform
      else
        save!
      end
    end
  end

  def find_potential_servers
    selection_str = "servers.active = 1 and servers.scheduling_allowed_yn = 1 and projects.id = #{self.project_id} and job_types.name = '#{self.class.name}' and job_types.allowed_yn = 1"
    if schedule_cores.to_i > 0
      selection_str += " and servers.cores >= #{schedule_cores}"
    end
    if schedule_diskspace.to_i > 0
      selection_str += " and servers.diskcapacity >= #{schedule_diskspace}"
    end
    if schedule_memory.to_i > 0
      selection_str += " and servers.memory >= #{schedule_memory}"
    end
    unless schedule_allow_virtual
      selection_str += " and (servers.virtual_host is null or servers.virtual_host = '')"
    end
    return Server.joins(:projects, :job_types).where(selection_str).readonly(false)
  end

  def ssh(cmd)
    Server.ssh_to_server(run_server,cmd)
  end

  def kill_linux_process(pid)
    cmd = "kill -- -$( ps -o pgid --no-headers #{pid} | tr -d ' ' )"
    result = ssh(cmd).chomp
    return result
  end

  def job_cancel(user_ref = 'system', rescheduled_yn = false)
    if user_ref.class == User
      user = user_ref
      user_name = user_ref.full_name
    else # user_ref is a user_name string
      user = User.find_by_full_name(user_ref)
      user_name = user_ref
    end
    Job.transaction do
      unless self.end_status?
        if status == STATUS_PROCESSING
          kill_linux_process(run_pid) unless run_pid.to_s.blank?
          if self.class == TestRequestJob
            cid = File.expand_path("#{ConfigParameter.get('maptest_base_work_dir', self.project_id.to_s)}/maptest_#{id.to_s}")
          else
            cid = File.expand_path("#{ConfigParameter.get("dakota_scripts_dir")}/dakota_#{id.to_s}") # no project_id available
          end
          self.fail_reason = self.fail_reason.to_s + "\r\n======\r\nKilled by user #{user_name}.\r\n"
          self.stdout_log = File.read("#{cid.to_s}.log") rescue "Log file #{cid.to_s} not found."
          self.stdout_log = self.stdout_log + "\r\n------------\r\nKilled by user #{user_name}.\r\n"
          self.finish_time = DateTime.now
        end
        if rescheduled_yn
          set_status(STATUS_RESCHEDULED)
          self.log_action("Job rescheduled", user)
        else
          set_status(STATUS_CANCELLED)
          self.log_action("Job cancelled", user)
        end
      end
      unless file_system_status == FILE_SYSTEM_STATUS_FREE
        clear_scripts
        clear_working_dir
      end
      save!
    end
  end

  def cancel(user='system')
    job_cancel(user)
    raise 'This method must be defined in each subclass of Job, including the call of the cancel method for each of its child objects.'
  end

  def terminate(user='system')
    job_cancel(user)
    raise 'This method must be defined in each subclass of Job, including the call of the terminate method for each of its child objects.'
  end

  def perform
    raise 'The perform is subclass specific, so define one there.'
  end

  def rsyncmove(src, dest, post_method=nil)
    self.reload  # make sure the job is up to date
    return unless self.file_system_status == FILE_SYSTEM_STATUS_MOVING    # job contents are no longer moving, terminate the rsyncmove

    # make sure trailing slashed are used on src and dest to set rsync mode of operation (do not create dir in dir)
    if !src.match(/\/$/)
      src = src + "/"
    end

    if !dest.match(/\/$/)
      dest = dest + "/"
    end

    if dest.match(/:/)
      mkdircommand = ""
    else
      mkdircommand = "mkdir -p #{dest} && "
    end

    Delayed::Worker.logger.debug("JOB #{self.id.to_s} Rsync move start " + src + " to " + dest + " on " + run_server)
    rsynccmd = "#{mkdircommand}chmod -R 755 #{src} && rsync --remove-source-files --partial -arv #{src} #{dest} 2>&1 && rm -rf #{src} 2>&1 ; echo exitcode:$?"
    rres =  ssh(rsynccmd).chomp
    Delayed::Worker.logger.debug(rres)
    exitcode = rres.scan(/exitcode:(.*)/).flatten[0]
    if exitcode.to_i != 0
      resultstr = "JOB #{self.id.to_s}  FAILURE: Rsync move FAILED " + src + " to " + dest + " at " + DateTime.now.to_s + " on " + run_server + " exitcode #{exitcode}"
      Delayed::Worker.logger.error(resultstr)
      self.fail_reason = resultstr
      self.log_action(resultstr)
      begin
        self.save!
      rescue ActiveRecord::StaleObjectError
        self.reload
        self.log_action(resultstr)
        if self.file_system_status == FILE_SYSTEM_STATUS_MOVING
          # although the job has been updated, it did not affect this rsyncmove
          save   # no save! as we are already in an exception rescue clause, if we can log the action great, otherwise, too bad
          raise resultstr
        else
          # the rsyncmove is no longer relevant
          self.log_action("File system status is #{self.file_system_status_str} instead of #{@@file_system_status_array[FILE_SYSTEM_STATUS_MOVING]}, cancelling the delayed move action.")
          save!
        end
      end
      raise resultstr
    else
      Delayed::Worker.logger.info("JOB #{self.id.to_s} SUCCESS: Rsync move completed " + src + " to " + dest + " at " + DateTime.now.to_s + " on " + run_server)
    end

    begin
      self.set_file_system_status(FILE_SYSTEM_STATUS_FREE)
      save!
    rescue ActiveRecord::StaleObjectError
      # stale object, reload it and if the file_system_status has still the expected value, retry to set it to FILE_SYSTEM_STATUS_FREE, otherwise exit without calling the post_method
      self.reload
      if self.file_system_status == FILE_SYSTEM_STATUS_MOVING
        self.set_file_system_status(FILE_SYSTEM_STATUS_FREE)
        save!
      else
        Postoffice.create_job_asynchronous_move_terminated(self.id, dest)
        post_method = nil
      end
    end

    if post_method
      Delayed::Worker.logger.info("JOB #{self.id.to_s} RSYNCMOVE Performing Post Processing : " + post_method)
      self.send(post_method)
      Delayed::Worker.logger.info("JOB #{self.id.to_s} RSYNCMOVE Performing Post Processing : " + post_method + " completed")
    end
  end

  def move(src, dest)
    # rsync move will verify successful file transfer #rsynccmd = "rsync --remove-source-files --partial
    # --progress -arv #{src} #{dest} && rm -rf #{src} >> /dev/null 2> /dev/null ; echo $?" #rres =
    # ssh(rsynccmd).chomp #if rres.to_i  != 0
    #  raise "Rsync move failed for #{src} to #{dest} exitcode: #{rres}"
    # #end

    cmd = "mkdir -p #{dest} && cp -rf #{src} #{dest} && rm -rf #{src} 2>&1"
    return ssh(cmd).chomp
  end

  def get_dir_size(path)
    cmd = "cd #{path} && du -cs . | head -1 | cut -d '.' -f 1"
    result = ssh(cmd).chomp.strip.to_i
    raise "Dir size could not be determined" if result == 0
    return result
  end

  def fix_bug_tracker_id
    unless self.bug_tracker_id.blank?
      self.bug_tracker_id.strip!
      self.bug_tracker_id.gsub!(/\s/,'_')
    end
  end

  def log_action(log,user=nil)
    if user
      name_user = user.full_name
    else
      name_user = "system"
    end
    logging = "#{Time.now.to_formatted_s(:db)} #{name_user}: #{log}"
    self.action_log = "#{logging}\r\n#{self.action_log}"
  end

  def log_scheduler(log,user=nil)
    name_user = user ? user.full_name : 'system'
    self.scheduler_log = "#{Time.now.to_formatted_s(:db)} #{name_user}: #{log}"
  end

  def add_log_scheduler(log)
    self.scheduler_log += "\r\n#{log}"
  end

  def clear_dir(directory)
    unless directory.blank?
      cmd = "chmod -R 777 #{directory};rm -rf #{directory}"
      ssh(cmd)
    end
  end

  def locate_working_dir
    if !self.working_dir.blank?
      return ssh("find /volumes1/dakota -type d -name #{self.working_dir} 2</dev/null").strip
    end
  end

  def locate_output_dir
    #return nil if working_dir.to_s.index(/^w_/) == 0
    output_dir = working_dir.gsub(/^w_/, "o_")
    return ssh("find /volumes1/dakota -type d -name #{output_dir} 2</dev/null").strip
  end

  def clear_scripts
    raise 'The clear scripts method is subclass specific, so define one there.'
  end

  def clear_working_dir
    # only clean working dir if there might be actually something to clean, this to prevent errors because of server unavailability
    # while no server access is actually needed
    return  unless [FILE_SYSTEM_STATUS_ON_SERVER, FILE_SYSTEM_STATUS_RESERVED, FILE_SYSTEM_STATUS_MOVING].include?(self.file_system_status)
    unless run_server.to_s.blank?
      clear_dir(locate_working_dir)
      clear_dir(locate_output_dir)
    end
    set_file_system_status(FILE_SYSTEM_STATUS_FREE)
    save!
  end

  def done?
    return self.status >= STATUS_FAILED
  end

  def estimate_duration
    return 0 # unknown
  end

  def busy?
    return self.file_system_status == FILE_SYSTEM_STATUS_MOVING && !self.run_server.blank?
  end

  def derived_job
    return Job.find_by_original_job_id(self.id)
  end

  def end_status?
    return [STATUS_CANCELLED, STATUS_COMPLETE, STATUS_RESCHEDULED].include?(self.status)
  end

  def rescheduled?
    self.status == STATUS_RESCHEDULED
  end

  def allow_cancel?
    return !end_status?
  end

  def allow_cleanup?
    return [FILE_SYSTEM_STATUS_ON_SERVER, FILE_SYSTEM_STATUS_RESERVED].include?(self.file_system_status) && self.status >= STATUS_FAILED
  end

  def allow_edit?
    return [STATUS_PLANNED, STATUS_QUEUED, STATUS_HOLD, STATUS_MANUAL].include?(self.status)
  end

  def allow_reschedule?
    allow_status_change?(STATUS_RESCHEDULED)
  end

  def working_dir_exists?
    if !self.run_server
      return false
    end
    if !self.working_dir.blank?
      if self.locate_working_dir == ''
        return false
      else
        return true
      end
    else
      return false
    end
  end

  def output_dir_exists?
    if !self.working_dir.blank?
      if self.locate_output_dir == ''
        return false
      else
        return true
      end
    else
      return false
    end
  end

end
