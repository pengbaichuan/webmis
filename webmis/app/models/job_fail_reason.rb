class JobFailReason < ActiveRecord::Base
  validates :reason,:job_type, :presence => true

  def self.job_types_select
    Job.group(:type).collect{|j|[j.type.titleize,j.type]}
  end  
end
