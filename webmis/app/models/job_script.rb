class JobScript
  attr_accessor :type, :parameters, :pre_improvement_steps, :post_improvement_steps, :data, :comments,
    :working_dir_prefix, :number_of_update_regions, :production_orderline_id, :product_id, :conversion_id,
    :main_executable, :conversiontool_code, :cvtool_bundle_name, :tag_list_string, :updated_by, :created_at, :updated_at

  TYPE_BASH   = "bash"
  TYPE_DAKOTA = "dakota"
  TYPE_FIXED  = "fixed"

  def initialize(json_str="[]")
    json = JSON.parse(json_str)
    json.each do |k,v|
      self.send "#{k}=", v
    end
  end

  def initialize_copy(orig)
    # find all attributes by searching for all available setters
    setters = self.methods.grep(/\w=$/)
    setters.each do |setter|
      # get the attribute
      attribute = setter.to_s.chomp("=")
      # if the attribute has a dup method, use it, otherwise ignore it by rescueing the resulting TypeError exception
      self.__send__ "#{setter}", orig.__send__(attribute).dup rescue TypeError
    end
  end

  def set_type(type)
    case type
    when TYPE_BASH
      @type = TYPE_BASH
      @parameters = []
      @pre_improvement_steps = nil
      @post_improvement_steps = nil
      @data = ""
    when TYPE_DAKOTA
      @type = TYPE_DAKOTA
      @comments = []
      @working_dir_prefix = ""
      @parameters = []
      @number_of_update_regions = ""
      @pre_improvement_steps = []
      @post_improvement_steps = []
      @data = []
    when TYPE_FIXED
      @type = TYPE_FIXED
      @parameters = nil
      @pre_improvement_steps = nil
      @post_improvement_steps = nil
      @data = ""
    else
      raise "Unsupported script type #{type}"
    end
  end

  def is_valid
    case type
    when TYPE_BASH
      valid = @parameter.class == Array &&
              @pre_improvement_steps.nil? &&
              @post_improvement_steps.nil? && 
              @data.class == String
    when TYPE_DAKOTA
      valid = @comments.class == Array &&
              @parameters.class == Array && 
              @pre_improvement_steps.class == Array && 
              @post_improvement_steps.class == Array &&
              @data.class == Array
    when TYPE_FIXED
      valid = @working_dir_prefix.nil? && @parameters.nil? && !@data.nil?
   end
    return valid
  end

  def to_script(job_type_id=nil, id=nil)
    case type
    when TYPE_BASH
      return to_bash
    when TYPE_DAKOTA
      return to_dakota(job_type_id, id)
    when TYPE_FIXED
      return self.data
    else
      raise "Unsupported script type #{type}"
    end
  end

  def self.valid_json?(json_str)  
    JSON.parse(json_str)  
    return true  
  rescue JSON::ParserError  
    return false  
  end  

  def to_bash
    result = "#!/bin/bash\n"
    result += parameters_to_bash
    result += self.data
    return result
  end

  def to_dakota(mapinfo_job_id, id)
    result  = "# AUTO-GENERATED DAKOTA SCRIPT - EXPORTED BY WEBMIS\n"
    result += "# Last manual update by #{updated_by}\n" unless self.updated_by.blank?
    result += self.comments.collect{|x| "# #{x}\n"}.join
    result += "# TAGLIST #{tag_list_string} \n" unless self.tag_list_string.blank?
    result += "strict DAKOTA_PREFIX_WORKDIR \"#{self.working_dir_prefix}\"\n" unless self.working_dir_prefix.blank?
    result += "mapinfo #{mapinfo_job_id} \"#{id}\"\n" unless mapinfo_job_id.blank? or id.blank?
    result += "mapinfo CONVERSION_ID \"#{self.conversion_id}\"\n" unless self.conversion_id.blank?
    result += "mapinfo PRODUCTION_ORDERLINE_ID \"#{self.production_orderline_id}\"\n" unless self.production_orderline_id.blank?
    result += "mapinfo PRODUCT_ID \"#{self.product_id}\"\n" unless self.product_id.blank?
    result += "setenv NDS_NUMPROCS #{self.number_of_update_regions}\n" if self.number_of_update_regions.to_i > 1
    result += "setenv DAKOTAPROCS \"" + (self.pre_improvement_steps+[self.main_executable] + self.post_improvement_steps).join(" ") + "\"\n"
    result += parameters_to_dakota + "\n"
    result += "1 . " + self.conversiontool_code + "/" + self.cvtool_bundle_name + " " + self.tag_list_string + "\n"
    result += data_to_dakota
    return result
  end

  def parameters_to_bash
    # each parameter is a 2-tuple ([parameter_name, parameter_value])
    return self.parameters.collect{|x| "#{x[0]}=\"#{x[1]}\"\n"}
  end

  def parameters_to_dakota
    # each parameter is a 3-tuple ([parameter_text, parameter_name, parameter_value])
    return self.parameters.collect{|x| x.join(" ")}.join("\n")
  end

  def data_to_dakota
    data_arr = self.data.collect{|x| x.join(" ")}
    data_str = data_arr.join("\n")
    if data_arr.size == 1
      data_str += "\n"
    end
    return data_str
  end
end