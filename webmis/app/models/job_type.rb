class JobType < ActiveRecord::Base
  belongs_to :server

  JOB_TYPE_CONVERSION   = "ConversionJob"
  JOB_TYPE_EXTRACT      = "ExtractJob"
  JOB_TYPE_PACKING      = "PackingJob"
  JOB_TYPE_SHIPPING     = 'ShippingJob'
  JOB_TYPE_TEST_REQUEST = "TestRequestJob"
  JOB_TYPE_VALIDATION   = "ValidationJob"

  scope :allowed, -> { where(:allowed_yn => true) }
  scope :conversion, -> { where(:name => JOB_TYPE_CONVERSION) }
  scope :extract, -> { where(:name => JOB_TYPE_EXTRACT) }
  scope :packing, -> { where(:name => JOB_TYPE_PACKING) }
  scope :shipping, -> { where(name: JOB_TYPE_SHIPPING) }
  scope :test_request, -> { where(:name => JOB_TYPE_TEST_REQUEST) }
  scope :validation, -> { where(:name => JOB_TYPE_VALIDATION) }

  @@job_types_array = [JOB_TYPE_CONVERSION, JOB_TYPE_EXTRACT, JOB_TYPE_PACKING, JOB_TYPE_SHIPPING, JOB_TYPE_TEST_REQUEST, JOB_TYPE_VALIDATION]

  def self.job_types
    return @@job_types_array
  end

end
