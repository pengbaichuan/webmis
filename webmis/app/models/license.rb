class License < ActiveRecord::Base

  def gen_lic
    s =[]
    9.times{s << rand(10)}
    self.licensenumber = s.to_s
  end

  def self.license_ok(lic)
    allowed = false

    begin
      iv = []
      20.times{iv << rand(10)}
      tosend = []
      10.times{tosend << rand(10)}

      c = OpenSSL::Cipher::Cipher.new("aes-256-cbc")
      c.encrypt
      # your pass is what is used to encrypt/decrypt
      c.key = Digest::SHA1.hexdigest("mslic5329")
      c.iv = iv.to_s
      #tosend secret calculator number
      e = c.update(tosend.to_s)
      e << c.final
      puts "contacting http://demo.mapscape.nl/licenses/ch/2"
      uri = URI.parse("http://demo.mapscape.nl/licenses/ch/2")
      response,data = Net::HTTP.post_form(uri, {"i" => iv.to_s, "l" => e, "li" => lic})
      c = OpenSSL::Cipher::Cipher.new("aes-256-cbc")
      c.decrypt
      c.key = Digest::SHA1.hexdigest("mslic53295")
      c.iv = iv.to_s
      d = c.update(data)
      d << c.final
      restoken = d.to_s.scan(/token: (.*) status: (.*)/)
      allowed =  (restoken[0][0].to_s.to_i + 165923 == tosend.to_s.to_i) && restoken[0][1].to_s == "OK" && response.class.to_s == "Net::HTTPOK"
    rescue => error
      puts "License server cannot be contacted, please check your internet connection"
    end

    return allowed
    #render :text => response.inspect

  end
end
