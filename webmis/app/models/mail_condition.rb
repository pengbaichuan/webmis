class MailCondition < ActiveRecord::Base
  belongs_to :user_mail_conditions, :foreign_key => 'user_mail_conditions_id'
  validates :field_value, uniqueness:true
  validates :field,:field_value, :presence => true
end
