class MailEvent < ActiveRecord::Base
  has_many :user_mails
  has_many :users, :through => :user_mails
end
