class Mediatype < ActiveRecord::Base
validates :name, presence:true
validates :ref, presence:true, uniqueness:true
end
