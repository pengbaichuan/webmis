class Ordertype < ActiveRecord::Base
  has_many :process_transitions
  belongs_to :product_category

  validates :product_category_id, :presence => true, :if => "!no_pds_yn.present?"
  validates :name, :presence => true
  accepts_nested_attributes_for :process_transitions

  STATUS_ACTIVE        =  20
  STATUS_OBSOLETE      = 999

  @@statusarray       = Array.new
  @@statusarray[STATUS_ACTIVE]   = 'Active'
  @@statusarray[STATUS_OBSOLETE] = 'Obsolete'

  scope :active, -> {where(:status => STATUS_ACTIVE)}

  before_save :defaults

  def defaults
    if self.status.blank?
      self.status = STATUS_ACTIVE
    end
  end

  def status_str
    return  @@statusarray[status]
  end

  def self.status_array
    return @@statusarray
  end
  
  def self.status_hash
    s_hash = Hash.new
    @@statusarray.each do |s|
      if !s.nil?
        s_hash[s] = @@statusarray.index(s)
      end
    end
    return s_hash.sort_by { |k,v| v }
  end

  def sorted_process_transitions
    r = ProcessTransition.where(:ordertype_id => self.id)
    return r.order('process_transitions.sequence')
  end

  def serialize(obj=self,processed=[])
    return nil if !obj
    return nil if processed.index(obj)
    processed << obj

    objhash = obj.attributes
    obj.class.reflect_on_all_associations.each do |assoc|
      next if assoc.name.to_s == "product_category"
      assoc_obj = obj.send(assoc.name)
      next if assoc_obj == self
      if assoc_obj.respond_to? :each # iterable?
        objhash[assoc.name.to_sym] = []
        assoc_obj.each do |as_obj|
          next if as_obj == self
          a = serialize(as_obj,processed)
          objhash[assoc.name.to_sym] << a
        end
      else
        objhash[assoc.name.to_sym] = serialize(assoc_obj,processed)
      end
    end
    return objhash
  end

  
end
