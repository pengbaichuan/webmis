class OutboundOrder < ActiveRecord::Base
  has_many :outbound_orderlines, :dependent => :destroy
  has_many :packings, :dependent => :destroy
  belongs_to :external_contact, :foreign_key => :contact_id
  belongs_to :distribution_list_member
  belongs_to :distribution_list
  belongs_to :company, :foreign_key => :shipto_id
  belongs_to :file_transfer_account
  belongs_to :shipping_specification
  validates :courier, :presence => true
  self.inheritance_column = "ruby_type"

  STATUS_MANUAL     =   0
  STATUS_PLACEHOLDER=   5
  STATUS_DRAFT      =  10
  STATUS_PACKING    =  50
  STATUS_SHIPPING   = 100
  STATUS_FAILED     = 180
  STATUS_CANCELLED  = 190
  STATUS_SHIPPED    = 200
  STATUS_COMPLETE   = 250

  @@status_array     = Array.new
  @@status_array[STATUS_MANUAL]      = 'MANUAL'
  @@status_array[STATUS_PLACEHOLDER] = 'STILL IN PRODUCTION'
  @@status_array[STATUS_DRAFT]       = 'DRAFT'
  @@status_array[STATUS_PACKING]     = 'PACKING'
  @@status_array[STATUS_SHIPPING]    = 'SHIPPING'
  @@status_array[STATUS_FAILED]      = 'FAILED'
  @@status_array[STATUS_CANCELLED]   = 'CANCELLED'
  @@status_array[STATUS_SHIPPED]     = 'SHIPPED'
  @@status_array[STATUS_COMPLETE]    = 'COMPLETE'

  def status_str 
    return  @@status_array[status]
  end

  def self.status_array
    return @@status_array
  end

  def self.status_hash
    s_hash = Hash.new
    @@status_array.each do |s|
      if !s.nil?
        s_hash[s] = @@status_array.index(s)
      end
    end
    return s_hash.sort_by { |k,v| v }
  end

  def allow_status_change?(new_status)
    allow_status?(new_status) && self.status != new_status
  end

  def allow_status?(new_status)
    old_status = self.status || new_status    # handle uninitialized begin status
    case new_status
      when STATUS_MANUAL
        # MANUAL means no automation support, you can choose for it from draft or failed status.
        [STATUS_DRAFT, STATUS_MANUAL, STATUS_FAILED].include?(old_status)
      when STATUS_PLACEHOLDER
        # PLACEHOLDER is used and handled by the automatic orderflow of the production order
        [STATUS_DRAFT, STATUS_PLACEHOLDER].include?(old_status) && self.courier == 'Automatic' && self.file_transfer_account
      when STATUS_DRAFT
        # the 'begin' status of the automatic shipping flow itself. The outbound order needs to be set up for automatic shipping and the correct
        # file_transfer_account needs to be known.
        [STATUS_MANUAL, STATUS_PLACEHOLDER, STATUS_DRAFT, STATUS_FAILED, STATUS_PACKING, STATUS_SHIPPING].include?(old_status) &&
            self.courier == 'Automatic' && self.file_transfer_account && self.outbound_orderlines.any? { |orderline| orderline.packing_lines.active.empty? }
      when STATUS_PACKING
        # the first automated step of the shipping flow, the packing is done through packing jobs. This step kicks in once all outbound orderlines have been
        # assigned to a packing (or when a packing which was to be shipped is cancelled instead)
        [STATUS_PLACEHOLDER, STATUS_DRAFT, STATUS_PACKING, STATUS_SHIPPING].include?(old_status) && self.courier == 'Automatic' && self.file_transfer_account &&
            !self.outbound_orderlines.any? { |orderline| orderline.packing_lines.active.empty? } && self.packings.where(status: Packing::STATUS_PROCESSING).any?
      when STATUS_SHIPPING
        # the second automated step of the shipping flow, the shipping of the packing results is done through shipping jobs.
        # all outbound orderlines should still be assigned to a packing and all packings should at least have reached the status READY (i.e. the packing jobs are finished)
        [STATUS_PACKING, STATUS_SHIPPED, STATUS_FAILED].include?(old_status) && self.courier == 'Automatic' && self.file_transfer_account &&
            !self.outbound_orderlines.any? { |orderline| orderline.packing_lines.active.empty? } &&
            !self.packings.any? {|packing| packing.status < Packing::STATUS_READY }
      when STATUS_FAILED
        [STATUS_SHIPPING, STATUS_FAILED].include?(old_status)
      when STATUS_CANCELLED
        [STATUS_MANUAL, STATUS_PLACEHOLDER, STATUS_DRAFT, STATUS_PACKING, STATUS_SHIPPING, STATUS_FAILED, STATUS_CANCELLED].include?(old_status)
      when STATUS_SHIPPED
        [STATUS_SHIPPING, STATUS_COMPLETE].include?(old_status) && self.courier == 'Automatic' && self.file_transfer_account &&
            self.packings.any? { |packing| packing.status == Packing::STATUS_SHIPPED } && self.packings.where("packings.status < #{Packing::STATUS_CANCELLED}").empty?
      when STATUS_COMPLETE
        [STATUS_MANUAL, STATUS_COMPLETE].include?(old_status) && self.packings.where("packings.status < #{Packing::STATUS_CANCELLED}").empty?
      else
        false      # undefined status
    end
  end

  def set_status(new_status)
    result = allow_status_change?(new_status)
    self.status = new_status if result
    result
  end

  def update_status
    # this method is called whenever the status of a packing changes.
    # an update caused by a packing status change should not resolve a failed outbound_order status, such failures must be handled manually
    return if status == STATUS_FAILED

    if self.courier != 'Automatic' || !self.file_transfer_account
      # the outbound order is not set to Automatic or the account to be used has not been specified yet, set to manual
      set_status STATUS_MANUAL
    elsif allow_status_change? STATUS_DRAFT
      # as long as there are outbound_orderlines which are not in a packing, the status is DRAFT
      set_status STATUS_DRAFT
    elsif allow_status_change? STATUS_PACKING
      # if one of the packings is still being packed, the outbound_order is also PACKING
      set_status STATUS_PACKING
    elsif allow_status_change? STATUS_SHIPPING
      # the packing is done, there are no failures and there is at least one package which can be shipped.
      # start the shipping.
      set_status STATUS_SHIPPING
      save!
      ship
    elsif allow_status_change? STATUS_SHIPPED
      set_status STATUS_SHIPPED
      # Send mail to distribution_list members
      # TODO: Postoffice.packed_product_release_mail(self).deliver_now
    elsif allow_status_change? STATUS_CANCELLED && self.packings.where("packings.status != #{Packing::STATUS_CANCELLED}").empty?
      # all active packings have been cancelled
      set_status STATUS_CANCELLED
    end
    # STATUS_PLACEHOLDER, STATUS_FAILED & STATUS_COMPLETE are not set because of packing status changes but because of other triggers.
    save!
  end

  def before_save
    if (self.shipto_id)
      r = Company.find(self.shipto_id)
      if r 
        self.shipto_name = r.name
      end
    end
    if (self.contact_id)
      r = ExternalContact.find(self.contact_id)
      if r
        if r.first_name
          self.contact_person = r.first_name + " " + r.last_name
        else
          self.contact_person = r.last_name
        end
      end
    end
  end

  def check_amounts
    if self.outbound_orderlines.where('amount = 0').empty?
      # all outbound_orderlines are ready for packing and shipping so start!
      set_status STATUS_DRAFT
      save!
      self.packings.each do |packing|
        packing.create_packing_job
      end
    end
  end

  def ship
    return unless self.status == STATUS_SHIPPING
    if self.file_transfer_account.transfer_type == 'SFTP'
      ship_sftp
    else
      # TODO: Implement other transfer_types upon request
      set_status STATUS_FAILED
      self.save
      raise "Transfer type #{self.file_transfer_account.tranfer_type} does not support automatic shipment yet."
    end
  end

  def ship_sftp
    self.packings.where(status: Packing::STATUS_READY).each{ |packing| packing.ship_sftp }
  end

  def product_title
    title = ''
    if self.packings && self.packings.first.packing_lines && self.packings.first.packing_lines.first.outbound_orderline && self.packings.first.packing_lines.first.outbound_orderline.product
      if self.packings.first.packing_lines.first.outbound_orderline.product.productsets
        title = self.packings.first.packing_lines.first.outbound_orderline.product.productsets.last.name 
      else
        title = self.packings.first.packing_lines.first.outbound_orderline.product.name
      end
    else
      if self.outbound_orderlines.size > 0
        title = self.outbound_orderlines.first.product_name
      end
    end
    return title
  end

  def self.flag_related_databases_removed
    puts "flag_related_databases_removed started at " + DateTime.now
    age_in_months = ConfigParameter.where("cfg_name = 'age_removal_conversion_databases_in_months'").first
    if !age_in_months
      raise "Config parameter 'age_removal_conversion_databases_in_months' not configured"
    end
    age_in_months = age_in_months.cfg_value.to_i
    orders = OutboundOrder.where("send_date >= '#{Time.new-age_in_months.months}'").all
    orders.each do |od|
      od.outbound_orderlines.each do |ol|
        if ol.product_location
          cv = ol.product_location.conversion
          if cv
            cv.source_conversion_databases.each do |cd|
              if cd.file_system_status < ConversionDatabase::FILE_SYSTEM_STATUS_MARKED_FOR_REMOVAL
                cd.set_file_system_status(ConversionDatase::FILE_SYSTEM_STATUS_MARKED_FOR_REMOVAL)
                cd.removal_remark = "#{cv.productid} shipped at #{od.send_date.strftime('%m-%d-%Y')} - Ship Order : (#{od.id.to_s})"
                cd.save!
              end
            end
          end
        end
      end
    end
    puts "flag_related_databases_removed finished at " + DateTime.now
  end

  def possible_release_notes
    list = []
    self.outbound_orderlines.each do |outbound_orderline|

      # try to find the associated production_orderline as the release notes are stored with the production_orderline or their production_order.
      if outbound_orderline.production_orderline
        po = outbound_orderline.production_orderline
      elsif outbound_orderline.product_location && outbound_orderline.product_location.production_orderlines.size > 0
        po = outbound_orderline.product_location.production_orderlines.last
      elsif outbound_orderline.product
        po = ProductionOrderline.where("product_id = '#{outbound_orderline.product.volumeid}'").last
      end

      if po
        product_release_note = ProductReleaseNote.find_by_production_orderline_id(po.id)
        list << product_release_note if product_release_note && product_release_note.status >= ProductReleaseNote::STATUS_APPROVED 
        product_release_note = ProductReleaseNote.find_by_production_order_id(po.production_order.id)
        list << product_release_note if product_release_note && product_release_note.status >= ProductReleaseNote::STATUS_APPROVED
      end

    end
    return list.uniq
  end

  def release_mail_subject
    str_subj = Rails.env == "staging" || Rails.env == "development" ? "WebMIS DEVELOPMENT TEST * " : ""
    if self.packings.size > 0
      str_subj += "Mapscape delivery: #{product_title}"
    else
      str_subj += "Mapscape delivery: #{self.outbound_orderlines.collect{|ol|ol.product.productsets.collect {|ps|ps.setcode}  if ol.product}.compact.join} / #{self.outbound_orderlines.collect{|ol|ol.product_id unless ol.product_id.blank?}.compact.join(',')}"
    end
    return str_subj
  end
end

