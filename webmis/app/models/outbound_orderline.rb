class OutboundOrderline < ActiveRecord::Base
 validates :amount, :presence => true
 belongs_to :outbound_order
 belongs_to :stock
 has_one :conversion, :through => :stock
 has_many :outbound_orderlines, :foreign_key => "parent_linenr", :dependent => :destroy
 belongs_to :product_location
 belongs_to :product, :primary_key => :volumeid, :foreign_key => :product_id
 belongs_to :production_orderline
 belongs_to :parent, :class_name => "OutboundOrderline", :foreign_key => "parent_linenr"
 has_many :packing_lines
 has_many :packings, :through => :packing_lines

 def production_order_line
   @production_orderline = []
   if self.conversion
     if self.conversion.production_orderline
       @production_orderline = self.conversion.production_orderline
     end    
   end
   return @production_orderline
 end
end
