class Packing < ActiveRecord::Base
  belongs_to :outbound_order
  belongs_to :packing_specification
  has_many :packing_lines, :dependent => :destroy
  has_many :outbound_orderlines, :through => :packing_lines
  has_many :packing_jobs, :foreign_key => :reference_id
  has_many :shipping_jobs, foreign_key: :reference_id

  validates :outbound_order_id, :presence => true
  validates :packing_specification_id, :presence => true
  validates :status, :presence => true

  STATUS_DRAFT        =   0
  STATUS_PROCESSING   =  50
  STATUS_READY        = 100
  STATUS_SHIPPING     = 120
  STATUS_FAILED       = 190
  STATUS_CANCELLED    = 200
  STATUS_SHIPPED      = 300

  @@status_array       = Array.new
  @@status_array[STATUS_DRAFT]       = "Draft"
  @@status_array[STATUS_PROCESSING]  = "Processing"
  @@status_array[STATUS_READY]       = "Ready"
  @@status_array[STATUS_SHIPPING]    = "Shipping"
  @@status_array[STATUS_FAILED]      = "Failed"
  @@status_array[STATUS_CANCELLED]   = "Cancelled"
  @@status_array[STATUS_SHIPPED]     = "Shipped"

  scope :shipped, -> {where(status: STATUS_SHIPPED)}

  def self.status_array
    @@status_array
  end

  def self.status_hash
    s_hash = Hash.new
    @@status_array.each do |s|
      if !s.nil?
        s_hash[s] = @@status_array.index(s)
      end
    end
    return s_hash.sort_by { |k,v| v }
  end

  def status_str
    self.status ? @@status_array[self.status] : ""
  end

  def allow_status_change?(new_status)
    allow_status?(new_status) && self.status != new_status
  end

  def allow_status?(new_status)
    old_status = self.status || new_status    # handle uninitialized begin status
    case new_status
      when STATUS_DRAFT
        # initial status, no active jobs yet
        [STATUS_DRAFT].include?(old_status) && self.packing_jobs.active.empty? && self.shipping_jobs.active.empty?
      when STATUS_PROCESSING
        [STATUS_DRAFT, STATUS_PROCESSING].include?(old_status) && (self.packing_jobs.outstanding.any? || self.packing_jobs.on_server.any?)
      when STATUS_READY
        [STATUS_PROCESSING, STATUS_READY, STATUS_FAILED].include?(old_status) && self.packing_jobs.outstanding.empty? && self.packing_jobs.on_server.empty? && self.packing_jobs.where(status: Job::STATUS_COMPLETE).any? && self.shipping_jobs.active.empty?
      when STATUS_SHIPPING
        [STATUS_READY, STATUS_SHIPPING, STATUS_FAILED].include?(old_status) && (self.shipping_jobs.outstanding.any? || self.shipping_jobs.on_server.any? || old_status == STATUS_READY) && self.outbound_order.status == OutboundOrder::STATUS_SHIPPING
      when STATUS_FAILED
        [STATUS_READY, STATUS_SHIPPING, STATUS_FAILED].include?(old_status)
      when STATUS_CANCELLED
        [STATUS_DRAFT, STATUS_PROCESSING, STATUS_READY, STATUS_SHIPPING, STATUS_FAILED, STATUS_CANCELLED].include?(old_status)
      when STATUS_SHIPPED
        [STATUS_SHIPPING, STATUS_FAILED, STATUS_SHIPPED].include?(old_status) && self.shipping_jobs.outstanding.empty? && self.shipping_jobs.on_server.empty? && self.shipping_jobs.where(status: Job::STATUS_COMPLETE).any?
      else
        false      # undefined status
    end
  end

  def set_status(new_status)
    result = allow_status_change?(new_status)
    if result
      self.status = new_status
      self.save!
      self.outbound_order.update_status
    end
    result
  end

  def update_status
    # this function is called whenever the status of a packing_job or shipping_job changes (except when it changes to STATUS_RESCHEDULED)
    # Note that FAILED is not an end-status for a job, the failure must be addressed on the job level.
    # The STATUS_FAILED on the packing level is meant for failed operations in the shipment part of the Packing object itself.
    Packing.transaction do
      # an update caused by a packing_job or shipping_job status change, should not resolve a failed packing status, such failures must be handled manually.
      return if status == STATUS_FAILED
      if allow_status? STATUS_DRAFT
        set_status STATUS_DRAFT
      elsif allow_status? STATUS_PROCESSING
        set_status STATUS_PROCESSING
      elsif allow_status? STATUS_READY
        set_status STATUS_READY
      # shipping (STATUS_SHIPPING) is started from the outbound_order
      elsif allow_status? STATUS_SHIPPED
        set_status STATUS_SHIPPED
      end
    end
  end

  def create_packing_job
    filesize = 0
    Packing.transaction do
      cp = PackingJob.new(status: Job::STATUS_PLANNED, project_id: Project.default.id)
      set_status STATUS_PROCESSING
      self.packing_jobs << cp
      save!
      cp.run_script  = "#!/bin/bash\n"
      cp.run_script += "PACKINGFILE=\"#{self.filename}\"\n"
      cp.run_script += "PRODUCTS=\""
      self.packing_lines.each do |line|
        outbound_orderline = line.outbound_orderline
        if outbound_orderline.product_location
          path = outbound_orderline.product_location.storage_path
        else
          path = outbound_orderline.filename
        end
        cp.run_script += path
        cp.run_script += "\n"
        filesize += File.stat(path).size
      end
      filesize_mb = (filesize.to_f / 2**20).round(0).to_i  # filesize in MB, rounded
      cp.run_script += "\"\n"
      cp.run_script += self.packing_specification.run_script
      cp.schedule_manual = self.packing_specification.schedule_manual ? self.packing_specification.schedule_manual : false
      cp.schedule_cores = self.packing_specification.schedule_cores ? self.packing_specification.schedule_cores : 1
      schedule_memory_base = self.packing_specification.schedule_memory_base ? self.packing_specification.schedule_memory_base : 0
      schedule_memory_times_input = self.packing_specification.schedule_memory_times_input ? self.packing_specification.schedule_memory_times_input : 0
      cp.schedule_memory = schedule_memory_base + (schedule_memory_times_input * filesize_mb)
      schedule_diskspace_base = self.packing_specification.schedule_diskspace_base ? self.packing_specification.schedule_diskspace_base : 0
      schedule_diskspace_times_input = self.packing_specification.schedule_diskspace_times_input ? self.packing_specification.schedule_diskspace_times_input : 0
      cp.schedule_diskspace = schedule_diskspace_base + (schedule_diskspace_times_input * filesize_mb)
      cp.schedule_allow_virtual = self.packing_specification.schedule_allow_virtual ? self.packing_specification.schedule_allow_virtual : false
      cp.schedule_exclusive = self.packing_specification.schedule_exclusive ? self.packing_specification.schedule_exclusive : false
      cp.save
      cp.queue
      cp
    end
  end

  def incoming_dir
    if self.outbound_order.file_transfer_account.address == 'localhost' && self.outbound_order.file_transfer_account.password.blank?
      File.join ConfigParameter.get('shared_temp_dir'), 'incoming'
    else
      'incoming'
    end
  end

  def outgoing_dir
    if self.outbound_order.file_transfer_account.address == 'localhost' && self.outbound_order.file_transfer_account.password.blank?
      File.join ConfigParameter.get('shared_temp_dir'), 'outgoing', self.subdirectory.to_s
    else
      File.join 'outgoing', self.subdirectory.to_s
    end
  end

  def ship_sftp
    return unless set_status STATUS_SHIPPING
    begin
      save!

      fta = outbound_order.file_transfer_account
      Net::SFTP.start(fta.address, fta.user_name, password: fta.password) do |sftp|
        # create subdirectories on the sftp server if specified and necessary
        to_be_created = []
        h = [self.outgoing_dir]
        exists = false
        while !exists do
          begin
            sftp.stat!(h[0])
            # you can only stat existing objects, so success
            exists = true
          rescue
            # h[0] does not exist, retry with its parent directory
            h = File.split(h[0])
            to_be_created << h[1]
            # sanity check, the root directory will be either '.' or '/', if those don't exist all bets are off
            # the sftp access will go wrong elsewhere, for now just make sure this loops ends.
            exists = h[0].length <= 1
          end
        end
        existing_dir = h[0]
        to_be_created.reverse_each do |dir|
          existing_dir = File.join existing_dir, dir
          sftp.mkdir(existing_dir)
        end

        #remove original file
        sftp.remove(File.join(self.outgoing_dir,  self.filename))
        create_shipping_job
      end
    rescue => e
      self.status = STATUS_FAILED
      save
      puts e.message
      puts e.backtrace
    end
  end

  def create_shipping_job
    OutboundOrder.transaction do
      sj = ShippingJob.new(status: Job::STATUS_PLANNED, project_id: Project.default.id)
      self.shipping_jobs << sj
      save!
      sj.run_script  = "#!/bin/bash\n"
      sj.run_script += "export SSHPASS=#{self.outbound_order.file_transfer_account.password}\n"
      sj.run_script += "sshpass -e sftp -oBatchMode=no -oStrictHostKeyChecking=no -b - #{self.outbound_order.file_transfer_account.user_name}@#{self.outbound_order.file_transfer_account.address}  << !\n"
      sj.run_script += "put #{File.join self.path, self.filename} #{File.join incoming_dir, self.filename}\n"
      sj.run_script += "rename #{File.join incoming_dir, self.filename} #{File.join outgoing_dir, self.filename}\n"
      sj.run_script += "!"

      sj.schedule_manual = false
      sj.schedule_cores = 1
      sj.schedule_memory = 0
      sj.schedule_diskspace = 0
      sj.schedule_allow_virtual = true
      sj.schedule_exclusive = false
      sj.save!
      sj.queue
      sj
    end
  end

  def full_path
    File.join(self.path.to_s,self.subdirectory.to_s,self.filename.to_s)
  end

  def destination_path
    if self.outbound_order.file_transfer_account && self.outbound_order.file_transfer_account.address == "localhost"
      ftp_root = ConfigParameter.get("shared_temp_dir")
    else
      ftp_root = ""
    end
    ftp_root + self.subdirectory.to_s + (self.subdirectory.blank? || self.subdirectory.match(/\/$/) ? "" : "/")  + self.filename.to_s
  end

  def download_path
    if self.outbound_order && self.outbound_order.file_transfer_account
      return "https://" + self.outbound_order.file_transfer_account.address + "/" + self.outbound_order.file_transfer_account.user_name +
            (self.destination_path.match(/^\//) ? "" : "/") + self.destination_path
    else
      return ""
    end
  end

  def cancel(name_user = nil)
    Packing.transaction do
      set_status STATUS_CANCELLED
      self.packing_jobs.each{ |j| j.cancel(name_user) }
      self.shipping_jobs.each{ |j| j.cancel(name_user) }
      save!
    end

  end

end
