class PackingJob < Job
  belongs_to :packing, :foreign_key => :reference_id
  belongs_to :fail_category_logged_by_user, :class_name => "User",:foreign_key => "fail_category_logged_by_user_id"
  has_one :outbound_order, :through => :packing
  after_update :update_packing_status

  comma :tagged_failed_jobs_report do    
    type 'Job type'
    fail_category 'Category'
    fail_category_logged_by_user :full_name => 'Tagged by'
    fail_category_logged_on 'Date tagged'
    bug_tracker_id
    fail_reason
    start_time
    finish_time
    conversion_id
    run_server
    action_log
    id
  end


  def duplicate(user=nil)
    PackingJob.transaction do 
      packing_job_clone = self.dup
      packing_job_clone.original_job_id = self.id
      # the rescheduled job has to be saved first because of the after_update hook
      self.status = STATUS_RESCHEDULED
      self.save!
      packing_job_clone.status = STATUS_PLANNED
      packing_job_clone.run_server = nil
      packing_job_clone.start_time = nil
      packing_job_clone.finish_time = nil
      packing_job_clone.file_system_status = FILE_SYSTEM_STATUS_FREE
      packing_job_clone.fail_reason = nil
      packing_job_clone.stdout_log = nil
      packing_job_clone.scheduler_log = nil
      packing_job_clone.working_dir = nil
      packing_job_clone.log_action("Cloned from packing job #{self.id}",user)
      packing_job_clone.save!
      packing_job_clone.reload
      return packing_job_clone
    end
  end

  def regenerate(user)
    PackingJob.transaction do
      job_cancel(user, rescheduled_yn = true)
      return self.packing.create_packing_job
    end
  end

  def perform
    pack
  end
  
  def pack
    begin
      # puts "Executing packing job perform for  " + self.id.to_s + " lock version : " + self.lock_version.to_s
      return unless allow_status_change?(STATUS_PROCESSING) && self.file_system_status == FILE_SYSTEM_STATUS_FREE
      clear_scripts
      cid = File.expand_path("#{ConfigParameter.get("dakota_scripts_dir", self.project_id.to_s)}/packing_#{id.to_s}")
      self.start_time = DateTime.now
      self.finish_time = nil
      self.stdout_log = ""
      self.fail_reason = ""
      self.exitcode = nil
      save!

      working_dir_prefix = Time.now.strftime("%y%m%d-%H%M%S")
      self.working_dir = File.join(ConfigParameter.get("packing_job_working_dir", self.project_id.to_s), "w_" + working_dir_prefix + "_" + self.id.to_s)
      running_script = self.run_script
      # the gsub is for the legacy case, kept in place as it won't harm a generated running_script
      File.open(cid, 'w') {|f| f.write(running_script.to_s.gsub("---->","").gsub("\r","")) }

      self.run_command = "cd #{self.working_dir};bash " + cid.to_s
      s = "#! /bin/bash\r\n"
      s = s + "echo PID: $$ > #{cid}.log\n"
      s = s + "export PROCESSID=$$\n"
      s = s + "mkdir -p #{self.working_dir}\n"
      s = s + "#{self.run_command} >> #{cid}.log\n"
      s = s + "echo $? >  #{cid.to_s}.done\n"
      File.open(cid + ".exec", 'w') {|f| f.write(s) }
      execstring = "source /data/users/#{ENV['prod_user']}/.profile;nohup sh #{cid}.exec > #{cid.to_s}.log 2>&1 &"
      ssh(execstring)
      # get pid
      pidexec= "cat  #{cid}.log | grep '^PID' | cut -d' ' -f2"
      pid= ssh(pidexec).chomp

      begin
        self.run_pid = pid
        self.set_status(STATUS_PROCESSING)
        self.set_file_system_status(FILE_SYSTEM_STATUS_ON_SERVER)
        self.log_action("#{self.status_str} packing job")
        save!
      rescue ActiveRecord::StaleObjectError => e
        # kill needed
        kill_linux_process(pid)
        clear_working_dir
        clear_scripts
        puts "Failed setting status to PROCESSING for job " + self.id.to_s + " .. killing stated linux process " + pid.to_s + " on " + self.run_server.to_s
        puts e.message
        puts e.backtrace
      end
    rescue Exception => e
      puts e.message
      puts e.backtrace
      self.stdout_log = stdout_log + "------------\r\n" + e.message + "\r\n======\r\n" + e.backtrace.join("\r\n")
      self.fail_reason = e.message + "\r\n======\r\n" + e.backtrace.join("\r\n")
      self.set_status(STATUS_FAILED)
      self.set_file_system_status(FILE_SYSTEM_STATUS_FREE)
      self.log_action("Packing job #{self.status_str}. #{e.message}")
      clear_scripts
    ensure
      save!
    end
  end

  def automonitor
    return unless status == STATUS_PROCESSING # only monitor running packing jobs
    # we currently just check whether the .done file exists
    cid = File.expand_path("#{ConfigParameter.get("dakota_scripts_dir", self.project_id.to_s)}/packing_#{id.to_s}")

    return unless File.file?("#{cid.to_s}.done") # packing process is still running
    begin
      contents = File.read("#{cid.to_s}.done")
      standardoutlog = File.read("#{cid.to_s}.log")
      taillog = ""
      if standardoutlog.size > 10000
        taillog = standardoutlog[standardoutlog.size-10000..-1]
      else
        taillog = standardoutlog
      end

      self.stdout_log = taillog
      m = self.working_dir
      # assume output will be in o_ directory
      source_path = m
      if contents.to_i == 0
        # success
        final_dir = File.join(ConfigParameter.get("packing_job_result_dir", self.project_id.to_s), self.id.to_s)
        PackingJob.transaction do
          self.exitcode = 0
          self.packing.path = final_dir
          self.packing.save!
          self.stdout_log =  stdout_log + "MOVING FILES FROM " + source_path + " TO FINAL DIR " + final_dir + "\r\n"
          self.set_file_system_status(FILE_SYSTEM_STATUS_RESERVED)     # currently no real disk space is being reserved
          self.set_status(STATUS_COMPLETE)
          self.set_file_system_status(FILE_SYSTEM_STATUS_MOVING)
          save!
        end
        # to be done asynchronously
        rsyncmove(source_path, final_dir, 'post_move') # asynchronous move
      else
        puts "Packing CRASHED : " + contents
        self.exitcode = contents.to_i
        self.stdout_log = taillog
        self.finish_time = DateTime.now
        self.set_status(STATUS_FAILED)
        if self.working_dir.blank?
          self.fail_reason = "Packing failed, No working_dir set."
          self.set_file_system_status(FILE_SYSTEM_STATUS_FREE)
        else
          self.fail_reason = "Packing failed failed."
        end
      end
    rescue Exception => e
      puts e.message
      puts e.backtrace
      self.stdout_log = stdout_log + "------------\r\n" + e.message + "\r\n======\r\n" + e.backtrace.join("\r\n")
      self.fail_reason = e.message + "\r\n======\r\n" + e.backtrace.join("\r\n")
      self.set_status(STATUS_FAILED)
      self.finish_time = DateTime.now
    ensure
      save!
    end
  end

  def post_move
    PackingJob.transaction do
      self.set_file_system_status(FILE_SYSTEM_STATUS_FREE)
      self.stdout_log = stdout_log + "Moving of results was completed.\r\n"
      save!
      clear_scripts
    end
  end

  # calculate the MD5 checksum before the actual file move
  alias_method :job_rsyncmove, :rsyncmove
  def rsyncmove(src, dest, post_method=nil)
    src += "/" unless src.match(/\/$/)  # ensure the src directory has a trailing slash
    src_file = src + self.packing.filename
    Delayed::Worker.logger.debug("JOB #{self.id.to_s} Calculating MD5 checksum of " + src_file + " on " + run_server)
    md5cmd = "md5sum #{src_file}"
    res =  ssh(md5cmd).chomp
    Delayed::Worker.logger.debug(res)
    md5checksum = res.split[0] unless res.to_s.blank?
    if md5checksum && md5checksum.length == 32
      packing = self.packing
      packing.md5_checksum = md5checksum
      packing.save!
    else
      resultstr = "JOB #{self.id.to_s}  FAILURE: MD5 calculationg FAILED for " + src_file + " at " + DateTime.now.to_s + " on " + run_server
      Delayed::Worker.logger.error(resultstr)
      raise resultstr
    end

    job_rsyncmove(src,dest,post_method)
  end

  def clear_scripts
    # delete script files
    pid = File.expand_path("#{ConfigParameter.get("dakota_scripts_dir", self.project_id.to_s)}/packing_#{id.to_s}")
    FileUtils.rm_f("#{pid}")
    FileUtils.rm_f("#{pid}.done")
    FileUtils.rm_f("#{pid}.exec")
    FileUtils.rm_f("#{pid}.log")
  end

  def terminate(user = 'system')
    # for a packing job the terminate and cancel methods are equivalent
    cancel(user)
  end

  def cancel(user = 'system')
    job_cancel(user)
  end

  def reschedule(user_ref = 'system', manual_yn = false)
    return unless allow_reschedule?

    PackingJob.transaction do
      job_cancel(user_ref, rescheduled_yn = true)
      user = (user_ref.class == User ? user_ref : nil)
      new_job = duplicate(user)
      if manual_yn
        new_job.set_status(STATUS_MANUAL)
      else
        new_job.queue
      end
      new_job.save!

      return new_job
    end
  end

  def update_packing_status
    if (self.status_changed? || self.file_system_status_changed?) && self.status != STATUS_RESCHEDULED
      self.packing.update_status if self.packing
    end
  end

  handle_asynchronously :rsyncmove
end
