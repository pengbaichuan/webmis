class PackingLine < ActiveRecord::Base
  belongs_to :outbound_orderline
  belongs_to :packing

  validates :outbound_orderline_id, :presence => true
  validates :packing_id, :presence => true

  scope :active, -> {eager_load(:packing).where("packings.status != #{Packing::STATUS_CANCELLED}")}

end