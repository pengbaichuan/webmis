class PackingSpecification < ActiveRecord::Base
  version_fu

  has_many  :packings, :dependent => :nullify
  has_many  :shipping_specifications, :dependent => :nullify

  scope :active, -> {where('status != "obsolete"').order(:name)}
  include HandleVersioning
  validates :status, :workflow => true
  validates_uniqueness_of :name

  @@attrs_requiring_review = [:run_script]

  def self.attrs_requiring_review
    return @@attrs_requiring_review
  end

end
