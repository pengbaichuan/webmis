# Class used to store a parameter
class Parameter
    attr_reader :name
    @value
    @valuedescription
    @description
    attr_accessor :datatype
    attr_accessor :default

    # Constructor
    def initialize(name, value, valuedescription, description, datatype, default)
        @name = name
        @value = value
        @valuedescription = valuedescription
        @description = description
        @datatype = datatype
        @default = default
    end



    def name
      @name
    end

    def description
      @description
    end

    def mandatory
      false
    end

    def valuedescription
      @valuedescription
    end

    # Print the parameter to the writer
    # writer            Writer object
    # appendix          The appendix hash
    # configurationHash The hash containing parsed configuration files
    def printToPDF(writer, appendix, configurationHash)
        # print parameter info to writer
        writer.text @name, :style => :bold
        writer.move_down 5

        # Create link to appendix for .csv file values and add table from actual csv file to appendix
        if (@value.length > 4 && @value[@value.length-4, @value.length] == ".csv")
            writer.text "<i>Used value</i>: <link anchor='#{@name}'><u>Configurationfile</u></link>", :inline_format => true
            appendix[@name] = configurationHash[@value]
        else
            # print normal value
            writer.text "<i>Used value</i>: " + @value, :inline_format => true
        end

        # print description of used value
        if (@valuedescription)
            writer.text "<i>Description</i>: " + @valuedescription, :inline_format => true
        end

        # convert html tables in description
        list = parseHTMLtablestring(@description)
        headerset = false
        list.each do |element|
            if (element.is_a? String)
                # support html breaks
                element.gsub!("<br></br>", "\n")
                # Call writer.text on each seperate line
                element.split("\n").each do |line|
                    if (headerset)
                        writer.text line, :inline_format => true
                    else
                        writer.text "<i>Parameter description</i>: " + line, :inline_format => true
                        headerset = true
                    end
                end
            else
                # print table
                if (headerset)
                    writer.table(element, :cell_style => {:inline_format => true}, :header => true)
                else
                    writer.text "<i>Parameter description</i>: ", :inline_format => true
                    writer.table(element, :cell_style => {:inline_format => true}, :header => true)
                end
            end
        end
        writer.move_down 20
    end

    def printDebug(indent)
        puts indent + " " + @name
    end
end