class ParameterSetting < ActiveRecord::Base
  version_fu do
    belongs_to :design_tag, :class_name => '::DesignTag'
  end
  belongs_to :product_line
  belongs_to :design_tag
  belongs_to :project
  has_one :environment, :through => :project
  belongs_to :parameter_setting, :foreign_key => 'parent_id'
  # acts_as_ordered_taggable_on and :platform_list can be removed after a successfull database migration to parameter_settings
  acts_as_ordered_taggable_on :platforms
  include HandleVersioning
  validates :status, :workflow => true
  #validates :name, :uniqueness => { :scope => :project_id, :message => "should be unique within project" }
  #validates :product_line_id, :uniqueness => {:scope => :project_id, :message => "Platform already in use by other parameter set within this project."}
  scope :active, -> {where("status != 'obsolete'")}

  before_save :defaults

  def defaults
    self.project_id = Project.default.id if self.project_id.blank?
  end

  #  for now no required review as we are still developing
  #  @@attrs_requiring_review = [:parameterset]
  @@attrs_requiring_review = []

  def self.attrs_requiring_review
    return @@attrs_requiring_review
  end

  def self.options_for_select(prj_id)
      ParameterSetting.active.where(project_id: prj_id).order(:name).collect{|x| [x.name, x.name]}.compact.uniq
  end

  def to_dakota(platformparams, exe=nil)
    # still used?
    
    if self.parent_id
      to_dakota(platformparams, ParameterSetting.find(self.parent_id))
    end

    params = self.parameterset ? JSON.parse(self.parameterset) : []
    params.each do |param|
      if !exe.nil?
        platformparams[param["name"]] = param["value"] if exe.param_names.include?(param["name"])
      else
        platformparams[param["name"]] = param["value"]
      end
    end

    return platformparams
  end

  def self.find_correct_version(id, use_test_versions_yn, checklog = nil)
    if id.nil?
      checklog.add_info_line("No parameter setting specified (id is nil).") if checklog
      return nil
    end

    # id is not nil
    ps = ParameterSetting.find(id)
    if ps.nil?
      checklog.add_error_line("No parameter setting found with id '#{id}'.") if checklog
      checklog.update_result(Checklog::STATUS_EXCEPTION) if checklog
      raise "No parameter setting found with id '#{id}'."
    end

    # we have a parameter setting
    if ps.obsolete?
      # the parameter setting is obsolete
      if use_test_versions_yn
        # test versions should not use obsolete settings
        checklog.add_error_line("Parameter setting '#{id}' ('#{ps.name}') is obsolete.") if checklog
        checklog.update_result(Checklog::STATUS_EXCEPTION) if checklog
        raise "Parameter setting '#{ps.id}' is obsolete."
      elsif ps.latest_approved
        # although obsolete, the latest approved version is still valid
        checklog.add_info_line("Parameter setting '#{id}' ('#{ps.name}') is now obsolete. Using the latest approved version '#{ps.latest_approved.version}'.") if checklog
        return ps.use_latest_approved
      else
        # the parameter setting has never been approved and is now obsolete
        checklog.add_error_line("Parameter setting '#{id}' ('#{ps.name}') has never been approved and is now obsolete.") if checklog
        checklog.update_result(Checklog::STATUS_EXCEPTION) if checklog
        raise "No approved version of obsolete parameter setting '#{id}'."
      end
    end

    # we have a non-obsolete parameter setting
    if use_test_versions_yn
      checklog.add_debug_line("Using parameter setting '#{id}' ('#{ps.name}') version '#{ps.version}' with status '#{ps.status}'.") if checklog
      return ps
    elsif ps.status == 'approved'
      checklog.add_debug_line("Using the latest version ('#{ps.version}) of parameter setting '#{id}' ('#{ps.name}'), with status '#{ps.status}'.") if checklog
      return ps
    elsif ps.latest_approved
      checklog.add_debug_line("Use the latest approved version ('#{ps.latest_approved.version}') of parameter setting '#{id}' ('#{ps.name}'). The current version is '#{ps.version}'' and has status '#{ps.status}'.") if checklog
      return ps.use_latest_approved
    else
      checklog.add_error_line("Parameter setting '#{id}' ('#{ps.name}') has not been approved yet. Approve it first before proceeding.") if checklog
      checklog.update_result(Checklog::STATUS_EXCEPTION)
      raise "No approved version of parameter setting '#{id}'."
    end
  end

  def sync(sync_path=nil)
    self.sync_template_path = sync_path if sync_path
    if self.sync_template_path.blank?
      raise "No conversion template specified for syncing"
    end
    convscript_name = File.basename(self.sync_template_path)
    self.name = convscript_name
    self.save
    ParameterSetting.import_convscript(convscript_name, self.sync_template_path,nil,self)
  end

  def self.import_convscripts(base_dir="/data/id/release/CVENV/ConvScripts_latest/prod/nds/GMN_NTG55H")
    scripts = Dir.glob(base_dir + "/**/*")
    scripts.each do |script|
      basename = File.basename(base_dir)
      import_convscript(File.basename(script), script, basename)
    end
  end

  def self.import_convscript(name="GMN_NDS_MSP_EUR", file_path="/data/id/release/CVENV/ConvScripts_20140923/prod/nds/GMN_NTG55H/GMN_NDS_MSP_EUR", basename=nil, ps=nil)
    convscript = File.open(file_path, 'rb') { |f| f.read }
    if !ps     
      ps = ParameterSetting.find_by_name(name)
    end

    if !ps
      ps = ParameterSetting.new
      ps.status = "approved"
      ps.name = name
      ps.sync_template_name = name
      ps.sync_template_path = file_path
      ps.save!
    end
    
    if ps
      ps.name = name
      ps.sync_template_name = name
      ps.sync_template_path = file_path
      ps.save!
    end

    imports = convscript.scan(/^use (.*)/).flatten
    imports.each do |path|
      basename = basename = File.basename(path)
      parent = import_convscript(File.basename(path),"/data/id/release/CVENV/ConvScripts_latest/" +path,basename)
      ps.parent_id = parent.id
    end

    params = []

    all_params = Executable.param_list
    params_to_import = convscript.scan(/^parameter ([^ ]*) (.*)/)
    params_to_import.each do |param|
      paramname = param[0]
      index = all_params.index{|x| x["name"] == paramname}
      if index
        result = all_params[index].clone
        result["value"] = param[1].gsub("\"","").encode("ASCII-8BIT").force_encoding("utf-8").to_s
        params << result
      else
        puts  "Warning ::: Parameter " + paramname + " not found in toolspecs"
      end
    end

    release = File.realpath(file_path).scan(/ConvScripts_([^\/]*)\//)
    release_name = "UNKNOWN"
    release_name = release[0][0] if release[0] && release[0][0]
    ps.parameterset = params.to_json
    ps.sync_template_release = release_name
    ps.last_sync_at = DateTime.now
    ps.save!
    return ps
  end

  def generate_convscript_array
    parent = ParameterSetting.find(parent_id) rescue nil

    if parent
      result = ["# ConvScript #{parent.name} used by #{self.name}"]
      result += parent.generate_convscript_array
      result << ''
    else
      result = []
    end

    params_pf = JSON.parse(parameterset)
    used_params = []

    result << "# ConvScript #{self.name} (id: #{self.id}), version #{self.version} (#{self.status}) for project #{self.project.name rescue '-'}"
    params_pf.each do |param|
      next if ( param['value'].to_s.blank? ) # Unset parameter result is superfluous param+
      param_text = used_params.index(param['name']) ? 'param+' : 'parameter'
      used_params << param['name']

      if FileSystemUtil.is_a_number?(param['value'].to_s)
        result << [param_text, param['name'].to_s, param['value'].to_s].join(' ')
      else
        result << [param_text, param['name'].to_s,"\"#{param['value'].to_s}\""].join(' ')
      end
    end

    return result
  end

  def generate_convscript(filename)
    result = generate_convscript_array
    temp_path = FileSystemUtil.temp_file_path('convscript')
    File.open(temp_path, 'w') do |f|
      f.puts result.join("\n")
    end
    Server.ssh_localhost("rsync #{temp_path} #{filename}")
    File.unlink(temp_path)
  end

  def self.generate_convscript(project_id = Project.default.id)
    release_dir_name = FileSystemUtil.new_web_mis_script_sub_dir_release('CVENV', project_id)
    ParameterSetting.active.where("project_id = #{project_id}").each do |parameter_setting|
      target_dir_name = parameter_setting.product_line.abbr rescue nil
      target_dir = FileSystemUtil.web_mis_script_target_dir(release_dir_name, target_dir_name)
      filename = File.join(target_dir, FileSystemUtil.sanitized_filename(parameter_setting.name)) 
      parameter_setting.generate_convscript(filename)
    end

    FileSystemUtil.update_web_mis_script_sub_dir_latest(release_dir_name, 'CVENV', project_id)
  end

  def executables_with_parameters
    return_h = Hash.new
    settings = JSON.parse(self.parameterset)
    settings.each do |setting|
      param_prefix = "parameter "
      value = setting["value"]
      if !value.to_s.match(/\A[+-]?\d+?(\.\d+)?\Z/).nil?
        param_value = value.to_s
      else
        param_value = "\"#{value.to_s}\""
      end
      if setting["allocate"] && value == ""
        value = '#ALLOCATION-REQUIRED'
      end
      parameter_string = "#{param_prefix}#{setting["name"]} #{param_value}"
      if return_h.has_key?(setting["exe"])
        return_h[setting["exe"]] = return_h[setting["exe"]] << parameter_string
      else
        return_h[setting["exe"]] = [parameter_string]
      end
    end
    return return_h
  end

end
