class ParentReception < ActiveRecord::Base

 belongs_to :reception
  self.inheritance_column = :ruby_type


  def parent_reception
    Reception.find(parent_reception_id)
  end

end
