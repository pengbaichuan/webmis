class Part < ActiveRecord::Base
  validates :name, presence:true
  validates :location, presence:true
end
