class PoiMapping < ActiveRecord::Base
  belongs_to :poi_mapping_layout

  def self.import_file(pml_csv_file,poi_mapping_layout_id=nil)
    tmp_file = '/tmp/pmfunx.csv'
    `tail --lines=+10 #{pml_csv_file} | sed -e "1s/Description/OutputDescription/" > #{tmp_file}`
    mappingarr = CSV.read(tmp_file, :headers => true, :col_sep => '|', :quote_char => "|")
    record = Hash.new

    mappingarr.each do |mapping|
      maphash = mapping.to_hash

       record["poi_mapping_layout_id"] = poi_mapping_layout_id
       record["output_category"]       = maphash["Output Category ID id[sub]"]
       record["output_description"]    = maphash["OutputDescription"]
       record["logical_type"]          = maphash["Logical type"]
       record["map_or_filter_type"]    = maphash["Filter Type"]
       record["map_or_filter_value"]   = maphash["Filter Value"]
       record["input_description"]     = maphash["Description"]
       record["import_hash"]           = maphash

      poi_mapping = PoiMapping.new(record)
      if poi_mapping.save
        # nothing
      else
        raise "Mapping import failed with #{record.inspect}"
      end
    end
  end

end
