class Point < ActiveRecord::Base
  require 'fastercsv'

  def self.percentages_where(conditions)
    connection.select_one("select #{%w[ street_name postal_code house_number city_name
                                        country_name last_activity_date food_type brandname
                                        absolute_xcoordinate absolute_ycoordinate phone_number].map {|column|
                                      "(count(nullif(#{column}, '')) / count(*)) * 100 as #{column}"
                                    }.join(',')}
                             from #{table_name}
                            where #{sanitize_conditions(conditions)}")
  end

  def self.create_file_to_geomatch(internet_datasource_id)
    @ids = InternetDatasource.find(internet_datasource_id)
    @ds = Datasource.find(@is.datasource_id)
    @filepath = "/data/dsm/files/#{@ds.name} #{Time.now} results.txt"
    File.open(@filepath, 'w') do |f|
      fields = %w[ name_of_facility country_name postal_code city_name street_name
                   house_number absolute_xcoordinate absolute_ycoordinate phone_number id ]

      f.puts(fields.join('|'))
      @ids.points.each do |point|
        f.puts(fields.map {|field| point.read_attribute(field) }.join('|'))
      end
    end
    @filepath
  end

  def self.load_geomatcher(path)
    @totalmatchlvl1 = 0
    @totalmatchlvl2 = 0
    @totalconflvl1  = 0
    @totalconflvl2  = 0

    new_count = 0
    FasterCSV.foreach(path, { :col_sep => "|", :headers => :first_row }) do |row|
      h = row.to_hash
      next if h["ID"].blank?

      p = Point.find(h["ID"]) or next

      @internet_datasource_id = p.internet_datasource_id
      %w[
        db1_coord_x db1_coord_y db1_bestconflvl db1_conflvl db1_matchtech
        db1_urbrur db1_diglvl db1_attrlvl db1_roadtype db1_matchlvl db1_dist
        db1_cou db1_citloc db1_cit db1_strloc db1_str db1_hnr db2_coord_x
        db2_coord_y db2_bestconflvl db2_conflvl db2_matchtech db2_urbrur
        db2_diglvl db2_attrlvl db2_roadtype db2_matchlvl db2_dist db2_cou
        db2_citloc db2_cit db2_strloc db2_str db2_hnr distance
      ].each {|s| p.write_attribute(s, h[s.upcase]) }

      p.gm_upload_date = Time.now
      p.save

      @totalconflvl1  += p.db1_conflvl
      @totalmatchlvl1 += p.db1_matchlvl
      @totalconflvl2  += p.db2_conflvl
      @totalmatchlvl2 += p.db2_matchlvl
      new_count += 1
    end
    
    @ids = InternetDatasoure.find(@internet_datasource_id)
    %w[ conflvl1 matchlvl1 conflvl2 matchlvl2 ].each {|s|
      # compute averages taking any old averages into account
      old_average = @ids.read_attribute("average_#{s}") || 0
      old_count   = @ids.number_geomatched              || 0

      old_total = old_average * old_count
      new_total = instance_variable_get("total#{s}")

      new_average = (old_total + new_total) / (old_count + new_count)
      @ids.write_attribute("average_#{s}", new_average)
    }
    @ids.save

    new_count
  end

  def self.create_late_binding(internet_datasource_id)
    File.open('test.rb', 'w') do |f|
      f.puts("#RecordLayout:NAME|ISO_LAN_CODE|GDFCAT|COORDX|COORDY|IMPORTANCE|BRANDNAME|PHONE|POSTALCODE|HOUSENUMBER|STREET|CITY|COUNTRY|STATE|VANITYCITY|VANITYCOORDX|VANITYCOORDY|")

      Point.where(internet_datasource_id:internet_datasource_id).each {|point|
        point.absolute_xcoordinate = [ point.absolute_xcoordinate, point.db1_coord_x, point.db2_coord_x ].find {|x| not x.blank? } or next
        point.absolute_ycoordinate = [ point.absolute_ycoordinate, point.db1_coord_y, point.db2_coord_y ].find {|y| not y.blank? } or next
        point.category   = "7315"
        point.importance = "1"

        f.puts(%w[ name_of_facility iso_language_code category absolute_xcoordinate absolute_ycoordinate
                    importance brandname phone_number postal_code house_number street_name city_name
                    country_name state_name vanity_city_name vanity_city_xcoordinate vanity_city_ycoordinate ].map {|w| point.read_attribute(w) }).join('|')
      }
    end
  end

  def country_iso3_code
    c = Country.where("country_iso_code = '#{self.country_code.to_s.strip}'").first
    if c && c.country_iso3_code
      return c.country_iso3_code
    else
      return self.country_code
    end
  end

  def to_dhive
    poi = DhivePoi.new
    # mapping
    poi.id = self.id
    poi.BrandName = self.brandname
    poi.PlaceName = self.city_name
    poi.InputCategory = self.category
    poi.CreationTimeStamp = self.checked_at
    poi.LastUpdatedTimeStamp = self.updated_at
    poi.PermanentId = self.id
    poi.InputCategoryType = "INTERNET"
    poi.Name = self.name_of_facility
    poi.StreetName = self.street_name
    poi.HouseNumber = self.house_number
    poi.PostalCode = self.postal_code
    poi.PhoneNumber = self.phone_number
    poi.Fax = self.fax_number
    poi.Internet = self.deeplink
    poi.Email = self.email_address
    poi.SupplierId = self.internet_datasource_id
    poi.ISOCountrycode = self.country_iso3_code
    #poi.CalculatedGeomWGS84 = GeoRuby::SimpleFeatures::Point.from_x_y(0,0)
    #poi.Language =
    if self.absolute_xcoordinate && self.absolute_ycoordinate
          poi.InputGeomWGS84 = GeoRuby::SimpleFeatures::Point.from_x_y((self.absolute_xcoordinate.to_f*100000).round,(self.absolute_ycoordinate.to_f*100000).round,4326)
          if self.db1_conflvl && self.db1_conflvl > 0
            poi.OutputCoordinateQuality = (self.db1_conflvl.to_i*10)
          end
    #else
    #      poi.InputGeomWGS84 = GeoRuby::SimpleFeatures::Point.from_x_y(0,0)
    end


    attr = DhiveAttribute.new
    attr.AttributeName = "UNMAPPED_ADDRESS"
    attr.AttributeType = "String"
    attr.AttributeValue = self.unmapped_address
    poi.dhive_attributes << attr

    return poi
  end

  def geomatch
     require 'hpricot'
     uri = "/maps/geo?q=#{URI.escape(self.street_name.to_s)}+#{URI.escape(self.house_number.to_s)}+#{URI.escape(self.postal_code.to_s)}+#{URI.escape(self.city_name.to_s)}+#{URI.escape(self.country_name.to_s)}&output=kml&oe=utf8&sensor=false&key=ABQIAAAAw6UMqp1auneBKHwrwzM2khTDQfSbrqs8V1cdgUFigQXUxB81LhSokPFKCW7-tGtkpq5xu2I0kT3ThA"
     point = nil
     status = nil
     #puts "#{self.internet_datasource_id.to_s} - #{self.id.to_s} =  http://maps.google.com" + uri 
     begin
      http = Net::HTTP.new("maps.google.com")
      resp,res = http.get(uri)
      raise EOFError if resp.code != "200"
     rescue Timeout::Error, Net::ProtoRetriableError, EOFError => e #, SOCKSError::TTLExpired
       retry
     end
    doc = Hpricot(res)
    status = doc.search("//status/code").inner_text
    if doc.search("//point/coordinates")[0] && doc.search("//addressdetails")[0]
      self.db1_coord_y = doc.search("//point/coordinates")[0].inner_text.split(",")[0]
      self.db1_coord_x =doc.search("//point/coordinates")[0].inner_text.split(",")[1]
      self.db1_conflvl = doc.search("//addressdetails")[0].attributes["Accuracy"]
      self.db1_str = doc.search("//address")[0].inner_text
      point = GeoRuby::SimpleFeatures::Point.from_x_y(self.db1_coord_x,self.db1_coord_y)
      #self.save
    else
      self.db1_conflvl = 0
      #self.save
    end
    return {:geodata => point,:accuracy => self.db1_conflvl,:geo_address => self.db1_str, :status => status} 
  end
  
end

