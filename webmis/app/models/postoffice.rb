class Postoffice < ActionMailer::Base
   default :from => "noreply@mapscape.eu",
           :return_path => "noreply@mapscape.eu",
           :content_type => "text/html"
  layout 'mail'
  
  def dummy_mail
       mail(:to => "jos.decallafon@mapscape.eu,johan.hendrikx@mapscape.eu,maurice.schekkerman@mapscape.eu",
         :subject => "WebMIS Test Email") 
  end

  def subject_string(subj = "")
    subject_str = 'WebMIS '
    subject_str += 'DEVELOPMENT TEST => ' if Rails.env == "staging" || Rails.env == "development"
    subject_str +=  subj
    return subject_str
  end

  def recipients(mail_event,extra_addresses=nil)
    recipient_arr = []
    mail_event.users.each do |u|
      recipient_arr << u.email
    end
    
    if extra_addresses
      extra_addresses.split(',').each do |extra_address|
        recipient_arr << extra_address
      end
    end

    recipient_arr << 'noreply@mapscape.eu' if recipient_arr.empty?
    return recipient_arr.uniq.join(',')
  end

  def find_or_create_mail_event(name)
    m = MailEvent.find_by name: name
    if !m
      MailEvent.transaction do
        m = MailEvent.new
        m.name = name
        m.save
      end
    end
    return m
  end

  def get_conditioned_addresses_for_data_types(args,extra_address=nil)
    m = args[:mail_event]
    r = args[:reception]
    d = DataType.find(r.data_type_id).name

    recipients = []

    m.user_mails.each do |x|
      conditions = Array.new
      x.mail_conditions.each {|u| conditions << u.field_value }

      if (conditions.size > 0)
        if conditions.include?("#{d}")
          recipients << x.user.email
        end
      else
        recipients << x.user.email
      end
    end

    if extra_address
      recipients << extra_address
    end

    recipients << 'noreply@mapscape.eu' if recipients.empty?
    return recipients.uniq.join(',')
  end
  
  def contract_end(recipient, id, datasource)
    @datasource = Datasource.find(id)
    str_subj      = subject
    mail(:to => recipient,
         :subject => str_subj)        
  end
  
  def confirm_reception(id, resend)

    @reception = Reception.find(id)
    @resend = resend
    m = find_or_create_mail_event("confirm_reception")
    re = Reception.find(id)

    if @reception.requested_by.blank?
      email_requester = nil
    else
      email_requester = User.find_by_full_name(@reception.requested_by).email
    end
    
    recipient = get_conditioned_addresses_for_data_types({:mail_event => m, :reception => re},email_requester)

    region = ""
    region = re.region.region_code if re.region
    purpose = re.purpose[0,4].upcase if re.purpose.size >= 4
    abbr =""
    if re.data_release
      if re.data_release.company_abbr && re.data_release.company_abbr.ms_abbr_3
        abbr = re.data_release.company_abbr.ms_abbr_3
      end
    else
      abbr = "UNKNOWN"
    end
   str_subj = "Reception notification #{re.id} #{re.core_tags.join(' ').upcase} #{purpose} #{DateTime.now.strftime('%Y%m%d')}"
   str_subj = "UPDATE: " + str_subj if resend
   
    mail( :to => recipient,
          :subject => subject_string(str_subj) )
  end

  def data_intake_request(id)
    m = find_or_create_mail_event("data_intake_request")
    re = Reception.find(id)   
    recipient = get_conditioned_addresses_for_data_types(:mail_event => m, :reception => re)
    @reception = Reception.find(id)

    regionstr = ""
    regionstr = re.region.region_code if re.region
    purpose = re.purpose[0,4].upcase if re.purpose.size >= 4
    abbr =""
    if re.data_release.company_abbr && re.data_release.company_abbr.ms_abbr_3
      abbr = re.data_release.company_abbr.ms_abbr_3
    end
   str_subj = "Data Intake Request for #{re.data_type.name} #{abbr} #{purpose} #{re.data_release.name} #{regionstr} #{DateTime.now.strftime('%Y%m%d')} #{re.referenceid}"

    mail(:to => recipient,
         :subject => subject_string(str_subj) )
  end

  def medium_request(id)
    @stock = Stock.find(id)
    m = find_or_create_mail_event("medium_request")

    if ((@stock.product_ref)&&(@stock.product_ref.to_s == ''))
      pid = 'test'
    else
      pid = @stock.product_ref
    end    
    s= CompanyAbbr.find(DataRelease.find(@stock.data_release_id).company_abbr_id).ms_abbr_3

    str_subj = "Medium Request #{@stock.id} #{pid} #{s} #{@stock.region.region_code}"
    
    mail(:to => recipients(m),
      :subject => subject_string(str_subj) )
  end
  
  def medium_result(id)
    @stock = Stock.find(id)
    m = find_or_create_mail_event("medium_result")
    
    if ((@stock.product_ref)&&(@stock.product_ref.to_s == ''))
      pid = 'test'
    else
      pid = @stock.product_ref.to_s
    end
    
    s= CompanyAbbr.find(DataRelease.find(@stock.data_release_id).company_abbr_id).ms_abbr_3
    str_subj = "Medium Result #{@stock.id} #{pid} #{s} #{@stock.region.region_code}"

    mail(:to => recipients(m),
      :subject => subject_string(str_subj) )
  end
  
  def medium_valid_result(id)
    @stock = Stock.find(id)    
    m = find_or_create_mail_event("medium_valid_result")
    
    if ((@stock.product_ref)&&(@stock.product_ref.to_s == ''))
      pid = 'test'
    else
      pid = @stock.product_ref.to_s
    end
    
    s= CompanyAbbr.find(DataRelease.find(@stock.data_release_id).company_abbr_id).ms_abbr_3
    str_subj = "Medium Approved #{@stock.id} #{pid} #{s} #{@stock.region.region_code}"

    mail(:to => recipients(m),
      :subject => subject_string(str_subj) )
  end  
  
  def reception_request(id)
    @reception = Reception.find(id)
    m = find_or_create_mail_event("reception_request")

    re = @reception
    priority_header = "normal"
    priority_header = "high" if re.priority.to_s.downcase == 'high'

    priority_str = "#{re.priority.to_s.upcase} PRIO"
    purpose = re.purpose[0,4].upcase if re.purpose.size >= 4
    str_subj = "Reception Request #{priority_str} for #{re.id} #{re.core_tags.join(' ').upcase} #{purpose} #{DateTime.now.strftime('%Y%m%d')}"

    mail(:to => recipients(m),
      'Importance' => priority_header,
      :subject => subject_string(str_subj) )
  end

  def validation_request(id)
    m = find_or_create_mail_event("validation_request")
    @reception = Reception.find(id)
    re = @reception  
    regionstr = ""
    regionstr = re.region.region_code if re.region
    purpose = re.purpose[0,4].upcase if re.purpose.size >= 4
    abbr =""
    if re.data_release.company_abbr && re.data_release.company_abbr.ms_abbr_3
      abbr = re.data_release.company_abbr.ms_abbr_3
    end
    str_subj      = "Validation Request for #{re.data_type.name} #{abbr} #{purpose} #{re.data_release.name} #{regionstr} #{DateTime.now.strftime('%Y%m%d')} #{re.referenceid}"

    mail(:to => get_conditioned_addresses_for_data_types(:mail_event => m, :reception => re),
      :subject => subject_string(str_subj) )
  end

  def review_request_executable(id)
    m = find_or_create_mail_event("review_request_executable")
    @obj = Executable.find(id)
    str_subj = "Review Request for executable #{@obj.name}, id: #{@obj.id}."
    mail(:to => recipients(m), :subject => subject_string(str_subj))
  end

  def review_request(obj,userids=nil)
    @class_name = obj.class.name
    m = find_or_create_mail_event("review_request_#{@class_name.downcase}")
    reviewer_addresses = []
    if userids
      userids.each do |user_id|
        reviewer_addresses << User.find(user_id).email
      end
    end
    # find the instance identified by id in the class identified by class_name
    @obj = obj
    @action_name = "handle"
    str_subj = "Review Request for #{@class_name} #{obj.name}, id: #{obj.id}"
    mail(:to => recipients(m,reviewer_addresses.join(',')), :subject => subject_string(str_subj))
  end

  def reject_reception(id)
    @reception = Reception.find(id)
    m = find_or_create_mail_event("reject_reception")
    
    firstn = @reception.requested_by.to_s.split(' ')[0]
    lastn =  @reception.requested_by.to_s.gsub("#{firstn}",'').gsub(' ','')
    usern = firstn + '.' + lastn
    to = User.find_by_user_name("#{usern}").email
    re = @reception
    regionstr = ""
    regionstr = re.region.region_code if re.region
    purpose = re.purpose[0,4].upcase if re.purpose.size >= 4
    abbr =""
    if re.data_release
      if re.data_release.company_abbr && re.data_release.company_abbr.ms_abbr_3
        abbr = re.data_release.company_abbr.ms_abbr_3
        dname = re.data_release.name
      end
    else
      abbr = ''
      dname = ''
    end
    str_subj = "REJECTED: Reception Request #{re.data_type.name} #{abbr} #{purpose} #{dname} #{regionstr} #{re.id.to_s}"

    mail(:to => recipients(m,to),
      :subject => subject_string(str_subj) )
  end

  def conversion_request(id)
    @conversion = Conversion.find(id)
    m = find_or_create_mail_event("conversion_request")
    supplier = ''
    release = ''
    c = @conversion
    if c.data_release_id
        supplier = CompanyAbbr.find(c.data_release.company_abbr).name
        release = DataRelease.find(c.data_release_id).name
    end
    ctype = "Conversion"
    ctype = "Patch conversion" if c.patch_request_yn
    str_subj = "#{ctype} Request: #{c.input_format} to #{c.output_format} #{c.productid} #{supplier} #{release} #{c.region.region_code}"

    mail(:to => recipients(m),
      :subject => subject_string(str_subj) )
  end

  def reject_conversion_request(id)
    @conversion = Conversion.find(id)
    m = find_or_create_mail_event("reject_conversion_request")
    to = ''
    if @conversion.request_by.to_s != ""
      firstn = @conversion.request_by.to_s.split(' ')[0]
      lastn =  @conversion.request_by.to_s.gsub("#{firstn}",'').gsub(' ','')
      usern = firstn + '.' + lastn
      to = User.find_by_user_name("#{usern}").email.to_s
    end

    str_subj = "Conversion Request: #{@conversion.id} REJECTED"

    mail(:to => recipients(m,to),
      :subject => subject_string(str_subj) )
  end

  def conversion_result(id, resend)
    @conversion = Conversion.find(id) 
    @resend = resend
    m = find_or_create_mail_event("conversion_result")
    to = ''
    if !@conversion.request_user.nil?
      to = @conversion.request_user.email
    end
    supplier = ''
    release = ''
    if @conversion.data_release_id
      supplier = CompanyAbbr.find(@conversion.data_release.company_abbr).name
      release = DataRelease.find(@conversion.data_release_id).name
    end
    ctype = "Conversion"
    ctype = "Patch conversion" if @conversion.patch_request_yn
    str_subj = "#{ctype} Result #{@conversion.id}: #{@conversion.input_format} to #{@conversion.output_format} #{@conversion.productid} #{supplier} #{release} #{@conversion.region.region_code} | Status: #{@conversion.status_str}"

    mail(:to => recipients(m,to),
      :subject => subject_string(str_subj) )
  end
 
  def datatype_conversion_result(id, resend)
    @conversion = Conversion.find(id) 
    @resend = resend
    m = find_or_create_mail_event("datatype_conversion_result")
    s= CompanyAbbr.find(DataRelease.find(@conversion.data_release_id).company_abbr_id).ms_abbr_3
    str_subj = "Data type Conversion Result: #{@conversion.output_format}_#{s}_#{@conversion.region.region_code} #{@conversion.productid}"

    mail(:to => recipients(m),
      :subject => subject_string(str_subj) )
  end

  def tpd_request(id)
    @conversion = Conversion.find(id)
    m = find_or_create_mail_event("tpd_request")
    str_subj = "TPD Request #{@conversion.region.region_code} #{@conversion.productid} #{DateTime.now.strftime('%Y%m%d')}"

    mail(:to => recipients(m),
      :subject => subject_string(str_subj) )
  end

  def tpd_result(id)
    @conversion = Conversion.find(id)
    m = find_or_create_mail_event("tpd_result")
    str_subj      = "TPD_Notification #{@conversion.region.region_code} #{@conversion.productid}"

    mail(:to => recipients(m),
      :subject => subject_string(str_subj) )
  end

  def production_order_update(id, event)
    @po = ProductionOrder.find(id)
    m = find_or_create_mail_event("#{event}")
    @event = event
    str_subj = "Production Order event for #{@po.id.to_s}/#{@po.name}"
    
    mail(:to => recipients(m),
      :subject => subject_string(str_subj) )
  end

  def notify_document_overwrite(new_user,asset_id,doc_id)
    @new_user = new_user
    @asset = Asset.find(asset_id)
    @doc = Document.find(doc_id)

    @user_match_email = ""
    @users_match_first_name = User.where(:first_name => (@asset.requested_by.to_s.split(" ")[0]))
    @users_match_first_name.each do |u|
      last_user_name = @asset.requested_by.to_s.split(" ")[1..10].to_s.gsub(" ",'').downcase
      if u.user_name.to_s.split('.')[1].to_s == last_user_name
        @user_match_email = u.email 
      end
    end

    recipient = @user_match_email.to_s
    str_subj  = "Your document #{@asset.ref_id} #{@asset.title} is overwritten"

    mail( :to => recipient,
          :subject => subject_string(str_subj) )
  end

  def po_confirm_work(id,status)
    @po = ProductionOrder.find(id)
    @status = status

    if m = find_or_create_mail_event("po_status_#{status.gsub(' ','_')}") # only members for particular status changes
      a = find_or_create_mail_event("po_confirm_work") # members for all status changes
      to_reciepents = recipients(m,recipients(a))
    else
      m = find_or_create_mail_event("po_confirm_work")
      to_reciepents = recipients(m)
    end
    
    str_subj      = "Production order: \"#{@po.name}\" #{status} "

    mail(:to => to_reciepents,
      :subject => subject_string(str_subj) )
  end

  def carin_rel_notes_request(psc_id,pse_id,current,equivalent,remarks)
    if psc_id != "0"
      @psc = Productset.find(psc_id)
    else
      @psc = Productset.new
    end
    
    if pse_id != "0"
      @pse = Productset.find(pse_id)
    else
      @pse = Productset.new
    end
    
    @current = current
    @equivalent = equivalent
    @remarks = remarks
    
    m = find_or_create_mail_event("carin_rel_notes_request")
    
    str_subj      = "Request CARiN Release Notes: #{@psc.setcode} "

    mail(:to => recipients(m),
      :subject => subject_string(str_subj) )
  end
  
  def test_request(id,resend=false)
    @resend = resend
    @test_request = TestRequest.find(id)    
    m = find_or_create_mail_event("test_request")
    recipient   = @test_request.assigned_user.email.to_s

    str_subj  = "Test Request #{@test_request.id} #{@test_request.test_type.name}"
    str_subj += " #{@test_request.product_id}" if @test_request.product_id.to_s != ""
    str_subj = "UPDATE: " + str_subj if resend
    
    mail( :to => recipient,
      :subject => subject_string(str_subj),
      :cc => recipients(m) )
  end
  
  def result_test_request(id)
    @test_request = TestRequest.find(id)    
    m = find_or_create_mail_event("result_test_request")
    recipient   = @test_request.request_user.email.to_s

    str_subj  = "Test Result #{@test_request.id} #{@test_request.test_type.name}"
    str_subj += " #{@test_request.product_id}" if @test_request.product_id.to_s != ""
    
    mail( :to => recipient,
      :subject => subject_string(str_subj),
      :cc => recipients(m) )
  end
  
  def process_data_status_change(process_data)
    @process_data = process_data
    m = find_or_create_mail_event("process_data_status_change")
    str_subj = "Process Data #{@process_data.id.to_s.rjust(4,"0")} \"#{@process_data.name}\" changed to #{@process_data.status_string}"
    
    mail(:to => recipients(m),
      :subject => subject_string(str_subj) )
  end
  
  def production_orderline_status_change(production_orderline)
    @production_orderline = production_orderline
    m = find_or_create_mail_event("production_orderline_status_change")
    str_subj = "Production Orderline #{@production_orderline.id.to_s.rjust(4,"0")} \"#{@production_orderline.product.name if @production_orderline.product}\" of production order #{@production_orderline.production_order ? @production_orderline.production_order.name : '-'} changed to #{@production_orderline.bp_name}"
    
    mail(:to => recipients(m),
      :subject => subject_string(str_subj) )
  end
  
  def product_release_mail(outbound_order,to_member,from_user,rn, rn_id)
    @outbound_order = outbound_order
    recipient = Rails.env == "production" ? to_member : from_user
    return_to = ConfigParameter.get("delivery_team_email_string").split('<')[1].gsub('>','')

    if rn
      # release notes must also be mailed
      if rn_id
        # include the release notes as attachement to the mail
        @send_release_notes_separately = false
        document = ProductReleaseNote.find_by_id(rn_id).document
        attachments[document.filename] = document.binary_data
      else
        # we don't have the release notes available, include a note in the mail that they will be sent separately
        @send_release_notes_separately = true
      end
    else
      # release notes are not part of this release mail (but available in the download for instance)
      @send_release_notes_separately = false
    end
    mail(:to => return_to, :bcc => recipient, :from => ConfigParameter.get("delivery_team_email_string"), :subject =>outbound_order.release_mail_subject) do |format|
      format.html { render :layout => 'mail_external' }
    end
  end

  def packed_product_release_mail(outbound_order,test = false)
    @outbound_order = outbound_order
    @sender = User.find_by_full_name(@outbound_order.send_by)
    return_to = ConfigParameter.get("delivery_team_email_string").split('<')[1].gsub('>','')

    recipient = @sender.email
    if Rails.env == "production" && @outbound_order.distribution_list && !@outbound_order.distribution_list.electronic_delivery_email_list.blank? && test == false
      recipient = @outbound_order.distribution_list.electronic_delivery_email_list
    end

    mail(:to => return_to, :bcc => recipient, :from => ConfigParameter.get("delivery_team_email_string"), :subject =>outbound_order.release_mail_subject,:return_path => return_to) do |format|
      format.html { render :layout => false }
    end   
  end

  def shipment_request(pol_id)
    @production_orderlines = ProductionOrderline.find(pol_id)
    m = find_or_create_mail_event("shipment_request")
    
    if pol_id.size > 1
      str_subj = "Shipment Request: #{@production_orderlines.first.production_order.name}"
    else
      str_subj = "Shipment Request: #{@production_orderlines.first.product_id} #{ @production_orderlines.first.production_order.ordertype.product_category.abbreviation}"
    end

    mail(:to => recipients(m),
      :subject => subject_string(str_subj) )
  end

  def expiry_notification(production_order)
    @production_order = production_order
    to = []
    # notify all admin project members in case of the default project, and all project members for all other projects
    production_order.project.users.each do |user|
      user.current_project_id = production_order.project_id
      if production_order.project_id != Project.default.id || user.has_role('admin')
        # skip notification of users without an email address
        to << user.email unless user.email.blank?
      end
    end
    ### TODO: Production has to many members and admins (43) at the moment. So for now hardcode the recipients, as it is not clear
    ### yet whether the automatic expire is indeed wanted for production and how the recipients should be determined
    to = %w(robert.deruyter@mapscape.eu martin.vandersommen@mapscape.eu dave.rijken@mapscape.eu maurice.hesselmans@mapscape.eu paul.willems@mapscape.eu) if production_order.project_id == Project.default.id

    subject = "Expiry notification for production order #{production_order.name}"
    # if a project has no members with an email address at all, there is nobody to be notified, so just skip the notification
    unless to.empty?
      mail(:to => to.uniq.join(','), :subject => subject_string(subject))
    end
  end

  def conversion_job_status_change(conversion_job_obj)
    @conversion_job = conversion_job_obj
    m = find_or_create_mail_event("conversion_job_status_change")
    to = ''
    to = User.find_by_full_name(@conversion_job.conversion.request_by).email if @conversion_job.conversion
    str_subj = "Conversion Job #{conversion_job_obj.id} #{@conversion_job.status_str.humanize} #{@conversion_job.run_server} #{@conversion_job.conversion.input_format if @conversion_job.conversion} to #{@conversion_job.conversion.output_format if @conversion_job.conversion} #{@conversion_job.tagmodel['coverage'].join(',').upcase}"

    mail(:to => recipients(m,to),
      :subject => subject_string(str_subj) )
  end

   def conversion_job_file_move_failed(conversion_job_id)
     @conversion_job = ConversionJob.find(conversion_job_id)
     m = find_or_create_mail_event("conversion_job_file_move_failed")
     to = User.find_by_full_name(@conversion_job.conversion.request_by).email
     str_subj = "File move failed: Conversion Job #{conversion_job_id} #{@conversion_job.status_str.humanize} #{@conversion_job.run_server} #{@conversion_job.conversion.input_format} to #{@conversion_job.conversion.output_format} #{@conversion_job.tagmodel['coverage'].join(',').upcase}"

     mail(:to => recipients(m,to),
          :subject => subject_string(str_subj) )
   end

   def validation_job_status_change(validation_job)
    m = find_or_create_mail_event("validation_job_status_change")
    to = User.find_by_full_name(validation_job.conversion.request_by).email
    str_subj = "Validation Job #{validation_job.id} #{validation_job.status_str.humanize} #{validation_job.run_server} #{validation_job.conversion_database.filename}"

    mail(:to => recipients(m,to),
      :subject => subject_string(str_subj) )
  end

  def test_request_job_status_change(test_request_job)
    @test_request_job = test_request_job
    m = find_or_create_mail_event("test_request_job_status_change")
    to = User.find(test_request_job.test_request.requested_by_user_id).email
    str_subj = "Test Request Job #{test_request_job.id} #{test_request_job.status_str.humanize} #{test_request_job.run_server}"

    mail(:to => recipients(m,to),
      :subject => subject_string(str_subj) )
  end

  def packing_job_status_change(packing_job_id)
    @packing_job = PackingJob.find(packing_job_id)
    m = find_or_create_mail_event("packing_job_status_change")
    to = User.find_by_full_name(@packing_job.outbound_order.send_by).email
    str_subj = "Packing Job #{packing_job_id} #{@packing_job.packing.filename} #{@packing_job.status_str.humanize} #{@packing_job.run_server} "

    mail(:to => recipients(m,to),
      :subject => subject_string(str_subj) )
  end

   def job_asynchronous_move_terminated(job_id, destination)
     @job = Job.find(job_id)
     if @job.class == ConversionJob || @job.class == ValidationJob
       full_name = @job.conversion.request_by
     elsif @job.class == PackingJob
       full_name = @job.outbound_order.send_by
     elsif @job.class == TestRequestJob
       full_name = @job.test_request.requested_by_user_id
     else
       full_name = nil
     end
     m = find_or_create_mail_event("job_asynchronous_move_terminated")
     to = User.find_by_full_name(full_name).email
     str_subj = "#{@job.class.model_name.human} #{job_id} move to '#{destination}' terminated"
     mail(:to => recipients(m, to),
          :subject => subject_string(str_subj))
   end

  def product_design_status_change(product_design_id)
    @product_design = ProductDesign.find(product_design_id)
    m = find_or_create_mail_event("product_design_status_change")
    str_subj = "Product design #{@product_design.name} to status #{@product_design.status.humanize}"
    to = "jos.decallafon@mapscape.eu"
    mail(:to => recipients(m,to),
      :subject => subject_string(str_subj) )
  end

  def process_data_review_request(process_data,current_user,reviewer_user)
    @process_data = process_data
    @request_user = current_user
    @review_user = reviewer_user
    str_subj = "Review Request Process data #{@process_data.id.to_s.rjust(4,"0")} #{@process_data.name}"
    to =  @review_user.email
    cc = @request_user.email
    mail(:to => to, :cc => cc, :subject => subject_string(str_subj) )
  end

  def process_data_review_result(process_data,current_user,reviewer_requester)
    @process_data = process_data
    @request_user = reviewer_requester
    @review_submit_user = current_user
    m = find_or_create_mail_event("process_data_review_result")
    str_subj = "Rework needed for process data #{@process_data.id.to_s.rjust(4,"0")} #{@process_data.name}"
    to =  @request_user.email
    cc = @review_submit_user.email
    mail(:to => to, :cc => recipients(m,cc), :subject => subject_string(str_subj) )
  end

  def conversion_database_validation_failed(validation_job_id)
    @validation_job = ValidationJob.find(validation_job_id)
    m = find_or_create_mail_event("conversion_database_validation_failed")
    str_subj = "Validation Job failed for database #{@validation_job.conversion_database.id.to_s} #{@validation_job.conversion_database.tags.join(' ')}"
    to = "johan.hendrikx@mapscape.eu"
    mail(:to => recipients(m,to),
      :subject => subject_string(str_subj) )
  end

  def internal_server_error(subject,details,trace,user=nil)
    m = find_or_create_mail_event("internal_server_error")
    @str_subj = subject
    @details = details
    @trace = trace
    @user_name = 'unknown'
    if user
      @user_name = user.full_name
    end

    mail(:to => recipients(m),
      :subject => subject_string("Internal server error") )
  end

  def tool_spec_synchronization_error(e)
    m = find_or_create_mail_event("tool_spec_synchronization_error")
    str_subj = "Tool spec synchronization error"
    details = e.message
    trace = e.backtrace
    mail(:to => recipients(m), :subject => subject_string(str_subj))
  end

  def product_edit_during_active_order(product_obj,order_obj,current_user)
    m = find_or_create_mail_event("product_edit_during_active_order")
    @product = product_obj
    @production_order = order_obj
    @user = current_user
    str_subj = "Forced edit notification of product #{@product.volumeid}"
    to_user = User.find_by_full_name(@production_order.requestor)
    if to_user
      to = to_user.email
    end
    mail(:to => recipients(m,to.to_s), :subject => subject_string(str_subj) )
  end

  def data_set_review_request(data_set,current_user,reviewer_user)
    @data_set = data_set
    @request_user = current_user
    @review_user = reviewer_user
    str_subj = "Data set review request #{@data_set.label(true)}"
    to =  @review_user.email
    cc = @request_user.email
    mail(:to => to, :cc => cc, :subject => subject_string(str_subj) )
  end

  def data_set_review_result(data_set,request_user,review_user,approved=true)
    m = find_or_create_mail_event("data_set_review_result")
    @request_user = request_user
    @review_user  = review_user
    if approved
      @str_subj_result = "approved"
    else
      @str_subj_result = "disapproved"
    end
    @data_set = data_set
    str_subj = "Data set #{@data_set.label(true)} #{@str_subj_result}"
    to =  DataSet.reviewers_email_addresses
    #mail(:to => to, :cc => m,:subject => subject_string(str_subj) )
    mail(:to => to, :subject => subject_string(str_subj) )
  end
  
end
