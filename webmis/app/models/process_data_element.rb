class ProcessDataElement < ActiveRecord::Base
  belongs_to :data_set
  belongs_to :process_data_specification
  belongs_to :reception
  has_many :process_data_lines
  has_many :conversion_job_process_data_elements
  has_many :conversion_jobs, :through => :conversion_job_process_data_elements
  has_many :conversion_databases, :through => :conversion_jobs
  has_many :conversions, :through => :conversion_jobs

  scope :active, -> { joins(:data_set).where("(process_data_elements.removed_yn = 0 or process_data_elements.removed_yn is null) and data_sets.status < #{DataSet::STATUS_REMOVED}") }

  def available?(verify_directory=nil)
    if ( !self.data_set.nil? && self.data_set.status < DataSet::STATUS_PRODUCTION && !self.removed_yn )
      if verify_directory
        return self.full_directory == verify_directory
      else
        return true
      end
    else
      return false
    end
  end

  def generate_directory(store = false, create = false)
    # sanity checks
    raise 'Element does not contain a specification!' unless self.process_data_specification
    raise 'Element does not belong to a data set.' unless self.data_set

    # Element
    specification_dir = self.process_data_specification.directory

    # Split by value
    split_by_dir = self.split_by_value.to_s.parameterize

    # Finalize
    result = File.join(self.data_set.generate_directory,specification_dir,split_by_dir).downcase.gsub('//','/')

    if create
      ssh_cmd = "mkdir -p #{File.join(result)}"
      self.data_set.ssh_localhost(ssh_cmd)
      self.data_set.create_directory_structure
    end

    if store
      self.directory = result
      self.save
    end

    result
  end

  def full_directory
    if self.directory.to_s.match(self.data_set.generate_directory)
      return File.join(self.directory.to_s).gsub('//','/').strip
    else
      # fallback old elements from process data age
      stripped = self.data_set.generate_directory.gsub(self.directory.to_s.split('/')[0..4].uniq.join('/'),'')
      return File.join(stripped,self.directory.to_s).gsub('//','/').strip
    end
  end

  def remove_and_sync(user_obj=nil)
    username = "unknown user"
    username = user_obj.full_name if user_obj
    spec_dir = File.join(self.data_set.generate_directory,self.process_data_specification.directory.to_s).gsub('//','/').strip

    cwd_path = self.full_directory
    if self.split_by_value
      # when this dir is empty after cleanup, clean up this on this level
      split_by_base = self.full_directory.gsub(self.split_by_value,'')
      if Dir[File.join(split_by_base,'*')].delete_if {|f|f.to_s.match(/\.reception/)}.delete_if {|f|f.to_s.match(cwd_path)}.empty?
       cwd_path = split_by_base
      end
    end

    if Dir[File.join(spec_dir,'*')].delete_if {|f|f.to_s.match(/\__trash/)}.delete_if {|f|f.to_s.match(spec_dir)}.empty?
      # no files in specification dir after cleanup, so clean up on this level
      cwd_path = spec_dir
    end

    new_path = File.join(self.data_set.generate_directory,'__trash')
    ssh_cmd = "mkdir -p #{new_path};mv #{cwd_path} #{new_path}/."
    ssh = self.data_set.ssh_localhost(ssh_cmd)

    self.transaction do
      if self.removed_yn == false
        self.removed_yn = true
        self.remark = "#{self.remark} Removed record and synced files by #{username}."
        self.save!
        log = ProcessDataLine.new
        log.data_set_id = self.data_set.id if self.data_set
        log.log_forward = "Deleted #{self.process_data_specification.name} from reception #{self.reception_id}."
        log.user_id = user_obj.id if user_obj
        log.process_data_element_id = self.id
        log.save!
      end
      return true if !File.directory?(cwd_path) # files already removed
      self.data_set.ssh_localhost(ssh_cmd)
      if ssh.blank?
        return true
      else
        raise "Sync failed on file system! ERROR => #{ssh}"
      end
    end
  end

  def in_use?
    jobs = self.conversion_jobs.outstanding
    if jobs.empty?
      return false
    else
      return true
    end
  end

  def databases_with_former_process_data_version(overrule_version=nil)
    if !overrule_version
      version = self.data_set.version
    else
      version = overrule_version
    end
    databases = self.conversion_databases.where("conversion_job_process_data_elements.data_set_version < #{version}").available
    return databases
  end

  def resulting_conversion_databases
    databases_with_former_process_data_version(self.data_set.version + 1)
  end

  def release_note_files
    files = {}
    if self.process_data_specification.do_not_process_yn
      fd = FileSystemUtil.directory_hash(self.full_directory)
        if fd[:children] && fd[:file_type] == "directory"
          fd[:children].each do |f|
            files[File.join(fd[:label],f[:label])] = f[:label]
          end
        end
    end
    return files
  end

  def log_status(args = {})
    args[:level] ||= 'ELEMENT'
    args[:user] ||= 'unknown'
    args[:version] ||= self.data_set.version
    args[:remarks] ||= self.beautify_changes
    args[:status] ||= self.data_set.status
    user_obj = nil
    if args[:user] != 'unknown'
      user_obj = User.find_by_user_name(args[:user])
    end
    ProcessDataLine.transaction do
      log = ProcessDataLine.new
      log.data_set_id = self.data_set.id if self.data_set
      log.log_forward = args[:remarks].to_s
      log.user_id = user_obj.id if user_obj
      log.process_data_element_id = self.id
      log.save!
      self.data_set.log_status(args)
    end
  end

  def beautify_changes
    msg = []
    self.changes.each do |a,v|
      smsg = ""
      case a
      when "reception_id"
        if v[0].nil?
          smsg += "Added reception Id #{v[1]}"
        else
          smsg += "Changed reception Id #{v[0]} to #{v[1]}"
        end
      when "process_data_specification_id"
        if v[0].nil?
          pdspec_name = ''
          r = ProcessDataSpecification.find(v[1])
          pdspec_name = r.name if r
          smsg += "Added process data specification '#{pdspec_name}'"
        else
          pdspec_name_a = ''
          pdspec_name_b = ''
          ra = ProcessDataSpecification.find(v[0])
          rb = ProcessDataSpecification.find(v[1])
          pdspec_name_a = ra.name if ra
          pdspec_name_b = rb.name if rb
          smsg += "Changed process data specification '#{pdspec_name_a}' to '#{pdspec_name_b}'"
        end
      when "removed_yn"
        if v[1]
          smsg += "Removed"
        end
      when "split_by_value"
        if v[0].nil?
          smsg += "Added split-by value '#{v[1]}'"
        else
          smsg += "Changed split-by value '#{v[0]}' to '#{v[1]}'"
        end
      when "directory"
        if v[0].nil?
          smsg += "Added directory '#{v[1]}'"
        else
          smsg += "Changed directory '#{v[0]}' to '#{v[1]}'"
        end
      end
      msg << smsg
    end
    return "#{self.id} #{self.directory.to_s}\n" + msg.join("\n")
  end

end
