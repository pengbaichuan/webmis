class ProcessDataEvent < ActiveRecord::Base
  has_many :process_data_sub_events
  after_initialize :defaults

  validates :name, :presence => true
  validates :status, :presence => true

  STATUS_ENTRY    =   0
  STATUS_ACTIVE   =  10
  STATUS_DISABLED =  90
  STATUS_REMOVED, STATUS_OBSOLETE  = 100
  
  @@statusarray      = Array.new
  @@statusarray[STATUS_ENTRY]     = 'Entry'
  @@statusarray[STATUS_ACTIVE]    = 'Active'
  @@statusarray[STATUS_DISABLED]  = 'Disabled'
  @@statusarray[STATUS_REMOVED]   = 'Removed'

  scope :active, -> {where('status = 10')}

  def defaults
    self.status = STATUS_ACTIVE if new_record?
  end
   
  def self.status_hash
    s_hash = Hash.new
    @@statusarray.each do |s|
      if !s.nil?
        s_hash[s] = @@statusarray.index(s)
      end
    end
    return s_hash.sort_by { |k,v| v }
  end

  def status_string
    @@statusarray[self.status] 
  end

  def removal_allowed?
    self.status == STATUS_DISABLED
  end

  def activation_allowed?
    self.status == STATUS_DISABLED
  end

  def deactivation_allowed?
    ( self.status == STATUS_ACTIVE || self.status == STATUS_ENTRY )
  end

  def restore_allowed?
    self.status == STATUS_REMOVED
  end

end
