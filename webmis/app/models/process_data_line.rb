class ProcessDataLine < ActiveRecord::Base
#Process data log lines
  belongs_to :data_set
  belongs_to :process_data_element
  belongs_to :process_data_sub_event
  belongs_to :reception
  has_one :process_data_event,:through => :process_data_sub_event
  belongs_to :user
  belongs_to :document
  
  validates :relation_line_id, :presence => true, :if => :type_relation?
  validates :process_data_element_id, :presence => true
  
  
  @@relationarray      = Array.new
  @@relationarray      =['Revert','Adaption']
  
  def self.line_relations
    return @@relationarray
  end
  
  def related_to_lines?
    @relations = ProcessDataLine.where(:relation_line_id => self.id).order(:id)
  end
  
  def update_version_data_set
    if self.data_set && self.changed?
      ds = self.data_set
      ds.version = ds.version.to_i + 1 if ds.status != DataSet::STATUS_PRODUCTION
      ds.status = DataSet::STATUS_DRAFT
      ds.save
      self.change_version = ds.version
    end
  end
  
end
