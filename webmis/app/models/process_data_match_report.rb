# To change this template, choose Tools | Templates
# and open the template in the editor.

class ProcessDataMatchReport
  attr_accessor :matches
  attr_accessor :complete

  class MatchResult
    attr_accessor :package_id
    attr_accessor :type
    attr_accessor :filling
    attr_accessor :found
    attr_accessor :include
    attr_accessor :data_sets
    attr_accessor :message
  end
  
  def initialize
    @matches = []
    @complete = true
  end

  def add_matchresult(package_id, type, filling, data_sets=nil, message=nil)
    mr = MatchResult.new
    mr.type = type
    mr.filling = filling
    mr.package_id = package_id
    if data_sets
      mr.found = true
      mr.data_sets = data_sets
    else
      mr.found = false
      @complete = false
    end
    mr.message = message
    @matches << mr
  end
  
end
