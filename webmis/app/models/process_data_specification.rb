class ProcessDataSpecification < ActiveRecord::Base
  has_many :process_data_elements
  belongs_to :data_type

  after_initialize :default_values
  
  validates :name, :presence => true, :uniqueness => true
  validates :directory, :presence => true

  def default_values
    self.include_in_all_regions ||= false
    self.active_yn ||= true
    self.directory_level_yn ||= false
    self.do_not_process_yn ||= false
  end

end
