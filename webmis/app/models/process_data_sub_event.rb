class ProcessDataSubEvent < ActiveRecord::Base
  belongs_to :process_data_event
  validates :name, :presence => true
  validates :process_data_event_id, :presence => true
  validates :status, :presence => true
  
  after_initialize :defaults

  STATUS_ENTRY    =   0
  STATUS_ACTIVE   =  10
  STATUS_DISABLED =  90
  STATUS_REMOVED, STATUS_OBSOLETE  = 100
  
  def defaults
    self.status = STATUS_ACTIVE if new_record?
  end
  
  @@statusarray      = Array.new
  @@statusarray[0]   = 'Entry'
  @@statusarray[10]  = 'Active'
  @@statusarray[90]  = 'Disabled'
  @@statusarray[100] = 'Removed'
  
  def self.status_hash
    s_hash = Hash.new
    @@statusarray.each do |s|
      if !s.nil?
        s_hash[s] = @@statusarray.index(s)
      end
    end
    return s_hash.sort_by { |k,v| v }
  end

  def status_string
    @@statusarray[self.status] 
  end

  def removal_allowed?
    self.status == STATUS_DISABLED
  end

  def activation_allowed?
    self.status == STATUS_DISABLED
  end

  def deactivation_allowed?
    ( self.status == STATUS_ACTIVE || self.status == STATUS_ENTRY )
  end

  def restore_allowed?
    self.status == STATUS_REMOVED
  end
  
end
