class ProcessTransition < ActiveRecord::Base
  
 belongs_to :ordertype
 belongs_to :from_process, :class_name => "BusinessProcess", :foreign_key => "from_bp"
 belongs_to :to_process, :class_name => "BusinessProcess", :foreign_key => "to_bp"
 belongs_to :cascade_to_process, :class_name => "BusinessProcess", :foreign_key => "cascade_to_bp"
 
 accepts_nested_attributes_for :from_process
 accepts_nested_attributes_for :to_process
 accepts_nested_attributes_for :cascade_to_process

 def self.can_complete_work?(from_bp_name,ordertype_id)
   from_bp = BusinessProcess.find_by_name("#{from_bp_name}")
   if from_bp && ordertype_id && (ProcessTransition.where("from_bp = #{from_bp.id} and from_level = 'PRODUCT' and ordertype_id = #{ordertype_id}").size > 0) && !from_bp.system_support_yn
     return true
   else
     return false
   end
 end

 def self.can_complete_action_link?(bp_name)
   bp = BusinessProcess.find_by_name("#{bp_name}")
   if !bp.system_support_yn
     return true
   else
     return false
   end
 end

 def self.can_cancel_work?(bp_name)
   bp = BusinessProcess.find_by_name("#{bp_name}")
   if bp && bp.name != "CANCELLED" && bp.name != "COMPLETE"
     return true
   else
     return false
   end
 end

 def self.can_reset_work?(bp_name)
   self.can_cancel_work?(bp_name)
 end

 def self.can_reactivate_work?(bp_name)
   bp = BusinessProcess.find_by_name("#{bp_name}")
   if bp && bp.name == "CANCELLED"
     return true
   else
     return false
   end
 end

end
