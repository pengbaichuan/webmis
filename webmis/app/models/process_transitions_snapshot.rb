class ProcessTransitionsSnapshot
 
 
  attr_accessor :transitions

  def initialize(transitions=nil)
    @transitions = transitions
  end

  def init_process_transition(hash)
    #hash[:from_process] = BusinessProcess.new(hash[:from_process]) if hash[:from_process]
    #hash[:to_process] = BusinessProcess.new(hash[:to_process]) if hash[:to_process]
    #hash[:cascade_to_process] = BusinessProcess.new(hash[:cascase_to_process]) if hash[:cascade_to_process]
    fp = nil
    tp = nil
    cp = nil
    fp =  BusinessProcess.new(hash[:from_process]) if hash[:from_process]
    tp = BusinessProcess.new(hash[:to_process]) if hash[:to_process]
    cp = BusinessProcess.new(hash[:cascade_toprocess]) if hash[:cascade_to_process]
    hash.except!("from_process")
    hash.except!("to_process")
    hash.except!("cascade_to_process")
    hash.except!(:from_process)
    hash.except!(:to_process)
    hash.except!(:cascade_to_process)
    
    pt =  ProcessTransition.new(hash)
    pt.from_process =  fp if fp
    pt.to_process = tp if tp
    pt.cascade_to_process = cp if cp
    pt.from_bp  = fp.id if fp
    pt.to_bp  = tp.id if tp
    pt.cascade_to_bp  = cp.id if cp
    pt.id = hash["id"]
    pt.id = hash[:id] if !pt.id

    return pt
  end

  def process_transitions
    self
  end

  def process_transition_objects
    object_transitions = []
    if @transitions 
      @transitions.each do |transition|
        transition_clone = transition.dup
        if !transition_clone.instance_of?(ProcessTransition)
          object_transitions << init_process_transition(transition_clone)
        else
          object_transitions << transition_clone
        end
      end
    else
      raise "No transitions cached"
    end
    return object_transitions
  end

  def to_json
    @transitions.to_json
  end

  def self.from_json(json)
    parsed = JSON.parse(json)
    pts = ProcessTransitionsSnapshot.new(parsed)
    return pts
  end

  def where(filter)

    filtered_transitions = []
    
    self.process_transition_objects.each do |transition|
      match = true
      filter.each do |k,v|
        if transition.send(k.to_s) != v
          match = false
        end
      end
      filtered_transitions << transition if match
    end
    return ProcessTransitionsSnapshot.new(filtered_transitions)
  end

  def order(sorter)
    sorted =  self.process_transition_objects.sort{|x,y|x.send(sorter.to_s) <=> y.send(sorter.to_s)}
    return ProcessTransitionsSnapshot.new(sorted)
  end

  def find
    
  end

  def first
    return self.process_transition_objects[0]
  end

  def last
    return  self.process_transition_objects[-1]
  end

  def all
    return self.process_transition_objects
  end

  def method_missing(m, *args, &block)
    return self.process_transition_objects.send m, *args, &block
  end
  
end
