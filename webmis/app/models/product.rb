class Product < ActiveRecord::Base
  # acts_as_versioned
  version_fu
  
  attr_accessor :link_productset_id
  attr_accessor :link_product_id
  attr_accessor :inc_product_id
  
  has_many :includedproducts
  has_many :productsets, :through => :includedproducts
  has_many :product_data_set_infos
  has_many :includeddocuments
  has_many :document_versions, :through => :includeddocuments
  has_many :high_level_boms
  has_many  :customer_contents, :dependent => :destroy
  belongs_to :assembly_header, :foreign_key => "volumeid", :primary_key => "product_id"
  belongs_to :coverage,   :class_name => "Coverage" ,:foreign_key => "primary_coverage_id", :dependent => :destroy
  belongs_to :secondary_coverage, :class_name => "Coverage", :foreign_key => "secondary_coverage_id", :dependent => :destroy
  belongs_to :poi_mapping_layout
  belongs_to :product_content_specification, :foreign_key => "update_region_mapping_id"
  belongs_to :product_line
  belongs_to :parent, :class_name => "Product", :foreign_key => "parent_id"
  belongs_to :predecessor, :class_name => "Product", :foreign_key => "predecessor_id"
  has_many   :children,  :class_name => "Product", :foreign_key => "parent_id"
  validates :name, :presence => true
  validates :volumeid, :uniqueness => {:message => "(Product ID) is already in use."}, :presence => {:message => "(Product ID) can't be blank."}
  belongs_to :product_design

  scope :approved, -> {where('status = 20')}
  @@statusarray      = Array.new
  @@statusarray[0]   = 'Entry'
  @@statusarray[5]   = 'Cloned'
  @@statusarray[10]  = 'Draft'
  @@statusarray[20]  = 'Approved'
  @@statusarray[999] = 'Removed'  
  
  def self.statusarray
    return @@statusarray
  end
  
  def self.statushash
    s_hash = Hash.new
    @@statusarray.each do |s|
      if !s.nil?
        s_hash[s] = @@statusarray.index(s)
      end
    end
    return s_hash.sort_by { |k,v| v }
  end

  def statusstr
    status_string =  @@statusarray.values_at(0)[0]
    if status
      status_string =  @@statusarray.values_at(status)[0]
    end
    return status_string
  end
  alias_method :status_str, :statusstr
 
  def self.find_by_real_volumeid(volumeid)
    scan = volumeid.scan(/(\A.*)\.\d{1,2}\z/) #MP104.8060.3 => MP104.8060
    if scan.size > 0
      real = scan[0][0]
      p = self.find_by_volumeid(real)
    end
    if !p
      p = self.find_by_volumeid(volumeid)
    end
    return p
  end

  def documents
    document_versions
  end

  def save
    d = 2
    d = delta(0).size if self.id
    if d >= 1
      super
    else
      return true
    end
  end

  def before_update
    # self.dateeffective = Time.now

    d = delta(0)
    if d.size >= 1
      self.last_change = "Product was changed-"
      self.version = self.version + 1
      if productsets && productsets.size > 0
        productsets.each do |pset|
          pset.setchanges = "Product #{volumeid} was changed"
          pset.save
        end
      end
    else
      self.last_change = "Product was changed-"
    end
    
    if inc_product_id && inc_product_id.size > 0
      ip = Includedproduct.find(inc_product_id)
      ip.link_product_id = link_product_id
      ip.save
    end
    
    
  end

  def after_create
    if link_productset_id && link_productset_id.size > 0
      i= Includedproduct.new
      i.product_id = id
      i.productset_id = link_productset_id
      i.link_product_id = link_product_id
      i.save
      i.productset.setchanges =  "#{volumeid} was added to productset\n"
      i.productset.majorupdate = "1"
      i.productset.save
    end
  end

  def hwncoverage_br
    hwncoverage.gsub("\n","<br/>")
  end

  def to_label
    return volumeid.to_s + " " + name.to_s
  end
  
  def self.to_dropdown
    return Product.where(:removed_yn => false).order('volumeid DESC').collect {|p|[p.volumeid.to_s + '  - ' + p.name,p.id]}    
  end

  def parent_part(set_id)
    i = Includedproduct.where("productset_id = #{set_id} and product_id = #{id}").first
    if i
      return i.link_product_id
    end
  end

  def components(set_id)
    ic = Includedproduct.where("productset_id = #{set_id} and product_id = #{id}").first
    i = Includedproduct.where("productset_id = #{set_id} and link_product_id = #{ic.id}")
    components = []
    i.each do |ip|
      components << ip.product
    end
    return components
  end

  def main_dataset
    ProductDataSetInfo.where("product_id = #{self.id} and major_dataset=1").first
  end
  
  def self.attributes_to_ignore_when_comparing
    [:id, :created_at, :updated_at,:lock_version,:yaml,:version,:major,:minor,:setchanges,:import_disabled]
  end

  def wdif?(other)
    self. attributes.except(*self.class.attributes_to_ignore_when_comparing.map(&:to_s)) ==
      other.attributes.except(*self.class.attributes_to_ignore_when_comparing.map(&:to_s))
  end  

  def delta(count=1)
    if count > 0
      ps1 =  ProductVersion.where("product_id = #{id} and version = #{version}").first
    else
      # compare current instance - assume version number not updated - object dirty (uncommited changes)
      ps1 =  ProductVersion.new(self.attributes)
    end
    
    ps2 = nil
    count = 0
    if ps1.version.to_i - count <= 0
      ps2 =  ProductVersion.where(:product_id => id, :version => 1).last
    else
      ps2 =  ProductVersion.where(:product_id => id, :version => version-count).last
      if ps2.nil?
        ps2 =  ProductVersion.where(:product_id => id, :version => 1).last
      end
    end

    ProductVersion.diff :exclude =>['lock_version','id', 'yaml','created_at','updated_at','version','major','minor','setchanges']

    psdiff = {}
    psdiff = ps1.diff(ps2) if !ps2.nil?
    return psdiff
  end

  def amount_multiple_product_usage(product_id)
    return Includedproduct.where(product_id:product_id).size
  end

  def get_differences_with(p_old)
    Product.diff :exclude =>[ 'id', 'yaml','created_at','updated_at','version','major','minor','setchanges']
    return self.diff(p_old)
  end

  def add_text(pdf,label,field,delta)
    pdf.text((label + ":").ljust(40).rjust(1) + @ps.send(field).to_s.strip).to_s.ljust(60)
  end

  def pdf
    pdf = PDF::TechBook.new(:orientation => :landscape, :paper => [29,40])
    pdf.add_image_from_file("public/webmis/msbackgroundwhite2.png",0,0,1150,850)
    @ps = self


    delta = @ps.delta
    pdf.text " "
    pdf.font_size = 20
    pdf.text  "Product details for "  + @ps.volumeid.to_s
    pdf.text " "
    pdf.font_size = 12

    # #pdf.select_font("Courier") #pdf.select_font("Times-Roman")
    
    pdf.select_font("Courier")
    add_text(pdf , "Product","volumeid",@ps)
    add_text(pdf , "Name","name",@ps)
    add_text(pdf , "Coverage","detailedcoverage",@ps)
    add_text(pdf , "Conversion settings","conversion_settings",delta)
    
    
    pdf.text  "Version:" + @ps.version.to_s
    pdf.text " "
    pdf.font_size = 20
    pdf.text "Used datasets for this product"
    pdf.font_size = 12
    pdf.text " "
    
    pdf.select_font("Times-Roman")
    table2 = PDF::SimpleTable.new
    table2.header_gap = 15
    table2.font_size = 10
    table2.width = 1050
    prods = [self.attributes]


    product_data_set_infos.each do |info|
      hash  = info.attributes
      hash["remarks_dataset"] = hash["remarks"]
      hash.delete("remarks")
      hash["datasetname"] = hash["name"]
      hash["datasetsupplier"] = info.company_abbr.ms_abbr_3
      hash.delete("name")
      prods <<  hash
    end


    table2.data = prods
    table2.column_order  = ["datasetname","datasetsupplier","data_release","area_abbr","data_info","remarks_dataset"]

    table2.columns["datasetname"] = PDF::SimpleTable::Column.new("datasetname")
    table2.columns["datasetsupplier"] = PDF::SimpleTable::Column.new("datasetsupplier")
    table2.columns["data_release"] = PDF::SimpleTable::Column.new("data_release")
    table2.columns["area_abbr"] = PDF::SimpleTable::Column.new("area_abbr")
    table2.columns["data_info"] = PDF::SimpleTable::Column.new("data_info")
    table2.columns["remarks_dataset"] = PDF::SimpleTable::Column.new("remarks_dataset")

    table2.columns["datasetname"].heading = "Dataset name"
    table2.columns["datasetsupplier"].heading = "Dataset supplier"
    table2.columns["data_release"].heading = "Release"
    table2.columns["area_abbr"].heading = "Area"
    table2.columns["data_info"].heading = "Dataset Info"
    table2.columns["remarks_dataset"].heading = "Exception/remarks"
    table2.render_on(pdf)
    return pdf

  end

  def databases
    dbs = ConversionDatabase.where(["product_id like '#{self.volumeid}%%'"]).collect {|c|[c.filename]}
    dbs = ConversionDatabase.where(["product_id like '#{self.volumeid}%%'"]).collect {|c|[c.path_and_file]} if !dbs
    dbs = Conversion.where(["productid like '#{self.volumeid}%%'"]).collect {|c|[c.database_name]} if !dbs
    dbs = Conversion.where(["productid like '#{self.volumeid}%%'"]).collect {|c|[c.result_files]} if !dbs
  
    if dbs
      return dbs
    else
      return nil
    end
  
  end

  def in_multiple_productsets?
    @ip = Includedproduct.where(product_id:self.id)
    if @ip && @ip.size > 1
      return true
    else
      return false
    end
  end

  def expr_update_region_tags
    block = self.remarks.scan(/Update\sregions:(.*$)/im)
    if block.size > 0
      selection = block[0].join.scan(/(\w*:)/).collect{|r|r.join.gsub(':','')} << ''
    else
      selection = []
    end
    return selection.sort
  end  

  def design
    self.product_design
  end

  def primary_coverage_regions
    @coverage = []
    if !self.primary_coverage_id.blank?
      @coverage = Coverage.find(self.primary_coverage_id).regions
    end
  end

  def secondary_coverage_regions
    @coverage = []
    if !self.secondary_coverage_id.blank?
      @coverage = Coverage.find(self.secondary_coverage_id).regions
    end
  end

  def compare_coverage(tmp_coverage_id,pri_or_sec="primary")
    if pri_or_sec != "primary"
      f = self.secondary_coverage_id
    else
      f = self.primary_coverage_id
    end
    coverage = []
    if !f.blank?
      coverage = Coverage.find(f).regions.collect {|r|r.region_code}
    end
    tmp_coverage = Coverage.find(tmp_coverage_id).regions.collect {|r|r.region_code}
    @delta = []
    ((coverage - tmp_coverage) + (tmp_coverage - coverage)).each do |d|
      if coverage.include?(d)
        @delta << "Removed #{pri_or_sec} coverage #{d}."
      else
        @delta << "Added #{pri_or_sec} coverage #{d}."
      end
    end
    return @delta
  end

  def update_coverage(tmp_pri_coverage_id,tmp_sec_coverage_id)
    primary_coverage_delta   = compare_coverage(tmp_pri_coverage_id)
    secondary_coverage_delta = compare_coverage(tmp_sec_coverage_id,'secondary') if tmp_sec_coverage_id

    if !primary_coverage_delta.empty?
      self.coverage.swap_temp_status(tmp_pri_coverage_id) if self.coverage
      self.primary_coverage_id = tmp_pri_coverage_id
      self.last_change = primary_coverage_delta.join(' ')
      self.save!
    end
    if tmp_sec_coverage_id && !secondary_coverage_delta.empty?
      self.secondary_coverage.swap_temp_status(tmp_sec_coverage_id) if self.secondary_coverage
      self.secondary_coverage_id = tmp_sec_coverage_id
      self.last_change = secondary_coverage_delta.join(' ')
      self.save!
    end


  end

  def data_set_regions
    regionids = []
    self.product_data_set_infos.each do |pdsi|
      regionids << pdsi.coverage.regions.collect{|r|r.id}
    end
    return Region.order(:name).find(regionids.uniq)
  end

  def update_customer_contents
    product_line = self.product_line
    if product_line
      ### remove all existing customer content not belonging to the Product line
      self.customer_contents.each do |customer_content|
        customer_content.destroy unless product_line.customer_content_definitions.find_by_customer_content_specification_id(customer_content.customer_content_specification_id)
      end

      CustomerContentSpecification.active?.each do |customer_content_specification|
        customer_content_definitions = customer_content_specification.customer_content_definitions.where(product_line_id:product_line.id)
        unless customer_content_definitions.empty?
          # the customer_content_specification belongs to the product_line, update or generate the customer_content
          customer_content = self.customer_contents.find_by_customer_content_specification_id(customer_content_specification.id)
          if customer_content
            # update the customer content unless it is not meant to be set on the product level or it is a manual editable field which already has a value
            if customer_content_specification.for_product? && (customer_content.value.to_s.blank? || !customer_content_specification.manual)
              new_value = customer_content.generate
              # to prevent the overwriting of values which were filled with validation scripts from other customer_content objects, only set the value if there is a new one.
              unless new_value.to_s.blank?
                customer_content.value = new_value
                customer_content.save
              end
            end
          else
            # customer_content not present yet, create it
            customer_content = CustomerContent.new()
            customer_content.customer_content_specification = customer_content_specification
            customer_content.product = self
            customer_content.value = customer_content.generate if customer_content_specification.for_product
            customer_content.save
          end

          # remove all existing customer_targets of the current customer_content which are not part of customer_content_definitions
          customer_content.customer_targets.each do |customer_target|
            customer_target.destroy unless customer_content_definitions.find(customer_target.customer_content_definition)
          end

          customer_content_definitions.each do |customer_content_definition|
            unless customer_content_definition.output_target == CustomerContentDefinition::OUTPUT_TARGET_NONE ||
                customer_content.customer_targets.find_by_customer_content_definition_id(customer_content_definition.id)
              customer_target = CustomerTarget.new()
              customer_target.customer_content = customer_content
              customer_target.customer_content_definition = customer_content_definition
              customer_target.output_target = customer_content_definition.output_target
              customer_target.label = customer_content_definition.label
              customer_target.save
            end
          end
        end
      end

    else
      # no Product line so no customer contents, remove all
      self.customer_contents.each do |customer_content|
        customer_content.destroy
      end
    end
  end

  def duplicate
    Product.transaction do
      product_clone = self.dup
      dup_volumeid = "Clone of: " + self.volumeid
      if Product.find_by_volumeid(dup_volumeid).nil?
        clone_volumeid = dup_volumeid
      else
        seq = Product.where("volumeid LIKE '%#{dup_volumeid}'").size + 1
        clone_volumeid = "(" + seq.to_s + ") " + dup_volumeid
      end
      product_clone.status = 5 # Cloned
      product_clone.volumeid = clone_volumeid
      product_clone.version = 0
      product_clone.save

      if self.coverage
        product_clone.primary_coverage_id = self.coverage.duplicate.id
        product_clone.save
      end

      if self.secondary_coverage
        product_clone.secondary_coverage_id = self.secondary_coverage.duplicate.id
        product_clone.save
      end

      self.product_data_set_infos.each do |data_set_info|
        data_set_info_clone = data_set_info.dup
        data_set_info_clone.product_id = product_clone.id
        data_set_info_clone.save
        if data_set_info.coverage
          data_set_info_clone.coverage_id = data_set_info.coverage.duplicate.id
          data_set_info_clone.save
        end
      end

      self.includeddocuments.each do |document|
        document_clone = document.dup
        document_clone.product_id = product_clone.id
        document_clone.save
      end

      self.high_level_boms.each do |bom|
        bom_clone = bom.dup
        bom_clone.product_id = product_clone.id
        bom_clone.save
      end

      self.customer_contents.each do |customer_content|
        customer_content_clone = customer_content.dup
        customer_content_clone.product_id = product_clone.id
        customer_content_clone.save
        customer_content.customer_targets.each do |customer_target|
          customer_target_clone = customer_target.dup
          customer_target_clone.customer_content_id = customer_content_clone.id
          customer_target_clone.save
        end
      end
      product_clone.reload
      return product_clone
    end

    
  end


  def file_for_derived_product(file)
    if self.product_content_specification
      self.product_content_specification.product_content_items.each do |item|
        if (item.resource_identification && item.resource_identification.size > 0)
          r = Regexp.new( item.resource_identification)
          if r.match(file)
            return [item, "RU"]
          end
        end
        if (item.resource_identification_iu && item.resource_identification_iu.size > 0)
          r = Regexp.new(item.resource_identification_iu)
          if r.match(file)
            return [item, "IU"]
          end
        end
      end
    end
    return nil
  end

  def create_derived_product(product_item,tag="")
    derived_product = self.dup
    derived_product.volumeid = self.volumeid + "_" + tag + "_" + product_item.destination_region_code

    Product.transaction do
      product = Product.where("volumeid = '#{derived_product.volumeid}'").first
      if !product
        derived_product.created_from = "PARENT"
        derived_product.parent_id = self.id
        derived_product.name = "#{derived_product.name} #{tag} #{product_item.destination_region_code}"
        derived_product.remarks = "Derived from #{self.volumeid} by System"
        derived_product.save!
      else
        derived_product = product
      end
    end
    return derived_product
  end

  def file_for_product?(file)
    if self.file_detect_regex && self.file_detect_regex.size > 0
      r = Regexp.new( self.file_detect_regex)
      if r.match(file)
        return true
      end
    end
    return false
  end

  def rdo_database
    #Find last production order that has rdo file
    Conversion.where("productid = '#{self.volumeid}'").order("id desc").each do |c|
      c.conversion_databases.available.where("path_and_file like '%rdo.sq3%'").order("created_at desc").each do |db|
        return db
      end
    end
    raise "Expected RDO file not found for #{self.volumeid}"
  end

  def pid_database
    #Find last production order that has rdo file
    Conversion.where("productid = '#{self.volumeid}'").order("id desc").each do |ol|
      ol.conversion_databases.available.where("path_and_file like '%pid.sq3%'").order("created_at desc").each do |db|
        return db
      end
    end
    raise "Expect pid path not found for #{self.volumeid}"
  end

  def uniquify_product_data_set_infos
    pdsi_h = self.product_data_set_infos.collect{|pdsi|[pdsi.id,pdsi.attributes.delete_if{|k,v|k == 'id'|| 
            k == 'description'|| k == 'remarks' || k == 'major_dataset' ||
            k == 'coverage_id' || k.match("_at")|| k.match("_version") }]}
    pdsi_attrs = []
    ProductDataSetInfo.transaction do
      pdsi_h.each do |h|
        if pdsi_attrs.include?(Digest::SHA1.hexdigest(h[1].to_s))
          ProductDataSetInfo.find(h[0]).destroy
        else
          pdsi_attrs << Digest::SHA1.hexdigest(h[1].to_s)
        end
      end
    end
  end

  def active_orders
    orderlines = ProductionOrderline.where(product_id:self.volumeid)
    active_orders = []
    orderlines.each do |line|
      next if !line.production_order.ordertype.job_support_yn
      if ( line.bp_name == 'COMPLETE' || line.bp_name == 'CANCELLED' )
        next
      else
        active_orders << line.production_order if ( line.production_order.status != 'COMPLETE' && line.production_order.status != 'CANCELLED' )
      end
    end
    return active_orders.uniq
  end

  def force_edit(current_user)
    # product has active orders but editing is forced
    self.active_orders.each do |order|
      order.log_orderstatus({:user => current_user.user_name, :remarks => "Forced editing of product #{self.name}.",:status => order.status,:reference => self.volumeid})
      Postoffice.product_edit_during_active_order(self,order,current_user).deliver
    end
  end

  def allow_import?
    if self.active_orders.empty?
      return true
    elsif !self.import_disabled
      return true
    else
      return false
    end
  end

end
