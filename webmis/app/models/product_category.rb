class ProductCategory < ActiveRecord::Base
  has_many :distribution_lists
  has_many :ordertypes
  has_many :static_release_note_titles
  has_many :default_content_release_notes
  
  has_many :release_note_attachments_product_categories
  has_many :release_note_attachments, :through => :release_note_attachments_product_categories
 
 def self.actives
   self.where(:active => true)
 end 
end
