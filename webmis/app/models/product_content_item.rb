class ProductContentItem < ActiveRecord::Base
  version_fu
  belongs_to :data_type, :foreign_key => 'source_data_type_id'
  belongs_to :region, :foreign_key => 'source_region_code_id'
  belongs_to :data_release, :foreign_key => 'source_data_release_id'
  belongs_to :product_content_specification
end
