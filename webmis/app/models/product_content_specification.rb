class ProductContentSpecification < ActiveRecord::Base
  version_fu
  has_many :product_content_items
end
