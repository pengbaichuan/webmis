class ProductDataSetInfo < ActiveRecord::Base
  belongs_to :product
  belongs_to :data_type, :foreign_key => 'datatype_id'
  belongs_to :company_abbr
  belongs_to :coverage
  belongs_to :data_set
  belongs_to :region, {:foreign_key => 'area_name', :primary_key => 'name'}

  after_create do
    self.create_non_existing_data_release
    if self.product_id && self.product_id.size > 0
      p = Product.find(self.product_id)
      p.last_change = "Dataset #{self.name} was added."
      p.save
    end
  end

  after_update do
    self.create_non_existing_data_release
    if self.product_id && self.product_id.size > 0
      p = Product.find(self.product_id)
      p.last_change = "Dataset #{self.name} was updated."
      p.save
    end
  end

def coverage_regions
  @coverage = []
  if !self.coverage_id.blank?
    @coverage = Coverage.find(self.coverage_id).regions
  end
end

def has_region?(region_id)
  if self.coverage
   r = self.coverage.coverages_regions.index{|x|x.region_id==region_id}
  end
  if r && r>=0
    return true
  else
    return false
  end
end

def create_non_existing_data_release
  if self.company_abbr && !self.data_release.blank?
    if self.company_abbr.data_releases.where(:name => self.data_release).empty?
      DataRelease.transaction do
        data_release_code = self.data_release.gsub('.','_').gsub(/^v/ix,'').gsub(/q/ix,'_Q').gsub('__','_').strip
        data_release = DataRelease.new(:name => self.data_release, :company_abbr_id => self.company_abbr_id, :active=>true, :release_code => data_release_code, :production => true)
        # data_release.save
        # Disabled with CR PROC-3651
      end
    end
  end
end

end
