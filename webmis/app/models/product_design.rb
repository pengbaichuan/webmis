class ProductDesign < ActiveRecord::Base
  include Rails.application.routes.url_helpers
  # before_save hook must come before version_fu, otherwise the before_save hook in version_fu
  # does not detect the column changes done by the before_save hook.
  belongs_to :parameter_setting
  before_save :before_save
  version_fu
  acts_as_ordered_taggable_on :tags
 
  attr_accessor :modelhash
  after_initialize :init
  include HandleVersioning
  #validates :status, workflow: true
  
  belongs_to :product_line
  belongs_to :region
  belongs_to :company_abbr
  belongs_to :data_release
  belongs_to :project
  validates :design_type, :presence => true
  validates :name, :presence => true
  validate :needs_platform
 
  has_many :used_product_designs
  has_many :used_in_product_designs, :class_name => "UsedProductDesign", :foreign_key => :used_product_design_id
  has_many :product_designs, :through => :used_product_designs
  has_many :in_use_product_designs, :source => :product_design, :through => :used_in_product_designs
  has_many :products
  belongs_to :project
  has_one :environment, :through => :project

  scope :frontend, -> {where('design_type = "frontend" AND status != "obsolete"')}
  scope :backend,  -> {where('design_type = "backend"  AND status != "obsolete"')}
  scope :active, -> {where('status != "obsolete"')}
  scope :approved, -> {where('status = "approved"')}
  
  def in_use_active_product_designs
    self.used_in_product_designs.active.collect{|ui_pd|ui_pd.source_product_design}.uniq
  end
  
  # for now no required review as we are still developing
  # @@attrs_requiring_review = [:name, :model]
  @@attrs_requiring_review = []

  def needs_platform
    if !(@modelhash["product_line"] && !@modelhash["product_line"]["id"].blank?)
      errors[:base] << "Product line can't be blank"
    end
  end

  def self.attrs_requiring_review
    return @@attrs_requiring_review
  end

  def self.init_with_model(jsonmodel=nil)
    pd = ProductDesign.new
    pd.model = jsonmodel
    pd.modelhash = JSON.parse(jsonmodel) 
    return pd
  end
      
  def init
    if !self.model 
      self.model = {}.to_json
    end
    @modelhash = JSON.parse(self.model)
    return @modelhash
  end

  def before_save
    if @modelhash
      self.model = @modelhash.to_json
    end
    self.project_id = Project.default.id if self.project_id.blank?
  end

  def collect_md5
    md5arr = []
    @modelhash["conversion_steps"].each do |step|
      if step["used_product_design_md5"]
        md5arr << step["used_product_design_md5"]
      end
    end

    return md5arr
  end

  def used_in_designs(md5)
    designs = []
    useddesigns =  UsedProductDesign.where('used_product_design_md5 = "' + md5 + '" and version_active_until is null')
    useddesigns.each do |udesign|
      designs << ProductDesign.find(udesign.product_design_id)
    end
    return designs.uniq
  end

  def sync_all_designs(md5)
    synced_designs = []
    used_in_designs(md5).each do |design|
      if design.synchronize_design(self, md5)
        design.save!
        synced_designs << design.id
      end
    end
    return synced_designs.uniq
  end

  def sync_used_designs
    md5arr = collect_md5
    self.used_product_designs.each do |used_design|
      if used_design.used_product_design_md5
        if !md5arr.index(used_design.used_product_design_md5)
          used_design.version_active_until = self.version
          used_design.save!
        end
      end
    end
  end

  def save_and_synchronize(original_md5=nil)
    synced_designs =[]
    self.product_design_md5 = self.calc_md5
    self.save!
    original_md5 = self.versions[-2].product_design_md5 if !original_md5
    if (original_md5 != self.product_design_md5)
      synced_designs = self.sync_all_designs(original_md5) if original_md5 # sync backend designs
    end
    self.save!
    return synced_designs
  end
  
  def get_region_name_from_taglist(taglist)
    area_name = ""
    taglist.each do |tag|
      region = Region.active.find_by_name(tag)
      if region
        if area_name == ""
          area_name = tag
        else
          raise "Duplicate Areas found on tags " + taglist.join(' ')
        end
      end
    end
    raise "Region not found for " + taglist.join(' | ') if area_name == ""
    return area_name
  end

  def get_datarelease_from_taglist(taglist, supplier)
    releaseobj = nil
    taglist.each do |tag|
      release = supplier.data_releases.find_by_name(tag)
      if release
        if !releaseobj
          releaseobj = release
        else
          raise "Duplicate release tag found on process data " + taglist.join(' ')
        end
      end
    end
    raise "Release not found for #{supplier.company_name}"  + taglist.join(' ') if !releaseobj
    return releaseobj
    
  end

  def get_supplier_from_taglist(taglist)
    supplierobj = nil
    taglist.each do |tag|
      supplier = CompanyAbbr.find_by_company_name(tag)
      if supplier
        if !supplierobj
          supplierobj = supplier
        else
          raise "Duplicate supplier tag found on process data " + taglist.join(' ')
        end
      end
    end
    raise "Supplier not found for " + taglist.join(' ') if !supplierobj
    return supplierobj
  end

  def get_datasets_from_process_data(dataset_hash, format, filling, major_dataset)

    items = DataSet.active.tagged_with(filling).where(id:dataset_hash.keys)
    if items.size > 1
      # match on format and filling
      items = DataSet.active.active.tagged_with([format,filling]).where(id:dataset_hash.keys)
    end

    if items.size > 1 || items.size == 0
      # match on format
      items = DataSet.active.active.tagged_with([format]).where(id:dataset_hash.keys)
    end

    if items.size ==0
      raise "Data set not found for : " + filling + " " + format
    end

    #if items.size > 1
    #  raise "Duplicate process Data Items not found for : " + filling + " " + format
    #end
    datasets = []
    items.each do |item|
      taglist = item.tag_list
      region_name = item.region.name
      dataset = ProductDataSetInfo.new
      dataset.major_dataset = major_dataset
      dataset.name = taglist.join(" ")
      dataset.description = taglist.join(" ")
      datatype = DataType.find_by_name(format).latest_baseline
      dataset.datatype_id = datatype.id if datatype
      dataset.remarks = "Resolved from process data item : " + item.id.to_s
      dataset.area_name = region_name
      supplier = item.company_abbr
      dataset.company_abbr_id = supplier.id
      dataset.data_release = item.data_release.name
      dataset = dataset.attributes
      dataset["datatype"] = format.upcase
      dataset["supplier_abbr"] = supplier.ms_abbr_3.upcase
      dataset["supplier_name"] = supplier.company_name
      dataset["filling"] = filling if filling
      dataset["datatype_version"] = datatype.version if datatype
      # assume coverage required is equal to the full database
      if dataset_hash[item.id.to_s].size > 0
        dataset["coverage_required"] = []
        dataset_hash[item.id.to_s].each do |region_code|
          dataset["coverage_required"] << region_code.downcase
        end
      else
        dataset["coverage_required"] << Region.active.where("name = '#{region_name}'").first.region_code.downcase
      end
      datasets << dataset
    end

    return datasets
  end

  def get_dataset(format, filling, product)
    datasets = nil
    if (filling && filling.size > 0)
      datasets = product.product_data_set_infos.includes(:data_type).where("data_types.name = '#{format}' AND filling = '#{filling}'").references(:data_types)
    else
      datasets = product.product_data_set_infos.includes(:data_type).where("data_types.name = '#{format}'").references(:data_types)
    end
    
    if datasets.size == 0
      raise "Dataset not found for : #{format} #{filling}"
    else
      return datasets
    end
  end

  def get_supplier(format)
    # this assumes a format is only delivered by one supplier
    supplier = "UNKNOWN"
    
    modelhash["input_datatypes"].each do |input|
      if input["type"] == format || input["type"] == "RDF" && format == "RDB"
        supplier = input["supplier"]
        return supplier
      end
    end
    if supplier == "UNKNOWN"
      raise "Supplier cannot be found for " + format
    end
    
    return supplier
  end

  def get_release(supplier, type, product)
    release = "UNKNOWN"
    coverage = "UNKOWN"
    major = 1

    product.product_data_set_infos.each do |dataset|
      dt_name = "NA"
      if dataset.data_type
        dt_name = dataset.data_type.name
      end
      if (dt_name == type || dataset.name == type) && dataset.company_abbr.ms_abbr_3 == supplier
        release = dataset.data_release
        coverage = dataset.area_name
        major = dataset.major_dataset
      end
    end
      
    if type == "RDB"
      type = "RDF"
      product.product_data_set_infos.each do |dataset|
        dt_name = "NA"

        if dataset.data_type
          dt_name = dataset.data_type.name
        end

        if dt_name == type && dataset.company_abbr.ms_abbr_3 == supplier
          release = dataset.data_release
          coverage = dataset.area_name
          major = dataset.major_dataset
        end
      end
    end
      
    if release == "UNKNOWN"
      raise "Release could not be determined for  " + supplier.to_s + " " + type.to_s + " " + product.volumeid
    end
    return [release,coverage,major]
  end

  def create_process_data_matchreport(dataset_hash)
    report = ProcessDataMatchReport.new
    if self.has_conversion_steps?
      modelhash["conversion_steps"].each do |step|
        step["connections"].each do |connection|
          next if !connection["source_id"].match("^process_data") && !connection["source_id"].match("^data_set")
          if connection["target_spec"]["format"]
            format = connection["target_spec"]["format"]
          else
            format = connection["target_spec"]["allocate_tags"]
          end
          package_id = connection["target_spec"]["package_id"]
          begin
            datasets = get_datasets_from_process_data(dataset_hash, format, connection["filling"].to_s, 0)
            report.add_matchresult(package_id, format, connection["filling"].to_s, datasets.collect{|x|x["remarks"].match(/item : (.*)/)[1].to_i }, message=nil)
          rescue => e
            report.add_matchresult(package_id,format, connection["filling"].to_s, nil, e.message)
          end
        end
      end
    else
      raise "Productdesign #{self.id} '#{self.name}' has no conversion steps!"
    end
    return report
  end

  def include_package?(connection, matchreport)
    package_id = connection["target_spec"]["package_id"]
    if matchreport.matches.index{|x|x.package_id == package_id && x.include == true }
      return true
    else
      return false
    end
  end

  def create_production_order_from_process_data(data_set_id, dataset_hash, conversiontool_id, cvtool_bundle_id, ordertype_id,requestor_user_id, purpose, use_test_versions_yn, conversion_environment_id, force_create, checklog, matchreport, v_project_id = Project.default.id, simulate_dakota=false, test_strategy="None")
    po = nil
    #steps_req_process_data = self.process_data_requirements.collect{|x| x["target_sequence"].to_i}.uniq
    ot = Ordertype.find(ordertype_id)
    startprocess = ot.process_transitions[0].from_process.name

    toolrelease = ''
    if !conversiontool_id.blank? && !cvtool_bundle_id.blank?
      release = Conversiontool.find(conversiontool_id)
      bundle = CvtoolBundle.find(cvtool_bundle_id)
      toolrelease = "#{release.code if release}/#{bundle.name if bundle}"
    end
    
    ProductionOrder.transaction do
      po = ProductionOrder.new(:project_id => v_project_id)
      po.test_strategy = test_strategy
      po.allow_planning = true
      po.allow_production = true
      po.bp_name = startprocess
      po.name = "Data set " + data_set_id.to_s + " - " + DataSet.find(data_set_id).name + " - #{toolrelease}"
      po.ordertype_id = ot.id
      po.process_transitions_snapshot = ProcessTransitionsSnapshot.new(ot.serialize[:process_transitions]).to_json
      po.use_test_versions_yn = use_test_versions_yn
      po.use_dakota_simulator_yn = simulate_dakota
      po.requestor = User.find(requestor_user_id).full_name
      po.save!
      po.checklog = checklog
      po.checklog.add_attributes({
          "Business process" => po.bp_name,
          "Name" => po.name.to_s,
          "Request date" => DateTime.now
        })
      po.checklog.add_debug_line("Production order successfully saved.")
    
      # create line for each conversion_step in design
      seq = 0
      idhash = {}
      tagmodel = {}
      count = 0
      if self.has_conversion_steps?
        modelhash["conversion_steps"].each do |step|
          conversion = Conversion.new
          conversion.force_create = po if force_create[count]
          conversion.checklog = Checklog.new(po.checklog.only_check_yn, po.checklog.loglevel)
          conversion.checklog.add_info_line("Creating conversion for conversion step #{step['name']}.")
          conversion.input_format = step["input"][0]["format"]
          conversion.output_format = step["output"][0]["format"]
          conversion.checklog.add_attributes({"Input format" => conversion.input_format, "Output format" => conversion.output_format})

          conversion.purpose = purpose
          conversion.design_sequence = seq
          conversion.checklog.add_attributes({"Purpose" => conversion.purpose, "Design sequence" => conversion.design_sequence})

          if cvtool_bundle_id && cvtool_bundle_id.size > 0
            conversion.cvtool_bundle_id = cvtool_bundle_id
            conversion.checklog.add_attributes({"CVTool bundle" => conversion.cvtool_bundle.name})
          end

          if conversiontool_id && conversiontool_id.size > 0
            conversion.conversiontool_id = conversiontool_id
            conversion.checklog.add_attributes({"Conversiontool" => conversion.conversiontool.code})
          end

          if step["connections"]
            conversion.checklog.add_debug_line("Stepping through the connections...")
            coverage_code_hist = ''
            filling_hist = ''
            core_tags_hist = []
            step["connections"].each do |connection|
              if matchreport && !include_package?(connection,matchreport) && ( connection["source_id"].match("^process_data") || connection["source_id"].match("^data_set") )
                connection["exclude"] = true
                connection["exclude_remarks"] = "User specified to exclude data package"
                next
              end
              # take first connection for input
              if connection["target_spec"]["format"] && !conversion.input_format
                conversion.input_format = connection["target_spec"]["datatype"]
              end
              if connection["target_spec"]["format"]
                format = connection["target_spec"]["format"]
              else
                format = connection["target_spec"]["allocate_tags"]
              end
              major_dataset = 0
              major_dataset = 1 if format.upcase == conversion.input_format.upcase
              if DataType.where(:name => format).first.is_intermediate 
                connection["source_spec"]["tag_model"] = idhash[connection["source_spec"]["package_id"]].clone
                if connection["source_spec"]["tag_model"]["coverage"]
                  coverage_code = connection["source_spec"]["tag_model"]["coverage"][0]
                  coverage_code_hist = coverage_code
                else
                  if coverage_code_hist.blank?
                    raise "Coverage not found for #{step["name"]}"
                  else
                    coverage_code = coverage_code_hist
                  end
                end

                conversion.region_id = Region.active.find_by_region_code(coverage_code).id

                # Prefer filling 'core' if available
                if connection["source_spec"]["tag_model"]["core"]
                  filling = 'core'
                  filling_hist = filling                 
                else
                  if connection["source_spec"]["tag_model"]["filling"]
                    filling = connection["source_spec"]["tag_model"]["filling"].first
                    filling_hist = filling
                  else
                    if filling_hist.blank?
                      raise "Filling not found for #{step["name"]}"
                    else
                      filling = filling_hist                     
                    end                    
                  end
                end

                if connection["source_spec"]["tag_model"][filling]
                  tags = connection["source_spec"]["tag_model"][filling][0]
                  core_tags = tags.split("|")
                  core_tags_hist = core_tags
                else
                  if core_tags_hist.empty?
                    raise "Filling not found for #{step["name"]}"
                  else
                    core_tags = core_tags_hist.flatten
                  end
                end
                company_abbr = core_tags.flatten[1]

                company_id = CompanyAbbr.find_by_ms_abbr_3(company_abbr).id

                release_name = core_tags[2]
                conversion.data_release_id =  DataRelease.where(:company_abbr_id => company_id, :name => release_name).first.id
              else
                datasets = get_datasets_from_process_data(dataset_hash, format, connection["filling"].to_s, major_dataset)
                dataset = datasets[0] # will work for one one one mapping
                connection["source_spec"]["datasets"] = datasets.clone
              end

              if (major_dataset == 1 && (!DataType.where(:name => format).first.is_intermediate))
                region= Region.active.find_by_name(dataset["area_name"])
                if region.instance_of?(Continent)
                  conversion.region_id = region.id
                else
                  if region.parent
                    conversion.region_id = region.parent.id
                  else
                    conversion.region_id = region.id
                  end
                end
                conversion.data_release_id = DataRelease.where(:company_abbr_id => dataset["company_abbr_id"], :name => dataset["data_release"]).first.id
                conversion.checklog.add_attributes({
                    "Data release id" => "#{conversion.data_release.name} (#{conversion.data_release_id})",
                    "Region" => conversion.region_id ? conversion.region.name : ""
                  })
              end
            end
          end

          step["output"].each do |output|
            taggings = ProductDesign.get_tagmodel_from_conversion_step(step)
            idhash[output["package_id"]] = taggings if taggings.size > 0 && !idhash[output["package_id"]]
          end

          step["output_tag_model"] =  ProductDesign.merge_tagmodel(tagmodel, ProductDesign.get_tagmodel_from_conversion_step(step))
          conversion.request_date = Time.now
          conversion.conversion_date = nil
          conversion.remarks = "AUTO GENERATED CONVERSION F.E."
          conversion.cvtoolenv = ""
          conversion.size_bytes = nil
          conversion.size_mb = nil
          conversion.database_name = ""
          conversion.converter = "WebMIS"
          conversion.additional_info = ""
          conversion.conversion_date = nil
          conversion.reception_reference = nil
          conversion.created_at = Time.now
          conversion.tpd_products = nil
          conversion.tpd_rejected = nil
          conversion.tpd_rejection_remark = nil
          conversion.tpd_result_sender = nil
          conversion.set_status(Conversion::STATUS_REQUESTED)
          conversion.server_name = nil
          conversion.result_files = nil
          conversion.status_remark = nil
          conversion.rejection_remark = nil
          conversion.conversion_environment_id = conversion_environment_id

          fpol = ProductionOrderline.new
          fpol.force_create = false
          fpol.product_design  = modelhash.to_json
          fpol.production_order_id = po.id
          fpol.product_id = nil
          fpol.quantity = 1
          fpol.remarks = "Auto generated Frontend Order"
          fpol.save!
          fpol.design_sequence = conversion.design_sequence
          fpol.checklog = checklog.new_sub("Production orderline #{fpol.id}", Checklog::LOGLEVEL_WARNING)
          fpol.checklog.add_attributes({
              "Product" => fpol.product_id,
              "Business process name" => fpol.bp_name
            })
          fpol.checklog.add_debug_line("Frontend production orderline created.")

          po.production_orderlines << fpol
          po.save!
          conversion.production_orderline_id = fpol.id
          conversion.request_by = User.find(requestor_user_id).full_name
          conversion.request_user_id = requestor_user_id
          conversion.save!
          seq += 1
          conversion.checklog.add_attributes({"Request by" => conversion.request_by, "Request userid" => conversion.request_user_id})
          fpol.checklog.add_sub("Conversion #{conversion.id}", conversion.checklog)
          count += 1
        end
      else
        raise "Productdesign #{self.id} '#{self.name}' has no conversion steps!"
      end
    
      po.save!
      po.transition(nil, 'forward')
    end
    return po
  end

  def is_backend_step?(step)
    #not used yet
    result = false
    
    step["output"].each do |output|
      dt = DataType.find_by_name(output["format"])
      if !dt
        raise "Datatype not found for " + output["format"]
      end
      result = true if !dt.is_intermediate || (dt.is_intermediate && step["Input_has_output"])
    end

    return result
  end

  def create_production_order(product_id, po=nil, conversiontool_id = nil, cvtool_bundle_id = nil, split_frontend = true, force_create = [], use_test_versions_yn = false, checklog = nil, simulate_dakota=false, test_strategy="None")
    ActiveRecord::Base.logger = nil
    fpo = nil
    process_data_created = []
	  create_frontend_order = true
    create_frontend_order = false if !po.create_frontend_order_yn
    # add screen to create production order
    # add status to design
    product = Product.find_by_volumeid(product_id)
    productset = product.productsets[0]
    steps_req_process_data = self.process_data_requirements.collect{|x| x["target_sequence"].to_i}.uniq
    
    ProductionOrder.transaction do
      lines = []
      po = ProductionOrder.new(:project_id => self.project_id) if !po
      po.use_dakota_simulator_yn = simulate_dakota
      po.test_strategy = test_strategy if !po.test_strategy
      if productset
        po.productset_id = productset.id
        po.major = productset.major
        po.minor = productset.minor
        po.generate_name(productset.setcode)
      else
        po.generate_name(product.volumeid)
      end
      
      po.ordertype_id = 4 if !po.ordertype_id# hardcoded for now
      po.process_transitions_snapshot = ProcessTransitionsSnapshot.new(Ordertype.find(po.ordertype_id).serialize[:process_transitions]).to_json if !po.process_transitions_snapshot
      
      startprocess = Ordertype.find(po.ordertype_id).process_transitions[0].from_process.name
      po.bp_name = startprocess
      #po.requestor = user
      #po.owner = user
      #po.comments = comment
      #po.priority = priority
      po.save
      po.checklog.add_attributes({
          "Productset id" => po.productset_id,
          "Version" => !po.major.to_s.blank? ? "#{po.major}.#{po.minor}" : "",
          "Name" => po.name,
          "Ordertype" => po.ordertype.name,
          "Business process" => po.bp_name,
        })
      po.checklog.add_debug_line("Production order successfully saved.")

      fpol = nil

      fpo_checklog = nil
      pol_checklog = nil
      fpol_checklog = nil


      pol = ProductionOrderline.new
      pol.force_create = false
      pol.production_order_id = po.id
      pol.product_id = product.volumeid
      pol.product_line_id = product.product_line_id
      pol.quantity = 1
      pol.remarks = product.remarks
      pol.design_sequence = 0
      pol.created_from = product.created_from
      if product.parent
        parent_linenr = po.production_orderlines.where("product_id = '#{product.parent.volumeid}'")
        pol.parent_linenr = parent_linenr[0].id if parent_linenr && parent_linenr[0]
      end
      
      pol.save

      pol.checklog = po.checklog.new_sub("Production orderline #{pol.id}", Checklog::LOGLEVEL_INFO)
      pol.checklog.add_attributes({
          "Product" => "#{pol.product_id} (#{pol.product.name})",
          "Business process name" => pol.bp_name,
          "Quantity" => pol.quantity,
          "Remarks" => pol.remarks,
          "Design sequence" => pol.design_sequence,
          "Created from" => pol.created_from,
          "Parent line" => pol.parent_linenr
        })
      pol.checklog.add_debug_line("Production orderline successfully saved.")

      pol.generate_customer_contents
      po.production_orderlines << pol
      po.save
      po.checklog.add_info_line("Production orderline added.")
      #product.product_design = modelhash.to_json
      #product.save
      
      seq = 0

      backend_tags = []
      current_source_tags = []
      backend_step = {}
      backend_input_dhive = {}
      related_conversions = []
      idhash = {}
      
      modelhash["conversion_steps"].each do |step|
        # START BLOCK STEPS
        conv_log = Checklog.new(po.checklog.only_check_yn, po.checklog.loglevel)
        po.checklog.add_info_line("Handling conversion step #{step['name']}.")
                  
        #assume 1 input for now"
        supplier = nil
        frontend= true
        major_input_format = ""
        last_data_release_id = nil
        supplier_abbr = ""
        datasets = nil
        dataset = nil
        tagmodel = {}
        region_ids = []
        conversion_data_release_id_attr = nil
        conversion_input_format_attr = nil
        
        if step["connections"]
          conv_log.add_debug_line("Stepping through the connections...")

          step["connections"].each do |connection|
            format = ""
            if connection["target_spec"]["format"]
              format = connection["target_spec"]["format"]
            else
              format = connection["target_spec"]["allocate_tags"]
            end

            #if is_backend_step?(step)
            if !connection["source_id"].match("^process_data") && !connection["source_id"].match("^data_set")
              #backend!!!
              conv_log.add_info_line("Found backend step '#{connection['source_id']}' => '#{connection['target_id']}'")
              
              backend_step = step
              backend_input_dhive = connection

              if idhash[connection["source_spec"]["package_id"]]
                tagmodel = ProductDesign.merge_tagmodel(tagmodel, idhash[connection["source_spec"]["package_id"]])
                connection["source_spec"]["tag_model"] = idhash[connection["source_spec"]["package_id"]]
              else
                raise "Tagmodel not found for #{connection["source_spec"]["package_id"]} connection:: #{connection.inspect}"
              end

              # ignoring the versioning on DataType for now as we just need to determine whether we're dealing with a fronted order or not.
              if DataType.find_by_name(step["output"][0]["format"]).is_intermediate
                # if the output is intermediate then add to frontend order but using tagmodel for allocation
                frontend = true
              else
                frontend = false
              end
              
              step["output"].each do |output|
                if !output["tags"]
                  output["tags"] = {}
                end
                if !output["tags"][format]
                  output["tags"][format] = []
                end
                output["tags"][format] << connection["source_spec"]["originating_format"]
                output["tags"][format] << connection["source_spec"]["supplier"]
                output["tags"][format] << connection["source_spec"]["area"]
                output["tags"][format] << connection["source_spec"]["release"]
                if connection["source_spec"]["tags"]
                  output["tags"][format] =  output["tags"][format] + connection["source_spec"]["tags"]
                end
              end
              
            else
              conv_log.add_info_line("Found process data for step: '#{connection['source_id']}' => '#{connection['target_id']}'.")
              dataset = nil
              if format == "PML"
                datasets = []
                pml_data_release = Reception.generate_quarter_release
                pml_data_release = product.poi_mapping_layout.definition_version if product
                datasets << {"filling" => 'POI Mapping Layout', "datatype" => 'PML', "supplier_name" => 'Mapscape',"supplier_abbr" => 'MSP', "data_release" => pml_data_release, "major_dataset" => 0, "area_name" => "World"}
                dataset = datasets.first
              else
                datasets = get_dataset(format, connection["filling"].to_s, product)
                newdatasets = []
                dataset = {}
                datasets.each do |dataset|
                  datatype=  dataset.data_type.name
                  supplier_abbr = dataset.company_abbr.ms_abbr_3
                  supplier_name = dataset.company_abbr.name
                  dataset_h = dataset.attributes
                  dataset_h["datatype"] = datatype
                  dataset_h["supplier_abbr"] = supplier_abbr
                  dataset_h["supplier_name"] = supplier_name
                  if product.coverage
                    if dataset.coverage
                      dataset_h["coverage_required"] = dataset.coverage.regions.map{|x|x.attributes if product.coverage.region_included?(x.region_code) }
                      if dataset_h["coverage_required"].blank?
                        raise "No matching regions found in coverage for product #{product.volumeid} and product dataset #{supplier_abbr}_#{dataset.data_release}_#{dataset.area_name}_#{datatype} #{connection["filling"]}."
                      end
                    else
                      raise "No coverage found for product dataset #{supplier_abbr}_#{dataset.data_release}_#{dataset.area_name}_#{datatype} #{connection["filling"].to_s}"
                    end
                  else
                    raise "No coverage found for product #{product.volumeid}."
                  end

                  current_source_tags << datatype
                  current_source_tags << connection["filling"].to_s
                  current_source_tags << supplier_abbr
                  current_source_tags << dataset.data_release
                  current_source_tags << dataset.area_name
                  newdatasets << dataset_h
                  dataset = dataset_h if dataset["major_dataset"]

                end
                datasets = newdatasets
                dataset = datasets.first if dataset.empty? # will work for one one one mapping
              end
          
              connection["source_spec"]["datasets"] = datasets
              connection["source_spec"]["supplier"] = supplier_abbr
              conv_log.add_attributes({"Source spec datasets" => datasets.to_s, "Source spec supplier" => supplier_abbr})
              
              if format == "PML"
                connection["source_spec"]["area"] = ""
                connection["source_spec"]["release"] = ""
                connection["source_spec"]["major_dataset"] = 0
                connection["source_spec"]["format"] = format
              else
                #release  = get_release(supplier_abbr, format, product)
                connection["source_spec"]["area"] = dataset["area_name"]
                connection["source_spec"]["release"] = dataset["data_release"]
                connection["source_spec"]["major_dataset"] = dataset["major_dataset"]
                connection["source_spec"]["format"] = dataset["datatype"]
              end
              conv_log.add_attributes({
                  "Source spec area" => connection["source_spec"]["area"],
                  "Source spec release" => connection["source_spec"]["release"],
                  "Source spec major dataset" => connection["source_spec"]["major_dataset"],
                  "Source spec format" => connection["source_spec"]["format"]
                })

              supplier = dataset["company_abbr_id"]
              if dataset["major_dataset"] == 1
                #conversion.input_format = format
                conversion_input_format_attr = format
                conv_log.add_attributes({"Input format" => format })
              end

              releases = DataRelease.where(company_abbr_id:supplier)
              if dataset.empty?
                r = Region.active.find_by_name(connection["target_spec"]["area"])
                region_ids << r.id
              else
                r = Region.active.find_by_name(dataset["area_name"])
                region_ids << r.id
              end

              releases.each do |rel|
                if dataset["data_release"] == rel.name
                  if dataset["major_dataset"] == 1
                    # conversion.data_release_id = rel.id
                    conversion_data_release_id_attr = rel.id                    
                  else
                    last_data_release_id = rel.id
                  end
                end
              end

              backend_tags << supplier_abbr
              backend_tags << connection["source_spec"]["area"]
              backend_tags << connection["source_spec"]["release"]
              
            end
          end
        else
          conv_log.add_info_line("No connections found.")
        end

        step["output"].each do |output|
          taggings = ProductDesign.get_tagmodel_from_conversion_step(step)
          idhash[output["package_id"]] = taggings if taggings.size > 0 && !idhash[output["package_id"]]

          # temporary fix to make sure nds tagmodel is calculated
          if output["package_id"].scan(/NDH/)
            idhash[output["package_id"].gsub("NDH","NDS")] = taggings if taggings.size > 0
          end

          if output["package_id"].scan(/NDS/)
            idhash[output["package_id"].gsub("NDS","NDH")] = taggings if taggings.size > 0
          end

        end

        # Create new conversion
        if ((!frontend) || (create_frontend_order && frontend))
          conv_log.add_info_line("Creating conversion for conversion step #{step['name']}.")
          conversion = Conversion.new
          if conversion_input_format_attr
            conversion.input_format = conversion_input_format_attr
          else
            conversion.input_format = step["input"][0]["format"]
          end
          conversion.output_format = step["output"][0]["format"]
          conv_log.add_attributes({"Input format" => conversion.input_format, "Output format" => conversion.output_format})

          conversion.request_by = po.requestor.to_s
          conversion.purpose = "Production"
          conversion.design_sequence = seq
          if force_create.index(seq)
            conversion.force_create = po
          else
            conversion.force_create = nil
          end
          conv_log.add_attributes({"Requested by" => conversion.request_by, "Purpose" => conversion.purpose, "Design sequence" => conversion.design_sequence})

          if cvtool_bundle_id && cvtool_bundle_id.size > 0
            conversion.cvtool_bundle_id = cvtool_bundle_id
            conv_log.add_attributes({"CVTool bundle" => conversion.cvtool_bundle.name})
          else
            conv_log.add_info_line("'CVTool bundle' is not specified.")
          end

          if conversiontool_id && conversiontool_id.size > 0
            conversion.conversiontool_id = conversiontool_id
            conv_log.add_attributes({"Conversiontool" => conversion.conversiontool.code})
          else
            conv_log.add_info_line("'Conversiontool' is not specified.")
          end

          step["output_tag_model"] =  ProductDesign.merge_tagmodel(tagmodel, ProductDesign.get_tagmodel_from_conversion_step(step))
          #pol.product_design = modelhash.to_json
          pol.save
          lines << pol

          conversion.data_release_id = conversion_data_release_id_attr
          conv_log.add_attributes({"Data release id" => "#{conversion.data_release.name} (#{conversion.data_release_id})"}) if conversion_data_release_id_attr
          if !conversion.data_release_id && last_data_release_id
            conversion.data_release_id = last_data_release_id
            conv_log.add_debug_line("No major dataset found, using the latest found data release and region.")
            conv_log.add_attributes({
                "Data release id" => "#{conversion.data_release.name} (#{conversion.data_release_id})",
                "Region" => conversion.region_id ? conversion.region.name : ""
              })
          end

          if !conversion.data_release_id
            #last resort - default to world and major dataset
            dataset = product.product_data_set_infos.where("major_dataset= 1").first
            if dataset
              release =  DataRelease.where("company_abbr_id = #{dataset.company_abbr_id} and name= '#{dataset.data_release}'").first
              conversion.data_release_id = release.id
              r = Region.active.find_by_name(dataset.area_name)
              region_ids << r.id
              conv_log.add_debug_line("No datasets found, using the major dataset.")
              conv_log.add_attributes({
                  "Data release id" => "#{conversion.data_release.name} (#{conversion.data_release_id})",
                  "Region" => conversion.region_id ? conversion.region.name : ""
                })
            else
              conv_log.add_debug_line("No datasets and no major dataset found.")
            end
          end

          if !conversion.data_release_id
            conv_log.add_warning_line("No datarelease found.")
            raise "Can't determine datarelease - check you data info"
          end

          conversion.request_date = Time.now
          conversion.conversion_date = nil
          conversion.converter = "system"
          conversion.remarks = "AUTO GENERATED CONVERSION"
          conversion.cvtoolenv = ""
          conversion.size_bytes = nil
          conversion.size_mb = nil
          conversion.database_name = ""
          conversion.additional_info = ""
          conversion.conversion_date = nil
          conversion.reception_reference = nil
          conversion.created_at = Time.now
          conversion.tpd_products = nil
          conversion.tpd_rejected = nil
          conversion.tpd_rejection_remark = nil
          conversion.tpd_result_sender = nil
          conversion.status = 10
          conversion.server_name = nil
          conversion.result_files = nil
          conversion.status_remark = nil
          conversion.rejection_remark = nil

          if frontend && split_frontend && create_frontend_order
            otypeid = 6
            ot = Ordertype.where(:is_frontend => true).first
            if ot
              otypeid = ot.id
            else
              raise "Ordertype frontend not found"
            end
            if fpo
              po.checklog.add_info_line("Frontend order #{fpo.name} (#{fpo.id}) already created, not creating it again.")
            else

              fpo = po.dup
              fpo.ordertype_id = otypeid
              fpo.process_transitions_snapshot = ProcessTransitionsSnapshot.new(ot.serialize[:process_transitions]).to_json

              fpo.generate_name(current_source_tags.uniq.join(" "), frontend)
              current_source_tags = []
              startprocess = Ordertype.find(otypeid).process_transitions[0].from_process.name
              fpo.bp_name = startprocess
              fpo.checklog.add_attributes({"Name" => fpo.name})
              fpo.save!
              po.checklog.add_info_line("Creating frontend order #{fpo.name} (#{fpo.id}).")
              fpo.checklog = po.checklog.new_sub("Front-end production order #{fpo.id}", Checklog::LOGLEVEL_WARNING)
              fpo.checklog.add_attributes({ "Ordertype" => fpo.ordertype.name })
              fpo.checklog.add_info_line("FrontEnd production order saved.")
            end

            # link to front-end production order
            fpol = ProductionOrderline.new
            fpol.force_create = !force_create.empty?
            fpol.product_design  = modelhash.to_json
            fpol.production_order_id = fpo.id
            fpol.product_id = product.volumeid  # ensure coverage in product by eval RDB2DH executable
            fpol.quantity = 1
            fpol.remarks = product.remarks
            fpol.save!
            fpol.design_sequence = conversion.design_sequence
            fpo.production_orderlines << fpol
            fpo.save!
            conversion.production_orderline_id = fpol.id
            conversion.productid = nil
            link_conv_log_to_frontend = true

            fpol.checklog = fpo.checklog.new_sub("Production orderline #{fpol.id}", Checklog::LOGLEVEL_WARNING)
            fpol.checklog.add_attributes({"Product" => '', "Business process name" => fpol.bp_name })
            fpol.checklog.add_info_line("Frontend production orderline created.")
          else
            if !(frontend && split_frontend)
              conversion.production_orderline_id = pol.id
              conversion.productid = product_id
            end
          end

          region_ids.uniq.each do |region_id|
            conversion.region_id = region_id
            if region_ids.uniq.size > 1
              maindataset = product.main_dataset
              if maindataset
                main_r = Region.active.find_by_name(maindataset.area_name)
                conversion.region_id = main_r.id if main_r
              end
            end
          end

          region= Region.active.find(conversion.region_id)
          if region.instance_of?(Continent)
            conversion.region_id = region.id
          else
            if region.parent
              conversion.region_id = region.parent.id
            end
          end

          conv_log.add_attributes({"Region" => conversion.region.name})
          conversion.save!
          related_conversions << conversion.id if frontend
          if link_conv_log_to_frontend
            fpol.checklog.add_sub("Conversion #{conversion.id}", conv_log)
          else
            pol.checklog.add_sub("Conversion #{conversion.id}", conv_log)
          end
          po.checklog.add_debug_line("Added Conversion #{conversion.id} to " + (link_conv_log_to_frontend ? "front-end orderline #{fpol.id}." : "production orderline #{pol.id}."))
        end # END if ((!frontend) || (create_frontend_order && frontend))
        seq = seq + 1
      end # END BLOCK STEPS
      backend_input_dhive["tags"] = backend_tags.uniq
      backend_step["related_conversions"] = related_conversions
      backend_step["tags"] = backend_tags.uniq
      if fpol
        fpol.product_design = modelhash.to_json
        fpol.save!
      end
      
      lines.each do |line|
        # make sure all lines have all design information
        line.product_design = modelhash.to_json
        line.save!
      end
      fpo.transition(nil, 'forward')  if fpo
    end

    # return fpo so we can check the associated frontend order if required.
    return [fpo,po]
    
  end
  
  def add_step(executable)
    attr = executable.attributes
    step = attr.delete_if{|k,v|k == "description"}
    inp = step["input"]
    outp = step["output"]
    step["input"] = JSON.parse(inp)
    step["output"] = JSON.parse(outp)
    step = add_step_by_hash(step)[0]
    step["input_has_output"] = input_has_output(JSON.parse(inp), JSON.parse(outp))
     
    if executable.parameters.size > 0
      parameters = JSON.parse(executable.parameters)["parameters"]
      step["parameters_spec"] = []
      parameters.each do |param|
        if param["allocate"] && param["allocate"] == true
          pid = get_package_id({"datatype" => param["allocate_tags"]},0)
          param['package_id'] = pid
          param.delete_if{|k,v|k == "description"}
          step["parameters_spec"] << param
        end

      end
    else
      step["parameters_spec"] = []
    end

    step.delete("parameters")

    return step
  end
  
  def add_step_by_hash(step)
    step = step.deep_dup
    step.delete("created_at")
    step.delete("updated_at")
    maphash = {}
    inp = step["input"].clone
    outp = step["output"].clone
    step["input"] = []
    step["output"] = []
    formats = {}
    inp.each do |format|


      if format.instance_of? String
        package = {:format => format}
      else
        package = format
        package[:format] = format["datatype"]
      end
      pid = get_package_id(format,0)

      if format["package_id"]
        maphash[format["package_id"]] = pid
      end

      package["package_id"] = pid
      if formats[format]
        formats[format] = formats[format] + 1
      else
        formats[format] = 1
      end
      step["input"] << package
    end
        
    outp.each do |format|
      if format.instance_of? String
        package = {:format => format}
      else
        package = format
        package["format"] = format["datatype"]
      end
      pid = get_package_id(format,0)
      if format["package_id"]
        maphash[format["package_id"]] = pid
      end
      package["package_id"] = pid
      step["output"] << package
    end

    return [step, maphash]
  end

  def executables
    executables = []
    if @modelhash["conversion_steps"]
      @modelhash["conversion_steps"].each do |stepper|
        executables += ProductDesign.executables_by_step(stepper) if !stepper.nil?
      end
    end
    return executables
  end

  def self.executables_by_step(step)
    executables = []
    executables << step["name"]
    if step["pre_improvement_steps"]
      step["pre_improvement_steps"].each do |impstep|
        executables << impstep["name"]
      end
    end
    if step["post_improvement_steps"]
      step["post_improvement_steps"].each do |impstep|
        executables << impstep["name"]
      end
    end
    return executables
  end

  def input_has_output(inputs,outputs)
    result = false
    inputs.each do |input|
      outputs.each do |output|
        result = true if output["datatype"] == input["datatype"]
      end
    end

    return result
  end
  
  def get_package_id(spec, step)
    maxseq= 0
    @modelhash["conversion_steps"].compact.each do |stepper|

      
      stepper["input"].each do |input|
        if input["datatype"] == spec["datatype"]
          curseq =  input["package_id"].scan(/[0-9]*$/)[0].to_i
          if curseq > maxseq
            maxseq = curseq
          end
        end
      end
      stepper["output"].each do |output|
        if output["datatype"] == spec["datatype"]
          curseq =  output["package_id"].scan(/[0-9]*$/)[0].to_i
          if curseq > maxseq
            maxseq = curseq
          end
        end
      end
      
      # check for in use allocatable parameters
      if stepper["pre_improvement_steps"]
        stepper["pre_improvement_steps"].each do |impstep|
          impstep["output"].each do |output|
            if output["datatype"] == spec["datatype"] && impstep["is_improvement_process"] == false &&
                !input_has_output(impstep["input"], impstep["output"])
              curseq =  output["package_id"].scan(/[0-9]*$/)[0].to_i
              if curseq > maxseq
                maxseq = curseq
              end
            end
          end

          impstep["parameters_spec"].each do |param|
            if param["allocate"] && param["allocate_tags"] == spec["datatype"]
              curseq =  param["package_id"].to_s.scan(/[0-9]*$/)[0].to_i
              if curseq > maxseq
                maxseq = curseq
              end
            end
          end
        end
      end

     
      if stepper["post_improvement_steps"]
        stepper["post_improvement_steps"].each do |impstep|
          impstep["output"].each do |output|
            if output["datatype"] == spec["datatype"] && impstep["is_improvement_process"] == false &&
                !input_has_output(impstep["input"], impstep["output"])
              curseq =  output["package_id"].scan(/[0-9]*$/)[0].to_i
              if curseq > maxseq
                maxseq = curseq
              end
            end
          end

          impstep["parameters_spec"].each do |param|
            if param["allocate"] && param["allocate_tags"] == spec["datatype"]
              curseq =  param["package_id"].to_s.scan(/[0-9]*$/)[0].to_i
              if curseq > maxseq
                maxseq = curseq
              end
            end
          end
        end
      end
    end

    new_package_id = spec["datatype"].to_s + "_" + (maxseq+1).to_s
    return new_package_id
  end
  
  def get_compiler_by_outputformat(outputformat)
    exes = Executable.find_by_outputformat(outputformat["type"])
      
    if exes.size == 0
      return exes[0]
    else
      raise "No output type specified - Can't calculate required compilation steps"
    end
  end
  
  def calculate_compiler_steps_new
    #pickup first required outputs format a calculate path
    if output_datatypes.size == 0
      raise "No output type specified - Can't calculate required compilation steps"
    end
    
    if input_datatypes.size == 0
      raise "No input type specified - Can't calculate required compilation steps"
    end
    
    #outputformat = output_datatypes[0]
    #inputformat  = input_datatypes[0]
    
    alternatives =[]
    
    
    output_datatypes.each do |outputformat|
      altindex = 0 # reset alternative id. all outputformats need same altid to be checked!!!         puts "Looking up outputformat " + outputformat.to_s
      exes = Executable.find_by_outputformat(outputformat["type"])
      while exes.size > 0
        exe = exes.pop
        next if exe.isimprovementprocess?

        if !exe.altid
          exe.altid = altindex
          altindex = altindex + 1
         
        end
        
        if !alternatives[exe.altid]
          alternatives[exe.altid] = []
        end
        
        if !(alternatives[exe.altid].index{|x| exe.name == x.name})
          #if  exe.name != "RDF2DH"
          alternatives[exe.altid] << exe
        
          #end
        end

        #rescursive check for path
        hasinputformat = false
        
        input_datatypes.each do |inputformat|
          hasinputformat = hasinputformat || exe.can_process_format?(inputformat)
        end
        
        if !hasinputformat
          inc = 1
          exe.preprocessors.each do |execp|
            if !(alternatives[exe.altid].index{|x| execp.name == x.name})
              execp.altid = exe.altid + inc
              alternatives[execp.altid] = alternatives[exe.altid].dup
             
              exes << execp
              inc = inc + 1
            end
          end
          
         
        end
      end
    end
    
    good_alternatives =[]
    alternatives.each do |alt|
      hasinputformat = false
      #next if !alt
      input_datatypes.each do |inputformat|
        hasinputformat = hasinputformat || alt[-1].can_process_format?(inputformat["type"])
      end
      good_alternatives << alt.reverse    if hasinputformat
    end
   
    #return largest number of steps
    return [good_alternatives.sort{|x,y| x.size <=> y.size}[-1]]
    
  end
  
  def simple_output
    
  end
  
  def calculate_compiler_steps
    #pickup first required outputs format a calculate path
    if output_datatypes.size == 0
      raise "No output type specified - Can't calculate required compilation steps"
    end
    
    if input_datatypes.size == 0
      raise "No input type specified - Can't calculate required compilation steps"
    end
    
    alternatives =[]
    
    output_datatypes.each do |outputformat|
      altindex = 0 # reset alternative id. all outputformats need same altid to be checked!!!         puts "Looking up outputformat " + outputformat.to_s
      exes = Executable.find_by_outputformat(outputformat["type"])
      while exes.size > 0
        alternativefound = false
        exe = exes.pop
        next if exe.isimprovementprocess?

        if !exe.altid
          exe.altid = altindex
          alternativefound = true
        end
        
        if !alternatives[exe.altid]
          alternatives[exe.altid] = []
        end
        
        if !(alternatives[exe.altid].index{|x| exe.name == x.name})
          if  exe.name != "RDF2DH"
            alternatives[exe.altid] << exe
        
          end
        end

        #rescursive check for path
        hasinputformat = false
        
        input_datatypes.each do |inputformat|
          hasinputformat = hasinputformat || exe.can_process_format?(inputformat)
        end
        
        if !hasinputformat
          exe.preprocessors.each do |execp|

            execp.altid = exe.altid + 1
            if !(alternatives[exe.altid].index{|x| execp.name == x.name})
              exes << execp  if execp.name != "RDF2DH"
            end
          end
        end
        
        altindex = altindex + 1 if alternativefound
      end

    end
    alternatives.each do |alt|
        
      hasinputformat = false
      next if !alt
      input_datatypes.each do |inputformat|
        hasinputformat = hasinputformat || alt[-1].can_process_format?(inputformat)
      end
      if hasinputformat
        #inputformat not reached -> cannot use this toolchain
        alternatives.delete(alt)
      else
        alt.reverse!
      end
    end
    
    return alternatives
  end
  
  def add_parameters_to_step(step)
    self.product_features.each do |feature|
      feature.system_steps.each do |transstep|
        # add parameters
        if transstep["type"] == "PARAMETER" && transstep["executable"] == step["name"]
          param = {"parameter_name"  => transstep["parameter_name"], "parameter_value" => transstep["parameter_value"]}
          step["parameters"] << param
        end
      end
    end
  end
  
  def add_improvements_to_step(step)
    self.product_features.each do |feature|
      feature.system_steps.each do |transstep|
        # add parameters
        if transstep["type"] == "PROCESS"
          exe = Executable.find_by_name(transstep["executable"]).use_latest_baseline
          step["output"].each do |output_f|
            if exe.can_process_format?(output_f[:format]) && exe.can_generate_format?(output_f[:format])
              # puts "required improvement detected!"
              step["post_improvement_steps"] = [] if !step["post_improvement_steps"]
              step["post_improvement_steps"] << {"name" => exe.name, "description" => exe.description, "input" => exe.input, "output" => exe.output, "id" => exe.id, "version" => exe.version, "parameters" =>transstep["parameters"]}
            end
          end
        end
      end
    end
    return step
  end
  
  def output_datatypes
    return @modelhash["output_datatypes"]
  end
  
  def input_datatypes
    return @modelhash["input_datatypes"]
  end

  def self.cannot_be_used_as_input_msg(object, project_id, recreate_package_ids, connection, force_create_id)
    if object.project_id != project_id
      result = " (skipped matching #{object.class} #{object.id} because it belongs to project #{Project.find(object.project_id).name})"
    elsif recreate_package_ids.include?(connection["source_spec"]["package_id"]) && object.conversion.force_create_id != force_create_id
      result = " (skipped matching #{object.class} #{object.id} because it should be force recreated)"
    else
      result = nil
    end
    return result
  end

  def self.allocate_half_product(step, connection , idtags, strategy_arg, production_orderline, branch, recreate_package_ids)
    src_package_id = connection["source_spec"]["package_id"]
    project_id = production_orderline.production_order.project_id
    strategy = strategy_arg || {:strategy => "best match"} # not used in this def
    cd = nil
    errors = []
    cannot_be_used_msg = ""
    only_no_conversion_database_found_errors = true
    #coverage = production_orderline.product.coverage
    pml = nil
    if connection["target_spec"]["exe"]
      tagmodel = connection["source_spec"]["tag_model"]
      tagmodel["branch"] = branch

      cdall = ConversionDatabase.find_by_tagmodel(tagmodel).where(:db_type => connection["source_spec"]["format"]).available
      # filter dbs with more filling then required
      cd = []
      cdall.each do |result|
        if result.tagmodel["filling"].size == tagmodel["filling"].size
          if msg = self.cannot_be_used_as_input_msg(result, project_id, recreate_package_ids, connection, production_orderline.production_order_id)
            cannot_be_used_msg += msg
          else
            cd << result
          end
        end
      end
      if cd && cd.size >= 1
        cd = cd.first
        connection["source_spec"]["datasets"] = [{}] if !connection["source_spec"]["datasets"]
        connection["source_spec"]["datasets"][0]["file_resources"] = [] if !connection["source_spec"]["datasets"][0]["file_resources"]
        connection["source_spec"]["datasets"][0]["file_match_perc"] = 100
        connection["source_spec"]["datasets"][0]["source"] = "CONVERSION DATABASE"
        connection["source_spec"]["datasets"][0]["conversion_database_id"] = cd.id.to_s
        connection["source_spec"]["datasets"][0]["allocation_status"] = "ALLOCATION SUCCESSFUL"
        filespec = {"file_id" =>  cd.path_and_file.gsub("/","").gsub(".","").gsub("@",""), "file_path" =>  cd.path_and_file, "coverage" => ["UNKNOWN"], "conversion_database_id" => cd.id, "tagmodel" => tagmodel}
        FileResourceUtil.add_file_resource_to_list(connection["source_spec"]["datasets"][0]["file_resources"], filespec)
      else
        if cd.size == 0
          raise "NO CONVERSION DATABASE FOUND for " + tagmodel.inspect + cannot_be_used_msg
        end

        if cd.size > 1
          raise "NO UNIQ DATA MATCH for " + tagmodel.inspect + cannot_be_used_msg
        end
      end
    else
      # with dhive - take coverage into account for allocating
      # because we create dhive on region (country) level
      regions = {}
      matchby = []
      if (step["name"].downcase == "dh2ndh" || step["name"].downcase == "ndh2nds") && production_orderline.production_order.allocate_by_update_region_yn
        #allocate by update region
        production_orderline.region_mapping_hash.each do |k,v|
          v.each do |updateregion|
            regions[updateregion] = [] if !regions[updateregion]
            regions[updateregion] << k[1].downcase # region - ignore filling k[0] and data_type k[2]
          end
        end
      else
        #single region dhive - > use coverage_required
        #regs = coverage.regions
        #regs.each do |region|
        #  regions[region.name] = [region.region_code.downcase]
        #end
        tagmodel = connection["source_spec"]["tag_model"]
        raise "Tagmodel not found for " + connection.inspect + " production orderline " + production_orderline.id.to_s if !tagmodel
        if tagmodel["filling"]
          if tagmodel["filling"].include?("core")
            # get regions from core
            regions =  {}
            tagmodel["core_coverage_required"].map{|x|x.values}.flatten.each do |region|
              regions[region] = [region]
            end
          else
            #assume first filling dataset has full coverage needed
            filling = tagmodel["filling"][0]
            regions = {}
            tagmodel["#{filling}_coverage_required"].map{|x|x.values}.flatten.each do |region|
              regions[region] = [region]
            end
          end
        else
          raise "Filling in tagmodel not found for " + connection.inspect + " production orderline " + production_orderline.id.to_s
        end

      end

      tagmodel = connection["source_spec"]["tag_model"]
      tagmodel["branch"] = branch
      raise "Tagmodel not found for " + connection.inspect + " production orderline " + production_orderline.id.to_s if !tagmodel

      regions.each do |name,region_code|
        begin
          lookuptags = idtags.clone << region_code
          if connection["source_spec"] && connection["source_spec"]["originating_format"]
            lookuptags << connection["source_spec"]["originating_format"]
          end
          cd = []
          subregions = []
          total_dbs_needed = 0
          selected_conversion = nil
          matchby = region_code
          pre_dbs = ConversionDatabase.where(:db_type => connection["source_spec"]["format"]).order("id desc").available
          xtra_dbs = []
          tagmodel_for_find =  tagmodel.clone
          tagmodel_for_find["coverage"] = region_code

          # create a filtered tagmodel for searching
          tagmodel["filling"].each do |fill|
            region_hit = []
            if tagmodel_for_find[fill].size > 1
              tagmodel_for_find[fill] = []
              tagmodel_for_find["#{fill}_coverage_required"] = []
              tagmodel["#{fill}_coverage_required"].each do |fcr|
                regions = fcr.map {|k,v|v.uniq}.flatten
                if regions.join().downcase == region_code.join().downcase
                  if region_hit == region_code.join
                    xtra_tagmodel_for_find = tagmodel.clone
                    xtra_tagmodel_for_find["#{fill}_coverage_required"] = [fcr]
                    xtra_tagmodel_for_find[fill] = [fcr.keys]
                    xtra_tagmodel_for_find[fill] = xtra_tagmodel_for_find[fill].flatten
                    xtra_tagmodel_for_find["#{fill}_coverage_required"] = xtra_tagmodel_for_find["#{fill}_coverage_required"].map {|x|x.map {|k,v|v.uniq}}.flatten
                    xtra_dbs = xtra_dbs + ConversionDatabase.find_by_tagmodel_and_coverage(pre_dbs, xtra_tagmodel_for_find, region_code)
                  else
                    region_hit = region_code.join
                    tagmodel_for_find["#{fill}_coverage_required"] << fcr
                    tagmodel_for_find[fill] << fcr.keys
                    tagmodel_for_find[fill] = tagmodel_for_find[fill].flatten
                  end
                end
              end
            end
            tagmodel_for_find["#{fill}_coverage_required"] = tagmodel_for_find["#{fill}_coverage_required"].map {|x|x.map {|k,v|v.uniq}}.flatten
          end

          dbs = ConversionDatabase.find_by_tagmodel_and_coverage(pre_dbs, tagmodel_for_find, matchby)
          dbs = xtra_dbs + dbs

          if dbs.size >= 1
            dbs.each do |db|
              if db.tagmodel["filling"].size == tagmodel["filling"].size && db.tagmodel["coverage"].size == matchby.size
                if msg = self.cannot_be_used_as_input_msg(db, project_id, recreate_package_ids, connection, production_orderline.production_order_id)
                  cannot_be_used_msg += msg
                else
                  cd << db
                  if db.group_amount.to_i > 1
                    if !selected_conversion
                      selected_conversion = db.conversion_id
                      total_dbs_needed = db.group_amount
                    end
                    if db.conversion_id == selected_conversion
                      subregions << db.tagmodel["subregion"].to_s
                    end
                  else
                    break
                  end
                end
              end
            end
            if selected_conversion && subregions.uniq.size != total_dbs_needed
              cd = []
              # conversion/subregions not complete
            end
          end
          if cd.size == 0
            total_dbs_needed = 0
            selected_conversion = nil
            # fallback to tagmodel coverage (in case per area/region is not available)
            matchby = tagmodel["coverage"]
            dbs = ConversionDatabase.where(:db_type => connection["source_spec"]["format"]).available
            match_dbs = ConversionDatabase.find_by_tagmodel_and_coverage(dbs, tagmodel, matchby)
            selected_dbs = []
            match_dbs.each do |db|
              msg = self.cannot_be_used_as_input_msg(db, project_id, recreate_package_ids, connection, production_orderline.production_order_id)
              if msg
                cannot_be_used_msg += msg
              else
                selected_dbs << db
              end
            end
            dbs = selected_dbs
          end

          parent_fallback = false
          parent_region = nil
          if cd.size == 0 && dbs.size == 0
            subregions = []
            region = Region.find_by_region_code(region_code[0])
            if region.class.name == "Area" && region.parent
              parent_region = region.parent
              parent_fallback = true
            end
            
            total_dbs_needed = 0
            selected_conversion = nil
            single_subregion = false
            # fallback to single subregion coverage
            if parent_fallback
              
              matchby = [parent_region.region_code]
              dbs = ConversionDatabase.where(:db_type => connection["source_spec"]["format"]).available
              dbs = ConversionDatabase.find_by_tagmodel_and_coverage(dbs, tagmodel, matchby)
              newdbs = []
              if dbs.size > 0
                #filter by subregion
                dbs.each do |db|
                  if db.tagmodel["subregion"] == region_code
                    if msg = self.cannot_be_used_as_input_msg(db, project_id, recreate_package_ids, connection, production_orderline.production_order_id)
                      cannot_be_used_msg += msg
                    else
                      newdbs << db
                    end
                  end
                end
              end
              dbs = newdbs
            end
          end

          if cd.size == 0 && dbs.size >= 1
            dbs.each do |db|
              # second condition is workaround for RDB which has only core-filling
              if ( db.tagmodel["filling"].size == tagmodel["filling"].size  && db.tagmodel["coverage"].size == matchby.size ) || ( db.db_type == 'RDB' && db.tagmodel["filling"].to_s == 'core' && db.tagmodel["coverage"].size == matchby.size )
                if msg = self.cannot_be_used_as_input_msg(db, project_id, recreate_package_ids, connection, production_orderline.production_order_id)
                  cannot_be_used_msg += msg
                else
                  cd << db
                  if db.group_amount.to_i > 0
                    if !selected_conversion
                      selected_conversion = db.conversion_id
                      if single_subregion
                        total_dbs_needed = 1
                      else
                        total_dbs_needed = db.group_amount 
                      end
                    end
                    if db.conversion_id == selected_conversion
                      subregions << db.tagmodel["subregion"].to_s
                    end
                  else
                    break
                  end
                end
              end
            end
            
            if subregions.uniq.size != total_dbs_needed
              cd = []
              # conversion/subregions not complete
            end
          end
          connection["file_resources"] = [] if !connection["file_resources"]
          cd.uniq!

          if ((cd && cd.size == 1) || (cd && cd.size >= 1 && total_dbs_needed >= 1))
            connection["source_spec"]["datasets"] = [{}] if !connection["source_spec"]["datasets"]
            connection["source_spec"]["datasets"][0]["file_resources"] = [] if !connection["source_spec"]["datasets"][0]["file_resources"]
            connection["source_spec"]["datasets"][0]["file_match_perc"] = 100
            connection["source_spec"]["datasets"][0]["source"] = "CONVERSION DATABASE"
            connection["source_spec"]["datasets"][0]["conversion_database_id"] = cd.first.id.to_s
            connection["source_spec"]["datasets"][0]["coverage"] = matchby
            cd.each do |file|
              filespec = {"file_id" =>  file.path_and_file.gsub("/","").gsub(".","").gsub("@",""), "file_path" =>  file.path_and_file, "coverage" => matchby,  "conversion_database_id" => file.id, "tagmodel" => file.tagmodel}         
              FileResourceUtil.add_file_resource_to_list(connection["source_spec"]["datasets"][0]["file_resources"], filespec)
            end
          else
            exceptions = AllocationException.where("1=1")
            if AllocationException.find_by_tagmodel_and_coverage(exceptions, tagmodel,[region_code]).size > 0
              next
            end
            if !cd || cd.size == 0
              if tagmodel["filling"].index("core")
                raise NoConversionDatabaseFoundError, "No #{connection["source_spec"]["format"]} Conversion Database found for "  + tagmodel["core"].join(" ").gsub("-", " " ).upcase + " Coverage : " + region_code.join(" ").to_s.upcase + " Branch : " + tagmodel["branch"].to_s
              else
                raise NoConversionDatabaseFoundError, "No #{connection["source_spec"]["format"]} Conversion Database found for "  + tagmodel.inspect + " Coverage : " + region_code.join(" ").to_s.upcase + " Branch : " + tagmodel["branch"].to_s
              end
            end

            raise "NO UNIQ CONVERSION DATABASE FOUND for #{connection["source_spec"]["format"].to_s}" + tagmodel.inspect + " Coverage : " + region_code.to_s.upcase + " Branch : " + tagmodel["branch"].to_s + "(#{cd.size.to_s} total needed #{total_dbs_needed}) (" + cd.map{|x|x.id.to_s}.join(" ") + ")"
          end
        rescue => e
          errors << e.message + cannot_be_used_msg
          only_no_conversion_database_found_errors &&= e.class == NoConversionDatabaseFoundError
        end
      end

      if errors.size > 0
        if only_no_conversion_database_found_errors
          raise NoConversionDatabaseFoundError, errors.join("\r\n")
        else
          raise errors.join("\r\n")
        end
      end
    end
    connection["allocation_status"] = "ALLOCATION SUCCESSFUL"
  end
  
  def self.allocate_process_data(step,connection, dataset , strategy_arg, production_orderline)
    strategy = strategy_arg || {:strategy => "best match"}
    idtags = []
    idtags << dataset["supplier_name"].to_s.downcase
    idtags << dataset["datatype"].to_s.downcase
    idtags << dataset["area_name"].to_s.downcase

    idtags << dataset["data_release"].to_s.downcase
    idtags << dataset["filling"].to_s.downcase if !dataset["filling"].blank?

    # Find in resources a data set with poi_mapping_layout attached
    data_set = nil
    dataset["file_resources"].each do |resource|
      if resource['process_data_element_id'] && !resource['process_data_element_id'].blank?
        data_set = ProcessDataElement.find(resource['process_data_element_id']).data_set
        if !data_set.poi_mapping_layout
          next
        end
      else
        next
      end
    end

    return connection if idtags[0] == '' # skip connections with empty tags
    if connection["target_spec"]["name"] && connection["target_spec"]["name"].downcase == "poi_mapping_layout"
      # get & write integrated POI Mapping file
      if production_orderline.product
        pml= production_orderline.product.poi_mapping_layout
      elsif data_set && data_set.poi_mapping_layout
        pml= data_set.poi_mapping_layout
      end
      if !pml
        raise "No Poi Mapping File found, check product configuration"
      end
      filespec = {"file_id" => pml.id.to_s, "file_path" => 'tbd'}
      dataset["file_match_perc"] = 100
      dataset["source"] = "POI MAPPING LAYOUT"
      dataset["poi_mapping_layout_id"] = pml.id.to_s
      dataset["allocation_status"] = "ALLOCATION SUCCESSFUL"
      dataset["file_resources"] = [filespec]
      return connection
    end

    if production_orderline.production_order.project == Project.default && Rails.env != 'test'
      pd_all = DataSet.production.tagged_with(idtags)
    else
      pd_all = DataSet.where("data_sets.status < #{DataSet::STATUS_REMOVED}").tagged_with(idtags)
    end

    pd = []
    if pd_all.size > 1
      pd_all.each do |ds|
        if ds.tag_list.size == idtags.size
          pd << ds
        end
      end
    else
      pd = pd_all
    end

    sidefileexe = false
    if pd.size == 1
      dataset["file_match_perc"] = 100
      dataset["source"] = "DATA DEVELOPMENT"
      dataset["data_development_id"] = pd[0].id
      dataset["data_development_version"] = "v4" # to be determined
      dataset["allocation_status"] = "ALLOCATION SUCCESSFUL"
      elements = pd.first.process_data_elements
      if strategy[:strategy] == "best match"
        list = []
        if connection["target_spec"]["exe"]
          # side file parameter lookup exe
          exe = Executable.find_by_name(connection["target_spec"]["exe"])
          sidefileexe= true
        else
          exe = Executable.find_by_name(step["name"])
        end
            
        if exe
          elements.each do |element|
            next if element.removed_yn || element.process_data_specification.do_not_process_yn
            path = element.full_directory
            if exe.source_data_def_type && exe.source_data_def_type == "FILE" && File.directory?(path) && !sidefileexe
              # check if directory
              if element.process_data_specification.split_by_key == "SubRegion"
                paths =  Dir[path + '/**'].reject {|fn| !File.directory?(fn) }
                if paths.size == 0
                  raise "No subregion dirs found for element #{element.id.to_s} #{path}"
                end
              else
                paths = [path]
              end
              paths.each do |path|
                files =  Dir[File.join(path,'/**')].reject {|fn| File.directory?(fn) }
                raise "No files found at dir #{path} for process data #{pd.first.id}, check the source data or specification" if files.size == 0
                files.each do |file_path|
                  filespec = {"file_id" => file_path.gsub("/","").gsub(".","").gsub("@",""), "file_path" => file_path.gsub("//","/")}
                  filespec["tags"] =  element.process_data_specification.script_tag.to_s.strip.split(',') if element.process_data_specification.script_tag.to_s.size > 0
                  if !element.process_data_specification.include_in_all_regions.nil?
                    filespec["include_in_all_regions"] =  element.process_data_specification.include_in_all_regions
                  else
                    filespec["include_in_all_regions"] =  false
                  end
                  filespec["process_data_element_id"] = element.id
                  FileResourceUtil.add_file_resource_to_list(list,filespec)
                  if !filespec["data_set_id"]
                    filespec["data_set_id"] = []
                  end
                  filespec["data_set_id"] << element.data_set.id

                  if element.process_data_specification.split_by_key == "SubRegion"
                    filespec["subregion"] = [path.scan(/\/([^\/]*)$/).flatten[0].to_s]
                  end
                  
                  if element.split_by_value && element.split_by_value != ""
                    if !filespec["system_tags"]
                      filespec["system_tags"] = []
                    end
                    if !filespec["coverage"]
                      filespec["coverage"] = []
                    end
                    filespec["system_tags"] << "SEPARATE_PROCESS"
                    filespec["coverage"] << element.split_by_value.to_s.downcase

                    if !filespec["group_by"]
                      filespec["group_by"] = []
                    end
                    if element.process_data_specification.split_by_key == "Region"
                      filespec["group_by"] << "coverage"
                    elsif element.process_data_specification.split_by_key == "ItemRegion"
                      filespec["group_by"] << "data_set_id"
                      filespec["group_by"] << "coverage"
                    elsif element.process_data_specification.split_by_key == "SubRegion"
                      filespec["group_by"] << "subregion"
                      filespec["group_by"] << "coverage"
                    end
                  else
                    region_code = element.data_set.region.region_code
                    filespec["coverage"] = [] if !filespec["coverage"]
                    filespec["coverage"] << region_code.downcase
                  end
                  if !filespec["coverage"] || filespec["coverage"].size == 0
                    raise "Coverage could not be determined for process data element " + element.id.to_s
                  end
                end
              end
            else
              paths = Dir[File.join(path,'/**')].reject {|fn| !File.directory?(fn) }
              if paths.size == 0
                raise "No dirs for element #{element.id.to_s} #{path}"
              end
              # TODO Not DRY see around 1771
              paths.each do |path|
                filespec = {"file_id" => path.gsub("/","").gsub(".","").gsub("@",""), "file_path" => path}
                filespec["tags"] =  element.process_data_specification.script_tag.to_s.strip.split(',') if element.process_data_specification.script_tag.to_s.size > 0
                if !element.process_data_specification.include_in_all_regions.nil?
                  filespec["include_in_all_regions"] =  element.process_data_specification.include_in_all_regions
                else
                  filespec["include_in_all_regions"] =  false
                end
                filespec["process_data_element_id"] = element.id
                if element.split_by_value && element.split_by_value != ""
                  if !filespec["system_tags"]
                    filespec["system_tags"] = []
                  end
                  filespec["system_tags"] << "SEPARATE_PROCESS"
                  if !filespec["coverage"]
                    filespec["coverage"] = []
                  end
                  filespec["coverage"] << element.split_by_value.to_s.downcase
                  if !filespec["group_by"]
                    filespec["group_by"] = []
                  end
                  if element.process_data_specification.split_by_key == "SubRegion"
                    filespec["subregion"] = [path.scan(/\/([^\/]*)$/).flatten[0].to_s]
                  end
                  filespec["group_by"] << "coverage"
                  if element.process_data_specification.split_by_key == "SubRegion"
                    filespec["group_by"] << "subregion"
                  end
                else
                  filespec["coverage"] = [] if !filespec["coverage"]
                  region_code = element.data_set.region.region_code
                  filespec["coverage"] << region_code.downcase
                end
                if !filespec["coverage"] || filespec["coverage"].size == 0
                  raise "Coverage could not be determined for process data element " + element.id.to_s
                end
                FileResourceUtil.add_file_resource_to_list(list,filespec)
              end
            end
          end
        end
        dataset["file_resources"] = list.uniq
      end
    else
      if pd.size == 0
        # data developer wants to distinguish between data not available on the system
        # and data not in the correct state, so check on availability and generate the corresponding exception message
        if DataSet.tagged_with(idtags).size > 0
          raise "DATA SET NOT IN STATUS #{DataSet.status_string(DataSet::STATUS_PRODUCTION)} for " + idtags.join(" ")
        else
          raise "NO DATA SET FOUND for " + idtags.join(" ")
        end
      end
          
      if pd.size > 1
        raise "NO UNIQ DATA MATCH for " + idtags.join(" ") + ". Found data sets #{pd.collect{|l|l.id}.join(' and ')}"
      end
    end
    return connection
  end

  def self.extended_tag(dataset)
    dataset["datatype"].to_s.downcase + "|" + dataset["supplier_abbr"].to_s.downcase + "|" + dataset["data_release"].downcase.to_s
  end

  def self.extended_coverage(coverage_required)
    result = []
    coverage_required.each do |x|
      if x.instance_of?(String)
        result << Region.find_by_region_code(x).region_code
      else
        result << Region.find(x["id"]).region_code
      end
    end
    return result
  end

  def self.merge_tagmodel(model1, model2)
    model_to_add = model2.clone
    model_result = {}
    model1.each do |k,v|
      model_result[k] = v
      if model_to_add[k]
        model_result[k] = (model_result[k] + model_to_add[k]).uniq.clone
        model_to_add.delete(k)
      end
    end

    model_to_add.each do |k,v|
      model_result[k] = v.clone
    end
    
    return model_result.clone
    
  end

  def self.get_tagmodel_from_conversion_step(conversion_step)
    tagmodel = {}
    # Unique signature to identify outcome for allocation
    tagmodel["executables"]   = []
    tagmodel["exe_signature"] = []
    tagmodel["executables"]   = ProductDesign.executables_by_step(conversion_step)
    tagmodel["exe_signature"] << Digest::MD5.hexdigest(tagmodel["executables"].join(','))
    conversion_step["connections"].each do |connection|
      if connection["source_spec"]["datasets"]
        # from process_data
        connection["source_spec"]["datasets"].each do |dataset|
          filling = dataset["filling"].to_s.downcase
          filling = "core" if filling == ""
          tagmodel["filling"] = [] if !tagmodel["filling"]
          tagmodel["filling"] << filling.to_s.downcase if !tagmodel["filling"].index(filling.to_s())
          tagmodel[filling + "_coverage"] = [] if !tagmodel[filling + "_coverage"]
          region_code = Region.active.find_by_name(dataset["area_name"]).region_code.downcase
          tagmodel[filling + "_coverage"] << region_code.downcase  if !tagmodel[filling + "_coverage"].index(region_code.downcase)
          tagmodel[filling] = [] if !tagmodel[filling]
          tagmodel[filling] << extended_tag(dataset) if !tagmodel[filling].index(extended_tag(dataset))
          
          if dataset["coverage_required"]
            tagmodel[filling + "_coverage_required"] = [] if !tagmodel[filling + "_coverage_required"]
            tagmodel[filling + "_coverage_required"] << {extended_tag(dataset) => self.extended_coverage(dataset["coverage_required"])}
          end
          
          tagmodel["coverage"] = [] if !tagmodel["coverage"]
          tagmodel["coverage"] << region_code.downcase  if !tagmodel["coverage"].index(region_code.downcase)
        end
      elsif connection["source_spec"]["tag_model"]
        tagmodel = self.merge_tagmodel(tagmodel,connection["source_spec"]["tag_model"])
      end
    end

    return tagmodel

  end

  def self.store_tag_model(object, model)
    contexts = []
    model.each do |k,v|
      next if k.to_s.match(/coverage_required$/) #
      if v[0] && v[0].instance_of?(String)
        contexts << "context_" + k.gsub(" ","_")
        object.set_tag_list_on("context_" + k.gsub(" ","_"), v)
      else
        v.each do |k2,v2|
          str = "context_" + k.gsub(" ","_") + "_sub_" + k2.to_a.flatten[0].gsub(" ","_").gsub("-","_").gsub(".","_dot_")
          str = str.gsub("|","_pipe_")
          contexts << str
          object.set_tag_list_on(str, k2.to_a[0][1])
        end
      end
      #object.save!
    end
    object.set_tag_list_on(:contexts, contexts)
    object.save!
  end

  def self.tagmodel(object,context_group=[])
    tagmodel = {}
    if context_group.empty?
      contexts = object.tag_list_on(:contexts)
    else
      contexts = context_group.collect{|c|"context_#{c}"}
    end
    contexts.each do |context|
      if context.match("_sub_")
        # nested tag
        arr = context.scan(/(.*)_sub_(.*)/)
        values = object.tag_list_on(context)
        values.map{|x|x.upcase!}
        nested_key = arr[0][0].gsub("context_","").gsub("_", " ").gsub(" coverage required","_coverage_required")
        tagmodel[nested_key] = [] if !tagmodel[nested_key]
        tagmodel[nested_key] << {arr[0][1].gsub("_dot_", ".").gsub("_pipe_","|").gsub("_","-") => values}
      else
        tagmodel[context.gsub("context_","").gsub("_", " ")] = object.tag_list_on(context)
      end
    end
    return tagmodel
  end

  def self.coverage_label_by_tagmodel(obj)
    tm = tagmodel(obj)
    if tm["coverage"]
      if tm["coverage"].size <= 6
        # Less then 6 is acceptable for views
        return tm["coverage"].sort.join(' ').upcase
      else
        parent_arr = []
        region_arr = []
        contin_arr = []
        tm["coverage"].each do |region_code|
          # Try to reduce with grouping by parents
          regionobj = Region.find_by_region_code(region_code)
          if regionobj
            parent = regionobj.parent
            if parent
              parent_arr << parent.region_code.upcase
            else
              # If no parent is setup
              region_arr << region_code.upcase
            end
            # Find continents in as region code
            contin_arr << region_code.upcase if  regionobj.ruby_type == "Continent"
          end
        end
        if ( parent_arr + region_arr ).uniq.size < 8
          return ( parent_arr + region_arr ).uniq.sort.join(' ')
        elsif !contin_arr.empty?
          return contin_arr.uniq.sort.join(' ')
        else
          # Nothing helps return full coverage string
          return tm["core coverage"].sort.join(' ').upcase
        end
      end
    else
      return 'UNKNOWN'
    end
  end

  def self.add_connection_to_step(step, datatype, file_path, database_id, tagmodel,script_tag="")
    connection = {}
    connection["target_sequence"] = "1"
    connection["source_id"] =  "process_data_1"
    connection["source_sequence"] = 1
    connection["target_id"] = "#{datatype}_1"
    connection["source_spec"] =  {}
    connection["source_spec"]["format"] = "GENERIC"
    connection["source_spec"]["source_id"] = "process_data_1"
    connection["source_spec"]["package_id"] = "process_data_1"
    connection["source_spec"]["sequence_generated"] = 0
    connection["source_spec"]["sequence"] = 1
    connection["target_spec"] = {}
    connection["target_spec"]["datatype"] = "#{datatype}"
    connection["target_spec"]["mandatory"] = false
    connection["target_spec"]["script_tag"] = script_tag
    connection["target_spec"]["format"] = "#{datatype}"
    connection["target_spec"]["package_id"] = "#{datatype}_1"
    connection["source_spec"]["datasets"] = [{}]
    connection["source_spec"]["datasets"][0]["file_resources"] = []
    connection["source_spec"]["datasets"][0]["file_match_perc"] = 100
    connection["source_spec"]["datasets"][0]["source"] = "CONVERSION DATABASE"
    connection["source_spec"]["datasets"][0]["conversion_database_id"] =  database_id
    connection["source_spec"]["datasets"][0]["allocation_status"] = "ALLOCATION SUCCESSFUL"
    filespec = {"file_id" =>  file_path.gsub("/","").gsub(".","").gsub("@",""), "file_path" => file_path, "coverage" => ["UNKNOWN"], "conversion_database_id" => database_id, "tagmodel" => tagmodel}
    if nolog != ""
      filespec["tags"] = [script_tag]
    end
    FileResourceUtil.add_file_resource_to_list(connection["source_spec"]["datasets"][0]["file_resources"], filespec)
    step["connections"] << connection
  end
  
  def self.is_nds_backend_step(step)
    if step["name"].to_s.downcase == "ndh2nds" || step["name"].to_s.downcase == "dh2ndh"
      return true
    else
      return false
    end
  end

  def self.allocate_files(model,strategy_arg, step_sequence_arg, production_orderline, branch)
    strategy = strategy_arg || {:strategy => "best match"}
    step_sequence = step_sequence_arg || nil
    data_matches = nil
    seq = 0
    idtags = nil
    errors = ""
    only_no_conversion_database_found_errors = true
    # the conversions which must be 'force recreated' have their force_create_id set to the current production order
    # by selecting their output package_ids we have a list of source package_ids which must be recreated
    recreate_package_ids = Conversion.where(:force_create_id => production_orderline.production_order_id).collect{|c| c.design_json["output"].collect{|x| x["package_id"]}}.flatten.uniq
    model["conversion_steps"].each do |step|
      

      if step_sequence && seq != step_sequence
        seq = seq + 1
        next
      end
      step["connections"].each do |connection|
        begin
          #connection["file_resources"] = []
        
          idtags = []
          idtags << connection["source_spec"]["supplier"].to_s
          idtags << connection["source_spec"]["format"].to_s
          idtags << connection["source_spec"]["area"].to_s
          idtags << connection["source_spec"]["release"].to_s
    
          idtags = idtags + connection["source_spec"]["tags"] if connection["source_spec"]["tags"]
        
          # try process data first
          #production status required
          isintermediate = false
          ds = DataType.where(:name => connection["source_spec"]["format"]).first
          isintermediate = ds.is_intermediate if ds
          if connection["source_spec"]["format"].to_s.upcase == "DHIVE" ||
              connection["source_spec"]["format"].to_s.upcase == "NDS" ||
              isintermediate
            # allocate on conversion_databases
            # need to determine types that end up in conversion_databases
            #include coverage
            connection["source_spec"]["datasets"] = [{}]
            allocate_half_product(step,connection,idtags,strategy, production_orderline, branch, recreate_package_ids)
          else
            next if connection["exclude"]
            connection["source_spec"]["datasets"].each do |dataset|
              dataset["file_resources"] = []
              dataset["allocation_status"] = "ALLOCATION RESET"
              allocate_process_data(step,connection, dataset ,strategy, production_orderline)
            end
          end

          next
        rescue => e
          errors = errors + e.message + "\r\n"
          only_no_conversion_database_found_errors &&= e.class == NoConversionDatabaseFoundError
        end
      end
      seq = seq +1
      if is_nds_backend_step(step)
        # add pid & redo if needed
        if production_orderline.product && production_orderline.product.predecessor
          begin
            # add rdo file input connection
            newconnection = step["connections"][0].clone
            #add_connection_to_step(step, newconnection, "RDO")
            rdodb = production_orderline.product.predecessor.rdo_database
            add_connection_to_step(step,"RDO", rdodb.path_and_file, rdodb.id, rdodb.tagmodel, "RDO")
            # add pid file input connection
            #add_connection_to_step(step, newconnection, "PID")
            newconnection = step["connections"][0].clone
            newconnection["target_spec"]["exe"] = "NDH2NDS"
            piddb = production_orderline.product.predecessor.pid_database
            add_connection_to_step(step, "PID", File.dirname(piddb.path_and_file), piddb.id, piddb.tagmodel, "PID")
          rescue => e
            errors = errors + e.message + "\r\n"
            only_no_conversion_database_found_errors &&= e.class == NoConversionDatabaseFoundError
          end
        end
      end
    end
 
    if errors.size > 0
      if only_no_conversion_database_found_errors
        raise NoConversionDatabaseFoundError, errors
      else
        raise errors
      end
    else
      return [model,data_matches]
    end
  end
  
  def self.find_matches(params)
    conditions = '1=1'
    conditions_string_ary = []
    conditions_param_values = []
    
    region = params[:search_region]
    unless region.blank?
      conditions_string_ary << ' and (regions.name = ?)'
      conditions_param_values << region
    end

    supplier = params[:search_supplier]
    unless supplier.blank?
      conditions_string_ary << " and (company_abbrs.ms_abbr_3 = ?)"
      conditions_param_values << supplier
    end

    release = params[:search_release]
    unless release.blank?
      conditions_string_ary << " and (data_releases.name = ?)"
      conditions_param_values << release
    end

    datatype = params[:search_datatype]
    unless datatype.blank?
      conditions_string_ary << " and (view_receptions_and_conversiondbs.datatype = ?)"
      conditions_param_values << datatype
    end
  
    unless params[:search].blank?
      conditions_string_ary << " and (view_receptions_and_conversiondbs.referenceid LIKE ?
                                     or view_receptions_and_conversiondbs.datatype LIKE ? 
                                       or data_releases.name LIKE ?
                                        or regions.name LIKE ?)"
      4.times do
        conditions_param_values << "\%#{params[:search].strip}\%"
      end
    end
    
    conditions = conditions + conditions_string_ary.join(" ")
    @conditions = conditions
    @conditions_param_values = conditions_param_values
    @assembly_lines = []
    
    @assembly_pickers = ViewReceptionsAndConversiondb.joins([{:data_release => :company_abbr}, :region]).where([conditions] + conditions_param_values).order("id desc")
    return @assembly_pickers
  end

  def self.rebuild_reception_tags
    recs = Reception.all
    count = 0
    recs.each do |reception|
      taglist = []
      if reception.data_release
        taglist << reception.data_release.company_abbr.ms_abbr_3
        taglist << reception.data_type.name
        taglist << reception.data_release.name
        taglist << reception.region.name
        reception.tag_list = taglist
        reception.save
        count = count + 1
      end
    end
  end
  
  def process_data_requirements
    requirements = []
    modelhash["conversion_steps"].each do |step|
      if step["connections"] && step["connections"].size > 0
        step["connections"].each do |connection|
          next if !connection
          if connection["source_id"].match("^process_data") || connection["source_id"].match("^data_set")
            requirements << connection
          end
        end
      end
    end
    return requirements
  end
   
  def self.add_status_message(model,type,message)
    model["process_message"] = [] if !model["process_message"]
    model["process_message"] << {:type => type, :text => message}
    return model
  end

  def has_conversion_steps?
    modelhash["conversion_steps"] && !modelhash["conversion_steps"].empty?
  end

  def product_features
    pfs = []
    if self.modelhash["features"]
      self.modelhash["features"].each do |f|
        pfs << ProductFeature.find_correct_version_by_name(f["name"],false)
      end
    end
    return pfs
  end

  alias_method :generic_valid_review?, :valid_review?
  def valid_review?
    generic_valid_review? &&
      name && name.length > 0 &&
      self.status != HandleVersioning.statusarray[HandleVersioning::STATUS_PENDING]
  end

  alias_method :generic_valid_approved?, :valid_approved?
  def valid_approved?
    generic_valid_approved? && valid_review? && self.status != HandleVersioning.statusarray[HandleVersioning::STATUS_PENDING] && self.has_conversion_steps?
  end

  alias_method :generic_use_latest, :use_latest
  def use_latest
    generic_use_latest
    @modelhash = JSON.parse(self.model)
    return self
  end

  alias_method :generic_use_latest_approved, :use_latest_approved
  def use_latest_approved
    generic_use_latest_approved
    @modelhash = JSON.parse(self.model)
    return self
  end
  
  def get_outputtags_from_connection(connection)
    tags = {}
    connection["source_spec"]["datasets"].each do |dataset|
      filling = "CORE"
      if dataset.filling && data.filling != ""
        filling = dataset.filling
      end
      tags[filling] = [] if !tags.filling
      tagshash = {}
      tagshash[:datatype] = dataset["datatype"]
      tagshash[:supplier] = dataset["supplier_name"]
      tagshash[:release] =  dataset["data_release"]
      tagshash[:area] =     dataset["area_name"]
      tags[filling] = tagshash
    end
    return tags
  end

  def get_output_tags_for_step(step)
    outputtags = []
    step["connections"].each do |connection|
      tags = get_outputtags_from_connection(connection)
      outputtags << tags
    end
    
  end

  def self.find_by_tagmodel_and_coverage(results, tagmodel, coverage)
    allresults = []
    tagcount = 0
    results = results.order("id desc")
    tagmodel.each do |k,v|
      next if k.to_s == "coverage"
      next if k.to_s.match(/coverage_required$/) #
      next if k.to_s.match(/_coverage$/) #
      next if k.to_s.match(/bundle_release$/) #
      next if k.to_s.match(/executables$/)
      results = results.tagged_with(v, :on => "context_" + k.gsub(" ","_"))
      tagcount = tagcount + 1
    end

    orgresults = results.clone
    coverage.each do |region|
      if tagcount >= 15 # workaround for 61 join limit on mysql
        if allresults == []
          allresults = results.all
        else
          allresults = allresults & results.all
        end
        tagcount = 0
        results = orgresults
        results.tagged_with(region, :on => "context_coverage")
      else
        results = results.tagged_with(region, :on => "context_coverage")
      end

      if allresults == []
        allresults = results.all
      else
        allresults = allresults & results.all
      end
      tagcount = tagcount + 1
    end

    # final filter to make sure filling and executable signatures are equal
    okresults = []
    allresults.each do |result|
      # TODO: Fix the 'exe_signature', 'exe signature' %#%$#$@
      if result.tagmodel["filling"].size == tagmodel["filling"].size && result.tagmodel["exe signature"].size == tagmodel["exe_signature"].size
        okresults << result
      end
    end
    return okresults
  end

  # Versioning methods
  # Although handle_versioning.rb is supposed to handle all versioning issues, for product_design we have an additional
  # status, 'INTERMEDIATE'. This status is used to handle automatic saves by the system. These automatic saves should not
  # be versioned. This is done by overriding the create_new_version? method of version_fu. This can't be easily done in
  # handle_versioning because you don't access to the version_fu methods there yet.
  alias_method :old_create_new_version?,  :create_new_version?
  def create_new_version?
    # create no new version for the intermediate status
    self.status != 'intermediate' && old_create_new_version?
  end

  def self.parameter_allocation_needed?(exe,name)
    executable = Executable.select("parameters, input, output").find_by_name(exe)
    if executable && !executable.parameters.blank?
      parameters_ar = JSON.parse(executable.parameters)["parameters"]
      index_i = parameters_ar.index(parameters_ar.take_while{|p| p["name"] != name }.last).to_i + 1
      if (index_i + 1) >= parameters_ar.size
        # Should be OK, but data not stable enough now
        #raise "#{name} not found for executable #{exe}"
        return false
      end
      match = parameters_ar[index_i]

      if match && match["allocate"] == nil || match["allocate"].blank? || match["allocate"] == false || match["allocate"] == false
        return false
      else
        return true
      end
    end
  end

  def product_feature_parameters
    return_hash = {}
    if self.modelhash['features']
      self.modelhash['features'].each do |feature|
        product_feature_code = feature["code"]
        product_feature = ProductFeature.find_by_abbr_code(product_feature_code)
        parameter_hash = {}
        product_feature.system_steps.each do |parameter|
          if ProductDesign.parameter_allocation_needed?(parameter["executable"], parameter["parameter_name"])
            parameter_hash[parameter["parameter_name"]] = '@ALLOCATE'
          else
            parameter_hash[parameter["parameter_name"]] = parameter["parameter_value"]
            value = parameter["parameter_value"]
            # Quote parameter value when spaces without quoting is used
            if value && !value.match(/\"|'/) && value.match(/\s/)
              value = "\"#{parameter["parameter_value"]}\""
            end
            parameter_hash[parameter["name"]] = value
          end
        end
        return_hash[product_feature.name] = parameter_hash
      end
    end
    return return_hash
  end

  def platform_feature_parameters
    return_hash = {}
    parameter_hash = {}
    if self.modelhash['product_line']["parameter_settings"]
      self.modelhash['product_line']["parameter_settings"].each do |parameter|
        if ProductDesign.parameter_allocation_needed?(parameter["exe"],parameter["name"])
          parameter_hash[parameter["name"]] = '@ALLOCATE'
        else
          value = parameter["value"]
          # Quote parameter value when spaces without quoting is used
          if !value.match(/\"|'/) && value.match(/\s/)
            value = "\"#{parameter["value"]}\""
          end
          parameter_hash[parameter["name"]] = value
        end
        return_hash[self.modelhash['product_line']["parameter_setting_name"]] = parameter_hash
      end
    end
    return return_hash
  end

  def md5_string(conversion_step)
    # if this is changed all the md5 hash needs to be recalculated for all
    # designs and IMPORTED designs
    template_arr = []
    
    # Product line & product parameters
    parameters = {}
    parameters = parameters.merge( platform_feature_parameters )
    parameters = parameters.merge( product_feature_parameters  )
    parameters.each do |feature,params|
      template_arr << "\n# #{feature.upcase}\n"
      params.each do |k,v|
        comment = ''
        if v == '@ALLOCATE'
          comment = '#'
        end
        template_arr << "#{comment}parameter #{k} #{v}\n"
      end
    end

    # Dakota PROCS
    template_arr << "setenv DAKOTAPROCS "
    if conversion_step["pre_improvement_steps"]
      conversion_step["pre_improvement_steps"].each do |step|
        template_arr << "\"#{step["name"]}\" "
      end
    end
    template_arr << "#{conversion_step["name"]} "
    if conversion_step["post_improvement_steps"]
      conversion_step["post_improvement_steps"].each do |step|
        template_arr << "\"#{step["name"]}\" "
      end
    end
    template_arr << "\n"
    return template_arr.join()
  end

  def dakota_template_from_conversion_step(conversion_step)
    template_arr = []
    template_arr << "\n# PRODUCT DESIGN #{self.id} #{self.name} #{self.status} #{self.version}\n"
    template_arr << "# CONVERSION STEP: #{modelhash["conversion_steps"].index(conversion_step)} #{conversion_step["name"]}\n"

    # Product line & product parameters
    parameters = {}
    parameters = parameters.merge( platform_feature_parameters )
    parameters = parameters.merge( product_feature_parameters  )

    parameters.each do |feature,params|  
      template_arr << "\n# #{feature.upcase}\n"
      params.each do |k,v|
        comment = ''
        if v == '@ALLOCATE'
          comment = '#'
        end
        template_arr << "#{comment}parameter #{k} #{v}\n"
      end
    end

    # Dakota PROCS
    template_arr << "setenv DAKOTAPROCS "
    if conversion_step["pre_improvement_steps"]
      conversion_step["pre_improvement_steps"].each do |step|
        template_arr << "\"#{step["name"]}\" "
      end
    end
    template_arr << "#{conversion_step["name"]} "
    if conversion_step["post_improvement_steps"]
      conversion_step["post_improvement_steps"].each do |step|
        template_arr << "\"#{step["name"]}\" "
      end
    end
    template_arr << "\n"
    return template_arr.join()
  end

  def api_template_hash
    return_hash = {}
    return_hash['name'] = self.name
    return_hash['description'] = self.description
    return_hash['version'] = self.version
    return_hash['status'] = self.status
    return_hash['region'] = ''
    return_hash['region'] = self.region.name if self.region
    return_hash['data_supplier'] = ''
    return_hash['data_supplier'] = self.company_abbr.company_name if self.company_abbr
    return_hash['data_release'] = ''
    return_hash['data_release'] = self.data_release.name if self.data_release
    return_hash['conversion_steps'] = {}
    if self.modelhash["conversion_steps"]
      step_hash = {}
      c= 0
      self.modelhash["conversion_steps"].each do |step|
        c = modelhash["conversion_steps"].index(step)
        step_hash[c] = {:name => step['name'],
          :description => step['description'],
          :input => step['input'].collect{|i|i['datatype']}.join(', '),
          :output => step['output'].collect{|i|i['datatype']}.join(', '),
          :api_dakota_template_url => url_for(:action => :sub_template,:controller => :product_designs,:step=>c,:only_path => false,:id=>self.id,:host=>ENV["host"]),
          :dakota_template => self.dakota_template_from_conversion_step(step)
        }

      end      
    end
    return_hash['conversion_steps'] = step_hash
    return return_hash
  end

  def self.find_correct_version(id, use_test_versions_yn, checklog)
    if id.nil?
      checklog.add_error_line("No product design specified (id is nil).")
      checklog.update_result(Checklog::STATUS_EXCEPTION)
      return nil
    end

    # id is not nil
    pd = ProductDesign.find(id)
    if pd.nil?
      checklog.add_error_line("No product design found with id '#{id}'.")
      checklog.update_result(Checklog::STATUS_EXCEPTION)
      return nil
    end

    # we have a product design
    if pd.obsolete?
      # the product design is obsolete
      if use_test_versions_yn
        # you use test versions to test designs before finalizing and approving them. Such versions should not use an obsolete design
        checklog.add_error_line("Product design '#{id}' ('#{pd.name}') is obsolete.")
        checklog.update_result(Checklog::STATUS_EXCEPTION)
        return nil
      elsif pd.latest_approved
        # although obsolete, the latest approved version is still valid
        checklog.add_info_line("Product design '#{id}' ('#{pd.name}') is now obsolete. Using the latest approved version '#{pd.latest_approved.version}'.")
        return pd.use_latest_approved
      else
        # the product design has never been approved and is now obsolete
        checklog.add_error_line("Product design '#{id}' ('#{pd.name}') has never been approved and is now obsolete.")
        checklog.update_result(Checklog::STATUS_EXCEPTION)
        return nil
      end
    end

    # we have a non-obsolete product design
    if use_test_versions_yn
      checklog.add_debug_line("Using product design '#{id}' ('#{pd.name}') version '#{pd.version}' with status '#{pd.status}'.")
      return pd
    elsif pd.status == HandleVersioning.statusarray[HandleVersioning::STATUS_APPROVED]
      checklog.add_debug_line("Using the latest version ('#{pd.version}) of product design '#{id}' ('#{pd.name}'), with status '#{pd.status}'.")
      return pd
    elsif pd.latest_approved
      checklog.add_debug_line("Use the latest approved version ('#{pd.latest_approved.version}') of product design '#{id}' ('#{pd.name}'). The current version is '#{pd.version}'' and has status '#{pd.status}'.")
      return pd.use_latest_approved
    else
      checklog.add_error_line("Product design '#{id}' ('#{pd.name}') has not been approved yet. Approve it first before proceeding.")
      checklog.update_result(Checklog::STATUS_EXCEPTION)
      return nil
    end
  end

  def calc_md5
    stringrep = ""
    if self.modelhash["conversion_steps"]
      self.modelhash["conversion_steps"].compact.each do |step|
        stringrep = stringrep + md5_string(step)
      end
    end
    md5 =  Digest::MD5.hexdigest(stringrep)
    return md5
  end
      
  def import_design(source)
    source.modelhash["conversion_steps"] = [] if !source.modelhash["conversion_steps"]
    self.modelhash["conversion_steps"] = [] if !self.modelhash["conversion_steps"]
    source_steps = source.modelhash["conversion_steps"]
    if !source.product_design_md5
      md5 = source.calc_md5
    else
      md5 = source.product_design_md5
    end

    source.modelhash["conversion_steps"].each do |step|
      step["used_product_design_md5"] = md5
      step["design_name"] = source.name
      step["product_design_id"] = source.id
      step["used_product_design_version"] = source.version+1
    end

    dest_steps = self.modelhash["conversion_steps"]
    offset = dest_steps.size
    overall_mappings = {}
    source_steps.each do |step|
      offset += 1
      stepresult = add_step_by_hash(step)
      dest_steps << stepresult[0]
      overall_mappings.merge!(stepresult[1])
      remap_connections(dest_steps[-1], overall_mappings, offset)
    end

    self.modelhash["conversion_steps"] = dest_steps
  end

  def get_step(md5, inputs, type="input")
    matchdetails = {}
    seq = 0
    packageid_mapping = {}
    found = false
    self.modelhash["conversion_steps"].each do |step|
      next if !step
      if step["used_product_design_md5"] == md5
        inputs.each do |input|
          step[type].each do |input_self|
            if input_self["datatype"] == input["datatype"] # add criteria can be added here
              packageid_mapping[input_self["package_id"]] = input["package_id"]
              matchdetails = {:step =>  seq, :package_mapping => packageid_mapping}
              found = true
            end
          end
        end
      end
      seq = seq +1
    end
    if !found
      raise "no match on md/datatype for #{md5} design id::: #{self.id.to_s}"
      # do not sync for now
    end
    return matchdetails
  end

  def remap_connections(step,overall_mappings,offset)
    if step["connections"]
      step["connections"].each do |connect|
        connect["source_spec"]["package_id"] = overall_mappings[connect["source_spec"]["package_id"]] if overall_mappings[connect["source_spec"]["package_id"]]

        connect["source_id"] = overall_mappings[connect["source_id"]] if overall_mappings[connect["source_id"]]
        connect["target_id"] = overall_mappings[connect["target_id"]] if overall_mappings[connect["target_id"]]

        # needed for draw in/out lines
        connect["target_sequence"] = offset

        if connect["source_spec"]["package_id"].match("^process_data") || connect["source_spec"]["package_id"].match("^data_set")
          connect["source_spec"]["package_id"] = "process_data_" + offset.to_s
          connect["source_spec"]["source_id"] = "process_data_" + offset.to_s
          connect["source_id"] = "process_data_" + offset.to_s
        end
        connect["target_spec"]["package_id"] = overall_mappings[connect["target_spec"]["package_id"]] if overall_mappings[connect["target_spec"]["package_id"]]
      end
    end
  end

  def synchronize_design(source, md5)
    begin
      if !self.modelhash["conversion_steps"] || self.modelhash["conversion_steps"].size == 0 ||
          self.design_type == "frontend" || source.design_type == "backend"
        return false
      end
      if source.modelhash["conversion_steps"].size == 0
        raise "This design is used in backend compilation - make sure there is at lease one conversion step in the design"
      end
      inputspec = source.modelhash["conversion_steps"][0]["input"]
      outputspec = source.modelhash["conversion_steps"][-1]["output"]
      olddesign  = @modelhash["conversion_steps"]
      startstep = get_step(md5, inputspec)
      endstep = get_step(md5,outputspec,"output")
      startstep_idx = startstep[:step]
      endstep_idx = endstep[:step]

      if !startstep_idx || !endstep_idx
        # accept for now - it will just not be synced
        raise "Imported original design not found in #{self.id.to_s} :: source id - " + source.id.to_s + " md5 :: " + source.modelhash["conversion_steps"][0]["name"] + " steps #{source.modelhash['conversion_steps'].size} md5hash :: " + md5.to_s + " target id" + self.id.to_s + " " + self.modelhash["conversion_steps"].map{|x|x["name"].to_s + " " + x["used_product_design_md5"].to_s}.inspect
        return @modelhash
      end
      
      overall_mappings = {}
      #newdesign["conversion_steps"] =  self.modelhash["conversion_steps"][0..startstep_idx-1]
      
      if startstep_idx == 0
        # empty steps if startstep = 0
        @modelhash["conversion_steps"] = []
      else
        @modelhash["conversion_steps"] = olddesign[0..startstep_idx-1]
      end
      newmd5 = source.calc_md5
      
      source.modelhash["conversion_steps"].each do |step|
        stepresult = add_step_by_hash(step)
        overall_mappings.merge!(stepresult[1])
        stepresult[0]["used_product_design_md5"] = newmd5
        stepresult[0]["design_name"] = source.name
        stepresult[0]["used_product_design_version"] = source.version
        @modelhash["conversion_steps"] << stepresult[0]
        remap_connections(@modelhash["conversion_steps"][-1], overall_mappings, @modelhash["conversion_steps"].size)
      end

      ending_steps = []
      olddesign[endstep_idx+1..-1].each do |st|
        ending_steps << st.deep_dup
      end

      ending_steps.each do |step|
        stepresult = add_step_by_hash(step)
        @modelhash["conversion_steps"] <<  stepresult[0]
        overall_mappings.merge!(stepresult[1])
        remap_connections(@modelhash["conversion_steps"][-1], overall_mappings, @modelhash["conversion_steps"].size)
      end


      # update used_designs
      # obsolete existing version
      used_designs = self.used_product_designs.where("used_product_design_md5 = '#{md5}' AND status <> #{UsedProductDesign::STATUS_OBSOLETE}")
      used_designs.each do |ud|
        ud.version_active_until = self.version
        ud.status = UsedProductDesign::STATUS_OBSOLETE
        ud.save!
      end
      
      upd = UsedProductDesign.new
      upd.product_design_id = self.id
      upd.used_product_design_id = source.id
      upd.used_product_design_md5 = newmd5
      upd.status = UsedProductDesign::STATUS_ACTIVE
      self.used_product_designs << upd
      self.save!
      upd.version_active_from = self.version
      upd.save!

      if self.status != HandleVersioning.statusarray[HandleVersioning::STATUS_DRAFT] || self.status != HandleVersioning.statusarray[HandleVersioning::STATUS_OBSOLETE]
        self.status = HandleVersioning.statusarray[HandleVersioning::STATUS_PENDING]
      end
      
      return true
    rescue => e
      puts "Cannot synchronize design " + e.message
      puts e.backtrace
      raise e
    end
  end

  def has_design_step(md5,model)
    found = false
    model["conversion_steps"].each do |step|
      if step && step["used_product_design_md5"] && step["used_product_design_md5"] == md5
        found = true
        break
      end
    end
    return found
  end

  def product_line
    # ProductLine is not a active record association
    if !(@modelhash["product_line"] && !@modelhash["product_line"]["id"].blank?)
      return nil
    else
      return ProductLine.find(@modelhash["product_line"]["id"])
    end
  end

  def obsolete_and_add_missing_imported_designs(newmodel)
    used_designs = self.used_product_designs.where("status <> #{UsedProductDesign::STATUS_OBSOLETE}")
    ProductDesign.transaction do
      used_designs.each do |ud|
        if !has_design_step(ud.used_product_design_md5,newmodel)
          ud.version_active_until = self.version
          ud.status = UsedProductDesign::STATUS_OBSOLETE
          ud.save!
        end
      end

      newmodel["conversion_steps"].each do |step|
        if (step && step["used_product_design_md5"])
          if !self.used_product_designs.where("used_product_design_md5 = '#{step["used_product_design_md5"]}' AND status <> #{UsedProductDesign::STATUS_OBSOLETE}")[0]
            upd = UsedProductDesign.new
            upd.product_design_id = self.id
            raise "Product Design id missing " if !step["product_design_id"]
            upd.used_product_design_id = step["product_design_id"]
            upd.used_product_design_md5 = step["used_product_design_md5"]
            upd.status = UsedProductDesign::STATUS_ACTIVE
            self.used_product_designs << upd
            self.save!
            upd.version_active_from = self.version
            upd.save!
          end
        end
      end
    end
  end

  def self.used_executable_names
    ft = []
    ft_ex = []
    st_ex = []

    ProductDesign.active.each do |pd|    
      begin
        ft = ft + pd.product_features
        st_ex << pd.executables
      rescue RuntimeError => e
        Rails.logger.debug { e }
      end
    end
    ft.uniq.compact.each do |pf|
      exe = pf.executables_with_parameters.map{|k,v|k}
      ft_ex << exe if !exe.blank?
    end

    r_arr = ft_ex.compact.flatten.uniq + st_ex.compact.flatten.compact
    return r_arr.uniq.sort
  end

  def self.find_object_by_design(design_hash,latest=false)
    if design_hash["source_environment"] && design_hash["source_environment"] == Rails.env
      begin
        pd = ProductDesign.find(design_hash["source_record_id"])
      rescue
        return nil
      end
      if pd && pd.find_version(design_hash["source_record_version"])
        if latest
          return pd
        else
          return pd.object_from_version(design_hash["source_record_version"])
        end
      end
    else
      pd = ProductDesign.find_by_name(design_hash["name"])
      if pd
        pd.name = pd.name + " (no exact match found!)"
        return pd
      else
        return nil
      end
    end
  end

  def object_from_version(vers=self.versions.latest.version)
    version_obj = self.find_version(vers)
    obj = ProductDesign.new
    version_obj.class.columns.each do |col|
      obj[col.name] = version_obj.send(col.name) if obj.has_attribute?(col.name)
    end
    obj.id = self.id
    return obj
  end

  def data_set_pre_selection(data_set_id = DataSet.where(:data_type_id => DataType.find_by_name("RDF"),:region_id => Region.find_by_region_code("EUR")).last.id,project_id=Project.default.id)
    start_data_set = DataSet.find(data_set_id)
    step_container = {}

    scopevar = "production"
    scopevar = "preproduction" if project_id != Project.default.id
    
    raise "Product design does not contain conversion steps" if !modelhash["conversion_steps"]
    modelhash["conversion_steps"].each do |step|
      data_types = {}
      raise "Product design does not contain connections in conversion steps" if !step["connections"]
      step_container[step["name"]] = []
      step["connections"].each do |conn|
        data_type = DataType.find_by_name(conn["target_spec"]["datatype"])
        if data_type
          next if data_type.is_intermediate
        else
          data_type = DataType.find_by_name(conn["target_spec"]["allocate_tags"])
          if data_type
            next if data_type.is_intermediate
          else
            raise "Data type '#{conn["target_spec"]["allocate_tags"]}' not known by system."
          end
        end
        data_types[data_type.name] = {"data_type" => data_type}
        data_types[data_type.name]["filling"] = []
        if !conn["filling"].blank?
          filling = Filling.find_by_name(conn["filling"])
          if filling
            data_types[data_type.name]["filling"] << filling
          else
            raise "Filling '#{conn["filling"]}' not known by system."
          end
        end

      end # end connections

      data_types.each do |data_type_name,v|
        data_type_obj = v["data_type"]
        filling_objs = v["filling"]
        if filling_objs.empty?
          # find without filling
          if start_data_set.data_type_id == data_type_obj.id && start_data_set.filling_id.to_s == ''
            step_container[step["name"]] << start_data_set
          else
            # match including release and region
            selected = DataSet.public_send(scopevar).where(:data_type_id => data_type_obj.id, :filling_id => nil, :data_release_id => start_data_set.data_release_id, :region_id => start_data_set.region_id, :company_abbr_id => start_data_set.company_abbr_id ).last
            if !selected
              # match without release with region and choose latest
              selected = DataSet.public_send(scopevar).where(:data_type_id => data_type_obj.id, :region_id => start_data_set.region_id, :filling_id => nil).order(:data_release_id).last
            end
            if !selected
              # match including release and without region
              selected = DataSet.public_send(scopevar).where(:data_type_id => data_type_obj.id, :filling_id => nil, :data_release_id => start_data_set.data_release_id ).last
            end
            if !selected
              # match without release and without region
              selected = DataSet.public_send(scopevar).where(:data_type_id => data_type_obj.id, :filling_id => nil ).last
            end
          end
        else
          # find with filling
          filling_objs.each do |filling|
            if start_data_set.data_type_id == data_type_obj.id && start_data_set.filling_id.to_s == filling.id.to_s
              step_container[step["name"]] << start_data_set
            else
              # match including release and region
              selected = DataSet.public_send(scopevar).where(:data_type_id => data_type_obj.id, :filling_id => filling.id, :data_release_id => start_data_set.data_release_id, :region_id => start_data_set.region_id, :company_abbr_id => start_data_set.company_abbr_id ).last
              if !selected
                # match without release with region and choose latest
                selected = DataSet.public_send(scopevar).where(:data_type_id => data_type_obj.id, :filling_id => filling.id, :region_id => start_data_set.region_id).order(:data_release_id).last
              end
              if !selected
                # match including release and without region
                selected = DataSet.public_send(scopevar).where(:data_type_id => data_type_obj.id, :filling_id => filling.id, :data_release_id => start_data_set.data_release_id ).last
              end
              if !selected
                # match without release and without region
                selected = DataSet.public_send(scopevar).where(:data_type_id => data_type_obj.id, :filling_id => filling.id).last
              end
            end
          end
        end
        step_container[step["name"]] << selected if selected
      end # end data_types

    end # end steps

    if step_container.map{|k,v|v}.flatten.collect{|d|d.id}.include?(data_set_id)
      return step_container
    else
      raise "Chosen design can not process data from this data set."
    end
  end

  def data_set_candidates(data_set_id = DataSet.where(:data_type_id => DataType.find_by_name("RDF"),:region_id => Region.find_by_region_code("EUR")).last.id,project_id=Project.default.id)
    scopevar = "production"
    scopevar = "preproduction" if project_id != Project.default.id

    return_hash = {}
    data_set_pre_selection(data_set_id).each do |step_name,ds_arr|
      return_hash[step_name] = []
      if !ds_arr.empty?
        ds_arr.each do |ds|
          if ds.filling
            return_hash[step_name] =  return_hash[step_name] + DataSet.public_send(scopevar).where("data_type_id = '#{ds.data_type_id}' AND filling_id = '#{ds.filling_id}' AND id != '#{ds.id}' AND company_abbr_id = '#{ds.company_abbr_id}'").order("data_release_id DESC")
          else
            return_hash[step_name] =  return_hash[step_name] + DataSet.public_send(scopevar).where("data_type_id = '#{ds.data_type_id}' AND ( filling_id IS NULL OR  filling_id = '' ) AND id != '#{ds.id}' AND company_abbr_id = '#{ds.company_abbr_id}'").order("data_release_id DESC")
          end
        end
      end
    end
    return return_hash
  end

  def approval_allowed?(user_obj,project_id)
    if self.allowed_status?("approved", user_obj.user_name,project_id) && self.has_conversion_steps?
      if self.product_line && self.product_line.members.include?(user_obj)
        return true
      else
        return false
      end
    else
      return false
    end
  end


  def sync_all_parameter_settings
    if self.parameter_setting_id
      ps = ParameterSetting.find(self.parameter_setting_id)
      if !ps.sync_template_path.blank?
        ps.sync
      end
      modelhash["conversion_steps"].each do |step|
        if step["dedicated_parameterset_id"]
          ps = ParameterSetting.find(step["dedicated_parameterset_id"])
          if step["sync_template_path"]
            ps.sync
          end
        end
      end
    end
  end

  def get_executables_from_subscript(path)
    exes = []
    script = File.open(path, 'rb') { |f| f.read }
    exe_string = script.scan(/setenv DAKOTAPROCS_SUBSCRIPT \"([^\"]*)/).flatten[0]
    exes = exe_string.split(" ") if !exe_string.blank?
    return exes
  end

  def self.sync_all_templates_and_subscripts
    ProductDesign.approved.each do |pd|
      pd.sync_all_parameter_settings
      pd.sync_subscripts
    end
  end

  def sync_subscripts(uuid=nil,subscript_path=nil)

    modelhash["conversion_steps"].each do |step|
      next if uuid && uuid != step["uuid"]
      step["sync_subscript_path"] = subscript_path if subscript_path
      next if step["sync_subscript_path"].blank?
      exes = get_executables_from_subscript(step["sync_subscript_path"])
      release = File.realpath(step["sync_subscript_path"]).scan(/SubScripts_([^\/]*)\//)
      release_name = "UNKNOWN"
      release_name = release[0][0] if release[0] && release[0][0]
      step["sync_subscript_release"] = release_name
      main_exe_index = exes.index(step["name"])\
      
      if !main_exe_index
        puts "Main executable #{step['name']} not found in conversion step for subscript #{step["subscript_path"]}"
        next
      end
            
      step["pre_improvement_steps"] = []
      step["post_improvement_steps"] = []
      current_exe_idx = -1
      exes.each do |exe|
        current_exe_idx = current_exe_idx + 1
        executable = Executable.where(:name => exe).first
        puts "Executable #{exe} not found" if !executable
        next if !executable
        puts "Processing " + executable.inspect
        step_exe = self.add_step(executable)
        next if exe == step["name"]
       puts current_exe_idx
       puts main_exe_index
        if current_exe_idx < main_exe_index
          step["pre_improvement_steps"] << step_exe
        else
          step["post_improvement_steps"] << step_exe
        end
      end
    end

    self.save
    puts "Sync succesful"
  end
  
end
