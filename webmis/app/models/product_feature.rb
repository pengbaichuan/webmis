class ProductFeature < ActiveRecord::Base
  version_fu do
    belongs_to :product_line, :class_name => '::ProductLine'
  end

  belongs_to :product_line
  belongs_to :project
  has_one :environment, :through => :project

  include HandleVersioning
  validates :status, :workflow => true
  validates :name, :presence => true

  before_save :defaults

  def defaults
    self.project_id = Project.default.id if self.project_id.blank?
  end

  # no mandatory reviews at the moment
  @@attrs_requiring_review = []
  
  before_save :defaults

  def self.attrs_requiring_review
    return @@attrs_requiring_review
  end

  def defaults
    if self.abbr_code.blank?
      asset_name = "Product feature code"
      at = AssetType.find_by_name(asset_name)
      nextnumber = at.get_next_number({ :title => self.name, :name=> asset_name })

      asset = Asset.new
      asset.ref_id = nextnumber
      asset.requested_by = self.updated_by
      asset.assettype = asset_name
      asset.title = self.name
      asset.remarks = "Generated during save of product feature."
      asset.save
      self.abbr_code = asset.ref_id
    end
  end

  def meta_string
      "design_id=#{design_id}|code=#{abbr_code}|name=#{name}"
  end

  def design_id
    "PF_#{abbr_code}"
  end

  def system_steps
    trans = {"parts" => []}
    if system_translation.to_s.size > 0
      trans = JSON.parse(system_translation)
    end
    return trans["parts"]
  end

  alias_method :generic_valid_review?, :valid_review?
  def valid_review?
    generic_valid_review? && name  &&
    product_line && name.length > 0
  end

  alias_method :generic_valid_approved?, :valid_approved?
  def valid_approved?
    generic_valid_approved? && valid_review? 
  end

  # returns an option list for a select box with all Product lines used by a product feature
  def self.used_product_lines
    option_list = []
    ids = self.select("distinct product_line_id").map{|p| p.product_line_id}
    ids.each do |i|
    end
    ProductLine.all.sort_by{|r| r.abbr}.each do |inst|
      option_list << [inst.abbr, inst.id] if ids.include?(inst.id)
    end
    option_list
  end

  # returns an option list for a select box with all features belonging to a specific Product line
  def self.option_list_for_product_line(v_product_line_id,v_project_id=Project.default.id)
    v_product_line_id ? self.where("product_line_id = ? AND status != 'obsolete' AND project_id = ?", v_product_line_id,v_project_id).map{|f| [f.abbr_code + " "+ f.name.to_s, f.meta_string]} : []
  end

  def self.find_correct_version_by_name(name, use_test_versions_yn, checklog = nil)
    if name.blank?
      checklog.add_info_line("Empty feature specified, skipping it.") if checklog
      return nil
    end

    # we have a name
    pf = ProductFeature.find_by_name(name)
    if pf.nil?
      checklog.add_error_line("No product feature found with name '#{pf.id}'.") if checklog
      checklog.update_result(Checklog::STATUS_EXCEPTION) if checklog
      raise "No parameter setting found with id '#{ps.id}'."
    end

    # we have a parameter setting
    if pf.obsolete?
      # the parameter setting is obsolete
      if use_test_versions_yn
        # test versions should not use obsolete settings
        checklog.add_error_line("Product feature '#{pf.id}' ('#{name}') is obsolete.") if checklog
        checklog.update_result(Checklog::STATUS_EXCEPTION) if checklog
        raise "Product feature '#{name}' is obsolete."
      elsif pf.latest_approved
        # although obsolete, the latest approved version is still valid
        checklog.add_info_line("Product feature '#{pf.id}' ('#{name}') is now obsolete. Using the latest approved version '#{pf.latest_approved.version}'.") if checklog
        return pf.use_latest_approved
      else
        # the product feature has never been approved and is now obsolete
        checklog.add_error_line("Product feature '#{pf.id}' ('#{name}') has never been approved and is now obsolete.") if checklog
        checklog.update_result(Checklog::STATUS_EXCEPTION) if checklog
        raise "No approved version of obsolete product feature '#{ps.id}'."
      end
    end

    # we have a non-obsolete product feature
    if use_test_versions_yn
      checklog.add_debug_line("Using product feature '#{pf.id}' ('#{name}') version '#{pf.version}' with status '#{pf.status}'.") if checklog
      return pf
    elsif pf.status == 'approved'
      checklog.add_debug_line("Using the latest version ('#{pf.version}) of product feature '#{pf.id}' ('#{name}'), with status '#{pf.status}'.") if checklog
      return pf
    elsif pf.latest_approved
      checklog.add_debug_line("Use the latest approved version ('#{pf.latest_approved.version}') of product feature '#{pf.id}' ('#{name}'). The current version is '#{pf.version}'' and has status '#{pf.status}'.") if checklog
      return pf.use_latest_approved
    else
      checklog.add_error_line("Product feature '#{pf.id}' ('#{name}') has not been approved yet. Approve it first before proceeding.") if checklog
      checklog.update_result(Checklog::STATUS_EXCEPTION) if checklog
      raise "No approved version of product feature '#{pf.id}'."
    end
  end

  def executables_with_parameters
    return_h = Hash.new
    if !self.system_translation.blank?
      parts = JSON.parse(self.system_translation)["parts"]
      parts.each do |part|
        param_prefix = ""
        param_prefix = "parameter " if part["type"] == "PARAMETER"
        value = part["parameter_value"]
        if !value.to_s.strip.match(/\A[+-]?\d+?(\.\d+)?\Z/).nil?
          param_value = value.to_s
        else
          param_value = "\"#{value.to_s}\""
        end
        if value.to_s.strip == ""
          exe_obj = Executable.find_by_name(part["executable"])
          exe_param = JSON.parse(exe_obj.parameters)["parameters"].find { |h| h['name'] == part["parameter_name"] }
          if exe_param["allocate"]
            param_value = '#ALLOCATION-REQUIRED'
          end
        end
        parameter_string = "#{param_prefix}#{part["parameter_name"]} #{param_value}"
        if return_h.has_key?(part["executable"])
          return_h[part["executable"]] = return_h[part["executable"]] << parameter_string
        else
          return_h[part["executable"]] = [parameter_string]
        end
      end
    end
    return return_h
  end
end
