class ProductLine < ActiveRecord::Base
  validates :name, :abbr, :presence => true
  validates_uniqueness_of :abbr

  belongs_to :company_abbr
  has_many :customer_content_definitions
  has_many :customer_content_specifications, :through => :customer_content_definitions
  has_many :products
  belongs_to :product_line, :foreign_key => 'parent_id'
  has_many :product_lines, :foreign_key => 'parent_id'
  belongs_to :shipping_specification
  has_many :product_line_users
  has_many :users, :through => :product_line_users
  has_many :parameter_settings
  has_many :test_area_configurations
  has_many :product_line_allowed_data
  

  scope :active?, -> {where(:is_active => true)}
 
  # generate a list of active target platforms which can be used in a select box
  def self.option_list(id = nil)
    list = []
    if id && instance = self.find(id)
      # add the current instance to the option list if it would normally not be included
      list << [instance.abbr, instance.id] unless instance.is_active
    end
    # add the normal options to the list
    self.where("is_active = 1").sort_by{|r| r.abbr}.each do |inst|
      list << [inst.abbr, inst.id]
    end
    list
  end

  # return the relevant target platform and parameter_setting information in hash format so it can
  # be included in a JSON string
  def self.to_hash(product_line_id,project_id=Project.default.id)
    hash = {}
    product_line = ProductLine.find(product_line_id) if product_line_id
    if product_line
      hash["id"] = product_line_id
      hash["abbr"] = product_line.abbr
      parameter_setting_version = product_line.parameter_settings.where(:project_id => project_id).last.approved if !product_line.parameter_settings.where(:project_id => project_id).empty?
    end
    if parameter_setting_version
      hash["parameter_setting_id"] = product_line.parameter_settings.where(:project_id => project_id).last.id
      hash["parameter_setting_version"] = parameter_setting_version.version
      hash["parameter_setting_version_id"] = parameter_setting_version.id
      hash["parameter_setting_status"] = parameter_setting_version.status
      hash["parameter_setting_name"] = parameter_setting_version.name
      hash["parameter_settings"] = JSON.parse(parameter_setting_version.parameterset)
    end
    hash
  end

  def members
    self.product_line_users.collect{|plu|plu.user}.uniq
  end

  def update_project_members(user_ids=[])
    current_user_ids = self.users.collect{|u|u.id.to_s}
    equals = current_user_ids & user_ids
    removals = current_user_ids - equals
    newbies = user_ids - equals
    begin
      ProductLine.transaction do
        removals.each do |rm_id|
          rm_user = self.product_line_users.find_by_user_id(rm_id)
          rm_user.destroy
        end
        newbies.each do |add_id|
          add_user = ProductLineUser.new(:user_id => add_id,:product_line_id => self.id)
          add_user.save!
        end
      end
    rescue
      return false
    end
    return true
  end

  def update_allowed_data(list=[])
    begin
      ProductLine.transaction do
        self.product_line_allowed_data.destroy_all
        list.each do |allowed_item|
          ids = allowed_item.split("-")
          next if ids[0] == "0"
          plad = ProductLineAllowedData.new(:product_line_id => self.id, :company_abbr_id => ids[0], :data_type_id => ids[1], :region_id => ids[2], :filling_id => ids[3])
          plad.save
        end
      end
    rescue
      return false
    end
    return true
  end

  def parameter_setting(project_id=Project.default.id)
    return self.parameter_settings.where(:project_id => project_id).last if self.id
  end
  
  def create_parameter_setting(project_id)
    ps = ParameterSetting.new 
    ps.product_line_id = self.id
    ps.name = self.abbr.gsub(" ","_")
    ps.project_id = project_id
    ps.status = 'draft'
    ps.parameterset = '[]'
    ps.save!
    ps
  end


end
