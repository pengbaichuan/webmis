class ProductLineAllowedData < ActiveRecord::Base
  belongs_to :product_line
  belongs_to :data_type
  belongs_to :company_abbr
  belongs_to :region
  belongs_to :filling
end
