class ProductLineUser < ActiveRecord::Base
  belongs_to :user
  belongs_to :product_line
end
