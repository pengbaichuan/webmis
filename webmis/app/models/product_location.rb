class ProductLocation < ActiveRecord::Base
  has_many :outbound_orderlines
  has_many :production_orderlines
  belongs_to :conversion
  has_many :conversion_databases, :through => :conversion
  validates :storage_path, :presence => true
  validates :product_id, :presence => true
  
  HUMANIZED_COLLUMNS = {:storage_path => "Product files"}

  def self.human_attribute_name(attribute, opt={})
    HUMANIZED_COLLUMNS[attribute.to_sym] || super
  end

  def map_from_cv(c, productid=c.productid)
    self.conversion_id = c.id
    self.product_id = c.productid
    self.release_sequence = ProductLocation.determine_new_release_squence("#{c.productid}")
    self.area = c.region_name
    self.product_format = c.output_format
    self.conversiontool = c.conversion_tool_release
    self.data_path = c.data_files
    if c.purpose == 'test'
      self.product_type = "Test Product"
    else
      self.product_type = "Product"
    end

    self.internal_storage_path = ""
    
    if c.result_files
      c.conversion_databases.each do |db|
        if db.product_id == productid
          self.internal_storage_path = self.internal_storage_path +  db.path_and_file + "\r\n"
        end
      end
    end
    
    if c.reception_reference && c.reception_reference.to_s != ""
      r = Reception.where(:id => "#{c.reception_reference}").first
      if r
        self.internal_storage_path = r.storage_location
        self.reception_id = c.reception_reference
      end
    end

    if c.data_release_id && DataRelease.find(c.data_release_id)
      dr = DataRelease.find(c.data_release_id)
      self.data_release = dr.name
      self.supplier = dr.company_abbr.name
    end
  end

  def map_from_po(po)
    
    lines = po.production_orderlines.where("bp_name = 'MASTERING' and bp_status ='PROCESSING'")
    self.internal_storage_path = ""
    self.product_id = ""
    self.save(:validate => false)
    lines.each do |line|
      self.conversion_id = line.chosen_conversion_id if !line.parent_linenr
      self.product_id = line.product_id if !line.parent_linenr
      self.release_sequence = ProductLocation.determine_new_release_squence("#{line.product_id}") if !line.parent_linenr
      # these fields need to be derived from the order/product
      #self.area = c.region_name
      #self.product_format = c.output_format
      #self.conversiontool = c.cvtool
      #self.data_path = c.data_files
      line.conversion_databases.each do |db|
         self.internal_storage_path = self.internal_storage_path +  db.path_and_file + "\r\n"
      end
      line.product_location_id = self.id
      line.save
    end
  end
  
  def map_from_rc(r)
    self.reception_id = r.id
    self.remarks = "Conversion not registered in WebMIS."
    self.product_id = r.product_id
    self.release_sequence = ProductLocation.determine_new_release_squence("#{r.product_id}")
    self.area = r.region_name
    self.product_format = DataType.find_by_id(r.data_type_id).name
    self.conversiontool = r.conversiontool if r.conversiontool
    self.internal_storage_path = r.storage_location if r.storage_location
    if r.purpose == 'test'
      self.product_type = "Test Product"
    else
      self.product_type = "Product"
    end
    dr = DataRelease.find(r.data_release_id)
    self.data_release = dr.name
    self.supplier = dr.company_abbr.name
  end


  
  def self.determine_new_release_squence(product_id)
    #legacy clean, ignore terrible dotted product_id
    volume_id = product_id.split('.')[0]
    sequence = 0
    
    previous = self.where("product_id LIKE \"#{volume_id}%\"").order(:created_at)
    sequence = previous.size #starts with zero
    
    if !previous.last.nil?
      if !previous.last.release_sequence.blank?
        sequence = previous.last.release_sequence + 1
      end
    end
    
    return sequence
  end
  
  def self.read_checksum_from_product_location_storage_path(storage_path)
    f = storage_path
    extension = f.reverse.split('.')[0].reverse
    checksum_file = f.gsub(extension,'checksum')
    if  FileTest.exist?(checksum_file)
      file = File.new(checksum_file, "r")
      return file.gets.to_s.scan(/.{32}/).join
    end
  end

  def filename_from_storage_path
    self.storage_path.split('/').last if !self.storage_path.blank?
  end

  def flag_conversion_databases_for_removed
    return if !self.internal_storage_path
    self.internal_storage_path.split("\r\n").each do |file|
      #only flag for removed if filename is the same and size of both files is the same
      if File.basename(file) == File.basename(self.storage_path) &&
         File.exists?(file) && File.exists?(self.storage_path) &&
         File.size(file) == File.size(self.storage_path)
        cd = ConversionDatabase.includes(:conversion).
          where(:conversion_id => self.conversion_id).
          where(:path_and_file => file).
          where(:product_id => self.product_id).first
        if cd
          ConversionDatabase.transaction do
            cd.set_file_system_status(ConversionDatabase::FILE_SYSTEM_STATUS_MARKED_FOR_REMOVAL)
            cd.save!
            conversion = Conversion.find(self.conversion_id)
            conversion.remarks = conversion.remarks = "#{Time.now.strftime('%d-%m-%Y %H:%M')}: Outcome automaticly removed. Product was registered with product location #{self.id}."
            conversion.save!
            return "Some Databases were flagged as removed"
          end
        end
      else
        return ""
      end
    end
    return ""
  end
  
end
