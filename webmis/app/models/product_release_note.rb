class ProductReleaseNote < ActiveRecord::Base

  belongs_to :production_order
  belongs_to :production_orderline
  has_many :content_release_notes
  belongs_to :document
  
  validates_uniqueness_of :production_order_id , :allow_nil => true, :message => 'There is already a release note for this production order.'
  validates_uniqueness_of :production_orderline_id, :allow_nil => true, :message => 'There is already a release note for this production orderline.'
  
  STATUS_DRAFT      =  0
  STATUS_REVIEW     = 10
  STATUS_APPROVED   = 30
  STATUS_RELEASED   = 40

  @@statusarray     = Array.new
  @@statusarray[STATUS_DRAFT]     = 'Draft'
  @@statusarray[STATUS_REVIEW]    = 'Review'
  @@statusarray[STATUS_APPROVED]  = 'Approved'
  @@statusarray[STATUS_RELEASED]  = 'Released'

  def self.statusarray
    return @@statusarray
  end

  def self.status_hash
    s_hash = Hash.new
    @@statusarray.each do |s|
      if !s.nil?
        s_hash[s] = @@statusarray.index(s)
      end
    end
    return s_hash.sort_by { |k,v| v }
  end
  
  def status_string
    @@statusarray[self.status]
  end
  
  def productset
    if self.production_order
      self.production_order.productset
    else
      self.production_orderline.production_order.productset
    end
  end
  
  def product_category
    if self.production_order
      self.production_order.ordertype.product_category
    else
      self.production_orderline.production_order.product_category
    end
  end
  
end