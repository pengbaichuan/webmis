class ProductReleaseNotePdf
  include ActionView::Helpers
  attr_accessor :product_release_note, :current_user, :production_order, :tmpdir, :nextnumber, :pdffilename, :zipfilename, :on_production_order_yn, :unused_static_release_note_titles
  require "zip/zip"

  def initialize (current_product_release_note, current_user=nil)
    self.product_release_note = current_product_release_note
    self.current_user = current_user if current_user
    self.on_production_order_yn = !self.product_release_note.production_order.nil?
    self.production_order = self.on_production_order_yn ? self.product_release_note.production_order : self.product_release_note.production_orderline.production_order
    
    if self.production_order.productset
      self.pdffilename = self.production_order.productset.name.to_s.gsub(/[^\w\s_-]+/, '').gsub(/(^|\b\s)\s+($|\s?\b)/, '\\1\\2').gsub(/\s+/, '_') + ".pdf"
      self.zipfilename = self.production_order.productset.setcode.to_s.gsub(' ','_') + ".zip"    
    else
      self.pdffilename = self.product_release_note.production_orderline.product.name.to_s.gsub(/[^\w\s_-]+/, '').gsub(/(^|\b\s)\s+($|\s?\b)/, '\\1\\2').gsub(/\s+/, '_') + ".pdf"
      self.zipfilename = self.product_release_note.production_orderline.product.name.to_s.gsub(' ','_') + ".zip"
    end
    
    self.set_unused_static_release_note_titles
  end

  def set_unused_static_release_note_titles
    self.unused_static_release_note_titles = Array.new
    if self.production_order.product_category
      self.product_release_note.content_release_notes.where(:addition_required_yn => true).each do |srt|
        self.unused_static_release_note_titles << srt
      end
    end
  end

  def generate
    self.set_nextnumber
    Dir.mktmpdir{ |tmpdir|
      self.write_pdf(tmpdir)
      self.write_appendices(tmpdir)
      self.write_pds(tmpdir)
      zip_file = self.zip_package(tmpdir)
      self.store_pdf_in_document(zip_file)
    }
  end

  def set_nextnumber
    asset_type = AssetType.find_by_name("Release Notes")
    self.nextnumber = asset_type.get_next_number({"dummy"=>"muppet"})
    asset = Asset.new
    asset.ref_id = self.nextnumber
    asset.requested_by = self.current_user.first_name + " " + self_current_user.last_name if self.current_user
    asset.remarks = "Generated via release notes with id #{self.product_release_note.id}."
    asset.assettype = asset_type.name.to_s
    asset.title = "Release notes #{self.product_release_note.productset.name.to_s}"
    asset.save
  end

  def write_pdf(tmpdir)
    Prawn::Document.generate(File.join(tmpdir,self.pdffilename)) do |pdf|
      # Main outline title
      if self.product_release_note.productset
        section = "#{self.product_release_note.productset.name}"
      else
        section = "#{self.product_release_note.production_orderline.product.name}"
      end
      asset = @nextnumber || 'DRAFT'
 
      #Document ID + Creation date
      pdf.formatted_text_box [ { :text => asset + " #{DateTime.now.strftime('%d %B %Y')}", :color => "C0C0C0" }], 
        :at => [-30, 50],:rotate => 90

      #outline menu of PDF
      pdf.outline.define do
        section("#{section}", :destination => 1) do
        end
      end

      #title / frontpage
      pdf.move_down 60
      pdf.text "Product Release Notes" , :size => 20, :align => :center, :style => :bold
      pdf.move_down 4
      pdf.text "#{section}" , :size => 20, :align => :center
      pdf.move_down 40
      pdf.image Rails.root.join('public','images','mapscape_world.png').to_s, :width => 400, :position => :center

      # TOC
      pdf.start_new_page
      pdf.outline.add_subsection_to("#{section}") do
        pdf.outline.page :title => "Table of contents",
        :destination => pdf.page_count
      end
      pdf.text "Table of contents" , :size => 15, :style => :bold
      pdf.move_down 10
      pdf.text "#{section}" , :size => 12
      pdf.move_down 10

      maincounter = 0
      subcounter = 0
      table = [["",""]]
      self.product_release_note.content_release_notes.each do |content|
        if !content.subrank
          maincounter = (maincounter + 1) 
          subcounter = 0
          
          c0 = "<link anchor='#{content.title}'><b>#{maincounter.to_s}</b></link>"
          c1 = ""
          c2 = "<link anchor='#{content.title}'>#{content.title}</link>"
          table << [c0,c2]
        else
          subcounter = (subcounter + 1)

          c1 = ""
          c0 = "<link anchor='#{content.title}'><b>#{maincounter.to_s}.#{subcounter.to_s}</b></link>"
          c2 = "<link anchor='#{content.title}'>#{content.title}</link>"
          table << [c0,c2]
        end  
      end

      if nds_size_stats(pdf,true) == true
        maincounter = (maincounter + 1)
        c0 = "<link anchor='NDS Size statistics'><b>#{maincounter.to_s}</b></link>"
        c1 = ""
        c2 = "<link anchor='NDS Size statistics'>NDS Size statistics</link>"
        table << [c0,c2]
      end

      documents = self.on_production_order_yn ? self.production_order.documents : self.product_release_note.production_orderline.documents
      if documents.size > 0
        maincounter = (maincounter + 1)
        c0 = "<link anchor='Appendix(es)'><b>#{maincounter.to_s}</b></link>"
        c1 = ""
        c2 = "<link anchor='Appendix(es)'>Appendix(es)</link>"
        table << [c0,c2]
      end
      pdf.stroke_horizontal_rule
      pdf.move_down 10
      pdf.table(table,:cell_style => {:borders => [],:inline_format => true},:column_widths => {0 => 70})
      pdf.move_down 15
      pdf.stroke_horizontal_rule
      pdf.start_new_page

      #content
      pdf.bounding_box [pdf.bounds.left, pdf.cursor], :width  => pdf.bounds.width, :height => pdf.cursor - pdf.bounds.bottom - 30 do
        maintitle = ""
        maincounter = 0
        subcounter = 0
        self.product_release_note.content_release_notes.each do |content|
          if content.subrank
            pdf.move_down 10
            subcounter = (subcounter + 1)
          else
            pdf.move_down 20
            maincounter = (maincounter + 1) 
            subcounter = 0
          end
          
          pdf.start_new_page if content.start_on_new_page
          
          if content.subrank
              pdf.outline.add_subsection_to("#{maintitle}") do
            pdf.outline.page :title => "#{content.title}",
            :destination => pdf.page_count
          end
          else
            maintitle = content.title
            pdf.outline.add_subsection_to("#{section}") do
             pdf.outline.page :title => "#{content.title}",
             :destination => pdf.page_count
            end
          end  

          pdf.add_dest("#{content.title}", pdf.dest_fit_horizontally(pdf.cursor, pdf.page.dictionary))
          if content.subrank
            pdf.text "#{maincounter}.#{subcounter} - #{content.title}", :size => 12, :style => :bold
            pdf.move_down 4
          else
            pdf.text "#{maincounter} - #{content.title}", :size => 15, :style => :bold
            pdf.move_down 6
          end
          
          is_hash = (content.content_type && content.content_type == CustomerContentSpecification::TYPE_HASH && JSON.parse(content.content).class == Hash) rescue is_hash = false
          if is_hash
            content_hash = JSON.parse(content.content)
            content_hash = { '-' => '-'} if content_hash.size == 0
            hashtable = content_hash.map { |key, value| [key, value] }

            pdf.table(hashtable, :row_colors => ["F2F2F2", "FAFAFA"], :cell_style => {:borders => [],:inline_format => true}, :header => true)
          else
            pdf.text "#{content.content}",:size => 10
          end
          product_list(pdf) if content.product_list == true
        end

        # NDS size statistics
        if nds_size_stats(pdf,true) == true
          pdf.start_new_page
          maincounter = (maincounter + 1)
          pdf.outline.add_subsection_to("#{section}") do
            pdf.outline.page :title => "NDS Size statistics",
              :destination => pdf.page_count
          end
          pdf.add_dest("NDS Size statistics", pdf.dest_fit_horizontally(pdf.cursor, pdf.page.dictionary))
          pdf.text "#{maincounter} - NDS Size statistics" , :size => 15, :style => :bold
          pdf.move_down 4
          pdf.text "Table with NDS regions and their building sizes." , :size => 8
          nds_size_stats(pdf)
          pdf.move_down 4
          pdf.text "All sizes in table are noted in Mb. (Mega bytes)" , :size => 8
        end
      end

      #appendixes
      if documents.size > 0
        pdf.start_new_page
        maincounter = (maincounter + 1) 
        pdf.outline.add_subsection_to("#{section}") do
          pdf.outline.page :title => "Appendix(es)",
          :destination => pdf.page_count
        end  
        pdf.add_dest("Appendix(es)", pdf.dest_fit_horizontally(pdf.cursor, pdf.page.dictionary))
        pdf.text "#{maincounter} - Appendix(es)" , :size => 15, :style => :bold
        
        pdf.move_down 4  
        table = [["<b>description</b>","<b>Directory</b>","<b>File name</b>"]]
        documents.each do |doc|
          table << [doc.document_object_id,doc.package_directory,doc.filename]
        end
        
        #PDS for attachment table
        attach_array = Array.new
        if self.production_order.product_category.attach_pds_in_rn_yn == true
          if self.production_order.productset
            self.production_order.productset.includedproductsetdocuments.each do |icd|
              if !icd.document.description.scan(/(\s\(PDS\))/i).empty?
                attach_array = [icd.document.document_object_id,'/product definition',icd.document.filename]
              end
            end
          else
            self.product_release_note.production_orderline.product.includeddocuments.each do |icd|
              if !icd.document.description.scan(/(\s\(PDS\))/i).empty?
                attach_array = [icd.document.document_object_id,'/product definition',icd.document.filename]
              end
            end
          end
        end    
        table << attach_array
        pdf.table(table, :row_colors => ["F2F2F2", "FAFAFA"], :cell_style => {:borders => [],:inline_format => true}, :header => true)
      end

      #page numbering
      string = "- <page> -"
      options = { :at => [pdf.bounds.right - 120, 10],
      :size => 8,
      :width => 150,
      :align => :right,
      :start_count_at => 1,
      :page_filter => lambda{ |pg| pg != 1 }
      }
      pdf.number_pages string, options

      pdf.repeat(lambda{ |pg| pg != 1 }) do
        pdf.image Rails.root.join('public','images','mapscape_corner.png').to_s, :width => 88, :position => 420, :vposition => -20 
      end

      #footer
      pdf.repeat :all do
            pdf.bounding_box [pdf.bounds.left, pdf.bounds.bottom + 20], :width  => pdf.bounds.width  do
              pdf.text "Mapscape B.V. | Luchthavenweg 34 | 5657 EB Eindhoven  | The Netherlands", :size => 6,:align => :center, :color => "0000FF"
              pdf.text "Commercial registry: 17210210  | Registered office: Eindhoven, The Netherlands", :size => 6,:align => :center, :color => "A4A4A4"
              pdf.text "Managing Directors: Henk Eemers, Ralf Stollenwerk", :size => 5,:align => :center, :color => "A4A4A4"
          end
      end
    end
  end

  # prawn code for the list of products 
  def product_list(pdf)
    pdf.move_down 4
    mediumheader = ""
    table = []
    orderlines = []
    if self.on_production_order_yn
      orderlines = self.production_order.production_orderlines.where('bp_name != "CANCELLED"')
    elsif self.product_release_note.production_orderline.bp_name != "CANCELLED"
      orderlines << self.product_release_note.production_orderline
    end
    orderlines.each do |ol|
      mediumid = "" 
      if (product = Product.find_by_volumeid(ol.product_id.gsub(/\..*/,'').to_s)) && ol.product_accepted != 'Rejected'

       if ['CAR', 'CARIN'].include?(self.product_release_note.product_category.abbreviation) && !ol.conversions.empty? && !ol.conversions.last.stock.validated.empty?
         mediumheader = "Medium ID"
         mediumid = ol.conversions.last.stock.validated.collect {|m|m.medium_number}.join(' / ')
       end

        table << ["<font size='10'>#{ol.product_id}</font>","<font size='10'>#{mediumid}","<font size='10'>#{product.name}","<font size='10'>#{product.detailedcoverage}"]
      end
    end
    table.insert(0,["Product ID",mediumheader,"Name","Detailed coverage"])
    pdf.table(table, :row_colors => ["FAFAFA", "F2F2F2"], :cell_style => {:borders => [],:inline_format => true}, :header => true)
    return pdf
  end

  # prawn code for the NDS Size Statistic Table
  def nds_size_stats(pdf,onlycheck=false)
    table = []
    size_stat_hash = {}
    orderlines = []
    if self.on_production_order_yn
      orderlines = self.production_order.production_orderlines.where('bp_name != "CANCELLED"')
    elsif self.product_release_note.production_orderline.bp_name != "CANCELLED"
      orderlines << self.product_release_note.production_orderline
    end
    if !orderlines.empty?
      orderlines.each do |ol|
        if ol.product && !ol.product.parent && ol.chosen_conversion
          ol.chosen_conversion.conversion_databases.each do |conversion_database|
            if conversion_database.db_type == 'NDS' && conversion_database.file_system_status != ConversionDatabase::FILE_SYSTEM_STATUS_REMOVED
              if !conversion_database.nds_size_statistics.empty?

                size_stat_hash = conversion_database.nds_size_statistics
                @cdb_id = size_stat_hash.delete("conversion_database_id")
              end
            end
          end
        end

        unless size_stat_hash.empty?
          return true if onlycheck # Must be done before pdf.object definition
          header_arr = size_stat_hash.sort.collect{|s|s[1].collect{|k,v|"#{k.strip}"}}.flatten.sort.uniq
          column_totals = header_arr.collect {|h|0.00}.unshift("<font size='6'>Total</font>") + [0.00]
          final_total = 0.00
          size_stat_hash.sort.each do |stat|
            content_arr = []
            total = 0.00
            header_arr.each do |h|
              if stat[1][h]
                s = stat[1][h]
                c = number_with_precision(s.to_f / 1024**2, precision: 2)
                column_totals[header_arr.index(h)+1] = number_with_precision((column_totals[header_arr.index(h)+1].to_f + c.to_f), precision: 2)
              else
                c = '-'
              end
              content_arr << "<font size='5'>#{c}</font>"
              total= c.to_f + total.to_f
            end
            final_total = final_total.to_f + total.to_f
            table << content_arr.unshift("<font size='5'>#{stat[0]}</font>") + ["<font size='5'>#{number_with_precision(total, precision: 2)}</font>"]
          end
          column_totals[-1] = number_with_precision(final_total, precision: 2)
          table << column_totals.collect{|t|"<font size='6' style='Bold'>#{t}</font>"}
          table.insert(0,header_arr.unshift("Region").collect{|h|"<font size='6' style='Bold'>#{h.downcase}</font>"}+["<font size='6'>Total</font>"])
          pdf.move_down 4
          pdf.table(table, :row_colors => ["FAFAFA", "F2F2F2"], :cell_style => {:inline_format => true,:borders => [],:overflow => :shrink_to_fit}, :header => true)
          pdf.move_down 2
          pdf.text "db-id: #{@cdb_id}" , :size => 2
          return pdf
        end

      end
    else
      return false if onlycheck # !orderlines.empty?
    end
  end

  def write_appendices(tmpdir)
    appendices = self.product_release_note.production_order ? self.production_order.documents : self.product_release_note.production_orderline.documents
    appendices.each do |doc|
      next if !doc.filename || doc.filename.blank?
      if doc.category != "release-note" || doc.category = nil
        packagedir = doc.package_directory.blank? ? 'appendixes' : doc.package_directory
      end
      Dir.mkdir("#{tmpdir}/#{packagedir}",0777) unless File.exists?("#{tmpdir}/#{packagedir}")
      puts doc.inspect
      File.open(File.join("#{tmpdir}","#{packagedir}", doc.filename), "wb") do |file|
        file.write(doc.binary_data)
      end
    end
  end

  def write_pds(tmpdir)
    if self.production_order.product_category.attach_pds_in_rn_yn
      self.production_order.productset.includedproductsetdocuments.each do |icd|
        packagedir = 'product definition'
        Dir.mkdir("#{tmpdir}/#{packagedir}", 0777) unless File.exists?("#{tmpdir}/#{packagedir}")
        File.open(File.join("#{tmpdir}", "#{packagedir}", icd.document.filename), "wb") do |file|
          file.write(icd.document.binary_data)
        end
      end
    end
  end

  def zip_package(tmpdir)
    zipfile = "#{tmpdir}/#{self.zipfilename}"
    Zip::ZipFile::open(zipfile, true) { |zf|
      Dir.chdir(tmpdir) {
        Dir["**/*"].each{|f|
          g=File.join(tmpdir,f)
          zf.add(f,g) rescue zf.replace(f,g)
        }
      }
    }
    FileUtils.chmod_R(0770, zipfile)
    return zipfile
  end

  def store_pdf_in_document(zipfile)
    f = File.open(zipfile)
    document = self.product_release_note.document_id.to_s.blank? ? Document.new : Document.find(self.product_release_note.document_id)
    document.content_type = "application/zip"
    document.filename = self.zipfilename
    document.description = "Release notes #{self.product_release_note.productset.name.to_s}"
    if self.product_release_note.productset
      document.document_object_id = "Release notes #{self.product_release_note.productset.name.to_s}"
    else
      document.document_object_id = "Release notes #{self.product_release_note.production_orderline.product.name.to_s}"
    end
    document.binary_data = f.read
    f.close
    document.save
    self.product_release_note.document_id = document.id
    self.product_release_note.status = ProductReleaseNote::STATUS_RELEASED
    self.product_release_note.save
  end
end