class ProductVersion < ActiveRecord::Base

  def delta(count=1)
    ps1 = self
    ps2 = nil
    if ps1.version.to_i - count <= 0
      ps2 =  ProductVersion.where("product_id = #{product_id} and version = 1").first
    else
      ps2 =  ProductVersion.where("product_id = #{product_id} and version = #{version-count}").first
    end
    ps2 = ps1 if !ps2
    ProductVersion.diff :exclude =>['id', 'yaml','created_at','updated_at','version','major','minor','changes']
    psdiff = ps1.diff(ps2)
    return psdiff
  end

  def components(set_id)
    ic = IncludedproductVersion.where("productset_id = #{set_id} and product_id = #{id}").first
    i = IncludedproductVersion.where("productset_id = #{set_id} and link_product_id = #{ic.id}")
    components = []
    i.each do |ip|
      components << ip.product
    end
    return components
  end

end
