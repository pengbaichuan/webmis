class ProductionOrder < ActiveRecord::Base

  before_create :add_ini_status
  after_initialize :defaults
   
  has_many   :production_orderlines
  has_many   :commitment_dates
  has_many   :orderstatuslogs
  has_many   :documents
  has_one    :product_release_note

  belongs_to :productset
  belongs_to :ordertype
  has_one :product_category, :through => :ordertype

  belongs_to :distribution_list
  belongs_to :rel_notes_request_user, :class_name => "User", :foreign_key =>  "rel_notes_requested_by_id"
  belongs_to :rel_notes_send_user,    :class_name => "User", :foreign_key =>  "rel_notes_send_by_id"

  belongs_to :project
  has_one :environment, :through => :project

  #acts_as_reportable
  acts_as_taggable

  attr_accessor :checklog

  validates :name, :presence => true
  validates :project_id, :presence => true
  validates :distribution_list_id , :presence => { :message => "is required when shipment is allowed." }, :if => :allow_shipment

  # fixed business process names defining the start and end business processes.
  BP_NAME_ORDERENTRY  = 'ORDERENTRY'
  BP_NAME_COMPLETE    = 'COMPLETE'
  BP_NAME_CANCELLED   = 'CANCELLED'
  BP_NAME_DELETED     = 'DELETED'

  COMMITMENT_BASELINE = 'baseline_commitment'
  COMMITMENT_MODIFIED = 'modified_commitment'

  scope :active, -> { where("production_orders.bp_name != 'COMPLETE' AND production_orders.bp_name != 'CANCELLED' AND production_orders.bp_name != 'DELETED'") }

  def defaults
   
    self.project_id ||= Project.default.id
    if self.new_record?
      # set a default expiry date in case of a new production order
      nrof_days = ConfigParameter.get('default_database_expiry_in_days', self.project_id).to_i
      self.expiry_date = Date.current + nrof_days if nrof_days > 0
    end
  end

  def add_ini_status
    # cannot be done in the defaults as you don't know the ordertype_id yet when creating a new ProductionOrder object
    transition = Ordertype.find(self.ordertype_id).process_transitions[0]
    self.bp_name ||= transition.from_process.name
    self.bp_seqnr ||= transition.sequence
  end
 
   def checklog
    @checklog ||= Checklog.new(only_check_yn = false, Checklog::LOGLEVEL_DEBUG)
  end

  def start_bp?
    [BP_NAME_ORDERENTRY].include?(self.bp_name)
  end

  def end_bp?
    [BP_NAME_COMPLETE, BP_NAME_CANCELLED, BP_NAME_DELETED].include?(self.bp_name)
  end

  def start_or_end_bp?
    start_bp? || end_bp?
  end

  def set_bp_name(new_bp_name, new_bp_seqnr)
    self.bp_name = new_bp_name
    self.bp_seqnr = new_bp_seqnr
  end

  def add_lines
    ProductionOrder.transaction do
      productset = Productset.find(self.productset_id)
      self.major = productset.major
      self.minor = productset.minor
      productset.products.each do |product|
        line = ProductionOrderline.new
        line.production_order_id = self.id
        line.product_id = product.volumeid
        line.quantity = 1
        line.remarks = product.remarks
        line.product_line_id = product.product_line_id
        self.production_orderlines << line
      end
      save!
    end
  end

  def generate_name(default_str, frontend = false)
    if frontend
      # AUTO BE/AUTO FE no longer used, so in effect this sets to name to default_str if name is not set or frontend is true
      if !self.name.sub!(/^Auto BE:/, 'Auto FE:') && !default_str.blank?
        self.name = default_str
      end
    else
      # self.name = "Auto BE: #{default_str}" if self.name.blank? 
      self.name = default_str if self.name.blank?
    end
  end

  def reset(user = nil, conversiontool_id = nil, cvtool_bundle_id = nil, dbs_to_remove = nil)
    return if [BP_NAME_COMPLETE].include?(self.bp_name)    # you can't reset a completed production order
    ProductionOrder.transaction do
      ot = Ordertype.find(self.ordertype_id)
      set_bp_name( ot.process_transitions[0].from_process.name, ot.process_transitions[0].sequence)
      self.production_orderlines.each do |line|
        line.reset(user.user_name, conversiontool_id, cvtool_bundle_id, dbs_to_remove)
      end
      save!
      transition
    end
  end
 
  def log_orderstatus(args= {})
  
    args[:level]    ||= 'PRODUCT'
    session = Thread.current[:session]
    unless session.nil?
      args[:user]     ||= session[:user].user_name
    end
    args[:remarks]  ||= "n.a."
    args[:sequence] ||= 0
    args[:reference]||= "n.a."
    args[:status]   ||= "n.a."
  
    os = Orderstatuslog.new
    os.production_order_id = self.id
  
    os.level =     args[:level]
    os.user  =     args[:user]
    os.remarks =   args[:remarks]
    os.sequence =  args[:sequence]
    os.reference = args[:reference]
    os.status =    args[:status]
    os.completion_date = Time.now
    os.save!
  
  end    

  def execute
    # to do
    # header level autoexecute here
  
    self.production_orderlines.where("parent_linenr is null").each do |line|
      begin
        line.checklog = self.checklog.new_sub("Production orderline #{line.id}", Checklog::LOGLEVEL_WARNING)
        line.autoflow
      rescue => e
        self.checklog.add_error_line("Autoflow production orderline #{line.id} encountered:\r\n" + e.message)
        self.checklog.add_debug_line(e.backtrace.join("\r\n"))
        self.checklog.update_result("Autoflow not completed")
      end
    end
  end

  def self.autoperform
    # Not used anymore
    while true
      self.perform
      puts "Sleeping..."
      sleep 10
    end
  end

  def self.acquire_lock(name)
    lock = SchedulerLock.find_by_process(name)
    if !lock
      lock = SchedulerLock.new
      lock.process = name
      lock.pid = Process.pid
      lock.server = Socket.gethostname
      lock.save
    else
      raise "Process already running at pid :: " + lock.pid.to_s if lock.pid
      lock.pid = Process.pid
      lock.server = Socket.gethostname
      lock.save!
    end
    # touch tempfile for watchdog purposes
    # if no lock could be obtained an exception is raised and this touch is not reached
    FileUtils.touch(File.join("/tmp",name))
  end

  def self.release_lock(name)
    lock = SchedulerLock.find_by_process(name)
    if lock
      raise "Process already running at different pid :: " + lock.pid.to_s if lock.pid && lock.pid != Process.pid
      lock.pid = nil
      lock.save!
    end
  end

  def self.perform
    begin
      acquire_lock("ProductionOrder.perform")
      FileUtils.touch('schedule.run')
      old_logger = ActiveRecord::Base.logger
      ActiveRecord::Base.logger = nil
      orders = ProductionOrder.where("bp_name <> '#{BP_NAME_CANCELLED}' and bp_name <> '#{BP_NAME_ORDERENTRY}' and bp_name <> '#{BP_NAME_COMPLETE}' and id >= 400")
      orders.each do |order|
        order.production_orderlines.each do |line|
          begin
            line.autoflow
          rescue => e
            puts e.message
            puts e.backtrace
          end
        end
      end
      puts "ProductionOrder.perform finished at :: " +  Time.new.localtime.inspect
      ActiveRecord::Base.logger = old_logger
    ensure
      release_lock("ProductionOrder.perform")
    end
  end

  def transition(user = nil, direction = 'forward')
    return if self.end_bp?
    if direction != 'forward'
      transition = self.process_transitions.where(:to_bp => BusinessProcess.find_by_name(self.bp_name).id,:to_level => 'SET').last
      transition = self.process_transitions.where(:to_level => 'SET').order(:sequence).first if transition.nil?
      if transition.from_bp.blank?
        self.bp_seqnr = transition.sequence
      else
        set_bp_name(transition.from_process.name, transition.sequence)
      end
    else
      transition = self.process_transitions.where(:from_bp => BusinessProcess.find_by_name(self.bp_name).id,:from_level => 'SET').last
      transition = self.process_transitions.where(:from_level => 'SET').order(:sequence).first if transition.nil?
      if transition.to_bp.blank?
        self.bp_seqnr = transition.sequence
      else
        set_bp_name(transition.to_process.name, transition.sequence)
      end
    end
    self.checklog.add_attributes({"Business process sequence" => self.bp_seqnr, "Business Process" => self.bp_name}, "executing")
    self.checklog.add_info_line("Production order transitioned #{direction} to business process '#{self.bp_name}'.")
    if self.changed? && save
      if self.checklog.only_check_yn
        self.checklog.add_debug_line("Production order changed: a confirmation email would be sent and the orderstatus logged.")
      else
        Postoffice.po_confirm_work(self.id, self.bp_name).deliver_now
        os = Hash.new
        os[:production_order_id] = self.id
        os[:level]    = transition.from_level
        os[:user]     = user.user_name if user
        os[:sequence] = transition.sequence
        os[:status]   = self.bp_name unless self.bp_name.blank?
        os[:remarks]  = ''
        self.log_orderstatus(os)
      end
    elsif self.changed?
      self.checklog.add_error_line("Production order changed but save failed")
      self.checklog.update_result(Checklog::STATUS_ERROR)
    else
      self.checklog.add_debug_line("No production order change detected.")
    end

    if !transition.cascade_to_bp.blank? && direction == 'forward'
      self.checklog.add_info_line("Cascading transition '#{transition.cascade_to_process.name}' to production orderlines.")

      self.production_orderlines.where("parent_linenr is null").each do |line|
        line.checklog = self.checklog.new_sub("Production orderline #{line.id}", Checklog::LOGLEVEL_WARNING)
        line.set_bp_name(transition.cascade_to_process.name, transition.sequence)
        line.save!

        line.checklog.add_attributes({ "Business process name" => line.bp_name, "Business process sequence" => line.bp_seqnr })
        line.checklog.add_info_line("Production orderline transitioned to status '#{line.bp_name}'.")
      end
    else
      self.checklog.add_debug_line("Transition not cascaded to production orderlines.")
    end
  end

  def self.schedule
    begin
      acquire_lock("ProductionOrder.schedule")
      old_logger = ActiveRecord::Base.logger
      ActiveRecord::Base.logger = nil

      jobs = Job.where("status >= #{Job::STATUS_QUEUED} AND status <= #{Job::STATUS_PROCESSING}")
      # Group the jobs by their type and reference and sort them on project_id so the default project will go first
      jobgroup = jobs.collect{|j|[j.reference_id,j.conversion_id,j.type,j.project_id]}.uniq.sort_by{|gj| gj[3]}
      # Handle each group and start longest job in this group first
      jobgroup.each do |gj|
        group = Job.where(:reference_id => gj[0],:conversion_id => gj[1],:type => gj[2]).order("estimated_duration DESC")
        group.each do |j|
          if j.status == Job::STATUS_PROCESSING
            j.automonitor
          else
            j.autoschedule
          end
        end
      end
      
      ActiveRecord::Base.logger = old_logger
    ensure
      release_lock("ProductionOrder.schedule")
    end
  end

  def self.autoschedule
    # Not used
    while true
      self.schedule
      puts "Sleeping"
      sleep 10
    end
  end

  def cancel
    ProductionOrder.transaction do
      # A production_order can only be cancelled if it contains no finished orderlines.
      # If it does, cancel the remaining lines and put the production_order to 'COMPLETE'
      line_complete_yn = false
      production_orderlines.each do |line|
        line_complete_yn ||= line.bp_name == ProductionOrderline::BP_NAME_COMPLETE
        line.cancel
      end

      self.bp_name = line_complete_yn ? BP_NAME_COMPLETE : BP_NAME_CANCELLED
      save!
    end
  end

   def terminate(user_obj)
    ProductionOrder.transaction do
      self.production_orderlines.each do |line|
         if line.bp_name != ProductionOrderline::BP_NAME_COMPLETE
           line.terminate(user_obj.user_name)
         end
      end
      self.bp_name = BP_NAME_CANCELLED
      os = Hash.new
      os[:production_order_id] = self.id
      os[:level]  = "SET"
      os[:status] = self.bp_name
      os[:user] = user_obj.user_name
      log_orderstatus(os)
      save!
      Postoffice.po_confirm_work(self.id,self.bp_name).deliver
    end
  end

  def process_transitions
    if self.process_transitions_snapshot
      return ProcessTransitionsSnapshot.from_json(self.process_transitions_snapshot)
    else
      return self.ordertype.process_transitions
    end
  end

  def expired?
    bp_name == BP_NAME_DELETED || bp_name == BP_NAME_CANCELLED || (expiry_date && expiry_date <= Date.current)
  end

  def self.expire
    ProductionOrder.where("bp_name != '#{BP_NAME_DELETED}' AND bp_name != '#{BP_NAME_CANCELLED}' AND expiry_date <= ?", Date.current + 7).each do |production_order|
      if production_order.expiry_date <= Date.current
        logger.info(DateTime.now.to_s + ": Expiring production order #{production_order.id}")
        production_order.expire('expiry daemon')
      elsif !production_order.expiry_notification_send
        logger.info(DateTime.now.to_s + ": Sending expiry notification for production order #{production_order.id}")
        Postoffice.expiry_notification(production_order).deliver
        production_order.expiry_notification_send = true
        production_order.save!
      end
    end
  end

  def expire(name_user = 'system')
    ProductionOrder.transaction do
      self.production_orderlines.each do |production_orderline|
        production_orderline.expire(name_user)
      end
      self.bp_name = BP_NAME_DELETED
      save!
    end
  end

  def force_complete(user_name = 'system')
    if self.ordertype.job_support_yn
      raise "force complete can only be used for manual orders."
    else
      ProductionOrder.transaction do
        self.production_orderlines.each do |line|
          line.cancel(user_name)
          line.save!
        end
        set_bp_name(BP_NAME_COMPLETE,self.ordertype.process_transitions.last.sequence)
        log = Hash.new
        log[:production_order_id] = self.id
        log[:level]    = "SET"
        log[:user]     = user_name
        log[:sequence] = self.bp_seqnr
        log[:status]   = self.bp_name
        log[:remarks]  = 'FORCED COMPLETE OF ORDER'
        self.log_orderstatus(log)
        save!
        Postoffice.po_confirm_work(self.id, self.bp_name).deliver_now
      end
    end
  end

  def expiry_date_passed?
    if !self.expiry_date.blank?
      self.expiry_date <= Date.current
    else
      false
    end
  end

  def update_baseline_commitment_date(date, commitment_change_reason, user_obj)
    log = Hash.new
    log[:production_order_id] = self.id
    log[:level]     = 'SET'
    log[:user]      = user_obj.user_name if user_obj
    log[:sequence]  = self.bp_seqnr
    log[:status]    = self.bp_name
    log[:remarks]   = "Baseline commitment update from #{self.baseline_commitment_date.strftime('%d %B %Y') if self.baseline_commitment_date} to #{date.strftime('%d %B %Y')} because of #{commitment_change_reason.reason}"
    self.baseline_commitment_date = date
    self.commitment_dates << CommitmentDate.new(:commitment_type => ProductionOrder::COMMITMENT_BASELINE, :commitment_change_reason_id => commitment_change_reason.id, :new_date => date, :updated_by => user_obj ? user_obj.user_name : 'system')
    self.log_orderstatus(log)
    save!
  end

  def update_modified_commitment_date(date, commitment_change_reason, user_obj)
    log = Hash.new
    log[:production_order_id] = self.id
    log[:level]     = 'SET'
    log[:user]      = user_obj.user_name if user_obj
    log[:sequence]  = self.bp_seqnr
    log[:status]    = self.bp_name
    log[:remarks]   = "Modified commitment update from #{self.modified_commitment_date.strftime('%d %B %Y') if self.modified_commitment_date} to #{date.strftime('%d %B %Y')} because of #{commitment_change_reason.reason}"

    self.modified_commitment_date = date
    self.commitment_dates << CommitmentDate.new(:commitment_type => ProductionOrder::COMMITMENT_MODIFIED, :commitment_change_reason_id => commitment_change_reason.id, :new_date => date, :updated_by => user_obj ? user_obj.user_name : 'system')
    self.log_orderstatus(log)
    save!
  end

  def update_expiry_date(date,user_obj)
    log = Hash.new
    log[:production_order_id] = self.id
    log[:level]    = "SET"
    log[:user]     = user_obj.user_name if user_obj
    log[:sequence] = self.bp_seqnr
    log[:status]   = self.bp_name
    log[:remarks]  = "Expiry date updated from #{self.expiry_date.strftime("%d %B %Y") if self.expiry_date} to #{date.strftime("%d %B %Y")}"

    self.expiry_date = date
    self.expiry_notification_send = false
    self.log_orderstatus(log)
    save!
  end

  def allow_edit?
    # Production orders should not be editable unless still in 'draft' mode, a mode which is currently not really used
    # So probably the edit can be removed all together
    self.start_bp?
  end
end
