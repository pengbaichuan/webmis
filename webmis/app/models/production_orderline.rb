class ProductionOrderline < ActiveRecord::Base

  belongs_to  :production_order
  belongs_to  :product_line
  has_many    :conversions
  has_many    :conversion_jobs, :through => :conversions
  has_many    :documents
  has_many    :test_requests
  has_many    :production_orderline_conversion_databases
  has_many     :conversion_databases, :through => :production_orderline_conversion_databases
  has_many    :outbound_orderlines
  has_one    :product_release_note
  belongs_to  :chosen_conversion, :class_name => "Conversion", :foreign_key =>  "chosen_conversion_id"
  has_one :stock
  belongs_to :product_location
  has_many    :customer_contents, :dependent => :destroy
  belongs_to :parent, :class_name => "ProductionOrderline", :foreign_key => "parent_linenr"
  after_initialize :defaults
  after_save  :auto_log_orderstatus

  attr_accessor :checklog

  BP_NAME_EMPTY        = ''   # used for derived production orderlines
  BP_NAME_CREATED      = 'CREATED'
  BP_NAME_COMPLETE     = 'COMPLETE'
  BP_NAME_CANCELLED    = 'CANCELLED'

  BP_STATUS_ACTIVE     = 100
  BP_STATUS_PROCESSING = 200
  BP_STATUS_FAILED     = 300
  BP_STATUS_COMPLETE   = 400

  @@bp_status_array    = Array.new
  @@bp_status_array[BP_STATUS_ACTIVE]     = 'Active'
  @@bp_status_array[BP_STATUS_PROCESSING] = 'Processing'
  @@bp_status_array[BP_STATUS_FAILED]     = 'Failed'
  @@bp_status_array[BP_STATUS_COMPLETE]   = 'Complete'

  comma :on_time_delivery_report do
    production_order :id => 'Production Order Id'
    production_order :name => 'Production Order Name'
    production_order :bp_name => 'Production Order Status'
    id 'Production Orderline Id'
    product :name => 'Product'
    bp_name 'Orderline Status'
    product_line :name => 'Product Line'
    production_order :baseline_commitment_date => 'Baseline Committed'
    production_order :modified_commitment_date => 'Modified Committed'
    ship_date 'Shipping Date'
  end

  def checklog
    @checklog ||= Checklog.new(only_check_yn = false, Checklog::LOGLEVEL_DEBUG)
  end

  def defaults
    self.product_accepted  ||= 'Unknown'
    self.customer_feedback ||= 'n.a.'
    self.bp_name           ||= BP_NAME_CREATED if self.new_record?
    self.bp_seqnr          ||= 0
    self.bp_status         ||= BP_STATUS_COMPLETE  if self.new_record?
  end

  def bp_status_str
    @@bp_status_array[bp_status]
  end

  def self.bp_status_array
    return @@bp_status_array
  end

  def self.bp_status_hash
    # for an option list of all possible business process statuses
    s_hash = Hash.new
    bp_status_array.each{ |s| s_hash[s] = @@bp_status_array.index(s) unless s.nil? }
    s_hash.sort_by{ |k,v| v }
  end

  def bp_status_hash
    # for an option list of all valid  business process statuses for the current business process and business process status.
    s_hash = Hash.new
    @@bp_status_array.each{ |s| s_hash[s] = @@bp_status_array.index(s) unless (s.nil?  || ! self.allow_bp_status?(@@bp_status_array.index(s))) }
    s_hash.sort_by{ |k,v| v }
  end

  def allow_bp_status_change?(new_bp_status, force = false)
    # bp_status is intended for the normal work flow within a business process step. The force flag is use to allow failed steps to complete
    # or to initialize the bp_status when the business process moves to the next step. It indicates the calling method knows what it is doing
    result = force ||
        case new_bp_status
          when BP_STATUS_ACTIVE
            # BP_STATUS_ACTIVE is the begin status of each business process step so it is only allowed when the next (or first) business process step is set
            false
          when BP_STATUS_PROCESSING
            # BP_STATUS_PROCESSING indicates that the user has to wait till WebMIS is finished with the current (automatic) business process step
            [BP_STATUS_ACTIVE, BP_STATUS_FAILED].include?(self.bp_status)
          when BP_STATUS_FAILED
            # BP_STATUS_FAILED indicates the automatic handling (e.g. BP_STATUS_PROCESSING) failed and the user needs to take action
            [BP_STATUS_PROCESSING].include?(self.bp_status)
          when BP_STATUS_COMPLETE
            # BP_STATUS_COMPLETE indicates the current business process step is complete and the next one (if any) can be started
            [BP_STATUS_ACTIVE, BP_STATUS_PROCESSING]
        end
    result
  end

  def allow_bp_status(new_bp_status, force = false)
    self.bp_status == new_bp_status || allow_status_change?(new_bp_status, force)
  end

  def set_bp_status(new_bp_status, force = false)
    result = allow_bp_status_change?(new_bp_status, force)
    self.bp_status = new_bp_status if result
    result
  end

  def start_bp?
    [BP_NAME_CREATED].include?(self.bp_name)
  end

  def end_bp?
    [BP_NAME_COMPLETE, BP_NAME_CANCELLED].include?(self.bp_name)
  end

  def start_or_end_bp?
    start_bp? || end_bp?
  end

  def set_bp_name(new_bp_name, new_bp_seqnr, bp_name_force = false, user_name = nil)
    if bp_name_force || self.bp_status == BP_STATUS_COMPLETE || !self.production_order.ordertype.job_support_yn
      self.bp_name = new_bp_name
      self.bp_seqnr = new_bp_seqnr
      self.owner = user_name if user_name
      # the begin and end business processes go directly to status Complete, the others start in status Active
      new_bp_status = start_or_end_bp? ? BP_STATUS_COMPLETE : BP_STATUS_ACTIVE
      # always force the new business process status
      set_bp_status(new_bp_status, bp_status_force = true)
    end
    # return whether successful
    self.bp_name == new_bp_name
  end

  def model
    self.product_design
  end

  def design_hash
    JSON.parse(self.product_design)
  end
  
  def product
    Product.find_by_real_volumeid(self.product_id) if self.product_id
  end

  def cancel(user_name = 'system')
    return if [BP_NAME_COMPLETE, BP_NAME_CANCELLED].include?(self.bp_name)

    ProductionOrderline.transaction do
      set_bp_name(BP_NAME_CANCELLED, bp_seqnr = 0, force = true, user_name)
      self.conversions.each do |conv|
        conv.cancel(user_name)
      end
      save!
      Postoffice.production_orderline_status_change(self).deliver_now
    end
  end

  def terminate(user_name = 'system')
    return if [BP_NAME_COMPLETE, BP_NAME_CANCELLED].include?(self.bp_name)

    ProductionOrderline.transaction do
      set_bp_name(BP_NAME_CANCELLED, bp_seqnr = 0, force = true, user_name)
      self.chosen_conversion_id = nil
      self.product_location_id = nil
      self.conversions.each do |conv|
        conv.terminate(user_name)
      end
      save!
      Postoffice.production_orderline_status_change(self).deliver
    end
  end

  def reset(user_name = 'system', conversiontool_id = nil, cvtool_bundle_id = nil, dbs_to_remove = nil)
    return if [BP_NAME_COMPLETE].include?(self.bp_name)    # you can't reset completed production orderlines, but you can reset cancelled ones!

    ProductionOrderline.transaction do
      self.conversions.current.each do |conv|
        new_conv = conv.duplicate
        new_conv.conversiontool_id = conversiontool_id if conversiontool_id
        new_conv.cvtool_bundle_id = cvtool_bundle_id if cvtool_bundle_id
        new_conv.additional_info += " Initiated with order-line reset by #{user_name}."
        new_conv.set_status(Conversion::STATUS_REQUESTED)
        new_conv.data_files = ''
        new_conv.save!
        new_conv.reload
        conv.terminate(user_name, dbs_to_remove)
        conv.replaced_by = new_conv
        conv.remarks = "#{conv.remarks} Cancelled due to order-line reset by #{user_name}.".strip
        conv.save!
        self.conversions << new_conv
      end
      self.set_bp_name(BP_NAME_CREATED, bp_seqnr = self.bp_seqnr)
      self.bp_message = "Production orderline reset by #{user_name}."
      save!
      first_transition
    end
  end

  def auto_log_orderstatus
    if self.bp_name_changed?
      os = Hash.new
      os[:sequence] = self.bp_seqnr
      os[:status] = self.bp_name
      os[:user] = self.owner
      os[:reference] = self.product_id
      os[:remarks] = self.id.to_s
      ProductionOrder.find(self.production_order.id).log_orderstatus(os)
    end
  end

  def get_next_conversion
    conversions.still_open.order("design_sequence").first
  end

  def auto_conversion
    # bp_name == 'AUTO CONVERSION', check bp_status
    return unless self.bp_status == BP_STATUS_PROCESSING || set_bp_status(BP_STATUS_PROCESSING)

    unless self.product_design
      self.checklog.update_result("ERROR", Checklog::LOGLEVEL_ERROR)
      self.checklog.add_error_line("No product design available while running an automated work flow.")
      set_bp_status(BP_STATUS_FAILED)
    end
    self.checklog.add_debug_line("Performing 'auto_conversion'.")
    info_messages = []
    messages = []
    active_detected = false

    self.conversions.still_open.each do |conversion|
      self.checklog.add_debug_line("Handling conversion '#{conversion.id}'.")
      conversion.checklog = self.checklog.new_sub("Conversion #{conversion.id}", Checklog::LOGLEVEL_WARNING)
      from_to = "(" + conversion.input_format.to_s + "-" + conversion.output_format.to_s + ")"
      if conversion.status <= Conversion::STATUS_REQUESTED
        begin
          conversion.checklog.add_debug_line("Performing allocate data.")
          conversion.allocate_data
        rescue => e
          msg = "Conversion #{conversion.id} #{from_to} ALLOCATE DATA :\r\n" + e.message
          messages << msg
          conversion.checklog.add_info_line(msg)
          next
        end

        begin
          conversion.checklog.add_debug_line("Performing add metadata.")
          conversion.add_metadata
        rescue => e
          msg = "Conversion #{conversion.id} ADD METADATA :\r\n"  + e.message
          messages << msg
          conversion.checklog.add_info_line(msg)
          next
        end
      else
        conversion.checklog.add_debug_line("Conversion has status #{conversion.status_str}, skipping allocate data, allocate bundle and add metadata.")
      end
      
      begin
        conversion.checklog.add_debug_line("Performing queue_conversion.")
        results = conversion.queue_conversion
        info_messages = messages + results
      rescue IsActiveRuntimeError => e2
        active_detected = true
        self.bp_message = e2.message
        messages << e2.message
        self.checklog.update_result(self.bp_status_str)
      rescue => e
        msg = "Conversion #{conversion.id} #{from_to}  QUEUE CONVERSION :\r\n" + e.message
        messages << msg
        conversion.checklog.add_info_line(msg)
        next
      end
            
      begin
        conversion.checklog.add_debug_line("Performing monitor_conversion.")
        result = conversion.monitor_conversion
        info_messages << "Conversion #{conversion.id} #{from_to}  COMPLETE (" + result + ")"
      rescue IsActiveRuntimeError => e2
        active_detected = true
        self.bp_message = e2.message
        messages << e2.message
        self.checklog.update_result(self.bp_status_str)
      rescue => e
        msg = "Conversion #{conversion.id} #{from_to} MONITOR CONVERSION :\r\n" + e.message
        messages << msg
        conversion.checklog.add_info_line(msg)
        next
      end
    end

    if messages.size > 0
      self.checklog.add_info_line("auto_conversion could not yet finish all conversions.")
      if active_detected
        save!
        raise IsActiveRuntimeError.new(messages.join("\r\n"))
      else
        save!
        raise messages.join("\r\n")
      end
    else
      self.bp_message = info_messages.join("\r\n") if info_messages.size > 0
      set_bp_status(BP_STATUS_COMPLETE)
      self.checklog.add_info_line("auto_conversion successfully finished all conversions.")
    end
    save!
  end

  def autoflow
    # kickoff automated product workflow - handle exception if fail
    begin
      # only use autoflow for business processes with automation support
      unless BusinessProcess.find_by_name(bp_name).system_support_yn
        self.checklog.add_debug_line("Autoflow: '#{bp_name}' is a manual step, skipping autoflow")
        return
      end

      # only use autoflow if the business process has not finished yet
      return if self.bp_status == BP_STATUS_COMPLETE

      # ProductionOrderline.transaction do
      function_name = bp_name.gsub(" ","_").downcase
      self.checklog.add_debug_line("Autoflow function_name: '#{function_name}'")
      if self.respond_to?(function_name)
        self.checklog.add_debug_line("Autoflow executing: '#{function_name}'")
        self.owner = 'SYSTEM'
        send(function_name)
        # this assumes synchronous
        if self.bp_status == BP_STATUS_PROCESSING
          self.checklog.add_debug_line("Autoflow: Performing '#{function_name} asynchronously")
        elsif self.bp_status == BP_STATUS_ACTIVE
          self.checklog.add_debug_line("Autoflow: Failed to start '#{function_name} asynchronously")
        elsif self.bp_status == BP_STATUS_FAILED
          self.checklog.add_debug_line("Autoflow: '#{function_name} failed")
        else # bp_status == BP_STATUS_COMPLETE
          self.checklog.add_debug_line("Autoflow finished: '#{function_name}'")
          self.bp_message = (self.bp_name == BP_NAME_COMPLETE ? 'Autoflow: completed' : '')
          save!
          transition
        end
      else
        # support autoflow conversion level
        conversion = get_next_conversion
        if conversion && conversion.respond_to?(function_name)
          conversion.checklog = self.checklog.new_sub("Conversion #{conversion.id}", Checklog::LOGLEVEL_WARNING)
          self.checklog.add_debug_line("Performing '#{function_name}' on conversion '#{conversion.id}'")
          self.set_bp_status(BP_STATUS_PROCESSING)
          self.owner = "SYSTEM"
          self.save!
          conversion.send(function_name)
          self.set_bp_status(BP_STATUS_COMPLETE)
          self.checklog.add_info_line("Performed '#{function_name}' on conversion '#{conversion.id}'")
          begin
            self.checklog.add_debug_line("Starting transition from autoflow.")
            
            transition
            self.checklog.add_debug_line("Starting nested autoflow.")
            autoflow
            self.checklog.add_debug_line("Finished nested autoflow.")
          rescue => e
            self.owner = "USER"
            self.checklog.add_info_line("No transition found.")
            self.checklog.add_info_line(e.message)
            self.checklog.add_backtrace_line(e.backtrace.join("\r\n"))
          end
        else
          # no applicable conversions or automation support business process not implemented yet
          self.checklog.add_debug_line("Autoflow found no conversion with work, try next transition.")
          begin
            set_bp_status(BP_STATUS_COMPLETE)
            transition
          rescue => e
            self.owner = "USER"
            to_console_yn = true
            self.checklog.add_info_line("No transition found.", to_console_yn)
            self.checklog.add_info_line(e.message, to_console_yn)
            self.checklog.add_backtrace_line(e.backtrace.join("\r\n"), to_console_yn)
          end
        end
      end
    rescue IsActiveRuntimeError => e2
      # conversion is still running
      self.bp_message = e2.message
      self.checklog.update_result(self.bp_status_str)
    rescue NoConversionDatabaseFoundError => e1
      # conversion still waiting for certain databases to become available
      self.checklog.add_info_line(e1.message)
      self.bp_message = e1.message
      self.checklog.update_result('WARNING')
    rescue => e
      unless e.message =~ /\AConversion/
        # the conversion exceptions are already in the conversion checklogs
        self.checklog.add_error_line(e.message)
        self.checklog.add_backtrace_line(e.backtrace.join("\r\n"))
        # only set the status to failed if the exception is in the production orderline
        set_bp_status(BP_STATUS_FAILED)
        self.owner = "USER"
      end
      self.bp_message = e.message
      self.checklog.update_result(self.bp_status_str)
    end
    save!
  end

  def first_transition
    # find the transition the production_order last performed and redo its cascade_to business process
    transition = self.production_order.ordertype.process_transitions.where("sequence = #{self.production_order.bp_seqnr}").first
    if transition && !transition.cascade_to_bp.blank?
      self.set_bp_name(transition.cascade_to_process.name, transition.sequence, force = true)
      save!
    end
  end

  def transition(user = nil, context = nil, used_checklog = nil)
    # you can only do a transition if the current business process step is complete
    if  bp_status != BP_STATUS_COMPLETE && self.production_order.ordertype.job_support_yn
      return
    end

    new_transition = nil
    nexttransitions = self.production_order.process_transitions.where(:from_bp => BusinessProcess.find_by_name(self.bp_name).id).order(:sequence).all
    if nexttransitions.empty?
      # no workflow transition from the current business process found
      # check whether we are in an end business process
      return unless self.production_order.process_transitions.where(:to_bp => BusinessProcess.find_by_name(self.bp_name).id).empty?
    end    
    nexttransitions = self.production_order.process_transitions.where(:to_level => 'PRODUCT').order(:sequence).all if nexttransitions.nil? || nexttransitions.size == 0 #first process
    if nexttransitions.size > 1
      if context
        instance_variable_set("@"+context.class.name.downcase,context)
        nexttransitions.each do |t|
          if !t.validation.blank?
            if eval(t.validation)
              new_transition = t
            end
          else
            new_transition = t
          end
        end
      else
	      # if multiple transitions found - choose the success transition if available
        nexttransitions.each do |transition|
          if transition.succes_transition
            new_transition = transition
            break
          end
        end
        new_transition = nexttransitions.first if !new_transition
      end
    else
      new_transition = nexttransitions.first
    end

    set_bp_name(new_transition.to_process.name, self.bp_seqnr = new_transition.sequence) unless new_transition.nil?
    save!
    reload

    if process_transition && !process_transition.cascade_to_bp.blank? && process_transition.cascade_to_level == "SET"
      if self.production_order.production_orderlines.where("parent_linenr is null").collect{ |l| l.bp_name }.uniq.size == 1
        po = self.production_order
        po.set_bp_name(BusinessProcess.find(self.process_transition.cascade_to_bp).name, self.process_transition.sequence)
        po.save!
        if self.checklog.only_check_yn
          self.checklog.add_debug_line("Production orderline changed: a confirmation email would be sent and the orderstatus logged.")
        else
          Postoffice.po_confirm_work(po.id,po.bp_name).deliver # not for checker so send mail
          os = Hash.new
          os[:production_order_id] = self.id
          os[:level]    = self.process_transition.from_level
          os[:user]     = user.user_name if user
          os[:sequence] = self.process_transition.sequence
          os[:status]   = self.process_transition.to_process.name
          os[:remarks]  = ''
          po.log_orderstatus(os)
        end
      end
    end
  end

  def process_transition
    if self.bp_seqnr.blank? || self.bp_seqnr == 0
      true
    else
      self.production_order.ordertype.process_transitions.where(:sequence => self.bp_seqnr).last
    end
  end

  def action_link
    if self.production_order.allow_production == true && self.bp_name != BP_NAME_CANCELLED && self.bp_name != BP_NAME_CREATED
      @business_process = BusinessProcess.find_by_name(self.bp_name)
      if @business_process
        if (@business_process.line_link_action.blank? && @business_process.line_link_controller.blank?)
          { :options =>{:action => 'complete_work_product',
              :poid => self.id, :id => self.production_order.id},:body => ''}
        else
          { :options =>{:action => @business_process.line_link_action,:controller => @business_process.line_link_controller,
              :production_orderline_id => self.id,:return_to_controller => 'production_orders',
              :return_to_action => 'manage',:return_to_id => self.production_order.id},:body => @business_process.name.humanize}
        end
      end
    end
  end

  def update_bp_message(status = nil, user = nil)
    if status
      self.bp_message = status
      self.owner = user.full_name if user
      save!
    end
  end

  def percentage_done_in_transition
    percentage = 0.0
    sequence_a = self.production_order.ordertype.process_transitions.order(:sequence).collect{|pt|pt.sequence}
    if sequence_a.size > 0 && !self.bp_seqnr.blank? && self.bp_seqnr > 0 && sequence_a.index(self.bp_seqnr)
      percentage_i = sequence_a.size / 100.0 * sequence_a.index(self.bp_seqnr) * 100
      percentage = percentage_i if percentage_i
    end
    return percentage
  end

  def find_customer_content_specification(specification)
    self.customer_contents.where(:customer_content_specification_id => specification.id).first rescue nil
  end

  def generate_customer_contents
    customer_contents_checklog = self.checklog.new_sub("Customer contents", Checklog::LOGLEVEL_WARNING)
    customer_contents_checklog.add_info_line("Generating customer content.")

    customer_contents_checklog.add_debug_line("Old content removed.") if self.customer_contents && self.customer_contents.size > 0
    self.remove_customer_content  # clean up, just to be sure

    self.product.customer_contents.each do |customer_content|
      customer_content_clone = customer_content.dup
      customer_content_clone.product_id = nil
      customer_content_clone.production_orderline = self
      customer_content_clone.save!
      log_targets = []
      customer_content.customer_targets.each do |customer_target|
        customer_target_clone = customer_target.dup
        customer_target_clone.customer_content = customer_content_clone
        customer_target_clone.save!
        log_targets << CustomerContentDefinition.outputarray[customer_target.output_target]
      end
      customer_contents_checklog.add_attributes({"#{customer_content_clone.customer_content_specification.name}" => "'#{customer_content_clone.value}' (#{log_targets.join(',')})"})
    end

    if self.product.customer_contents.size > 0
      customer_contents_checklog.add_debug_line("New content saved.")
    else
      customer_contents_checklog.add_info_line("No content found.")
    end
  end

  def remove_customer_content
    self.customer_contents.each do |content|
      content.destroy
    end
  end

  def customer_contents_for_release_notes?
    return self.customer_contents.inject(false){ |included, cc| included || cc.customer_targets.where(:output_target => CustomerContentDefinition::OUTPUT_TARGET_RELEASE_NOTES).size > 0}
  end

  def region_mapping_hash
    mapping = {}
    if self.product
      if self.product.product_content_specification
        self.product.product_content_specification.product_content_items.each do |m|
          if m.region
            data_type = m.data_type ? m.data_type.name.to_s.downcase : ""
            mapping[[m.filling.to_s.downcase, m.region.region_code.to_s.downcase, data_type]] = [] if !mapping[[m.filling.to_s.downcase, m.region.region_code.to_s.downcase, data_type]]
            mapping[[m.filling.to_s.downcase, m.region.region_code.to_s.downcase, data_type]] << m.destination_region_code
          else
            raise "No region defined in region mapping in region_mapping_line #{m.id}"
          end
        end
      end
    end
    return mapping
  end

  def mastering
    # bp_name == 'MASTERING', check bp_status
    return unless self.bp_status == BP_STATUS_PROCESSING || set_bp_status(BP_STATUS_PROCESSING)

    self.checklog.add_info_line("Performing 'mastering'.")
    # TO-DO: Optional mastering job, similar to the packaging job, for those cases where the mastering is not done in the last dakota conversionstep.
    # for now mastering is always successful.
    set_bp_status(BP_STATUS_COMPLETE)
    save!
    self.checklog.add_info_line("Mastering done.")
  end

  def release
    # bp_name == 'RELEASE', check bp_status
    return unless self.bp_status == BP_STATUS_PROCESSING || set_bp_status(BP_STATUS_PROCESSING)

    self.checklog.add_info_line("Performing release.")
    unless self.product_release_note
      # only generate the production_orderline release note if it has not already been generated (for instance manually)
      self.checklog.add_debug_line("Generating release notes.")
      release_note = ProductReleaseNote.new
      release_note.production_orderline = self
      release_note.status = ProductReleaseNote::STATUS_DRAFT
      release_note.save!
      ContentReleaseNote.prepare_default_content(self.production_order.ordertype.product_category_id, release_note.id)
      reload
    end
    # we have a release note, checking the status
    case self.product_release_note.status
      when ProductReleaseNote::STATUS_DRAFT
        self.checklog.add_debug_line("Release notes still in status draft, awaiting manual action.")
        raise "Product release note for #{self.product_id} requires review."
      when ProductReleaseNote::STATUS_REVIEW
        self.checklog.add_debug_line("Release notes still in status review, awaiting manaul action.")
        raise "Product release note for #{self.product_id} awaits review."
      when ProductReleaseNote::STATUS_APPROVED
        self.checklog.add_debug_line("Release notes approved, start generation of the package with the release notes.")
        ProductReleaseNotePdf.new(self.product_release_note).generate
        self.product_release_note.status = ProductReleaseNote::STATUS_RELEASED
        self.product_release_note.save!
        set_bp_status(BP_STATUS_COMPLETE)
        save!
        self.checklog.add_info_line("Release notes stored.")
        #raise "Product release notes generated, awaiting product registration."
      when ProductReleaseNote::STATUS_RELEASED
        self.checklog.add_debug_line("Release notes finished.")
        set_bp_status(BP_STATUS_COMPLETE)
        save!
        # TODO: probably need some code for a manual (or automatic) release decision
      else
        self.checklog.add_error_line("Release notes has an unsupported status, awaiting manual correction.")
        raise "Product release for self.product_id has an unsupported status of self.product_release_note.status_string (self.product_release_note.status)."
    end

  end

  def get_product_location_dir
    rs = 0
    rs = self.product_location.release_sequence if self.product_location && self.product_location.release_sequence
    return File.join(ConfigParameter.get("product_location_dir",self.production_order.project_id.to_s),self.product_location.product_id,"R#{'%.2i' % self.product_location.release_sequence}")
  end

  def find_mastered_cd
    # we have no proper way to identify the mastered data at the moment
    # so for now just take the latest generated conversion database, as the mastering should be the latest conversion job.
    #self.conversion_databases.available_no_intermediate.last
    Conversion.find(self.chosen_conversion_id).conversion_databases.available.last
  end

  def post_register_product
    ProductionOrderline.transaction do
      reload

      return unless self.bp_name == 'REGISTER PRODUCT' && self.bp_status == BP_STATUS_PROCESSING

      if self.product_release_note && self.product_release_note.status == ProductReleaseNote::STATUS_RELEASED
        register_release_notes   # another delayed job, will call post_release_copy upon completion.
      else
        # trigger related outbound_orderlines
        post_release_copy
      end
    end
  end

  def register_product
    # bp_name == 'REGISTER PRODUCT', check bp_status
    return unless self.bp_status == BP_STATUS_PROCESSING || set_bp_status(BP_STATUS_PROCESSING)

    ProductLocation.transaction do
      # if we already have a product_location as placeholder, take it, otherwise create a new one
      self.product_location = ProductLocation.new unless self.product_location
      # map_from_cv links the product_location to the conversion it belongs to.
      # the three map_from functions are not the best way to fill the product_location, but they are already in use
      # for the manual product registration. By using one of them here the manual and automated method will be consistent
      # Besides, the registered_product contains a lot of superfluous attributes (when looking at it from the automated
      # view), so a proper evaluation of what should really be contained in this class might be in order but falls outside
      # the scope of the current edit. For now the behaviour of the manual creation is copied.
      cd = find_mastered_cd
      self.product_location.map_from_cv(cd.conversion,product_location.product_id)
      src = File.dirname(cd.path_and_file)
      dest = get_product_location_dir
      self.product_location.storage_path = File.join(dest, File.basename(src))
      # check if outbound orderline is found

      self.product_location.save!
      ol = OutboundOrderline.find_by_product_location_id(self.product_location.id)
      if ol
        packings = ol.packings
        ol.filename =  File.join(dest, File.basename(src)) if !ol.filename
        if packings[0]
          packings[0].filename = File.basename(src) + ".tar.gz" if !packings[0].filename
          packings[0].save!
        end
        ol.save!
      end
      
      self.product_design = nil
      save!
      rsynccopy(src,dest,'post_register_product')
    end
  end

  def register_release_notes
    # sanity check, make sure the business process is still the current one and in progress
    return unless self.bp_name == 'REGISTER PRODUCT' && self.bp_status == BP_STATUS_PROCESSING

    # puts zip file containing the release notes and its attachments in the product_location directory
    dest = get_product_location_dir
    document = self.product_release_note.document.get_latest_version
    tmpfile = File.join("/tmp/", document.filename)
    File.open(tmpfile, "wb") do |file|
      file.write(document.binary_data)
    end
    rsynccopy(tmpfile, dest, "post_release_copy")
  end

  def post_release_copy
    reload

    # sanity check, make sure the business process is still the current one and in progress
    return unless self.bp_name == 'REGISTER PRODUCT' && self.bp_status == BP_STATUS_PROCESSING

    self.outbound_orderlines.each do |ol|
      if ol.product_location && !ol.product_location.storage_path.to_s.blank?
        # the product_location is filled and no longer just a placeholder
        ol.amount = 1
        ol.save!
        if ol.outbound_orderlines.any?
          # handle release notes, we assume for now the first child will be the release notes as the release notes are the only
          # child outbound_orderlines currently created. If necessary we could also check on the productname of the outbound_orderline
          rol = ol.outbound_orderlines.first
          rol.amount = 1
          rol.filename = File.join(get_product_location_dir, self.product_release_note.document.get_latest_version.filename)
          rol.save!
        end
        ol.outbound_order.check_amounts
      end
    end
    # step completed - transition to next step
    # needed because of async process
    set_bp_status(BP_STATUS_COMPLETE)
    transition
  end

  def ship_date
    if bp_name != ProductionOrderline::BP_NAME_CANCELLED
      outbound_orderlines.collect{|ool| ool.outbound_order.send_date}.compact.min
    end
  end

  DELIVERY_ON_TIME          =  9
  DELIVERY_PLUS_ONE_WEEK    =  8
  DELIVERY_PLUS_TWO_WEEKS   =  7
  DELIVERY_PLUS_THREE_WEEKS =  6
  DELIVERY_PLUS_FOUR_WEEKS  =  5
  DELIVERY_PLUS_FIVE_WEEKS  =  4
  DELIVERY_PLUS_SIX_WEEKS   =  3
  DELIVERY_NOT_SHIPPED      =  2
  DELIVERY_ERROR            =  1
  DELIVERY_NO_COMMIT_DATE   =  0

  @@delivery_category_array = Array.new
  @@delivery_category_array[DELIVERY_ON_TIME]           = 'on time'
  @@delivery_category_array[DELIVERY_PLUS_ONE_WEEK]     = '+1 week'
  @@delivery_category_array[DELIVERY_PLUS_TWO_WEEKS]    = '+2 weeks'
  @@delivery_category_array[DELIVERY_PLUS_THREE_WEEKS]  = '+3 weeks'
  @@delivery_category_array[DELIVERY_PLUS_FOUR_WEEKS]   = '+4 weeks'
  @@delivery_category_array[DELIVERY_PLUS_FIVE_WEEKS]   = '+5 weeks'
  @@delivery_category_array[DELIVERY_PLUS_SIX_WEEKS]    = '>= +6 weeks'
  @@delivery_category_array[DELIVERY_NOT_SHIPPED]       = 'not shipped yet'
  @@delivery_category_array[DELIVERY_ERROR]             = 'unknown'
  @@delivery_category_array[DELIVERY_NO_COMMIT_DATE]    = ''

  def self.delivery_categories
    return @@delivery_category_array
  end

  def on_time_delivery_category(use_baseline_commitment = false)
    commitment_date = use_baseline_commitment ? production_order.baseline_commitment_date : production_order.modified_commitment_date if production_order
    if ship_date && commitment_date
      case (ship_date - commitment_date.to_date).to_i
        when -10000..0
          DELIVERY_ON_TIME
        when 1..7
          DELIVERY_PLUS_ONE_WEEK
        when 8..14
          DELIVERY_PLUS_TWO_WEEKS
        when 15..21
          DELIVERY_PLUS_THREE_WEEKS
        when 22..28
          DELIVERY_PLUS_FOUR_WEEKS
        when 29..35
          DELIVERY_PLUS_FIVE_WEEKS
        else
          DELIVERY_PLUS_SIX_WEEKS
      end
    elsif bp_name == BP_NAME_COMPLETE && commitment_date
      DELIVERY_ERROR
    elsif commitment_date
      DELIVERY_NOT_SHIPPED
    else
      DELIVERY_NO_COMMIT_DATE
    end
  end

  def expired?
    self.bp_name == BP_NAME_CANCELLED || (self.production_order && self.production_order.expired?)
  end

  def expire(name_user = nil)
    if self.bp_name == BP_NAME_CANCELLED
      # finished
    elsif self.bp_name == BP_NAME_COMPLETE
      self.conversions.each do |conversion|
        conversion.expire(name_user)
      end
    else
      # production orderline is still in progress, cancel it
      self.cancel
    end
  end

  def ssh_localhost(cmd)
    stdout = ""
    user = ENV['prod_user']
    server = 'localhost'
    Net::SSH.start( server , user) do |session|
      session.exec!(cmd) do |channel, stream, data|
        stdout << data if stream == :stdout
      end
      session.close
      return stdout
    end
  end

  def rsynccopy(src, dest, post_method=nil)
    Delayed::Worker.logger.debug("Registering product for production orderline #{self.id.to_s}")
    rsync_cmd = "mkdir -p #{dest} && rsync --partial -arv --no-o --no-g #{src} #{dest} 2>&1 ; echo exit_code:$?"
    result = ssh_localhost(rsync_cmd).chomp
    Delayed::Worker.logger.debug(result)
    exit_code = result.scan(/exit_code:(.*)/).flatten[0]
    if exit_code.to_i != 0
      result_str = "Registering production orderline #{self.id.to_s} FAILURE: Rsync copy failed " + src + " to " + dest + " at " + DateTime.now.to_s
      Delayed::Worker.logger.error(result_str)
      raise result_str
    else
      Delayed::Worker.logger.info("Registering production orderline #{self.id.to_s} SUCCESS: Rsync copy completed " + src + " to " + dest +  " at " + DateTime.now.to_s)
    end

    if post_method
      Delayed::Worker.logger.info("Registering production orderline #{self.id.to_s} RSYNCCOPY Performing Post Processing : " + post_method)
      self.send(post_method)
      Delayed::Worker.logger.info("Registering production orderline #{self.id.to_s} RSYNCCOPY Performing Post Processing : " + post_method + " completed")
    end
  end

  def create_outbound_order
    shipping_specification = self.product.product_line.shipping_specification rescue nil
    if shipping_specification
      # we have a shipping specification, so we can generate the outbound order
      ProductionOrderline.transaction do

        # first step, create a production_location as placeholder for the final registered product
        pl = ProductLocation.new
        pl.storage_path = "-" # must have a value
        pl.product_id = self.product_id
        pl.remarks = "Autogenerated placeholder for production order #{self.production_order_id} (#{self.production_order.name})"
        pl.save!

        # step two, create the outbound_order
        oo = OutboundOrder.new
        oo.status = OutboundOrder::STATUS_PLACEHOLDER
        oo.remarks = remarks
        if shipping_specification.external_contact_id
          # TODO: Move this code to generic methods in outbound_order.rb
          external_contact = ExternalContact.find(shipping_specification.external_contact_id)
          oo.contact_id = shipping_specification.external_contact_id
          oo.contact_person = [external_contact.first_name, external_contact.last_name].join(" ")
          oo.shipto_id = shipping_specification.external_contact_id
          oo.shipto_name = external_contact.company.name
          oo.shipto_postalcode = external_contact.company.shipping_address_postalcode
          oo.shipto_streetname = external_contact.company.shipping_address_street
          oo.shipto_city = external_contact.company.shipping_address_city
          oo.shipto_state = external_contact.company.shipping_address_state
          oo.shipto_country = external_contact.company.shipping_address_country
        end
        oo.type = shipping_specification.shipment_type
        oo.courier = shipping_specification.courier
        oo.file_transfer_account_id = shipping_specification.file_transfer_account_id
        oo.distribution_list_id = self.production_order.distribution_list_id || shipping_specification.distribution_list_id
        oo.save!

        # step three, create the outbound_orderline containing this product
        ol = OutboundOrderline.new
        ol.outbound_order_id = oo.id
        ol.product_id = self.product_id
        ol.product_location_id = pl.id
        ol.product_name = self.product.name
        ol.amount = 0 # 0 to indicate it is not available yet
        ol.production_orderline_id = self.id
        ol.save!

        # step four, create a child outbound_orderline for the release notes, so it will be possible to handle them differently if required
        rol = OutboundOrderline.new
        rol.outbound_order_id = oo.id
        rol.product_id = self.product_id
        rol.product_name = "release notes"
        rol.amount = 0
        # don't couple it directly to the production_orderline, instead set it as a child of ol, that way there is only one outbound orderline
        # coupled to the production_orderline, reducing the risk of choosing the wrong one.
        rol.parent_linenr = ol.id
        rol.save!

        # step five, create a packing
        pa = Packing.new
        pa.outbound_order_id = oo.id
        pa.packing_specification_id = shipping_specification.packing_specification_id
        pa.status = Packing::STATUS_DRAFT
        pa.save!

        pal1 = PackingLine.new
        pal1.packing_id = pa.id
        pal1.outbound_orderline_id = ol.id
        pal1.save!

        pal2 = PackingLine.new
        pal2.packing_id = pa.id
        pal2.outbound_orderline_id = rol.id
        pal2.save!

        # last step, commit
        self.product_location_id = pl.id
        self.save!
      end
    else
      # we don't have a shipping specification, skip the creation of the outbound order
      self.remarks = self.remarks ? self.remarks + "\n" : ""
      if self.product.nil?
        self.remarks += "Shipping order not created because this orderline does not have a product."
      elsif self.product.product_line.nil?
        self.remarks += "Shipping order not created because the product #{self.product.volumeid} (#{self.product.id}) does not have a Product line."
      else
        self.remarks += "Shipping order not created because product_line #{self.product.product_line.name} (#{self.product.product_line.id}) does not have a shipping specification."
      end
    end
  end

  def used_cv_tooling
    if  self.conversions.size > 0
      results = []
      
      self.conversions.each do |c|
        if c.conversiontool && c.cvtool_bundle
          results << [[c.conversiontool.code,c.conversiontool_id],[c.cvtool_bundle.name,c.cvtool_bundle_id]]
        end
      end
      if results.size > 0
        return results.last
      else
        return [[],[]]
      end
    else
      return [[],[]]
    end

  end

  def define_pre_process_for_manual_shipment_registration
    # Determine if and what process needs to be done before a conversion result can be shipped
    # example: CARiN master medium needs to be archived before shipping

    from_stock = false
    allow_ship = false
    rec_id = self.id
    todo = 'not_available'
    label = 'n.a.'
    desc = 'No product for shipping available'
    
    if self.chosen_conversion

      # Check if there is a stock record for possible CD's
      self.chosen_conversion.stock.each do |stock|
        from_stock = true
        rec_id = stock.id
        if stock.status.to_s == 'ARCHIVED'
          allow_ship = true
        elsif stock.status.to_s == 'VALIDATED'
          allow_ship = false
        end
      end

      if !from_stock # No CD on stock, so check product location
        if self.product_location.nil?
          allow_ship = false
          rec_id = self.chosen_conversion_id
        else
          allow_ship = true
          rec_id = self.id
        end
      end
      
      if from_stock == true  && allow_ship == false
        todo = 'register_cd'
        label = 'Archive medium'
        desc = 'Medium needs to be archived before shipment is allowed'
      elsif from_stock == true  && allow_ship == true
        todo = 'ship_cd'
        label = 'Ship product'
        desc = 'Medium is archived, so all copies may be shipped'
      elsif from_stock == false && allow_ship == false
        todo = 'register_file'
        label = 'Register product'
        desc = 'Database needs to be registered in product location first'
      elsif from_stock == false && allow_ship == true  # Product location known, so shipping allowed
        todo = 'ship_file'
        label = 'Ship product'
        desc = 'Product location known, so shipping allowed'
      end
    end

    return [todo,rec_id,label,desc]
  end

  def update_from_manual_conversion_request(current_user,conversion_obj,skip_status_validation=false)

    if skip_status_validation
    proceed = true
    elsif ( conversion_obj.status_changed? && conversion_obj.status == Conversion::STATUS_FINISHED )
      proceed = true
    elsif !conversion_obj.status_change.nil? && conversion_obj.status_change[0] &&
      conversion_obj.status_change[0] != Conversion::STATUS_FINISHED &&
      conversion_obj.status_change[0] != Conversion::STATUS_REJECTED &&
      conversion_obj.status != Conversion::STATUS_REJECTED
      proceed = true
    else
      proceed = false
    end

    if !conversion_obj.production_orderline.nil? && proceed && conversion_obj.allow_production_orderline_transition?
      ProductionOrderline.transaction do
        self.set_bp_status(ProductionOrderline::BP_STATUS_COMPLETE)
        self.transition(current_user,conversion_obj)
        self.update_bp_message("#{conversion_obj.status_str}")

        # check if conversion_databases for other products
        self.production_order.production_orderlines.each do |line|
          next if line.bp_seqnr && line.bp_seqnr > self.bp_seqnr
          conversion_obj.conversion_databases.each do |db|
            if  line.product.file_for_product?(db.path_and_file)
              db.product_id = line.product_id
              db.production_orderline_id = line.id
              db.save!
              while line.bp_seqnr < self.bp_seqnr
                line.transition(current_user,conversion_obj)
              end
              line.update_bp_message("#{conversion_obj.status_str}")
              line.chosen_conversion_id = conversion_obj.id
              line.save!
            end
          end
        end
        self.save!

        os = Orderstatuslog.new
        os.production_order_id = self.production_order_id
        os.completion_date = Time.now
        os.level = 'PRODUCT'
        os.user = current_user.user_name
        os.sequence = self.bp_seqnr
        os.status = self.bp_name
        os.remarks = "Conversion #{conversion_obj.id } #{conversion_obj.status_str}"
        os.reference = conversion_obj.productid.to_s
        os.save!

        return "Production status order is set #{self.bp_name}.<br />"
      end
    else
      return ""
    end
  end

  handle_asynchronously :rsynccopy
end
