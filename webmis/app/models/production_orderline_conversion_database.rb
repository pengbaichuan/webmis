class ProductionOrderlineConversionDatabase < ActiveRecord::Base
  belongs_to :production_orderline
  belongs_to :conversion_database
end
