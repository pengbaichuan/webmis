class Productset < ActiveRecord::Base
  acts_as_taggable
  version_fu
  
  attr_accessor :majorupdate
  attr_accessor :addchanges
  
  @@statushash =  {20 => :Approved,  0 => :Cancelled, 2 => :Cloned ,5 => :Draft,  50 => :Hold,100 => :Obsolete, 10 => :Proposal }

  has_many :includedproducts
  has_many :products, :through => :includedproducts
  has_many :product_lines, :through => :products
  has_many :product_data_set_infos, :through => :products
  has_many :includedproductsetdocuments
  has_many :document_versions, :through => :includedproductsetdocuments
  has_many :production_orders
  
  belongs_to :tmc_info , :foreign_key => "tmc_info_id"
  belongs_to :product_category
  belongs_to :company_abbr
  
  validates_uniqueness_of :setcode, :scope => :product_category_id
  validates :setcode, :presence => true
  
  before_save :before_save

  def before_save
    self.dateeffective = Time.now

    if self.removed_yn
      self.status = @@statushash.key(:Obsolete)
    end

    if self.status == @@statushash.key(:Obsolete)
      self.removed_yn = true
    end


	 	if (customer_id)
      r = Manufacturer.find(customer_id)
      if r
        self.customer_name = r.name
      end
	 	end

	 	if (firsttier_id)
      s = Manufacturer.find(firsttier_id)
	 		if (s)
        self.firsttiername =s.name
      end
	 	end

	 	if (region_id)
      r = Region.find(region_id)
      if r
        self.area_name = r.name
      end
		end

		if (datarelease_id)
	 		dr = DataRelease.find(datarelease_id)
	 		if dr
        self.datarelease_name = dr.datasource.name + " " + dr.name
	 		end
		end

    calculate_next_version
	end
  
  def after_save
    #take_snapshot
  end

  def headrs
    sproducts = Includedproduct.where(:productset_id => [id]).all
    hdrs = []
    
    sproducts.each do |pid|
      product = Product.find_by_id(pid.product_id)
    
      if product.volumeid != nil
        hdrs.push("0_ID") if product.volumeid.size > 0
      end
      if product.name != nil
        hdrs.push("1_Name") if product.name.size > 0
      end
      if product.detailedcoverage != nil
        hdrs.push("2_Coverage Detailed") if product.detailedcoverage.size > 0
      end
      if product.hwncoverage != nil
        hdrs.push("3_Coverage HWN") if product.hwncoverage.size > 0
      end
      if product.tmclocation != nil
        hdrs.push("4_TMC Location Tables") if product.tmclocation.size > 0
      end
      if product.thirdpartydata1 != nil
        hdrs.push("5_TPD I") if product.thirdpartydata1.size > 0
      end
      if product.thirdpartydata2 != nil
        hdrs.push("6_TPD II") if product.thirdpartydata2.size > 0
      end
      if product.product_data_set_infos.size != nil
        hdrs.push("7_Datasets") if product.product_data_set_infos.size > 0
      end
      if product.remarks != nil
        hdrs.push("8_Exceptions/remarks:") if product.remarks.size > 0
      end
    end
    return hdrs.uniq
  end

  def take_snapshot
    #take snapshop of productconfiguration
    latest = versions.latest  #find_version(version)
    Includedproduct.where(:productset_id => id, :link_product_id => nil).each do |product|
      ipv = IncludedproductVersion.new
      ipv.productset_id = latest.id
      ipv.includedproduct_id = product.id
      productversion = product.product.version
      pr = product.product.find_version(productversion)
      ipv.product_id = pr.id if pr
      ipv.save
    end

    Includedproduct.where(:productset_id => id, :link_product_id => nil).each do |product|
      ipv = IncludedproductVersion.new
      ipv.productset_id = latest.id
      ipv.includedproduct_id = product.id
      productversion = product.product.version
      pr = product.product.find_version(productversion)
      ipv.product_id = pr.id
      if (product.link_product_id)
        ipv.link_product_id = IncludedproductVersion.where(:includedproduct_id => product.link_product_id, :productset_id => latest.id ).first.id
      end
      ipv.save
    end



  end

  # just return input on exceptions
  def self.try(input)
    yield input rescue input
  end

	def self.analyze_productname(name)
    input = name.split(" ")
    result = {}

    # if it's a test product
    if input.last == "(t)"
      # (t) is part of the media type part, combine them
      input.last += " "+input.pop
    end

    # shift known parts from the front
    result[:customer] = try(input.shift) {|s| CompanyAbbr.find_by_abbr(s).id }
    result[:product_line] = try(input.shift) {|s| ProductLine.find_by_abbr(s).id }
    result[:supplier] = try(input.shift) {|s| CompanyAbbr.find_by_abbr(s).id }
    result[:release] = input.shift

    # pop known parts from the back (ciq_type will end up being nil if it wasn't there)
    result[:mediatype] = try(input.pop) {|s| Mediatype.find_by_ref(s).id }
    result[:area]      = try(input.pop) {|s| Region.find_by_area_code(s).id }
    result[:ciq_type]  = try(input.pop) {|s| CiqType.find_by_name(s).id }

    result
	end

  def self.statushash
    @@statushash
  end

  def product_ids=(new_ids)
    ids = (new_ids || []).reject(&:blank?)
    old_ids = self.product_ids

    self.transaction do
      Includedproduct.destroy_all({ :product_id => old_ids - new_ids, :productset_id => self.id })
      (new_ids - old_ids).each do |product_id|
        self.includedproducts.create!(:product_id => product_id)
      end
    end
  end
 
  def clone_all
    p = ""
    self.transaction do
      p = self.dup
      p.status = 5
      p.save
      p.product_ids = self.product_ids
      p.save
    end
    return p
  end

  def calculate_next_version
    self.major = 0 if !self.major
    self.minor = 0 if !self.minor

    if self.majorupdate == "1"
      self.major = self.major + 1
    else
      self.minor = self.minor + 1
      if self.minor >= 10
        self.minor = 0
        self.major = self.major + 1
      end
    end 
    self.version = self.version + 1
  end 

  def current_version
    self.major = 0 if !major
    self.minor = 0 if !minor
    self.major.to_s + "." + self.minor.to_s
  end

  def add_text(pdf,label,field,delta)
    if delta[field.to_sym]
      pdf.fill_color(Color::RGB::Red)
    end
    pdf.text((label + ":").ljust(30).rjust(1) + @ps.send(field).to_s).to_s.ljust(50)
    pdf.fill_color(Color::RGB::Black)
  end

  def pdf_nc
    @ps = self
    pdf = PDF::TechBook.new(:orientation => :landscape, :paper => [29,40])
    pdf.add_image_from_file("public/webmis/msbackgroundwhite2.png",0,0,1150,850)
    pdf.font_size = 20
    pdf.text  "Productset details for "  + @ps.setcode.to_s
    pdf.text " "
    pdf.font_size = 12
    pdf.select_font("Courier")
   
    delta = @ps.delta
    add_text(pdf , "Productset","setcode",delta)
    add_text(pdf , "Name","name",delta)
    add_text(pdf , "Customer","customer_name",delta)
    add_text(pdf , "Set Type","settype",delta)
    add_text(pdf , "Coverage","area_name",delta)
    add_text(pdf , "Continent","contabbr",delta)
    add_text(pdf , "Datasource Release","datarelease_name",delta)
    add_text(pdf , "Conversion tool","conversiontool_name",delta)
    add_text(pdf , "Medium","medium_name",delta)
    pdf.text  "Version:" + @ps.major.to_s + "." + @ps.minor.to_s

    #ruby
    pdf.text  " "
    pdf.text  " "

    pdf.select_font("Times-Roman")
    table2 = PDF::SimpleTable.new
    table2.header_gap = 15
    table2.font_size = 10
    table2.width = 1050
    prods = []

    self.major_products.each do |prod|
      prodatts = prod.attributes
      delta = prod.delta(2)
      prodatts.each do |key,value|
        prodatts[key] = value.to_s + " {RED}" if delta[key.to_sym]
      end
      comps = ""
      prod.components(self.id).each do |prod|
        comps = comps  + prod.volumeid + "\r\n"
      end
      #prodatts["components"] = comps
      prods << prodatts



      
      prod.product_data_set_infos.each do |info|
        hash  = info.attributes
        hash["remarks_dataset"] = hash["remarks"]
        hash.delete("remarks")
        hash["datasetname"] = hash["name"]
        hash["datasetsupplier"] = info.company_abbr.ms_abbr_3

        hash.delete("name")
        prods <<  hash
      end

    end


    table2.data = prods
    table2.column_order  = ["volumeid","name","detailedcoverage","conversion_settings","datasetname","datasetsupplier","data_release","area_abbr","data_info","remarks_dataset"]
    table2.columns["volumeid"] = PDF::SimpleTable::Column.new("volumeid")
    table2.columns["name"] = PDF::SimpleTable::Column.new("name")
    table2.columns["detailedcoverage"] = PDF::SimpleTable::Column.new("detailedcoverage")
    table2.columns["conversion_settings"] = PDF::SimpleTable::Column.new("conversion_settings")

    table2.columns["datasetname"] = PDF::SimpleTable::Column.new("datasetname")
    table2.columns["datasetsupplier"] = PDF::SimpleTable::Column.new("datasetsupplier")
    table2.columns["data_release"] = PDF::SimpleTable::Column.new("data_release")
    table2.columns["area_abbr"] = PDF::SimpleTable::Column.new("area_abbr")
    table2.columns["data_info"] = PDF::SimpleTable::Column.new("data_info")
    table2.columns["remarks_dataset"] = PDF::SimpleTable::Column.new("remarks_dataset")



    table2.columns["remarks"] = PDF::SimpleTable::Column.new("remarks")
    table2.columns["volumeid"].heading = "ID"
    table2.columns["name"].heading = "Product Name"
    table2.columns["detailedcoverage"].heading = "Coverage Detailed"
    table2.columns["conversion_settings"].heading = "Feature & Conversion Settings"

    table2.columns["remarks"].heading = "Exceptions/Remarks"

    table2.columns["datasetname"].heading = "Dataset name"
    table2.columns["datasetsupplier"].heading = "Dataset supplier"
    table2.columns["data_release"].heading = "Release"
    table2.columns["area_abbr"].heading = "Area"
    table2.columns["data_info"].heading = "Dataset Info"
    table2.columns["remarks_dataset"].heading = "Exception/remarks"


    
    table2.render_on(pdf)

    prods =[]
    self.components.each do |prod|
      prodatts = prod.attributes
      delta = prod.delta(2)
      prodatts.each do |key,value|
        prodatts[key] = value + " {RED}" if delta[key.to_sym]
      end
      prods << prodatts
    end

    table2 = PDF::SimpleTable.new
    table2.header_gap = 15
    table2.font_size = 10
    table2.width = 1100
    table2.data = prods
    table2.column_order  = ["volumeid","name","detailedcoverage","hwncoverage","tmclocation","thirdpartydata1","thirdpartydata2","remarks"]
    table2.columns["volumeid"] = PDF::SimpleTable::Column.new("volumeid")
    table2.columns["name"] = PDF::SimpleTable::Column.new("name")
    table2.columns["detailedcoverage"] = PDF::SimpleTable::Column.new("detailedcoverage")
    table2.columns["hwncoverage"] = PDF::SimpleTable::Column.new("hwncoverage")
    table2.columns["tmclocation"] = PDF::SimpleTable::Column.new("tmclocation")
    table2.columns["thirdpartydata1"] = PDF::SimpleTable::Column.new("thirdpartydata1")
    table2.columns["thirdpartydata2"] = PDF::SimpleTable::Column.new("thirdpartydata2")
    table2.columns["remarks"] = PDF::SimpleTable::Column.new("remarks")
    table2.columns["volumeid"].heading = "ID"
    table2.columns["name"].heading = "Name"
    table2.columns["detailedcoverage"].heading = "Coverage Detailed"
    table2.columns["hwncoverage"].heading = "Coverage HWN"
    table2.columns["tmclocation"].heading = "Tmc Location Tables"
    table2.columns["thirdpartydata1"].heading = "TPD I"
    table2.columns["thirdpartydata2"].heading = "TPD II"
    table2.columns["remarks"].heading = "Remarks"

    if self.components.size > 0
      pdf.text "Components"
      pdf.text " "
      table2.render_on(pdf)
    end

    pdf.text  " "

    footer = [{"remarks" => self.remarks.to_s, "approval" => self.readable_status.to_s + " " + self.approval_text.to_s}]

    table2 = PDF::SimpleTable.new
    table2.font_size = 10
    table2.width = 1050
    table2.data = footer
    table2.column_order  = ["remarks", "approval"]
    table2.columns["remarks"] = PDF::SimpleTable::Column.new("remarks")
    table2.columns["approval"] = PDF::SimpleTable::Column.new("approval")
    table2.shade_rows = :none
    table2.show_headings = false
    table2.show_lines = :none
    table2.render_on(pdf)
    return pdf
    
  end

  def pdf
    @ps = self
    
    pdf = PDF::TechBook.new(:orientation => :landscape, :paper => [29,40])
    pdf.add_image_from_file("public/webmis/msbackgroundwhite2.png",0,0,1150,850)
    pdf.font_size = 20
    pdf.text  "Productset details for "  + @ps.setcode.to_s
    pdf.text " "
    pdf.font_size = 12
    pdf.select_font("Courier")
    delta = @ps.delta
    add_text(pdf , "Sheet code","setcode",delta)
    add_text(pdf , "Product Coverage/Name","name",delta)
    add_text(pdf , "Customer name","customer_name",delta)
    add_text(pdf , "Datasource","supplier_name",delta)
    add_text(pdf , "Datasource Release","datarelease_name",delta)
    add_text(pdf , "CIQ-(BRN)","ciq",delta)
    add_text(pdf , "Tmc Name","tmc_info_name",delta)
    add_text(pdf , "Area","area_name",delta)
    add_text(pdf , "Product spec","nds_name",delta)
    add_text(pdf , "Conversion tool","conversiontool_name",delta)
    add_text(pdf , "Medium","medium_name",delta)
    pdf.text  "Version:" + @ps.major.to_s + "." + @ps.minor.to_s

    #ruby
    pdf.text  " "
    pdf.text  " "

    pdf.select_font("Times-Roman")
    table2 = PDF::SimpleTable.new
    table2.header_gap = 15
    table2.font_size = 10
    table2.width = 1050
    prods = []

    self.major_products.each do |prod|
      prodatts = prod.attributes
      delta = prod.delta(2)
      prodatts.each do |key,value|
        prodatts[key] = value.to_s + " {RED}" if delta[key.to_sym]
      end
      comps = ""
      prod.components(self.id).each do |prod|
        comps = comps  + prod.volumeid + "\r\n"
      end
      prods << prodatts
    end

    table2.data = prods
    table2.column_order  = ["volumeid","name","detailedcoverage","hwncoverage","tmclocation","thirdpartydata1","thirdpartydata2","remarks","nds_name"]
    table2.columns["volumeid"] = PDF::SimpleTable::Column.new("volumeid")
    table2.columns["name"] = PDF::SimpleTable::Column.new("name")
    table2.columns["detailedcoverage"] = PDF::SimpleTable::Column.new("detailedcoverage")
    table2.columns["hwncoverage"] = PDF::SimpleTable::Column.new("hwncoverage")
    table2.columns["tmclocation"] = PDF::SimpleTable::Column.new("tmclocation")
    table2.columns["thirdpartydata1"] = PDF::SimpleTable::Column.new("thirdpartydata1")
    table2.columns["thirdpartydata2"] = PDF::SimpleTable::Column.new("thirdpartydata2")
    table2.columns["remarks"] = PDF::SimpleTable::Column.new("remarks")
    table2.columns["nds_name"] = PDF::SimpleTable::Column.new("nds_name") 
    table2.columns["volumeid"].heading = "ID"
    table2.columns["name"].heading = "Name"
    table2.columns["detailedcoverage"].heading = "Coverage Detailed"
    table2.columns["hwncoverage"].heading = "Coverage HWN"
    table2.columns["tmclocation"].heading = "Tmc Location Tables"
    table2.columns["thirdpartydata1"].heading = "TPD I"
    table2.columns["thirdpartydata2"].heading = "TPD II"
    table2.columns["remarks"].heading = "Remarks"
    table2.columns["nds_name"].heading = "Product spec."

    table2.render_on(pdf)

    prods =[]
    self.components.each do |prod|
      prodatts = prod.attributes
      delta = prod.delta(2)
      prodatts.each do |key,value|
        prodatts[key] = value + " {RED}" if delta[key.to_sym]
      end
      prods << prodatts
    end

    table2 = PDF::SimpleTable.new
    table2.header_gap = 15
    table2.font_size = 10
    table2.width = 1100
    table2.data = prods
    table2.column_order  = ["volumeid","name","detailedcoverage","hwncoverage","tmclocation","thirdpartydata1","thirdpartydata2","remarks"]
    table2.columns["volumeid"] = PDF::SimpleTable::Column.new("volumeid")
    table2.columns["name"] = PDF::SimpleTable::Column.new("name")
    table2.columns["detailedcoverage"] = PDF::SimpleTable::Column.new("detailedcoverage")
    table2.columns["hwncoverage"] = PDF::SimpleTable::Column.new("hwncoverage")
    table2.columns["tmclocation"] = PDF::SimpleTable::Column.new("tmclocation")
    table2.columns["thirdpartydata1"] = PDF::SimpleTable::Column.new("thirdpartydata1")
    table2.columns["thirdpartydata2"] = PDF::SimpleTable::Column.new("thirdpartydata2")
    table2.columns["remarks"] = PDF::SimpleTable::Column.new("remarks")
    table2.columns["volumeid"].heading = "ID"
    table2.columns["name"].heading = "Name"
    table2.columns["detailedcoverage"].heading = "Coverage Detailed"
    table2.columns["hwncoverage"].heading = "Coverage HWN"
    table2.columns["tmclocation"].heading = "Tmc Location Tables"
    table2.columns["thirdpartydata1"].heading = "TPD I"
    table2.columns["thirdpartydata2"].heading = "TPD II"
    table2.columns["remarks"].heading = "Remarks"

    if self.components.size > 0
      pdf.text "Components"
      pdf.text " "
      table2.render_on(pdf)
    end

    pdf.text  " "

    footer = [{"remarks" => self.remarks.to_s, "approval" => self.readable_status.to_s + " " + self.approval_text.to_s}]

    table2 = PDF::SimpleTable.new
    table2.font_size = 10
    table2.width = 1050
    table2.data = footer
    table2.column_order  = ["remarks", "approval"]
    table2.columns["remarks"] = PDF::SimpleTable::Column.new("remarks")
    table2.columns["approval"] = PDF::SimpleTable::Column.new("approval")
    table2.shade_rows = :none
    table2.render_on(pdf)
    return pdf
  end

  def components
    comps = []
    self.includedproducts.each do |ic|
      if ic.link_product_id && !comps.index(ic.product)
        comps <<  ic.product
      end
    end
    return comps
  end

  def major_products
    mproducts = []
    self.includedproducts.each do |ic|
      if !ic.link_product_id && !mproducts.index(ic.product)
        mproducts <<  ic.product
      end
    end
    return mproducts
  end

  def major_includedproducts
    mproducts = []
    self.includedproducts.each do |ic|
      if !ic.link_product_id && !mproducts.index(ic.product)
        mproducts <<  ic
      end
    end
    return mproducts
  end

  def remarks_html
    remarks.to_s.gsub("\n","<br/>")
  end

  def delta(count=1)
    ps1 = ProductsetVersion.where(:productset_id => id, :version => version).first
    ps2 = nil
    if ps1.version.to_i - count <= 0
      ps2 = ProductsetVersion.where(:productset_id => id, :version => version).order('productset_versions.version').first
    else
      ps2 = ProductsetVersion.where(:productset_id => id, :version => version-count).first
    end
    ProductsetVersion.diff :exclude =>['id', 'yaml','created_at','updated_at','version','major','minor','setchanges']
    ps2 = ps1 if !ps2
    psdiff = ps1.diff(ps2)
    return psdiff
  end

  def delta_by_mm(majorp,minorp)
    ps2 = ProductsetVersion.where(:productset_id => id, :marjor => majorp, :minor => 1).first
    
    ps2 = ProductsetVersion.where(:productset_id => id, :marjor => majorp, :minor => minorp).first
    
    ProductsetVersion.diff :exclude =>['id', 'yaml','created_at','updated_at','version','major','minor','setchanges']
    return ps1.diff(ps2)
  end

  def get_differences_with(p_old)
    Productset.diff :exclude =>['id', 'yaml','created_at','updated_at','version','major','minor','setchanges']
    return self.diff(p_old)
  end

  def readable_status
    @@statushash[status.to_i]
  end
  alias_method :status_str, :readable_status

  def self.import(cat_id)
    Importer.import(cat_id)
  end

  def major_dataset
    release = "UNKOWN"
    info = ProductDataSetInfo.where("product_id in (select product_id from includedproducts where productset_id = #{self.id} and major_dataset = 1)").first
    if info
      release = info.company_abbr.ms_abbr_3.to_s + " " + info.data_release.to_s + " " + info.area_abbr.to_s + " " + info.data_info.to_s
    end
    return release
  end
  
  def has_production_order?
    if self.production_orders.size > 0
      return true
    else
      return false
    end
  end

  def clone_set
    clone = self.dup
    dup_setcode = "Clone of: " + self.setcode
    if Productset.find_by_setcode(dup_setcode).nil?
      clone_setcode = dup_setcode
    else
      seq = Productset.where(setcode:dup_setcode).size + 1
      seq = Productset.where("setcode LIKE '%#{dup_setcode}'").size + 1
      clone_setcode = "(" + seq.to_s + ") " + dup_setcode
    end
    clone.setcode = clone_setcode
    clone.status = 2 # Cloned
    clone.version = 0
    clone.set_release_major = 1
    clone.set_release_minor = 0
    clone.save
    
    clone.reload
    
    old_parent_product_id = ''
    old_parent_product = self.products.where("parent_id IS NULL OR parent_id = ''").first
    old_parent_product_id = old_parent_product.id if old_parent_product

    self.products.each do |product|
      product_clone = product.duplicate
      included_product = Includedproduct.new
      included_product.productset_id = clone.id
      included_product.product_id = product_clone.id
      included_product.save
    end

    new_parent_product = clone.products.where("parent_id IS NULL OR parent_id = ''").first
    if new_parent_product
      new_parent_product_id = new_parent_product.id
      clone.products.where(:parent_id => old_parent_product_id).each do |cp|
        cp.parent_id = new_parent_product_id
        cp.save
      end
    end

    return clone
  end

  def has_design?
    bool = false
    products.each do |product|
      if product.product_design
        bool = true
        break
      end
    end
    return bool
  end
  
  def predecessor_obj
    return Productset.find_by_setcode(self.predecessor.strip)
  end

  def successor_objs
    return Productset.where(predecessor:self.setcode.strip)
  end

end
