class ProductsetVersion < ActiveRecord::Base
  
  has_many :includedproduct_versions, :foreign_key => 'productset_id'
  has_many :product_versions, :through => :includedproduct_versions, :foreign_key => 'productset_id'

  def includedproducts
    includedproduct_versions
  end

  def products
    product_versions
  end

  def components
    comps = []
    self.includedproducts.each do |ic|
      if ic.link_product_id && !comps.index(ic.product)
        comps <<  ic.product
      end
    end
    return comps
  end

  def major_products
    mproducts = []
    self.includedproducts.each do |ic|
      if !ic.link_product_id && !mproducts.index(ic.product)
        mproducts <<  ic.product
      end
    end
    return mproducts
  end

  def major_includedproducts
    mproducts = []
    self.includedproducts.each do |ic|
      if !ic.link_product_id && !mproducts.index(ic.product)
        mproducts <<  ic
      end
    end
    return mproducts
  end

  def pdf
    pdf = PDF::TechBook.new(:orientation => :landscape)
    # #pdf.margins_pt(50,50,20,20)
    pdf.add_image_from_file("public/webmis/msbackgroundwhite2.png",0,0,800,650)
    pdf.font_size = 8
    pdf.select_font("Courier")
    @ps = self
    delta = @ps.delta
    add_text(pdf , "Sheet code","setcode",delta)
    add_text(pdf , "Product Coverage/Name","name",delta)
    add_text(pdf , "Customer name","customer_name",delta)
    add_text(pdf , "Datasource","supplier_name",delta)
    add_text(pdf , "Datasource Release","datarelease_name",delta)
    add_text(pdf , "CIQ-(BRN)","ciq",delta)
    add_text(pdf , "Area","area_name",delta)
    add_text(pdf , "Product spec","nds_name",delta)
    add_text(pdf , "Conversion tool","conversiontool_name",delta)
    add_text(pdf , "Medium","medium_name",delta)

    # #pdf.text  "Product Coverage: " + @ps.name.to_s #pdf.text  "Customer name:    " + @ps.customer_name.to_s
    # #pdf.text  "Datasource:       " + @ps.supplier_name.to_s + " " + @ps.datarelease_name.to_s #pdf.text
    # "CIQ-(BRN):        " + @ps.ciq.to_s #pdf.text  "Area:             " + @ps.area_name.to_s #pdf.text
    # "Product spec:     " + @ps.nds_name.to_s #pdf.text  "Conversion tool:  " + @ps.conversiontool_name.to_s
    # #pdf.text  "Medium:           " + @ps.medium_name.to_s
    pdf.text  "Version:" + @ps.major.to_s + "." + @ps.minor.to_s

    # #ruby
    pdf.text  " "
    pdf.select_font("Times-Roman")
    table2 = PDF::SimpleTable.new
    table2.header_gap = 15
    table2.font_size = 8
    table2.width = 710
    prods = []

    self.major_products.each do |prod|
      prodatts = prod.attributes
      delta = prod.delta(2)
      prodatts.each do |key,value|
        prodatts[key] = value + " {RED}" if delta[key.to_sym]
      end
      comps = ""
      prod.components(self.id).each do |prod|
        comps = comps  + prod.volumeid + "\r\n"
      end
      prodatts["components"] = comps
      prods << prodatts
    end

    table2.data = prods
    table2.column_order  = ["volumeid","name","detailedcoverage","hwncoverage","tmclocation","thirdpartydata1","thirdpartydata2","remarks","components"]
    table2.columns["volumeid"] = PDF::SimpleTable::Column.new("volumeid")
    table2.columns["name"] = PDF::SimpleTable::Column.new("name")
    table2.columns["detailedcoverage"] = PDF::SimpleTable::Column.new("detailedcoverage")
    table2.columns["hwncoverage"] = PDF::SimpleTable::Column.new("hwncoverage")
    table2.columns["tmclocation"] = PDF::SimpleTable::Column.new("tmclocation")
    table2.columns["thirdpartydata1"] = PDF::SimpleTable::Column.new("thirdpartydata1")
    table2.columns["thirdpartydata2"] = PDF::SimpleTable::Column.new("thirdpartydata2")
    table2.columns["remarks"] = PDF::SimpleTable::Column.new("remarks")
    table2.columns["components"] = PDF::SimpleTable::Column.new("components")
    table2.columns["volumeid"].heading = "ID"
    table2.columns["name"].heading = "Name"
    table2.columns["detailedcoverage"].heading = "Coverage Detailed"
    table2.columns["hwncoverage"].heading = "Coverage HWN"
    table2.columns["tmclocation"].heading = "Tmc Location Tables"
    table2.columns["thirdpartydata1"].heading = "TPD I"
    table2.columns["thirdpartydata2"].heading = "TPD II"
    table2.columns["remarks"].heading = "Remarks"
    table2.columns["components"].heading = "Used Components"

    table2.render_on(pdf)

    prods =[]
    self.components.each do |prod|
      prodatts = prod.attributes
      delta = prod.delta(2)
      prodatts.each do |key,value|
        prodatts[key] = value + " {RED}" if delta[key.to_sym]
      end
      prods << prodatts
    end

    table2 = PDF::SimpleTable.new
    table2.header_gap = 15
    table2.font_size = 8
    table2.width = 700
    table2.data = prods
    table2.column_order  = ["volumeid","name","detailedcoverage","hwncoverage","tmclocation","thirdpartydata1","thirdpartydata2","remarks"]
    table2.columns["volumeid"] = PDF::SimpleTable::Column.new("volumeid")
    table2.columns["name"] = PDF::SimpleTable::Column.new("name")
    table2.columns["detailedcoverage"] = PDF::SimpleTable::Column.new("detailedcoverage")
    table2.columns["hwncoverage"] = PDF::SimpleTable::Column.new("hwncoverage")
    table2.columns["tmclocation"] = PDF::SimpleTable::Column.new("tmclocation")
    table2.columns["thirdpartydata1"] = PDF::SimpleTable::Column.new("thirdpartydata1")
    table2.columns["thirdpartydata2"] = PDF::SimpleTable::Column.new("thirdpartydata2")
    table2.columns["remarks"] = PDF::SimpleTable::Column.new("remarks")
    table2.columns["volumeid"].heading = "ID"
    table2.columns["name"].heading = "Name"
    table2.columns["detailedcoverage"].heading = "Coverage Detailed"
    table2.columns["hwncoverage"].heading = "Coverage HWN"
    table2.columns["tmclocation"].heading = "Tmc Location Tables"
    table2.columns["thirdpartydata1"].heading = "TPD I"
    table2.columns["thirdpartydata2"].heading = "TPD II"
    table2.columns["remarks"].heading = "Remarks"

    if self.components.size > 0
      pdf.text "Components"
      pdf.text " "
      table2.render_on(pdf)
    end

    return pdf
  end

  def add_text(pdf,label,field,delta)
    if delta[field.to_sym]
      pdf.fill_color(Color::RGB::Red)
    end
    pdf.text((label + ":").ljust(30).rjust(1) + @ps.send(field).to_s).to_s.ljust(50)
    pdf.fill_color(Color::RGB::Black)
  end

  def delta(count=1)
    ps1 =  self
    ps2 = nil
    if ps1.version.to_i - count <= 0
      ps2 =  ProductsetVersion.where("productset_id = #{productset_id}").order('version').first
    else
      ps2 =  ProductsetVersion.where("productset_id = #{productset_id} and version = #{version-count}").first
    end
    ProductsetVersion.diff :exclude =>['id', 'yaml','created_at','updated_at','version','major','minor','changes']
    ps2 = ps1 if !ps2
    psdiff = ps1.diff(ps2)
    return psdiff
  end

end
