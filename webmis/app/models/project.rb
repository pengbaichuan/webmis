class Project < ActiveRecord::Base
  belongs_to :environment
  has_many :config_parameters
  has_many :roles_users
  has_many :product_designs
  has_many :production_orders
  has_many :product_features
  has_many :parameter_settings
  has_many :jobs

  has_many :project_servers
  has_many :servers, :through => :project_servers
  has_many :project_users
  has_many :users, :through => :project_users

  STATUS_ENTRY     =  10
  STATUS_ACTIVE    =  20
  STATUS_OBSOLETE  =  30
  STATUS_REMOVED   =  40

  @@statusarray                  = Array.new
  @@statusarray[STATUS_ENTRY]    = 'Entry'
  @@statusarray[STATUS_ACTIVE]   = 'Active'
  @@statusarray[STATUS_OBSOLETE] = 'Obsolete'
  @@statusarray[STATUS_REMOVED]  = 'Removed'

  scope :active, -> {where(:status => STATUS_ACTIVE) }

  def status_str
    return  @@statusarray[self.status] if self.status
  end

  def self.statusarray
    return @@statusarray
  end

  def self.status_hash
    s_hash = Hash.new
    @@statusarray.each do |s|
      if !s.nil?
        s_hash[s] = @@statusarray.index(s)
      end
    end
    return s_hash.sort_by { |k,v| v }
  end

  def allow_status?(status_int)
    case status_int
    when self.status
      return false
    else
      return true
    end
  end

  def supports_expiry?
    ConfigParameter.get("default_database_expiry_in_days", self.id).to_i > 0 rescue false
  end

  def self.default
    prj_name = ConfigParameter.find_by_cfg_name("default_project_name").cfg_value rescue 'Production'
    return Project.find_by_name(prj_name)
  end

  def self.default?(project_id)
    proj = Project.find(project_id)
    if self.default == proj
      return true
    else
      return false
    end
  end

  def project_member?(user_id)
    if self.users.where(:id => user_id).empty?
      return false
    else
      return true
    end
  end

  def self.project_associated?(obj,scope_project_id=nil)
    if obj.has_attribute?("project_id")
      if scope_project_id.nil? || obj.project_id.blank?
        return true
      else
        if obj.project_id != scope_project_id
          return false
        else
          return true
        end
      end
    else
      return false
    end
  end

  def color_profile
    return ConfigParameter.get("project_color_profile",self.id.to_s)
  end
end
