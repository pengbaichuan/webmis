class PurchaseOrder < ActiveRecord::Base 
  belongs_to :user
  belongs_to :external_contact
  has_many :purchase_orderlines
  has_many :included_article_license_purchase_orderlines, :through => :purchase_orderlines
  has_many :article_licenses, :through => :included_article_license_purchase_orderlines


  scope :available, -> {where(:deleted => [false,nil])}

 @@statusarray      = Array.new
  @@statusarray[0]   = 'Entry'
  @@statusarray[10]  = 'Ordered'
  @@statusarray[20]  = 'Received'
  @@statusarray[30]  = 'Settled'
  @@statusarray[999] = 'Deleted'

  def self.statusarray
    return @@statusarray
  end

  def statusstr
    status_string =  @@statusarray.values_at(0)[0]
    if status
      status_string =  @@statusarray.values_at(status)[0]
    end
    return status_string
  end  

  def self.purchase_license_fee_by_report(external_contact_id,period_date,user_id,label="",po_reference="n.a.",remarks="",create=false)

    @external_contact = ExternalContact.find(external_contact_id)

    conditions = '(external_contact_id = ?) and (active_yn = 1)'
    param_values = [external_contact_id]
    unless label.blank?
      conditions += ' and (label =?)'
      param_values << label
    end
    @article_licenses = ArticleLicense.where([conditions] + param_values)

    conditions = 'shipping_orderlines.status >= 40'
    param_values = []

    conditions += ' and (article_licenses.external_contact_id = ?)'
    param_values << external_contact_id

    unless label.blank?
      conditions += ' and (article_licenses.label = ?)'
      param_values << label
    end

    conditions += ' and (delivery_date between ? and ?)'
    param_values << period_date.beginning_of_month
    param_values << period_date.end_of_month

    @license_fee_reports = ShippingOrderline.includes(:article => {:high_level_bom => :article_licenses }).where([conditions] + param_values).references(:article_licenses)
    @license_fee_reports.each do |sol|
      @article_licenses.each do |alic|
        if !sol.included_article_license_purchase_orderlines.where(:article_license_id => "#{alic.id}").empty?
          @license_fee_reports = @license_fee_reports.where("shipping_orderlines.id != #{sol.id}")
        end
      end
    end

    if create && @license_fee_reports.size > 0

      transaction do
        @purchace_order = self.new
        @purchace_order.purchase_reference = po_reference
        @purchace_order.date_ordered  = @license_fee_reports.last.delivery_date
        @purchace_order.date_received = Date.today
        @purchace_order.remarks = remarks
        @purchace_order.external_contact_id = external_contact_id
        @purchace_order.user_id = user_id
        @purchace_order.status = 20 # Received
        @purchace_order.deleted = false
        if @purchace_order.save
          #lines
          @article_licenses.each do |fee|
            purchase_orderline = PurchaseOrderline.new
            purchase_orderline.purchase_order_id = @purchace_order.id
            purchase_orderline.quantity = @license_fee_reports.where("articles.high_level_bom_id = ?", fee.high_level_bom_id).sum("amount")
            purchase_orderline.unit_price = fee.fee
            purchase_orderline.item_description = "License fee #{fee.description}"
            purchase_orderline.item_reference = @license_fee_reports.where("articles.high_level_bom_id = ?", fee.high_level_bom_id).collect {|so|so.article.customer_reference}.uniq.join("\n")
            if purchase_orderline.quantity > 0
              purchase_orderline.save
              @license_fee_reports.each do |shipping_orderline|
                rel_table = IncludedArticleLicensePurchaseOrderline.new
                rel_table.shipping_orderline_id = shipping_orderline.id
                rel_table.article_license_id = fee.id
                rel_table.purchase_orderline_id =  purchase_orderline.id
                rel_table.save
              end
            end
          end
        end
      end
      return true
    else
      return [@external_contact,@article_licenses,@license_fee_reports,@period_date]
    end
  end
end
