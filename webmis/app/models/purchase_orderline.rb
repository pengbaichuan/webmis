class PurchaseOrderline < ActiveRecord::Base
  belongs_to :purchase_order
  has_many :included_article_license_purchase_orderlines
  has_many :shipping_orderlines, :through => :included_article_license_purchase_orderlines
end
