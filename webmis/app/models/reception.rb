class Reception < ActiveRecord::Base
  
 acts_as_ordered_taggable_on :tags
  
  belongs_to :data_release
  belongs_to :data_medium
  belongs_to :data_type
  belongs_to :region , :foreign_key => :area_id
  belongs_to :data_set
  belongs_to :filling
  
  has_many :reception_lines
  has_many :parent_receptions
  has_many :receptions, :through => :parent_receptions
  has_many :product_locations
  has_many :high_level_boms
  has_many :process_data_elements
  has_many :data_sets , :through => :process_data_elements
  
  validates :area_id, :presence => true
  validates :purpose, :presence => true
  validates :data_release_id, :presence => true
  validates :data_type_id, :presence => true

  validates :reception_date,:presence => true unless Proc.new { |reception| reception.status == 'EXPECTED' }
  validates :reference_id,:presence => true unless Proc.new { |reception| reception.status == 'EXPECTED' }
  validates :source_location,:presence => true, :if => lambda { |reception| reception.try(:status) =="REQUESTED" }
  validates :storage_location,:presence => true, :if => lambda { |reception| reception.try(:status) =="COMPLETED" }
  
  validates_exclusion_of :persistent_yn, :in => [true,false],:allow_blank => true, :message => "Not allowed to remove", :if => :can_be_removed_yn
  validates_exclusion_of :persistent_yn, :in => [true,false],:allow_blank => true, :message => "Not allowed to remove", :if => :removed_yn

  scope :available, -> {where(:status => "COMPLETED",:can_be_removed_yn => false,:removed_yn => false) }
  scope :available_prod, -> {where(:status => "COMPLETED",:can_be_removed_yn => false,:removed_yn => false,:purpose => 'production') }
  scope :requests, -> {where(:status => ["REJECTED","INPROGRESS"],:can_be_removed_yn => false,:removed_yn => false) }

  before_save :before_save

  STATUS_EXPECTED   = 'EXPECTED'
  STATUS_REQUESTED  = 'REQUESTED'
  STATUS_INPROGRESS = 'INPROGRESS'
  STATUS_COMPLETED  = 'COMPLETED'
  STATUS_REJECTED   = 'REJECTED'

  def self.status_array
    [STATUS_EXPECTED,STATUS_REQUESTED,STATUS_INPROGRESS,STATUS_COMPLETED,STATUS_REJECTED]
  end

  def available?
    (!can_be_removed_yn && !removed_yn)
  end

  def self.requested
    where("status = 'REQUESTED' OR  status = 'REJECTED' OR status = 'INPROGRESS' AND requested_by IS NOT NULL")
  end
    
  def self.search(search)
    if search
      where('storage_location LIKE ?', "%#{search}%")
    else
      scoped
    end
  end  

  def parents
    parents = ParentReception.where("reception_id = #{self.id}")
    return parents
  end

  def family
    return_arr = []  
    self.related.each do |p|
      p_obj = self
      r_msg = 'Related to'
      if p.reception_id == self.id.to_s
        p_obj = Reception.find(p.parent_reception_id)
        # pushed relation
        case p.type
        when 'Child'
          r_msg = "Child of"
        when 'Parent'
          r_msg = "Parent of"
        when 'Replacement'
          r_msg = "Replacement of"
        end
      else
        p_obj = Reception.find(p.reception_id)
        # received relation
        case p.type
        when 'Child'
          r_msg = "Parent of"
        when 'Parent'
          r_msg = "Child of"
        when 'Replacement'
          r_msg = "Replaced by"
        end
      end
      return_arr << [p_obj,r_msg]
    end
    return return_arr
  end
  
  def related
    return ParentReception.where("reception_id = #{self.id} or parent_reception_id = #{self.id}")
  end

  def used_in
    ParentReception.where("parent_reception_id=#{self.id}")
  end

  def before_save
    self.can_be_removed_yn = false if self.can_be_removed_yn == nil
    self.removed_yn = false if self.removed_yn == nil
    unless self.references.blank? || (self.data_release.aliases && self.data_release.aliases.include?(self.references))
      if self.data_release.aliases.blank?
        self.data_release.aliases = self.references
      else
        self.data_release.aliases += ',' + self.references
      end
      self.data_release.save!
    end
    true
  end


  def formatted_data_intake_requested
    if !data_intake_requested
      return  "NO"
    else
      return  "YES"
    end
  end

  def formatted_validation_requested
    if (!validation_requested) || validation_requested == 0
      return  "NO"
    else
      return "YES"
    end
  end

  def formatted_validated_yn
    return "" if validated_yn == nil
    if (!validated_yn) || validated_yn == 0
      return  "NOT OK"
    else
      return  "OK"
    end
  end


  def formatted_data_intake_ok
    return "" if data_intake_ok == nil
    if (!data_intake_ok) || data_intake_ok == 0
      return  "NOT OK"
    else
      return  "OK"
    end
  end



  def formatted_purpose
    if (purpose == "TEST" || purpose == 'Test' || purpose == 'test')
      return  "Sample Data"
    else
      return "Production Data"
    end
  end

  def used_in_conversions
    usedin =[]
    usedin = Conversion.find_by_sql("select * from conversions where (reference_numbers regexp 'reception-id-#{id}')")

    
  end

  def creation_conversion
    Conversion.where("reception_reference = '#{id}'").first
  end
  
    def find_related_conversion(id)
    Conversion.where("reception_reference = '#{id}'").first
  end


  def isexternal?
   if creation_conversion
     return false
   else
     return true
   end
  end

  def parent_data
    sourceconv =  creation_conversion
    reception_array = []
    if sourceconv
      reception_array =  sourceconv.source_receptions
    end
    return reception_array
  end

  def root_data
    root_data = []
    queue = Queue.new
    queue << self
    while !queue.empty?
      reception = queue.pop
      if reception.isexternal?
        root_data << reception
      else
        parents = reception.parent_data
        parents.each do |parent|
          queue << parent
        end
      end
    end
    return root_data
  end

  def storage_location_path
    continent = ''
    dtype = self.data_type.name.downcase
    if self.purpose == 'test'
      purp = self.purpose.to_s
    else
      purp = "prod"
    end
    if self.data_release
      suppl_abbr = self.data_release.company_abbr.ms_abbr_3.to_s.downcase
      if self.data_release.release_code.blank?
        datarel = self.data_release.name.to_s.gsub('.','_').gsub(/(Q)/,'_Q')
      else
        datarel = self.data_release.release_code
      end
    else
      suppl_abbr = ''
      datarel = ''
    end
    if self.region
      area = self.region.region_code.downcase
      if self.region.ruby_type == 'Continent'
        continent = area
      end
      if self.region.parent && continent.blank?
        continent = self.region.parent.continent_code.downcase if !self.region.parent.continent_code.blank?
      end
      if continent.blank?
        continent = area
      end
    else
      area = '[COUNTRY]'
      continent = '[CONT]'
    end
    return ConfigParameter.get('data_input_directory') + purp + '/' + dtype + '/' + continent + '/' + area +
      '/' + suppl_abbr + '_' + datarel + '_' + Time.now.strftime("%Y%m%d") # no project_id dependency
  end

  def core_tags
    return_arr = []
    if self.data_release
      return_arr << self.data_release.company_abbr.ms_abbr_3
      if self.data_release.release_code != ''
        return_arr << self.data_release.release_code.gsub('.','_').gsub(' ','').strip
      else
        return_arr << self.data_release.name.gsub('.','_').gsub(' ','').strip
      end
    end
    return_arr << self.region.region_code if self.region
    return_arr << self.data_type.name if self.data_type
    return_arr << self.filling.name if self.filling
    return return_arr
  end

  def self.generate_quarter_release
    date = Date.today
    quarters = [[1,2,3], [4,5,6], [7,8,9], [10,11,12]]
    quarter = (quarters.index(quarters[(date.month - 1) / 3]) + 1)
    return "#{date.year}Q#{quarter}"
  end

  def mark_for_removal_allowed?
    if !self.can_be_removed_yn && !self.persistent_yn && !self.removed_yn
      return true
    else
      return false
    end
  end
  
  def removal_allowed?
    if self.can_be_removed_yn && !self.persistent_yn && !self.removed_yn
      return true
    else
      return false
    end
  end

  def related_datasets
    dss = []
    dss << self.data_set if self.data_set
    dss += self.process_data_elements.active.collect{|pde|pde.data_set if pde.data_set}.uniq.compact
    return  dss.uniq
  end

end
