class ReceptionLine < ActiveRecord::Base
  
  attr_accessor :tags
  after_initialize :init
  
  def init
    @tags = []
  end
  
  def absolute_filepath
    File.join(path,file)
  end
  
  def expand
    lines = []
    Dir["#{absolute_filepath}/**/*"].each do |file|
      rl = self.dup
      rl.file = File.basename(file)
      rl.path = File.dirname(file)
      lines << rl unless rl.absolute_filepath == self.absolute_filepath
    end
    return lines
  end
  
  def tag tagstring
    self.tags << tagstring unless self.tags.index tagstring
  end

  
end
