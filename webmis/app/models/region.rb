class Region < ActiveRecord::Base
  self.inheritance_column = "ruby_type"
  has_many :coverages_regions
  has_many :coverages, :through => :coverages_regions
  has_many :region_aliases
  has_many :regions, :foreign_key => :parent_id
  has_many :product_designs
  has_many :conversion_databases
  acts_as_tree 

  has_many :region_modules
  has_many :app_modules, :through => :region_modules
  has_and_belongs_to_many :poi_mapping_layouts
  has_many :test_area_configurations

  validates_length_of :country_iso3_code, :is => 3, :allow_nil => true, :allow_blank => true
  validates_length_of :country_iso_code, :is => 2, :allow_nil => true, :allow_blank => true
  validates_length_of :area_code, :minimum => 2, :allow_nil => true
  validates_length_of :continent_code, :is => 3, :allow_nil => true
  validates_length_of :city_iso_code, :is => 3, :allow_nil => true
  validates :dakota_id, :uniqueness => true, :presence => true, :if => :dakota_id_required?

  scope :active, -> {where(:isactive => true).order(:name)}

  def active?
    return isactive
  end

  def dakota_id_required?
    return isactive && ruby_type != "RegionAlias"
  end

  def country_iso_unique?
    if self.ruby_type == "Country"
      m = Region.where(:country_iso_code => self.country_iso_code,:ruby_type => "Country").last
    end
    if m.nil?
      return true
    else
      return false
    end
  end

  def app_module_ids
    self.app_modules.map(&:id)
  end

  def app_module_ids=(new_ids)
    ids = (new_ids || []).reject(&:blank?)
    old_ids = self.app_module_ids

    self.transaction do
      RegionModule.destroy_all({ :app_module_id => old_ids - new_ids, :region_id => self.id })
      (new_ids - old_ids).each do |app_module_id|
        if app_module_id != nil
         self.region_modules.create!(:app_module_id => app_module_id)
        end
      end
    end
  end

 def self.find_all_for_module(app_module)
   Region.joins("join region_modules on region_modules.region_id = regions.id join app_modules on region_modules.app_module_id = app_modules.id").where("app_modules.name = '#{app_module}'").order("regions.name")
 end
 
 def self.product_coverage_select_tag
   # Region.active.where("supplier_id is null or supplier_id = ''").order(:name).collect {|r|[r.name,r.id]}
   regions_select = []
   Region.where(:isactive => true).order(:name).group_by { |m| m.ruby_type if m.ruby_type != 'UpdateRegion' }.each do |c|
     regions_select << [[c[0]],c[1].collect {|s| [s.name,s.id] if s.ruby_type != 'UpdateRegion'}] if c[0]
   end
   return regions_select
 end
 
 def region_code
   r = ''
   case self.ruby_type
   when "RegionAlias"
     r =  Region.find(self.region_id).region_code
   when "Continent"
     r=self.continent_code
   when "Country"
     r=self.country_iso3_code
   when "City"
     r=self.city_iso_code
   when "Area"
     r=self.area_code
   when "State"
     r=self.state_code
   when "UpdateRegion"
     r=self.name
   else
     r=self.country_iso3_code
   end
   if r.blank?
     if !self.country_iso3_code.blank?
       r=self.country_iso3_code
     end
   end
   return r if !r.blank?
 end

 def self.find_by_region_code(code)
   val = []
   4.times do code
     val << code
   end
   hit = Region.active.where(["country_iso3_code = ? OR continent_code = ? OR state_code = ? OR city_iso_code OR area_code = ?"] + val)
   if !hit.empty?
     return hit.first
   end
 end

 def self.find_all_by_region_code(code,active=true)
   val = []
   4.times do code
     val << code
   end
   if active
     hit = Region.active.where(["country_iso3_code = ? OR continent_code = ? OR state_code = ? OR city_iso_code OR area_code = ?"] + val)
   else
     hit = Region.where(["country_iso3_code = ? OR continent_code = ? OR state_code = ? OR city_iso_code OR area_code = ?"] + val)
   end
   hit.each do |h|
     hit = hit + h.region_aliases 
   end
   return hit
 end

 def self.real_area_grouped_for_select(return_val='id')
   regions_select = []
   Region.where(:isactive => true).order(:name).group_by { |m| m.ruby_type if m.ruby_type != 'Area' && m.ruby_type != 'RegionAlias' && m.ruby_type != 'UpdateRegion' }.each do |c|
     if return_val != 'region_code'
       regions_select << [[c[0]],c[1].collect {|s| [s.name,s[return_val]] if s.ruby_type != 'Area' && s.ruby_type != 'RegionAlias' && s.ruby_type != 'UpdateRegion'}] if c[0]
     else
       regions_select << [[c[0]],c[1].collect {|s| [s.name,s.region_code] if s.ruby_type != 'Area' && s.ruby_type != 'RegionAlias' && s.ruby_type != 'UpdateRegion'}] if c[0]
     end
   end
   return regions_select
 end

 def self.splitby_area_grouped_for_select(return_val='id')
   regions_select = []
   Region.where(:isactive => true).order(:name).group_by { |m| m.ruby_type if m.ruby_type != 'UpdateRegion' && m.ruby_type != 'RegionAlias' }.each do |c|
     if return_val != 'region_code'
       regions_select << [[c[0]],c[1].collect {|s| [s.name,s[return_val]] if s.ruby_type != 'UpdateRegion' && s.ruby_type != 'RegionAlias'}] if c[0]
     else
       regions_select << [[c[0]],c[1].collect {|s| [s.name,s.region_code] if s.ruby_type != 'UpdateRegion' && s.ruby_type != 'RegionAlias'}] if c[0]
     end
   end
   return regions_select
 end

 def dakota_region_id
   if !self.dakota_id.blank?
     return "#{self.region_code.downcase}, #{self.dakota_id}"
   end
 end

 def self.dakota_region_ids
   csv = ""
   regions = Region.where("dakota_id is not null OR dakota_id != ''").order(:dakota_id)
   regions.each do |region|
     csv += "#{region.dakota_region_id}\n"
   end
   return csv
 end

 def self.dakota_region_ids_file
   csv = "#================================================================\n# region string, code\n"
   csv += Region.dakota_region_ids
   return csv
 end

 def dakota_name
   return "#{self.region_code.downcase}    #{self.name}    #{self.name.upcase}    #{self.region_code.upcase}"
 end

 def self.dakota_names
   names = ""
   longest_name = Region.active.collect{|r|r.name}.max {|a,b|a.length <=> b.length}
   longest_code = Region.active.collect{|r|r.region_code}.max {|a,b|a.length <=> b.length}
   max_name_column_width = longest_name.length + 4
   max_code_column_width = longest_code.length + 4
   Region.active.order(:name).each do |r|
     amount_name_whitespace = max_name_column_width - r.name.length
     amount_code_whitespace = max_code_column_width - r.region_code.length
     name_spacer = ''
     code_spacer = ''
     amount_name_whitespace.times do
       name_spacer += ' '
     end
     amount_code_whitespace.times do
       code_spacer += ' '
     end

     if r.ruby_type != "RegionAlias" && r.region_code
       names += "#{r.region_code.downcase}#{code_spacer}#{r.name}#{name_spacer}#{r.name.upcase}#{name_spacer}#{r.region_code.upcase}\n"
     end
   end
   return names
 end

 def self.dakota_names_file
   names = ""
   longest_name = Region.active.collect{|r|r.name}.max {|a,b|a.length <=> b.length}
   longest_code = Region.active.collect{|r|r.region_code}.max {|a,b|a.length <=> b.length}
   max_name_column_width = longest_name.length + 4
   max_code_column_width = longest_code.length + 4
   head_zero= 'CODE'
   head_one = 'CITY/AREA'
   head_two = 'COUNTRY'
   (max_code_column_width - head_zero.length).times do
     head_zero += ' '
   end
   (max_name_column_width - head_one.length).times do
     head_one += ' '
   end
   (max_name_column_width - head_two.length).times do
     head_two += ' '
   end

   names = "= do NOT use TAB characters in this file !!!\n"
   names += "================================================================\n"
   names += "#{head_zero}#{head_one}#{head_two}C-CODE\n"
   names += Region.dakota_names
   return names
 end

 def self.generate_dakota_id
    region = Region.where("dakota_id is not null OR dakota_id != ''").order(:dakota_id).last
    if region
      return region.dakota_id + 1
    else
      return 1
    end
 end

 def real_region?
   case self.ruby_type
   when "RegionAlias"
     return false
   when "UpdateRegion"
     return false
   else
     return true
   end
 end

 def test_area_configurations_string(p_product_line_id=nil)
   tacs = []
   r_str = []
   if p_product_line_id
     tacs  = self.test_area_configurations.where("product_line_id = ?",p_product_line_id)
   else
     tacs = self.test_area_configurations.where("product_line_id = '' or product_line_id is null")
   end
   tacs.each do |tac|
     tac.generate_areas.each do |val|
       r_str << "#{val[0].parameterize.underscore} #{val[1]["x1"]} #{val[1]["y1"]} #{val[1]["x2"]} #{val[1]["y2"]}"
     end
   end
   return r_str.join("\n")
 end
 
end
