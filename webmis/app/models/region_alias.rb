class RegionAlias < Region
  belongs_to :region
  validates :region_id ,:presence => true
  validates :name , :uniqueness => { :scope => :region_id, :message => "already exist as alias.", :case_sensitive => false }, :presence => true
end
