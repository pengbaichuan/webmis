class ReleaseNoteAttachment < ActiveRecord::Base 
  has_many :release_note_attachments_product_categories
  has_many :product_categories, :through => :release_note_attachments_product_categories

  scope :active, -> {where('release_note_attachments.removed_yn = 0 OR release_note_attachments.removed_yn IS NULL')}
  
  validates :title, :presence => true
  validates :package_directory, :presence => true
end
