class ReleaseNoteAttachmentsProductCategory < ActiveRecord::Base
  belongs_to :release_note_attachment
  belongs_to :product_category

  scope :active, -> {where('release_note_attachments_product_categories.removed_yn = 0 OR release_note_attachments_product_categories.removed_yn IS NULL')}
  
  validates :product_category_id, uniqueness: {:scope => [:removed_yn,:release_note_attachment_id], :if => Proc.new{|r| r.removed_yn.nil? }  }
end
