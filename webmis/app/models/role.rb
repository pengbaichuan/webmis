class Role < ActiveRecord::Base
  has_many :roles_users
  has_many :users, :through => :roles_users
  has_many :role_actions

  def self.select_tag
    self.order(:title).all.collect {|r|[r.title,r.id]}
  end
end
