class RoleAction < ActiveRecord::Base
  belongs_to :role

  validates :controller_name, :presence => true
  validates :role_id, :presence => true

  scope :defaults ,-> {where("action_name is null or action_name = ''")}
  before_save :expire_cache

  @@ability_hash = nil
  
  def model_naming
    if Object.const_defined?(self.controller_name.classify)
      return self.controller_name.classify
    else
      return nil
    end
  end

  def self.app_action_controllers_hash
    returnhash = {}
    Rails.application.routes.routes.map do |route|
      if returnhash[route.defaults[:controller]].nil?
        returnhash[route.defaults[:controller]] = [] << route.defaults[:action]
      else
        returnhash[route.defaults[:controller]] =  returnhash[route.defaults[:controller]] << route.defaults[:action]
      end
    end
    return returnhash
  end

  def expire_cache
    RoleAction.expire_cache
  end

  def self.expire_cache
    Rails.cache.delete(:ability_hash)
  end

  def self.feed_ability
    cachehash = Rails.cache.fetch(:ability_hash)
    if cachehash
      return cachehash
    end
    
    returnhash = {}
    self.app_action_controllers_hash.each do |k,v|
      if !k.nil? && !k.classify.match(':') && Object.const_defined?(k.classify) # Ability needs Objects
        default_roles = []
        assigned_roles = []
        default_roles = RoleAction.defaults.where(:controller_name => k).collect{|ac|ac.role.title}.uniq.sort

        v.each do |action|
          assigned_roles = RoleAction.where(:action_name => action,:controller_name => k).collect{|ra|ra.role.title}

          if assigned_roles.empty? && !default_roles.empty?
            if returnhash[k].nil?             
              returnhash[k] = [] << ({action=>default_roles})
            else
              returnhash[k] = returnhash[k] << ({action=>default_roles})
            end
          elsif !assigned_roles.empty?
            if returnhash[k].nil?
              returnhash[k] = [] << ({action=>assigned_roles})
            else
              returnhash[k] = returnhash[k] << ({action=>assigned_roles})
            end
          end
        end
      end
    end
    Rails.cache.write(:ability_hash,returnhash)
    return returnhash
  end

  def used_role_ids
    RoleAction.where(:action_name => self.action_name,:controller_name => self.controller_name).collect{|ra|ra.role.id}
  end

  def same_action_name
    RoleAction.where(:action_name => self.action_name,:controller_name => self.controller_name).collect{|ra|ra.id}
  end

  def same_controller_name
    RoleAction.where(:controller_name => self.controller_name).collect{|ra|ra.id}
  end

  def used_action_names
    actions = []
    self.same_controller_name.each do |id|
      action = RoleAction.find(id).action_name
      action = 'default' if action.blank?
      actions << action
    end
    return actions
  end

  def unused_action_names
    returnarray = []
    returnarray =  RoleAction.app_action_controllers_hash[self.controller_name]
    if RoleAction.defaults.where(:controller_name => self.controller_name).empty?
      returnarray << ''
    end
    RoleAction.where(:controller_name => self.controller_name).each do |role_action|
      if returnarray.include?(role_action.action_name)
        returnarray.delete_at(returnarray.index(role_action.action_name))
      end
    end
    return returnarray.sort
  end

  def unused_roles
    all_roles = Role.all.collect{|r|r.id}
    RoleAction.where(:action_name => self.action_name,:controller_name => self.controller_name).each do |ra|
      if all_roles.include?(ra.role_id)
        all_roles.delete_at(all_roles.index(ra.role_id))
      end
    end
    return all_roles.collect{|r|[Role.find(r).title,r]}
  end

  def self.app_controllers
    returnarray = []
    Rails.application.routes.routes.map do |route|
      returnarray << route.defaults[:controller]
    end
    return returnarray.uniq.compact.sort
  end

  def self.unmapped_app_controllers
    used = RoleAction.all.collect{|a|a.controller_name}
    all = self.app_controllers
    return all.reject{|c| used.include? c}
  end

  def self.migrate_old_ablity
    create_migration(['logistics admin'],['Article','HighLevelBom','StockDepot'])
    create_migration(['logistics admin','logistics manager'],['Customer','CustomerDepot','WarehouseStock','ShippingOrder','StockOrder','StockOrderline'])
    create_migration(['data handler'],['Reception','Stock','CompanyAbbr','DataRelease','DataType','InboundOrderline'])
    create_migration(['conversion engineer'],['Conversion','Server','Conversiontool','AssemblyHeader','AssemblyLine','ConversionDatabase','DataRelease','ViewReceptionsAndConversiondb','CvtoolTemplate','CvtoolBundle','Stock'])
    create_migration(['test engineer','product manager'],['TestRequest'])
    create_migration(['product manager'],['Product','Productset','ProductCategory','ProductionOrder','BusinessProcess','DataRelease','DistributionList','DistributionListMember','CompanyAbbr','Datasource','Ordertype','ProcessData','DefaultContentReleaseNote','ReleaseNoteAttachment','ReleaseNoteAttachmentsProductCategory'])
    create_migration(['financial controller','logistics admin'],['ArticleLicense'])
    create_migration(['shipping handler'],['OutboundOrder','ProductLocation','Stock'])

    create_migration(['financial controller'],['ShippingOrderline'],'invoice')
    create_migration(['financial controller'],['ShippingOrderline'],'process_unpaid_to_paid')
    create_migration(['financial controller'],['ShippingOrderline'],'paid')
    create_migration(['test engineer'],['Stock'],'medium_validated')
    create_migration(['test engineer'],['Stock'],'reject_medium')
    create_migration(['read only'],['Reception'],'requests')
    create_migration(['read only'],['Reception'],'new_request')
    create_migration(['read only'],['Reception'],'edit_request')
    create_migration(['read only'],['Reception'],'get_datareleases')
    create_migration(['read only'],['Reception'],'get_areas')
    create_migration(['read only'],['Server'],'overview')
    create_migration(['read only'],['Document'],'download')
    create_migration(['read only'],['AssetType'],'manage')
  end

  def self.create_migration(roles,model_names,action_name=nil)
    role_ids = roles.collect{|r|Role.find_or_create_by_title(r).id}
    role_ids.each do |rid|
      model_names.each do |model_name|
        controller_name = model_naming.underscore.pluralize
        new = RoleAction.new(:controller_name=>controller_name,:role_id=>rid)
        if action_name
          new.action_name = action_name
        end
        existing = RoleAction.where(:controller_name=>controller_name,:role_id=>rid)
        existing = RoleAction.where(:controller_name=>controller_name,:role_id=>rid,:action_name=>action_name) if action_name
        if existing.empty?
          new.save
        else
          puts 'RoleAction already exist.'
        end
      end
    end
  end

end
