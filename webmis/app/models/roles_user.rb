class RolesUser < ActiveRecord::Base
  belongs_to :user
  belongs_to :role
  belongs_to :project
  has_one :environment, :through => :project

  before_save :defaults

  def defaults
    self.project_id = Project.default.id if self.project_id.blank?
  end

  def self.update_user_roles(project_id,current_user_obj,member_roles=[])
    default_role = Role.find_by_title(ConfigParameter.get("default_role_name")) # no project_id dependency
    admin_role = Role.find_by_title("admin")
    project = Project.find(project_id)
    ProjectUser.transaction do
      current_project_users = project.project_users
      # remove unwanted users
      current_project_users.each do |cpu|
        if !member_roles.collect{|mr|mr[0] if mr}.index("#{cpu.user_id}")
          cpu.destroy
        end
      end
      # when project creator/owner forgot to add himself as member to project
      if member_roles.collect{|mr|mr[0]}.index("#{current_user_obj.id}") == nil
        new_project_user = ProjectUser.new({:user_id => current_user_obj.id,:project_id => project_id})
        new_project_user.save!
        ad_admin_role_user = RolesUser.new({:user_id => current_user_obj.id,:project_id => project_id,:role_id => admin_role.id, :module => "MIS"})
        ad_admin_role_user.save!
      end
      member_roles.each do |mr|
        if mr && mr[1]
          user_id = mr[0]
          role_ids = mr[1].split(',')
          user = User.find(user_id)
          if project.users.where(:id => user_id).empty?
            pu = ProjectUser.new({:project_id => project.id, :user_id => user_id})
            pu.save!
          end
          # remove unwanted roles for user
          current_role_users = user.roles_users.where(:project_id => project.id)
          current_role_users.each do |cru|
            if !role_ids.index(cru.role_id)
              cru.destroy
            end
          end
          # when no role is defined for a user
          if role_ids.empty?
            role_ids << default_role.id
          end
          # add roles
          role_ids.each do |role_id|
            if RolesUser.where(:project_id => project.id,:user_id=> user.id,:role_id => role_id).empty?
              new_role = RolesUser.new({:user_id => user.id, :role_id => role_id, :module => "MIS", :project_id => project_id})
              new_role.save!
            end
            # read only always available for project users
            if RolesUser.where(:project_id => project.id,:user_id=> user.id,:role_id => default_role.id).empty?
              new_role = RolesUser.new({:user_id => user.id, :role_id => default_role.id, :module => "MIS", :project_id => project_id})
              new_role.save!
            end
          end
          # admin for creator when no admin is defined
          if project.users.eager_load("roles_users","project_users").where("roles_users.role_id = #{admin_role.id} and roles_users.project_id = #{project.id}").empty?
            new_role = RolesUser.new({:user_id => current_user_obj.id, :role_id => admin_role.id, :module => "MIS", :project_id => project_id})
            new_role.save!
          end
        end
      end
    end
  end

  def self.user_names_by_role(role_title,project_id)
    RolesUser.eager_load(:role,:user).where("roles.title = '#{role_title}' AND roles_users.project_id = #{project_id}").collect{|ru|ru.user.user_name if ru.user.active?}.compact.uniq.sort
  end

end
