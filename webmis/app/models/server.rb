class Server < ActiveRecord::Base
  attr_accessor :running_jobs

  belongs_to :owned_by, :class_name => "Project", :foreign_key => :owned_by_project_id
  has_many :project_servers
  has_many :job_types
  has_many :projects, :through => :project_servers

  validates :owned_by_project_id, :presence => true
  validate :server_id_validation

  def self.owned_by(project_id)
    Server.where(:owned_by_project_id => project_id)
  end

  def self.allocated_to(project_id, job_type = nil)
    if job_type
      return Server.joins(:projects, :job_types).where("projects.id = #{project_id} and job_types.name = ? and job_types.allowed_yn = 1", job_type)
    else
      return Server.joins(:projects).where("projects.id = #{project_id}")
    end
  end

  def server_name
    name = self.server_id.strip.scan(/(^[^\W]\w*)/)[0]
    if name
      return name.flatten.join
    else
      return '' # health check will disable server anyway
    end
  end

  def server_id_validation
    if self.server_id.match(/\W/)
      errors.add(:server_id, "Does not match the specifications for a server name.")
    end
  end

  def assigned_jobs
    assigned = []
    @conversion_jobs = ConversionJob.assigned.where(:run_server => self.server_id)
    @conversion_requests = Conversion.where(:server_name => self.server_id,:status => Conversion::STATUS_CONVERSION)
    assigned << @conversion_jobs.collect{|j|[j.conversion_id,"#{j.conversion.region.name} #{j.conversion.input_format}2#{j.conversion.output_format}"]}.uniq.compact
    assigned << @conversion_requests.collect{|c|[c.id,"#{c.region.name} #{c.input_format}2#{c.output_format}"]}.uniq.compact
    return assigned.uniq.compact
  end

  def set_job_type(job_type_name, allowed_yn)
    job_type = self.job_types.where("name = ?", job_type_name).first || JobType.new( { :server_id => self.id, :name => job_type_name })
    job_type.allowed_yn = allowed_yn
    job_type.save!
  end

  def set_job_types(job_type_names)
    JobType.job_types.each do |job_type_name|
      set_job_type(job_type_name, job_type_names.include?(job_type_name))
    end
  end

  def ssh(cmd)
    Server.ssh_to_server(server_id, cmd)
  end

  def self.ssh_to_server(server, cmd)
    stdout = ""
    puts "*** SSH.start(#{server},#{ENV['prod_user']}): #{cmd}" unless Rails.env == 'test'
    Net::SSH.start( server , ENV['prod_user']) do |session|
      session.exec!(cmd) do |_, stream, data|
        stdout << data if stream == :stdout
      end
      session.close
      return stdout
    end
  end

  def self.ssh_localhost(cmd)
    Server.ssh_to_server('localhost', cmd)
  end

  def healthy?(returnopt=nil)
    if returnopt && returnopt == 'status'
      returnstatus = true
    elsif returnopt && returnopt == 'message'
      returnmsg = true
    end
    dakota_path = '/volumes1/dakota'

    # Check SSH login
    begin
      self.ssh('')
    rescue
      status = 10
      return health_msg("Cannot establish a SSH connection to #{self.server_id}.",status,returnmsg,returnstatus)
    end

    # Check Dakota directory availability
    if self.ssh("ls -d1 #{dakota_path}").blank?
      status = 20
      return health_msg("#{self.server_id}: #{dakota_path} not available.",status,returnmsg,returnstatus)
    end
    # Check Dakota write permission
    pf = "#{dakota_path}/backoffice_health_check.chk"
    if !self.ssh("touch #{pf};rm #{pf}").blank?
      status = 30
      return health_msg("#{self.server_id}: Cannot write in #{dakota_path}.",status,returnmsg,returnstatus)
    end

    # Check MAPPROD write permission
    pf = "#{dakota_path}/MAPPROD-NONE/backoffice_health_check.chk"
    if !self.ssh("touch #{pf};rm #{pf}").blank?
      status = 40
      return health_msg("#{self.server_id}: Cannot write in #{dakota_path}/MAPPROD-NONE.",status,returnmsg,returnstatus)
    end

    # Check disk capacity
    percentage = self.ssh("df -H #{dakota_path}| grep -vE '^Filesystem' | awk '{ print $5}'").strip.to_i
    if percentage >= 99
      status = 50
      return health_msg("#{self.server_id}: Disk capacity is less then 1 percent.",returnmsg,returnstatus)
    end
    
    # Check input directory
    if self.ssh("ls -d1 #{ConfigParameter.get("data_input_directory")}").blank? # no project_id dependency
      status = 60
      return health_msg("#{self.server_id}: Cannot read #{ConfigParameter.get("data_input_directory")}.",status,returnmsg,returnstatus)
    end

    # Check Dakota scripts directory
    if self.ssh("ls -d1 #{ConfigParameter.get("dakota_scripts_dir")}").blank? # no project_id dependency
      status = 70
      return health_msg("#{self.server_id}: Cannot read #{ConfigParameter.get("dakota_scripts_dir")}.",status,returnmsg,returnstatus)
    end

    # No server issues found
    status = 0
    return health_msg("",status,returnmsg,returnstatus)
  end

  def health_msg(msg,status,returnmsg=nil,returnstatus=nil)
    if msg.blank?
      if returnmsg
        return "#{self.server_id} is healthy."
      elsif returnstatus
        return status
      else
        return true
      end
    else
      if returnmsg
        return msg
      elsif returnstatus
        return status
      else
        return false
      end
    end
  end

end
