class ShippingJob < Job
  belongs_to :packing, foreign_key: :reference_id
  belongs_to :fail_category_logged_by_user, class_name: 'User', foreign_key: :fail_category_logged_by_user_id
  has_one :outbound_order, through: :packing
  after_update :update_shipping_status
  
  comma :tagged_failed_jobs_report do
    type 'Job type'
    fail_category 'Category'
    fail_category_logged_by_user full_name: 'Tagged by'
    fail_category_logged_on 'Date tagged'
    bug_tracker_id
    fail_reason
    start_time
    finish_time
    conversion_id
    run_server
    action_log
    id
  end

  def duplicate(user=nil)
    ShippingJob.transaction do
      shipping_job_clone = self.dup
      shipping_job_clone.original_job_id = self.id
      # the rescheduled job has to be saved first because of the after_update hook
      self.status = STATUS_RESCHEDULED
      self.save!
      shipping_job_clone.status = STATUS_PLANNED
      shipping_job_clone.run_server = nil
      shipping_job_clone.start_time = nil
      shipping_job_clone.finish_time = nil
      shipping_job_clone.file_system_status = FILE_SYSTEM_STATUS_FREE
      shipping_job_clone.fail_reason = nil
      shipping_job_clone.stdout_log = nil
      shipping_job_clone.scheduler_log = nil
      shipping_job_clone.working_dir = nil
      shipping_job_clone.log_action("Cloned from shipping job #{self.id}",user)
      shipping_job_clone.save!
      shipping_job_clone.reload
      return shipping_job_clone
    end
  end

  def regenerate(user)
    ShippingJob.transaction do
      job_cancel(user, rescheduled_yn = true)
      return self.packing.create_shipping_job
    end
  end

  def perform
    begin
      return unless allow_status_change?(STATUS_PROCESSING) && self.file_system_status == FILE_SYSTEM_STATUS_FREE
      clear_scripts
      cid = File.expand_path("#{ConfigParameter.get('dakota_scripts_dir', self.project_id.to_i)}/shipping_#{id.to_s}")
      self.start_time = DateTime.now
      self.finish_time = nil
      self.stdout_log = ''
      self.fail_reason = ''
      self.exitcode = nil
      save!

      # although shipping_job does not really need a working dir, still create one as some generic job methods might expect its existence
      working_dir_prefix = Time.now.strftime('%y%m%d-%H%M%S')
      self.working_dir = File.join(ConfigParameter.get('packing_job_working_dir', self.project_id.to_s), 'w_' + working_dir_prefix + '_' + self.id.to_s)

      File.open(cid, 'w') { |f| f.write self.run_script }

      self.run_command = "cd #{self.working_dir}; bash #{cid}"
      s  = "#! /bin/bash\n"
      s += "echo PID: $$ > #{cid}.log\n"
      s += "export PROCESSID=$$\n"
      s += "mkdir -p #{self.working_dir}\n"
      s += "#{self.run_command} >> #{cid}.log\n"
      s += "echo $? > #{cid.to_s}.done\n"
      File.open(cid + '.exec', 'w') { |f| f.write s }
      ssh "source /data/users/#{ENV['prod_user']}/.profile;nohup sh #{cid}.exec > #{cid}.log 2>&1 &"
      pid_exec = "cat #{cid}.log | grep 'ID' | cut -d' ' -f2"
      pid = ssh(pid_exec).chomp

      begin
        self.run_pid = pid
        self.set_status(STATUS_PROCESSING)
        self.set_file_system_status(FILE_SYSTEM_STATUS_ON_SERVER)
        self.log_action("#{self.status_str} shipping job")
        save!
      rescue ActiveRecord::StaleObjectError => e
        # kill needed
        kill_linux_process(pid)
        clear_working_dir
        clear_scripts
        puts "Failed setting status to PROCESSING for job #{self.id} .. killing stated linux process #{pid} on #{self.run_server}"
        puts e.message
        puts e.backtrace
      end
    rescue Exception => e
      puts e.message
      puts e.backtrace
      self.stdout_log = stdout_log + "------------\r\n" + e.message + "\r\n======\r\n" + e.backtrace.join("\r\n")
      self.fail_reason = e.message + "\r\n======\r\n" + e.backtrace.join("\r\n")
      self.set_status(STATUS_FAILED)
      self.set_file_system_status(FILE_SYSTEM_STATUS_FREE)
      self.log_action("Shipping job #{self.status_str}. #{e.message}")
      clear_scripts
    ensure
      save!
    end
  end

  def automonitor
    return unless status == STATUS_PROCESSING # only monitor running shipping jobs
    # we currently just check whether the .done file exists
    cid = File.expand_path("#{ConfigParameter.get('dakota_scripts_dir', self.project_id.to_s)}/shipping_#{id}")
    return unless File.file?("#{cid}.done") # shipping process is still running

    begin
      contents = File.read("#{cid}.done")
      standard_out_log = File.read("#{cid}.log")
      self.stdout_log = standard_out_log.size > 10000 ? standard_out_log[standard_out_log.size-10000..-1] : standard_out_log
      self.finish_time = DateTime.now
      self.exitcode = contents.to_i
      self.set_file_system_status(FILE_SYSTEM_STATUS_FREE)

      if self.exitcode == 0
        # success
        self.set_status(STATUS_COMPLETE)
        self.stdout_log += 'Shipping was completed.\r\n'
      else
        self.set_status(STATUS_FAILED)
        self.stdout_log += 'Shipping crashed.\r\n'
      end
    rescue Exception => e
      puts e.message
      puts e.backtrace
      self.stdout_log = stdout_log + '------------\r\n' + e.message + '\r\n======\r\n' + e.backtrace.join('\r\n')
      self.fail_reason = e.message + '\r\n======\r\n' + e.backtrace.join('\r\n')
      self.set_status(STATUS_FAILED)
      self.set_file_system_status(FILE_SYSTEM_STATUS_FREE)
      self.finish_time = DateTime.now
    ensure
      save!
      clear_scripts
    end
  end

  def clear_scripts
    pid = File.expand_path("#{ConfigParameter.get('dakota_scripts_dir', self.project_id.to_s)}/shipping_#{id}")
    FileUtils.rm_f("#{pid}")
    FileUtils.rm_f("#{pid}.done")
    FileUtils.rm_f("#{pid}.exec")
    FileUtils.rm_f("#{pid}.log")
  end

  def terminate(user_ref = 'system')
    # for a shipping job the terminate and cancel methods are equivalent
    job_cancel(user_ref)
  end

  def cancel(user_ref = 'system')
    job_cancel(user_ref)
  end

  def reschedule(user_ref = 'system', manual_yn = false)
    return unless allow_reschedule?

    ShippingJob.transaction do
      job_cancel(user_ref, rescheduled_yn = true)
      user = (user_ref.class == User ? user_ref : nil)
      new_job = duplicate(user)
      if manual_yn
        new_job.set_status(STATUS_MANUAL)
      else
        new_job.queue
      end
      new_job.save!
    end
  end

  def update_shipping_status
    if (self.status_changed? || self.file_system_status_changed?) && self.status != STATUS_RESCHEDULED
      self.packing.update_status if self.packing
    end
  end
end
