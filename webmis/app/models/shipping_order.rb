class ShippingOrder < ActiveRecord::Base
  # TODO: Remove Deleted attribute

  has_many :shipping_orderlines
  has_many :shipping_order_statuses
  has_many :warehouse_stock
  has_many :invoices

  belongs_to :article
  belongs_to :ordering_customer_depot, class_name: 'CustomerDepot', foreign_key: 'ordering_customer_depot_id'
  belongs_to :shipping_customer_depot, class_name: 'CustomerDepot', foreign_key: 'shipping_customer_depot_id'
  belongs_to :invoicing_customer_depot, class_name: 'CustomerDepot', foreign_key: 'invoicing_customer_depot_id'
  belongs_to :init_user, class_name: 'User', foreign_key: 'initiated_by_user_id'

  validates :article_id, presence: true
  validates :po_reference, presence: true
  validates :ordering_customer_depot_id, presence: true
  validates :shipping_customer_depot_id, presence: true
  validates :invoicing_customer_depot_id, presence: true

  STATUS_DRAFT     =  0
  STATUS_RECEIVED  = 10
  STATUS_ALLOCATED = 30
  STATUS_SHIPPED   = 40
  STATUS_DELIVERED = 50
  STATUS_INVOICED  = 60
  STATUS_PAID      = 70
  STATUS_CANCELLED = 80

  @@status_array     = Array.new
  @@status_array[STATUS_DRAFT]     = 'Draft'
  @@status_array[STATUS_RECEIVED]  = 'Received'
  @@status_array[STATUS_ALLOCATED] = 'Allocated'
  @@status_array[STATUS_SHIPPED]   = 'Shipped'
  @@status_array[STATUS_DELIVERED] = 'Delivered'
  @@status_array[STATUS_INVOICED]  = 'Invoiced'
  @@status_array[STATUS_PAID]      = 'Paid'
  @@status_array[STATUS_CANCELLED] = 'Cancelled'

  def status_str
    @@status_array[self.status]
  end

  def self.status_array
    return @@status_array
  end

  def self.status_hash
    s_hash = Hash.new
    @@status_array.each do |s|
      if !s.nil?
        s_hash[s] = @@status_array.index(s)
      end
    end
    return s_hash.sort_by { |_,v| v }
  end

  def allocate
    raise 'Order already allocated' if self.status >= STATUS_ALLOCATED
    ShippingOrder.transaction do
      self.status = STATUS_ALLOCATED
      shipping_orderlines.each do |orderline|
        orderline.allocate
      end
      save!
    end
    return true
  end

  def cancel
    return if self.status >= STATUS_PAID # end status
    ShippingOrder.transaction do
      self.status = STATUS_CANCELLED
      self.shipping_orderlines.each{|sol| sol.cancel}
      save!
    end
  end

  def all_lines_equal_status?
    e = true
    v = 'empty'
    self.shipping_orderlines.each do |sol|
      if v != sol.status
        e = false if v != 'empty'
        v = sol.status
      end
    end
    return e
  end


end
