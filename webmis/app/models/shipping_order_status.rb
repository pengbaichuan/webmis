class ShippingOrderStatus < ActiveRecord::Base
  belongs_to :shipping_order
  belongs_to :shipping_orderline
  
  validates :shipping_order_id, :presence => true
  
end
