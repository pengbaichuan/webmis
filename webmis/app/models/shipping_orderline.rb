class ShippingOrderline < ActiveRecord::Base

  belongs_to :shipping_order
  belongs_to :article
  belongs_to :invoice
  belongs_to :stock_order

  has_many :warehouse_stock
  has_one :ordering_customer_depot, through: :shipping_order
  has_many :included_article_license_purchase_orderlines
  has_many :purchase_orderlines, through: :included_article_license_purchase_orderlines
  has_many :article_licenses, through: :included_article_license_purchase_orderlines
  validates :transport_reference, uniqueness: { case_sensitive: false }, allow_blank: true,if: "transport_reference && transport_reference.downcase != 'n.a.'"

  after_save :logit

  @@statusarray     = Array.new
  @@statusarray[ShippingOrder::STATUS_DRAFT]     = 'Draft'
  @@statusarray[ShippingOrder::STATUS_RECEIVED]  = 'Received'
  @@statusarray[ShippingOrder::STATUS_ALLOCATED] = 'Allocated'
  @@statusarray[ShippingOrder::STATUS_SHIPPED]   = 'Shipped'
  # @@statusarray[ShippingOrder::STATUS_DELIVERED] = 'Delivered'
  @@statusarray[ShippingOrder::STATUS_INVOICED]  = 'Invoiced'
  @@statusarray[ShippingOrder::STATUS_PAID] = 'Paid'
  @@statusarray[ShippingOrder::STATUS_CANCELLED] = 'Cancelled'

  scope :allocated, -> {where("(status = #{ShippingOrder::STATUS_ALLOCATED})")}

  def self.status_hash
    s_hash = Hash.new
    @@statusarray.each do |s|
      if !s.nil?
        s_hash[s] = @@statusarray.index(s)
      end
    end
    return s_hash.sort_by { |_,v| v }
  end

  def status_str
    if self.invoice_id
      'On invoice'
    else
      @@statusarray[self.status]
    end
  end

  def edit_allowed?
    self.status < ShippingOrder::STATUS_SHIPPED
  end

  def logit(remark_str='-')
    @last_log = ShippingOrderStatus.where(shipping_orderline_id: self.id).last
    if !@last_log.nil?
     @new_log = @last_log.dup
    else
      @new_log = ShippingOrderStatus.new
      @new_log.remarks = 'Created.'
    end

    @new_log.shipping_orderline_id = self.id
    @new_log.shipping_order_id = self.shipping_order_id
    @new_log.level = 'line'
    @new_log.status = self.status
    session = Thread.current[:session]
    unless session.nil?
        @new_log.user_id = session[:user].id
    end

    if !@last_log.nil?
      @new_log.changes.each {|key, value| @new_log.remarks = "#{key} changed from #{value[0]} to #{value[1]}" }
      @new_log.created_at = Time.now
      @new_log.updated_at = Time.now
    end

    if remark_str != '-'
      @new_log.remarks = remark_str.to_s
    end

    @new_log.save!
    self.group_status
  end

  def cancel
    return if self.status >= ShippingOrder::STATUS_PAID   # end status
    deallocate rescue true   # try to deallocate but don't sweat it
    self.status = ShippingOrder::STATUS_CANCELLED
    save!
  end

  def deallocate
      raise "Cannot deallocate orderline #{self.id} at invalid status" if self.status >= ShippingOrder::STATUS_SHIPPED
      quantity_left_to_deallocate = self.amount
      # find stock - conditions to expand
      allocated_stock = warehouse_stock
      allocated_stock.each do |stock|
        raise "Cannot deallocate - Stock #{self.id} at illegal status"  if stock.status != ShippingOrder::STATUS_SHIPPED
        quantity_left_to_deallocate = stock.deallocate(quantity_left_to_deallocate)
        break if quantity_left_to_deallocate == 0
      end

      if quantity_left_to_deallocate != 0
          raise "Not enough stock available for deallocation on orderline #{self.id}" if quantity_left_to_deallocate == 0
      end
      self.status = ShippingOrder::STATUS_RECEIVED
      save!
  end


  def allocate
    if self.article && self.article.non_stockable != true # unlimited stock
      raise 'Line already allocated' if self.status >= ShippingOrder::STATUS_ALLOCATED
      self.status = ShippingOrder::STATUS_ALLOCATED
      quantity_left_to_allocate = self.amount
      # find stock - conditions to expand
      available_stock = WarehouseStock.where(["article_id = ? and status = #{WarehouseStock::STATUS_AVAILABLE} and quantity > 0", self.article_id]).order('created_at')
      available_stock.each do |stock|
        quantity_left_to_allocate = stock.allocate(self,quantity_left_to_allocate)
        break if quantity_left_to_allocate == 0
      end
      if quantity_left_to_allocate != 0
        raise 'Not enough stock'
      end
      save!
    else
      return true # skip allocation
    end
  end

  def group_status
    if self.shipping_order.all_lines_equal_status?
      @so = self.shipping_order
      @so.status = self.status
      @so.save
    end
  end

end
