class SpaceReserved < ActiveRecord::Base

  def self.reserve_space(path, size)
    #check size
    mount_dir = get_mount_dir(path)
    free =  directory_free_space(mount_dir)
    buffer_size = ConfigParameter.find_by_cfg_name("reserve_buffer_size_bytes").cfg_value.to_i
    sr = SpaceReserved.find_by_path(mount_dir)
    if !sr
      sr = SpaceReserved.new(:path => mount_dir)
      sr.save!
  end
    if free < size + sr.reserved_size.to_i + buffer_size.to_i
      raise "Not enough space on #{path} (need #{size+buffer_size-free} more bytes)"
    end
    sr.total_size = free
    sr.buffer_size = buffer_size
    sr.reserved_size = sr.reserved_size.to_i + size
    sr.reserved_amount = sr.reserved_amount.to_i + 1
    sr.save!
  end

  def self.unreserve_space(path, size)
    10.times do
      begin
        #pre - space must have been reserved
        mount_dir  = get_mount_dir(path)
        free =  directory_free_space(mount_dir)
        sr = SpaceReserved.find_by_path(mount_dir)
        if sr
          sr.total_size = free
          sr.reserved_size = sr.reserved_size - size
          sr.reserved_amount = sr.reserved_amount - 1
          sr.save!
        else
          # Failure can happen when during running jobs, the output dir ConfigParameter is changed.
          # This behavior is not done!, so we log it.
          logger.debug "ERROR: Can't find \"#{mount_dir}\" based on path \"#{path}\" in SpaceReserved.unreserve_space !"
        end
        break
      rescue => e
        puts e.message
        puts e.backtrace
      end
    end
  end

  def self.get_mount_dir(dir)
    pathname = Pathname.new(dir)
    while !pathname.mountpoint?
      pathname = pathname.parent
      if pathname.root?
        break
      end
    end
    return pathname.to_s
  end

  def self.directory_size(path)
    path << '/' unless path.end_with?('/')

    raise RuntimeError, "#{path} is not a directory" unless File.directory?(path)

    total_size = 0
    Dir["#{path}**/*"].each do |f|
      total_size += File.size(f) if File.file?(f) && File.size?(f)
    end
    total_size
  end

  def self.directory_free_space(path)
    FreeDiskSpace.bytes(path)
  end

  
end
