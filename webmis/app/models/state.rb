class State < Region
  validates :state_code , :uniqueness => true,:presence => true
  validates :name , :uniqueness => true,:presence => true
end
