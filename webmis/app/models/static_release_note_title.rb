class StaticReleaseNoteTitle < ActiveRecord::Base
  belongs_to :product_category
  belongs_to :product_release_note
  validates :product_category_id, :title, :presence => true
  validates :title, uniqueness: {:scope => [:product_category_id], :message => "Title is already used for this product category."}
  
  @@statusarray     = Array.new
  @@statusarray[10]   = 'Active'
  @@statusarray[90]  =  'Inactive'
  @@statusarray[100] =  'Removed'

  def self.status_hash
    s_hash = Hash.new
    @@statusarray.each do |s|
      if !s.nil?
        s_hash[s] = @@statusarray.index(s)
      end
    end
    return s_hash.sort_by { |k,v| v }
  end
  
  def status_string
    @@statusarray[self.status]
  end  
end
