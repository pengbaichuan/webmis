class Stock < ActiveRecord::Base
validates :data_release_id,:presence => true, :if => Proc.new {|stock| stock.conversion_id}
validates :region_id,:presence => true, :if => Proc.new {|stock| stock.conversion_id}
validates :nr_of_copies_request,:presence => true, :if => Proc.new {|stock| stock.status ==  "REQUESTED"}
validates :medium_number,:presence => true, :if => Proc.new {|stock| stock.status == 'CREATED'}
validates :recognition_file,:presence => true, :if => Proc.new {|stock| stock.status ==  "REQUESTED"}
validates :medium_number, uniqueness:{:if => Proc.new {|stock| stock.status == 'ARCHIVED'}}
belongs_to :data_release
belongs_to :conversion
belongs_to :outbound_order
belongs_to :region
has_many :conversion_databases, -> {where("file_system_status != #{ConversionDatabase::FILE_SYSTEM_STATUS_REMOVED}")},:through => :conversion
belongs_to :company_abbr, :foreign_key => :supplier_id
belongs_to :production_orderline
  
scope :validated, -> {where('status = "VALIDATED" OR status = "ARCHIVED"')}


 def prepare_from_conversion(conversion)
   if conversion.data_release.company_abbr
    self.supplier_id = conversion.data_release.company_abbr.id
    self.supplier_name = conversion.data_release.company_abbr.company_name
   end
   self.data_release_id = conversion.data_release.id
   self.product_ref = conversion.productid
   self.brn = conversion.ca_brn
   self.tpd_phase_2 = conversion.tpd
   self.tpd_phase_2_description = conversion.tpd_products
   dbname = ConversionDatabase.where("conversion_id = #{conversion.id} and file_system_status < #{ConversionDatabase::FILE_SYSTEM_STATUS_REMOVED}").last.path_and_file rescue conversion.database_name
   self.databasename = dbname
   self.region_id = conversion.region_id
   if conversion.production_orderline
     if conversion.production_orderline.production_order.distribution_list
       a = 1
       conversion.production_orderline.production_order.distribution_list.distribution_list_members.collect {|c| a=a+c.hard_copy_amount}
       self.nr_of_copies_request = a
     end
   end
 end


def before_save
 if (region_id)
   r = Region.find(region_id)
   self.region_name = r.name
 end
end

end
