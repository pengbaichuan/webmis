class StockDepot < ActiveRecord::Base
  has_many :warehouse_stock
  has_many :stock_orders
end
