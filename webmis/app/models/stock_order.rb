class StockOrder < ActiveRecord::Base
  has_many :stock_orderlines
  has_many :warehouse_stock
  belongs_to :stock_depot, :foreign_key => :depot_id
  
  @@statusarray     = Array.new
  @@statusarray[0]  = 'Created'
  @@statusarray[30] = 'Ordered'
  @@statusarray[60] = 'Confirmed'
  
  def self.status_hash
    s_hash = Hash.new
    @@statusarray.each do |s|
      if !s.nil?
        s_hash[s] = @@statusarray.index(s)
      end
    end
    return s_hash.sort_by { |k,v| v }
  end

  def status_string
    @@statusarray[self.status].to_s
  end

  def close
    StockOrder.transaction do
      raise 'Stock Order at illegal status ' + status_string.to_s if self.status >= 60
      self.status = 60
      stock_orderlines.each do |line|
        raise "Orderline #{line.id} at illegal status #{line.status}" if line.status >= 60
        line.status= 60
        line.save
        line.warehouse_stock.each do |stock|
          raise "Stock #{stock.id} at illegal stock #{stock.status}" if stock.status >= 20
          stock.update_status(20)
          stock.save
        end
      end
      #save 
    end
  end
  
  def all_lines_confirmed?
    c = true
    self.stock_orderlines.each do |sol|
      if sol.status < 60
        c = false
      end
    end
    return c
  end
  
  def all_lines_ordered?
    o = true
    self.stock_orderlines.each do |sol|
      if sol.status < 30
        o = false
      end
    end
    return o
  end

end