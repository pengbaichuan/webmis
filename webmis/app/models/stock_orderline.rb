class StockOrderline < ActiveRecord::Base
  belongs_to :stock_order
  belongs_to :article
  has_many :warehouse_stock

  validates :stock_order_id, :presence => true
  validates :article_id, :presence => true
  validates :amount_order, :presence => true
  
  after_save :group_status
  
  @@statusarray     = Array.new
  @@statusarray[0]  = 'Created'
  @@statusarray[30] = 'Ordered'
  @@statusarray[50] = 'Partly Confirmed'
  @@statusarray[60] = 'Confirmed'
  
def self.status_hash
  s_hash = Hash.new
  @@statusarray.each do |s|
    if !s.nil?
      s_hash[s] = @@statusarray.index(s)
    end
  end
  return s_hash.sort_by { |k,v| v }
end

def status_string
   @@statusarray[self.status] 
end 

def group_status
  if self.stock_order.all_lines_confirmed?
    @stock_order = self.stock_order
    if @stock_order.status != 60
        @stock_order.status = 60
        @stock_order.confirmed_date = Time.now
        @stock_order.save
    end
  else
    if self.stock_order.all_lines_ordered?
      @stock_order = self.stock_order
      if @stock_order.status != 30
        @stock_order.status = 30
        @stock_order.date_ordered = Time.now
        @stock_order.save
      end
    end
  end
  
end  
  
end
