class Supplier < Company
has_many :areas
has_many :regions
scope :active, -> {where('status = 100').order(:name)}
  
def self.find_all hash=nil
 # Strange and invalid code, :company_custom does not exist in models!
 if hash && hash[:conditions]
  c = hash[:conditions] + " and deleted = 0"
  Supplier.includes(:company_custom).where(c).order(:name)
 else
  Supplier.includes(:company_custom).where('data_type_c <> "None" and data_type_c is not null').order(:name)
 end
  
end

def self.select_tag
  self.active.collect {|s|[s.name,s.id]}
end  

end
