class TestAreaConfiguration < ActiveRecord::Base
  version_fu

  validates :name, :presence => true
  validates :max_x, :presence => true
  validates :max_y, :presence => true
  validates :min_y, :presence => true
  validates :nr_of_test_areas_x, :presence => true
  validates :nr_of_test_areas_y, :presence => true
  validates :region_id, :presence => true
  validates :name, :uniqueness => true

  belongs_to :product_line
  belongs_to :region

  def generate_areas
    area_hash = {}
    x_range = 0
    y_range = 0
    if self.max_x >= 0 &&  self.min_x >= 0
      x_range = (self.max_x.round(8).abs - self.min_x.round(8).abs).abs
    elsif self.max_x < 0 && self.min_x >= 0
      x_range = (self.max_x.round(8).abs + self.min_x.round(8).abs).abs
    elsif self.max_x >= 0 && self.min_x < 0
      x_range = (self.max_x.round(8).abs + self.min_x.round(8).abs).abs
    elsif self.max_x < 0 && self.min_x < 0
      x_range = (self.max_x.round(8).abs - self.min_x.round(8).abs).abs
    end


    if self.max_y >= 0 && self.min_y >= 0
      y_range = (self.max_y.round(8).abs - self.min_y.round(8).abs).abs
    elsif self.max_y < 0 && self.min_y >= 0
      y_range = (self.max_y.round(8).abs + self.min_y.round(8).abs).abs
    elsif self.max_y >= 0 &&  self.min_y < 0
      y_range = (self.max_y.round(8).abs + self.min_y.round(8).abs).abs
    elsif self.max_x < 0 && self.min_y < 0
      y_range = (self.max_y.round(8).abs - self.min_y.round(8).abs).abs
    end
    
    x_step = (x_range / self.nr_of_test_areas_x).round(8)
    y_step = (y_range / self.nr_of_test_areas_y).round(8)

    c = 0
    xc = 0
    yc = 0
    self.nr_of_test_areas_y.times do
      xc = 0
      self.nr_of_test_areas_x.times do
        c += 1
        area_name = "#{self.name}-#{c.to_s.rjust(2,"0")}"
        if self.product_line
          area_name = "#{self.product_line.name}-#{area_name}"
        end
        x_a = self.min_x + (xc * x_step)
        y_a = self.min_y + (yc * y_step)
        x_b = ( x_a + x_step)
        y_b = ( y_a + y_step )
        area_hash[area_name] = {"x1"=>(sprintf "%.8f",x_a),"y1"=>(sprintf "%.8f",y_a),"x2"=>(sprintf "%.8f",x_b),"y2"=>(sprintf "%.8f",y_b)}
        xc += 1
      end
      yc += 1
    end
    return area_hash
  end

  def generate_exact_script
    content = []
    self.generate_areas.each do |val|
      label_x = ( ( val[1]["x1"].to_d + val[1]["x2"].to_d ) / 2 ).round(8)
      label_y = ( ( val[1]["y1"].to_d + val[1]["y2"].to_d ) / 2 ).round(8)
      content << "Exact_AddAttribute(\"#{val[1]["x1"]}, #{val[1]["y1"]}\", \"#{val[1]["x2"]}, #{val[1]["y1"]}\",9,0,3,TRUE,\"\")"
      content << "Exact_AddAttribute(\"#{val[1]["x2"]}, #{val[1]["y1"]}\", \"#{val[1]["x2"]}, #{val[1]["y2"]}\",9,0,3,TRUE,\"\")"
      content << "Exact_AddAttribute(\"#{val[1]["x2"]}, #{val[1]["y2"]}\", \"#{val[1]["x1"]}, #{val[1]["y2"]}\",9,0,3,TRUE,\"\")"
      content << "Exact_AddAttribute(\"#{val[1]["x1"]}, #{val[1]["y2"]}\", \"#{val[1]["x1"]}, #{val[1]["y1"]}\",9,0,3,TRUE,\"\")"
      content << "Exact_AddAttribute(\"#{label_x}, #{label_y}\",9,0,3,TRUE,\"#{val[0]}\")"      
    end
    return content.join("\n")
  end

  def amount_test_areas
    return ( self.nr_of_test_areas_x * self.nr_of_test_areas_y )
  end

  def file_name(extension,suffix=nil)
    fn = "test_areas_#{self.region.region_code.downcase}_#{self.name.parameterize.underscore}.#{extension}"
    fn += "_#{suffix}" if suffix
    return fn
  end


end
