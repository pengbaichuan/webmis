class TestCase < ActiveRecord::Base
  self.table_name = "mapgrade.test_cases"
  scope :active ,-> {where(:active_yn => 1)}

  def self.option_list
    return TestCase.active.order(:name).collect{|tc|[tc.name,tc.name]}
  end

  def self.supports_parallel?(name_testcase)
    if name_testcase == "detailedmapcompare"
      return true
    end
    return false
  end
end
