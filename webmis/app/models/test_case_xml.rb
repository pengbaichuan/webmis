require 'xml/mapping'
class TestCaseXml
  include XML::Mapping

  text_node :testname, "testname"
  array_node :parameters, "parameter",  :class=>ParameterXml  
end
