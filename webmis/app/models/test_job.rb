class TestJob < ActiveRecord::Base
  self.table_name = "mapgrade.test_jobs"
  self.inheritance_column = "jobtype"

  has_many :test_runs
  belongs_to :test_case
  has_many :testjob_parameters

  def test_set
    return nil
  end
end
