class TestPlan < ActiveRecord::Base
  version_fu
  include HandleVersioning

  belongs_to :product_line

  validates :status, :workflow => true
  validates :name, :presence => true, :uniqueness => true

  @@attrs_requiring_review = [:plan_details]
  def self.attrs_requiring_review
    return @@attrs_requiring_review
  end

  def create_new_version?
    r = false
    r = true if (plan_details_changed? && status_changed?)|| self.status == HandleVersioning.statusarray[HandleVersioning::STATUS_DRAFT]
    return r
  end

  def test_case_option_list(custom_plan_details=nil)
    org_option_list = TestCase.option_list
    own_option_list = []
    if self.plan_details && !custom_plan_details
      own_option_list = JSON.parse(self.plan_details).collect{|tc|[tc["test_case"],tc["test_case"]]}
      own_option_list.each do |ol|
        org_option_list.delete(ol)
      end
    elsif custom_plan_details
      own_option_list = custom_plan_details.collect{|tc|[tc["test_case"],tc["test_case"]]}
      own_option_list.each do |ol|
        org_option_list.delete(ol)
      end
    end
    return (org_option_list).uniq.sort
  end

  def self.crud_testcase(plan_details_a,test_case_name,parameter_name=nil,parameter_value=nil,rm_tc=false,rm_par=false,change_test_case_name=nil)
    if plan_details_a.blank?
      a = []
    else
      a = plan_details_a
    end
    parameters = []
    tc = []
    current_test_cases = a.collect{|tc|tc["test_case"]}
    if current_test_cases.include?(test_case_name)
      a.each do |tch|
        if tch["test_case"] == test_case_name
          tc = tch
          a.delete(tch)
          if tch["parameters"].kind_of?(Array)
            parameters = tch["parameters"]
          end
        end
      end
    end

    if !rm_tc
    parameters.each do |ph|
      if parameter_name == ph["parameter_name"]
        parameters.delete(ph)
      end
    end
    parameters << {"parameter_name" => parameter_name,"parameter_value" => parameter_value} if parameter_name && !rm_par
    tc = {"test_case" => test_case_name, "parameters" => parameters} if !rm_par && !change_test_case_name
    tc = {"test_case" => change_test_case_name, "parameters" => parameters} if change_test_case_name
    a << tc if !tc.empty?

    end
    return a
  end

  def delete_parameter(test_case_name,parameter_name,saving=true)
    h = TestPlan.crud_testcase(JSON.parse(self.plan_details),test_case_name,parameter_name,nil,false,true)
    if saving
      self.plan_details = h.to_json
      self.save
    else
      return h
    end
  end

  def delete_test_case(test_case_name,saving=true)
    h = TestPlan.crud_testcase(JSON.parse(self.plan_details),test_case_name,nil,nil,true,false)
    if saving
      self.plan_details = h.to_json
      self.save
    else
      return h
    end
  end

  def update_test_case(test_case_name,new_test_case_name,saving=true)
    h = TestPlan.crud_testcase(JSON.parse(self.plan_details),test_case_name,nil,nil,nil,nil,new_test_case_name)
    if saving
      self.plan_details = h.to_json
      self.save
    else
      return h
    end
  end

  def add_update_parameter(test_case_name,parameter_name,parameter_value,saving=true)
    h = TestPlan.crud_testcase(JSON.parse(self.plan_details),test_case_name,parameter_name,parameter_value)
    if saving
      self.plan_details = h.to_json
      self.save
    else
      return h
    end
  end

  def add_test_case(test_case_name,saving=true)
    h = TestPlan.crud_testcase(JSON.parse(self.plan_details),test_case_name)
    if saving
      self.plan_details = h.to_json
      self.save
    else
      return h
    end
  end

  def clone
    test_plan = self.dup
    test_plan.version = nil
    test_plan.name = "clone-" + SecureRandom.hex(3) + " " + self.name.gsub(/^clone-\w{6}/,'')
    test_plan.status = HandleVersioning.statusarray[HandleVersioning::STATUS_DRAFT]
    return test_plan
  end

  def self.default
    TestPlan.find_by_name("Default Test Plan")
  end

end