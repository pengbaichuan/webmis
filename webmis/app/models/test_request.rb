require 'xml/mapping'

class TestRequest < ActiveRecord::Base
  belongs_to :test_type
  belongs_to :production_orderline
  has_one :production_order, :through => :production_orderline
  belongs_to :product, :primary_key => :volumeid, :foreign_key => :product_id
  belongs_to :request_user, :class_name => "User", :foreign_key =>  "requested_by_user_id"
  belongs_to :assigned_user,:class_name => "User", :foreign_key =>  "assigned_to_user_id"
  belongs_to :test_db, :class_name => "ConversionDatabase",:foreign_key => "test_db_conversion_databases_id"
  belongs_to :ref_db,  :class_name => "ConversionDatabase",:foreign_key => "ref_db_conversion_databases_id"
  has_one :region, :through => :test_db
  has_one :conversion, :through => :test_db
  belongs_to :test_plan
  belongs_to :project
  belongs_to :test_tool_release
  has_many :test_request_jobs, :foreign_key => "reference_id"

  
  validates  :test_type_id, :assigned_user, :presence => true
  validates  :test_db_conversion_databases_id, :presence => true
  validates  :ref_db_conversion_databases_id, presence: true, if: "use_scheduler?"
  validates  :test_plan_id, presence: true, if: "use_scheduler?"

    STATUS_CREATED   = 'CREATED'
    STATUS_REQUESTED = 'REQUESTED'
    STATUS_RUNNING   = 'RUNNING'
    STATUS_FINISHED  = 'FINISHED'
    STATUS_CANCELLED = 'CANCELED'

    @@statusarray       = Array.new
    @@statusarray = [STATUS_CREATED, STATUS_REQUESTED, STATUS_RUNNING, STATUS_FINISHED, STATUS_CANCELLED]
  
  def after_initialize
    if new_record?
      self.removed_yn = false if self.removed_yn.nil?
      self.project_id = Project.default.id if self.project_id.blank?
    end
  end

  def self.statusarray
    return @@statusarray
  end

  def update_result_allowed?
    return self.status == STATUS_RUNNING
  end

  def status_running_allowed?
    return self.status == STATUS_REQUESTED
  end

  def status_finished_allowed?
    return self.status == STATUS_RUNNING
  end

  def status_cancelled_allowed?
    if self.status != STATUS_FINISHED && self.status != STATUS_CANCELLED
      return true
    else
      return false
    end
  end

  def status_requested_allowed?
    return self.status == STATUS_CREATED
  end

  def edit_allowed?
    if self.status == STATUS_CREATED || self.status == STATUS_REQUESTED
      if self.use_scheduler
        return false
      else
        return true
      end
    end
  end
  
  
  def generate_jobs
    plan = self.test_plan
    if !plan
      plan = TestPlan.find_by_name("Default Test Plan")
    end
    test_cases = JSON.parse(plan.plan_details) if plan
    jobs = []
    trj = 0
    incomplete_regions = []

    TestRequestJob.transaction do
      test_cases.each do |tc|
        
        coverage = self.test_db.tagmodel["coverage"]
        if !coverage
          # fallback to product coverage. Used for databases created by manual nds conversions
          product = Product.find_by_volumeid(self.product_id.to_s)
          if product
            coverage = product.coverage
            if coverage
              coverage= coverage.regions.collect{|x|x.region_code}
            end
          else
            raise "Product id #{self.product_id} not found, coverage cannot be determined"
          end
        end

        if !coverage || coverage.size == 0
          raise "No coverage defined for product #{self.product_id}"
        end

        complete_test_area_coverage = true
        coverage.each do |region_code|
          region = Region.find_by_region_code(region_code)
          if region.test_area_configurations.size == 0
            incomplete_regions << region.name
            complete_test_area_coverage = false
          end
        end

        if self.run_parallel && TestCase.supports_parallel?(tc["test_case"]) && complete_test_area_coverage

          coverage.each do |area|
            region = Region.find_by_region_code(area)
            # region = Region.find(2748792)
            region.test_area_configurations.each do |test_area|
              # Create TestRequestJob Create TestRun Create TestJob
              areas = test_area.generate_areas
              areas.to_a.each do |areas|
                trj = create_test_request_job(tc["test_case"], areas, tc["parameters"], region.region_code)
                jobs << trj
              end
            end
          end
        else
          trj = create_test_request_job(tc["test_case"], nil, tc["parameters"])
          jobs << trj
        end
      end
      if !incomplete_regions.empty? &&
          self.request_remarks +=  "\r\nParallel job support is not possible because there is no test Area configuration defined for the following region(s): #{incomplete_regions.uniq.join(', ')}.\r\n"
        self.save
      end
    end
  end

  def create_test_request_job(test_case, test_area=nil, test_request_parameters=[],test_area_name=nil)
    trj = nil
    base_exe_dir = ConfigParameter.get('testtool_release_directory') # no project_id dependency
    ubuntu_release =  ConfigParameter.get('testtool_ubuntu_release') # no project_id dependency
    TestRequestJob.transaction do
      trj = TestRequestJob.new
      trj.save!
      cid= File.expand_path("#{ConfigParameter.get("maptest_scripts_dir",self.project_id.to_s)}/maptest_#{trj.id.to_s}")
      execmd = "maptest"
      if self.test_tool_release
        execmd = File.join(base_exe_dir, "MapTest_#{self.test_tool_release.release_name.to_s}", ubuntu_release, "Maptest")
      end
      trj.run_command = "#{execmd} --xml #{cid + '.xml'}"
      trj.run_script = "< maptest xml >"
      trj.reference_id = self.id
      trj.project_id = self.project_id
      trj.test_case_name = test_case

      if self.test_db
        cd_test = TestRequestJobConversionDatabase.new
        cd_test.test_request_job_id = trj.id
        cd_test.conversion_database_id = self.test_db.id
        cd_test.relation_type = "test"
        cd_test.save
      else
        raise "Test database not found for test request #{self.id}"
      end

      if self.ref_db
        cd_ref = TestRequestJobConversionDatabase.new
        cd_ref.test_request_job_id = trj.id
        cd_ref.conversion_database_id = self.ref_db.id
        cd_ref.relation_type = "ref"
        cd_ref.save
      end

      trj.set_status(Job::STATUS_QUEUED)
      trj.run_script = TestSetXml.create_from_test_request_job(trj, test_case, test_area,test_request_parameters).to_yaml
      # map to label to show up in server overview
      trj.set_tag_list_on(:context_labeltestcase, trj.test_case_name)
      trj.set_tag_list_on(:context_labelarea, test_area_name.to_s)
      trj.save
    end
    return trj
  end

  def cleanup
    self.test_request_jobs.each do |trj|
      trj.complete
    end
    base_path = File.expand_path("#{ConfigParameter.get("maptest_base_work_dir",self.project_id.to_s)}")
    tr_work_dir = File.join(base_path, Rails.env.to_s, self.id.to_s)
    
    self.test_request_jobs.each do |trj|
      # clear cached dbs at test request level
      trj.clear_dir(tr_work_dir) if trj.run_server
    end
    
  end

  def cancel(user='system')
    self.test_request_jobs.each do |trj|
      trj.cancel(user)
    end
  end

  def terminate(user='system')
    self.test_request_jobs.each do |trj|
      trj.terminate(user)
    end
    base_path = File.expand_path("#{ConfigParameter.get("maptest_base_work_dir",self.project_id.to_s)}")
    tr_work_dir = File.join(base_path, Rails.env.to_s, self.id.to_s)

    self.test_request_jobs.each do |trj|
      # clear cached dbs at test request level
      trj.clear_dir(tr_work_dir) if trj.run_server
    end
  end

  def self.regions_for_select
    Conversion.includes("region").select("distinct conversions.region_id").map{|x| [x.region.name,x.region.id] if x.region }
  end

  def self.data_types_for_select
    Conversion.select("distinct conversions.output_format").order("conversions.output_format").map{|x| [x.output_format,x.output_format] }
  end

  def self.conversion_databases_for_select(selected_database_id=nil)
    dbs = ConversionDatabase.select([:path_and_file,:id,:conversion_id] ).available.map{|x| ["#{x.conversion_id} #{x.path_and_file}",x.id]}
    if !selected_database_id.blank?
      begin
      s = ConversionDatabase.find(selected_database_id)
      rescue
        dbs = [['unknown database id !',selected_database_id]] + dbs
      end
      dbs = [[s.path_and_file,s.id]] + dbs if s
    end
    return dbs
  end

  def conversions
    r = []
    r << self.test_db.conversion.id if self.test_db
    r << self.ref_db.conversion.id if self.ref_db
    return Conversion.find(r.uniq)
  end
  
  def regions
    return Region.find(self.conversions.collect{|c|c.region_id if c.region_id}.uniq)
  end

  def conversion_databases
    return ConversionDatabase.find(self.conversions.collect{|c|c.conversion_databases.collect{|cd|cd.id}}.flatten.uniq)
  end

end
