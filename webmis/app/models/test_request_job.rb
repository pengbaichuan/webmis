class TestRequestJob < Job
  require "test_case_xml"
  require "parameter_xml"
  acts_as_ordered_taggable_on :tags
  has_many :test_request_job_conversion_databases
  has_many :conversion_databases, :through => :test_request_job_conversion_databases
  belongs_to :test_request, :foreign_key => 'reference_id'
  has_one  :test_run


  def ref_db
    test_request_job_conversion_databases.each do |cd|
      return cd.conversion_database if cd.relation_type == "ref"
    end
    return nil
  end

  def test_db
    test_request_job_conversion_databases.each do |cd|
      return cd.conversion_database if cd.relation_type == "test"
    end
    return nil
  end

  def delta_db
    test_request_job_conversion_databases.each do |cd|
      return cd.conversion_database if cd.relation_type == "delta"
    end
    return nil
  end

  def simulate_maptest?
    false
  end

  def parallel_job?
    TestCase.supports_parallel?(self.test_case_name) && self.test_request.run_parallel
  end

  def perform
    begin
      # puts "Executing Test Request Job perform for  " + self.id.to_s + " lock version : " + self.lock_version.to_s
      return unless allow_status_change?(STATUS_PROCESSING) && self.file_system_status == FILE_SYSTEM_STATUS_FREE
      clear_scripts

      base_exe_dir = ConfigParameter.get('testtool_release_directory') # no project_id dependency
      ubuntu_release =  ConfigParameter.get('testtool_ubuntu_release') # no project_id dependency
      if simulate_maptest?
        cmd = maptest_simulator_path
      else
        if self.test_request.test_tool_release
          cmd = File.join(base_exe_dir, "MapTest_#{self.test_request.test_tool_release.release_name.to_s}", ubuntu_release, "Maptest")
        else
          cmd = "maptest"
        end
      end

      cid= File.expand_path("#{ConfigParameter.get("maptest_scripts_dir",self.project_id.to_s)}/maptest_#{id.to_s}")
      self.start_time = DateTime.now
      self.finish_time = nil
      if !self.run_command
        self.run_command = "#{cmd} --xml #{cid}.xml".strip
      end
      self.stdout_log = ""
      self.fail_reason = ""
      self.exitcode = nil
      save!

      working_dir_prefix = Time.now.strftime("%Y%m%d-%H%M%S") + "-#{self.id}"
      self.working_dir = "w_#{working_dir_prefix}_maptest_#{self.id}"
      runscriptobj = YAML.load(self.run_script)
      base_path = File.expand_path("#{ConfigParameter.get("maptest_base_work_dir",self.project_id.to_s)}")


      tr_work_dir = File.join(base_path, Rails.env.to_s, self.test_request.id.to_s)
      trj_work_dir = File.join(tr_work_dir, self.id.to_s, self.working_dir)
      self.working_dir = trj_work_dir
      if self.test_db
        local_test_db_path = File.join(tr_work_dir, File.dirname(self.test_db.path_and_file).split("/")[-1], "#{File.basename(self.test_db.path_and_file)}")
      end
      if self.ref_db
        local_ref_db_path = File.join(tr_work_dir, File.dirname(self.ref_db.path_and_file).split("/")[-1], "#{File.basename(self.ref_db.path_and_file)}")
      end
      if !runscriptobj.instance_of?(String)
        runscriptobj.testdatabase = local_test_db_path
        runscriptobj.referencedatabase = local_ref_db_path
        runscriptobj.resultdatabase = File.join(trj_work_dir,"result_#{self.id}.sq3")
        self.run_script = runscriptobj
        File.open(cid+ ".xml", 'w') {|f| f.write(runscriptobj.to_xml.gsub("test-set-xml","testset"))}
      end

      #write execscript
      s = "#! /bin/bash\r\n"
      s = s + "echo PID: $$ || exit\n"
      s = s + "export PROCESSID=$$\n"
      s = s + "mkdir -p  #{trj_work_dir}\n"
      s = s + "while [ ! -f #{tr_work_dir}/synced ]\n"
      s = s + "do\n"
      s = s + " if [ ! -f #{tr_work_dir}/syncing ]; then\n"
      s = s + "   touch #{tr_work_dir}/syncing\n"
      if self.test_db
        s = s + "echo Copying source db\n"
        #mkdir -p #{dest} && chmod -R 755 #{src} && rsync --remove-source-files --partial -arv #{src} #{dest} 2>&1
        s = s + "rsync --partial -arv #{File.dirname(self.test_db.path_and_file)} #{tr_work_dir}\n"
      end

      if self.ref_db
        s = s + "echo Copying ref db\n"
        s = s + "rsync --partial -arv #{File.dirname(self.ref_db.path_and_file)} #{tr_work_dir}\n"
      end

      s = s + "   touch #{tr_work_dir}/synced\n"
      s = s + "   rm #{tr_work_dir}/syncing\n"
      s = s + " else\n"
      s = s + "   echo waiting for sync\n"
      s = s + "   sleep 10\n"
      s = s + " fi\n"
      s = s + "done\n"

      
      s = s + "echo starting maptest\n"

      s = s + "cd #{trj_work_dir} && #{self.run_command} >> #{cid}.log\n"
      s = s + "echo $? >  #{cid.to_s}.done\n"
      File.open(cid + ".exec", 'w') {|f| f.write(s) }
      execstring = "source /data/users/#{ENV['prod_user']}/.profile;nohup sh #{cid}.exec >> #{cid.to_s}.log 2>&1 &"
      ssh(execstring)
      # get pid
      pidexec= "cat  #{cid}.log | grep '^PID' | cut -d' ' -f2"
      pid= ssh(pidexec).chomp
      #childpidexec = "ps -eaf | grep #{pid} | egrep 'daklite -f|dakota -f' | grep -v 'grep' | awk '{print $2}'"
      #childpid = ssh(childpidexec).chomp

      begin
        self.run_pid = pid
        self.set_status(STATUS_PROCESSING)
        self.set_file_system_status(FILE_SYSTEM_STATUS_ON_SERVER)
        self.log_action("Started #{self.status_str} job.")
        save!
      rescue ActiveRecord::StaleObjectError => e
        # kill needed
        kill_linux_process(pid)
        clear_working_dir
        clear_scripts
        puts "Failed setting status to PROCESSING for job " + self.id.to_s + " .. killing stated linux process " + pid.to_s + " on " + self.run_server.to_s
        puts e.message
        puts e.backtrace
      end
    rescue Exception => e
      puts e.message
      puts e.backtrace
      self.stdout_log = stdout_log.to_s + "------------\r\n" + e.message + "\r\n======\r\n" + e.backtrace.join("\r\n")
      self.fail_reason = e.message + "\r\n======\r\n" + e.backtrace.join("\r\n")
      ############################ cd.process_location = working_dir
      clear_scripts
      self.set_status(STATUS_FAILED)
      self.set_file_system_status(FILE_SYSTEM_STATUS_FREE)
      self.log_action("Job #{self.status_str}. #{e.message}")
    ensure
      save!
    end
  end

  def automonitor
    return unless status == STATUS_PROCESSING # only monitor running conversion jobs
    # we currently just check whether the .done file
    cid = File.expand_path("#{ConfigParameter.get("maptest_scripts_dir",self.project_id.to_s)}/maptest_#{id.to_s}")
    # puts "Checking Job " + self.id.to_s +  " at " + "#{cid.to_s}"

    return unless File.file?("#{cid.to_s}.done") # conversion process is still running
    begin
      contents = File.read("#{cid.to_s}.done")
      standardoutlog = File.read("#{cid.to_s}.log") if File.exists?("#{cid.to_s}.log")
      taillog = ""
      if standardoutlog.size > 10000
        taillog = standardoutlog[standardoutlog.size-10000..-1]
      else
        taillog = standardoutlog
      end

      if contents.to_i == 0
        # success
        TestRequestJob.transaction do
          self.exitcode = 0
          self.stdout_log = taillog
          if parallel_job?
            merge_job = determine_merge_location_job
            if merge_job.id != self.id
              self.set_file_system_status(FILE_SYSTEM_STATUS_RESERVED)  # we should reserve the space first, but for now, just go through the motions
              rsyncmove(self.working_dir, "#{merge_job.run_server}:#{merge_job.working_dir}", 'post_move')
              self.set_file_system_status(FILE_SYSTEM_STATUS_MOVING)    # you can only go from file system status 'reserved' to 'moving'
            else
              self.set_status(STATUS_COMPLETE)
              self.finish_time = DateTime.now
              self.create_merge_job
            end
          else
            self.set_status(STATUS_COMPLETE)
            self.finish_time = DateTime.now
          end
          self.save!
        end
      else
        puts "Maptest CRASHED : " + contents
        self.exitcode = contents.to_i
        self.stdout_log = taillog
        self.finish_time = DateTime.now
        fail_msg = "Maptest scripts failed."
        self.set_status(STATUS_FAILED)
        self.fail_reason = fail_msg
        self.log_action("Job #{self.status_str}. " + fail_msg)
      end
    rescue Exception => e
      puts e.message
      puts e.backtrace
      self.stdout_log = stdout_log + "------------\r\n" + e.message + "\r\n======\r\n" + e.backtrace.join("\r\n")
      self.fail_reason = e.message + "\r\n======\r\n" + e.backtrace.join("\r\n")
      self.set_status(STATUS_FAILED)
      self.finish_time = DateTime.now
    ensure
      save!
    end
  end

  def post_move
    success = false
    tries = 0
    while !success && tries < 10
      begin
        TestRequest.transaction do
          # doing update on test request to synchronize checks
          self.test_request.request_remarks = self.test_request.request_remarks + "Test Request Job #{self.id} was moved to merge location\n"
          self.set_file_system_status(FILE_SYSTEM_STATUS_FREE)
          self.log_action("Moving result completed. Job #{self.status_str}.")
          self.stdout_log = self.stdout_log.to_s + "MOVING OF RESULTS TO MERGE SERVER WAS COMPLETED\r\n"
          self.finish_time = DateTime.now
          self.test_request.save!
          self.save!
          success = true
          if open_parallel_jobs == 0
            merge_job = find_merge_job
            merge_job.set_status(STATUS_QUEUED)
            merge_job.save!
          end
          return 
        end

      rescue ActiveRecord::StaleObjectError
        self.reload
        self.test_request.reload
        tries = tries + 1
      end
    end
  end

  def open_parallel_jobs
    return nil unless TestCase.supports_parallel?(self.test_case_name)
    self.test_request.test_request_jobs.where("(status <= #{STATUS_FAILED} or file_system_status = #{FILE_SYSTEM_STATUS_MOVING}) and test_case_name = '#{self.test_case_name}'").size
  end
  
  def determine_merge_location_job
    return nil if !TestCase.supports_parallel?(self.test_case_name)
    continue = true
    tr_job = nil
    while continue
      begin
        TestRequestJob.transaction do
          tr = self.test_request
          tr_job = tr.test_request_jobs.where("test_case_name = '#{self.test_case_name}' and merge_location = true").first
          if !tr_job
            self.merge_location = true
            tr.request_remarks = tr.request_remarks + "\nTest Request Job #{self.id} will be merge location for #{self.test_case_name}\n"
            tr.save!
            self.save!
            tr_job = self
          end
          continue = false
        end
      rescue ActiveRecord::StaleObjectError => e
        puts e.message
        puts e.backtrace
        self.log_action("Job #{self.status_str}. #{e.message}")
      end
    end
    return tr_job
  end

  def create_merge_job
    mjb = determine_merge_location_job
    if mjb
      mergetool = ConfigParameter.get("maptest_merge_tool")
      trj = TestRequestJob.new
      trj.save!                      # Merge job will initially have status 'Planned' as it has to wait for all moves to finish (checked in post_move)
      trj.project_id = self.project_id
      trj.run_command = "mkdir -p #{mjb.working_dir}/merge && #{mergetool} --input #{mjb.working_dir} --output #{mjb.working_dir}/merge/merged.sq3"
      trj.run_script = "N.A."
      trj.reference_id = self.test_request.id
      trj.working_dir = mjb.working_dir
      trj.test_case_name = "mtmergeresults"
      trj.run_server = mjb.run_server
      trj.log_action("Merge job created with status #{trj.status_str}, waiting for other parallel jobs to finish and moved their results here.")
      trj.save!
    end
  end

  def find_merge_job
    self.test_request.test_request_jobs.where('test_case_name="mtmergeresults"').last
  end

  def duplicate(user=nil)
    TestRequestJob.transaction do
      test_request_job_clone = self.dup
      test_request_job_clone.original_job_id = self.id
      test_request_job_clone.status = STATUS_PLANNED
      test_request_job_clone.run_server = nil
      test_request_job_clone.start_time = nil
      test_request_job_clone.finish_time = nil
      test_request_job_clone.file_system_status = FILE_SYSTEM_STATUS_FREE
      test_request_job_clone.fail_reason = nil
      test_request_job_clone.stdout_log = nil
      test_request_job_clone.scheduler_log = nil
      test_request_job_clone.working_dir = nil
      test_request_job_clone.log_action("Cloned from test request job #{self.id}",user)
      test_request_job_clone.save!

      test_db = self.test_request.first_test_db
      ref_db = self.test_request.first_ref_db
      if test_db
        cd_test = TestRequestJobConversionDatabase.new
        cd_test.test_request_job_id = test_request_job_clone.id
        cd_test.conversion_database_id = test_db.id
        cd_test.relation_type = "test"
        cd_test.save
      else
        raise "Test database not found for test request #{self.id}"
      end

      if ref_db
        cd_ref = TestRequestJobConversionDatabase.new
        cd_ref.test_request_job_id = test_request_job_clone.id
        cd_ref.conversion_database_id = ref_db.id
        cd_ref.relation_type = "ref"
        cd_ref.save
      end


      self.set_status(STATUS_RESCHEDULED)
      self.log_action("Rescheduled with clone to test_request_job #{test_request_job_clone.id}", user)
      self.save!
      return test_request_job_clone
    end
  end

  def regenerate(current_user = 'system', manual_yn = false)
    return unless allow_reschedule?

    TestRequestJob.transaction do
      job_cancel(current_user, rescheduled_yn = true
      )
      # for now the regenerate is just a duplicate
      user = (current_user.class == User ? current_user : nil)
      test_request_job_clone = self.duplicate(user)

      if manual_yn
        test_request_job_clone.set_status(STATUS_MANUAL)
      else
        test_request_job_clone.queue
      end
      test_request_job_clone.save!

      return test_request_job_clone
    end
  end

  def complete
    self.clear_working_dir
    self.set_status(STATUS_COMPLETE)
    self.save!
  end

  def clear_scripts
    cid= File.expand_path("#{ConfigParameter.get("maptest_scripts_dir",self.project_id.to_s)}/maptest_#{id.to_s}")
    FileUtils.rm_f("#{cid}")
    FileUtils.rm_f("#{cid}.done")
    FileUtils.rm_f("#{cid}.exec")
    FileUtils.rm_f("#{cid}.log")
  end

  def terminate(user='system')
    # for a test request job the terminate and cancel methods are equivalent
    cancel(user)
  end

  def cancel(user='system')
    job_cancel(user)
  end

  def reschedule(user_ref = 'system', manual_yn = false)
    return unless allow_reschedule?

    TestRequestJob.transaction do
      job_cancel(user_ref, rescheduled_yn = true)

      user = (user_ref.class == User ? user_ref : nil)
      new_job = duplicate(user)
      if manual_yn
        new_job.set_status(STATUS_MANUAL)
      else
        new_job.queue
      end
      new_job.save!

      return new_job
    end
  end

  def locate_working_dir
    # contrary to the other job classes, test_request_job.working_dir contains the whole path, so just perform a sanity check before returning it
    base_path = File.expand_path("#{ConfigParameter.get("maptest_base_work_dir",self.project_id.to_s)}")
    base_work_dir = File.join(base_path, Rails.env.to_s, self.test_request.id.to_s, self.id.to_s)
    self.working_dir.blank? || !self.working_dir.start_with?(base_work_dir)  ? nil : self.working_dir
  end

  def locate_output_dir
    # test request jobs don't have an output dir
    nil
  end

  def label_test_case_name
    return self.tag_list_on(:context_labeltestcase).join('')
  end

  def label_test_area_name
    ta = self.tag_list_on(:context_labelarea).join('')
    if ta.blank?
      dbs = self.conversion_databases
      r = dbs.first.region if dbs.size > 0
      ta = r.name if r
    end
    return ta.to_s
  end
  
end
