require 'xml/mapping'
require 'rexml/document'
    
class TestSetXml
  include XML::Mapping
  attr_accessor :testjob_id
  
  text_node :testsetname, "testsetname"
  text_node :testdatabase, "testdatabase"
  text_node :referencedatabase, "referencedatabase", :default_value=> nil
  text_node :resultdatabase, "resultdatabase"
  array_node :testcases, "testcase",  :class=>TestCaseXml,:default_value=>[]
  hash_node  :parameters, "parameter", "@id", :class=>ParameterXml,:default_value=>[]

  def to_xml
    xmlelement = self.write_to_xml
    bar = REXML::Formatters::Default.new
    out = String.new
    bar.write(xmlelement, out)
    puts out
  end

  def self.create(testjob)
    ts = TestSetXml.new
    ts.testjob_id = testjob.id



    if testjob.test_set
      ts.testsetname = testjob.test_name
    else
      ts.testsetname = "MAPGRADE TESTCASE"
    end

    if testjob.ref_db
      ts.referencedatabase = testjob.ref_db.to_s.strip
    end

    ts.testdatabase = testjob.test_db

    if testjob.result_db
      ts.resultdatabase = testjob.result_db.to_s.strip
    else
      ts.resultdatabase = "UNKNOWN"
    end



    if testjob.test_case_id
      tc= TestCaseXml.new
      tc.testname = testjob.test_case.name.to_s.strip.chomp.gsub(/^MT_/,"")
      tc.parameters = []
      testjob.testjob_parameters.each do |param|
        px = ParameterXml.new
        px.parametername = param.parametername
        px.parametervalue = param.parametervalue
        tc.parameters << px
      end
      ts.testcases << tc
    else
      #setset

      testjob.test_set.testset_testcases.each do |tcase|
        tc= TestCaseXml.new

        tc.testname = tcase.test_case.name.to_s.strip.chomp.gsub(/^MT_/,"")
        tc.parameters = []

        tcase.testset_test_case_parameters.each do |param|
          px = ParameterXml.new
          px.parametername = param.parameter_name
          px.parametervalue = param.parameter_value
          tc.parameters << px
        end

        ### PROBLEM!!!
        ts.testcases << tc
      end

    end

    return ts
  end

  def self.create_from_test_request_job(test_request_job, test_case_name, test_area, testjob_parameters)
    ts = TestSetXml.new
    ts.testjob_id = test_request_job.id
    if test_area
      ts.testsetname = test_area[0].to_s
    else
      ts.testsetname = "WEBMIS TESTCASE"
    end
    if test_request_job.ref_db
      ts.referencedatabase = test_request_job.ref_db.path_and_file.to_s.strip
    end

    if test_request_job.test_db
       ts.testdatabase = test_request_job.test_db.path_and_file.to_s.strip
    end
    ts.resultdatabase = "UNKNOWN"
  
    tc= TestCaseXml.new
    tc.testname = test_case_name.to_s.strip.chomp.gsub(/^MT_/,"")
    tc.parameters = []
      
    testjob_parameters.each do |param|
      px = ParameterXml.new
      px.parametername = param["parameter_name"].to_s
      px.parametervalue = param["parameter_value"].to_s
      tc.parameters << px
    end

    if test_area
      px = ParameterXml.new
      px.parametername = "testarea"
      px.parametervalue = "#{test_area[1]['x1']}, #{test_area[1]['y1']} ; #{test_area[1]['x2']}, #{test_area[1]['y2']}"
      tc.parameters << px
    end


    ts.testcases << tc
    return ts
  end

  def mock
    ts = TestSetXml.new
    ts.testsetname = "Mock Testset"
    ts.testdatabase = "mock_nds_testdatase"
    ts.resultdatabase = "mock_resultdatase"
  end

  def to_xml
    formatter = REXML::Formatters::Default.new
    out = ""
    formatter.write(self.save_to_xml,out)
    return out
  end

 
  

  def self.run
    TestJob.last.trigger("/tmp","mock.sq3")
  end

end
