class TestToolRelease < ActiveRecord::Base
  scope :active, -> {where('active_yn = 1').order('release_name desc')}
  
  def self.sync_with_filesystem
    testtool_release_dir = ConfigParameter.get('testtool_release_directory') # no project_id dependency

    Dir.chdir(testtool_release_dir) do
      # testtool release directories have the format <name>_yyyymmdd[_n]
      testtool_names = Dir.glob("MapTest_[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]{,_[0-9]}")
      testtool_names.map{|x|x.gsub!("MapTest_","")}
      # set all testtools for which no directory was found in bundledir to inactive
      TestToolRelease.active.each do |testtool_release|
        testtool_release.active_yn = testtool_names.include?(testtool_release.release_name)
        testtool_release.save!
      end

      testtool_names.each do |dirname|
        testtool = TestToolRelease.find_by_release_name(dirname)
        if testtool
          testtool.active_yn = true
        else
          testtool = TestToolRelease.new(:release_name => dirname, :active_yn => true)
        end
        testtool.save!
      end
    end
  end
end
