class TestType < ActiveRecord::Base
  has_many :test_requests
  
  scope :active, -> {where(:removed_yn => false, :active_yn => true).order(:name)}
  
  def after_initialize
    if new_record?
        self.removed_yn = false if self.removed_yn.nil?
        self.active_yn = true if self.active_yn.nil?
    end
  end
end
