class TextBlock < ActiveRecord::Base
  has_many :customer_depots, :foreign_key => :payment_statement_text_block_id
  
  STATUS_ACTIVE    = 100
  STATUS_OBSOLETE  = 999

  @@statusarray = Array.new
  @@statusarray[STATUS_ACTIVE]     = 'Active'
  @@statusarray[STATUS_OBSOLETE]   = 'Removed'

  validates :name,    :presence => true
  validates :content, :presence => true
  validates :status,  :presence => true
  validates :name, :uniqueness => true

  scope :active, -> {where(:status => STATUS_ACTIVE).order(:name)}

  after_initialize :default_values

  def status_string
    return @@statusarray[self.status]
  end

  def change_status(status)
    case status
    when "#{STATUS_ACTIVE}"
      if self.status != status
        self.status = status
      end
    when "#{STATUS_OBSOLETE}"
      if self.status != status && self.remove_allowed?
        self.status = status
      end
    end
    self.save
  end

  def remove_allowed?
    self.status == STATUS_ACTIVE
  end

  private
  def default_values
    self.status ||= STATUS_ACTIVE
  end
end
