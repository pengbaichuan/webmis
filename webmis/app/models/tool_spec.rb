class ToolSpec
  attr_accessor :toolspechash
  attr_accessor :content
  attr_accessor :xml

  def initialize(xml)
    @xml = Nokogiri::XML(xml)
  end

  def xml
    @xml
  end

  def self.synchronize_tool(tool_name, tool_release, contents, mail_exception = true)
    begin
      # perform some sanity checks on the parameters
      if tool_name.blank? || tool_release.blank?
        raise "Invalid tool information (tool_name='#{tool_name}, release='#{release}'"
      end
      raise "Contents is nil" unless contents

      # read the contents JSON, an invalid JSON will raise an exception
      parsed_contents = ToolSpec.new(contents).tools
      raise "No executable specified in the contents" unless parsed_contents["tools"] && parsed_contents["tools"].size > 0

      release_name = tool_name + "_" + tool_release
      parsed_contents["tools"].each do |executable_contents|
        executable = Executable.find_by_name(executable_contents['name'])
        executable.synchronize_with_toolspec(nil, nil, tool_name, tool_release, release_name, executable_contents, contents)
      end
    rescue => e
      puts e.message
      puts e.backtrace
      # sends an email if required, use to handle exception raised by cron jobs
      Postoffice.tool_spec_synchronization_error(e) if mail_exception
    end
  end

  def self.synchronize_bundle(bundles, bundle, release, contents, mail_exception = true)
    begin
      # perform some sanity checks on the parameters
      if bundles.blank? || bundle.blank? || release.blank?
        raise "Invalid bundle information (bundles='#{bundles}', bundle='#{bundle}', release='#{release}')"
      end
      raise "Contents is nil" unless contents

      # read the contents JSON, an invalid JSON will raise an exception
      parsed_contents = ToolSpec.new(contents).tools
      raise "No excecutable specified in the contents" unless parsed_contents['tools'] && parsed_contents['tools'].size > 0

      # synchronize all found executables
      parsed_contents["tools"].each do |executable_contents|
        # Skip empty or missing executable names
        next if executable_contents['name'].blank?

        executable_name = executable_contents['name']
        tool_release = executable_contents['release']
        tool_release = ToolSpec.get_tool_release_from_bundle_release_and_tool(bundles, bundle, release, executable_name) if tool_release
        if bundles.blank?
          release_name = bundle + "_" + release
          bundles_release = bundle + "_" + release
        else
          release_name = bundles + "_" + release + "/" + bundle
          bundles_release = bundles + "_" + release
        end

        ct = Conversiontool.find_by_code(bundles_release)
        if ct.nil?
          ct = Conversiontool.new(:code => bundles_release, :description => bundles_release, :isactive => false, :prod_released_yn => false, :remarks => "added by toolspec importer")
          ct.save!
          ct.reload
        end

        cvtool_bundle = CvtoolBundle.where("`name`='#{bundle}'").first
        if cvtool_bundle.nil?
          cvtool_bundle = CvtoolBundle.new(:name => bundle, :remarks => "added by toolspec importer", :active_yn => false)
          cvtool_bundle.save!
          cvtool_bundle.reload
        end

        executable = Executable.find_by_name(executable_name)
        if executable.nil?
          executable = Executable.new(:status => 'new', :name => executable_name, :description => '', :input => "[]", :output => "[]", :parameters => '{"parameters":[]}')
          executable.save!
          executable.reload
        end

        executable.synchronize_with_toolspec(cvtool_bundle, ct, nil, tool_release, release_name, executable_contents, contents)
      end


    rescue => e
      puts e.message
      puts e.backtrace
      # sends an email if required, use to handle exception raised by cron jobs
      Postoffice.tool_spec_synchronization_error(e) if mail_exception
    end
  end


  def handleParameters(node, featuregroup, parameterHash, configurationHash, toolname, appendix, writer)
    # TODO Rewrite the whole method
    # Don't know what is needed for what anymore. WebMIS itself just needs the parameterHash, all parameters hidden in parameterRefs, featuregroups
    # and featuregroupRefs are put in this hash. The featuregroups are stored for now but as they are not used, treat them with caution.
    refparameters = []
    ref_featuregroups = []
    node.children.each do |paramorfeat|
      # Handle Featuregroup
      if (paramorfeat.name == 'featuregroup' || paramorfeat.name == 'featuregroupRef')
        p = paramorfeat.name == 'featuregroup' ? paramorfeat : @xml.root.at_xpath("//xmlns:featuregroup[@name='#{paramorfeat['name']}']")
        # find description
        featgroupdescription = nil
        parametersNode = nil
        p.children.each do |paramorfeatchild|
          if (paramorfeatchild.name == "description")
            # Remove div tags from description
            featgroupdescription = paramorfeatchild.inner_html
            featgroupdescription.gsub!("<div xmlns=\"http://www.w3.org/1999/xhtml\">", "")
            featgroupdescription.gsub!("</div>", "")
            # strip leading and trailing whitespace
            featgroupdescription.strip!
          elsif (paramorfeatchild.name == "parameters")
            parametersNode = paramorfeatchild
          end
        end
        # Create featuregroup object
        # for now flatten hierarchy - no featuregroups
        newfeaturegroup = Featuregroup.new("#{p["name"]}", featgroupdescription)
        featuregroup.addFeaturegroup(newfeaturegroup)
        handleParameters(parametersNode, newfeaturegroup, parameterHash, configurationHash, toolname, appendix, writer)
        # Handle Parameter
      elsif (paramorfeat.name == "parameter" || paramorfeat.name == 'parameterRef')
        p = paramorfeat.name == 'parameter' ? paramorfeat : @xml.root.at_xpath("//xmlns:parameter[@name='#{paramorfeat['name']}']")
        # Skip when tool is not in parameterHash
        if (!parameterHash.key?(toolname))
          parameterHash[toolname] = {}
        end
        #Check if parameter is used
        #if (parameterHash[toolname].key?(paramorfeat["name"].to_s))
        # extract value
        value = "N/A"
        # Create a hash with the description of all values
        valueHash = Hash::new
        p.search("value").each do |posValue|
          valueHash[posValue["value"].to_s] = posValue["description"]
        end
        # extract description
        description = nil
        p.search("description").each do |descriptionNode|
          # Remove div tags from description
          description = descriptionNode.inner_html
          description.gsub!("<div xmlns=\"http://www.w3.org/1999/xhtml\">", "")
          description.gsub!("</div>", "")
          # strip leading and trailing whitespace
          description.strip!
        end
        # Create parameter object
        newparameter = Parameter.new(p["name"],
          value,
          valueHash[value],
          description,
          p["datatype"],
          p["default"])
          
        featuregroup.addParameter(newparameter)
        #end
      elsif (paramorfeat.name == 'featuregroupRef')
        ref_featuregroups << [paramorfeat['name']]
      end
    end

    refparameters.each do |ref_parameter|
      #if !Executable.find_by_parameter(ref_parameter[0]).collect{|e|e.name}.include?(toolname)
        newparameter = Parameter.new(ref_parameter[0],"@ref",Hash::new,'@ref',nil,ref_parameter[1])
        featuregroup.addParameter(newparameter)
      #end
    end

    ref_featuregroups.each do |ref_featuregroup|
      newfeaturegroup = Featuregroup.new(ref_featuregroup[0], '@ref')
      featuregroup.addFeaturegroup(newfeaturegroup)
    end
  end

  def build_content(parameterHash={}, configurationHash={}, toolname="NDH2NDS", appendix={}, writer=nil)
    # parse xml
    xmlroot = @xml.root
    # Initialize nesting depth
    Featuregroup.initDepth()

    # Fill a datastructure for delayed writing
    # This way we can sort the parameters alphabetically
    # and leave out empty feature groups
    content = Featuregroup.new(nil, nil)

    # Handle parameters/tools
    xmlroot.children.each do |configchild|
      if (configchild.name == "parameters")
        # Handle general parameters
        generalParameters = Featuregroup.new("General parameters", nil)
        content.addFeaturegroup(generalParameters)
        handleParameters(configchild, generalParameters, parameterHash, configurationHash, toolname, appendix, writer)
      elsif (configchild.name == 'featuregroup')
        description = configchild.children.select{|x| x.name == 'description'}.first.content.strip rescue ''
        parameters = configchild.children.select{ |x| x.name == 'parameters'}.first
        childFeaturegroup = Featuregroup.new(configchild['name'], description)
        content.addFeaturegroup(childFeaturegroup)
        handleParameters(parameters, childFeaturegroup, parameterHash, configurationHash, toolname, appendix, writer)
      elsif (configchild.name == "tool")
        # find tool description, input, output and parameters
        tooldescription = nil
        toolinput = {}
        tooloutput = {}
        toolparameters = nil
        toolname = configchild["name"]
        isimprovementprocess = false
        if configchild["type"] == "improvement"
          isimprovementprocess = true
        end
        configchild.children.each do |toolchild|
          if (toolchild.name == "description")
            tooldescription = toolchild.inner_html
            tooldescription.gsub!("<div xmlns=\"http://www.w3.org/1999/xhtml\">", "")
            tooldescription.gsub!("</div>", "")
            # strip leading and trailing whitespace
            tooldescription.strip!
          elsif (toolchild.name == "input")
            inputs =[]
            toolchild.children.search("format").each do |format|
              inputhash = {"datatype" =>  format.attributes["tag"].text}
              add_id(inputhash)
              inputs << inputhash
            end
            toolinput = inputs
          elsif (toolchild.name == "output")
            outputs = []
            toolchild.children.search("format").each do |format|
              outputhash = {"datatype" =>  format.attributes["tag"].text}
              add_id(outputhash)
              outputs << outputhash
            end
            tooloutput = outputs
          elsif (toolchild.name == "parameters")
            toolparameters = toolchild
          end

        end
        # Create tool as featuregroup object
        tool = Featuregroup.new(toolname, tooldescription)
        tool.setTool(toolinput, tooloutput)
        tool.setImprovProc if isimprovementprocess
        content.addFeaturegroup(tool)
        if (toolparameters)
          handleParameters(toolparameters, tool, parameterHash, configurationHash, toolname, appendix, writer)
        end
      end
    end
    
    # Remove empty entries
    #content.removeEmptyObjects
    # Sort the parameters
    #content.sort
    # print contents to pdf
    
    @content = content
  end


  def tools
    build_content
    tools = []
    @content.children.each do |tool|
      tools << map_tool(tool)
    end

    return {"tools" => tools}
  end

  def map_parameter(parameter)
    mapped_parameter = {"name" => parameter.name,
      "description" => parameter.description,
      "datatype" => parameter.datatype,
      "default_value" => parameter.default,
      "mandatory" => parameter.mandatory
    }

    return mapped_parameter
  end
  

  def map_tool(tool)
    mapped_tool = {}
    mapped_tool["description"] = tool.description
    mapped_tool["name"] = tool.name
    mapped_tool["input"] = tool.input
    mapped_tool["output"] = tool.output
    mapped_tool["is_improvement_process"] = tool.is_improvement_process

    all_mapped_parameters = []
    mapped_parameters = []
    featuregroups = []

    tool.parameters.each do |parameter|
      mapped_parameters << map_parameter(parameter)
    end
    # create global feature group
    featuregroup = {}
    featuregroup["name"] = "NONE"
    featuregroup["description"] = "NONE"
    featuregroup["parameters"] = mapped_parameters
    featuregroups << featuregroup

    all_mapped_parameters = all_mapped_parameters + mapped_parameters
    
    tool.children.each do |child|
      mapped_parameters = []
      child.parameters.each do |parameter|
        mapped_parameters << map_parameter(parameter)
      end
      featuregroup = {}
      featuregroup["name"] = child.name
      featuregroup["description"] = child.description
      featuregroup["parameters"] = mapped_parameters
      featuregroups << featuregroup
      all_mapped_parameters = all_mapped_parameters + mapped_parameters
    end
    mapped_tool["parameters"] = all_mapped_parameters
    mapped_tool["featuregroups"] = featuregroups
    return mapped_tool
  end

  def add_id(typehash)
    if typehash["datatype"].downcase == "dhive"
      typehash["identification"] = ".dh.sq3"
    end
    if typehash["datatype"].downcase == "nds"
      typehash["identification"] = "ROOT.NDS"
    end
    if typehash["datatype"].downcase == "rdo"
      typehash["identification"] = ".rdo.sq3"
    end

    if typehash["datatype"].downcase == "rdb"
      typehash["identification"] = ".db"
    end
  end
  
:private
  def self.get_tool_release_from_bundle_release_and_tool(bundles,bundle,bundle_release, toolname)
    release_dir = ConfigParameter.get('cvtool_release_directory')
    bundle_dir = 'Bundles'
    bundle_release_dir = "#{bundles}_#{bundle_release}"
    full_path = File.join(release_dir,bundle_dir,bundle_release_dir,bundle,'*','bin',toolname)
    globs = Dir.glob(full_path)

    symlink = globs[0]
    target = File.readlink(symlink.to_s) if symlink
    if target
      tr_arr = target.scan(/(\d*(_\d|))\/x86/)
      if !tr_arr.empty?
        return tr_arr[0][0]
      end
    end
  end
 
end
