class ToolspecFeaturegroup < ActiveRecord::Base

  has_many :toolspec_parameters
  belongs_to :conversiontool
  belongs_to :cvtool_bundle


  def self.create_or_sync(featuregroup_contents, executable_id, conversiontool_id, cvtool_bundle_id)
    ToolspecFeaturegroup.transaction do
      featuregroup = ToolspecFeaturegroup.find_or_create_by(name: featuregroup_contents[:name], executable_id: executable_id, conversiontool_id: conversiontool_id, cvtool_bundle_id: cvtool_bundle_id)
      featuregroup.description = featuregroup_contents[:description]
      featuregroup.toolspec_parameters.destroy_all
      if featuregroup_contents[:parameters]
        featuregroup_contents[:parameters].each do |param|
          ts_param = ToolspecParameter.new
          ts_param.name = param[:name]
          ts_param.description = param[:description]
          ts_param.datatype = param[:datatype]
          ts_param.default = param[:default_value]
          ts_param.mandatory = param[:mandatory]
          featuregroup.toolspec_parameters << ts_param
          #TODO: parameter values
        end
      end
      featuregroup.save!
    end
  end

end

