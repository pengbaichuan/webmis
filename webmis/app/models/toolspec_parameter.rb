class ToolspecParameter < ActiveRecord::Base

  belongs_to :toolspec_featuregroup
  has_many :toolspec_parameter_values
end
