class UpdateRegion < Region
validates :name, :presence => true
validates :parent_id, :presence => { :message => 'Continent is mandatory.' }
scope :active, -> { where(:isactive => true).order(:name) }
end
