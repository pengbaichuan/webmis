class UsedProductDesign < ActiveRecord::Base
  
  STATUS_ACTIVE   =  10
  STATUS_OBSOLETE = 100

  @@statusarray = Array.new
  @@statusarray[STATUS_ACTIVE]   = 'active'
  @@statusarray[STATUS_OBSOLETE] = 'obsolete'

  belongs_to :product_design, :foreign_key => :used_product_design_id
  belongs_to :source_product_design, :foreign_key => :product_design_id, :class_name => "ProductDesign"

  scope :active,  -> {includes(:product_design).where("used_product_designs.status = #{STATUS_ACTIVE}")}
  scope :obsolete,-> {includes(:product_design).where("used_product_designs.status = #{STATUS_OBSOLETE}")}
  
end
