require 'socket'

class User < ActiveRecord::Base
    
  has_many :datasource_internalcontacts
  has_many :datasources, :through => :datasource_internalcontacts
  has_many :roles_users
  has_many :roles, :through => :roles_users
  has_many :callsessions
  has_many :test_requests
  has_many :jobs
  has_many :conversions
  has_many :high_level_boms
  has_many :articles
  has_many :dashboard_grids
  has_many :purchase_orders

  has_many :user_mails
  has_many :mail_events, :through => :user_mails
  has_many :mail_conditions, :through => :user_mail_conditions
  has_many :product_line_users
  has_many :product_lines, :through => :product_line_users

  has_many :project_users
  has_many :projects, :through => :project_users
  
  has_one :user_profile
  attr_accessor :current_project_id
  acts_as_tagger
  
  scope :active, -> {where("job_title not like '%Exit%' and job_title not like '%Functional %'").order(:first_name)}
  
  validates :email,:presence => true
  
  def has_role(role)
    if retreive_current_project_id
      #self.roles.includes('roles_users').count(:where => ["roles_users.project_id = #{retreive_current_project_id} and roles.title = ? and roles_users.module = '#{ENV['module']}'", role]) > 0
      self.roles.includes('roles_users').where(["roles_users.project_id = #{retreive_current_project_id} and roles.title = ? and roles_users.module = '#{ENV['module']}'", role]).count > 0
    else
      return false
    end
  end

  def make_call phone_number
    raise "Unknown phone extension" if phone_ext.blank?
    sock = TCPSocket::new("msvoip01", 5038)
    sock.send("ACTION: LOGIN\r\nUSERNAME: webapp\r\nSECRET: mamalou\r\n\r\n",0)
    sock.send("ACTION: Originate\r\n",0)
    sock.send("Channel: SIP/#{phone_ext}\r\n",0)
    sock.send("Exten: #{phone_number}\r\n",0)
    sock.send("Priority: 1\r\n",0)
    sock.send("CallerID: #{phone_ext}\r\n",0)
    sock.send("Context: from-internal\r\n",0)
    sock.send("\r\n",0)
    sock.close
  end

  def user_call(user_obj)
    if self.phone_ext.blank?
      raise "The users phone extension is unknown"
    elsif user_obj.phone_ext.blank?
      raise "Your phone extension is unknown"
    else
      make_call(user_obj.phone_ext.strip)
    end
  end

  def get_api_key
   key = ApiKey.find_by_user_id(self.id)
   return key if key
   key = ApiKey.new
   key.user_id = self.id
   key.owner = self.user_name
   key.save
   return key
 end

  def docrolesallowed
    result = ['Others']
    dr = docrole
    result << dr

    if dr == "Managing Partners"
      result << 'HRM'
      result << 'Sales'
      result << 'Management'
    end

    if dr == "Management"
      result << 'Sales'
    end
    return result
 end
 
  def docrole
     role = "Others"
     roles = self.roles_users
      roles.each do |i|
        role = i.role.title if i.module == "DOCGEN"
      end
     return role
  end

  def User.current
    # This does not refer to the session because the application has set
    # this from the session in user_setup.
   Thread.current[:user]
  end

  # Set the user for the current request. It is guaranteed that this is
  # set for each request in the before_filter for the application.
  #
  # This function uses the Ruby Thread class to do thread-local storage,
  # which will be overkill if the Rails server implementation isn't also
  # using Ruby threads, but works everywhere.
  #
  # User.current(), User.current=(), and UserSupport#user_setup encapsulate
  # session storage of user information. Only these three functions should
  # know whether we store the entire User object in the session or only
  # User#id.
  #
  def User.current=(u)
    Thread.current[:user] = u
    session = Thread.current[:session]
    if session.nil?
      message = "Session not found"
      raise RuntimeError.new(message)
    end

    # Don't cause a session store unnecessarily
    if session[:user] != u
      session[:user] = u
    end
  end
  
  def full_name
    rs = self.first_name.to_s + " " + self.last_name.to_s
    return rs
  end
  
  def has_role?(rolename ,modul='MIS')
    if retreive_current_project_id

      rolename.to_a.each do |role|
        if has_role(role)
          return true
        else
          next
        end
      end
      return false
    else
      return nil
    end
  end

  def unused_roles(project_id)
    used = RolesUser.where(:user_id => self.id, :module => "MIS",:project_id => project_id).collect{|ru|ru.role_id}
    return Role.order(:title).all.collect{|r|r if !used.include?(r.id)}.compact
  end

  def unused_mail_events
    used = self.mail_events.collect{|me|me.id}
    return MailEvent.order(:name).all.collect{|me|me if !used.include?(me.id)}.compact
  end


  def self.find_by_full_name(full_name)
    match = nil
    if !full_name.blank?
      User.where(first_name:full_name.split(' ')[0].strip).each do |u|
        full_found = "#{u.first_name} #{u.last_name}"
        if full_found.downcase == full_name.downcase
          match = u
        end
      end
    end
    if match
      return match
    else
      return User.new
    end
  end

  def self.sync_ldap_users
    message = ""
    host = 'admin.mapscape.nl'
    port = 636
    conn = LDAP::SSLConn.new(host, port)
    conn.set_option( LDAP::LDAP_OPT_PROTOCOL_VERSION, 3 )
    filter = "Objectclass=*"
    a = conn.search("ou=people,o=Mapscape_BV,dc=mapscape,dc=nl",1,filter){|entry|
      check = User.where("first_name = '"+entry['givenName'].join.to_s.strip.chomp+"' and last_name = '"+entry['sn'].join.to_s.strip.chomp+"'")
      if check.length == 0
        User.transaction do
        user = User.new
        user.first_name = entry['givenName'].join.to_s.strip.chomp
        user.last_name = entry['sn'].join.to_s.strip.chomp
        user.user_name = entry['uid'].join.to_s.strip.chomp
        user.email = user.user_name + "@mapscape.eu"
        user.job_title = ''
        if user.save
            default_role = Role.find_by_title(ConfigParameter.get('default_role_name')) # no project_id dependency
          role = RolesUser.new
          role.user_id = user.id
          role.role_id = default_role.id if default_role
          role.module = "MIS"
          role.save
            profile = UserProfile.new({:user_id => user.id,:last_used_project_id => Project.default.id})
            profile.save
            message = message + user.user_name.to_s + " Added.\n"
          end
        end        
      end
    }
    return message
  end

  def roles_by_project(project_id)
    role_ids = self.roles_users.where(:project_id => project_id).collect{|ru|ru.role_id}
    if role_ids.empty?
      return role_ids
    else
      return Role.find(role_ids)
    end
  end

  def retreive_current_project_id
    if self.current_project_id || ( self.user_profile && !self.user_profile.last_used_project_id.blank? )

      if self.current_project_id
        cprj_id = self.current_project_id
      else
        puts "NOTICE => project_id retrieved from user_profile instead of session!"
        cprj_id = self.user_profile.last_used_project_id
      end
      return cprj_id
    else
      return nil
    end
  end

  def active?
    User.active.collect{|u|u.id}.include?(self.id)
  end

end
