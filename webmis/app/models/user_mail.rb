class UserMail < ActiveRecord::Base

  belongs_to :user
  belongs_to :mail_event
  has_many :user_mail_conditions, :foreign_key => 'users_mail_id'
  has_many :mail_conditions, :through => :user_mail_conditions

end
