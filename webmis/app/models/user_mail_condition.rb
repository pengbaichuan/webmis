class UserMailCondition < ActiveRecord::Base
  belongs_to :mail_condition, :foreign_key => 'mail_conditions_id'
  belongs_to :user_mails
  belongs_to :users
  validates :users_mail_id, :presence => true
  validates :mail_conditions_id, :presence => true
end
