class UserProfile < ActiveRecord::Base
  belongs_to :user
  belongs_to :last_used_project, :class_name => "Project", :foreign_key => :last_used_project_id
end
