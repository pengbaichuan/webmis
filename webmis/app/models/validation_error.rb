class ValidationError < ActiveRecord::Base
  belongs_to :conversion_database
  has_one :conversion, :through => :conversion_database

  scope :unhandled, -> {where(:resolved_yn => false, :ticket_nr => nil)}
  scope :unresolved,-> {where(:resolved_yn => false) }

  def unhandled?
    return !self.resolved_yn && self.ticket_nr.nil?
  end

  def close_validation_error(error_ok, user)
    self.resolved_yn = true
    self.save!
    db = self.conversion_database

    if error_ok
      db.handle_validation_error_accepted(self.name, user)
    else
      db.handle_validation_error_rejected(self.name, user)
    end

    # return the conversion_job
    return db.conversion_job
  end
end
