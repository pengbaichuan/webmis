class ValidationJob < Job
  acts_as_ordered_taggable_on :tags
  belongs_to :conversion_database, :foreign_key => :reference_id
  has_one :conversion_job, :through => :conversion_database
  has_one :conversion, :through => :conversion_database
  has_one :production_orderline, :through => :conversion
  has_one :production_order, :through => :production_orderline
  belongs_to :fail_category_logged_by_user, :class_name => "User",:foreign_key => "fail_category_logged_by_user_id"

  comma :tagged_failed_jobs_report do
    type 'Job type'
    fail_category 'Category'
    fail_category_logged_by_user :full_name => 'Tagged by'
    fail_category_logged_on 'Date tagged'
    bug_tracker_id
    fail_reason
    start_time
    finish_time
    conversion :id => 'Conversion'
    run_server
    action_log
    id
    conversion_job "Coverage" do |conversion_job|conversion_job.coverage_label end
    conversion_database :filename => "Database"
    conversion_job "Data type" do |conversion_job|conversion_job.conversion.output_format end
    conversion "Conversion tool" do |conversion|conversion.conversiontool.code end
    conversion "Conversion bundle" do |conversion|conversion.cvtool_bundle.name end
    conversion "Data release" do |conversion|conversion.data_release.name if conversion.data_release end
    conversion "Supplier" do |conversion|conversion.data_release.company_abbr.company_name if conversion.data_release end
    production_order :id => "Production order id"
    production_order :name => "Production order name"
    conversion "Product ID" do |conversion|conversion.product.volumeid if conversion.product end
    conversion "Product Name" do |conversion|conversion.product.name if conversion.product end
  end

  def duplicate(user=nil)
    ValidationJob.transaction do 
      validation_job_clone = self.dup
      validation_job_clone.original_job_id = self.id
      tag_list_str = self.conversion_database.filename.split(".")[0]

      validation_job_clone.set_tag_list_on(:context_coverage, self.tag_list_on(:context_coverage)) if !self.tag_list_on(:context_coverage).empty?
      validation_job_clone.set_tag_list_on(:context_subregion, self.tag_list_on(:context_subregion)) if !self.tag_list_on(:context_subregion).empty?
      validation_job_clone.set_tag_list_on(:context_subregion, self.tag_list_on(:context_subregion)) if !self.tag_list_on(:context_subregion).empty?
      validation_job_clone.set_tag_list_on(:context_labelsupplier, self.tag_list_on(:context_labelsupplier)) if !self.tag_list_on(:context_labelsupplier).empty?
      validation_job_clone.set_tag_list_on(:context_labelrelease, self.tag_list_on(:context_labelrelease)) if !self.tag_list_on(:context_labelrelease).empty?
      
      validation_job_clone.status = STATUS_PLANNED
      validation_job_clone.run_server = nil
      validation_job_clone.start_time = nil
      validation_job_clone.finish_time = nil
      validation_job_clone.file_system_status = FILE_SYSTEM_STATUS_FREE
      validation_job_clone.fail_reason = nil
      validation_job_clone.stdout_log = nil
      validation_job_clone.scheduler_log = nil
      validation_job_clone.working_dir = "w_################-" + tag_list_str
      validation_job_clone.log_action("Cloned from validation job #{self.id}",user)
      validation_job_clone.save!
      
      validation_job_clone.reload
      return validation_job_clone
    end
  end

  def regenerate(current_user, conversiontool_id, cvtool_bundle_id, manual_yn = false)
    return unless allow_reschedule?

    ValidationJob.transaction do
      job_cancel(current_user, rescheduled_yn = true)

      user = (current_user.class == User ? current_user : nil)
      validation_job_clone = self.duplicate(user)
      if validation_job_clone.job_script
        validation_job_clone.job_script.updated_by = user ? user.full_name : current_user
        validation_job_clone.job_script.cvtoolbundle_name = CvtoolBundle.find(cvtool_bunde_id).name
        validation_job_clone.job_script.conversiontool_code = Conversiontool.find(conversiontool_id).code
      else
        # script not generated, check whether the tool bundle was changed.
        unless self.conversion && self.conversion.cvtool_bundle_id == cvtool_bundle_id && self.conversion.convertool_id == conversiontool_id
          # chosen tool bundle differs from the conversion one, as we can't generate the script
          # just put the job on manual for now and leave it to the user to adapt the script.
          manual_yn = true
          validation_job_clone.log_action("Tool bundle changed, job must be edited manually before proceeding", user)
        end
      end

      if manual_yn
        validation_job_clone.set_status(STATUS_MANUAL)
      else
        validation_job_clone.queue
      end
      validation_job_clone.save!

      return validation_job_clone
    end
  end

  def reschedule(user_ref = 'system', manual_yn = false)
    return unless allow_reschedule?

    ValidationJob.transaction do
      job_cancel(user_ref, rescheduled_yn = true)

      user = (user_ref.class == User ? user_ref : nil)
      new_job = duplicate(user)
      if manual_yn
        new_job.set_status(STATUS_MANUAL)
      else
        new_job.queue
      end
      new_job.save!

      return new_job
    end
  end

  def simulate_dakota?
    conversion.production_orderline.production_order.use_dakota_simulator_yn
  end

  def dakota_simulator_path
     File.expand_path("#{ConfigParameter.get("dakota_simulator_dir",self.project_id.to_s)}/daksim")
  end

  def perform
    validate
  end

  def validate
    begin
      # puts "Executing validation job perform for  " + self.id.to_s + " lock version : " + self.lock_version.to_s
      return unless allow_status_change?(STATUS_PROCESSING) && self.file_system_status == FILE_SYSTEM_STATUS_FREE
      clear_scripts
      cmd = simulate_dakota? ? dakota_simulator_path : 'daklite'
      daklite_opts = ""
      vid = File.expand_path("#{ConfigParameter.get("dakota_scripts_dir",self.project_id.to_s)}/validation_#{id.to_s}")
      self.start_time = DateTime.now
      self.finish_time = nil
      self.run_command = "#{cmd} -f #{vid} #{daklite_opts}".strip
      self.stdout_log = ""
      self.fail_reason = ""
      self.exitcode = nil
      save!

      working_dir_prefix = Time.now.strftime("%y%m%d-%H%M%S") + "-#{self.id}"
      if self.job_script
        self.job_script.working_dir_prefix = working_dir_prefix
        running_script = self.job_script.to_dakota("VALIDATION_JOB_ID", self.id)
        self.working_dir = "w_#{working_dir_prefix}-#{self.job_script.tag_list_string}"
      else
        # legacy method of script handling, kept in place for now so the old jobs don't break
        running_script = "mapinfo VALIDATION_JOB_ID \"#{self.id}\""+ "\n" + self.run_script
        running_script = running_script.gsub(/\r\n?/, "\n") # Remove carriage returns
        self.working_dir = self.working_dir.gsub(/^w_#*-/,"w_" + working_dir_prefix + "-")
      end
      File.open(vid, 'w') {|f| f.write(running_script.to_s.gsub("--->","")) }

      s = "#! /bin/bash\n"
      s = s + "echo PID: $$ > #{vid}.log || exit\n"
      s = s + "export PROCESSID=$$\n"
      s = s + "#{self.run_command} >> #{vid}.log\n"
      s = s + "echo $? >  #{vid.to_s}.done\n"
      File.open(vid + ".exec", 'w') {|f| f.write(s) }
      execstring = "source /data/users/#{ENV['prod_user']}/.profile;nohup sh #{vid}.exec > #{vid.to_s}.log 2>&1 &"
      ssh(execstring)
      # get pid
      pidexec = "cat  #{vid}.log | grep '^PID' | cut -d' ' -f2"
      pid = ssh(pidexec).chomp
        
      begin
        self.run_pid = pid
        self.set_status(STATUS_PROCESSING)
        self.set_file_system_status(FILE_SYSTEM_STATUS_ON_SERVER)
        self.log_action("#{self.status_str} validation job.")
        save!
      rescue ActiveRecord::StaleObjectError => e
        # kill needed
        kill_linux_process(pid)
        clear_working_dir
        clear_scripts
        puts "Failed setting status to PROCESSING for job " + self.id.to_s + " .. killing stated linux process " + pid.to_s + " on " + self.run_server.to_s
        puts e.message
        puts e.backtrace
      end
    rescue Exception => e
      puts e.message
      puts e.backtrace
      self.stdout_log = stdout_log + "------------\r\n" + e.message + "\r\n======\r\n" + e.backtrace.join("\r\n")
      self.fail_reason = e.message + "\r\n======\r\n" + e.backtrace.join("\r\n")
      clear_scripts
      self.set_status(STATUS_FAILED)
      self.set_file_system_status(FILE_SYSTEM_STATUS_FREE)
      self.log_action("Validation job #{self.status_str}. #{e.message}")
    ensure
      save!
    end
  end

  def automonitor
    return unless status == STATUS_PROCESSING  # only monitor running validation jobs
    # we currently just check whether the .done file exists
    vid = File.expand_path("#{ConfigParameter.get("dakota_scripts_dir",self.project_id.to_s)}/validation_#{id.to_s}")
    # puts "Checking Validation Job " + self.id.to_s +  " at " + "#{vid.to_s}"

    return unless File.file?("#{vid.to_s}.done") # validation process is still running
    begin
      cd = self.conversion_database
      contents = File.read("#{vid.to_s}.done")
      standardoutlog = File.read("#{vid.to_s}.log")
      taillog = ""
      if standardoutlog.size > 10000
        taillog = standardoutlog[standardoutlog.size-10000..-1]
      else
        taillog = standardoutlog
      end

      self.stdout_log = taillog
      m = standardoutlog.match(/The new working dir is: (.*)/)
      if contents.to_i == 0
        # validation process finished normally, put result with the validated conversion database
        source_path = m[1].gsub(/\/w_/, "/o_") if m
        output_dir = self.conversion_database.path
        crashed = false
      elsif self.working_dir.blank?
        crashed = true
        source_path = nil
      else
        crashed = true
        source_path = m[1] if m && m[1]
      end

      if cd.remarks
        cd.remarks += "\n"
      else
        cd.remarks = ""
      end
      logfile = source_path ? ssh("ls #{File.join(source_path, 'validate*.log')} 2</dev/null").strip : ""
      if crashed || logfile.blank?
        self.set_status(STATUS_FAILED)
        self.log_action("#{self.status_str} validation job. No validate logfile found.")
      else
        # move the validation log to the conversion database directory
        cd.remarks += move(logfile, output_dir)
        self.log_action("Moving #{logfile} to #{output_dir}.")
        # move script files
        self.stored_scriptfiles.each do |script|
          move(script, output_dir)
        end

        # try to determine validation results
        lines = IO.readlines(File.join(output_dir, File.basename(logfile)))
        cd.remarks += "\n" + lines.grep(/^(ERROR|WARNING|INFO):/).join("\n")
        error_count = fill_validation_errors(lines)
        warning_count = lines.grep(/^WARNING:/).size

        ValidationJob.transaction do
          # first set job status to complete as its new status is used to determine the allowed statuses of its conversion database
          self.finish_time = DateTime.now
          self.set_status(STATUS_COMPLETE) if self.status == STATUS_PROCESSING
          self.save!

          if error_count > 0
            cd.set_status(ConversionDatabase::STATUS_HAS_ERRORS)
            cd.remarks += "\nThe validation returned #{error_count} #{'error'.pluralize(error_count)}."
          elsif warning_count > 0
            # Currently warnings are being ignored, hence the use of STATUS_ACCEPTED instead of STATUS_HAS_WARNINGS
            # cd.status = ConversionDatabase::STATUS_HAS_WARNINGS
            cd.set_status(ConversionDatabase::STATUS_ACCEPTED)
            cd.remarks += "\nThe validation succeeded with #{warning_count} #{'warning'.pluralize(warning_count)}."
          else
            cd.set_status(ConversionDatabase::STATUS_ACCEPTED)
            cd.remarks += "\nThe validation was successful."
          end
          cd.save!

          if cd.status == ConversionDatabase::STATUS_REJECTED || cd.status == ConversionDatabase::STATUS_HAS_ERRORS
            Postoffice.conversion_database_validation_failed(self.id).deliver
          end
          clear_scripts
          clear_working_dir
        end
      end

    rescue Exception => e
      puts e.message
      puts e.backtrace
      self.stdout_log = stdout_log + "------------\r\n" + e.message + "\r\n======\r\n" + e.backtrace.join("\r\n")
      self.fail_reason = e.message + "\r\n======\r\n" + e.backtrace.join("\r\n")
      self.set_status(STATUS_FAILED)
      self.log_action("Validation job #{self.status_str}. #{e.message}")
      self.finish_time = DateTime.now
      clear_scripts
    ensure
      save!
    end
  end

  def fill_validation_errors(lines)
    validation_errors_count = 0
    lines.grep(/^ERROR:/).uniq.each do |line|
      # take the part after ':' and remove any leading and trailing whitespace
      error_line = line.partition(':')[2].strip
      if error_line.match(/^\d+\s+/)
        # the error line is of the form '<count> <errorstring>'
        error_line_parts = error_line.partition(/\s+/)
        error_count = error_line_parts[0]
        error_string = error_line_parts[2]
      else
        # unknown format of the error line, just take the whole error line
        error_count = nil
        error_string = error_line
      end

      # create the validation error for this conversion if it does not exist yet.
      validation_error = ValidationError.new(:name => error_string, :count => error_count, :copied_to_crash_yn => false, :resolved_yn => false)
      self.conversion_database.validation_errors << validation_error
      validation_error.save!
      validation_errors_count += 1
    end
    return validation_errors_count
  end

  def clear_scripts
    vid = File.expand_path("#{ConfigParameter.get("dakota_scripts_dir",self.project_id.to_s)}/validation_#{id.to_s}")
    FileUtils.rm_f("#{vid}")
    FileUtils.rm_f("#{vid}.done")
    FileUtils.rm_f("#{vid}.exec")
    FileUtils.rm_f("#{vid}.log")
  end

  def terminate(user='system')
    # for a validation job the terminate and cancel methods are equivalent
    cancel(user)
  end

  def cancel(user='system')
    Job.transaction do
      job_cancel(user)
      cd = self.conversion_database
      # if the cancel was called from the conversion_database cancel, the status of cd will already be in an end-status, so the set_status will return false
      if cd.set_status(ConversionDatabase::STATUS_NOT_VALIDATED)
        if cd.remarks
          cd.remarks +="\n"
        else
          cd.remarks = ""
        end
        cd.remarks += "The validation job #{self.id} was cancelled."
        cd.save!
      end
    end
  end

  def stored_scriptfiles
    base = File.expand_path("#{ConfigParameter.get("dakota_scripts_dir",self.project_id.to_s)}/validation_#{id.to_s}")
    if File.exists?(base)
      return Dir.glob(base+'*')
    else
      # maybe moved to conversion database?
      if self.conversion_database
        db = self.conversion_database
        if !db.path_and_file.empty?
          outputdir = db.path_and_file.scan(/.*o_\d{8}.*\//).join()
          base = File.expand_path("#{outputdir}/validation_#{self.id.to_s}")
          if File.exists?(base)
            return Dir.glob(base+'*')
          end
        end
      end
    end
    return [] # returns empty array when no files found
  end

  def estimate_duration
    duration = 0 # Default / Unknown
    match = []
    prematch = ValidationJob.includes(:conversion).where("jobs.start_time is not null and jobs.finish_time is not null AND conversions.input_format = '#{self.conversion.input_format}' AND conversions.output_format = '#{self.conversion.output_format}' AND jobs.status = #{STATUS_COMPLETE}").references(:conversions).tagged_with(self.tag_list_on(:context_labelsupplier), :on => :context_labelsupplier).tagged_with(self.tag_list_on(:context_coverage), :on => :context_coverage)

    # For older validation jobs who doesn't have context-tags, this block can be removed in the future
    if prematch.empty?
      basematch = ValidationJob.includes(:conversion).where("jobs.start_time is not null and jobs.finish_time is not null AND conversions.input_format = '#{self.conversion.input_format}' AND conversions.output_format = '#{self.conversion.output_format}' AND jobs.status = #{STATUS_COMPLETE}").references(:conversions)
      # Search conversion-job tags and compare them with self
      basematch.each do |bmvj|
        if !bmvj.conversion_job.tag_list_on(:context_coverage).empty? && self.tag_list_on(:context_coverage) == bmvj.conversion_job.tag_list_on(:context_coverage)
          if !bmvj.conversion_job.tag_list_on(:labelsupplier).empty? && self.tag_list_on(:labelsupplier) == bmvj.conversion_job.tag_list_on(:labelsupplier)
            if !self.tag_list_on(:context_subregion).empty?
              if !bmvj.conversion_job.tag_list_on(:context_subregion).empty? && self.tag_list_on(:context_subregion) == bmvj.conversion_job.tag_list_on(:context_subregion)
                match << bmvj
                next
              else
                next
              end
            else
              match << bmvj
              next
            end
          end
        end
      end
    end
    # end block

    if match.empty?
      if self.tag_list_on(:context_subregion).empty?
        match = prematch
      else
        match = prematch.tagged_with(self.tag_list_on(:context_subregion), :on => :context_subregion)
      end
    end

    if !match.empty?
      durations = match.collect{|j|((j.finish_time - j.start_time) / 1.minute).round}
      average_duration = (durations.inject(0.0) { |sum, el| sum + el } / durations.size).round
      duration = average_duration
    end
    return duration
  end

  alias_method :old_allow_reschedule?, :allow_reschedule?
  def allow_reschedule?
    return old_allow_reschedule? && self.conversion_database.status < ConversionDatabase::STATUS_REJECTED
  end

end
