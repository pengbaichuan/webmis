class ViewReceptionsAndConversiondb < ActiveRecord::Base
  attr_accessor :match_percentage
  has_one :company_abbrs, :through => :data_release
  belongs_to :data_release
  belongs_to :data_type
  belongs_to :region , :foreign_key => :area_id
  
  scope :conversion, -> { where(:source => 'conversion') }
  scope :reception,  -> { where(:source => 'reception' ) }
  
  def tag_list
    if self.source == "reception"
     Reception.find(self.id).tag_list
    else
      # conversion db taglist not available yet
      []
    end
  end
  
  def match_perc(filter_tags)
    
    total = tag_list.size + filter_tags.size
    
    
    unmatched_source = tag_list - filter_tags
    missing_source = filter_tags - tag_list
    
    problems = unmatched_source.size + missing_source.size
    match_perc = 100
    if problems.size >0 
      match_perc = 100 
      match_perc = 100 - ((problems.to_f/total.to_f) * 100) if total.to_f > 0
      match_perc = match_perc.round
    end
    
    return [match_perc, unmatched_source, missing_source]
    
  end

  def source_obj(file_name)
    case self.source
    when "reception"
      return "Reception".constantize.find(self.uniqid)
    when "conversion"
      return "ConversionDatabase".constantize.available.where(conversion_id:self.uniqid,path_and_file:file_name).last
    when "pdata"
      return "ProcessDataElement".constantize.active.where(data_set_id:self.uniqid,directory:file_name).last
    end
  end

  def self.create_assembly_line(id,file_name,assembly_header_id)
    view_element = ViewReceptionsAndConversiondb.where(uniqid:id)[0]
    AssemblyLine.transaction do
      assembly_line = view_element.map_to_assembly_line(file_name)
      assembly_line.assembly_header_id = assembly_header_id
      assembly_line.save!
      return assembly_line
    end
  end

  def map_to_assembly_line(file_name)
    obj = self.source_obj(file_name)
    assembly_line = AssemblyLine.new
    assembly_line.source_type = obj.class.name
    assembly_line.source_id = obj.id
    assembly_line.file_name = file_name
    assembly_line.reception_id = self.uniqid
    assembly_line.reference_id = self.uniqid

    case obj.class.name
    when "Reception"
      reception = obj
      assembly_line.area = reception.region.name
      assembly_line.reference_id = reception.referenceid
      assembly_line.data_release_id = reception.data_release_id
      assembly_line.data_release_name = reception.data_release.company_abbr.name.to_s + " " + reception.data_release.name.to_s
      assembly_line.company_abbr_id = reception.data_release.company_abbr.id
    when "ConversionDatabase"
      conversion_database = obj
      conversion = conversion_database.conversion
      assembly_line.area = conversion.region_name
      assembly_line.data_release_id = conversion.data_release_id
      assembly_line.data_release_name = conversion.data_release.company_abbr.name.to_s + " " + conversion.data_release.name.to_s
      assembly_line.company_abbr_id = conversion.data_release.company_abbr.id
    when "ProcessDataElement"
      element = obj
      data_set = element.data_set
      assembly_line.area = data_set.region.name
      assembly_line.data_release_id = data_set.data_release.id
      assembly_line.data_release_name = data_set.data_release.name
      assembly_line.company_abbr_id = data_set.company_abbr_id
    else
      raise "Unknown source for ViewReceptionsAndConversiondb"
    end
    return assembly_line
  end
  
  
  
end
