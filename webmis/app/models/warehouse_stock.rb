class WarehouseStock < ActiveRecord::Base
  belongs_to :stock_order
  belongs_to :stock_orderline
  belongs_to :shipping_order
  belongs_to :shipping_orderline
  belongs_to :article
  belongs_to :stock_depot
  has_many :warehouse_stock_statuses
  
  after_save :logit

  STATUS_AVAILABLE = 20  # TODO, the others, this one already done because of reference in ShippingOrderline

  @@statushash      = Hash.new
  @@statushash[10]  = 'inbound'
  @@statushash[20]  = 'available'
  @@statushash[40]  = 'allocated'
  @@statushash[50]  = 'shipped'
  @@statushash[60]  = 'delivered'
  @@statushash[70]  = 'canceled' 
  @@statushash[80]  = 'adjusted' 
  
  
  def self.status_hash
    @@statushash.sort_by { |k,v| k }.map{|x|x.reverse}
  end

  def status_string
     @@statushash[self.status] 
  end
  
  def logit(remarkstr='-')
    @last_log = WarehouseStockStatus.where(:warehouse_stock_id => self.id).last
    if !@last_log.nil?
     @new_log = @last_log.dup
    else
      @new_log = WarehouseStockStatus.new
      @new_log.remarks = 'Created.'
    end
    
    @new_log.warehouse_stock_id = self.id
    @new_log.status = self.status
    session = Thread.current[:session]
    unless session.nil?
        @new_log.user_id = session[:user].id
    end
    
    if !@last_log.nil?
      @new_log.changes.each {|key, value| @new_log.remarks = "#{key} changed from #{value[0]} to #{value[1]}" }
      @new_log.created_at = Time.now
      @new_log.updated_at = Time.now
    end
    
    if remarkstr != '-'
      @new_log.remarks = remarkstr.to_s
    end
    
    @new_log.save  
  end
  
  def self.map(stock_orderline,ws=nil)
    if !ws
      ws = WarehouseStock.new
      ws.status = 10 #inbound for now
    end
    if ws.status > 10
      raise "Cannot update - illegal stock status"
    end
    ws.stock_order_id = stock_orderline.stock_order_id
    ws.stock_orderline_id = stock_orderline.id
    ws.article_id = stock_orderline.article_id
    if stock_orderline.status < 50 && !stock_orderline.amount_confirmed 
      ws.org_quantity = stock_orderline.amount_order
      ws.quantity = stock_orderline.amount_order
    else
      ws.org_quantity = stock_orderline.amount_confirmed
      ws.quantity = stock_orderline.amount_confirmed
    end
    ws.stock_reference = stock_orderline.stock_reference
    ws.remarks = stock_orderline.remarks
    ws.stock_depot_id = stock_orderline.stock_order.depot_id
    return ws
  end
  
  def self.create_or_update_from_orderline(stock_orderline)
    stock = WarehouseStock.where(stock_orderline_id:stock_orderline.id)
    if stock.size > 1
      raise "Integrity error"
    end
    ws = nil
    if stock.size == 0
      ws= map(stock_orderline)
    else
      ws = map(stock_orderline,stock[0])
    end
    return ws
  end
  
  def update_status(status)
     self.status = 20
  end
  
  def allocate(shipping_orderline, quantity_left_to_allocate)
    split_stock = dup
    if self.quantity <= quantity_left_to_allocate
       # full stock allocation - leave root stockrecord
       self.quantity = 0
       split_stock.org_quantity = split_stock.quantity 
       quantity_left_to_allocate = quantity_left_to_allocate - split_stock.quantity
    else
       split_stock.quantity = quantity_left_to_allocate
       split_stock.org_quantity = quantity_left_to_allocate
       self.quantity = self.quantity - quantity_left_to_allocate
       quantity_left_to_allocate = 0
       #line fully allocated -> update status
       shipping_orderline.status = 30
       shipping_orderline.save
    end
    split_stock.status = 40
    split_stock.org_id = self.id
    if shipping_orderline.has_attribute?("shipping_order_id")
      split_stock.shipping_order_id = shipping_orderline.shipping_order_id
      split_stock.shipping_orderline_id = shipping_orderline.id
    else
      #adjustment
      split_stock.warehouse_stock_adjustment_id = shipping_orderline.id
    end
    
    split_stock.save
    save
    return quantity_left_to_allocate
  end
  
  def deallocate(quantity_to_deallocate)
    raise "Stockid #{self.id} at invalid stock, cannot deallocate " if self.status >= 50
    orgstock = WarehouseStock.find(self.org_id)
    raise "Stock #{orgstock.id} at invalid status, cannot deallocate " if orgstock.status != 20
           
    if self.quantity >= quantity_to_deallocate
      orgstock.quantity = orgstock.quantity + quantity_to_deallocate
      quantity_to_deallocate = 0
    else
      orgstock.quantity = orgstock.quantity + self.quantity
      quantity_to_deallocate = quantity_to_deallocate - self.quantity
    end
    self.status = 70
    orgstock.save
    save
    return quantity_to_deallocate
  end
  
  
end
