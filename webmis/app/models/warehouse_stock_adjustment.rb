class WarehouseStockAdjustment < ActiveRecord::Base 
  has_many :warehouse_stocks
  
  @@statushash      = Hash.new
  @@statushash[30]  = 'adjusted'

  def self.status_hash
    @@statushash.sort_by { |k,v| k }.map{|x|x.reverse}
  end

  def status_string
     @@statushash[self.status]
  end
  
  def adjust
    if !self.quantity
      raise "Please specify quantity"
    end

    WarehouseStockAdjustment.transaction do
      if self.quantity > 0
        #duplicate or add stock and link to adjustment
        increase_stock
      else
        allocate
      end
    end
  end
  
  def increase_stock
    # find stock - conditions to expand - add batch number
    qstring = "article_id = ? and status = 20 and quantity > 0"
    vals = [self.article_id]
    
    if self.stock_order_id && self.stock_order_id.size  > 0 
      qstring = qstring + " and stock_order_id = ?"
      vals << self.stock_order_id
    end
    
    available_stock = WarehouseStock.where([qstring] + vals).order('created_at').first
    if !available_stock
      raise "No stock found to adjust"
    else
      adjusted_stock = available_stock.dup
      adjusted_stock.warehouse_stock_adjustment_id = self.id
      adjusted_stock.quantity = self.quantity
      adjusted_stock.save
    end
  end
  
  def allocate
    
    raise "Adjustment already done" if  self.status && self.status >= 30
    self.status = 30
    quantity_left_to_allocate = 0 - self.quantity
    
    if quantity_left_to_allocate <= 0
      raise "Invalid quantity"
    end
    
    # find stock - conditions to expand - add batch number
    qstring = "article_id = ? and status = 20 and quantity > 0"
    vals = [self.article_id]
    
    if self.stock_order_id && self.stock_order_id.size > 0
      qstring = qstring + " and stock_order_id = ?"
      vals << self.stock_order_id
    end
    
    available_stock = WarehouseStock.where([qstring] + vals).order('created_at')
    available_stock.each do |stock|
      quantity_left_to_allocate = stock.allocate(self,quantity_left_to_allocate)
      break if quantity_left_to_allocate == 0
    end
    if quantity_left_to_allocate != 0
      raise "Not enough stock"
    end
    
    #update allocated stock to status adjusted within transaction
    adjusted = WarehouseStock.where("warehouse_stock_adjustment_id = #{self.id}")
    adjusted.each do |stock|
      stock.status = 80
      stock.save
    end
    save
  end
  
  
end
