json.array!(@shipping_jobs) do |shipping_job|
  json.extract! shipping_job, :id
  json.url shipping_job_url(shipping_job, format: :json)
end
