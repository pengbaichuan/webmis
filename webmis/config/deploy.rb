require "delayed/recipes" 
require 'thinking_sphinx/capistrano'

default_run_options[:pty] = true
set :runner, nil
set :application, "WebMIS"
set :repository,  "."
set :tmp_dir,"/tmp/deploy/#{application}"
set :copy_dir,"/tmp/"
set :deploy_to, "/var/rails_apps/#{application}"
set :scm, :none
set :deploy_via, :copy
set :checkout, "export"
set :use_sudo, false

if env && env == 'production'
  set :rails_env, "production" #added for delayed job 
  puts "*********** DEPLOYING TO PRODUCTION ***********"
  before 'deploy', 'mv_dir_to_tmp'
  role :app, "msbackoffice05"
  role :web, "msbackoffice05"
  role :db,  "msbackoffice05", :primary => true
  set :user, 'rails.prod'
  set :server_name, "msbackoffice05.mapscape.nl"
elsif env && env == 'developmenttest'
  puts "*********** DEPLOYING TO DEVELOPMENTEST ***********"
  set :rails_env, "developmenttest" #added for delayed job 
  set :user, `whoami`
  print "User is set to #{user}\n"
  set :desktop,Socket.gethostname
  print "Server is set to #{desktop}\n"
  role :app, desktop
  role :web, desktop
  role :db,  desktop, :primary => true
  set :server_name, "#{desktop}.mapscape.nl"
elsif env && env == 'staging'
  set :rails_env, "staging" #added for delayed job 
  puts "*********** DEPLOYING TO STAGING ***********"
  before 'deploy', 'rm_models'
  role :app, "msbackoffice06"
  role :web, "msbackoffice06"
  role :db,  "msbackoffice06", :primary => true
  set :user, 'rails.test'
  set :server_name, "msbackoffice06.mapscape.nl"
else
  puts "Please specify valid environment"
  return
end

deploy.task :restart, :roles => :app do
  sudo "chgrp -R www-data #{release_path}"
  sudo "chmod -R 770 #{release_path}"
  run "touch #{current_path}/tmp/restart.txt"
end

task :copy_files do
  if env && env == 'production'
    print "Cleanup temporary directory #{tmp_dir}\n"
    system "rm -rf #{tmp_dir}"
  end
end

task :mv_dir_to_tmp do
  print "Copy project to writeable tmp-dir\n"
  system "mkdir -p #{tmp_dir}"
  system "cp -R #{repository} #{tmp_dir}"
  system "chmod -R 777 #{tmp_dir}"
  print "Removing Gemfile.lock\n"
  system "rm #{tmp_dir}/Gemfile.lock"
  print "Remove the app/models link if it exists\n"
  system "rm -f #{tmp_dir}/app/models"
  set :repository, "#{tmp_dir}"
end

task :rm_models do
  print "Remove the app/models link if it exists\n"
  system "rm -f #{repository}/app/models"
end

namespace :bundle do

  desc "run bundle install and ensure all gem requirements are met"
  task :install do
    sudo "bundle install --gemfile #{release_path}/Gemfile"
    sudo "chown -R '#{user}' #{release_path}"
  end

end
before "whenever:update_crontab", "bundle:install"

after "deploy:stop",    "delayed_job:stop"
after "deploy:start",   "delayed_job:start"
after "deploy:restart", "delayed_job:restart"

# If you want to use command line options, for example to start multiple workers,
# # define a Capistrano variable delayed_job_args:
# #
# #   set :delayed_job_args, "-n 2"
#

after "deploy:finalize_update", "copy_files"

set :whenever_environment, defer { env }
set :whenever_identifier, defer { "#{application}_#{env}" }
set :whenever_command, "bundle exec whenever"
set :whenever_variables, defer { "'environment=#{env}&current_path=#{current_path}'" }
require 'whenever/capistrano'
after 'deploy:finalize_update','whenever:update_crontab'
