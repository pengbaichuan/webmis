# Load the rails application
require File.expand_path('../application', __FILE__)
require 'net/ssh'
require 'ldap'
#require 'ruport'
#require 'ruport/acts_as_reportable'

ENV['ldap_host']= "admin.mapscape.nl"
ENV['ldap_port']= '636'

ENV['module'] = "MIS"
ENV['pds_document_path'] = "/data/newgroups/WebmisDocuments/Product_Definitions/Individual_Sheets/"

require "handle_versioning.rb"
require "exceptions/is_active_runtime_error.rb"
require "validators/customer_content_valid_validator.rb"
require "validators/workflow_validator.rb"
require "file_system_util.rb"

# Initialize the rails application
# MIS::Application.initialize!
Rails.application.initialize!

Encoding.default_external = Encoding::UTF_8
Encoding.default_internal = Encoding::UTF_8

def r
  reload!
  return "loaded"
end

def nolog
  ActiveRecord::Base.logger = nil
  return "Active record logger disabled"
end

require 'test_set_xml'

