MIS::Application.configure do
  # Settings specified here will take precedence over those in config/environment.rb

  # In the development environment your application's code is reloaded on
  # every request.  This slows down response time but is perfect for development
  # since you don't have to restart the webserver when you make code changes.
  config.cache_classes = false

  # Log error messages when you accidentally call methods on nil.
  config.whiny_nils = true

  # Show full error reports and disable caching
  config.consider_all_requests_local = true
  #config.action_view.debug_rjs                         = true
  config.action_controller.perform_caching             = true
  #config.action_view.cache_template_loading            = true
  config.cache_store = :file_store, "/tmp/railscache"
   

  # Don't care if the mailer can't send
  config.action_mailer.raise_delivery_errors = false

  # For production and stagin
  #config.action_controller.relative_url_root = "/webmis"
  config.assets.prefix = '/webmis/public'
  config.assets.precompile << /\.(?:svg|eot|woff|ttf)$/
  config.eager_load=true
  
  ENV['crm_connect'] = "staging_crm"
  ENV['host'] = "backofficestaging.mapscape.nl"
  ENV['ppf_import'] = '/data/newgroups/Product_Management/Product_Definitions/Individual_Sheets/carin/PDS__Export.csv'
  ENV['ldap_host']= "admin.mapscape.nl"
  ENV['ldap_port']= '636'
  
  ENV['prod_user'] = 'backoffice_test'
  ENV['default_database_expiry_in_days'] = '14'
  ENV['TMPDIR'] = Rails.root.to_s + '/tmp'
end
