MIS::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false
  config.eager_load = false

  # Show full error reports and disable caching
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send
  config.action_mailer.raise_delivery_errors = true  
  
  # Print deprecation notices to the Rails logger
  config.active_support.deprecation = :log


  # Raise exception on mass assignment protection for Active Record models
  #config.active_record.mass_assignment_sanitizer = :strict

  # Do not compress assets
  config.assets.compress = false

  # Expands the lines which load the assets
  config.assets.debug = false
  config.assets.logger = false
  
  # For production and staging
  #config.action_controller.relative_url_root = "/webmis"
  config.assets.prefix = '/webmis/sprockets'

  ENV['host'] = "localhost:3000"
  ENV['crm_connect'] = "development_crm"
  ENV['prod_user'] = 'backoffice_test'
  ENV['TMPDIR'] = Rails.root.to_s + '/tmp'

  # Raise an error on page load if there are pending migrations.
  #config.active_record.migration_error = :page_load

  # to prevent DEPRECATION WARING
  config.active_support.test_order = :sorted
end
