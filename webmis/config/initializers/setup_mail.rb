ActionMailer::Base.smtp_settings = {
  :address => "smtp.mapscape.nl",
  :port => 25,
  :domain => "mapscape.eu",
  :openssl_verify_mode  => 'none'
}

ActionMailer::Base.default_url_options[:host] = "localhost:3000"
ActionMailer::Base.raise_delivery_errors