MIS::Application.routes.draw do

  # Error pages
  get '404', :to =>"errors#not_found"
  get '500', :to => "errors#internal_server_error"
  
  get '/', to: redirect('/webmis')

  scope '/webmis' do
      
    # The priority is based upon order of creation: first created -> highest priority.

    # Sample of regular route:
    #   get 'products/:id' => 'catalog#view'
    # Keep in mind you can assign values other than :controller and :action
    match "/delayed_job" => DelayedJobWeb, :anchor => false, via: [:get, :post]
    # Sample of named route:
    #   get 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
    # This route can be invoked with purchase_url(:id => product.id)

    # Sample resource route (maps HTTP verbs to controller actions automatically):
    #   resources :products

    # Sample resource route with options:
    #   resources :products do
    #     member do
    #       get 'short'
    #       post 'toggle'
    #     end
    #
    #     collection do
    #       get 'sold'
    #     end
    #   end

    # Sample resource route with sub-resources:
    #   resources :products do
    #     resources :comments, :sales
    #     resource :seller
    #   end

    # Sample resource route with more complex sub-resources
    #   resources :products do
    #     resources :comments
    #     resources :sales do
    #       get 'recent', :on => :collection
    #     end
    #   end

    # Sample resource route within a namespace:
    #   namespace :admin do
    #     # Directs /admin/products/* to Admin::ProductsController
    #     # (app/controllers/admin/products_controller.rb)
    #     resources :products
    #   end

    # You can have the root of your site routed with "root" just remember to delete public/index.html. root
    # :to => 'welcome#index'

    # See how all your routes lay out with "rake routes"

    # This is a legacy wild controller route that's not recommended for RESTful applications. Note: This route
    # will make all actions in every controller accessible via GET requests. get
    # ':controller(/:action(/:id))(.:format)'

    root :to => 'main#index'
    resources :allocation_exceptions
    resources :errors do
      get :not_found, :on => :collection
      get :internal_server_error, :on => :collection
    end
    resources :api_logs

    resources :warehouse_stock_adjustments do
      get :onhand, :on => :collection
      post :onhand, :on => :collection
    end

    resources :areas do
      get :generate_dakota_id, :on => :collection
      member do
      end
    end
    resources :area_types
    resources :articles
    resources :article_licenses do
      get :license_fee_report, :on => :collection
      post :html2pdf_license_fee_po, :on => :collection
    end
    resources :assembly_headers do
      member do
        get 'create_csv'
        get 'cloneit'
        get 'table_edit'
        post 'add_checked'
        post 'destroy_line'
        get 'add_tag4mre'
        get 'remove_mre_tag'
        get 'add_update_region_tag'
        get 'replace_data'
        get 'update_coverage_view_by_map'
      end
    end
    resources :assembly_lines
    resources :assets do
      member do

      end
    end
    resources :asset_type_number_ranges
    resources :asset_types do
      get 'create_number', :on => :collection
      post 'create_number', :on => :collection
      member do

      end
    end
    # resources :assets
    resources :business_processes
    resources :carin_productsets
    resources :cities do
      post 'redirect_to_index' , :on => :collection
      get :generate_dakota_id, :on => :collection
    end
    resources :commitment_change_reasons
    resources :companies do
      member do
        get  :handle
        post :change_status
      end
    end
    resources :company_abbrs, :tags do
      get :dakota_supplier_ids_file, :on => :collection
      get :generate_dakota_id, :on => :collection
    end
    resources :content_release_notes do
      get :get_versions_and_regions_for_project, :on => :collection
      get :get_jira_release_note, :on => :collection
    end
    resources :conversion_databases do
      get :marked_for_removal_index, :on => :collection
      post :batch_mark_for_removal, :on => :collection
      post :db_remove_script, :on => :collection
      get :download_db_remove_script, :on => :collection
      get :render_file_tree, :on => :collection
      member do
        get :handle
        post :change_status
        get :download_stdout_log
        post :mark_for_removal
        post :unmark_for_removal
      end
    end
    resources :conversion_jobs do
      get :failures, :on => :collection
      post :tagging_fail_reason, :on => :collection
      member do
        post :cancel
        post :clean_working_dir
        post :edit_new_copy
        get :edit_new_copy
        post :handle_crashed
        post :reschedule
        get :tail_z_log
        get :terminal
        get :cancel
        post :generated_script
      end
    end
    resources :conversions, :tags do
      get :check_cvtool, :on => :collection
      get :get_cvtool_bundles, :on => :collection
      get :get_datareleases, :on => :collection
      get :get_datareleases_for_overview, :on => :collection
      get :get_production_orders, :on => :collection
      get :import_assembly , :on => :collection
      get :import_process_data , :on => :collection
      get :get_data_by_pid , :on => :collection
      post :check_cvtool, :on => :collection
      post :allocate_data, :on => :collection
      get :cloneit, :on => :collection
      get :show_validation_errors, :on => :collection
      post :handle_validation_errors, :on => :collection
      post :close_validation_errors, :on => :collection
      get :patch_requests_report, :on => :collection
      post :change_year_patch_requests_report, :on => :collection
      get :conversions_for_select, :on => :collection
      get :fill_cloned_attributes, :on => :collection
      member do
        get 'handle'
        get 'report'
        get 'mail'
        post :toggle_mark_for_removal
        get 'tpd'
        get 'to_remove'
        get 'favorite'
        get 'remove_tag'
        get 'reset_outcome'
        get 'fill_marking_for_removal_modal_confirm'
        post 'restart_failed_and_crashed_conversion_jobs'
        post 'toggle_outcome_persistence'
        get :new_patch_request
        get :edit_result
        post :update_result
      end
    end
    resources :config_parameters
    resources :conversion_environments do
      member do
        get :toggle_active_yn
      end
    end
    resources :conversiontools do
      post :sync, :on => :collection
      member do
        post :toggle_isactive
      end
    end
    resources :continents do
      get :generate_dakota_id, :on => :collection
    end
    resources :countries do
      post 'redirect_to_index', :on => :collection
      get :generate_dakota_id, :on => :collection
    end
    resources :country_conversion_productsets
    resources :customer_depots
    resources :cvtool_bundles do
      get :get_cvtool_bundles, :on => :collection
      member do
        post :toggle_active_yn
      end
    end
    resources :cvtool_templates do
      member do
        post :upload_script
        get :sendscript
      end
    end
    resources :dashboard_grid_widgets do
      post :save_position, :on => :collection
    end
    resources :dashboard_widgets do
      get :render_widget, :on => :collection
      get :widget_demo_01, :on => :collection
      get :widget_demo_02, :on => :collection
      get :widget_demo_03, :on => :collection
      get :widget_demo_04, :on => :collection
      member do
        
        get :drop_widget_from_grid
      end
    end
    resources :dashboard_grids do
      get :my_dashboards, :on => :collection
      member do
        get :unused_widgets
      end
    end
    resources :data_media
    resources :data_releases do
      member do
        get :render_prev_data_releases
      end
    end
    resources :data_sets do
      post :filling_for_data_type, :on => :collection
      post :release_for_company_abbr, :on => :collection
      post :update_coverage_regions_list_view, :on => :collection
      get :receptions_for_select, :on => :collection
      post :get_split_by_key, :on => :collection
      get :render_regions_for_input, :on => :collection
      member do
        get :handle
        post :change_status
        get :clone
        put :duplicate
        patch :duplicate
        post :add_to_group
        post :remove_from_group
        post :create_group
        get :check_production_order
        post :checked_production_order
        post :create_production_order
        get :edit_process_data_elements
        get :browse_directory
        post :save_process_data_element
        post :remove_process_data_element
        post :save_process_data_element_action_log
        post :save_poi_mapping_layout
        post :sync_with_directory_structure
        get :validate_structure
        get :element_counter
        get :show_process_data_elements
        get :download_file
        get :render_data_set_pre_selection
        post :request_review
        post :review_result
        get :in_use_orders
        get :show_jobs_modal
      end
    end
    resources :data_set_groups do
      post :release_for_company_abbr, :on => :collection
      post :filling_for_data_type, :on => :collection
      member do
        get :handle
        post :remove
        post :restore
        get :clone
        get :check_production_order
      end
    end
    resources :data_types do
      post 'add_filling_to_list', :on => :collection
      post 'remove_filling_from_list', :on => :collection
      member do
        get "handle"
        get "approved"
        post "draft"
        post "review"
        post "approve"
        post "approve_directly"
        post "obsolete"
        post "revert"
      end
    end
    resources :data_type_versions
    resources :default_content_release_notes do
      get 'ranking', :on => :collection
      member do
        get :rank_it
      end
    end
    resources :distribution_list_members
    resources :distribution_lists do
      member do
        get 'delete_y'
        get 'select_new_hard_copies'
      end
    end
    resources :documents do
      post :create_from_external_controller, :on => :collection
      member do
        get 'download'
        get 'select'
        get :delete
      end
    end
    resources :external_contacts do
      member do
        get  :handle
        post :change_status
      end
    end
    resources :file_transfer_accounts do
      get 'new'
      member do
        get 'new'

      end
    end
    resources :fillings do
      member do
        get 'handle'
        post 'remove'
        post 'restore'
        post 'deactivate'
        post 'activate'
      end
    end
    resources :high_level_boms do
      get 'get_product_locations', :on => :collection
      member do

      end
    end
    resources :inbound_orderlines
    resources :invoices do
      member do
        post :invoice
        post :paid
        get :print
        get :credit_invoice
      end
    end
    resources :job_fail_reasons
    resources :jobs do
      get :tagged_failed_jobs_report, :on => :collection
    end
    resources :licenses
    resources :locations
    resources :main do
      get  'routes', :on => :collection
      get  'archive', :on => :collection
      get  'administration', :on => :collection
      get  'shipping', :on => :collection
      get  'portfolio', :on => :collection
      get  'denied', :on => :collection
      post 'searchit', :on => :collection
      get 'searchit', :on => :collection
      get 'change_project', :on => :collection
      get :get_json_file_tree , :on => :collection
      member do
        get 'receptions_admin'
        get 'media_admin'
      end
    end
    resources :mail_conditions
    resources :mail_events do
      member do
        get 'show_sub'
      end
    end
    resources :ordertypes do
      get 'access_control', :on => :collection
      member do
        get 'edit_workflow'
        get 'clone'
        post 'change_status'
      end
    end
    resources :outbound_orderlines do
      member do
        get 'destroy_line'
        get 'report'
      end
    end
    resources :outbound_orders do
      get 'link', :on => :collection
      get 'release_notes_request', :on => :collection
      post 'send_release_notes_request', :on => :collection
      get 'get_products_for_request', :on => :collection
      get 'create_from_order', :on => :collection
      member do
        get 'clone_line'
        get 'html2pdf_slip'
        get 'ordr_lines'
        get 'get_order_lines_mediums'
        get 'get_order_lines_products'
        get 'get_order_lines_manually'
        get :edit_packing
        get :delete_me
        get :show_release_mail
        get :send_bulk_release_mail
        get :send_release_mail
        get :send_release_mail_to_self
        post :cancel_packing
        post :update_packing
      end
    end
    resources :customer_contents do
      member do
        get "render_value"
      end
    end
    resources :customer_content_definitions
    resources :customer_content_specifications do
      post :customer_content_values, :on => :collection
      post :create, :on => :collection
      member do
        get "handle"
        get "approved"
        post "draft"
        post "review"
        post "approve"
        post "approve_directly"
        post "obsolete"
        post "revert"
      end
    end
    resources :customer_content_specification_versions

    resources :customer_content_values do
      member do
        get "render_content"
      end
    end
    resources :customer_targets
    resources :product_lines do
      post :parameters, :on => :collection
      get  :customer_content_definitions, :on => :collection

      post :update_project_members_list, :on => :collection
      member do
        post :customer_content_definitions
        
      end
    end

    resources :packing_jobs do
      get :failures, :on => :collection
      post :tagging_fail_reason, :on => :collection
      member do
        post :cancel
        post :clean_working_dir
        post :edit_new_copy
        post :regenerate
        post :reschedule
        get :cancel
      end
    end
    resources :packing_specifications do
      member do
        get "handle"
        get "approved"
        post "draft"
        post "review"
        post "approve"
        post "approve_directly"
        post "obsolete"
        post "revert"
      end
    end
    resources :packing_specification_versions

    resources :poi_mappings do
      get :content_filter, :on => :collection
      get :details, :on => :collection
      member do
        get :details
      end
    end
    resources :poi_mapping_layouts do
      get :upload_new, :on => :collection
      get :upload_create, :on => :collection
      post :upload_create, :on => :collection
      member do
        get :export_csv
      end
    end
    resources :points
    resources :process_data do
      get :receptions_for_select, :on => :collection
      get :get_data_releases_by_supplier_name, :on => :collection
      get :render_modal_edit_element, :on => :collection
      get :render_modal_edit_log, :on => :collection
      post :cancel_extract_job, :on => :collection
      get :sync_file_structure, :on => :collection
      get :delete_process_data_element, :on => :collection
      get :receptions, :on => :collection
      get :reception_content, :on => :collection
      post :copy_reception_data, :on => :collection
      get :get_datareleases_for_overview, :on => :collection
      post :filling_for_data_type, :on => :collection
      post :update_pml, :on => :collection
      post :request_review, :on => :collection
      post :get_split_by_key, :on => :collection
      get :download_file, :on => :collection
      member do
        get :maintain
        get 'edit_process_data_line'
        post 'save_item_and_tags'
        post 'remove_item'
        get 'new_document'
        post 'create_document'
        put 'update_process_data_line'
        get 'change_status'
        get 'chart'
        post 'save_tags'
        put 'save_process_data_element'
        post 'save_process_data_element'
        get :patch_element
        post 'save_process_data_log'
        get 'delete_process_data_element'
        get :new_production_order
        get :check_production_order
        post :create_production_order
        post :checked_production_order
        get :create_directory_structure
        post :update_header
        get :extract_jobs
        get :validate_directory_structure_report
        get :mark_former_conversion_databases_for_removal
        post :perform_mark_former_conversion_databases_for_removal
        get :show_resulting_databases
        get :show_involved_jobs
        get :render_file_tree
        post :reject_review
        
      end
    end
    resources :process_data_lines

    resources :process_data_sub_events do
      member do
        get :handle
        post :change_status
      end
    end

    resources :process_data_events do
      member do
        get :handle
        post :change_status
      end
    end

    resources :process_data_elements

    resources :process_data_specifications do
      post 'filling_for_data_type' , :on => :collection
    end


    resources :process_transitions do
      member do
        get 'delete'
      end
    end
    resources :product_categories
    resources :product_data_set_infos do
       get :get_data_releases_for_select, :on => :collection
       get :get_filling_for_select, :on => :collection
    end
    resources :product_content_specifications do
      get :content_filter, :on => :collection
      get :data_types, :on => :collection
      get :datareleases, :on => :collection
      get :regions, :on => :collection
      get :filling, :on => :collection
      get :update_regions, :on => :collection
    end
    resources :product_locations do
      get :get_details_by_conversion, :on => :collection
      get :get_details_by_reception, :on => :collection
      get :get_checksum_file_content, :on => :collection
      member do
        get 'report'
      end
    end
    resources :product_release_notes do
      get :create_from_production_order, :on => :collection
      get :create_from_production_orderline, :on => :collection
      member do
        get 'to_status'
        get 'move_updown'
        get 'move_leftright'
        get 'package'
      end
    end
    resources :production_orders,:tags do
      get :index_blocks, :on => :collection
      get :get_ordertype, :on => :collection
      get :new_from_product, :on => :collection
      post :new_from_product, :on => :collection
      get :my_work, :on => :collection
      post :feedback, :on => :collection
      post :create_from_product, :on => :collection
      get :get_productsets_by_type, :on => :collection
      get :check, :on => :collection
      get :check_from_product, :on => :collection
      post :checked, :on => :collection
      post :checked_from_product, :on => :collection
      get :show_conversion_jobs, :on => :collection
      get :get_conversion_steps_from_design, :on => :collection
      get :render_product_design_select, :on => :collection
      post :restart_failed_and_crashed_conversion_jobs, :on => :collection
      get :dynamic_id_show, :on => :collection

      member do
        get 'trace'
        get 'manage'
        get 'complete_work'
        get 'cancel'
        post 'terminate'
        get 'release_notes_send'
        post 'cancel_product'
        post 'terminate_product'
        post 'reset_product'
        get 'select_product'
        get 'add_product'
        get 'activate_product'
        get 'complete_work_product'
        get 'delete_product'
        patch 'confirm_work'
        post 'add_product'
        get 'remove_tag'
        get 'add_tag'
        patch 'confirm_work_product'
        get 'line_transition_back'
        get 'refresh_lines'
        get 'refresh_lines_from_show'
        get 'refresh_lines_from_manage'
        post 'reset_order'
        post :force_complete
        get :edit_baseline_commitment
        get :edit_modified_commitment
        get :edit_expiry_date
        patch :update_baseline_commitment
        patch :update_modified_commitment
        patch :update_expiry_date
        get :parameters_for_product
        get :tool_bundles
        get :output_location_for_product
        get :improvement_processes_for_product
        get :parameters_for_product
        get :features_for_product
        get :poi_mapping_for_product
      end
    end

    resources :production_orderlines do
      get 'shipment_index', :on => :collection
      get 'mastering_index', :on => :collection
      get :on_time_delivery_report, :on => :collection
      member do
        get 'show_shipment_request'
        get 'request_shipment'
        get 'request_shipment_bulk'
        get 'evaluate'
        post 'release_or_reset'
      end
    end
    resources :products do
      get :update_coverage_view_by_map, :on => :collection
      put  :update_coverage_view_by_select, :on => :collection
      post :update_coverage_view_by_select, :on => :collection
      patch :update_coverage_view_by_select, :on => :collection
      put :update_coverage_view_by_delete, :on => :collection
      patch :update_coverage_view_by_delete, :on => :collection
      put :save_coverage_for_data_sets, :on => :collection
      post :save_coverage_for_data_sets, :on => :collection
      patch :save_coverage_for_data_sets, :on => :collection
      put  :sync_regions,:on => :collection
      post :sync_regions,:on => :collection
      post :get_regions, :on => :collection
      post :validate_volumeid, :on => :collection
      
      member do
        get 'pdf_all'
        get 'select'
        get 'disable'
        get 'destroy_link'
        get 'destroy_link_document'
        post 'clone'
        post :pdsi_from_product
        patch :pdsi_from_product
        post :filling_necessary
        post :create_data_set_from_definition
        post :create_data_sets_from_definition_group
      end
    end
    resources :productsets, :tags do
      get :show_import, :on => :collection
      get :start_import, :on => :collection
      get :show_import_category_name, :on => :collection
      get :get_datareleases_by_company_abbr_id, :on => :collection

      member do
        get 'remove_tag'
        get 'favorite'
        get 'export'
        get 'export_pdf'
        get 'show_tmc'
        get 'salespack'
        get 'disable'
        get 'destroy_link'
        get 'destroy_link_document'
        get 'clone_set'
        get 'handle'
      end
    end

    resources :projects do
      member do
        get :handle
        post :change_status
      end
    end

    resources :included_article_license_purchase_orderlines
    resources :purchase_orderlines
    resources :purchase_orders do
      member do
        get :change_status
      end
    end
    resources :receptions, :tags do
      get :requests, :on => :collection
      get :new_request, :on => :collection
      post :create_request, :on => :collection
      get :index_can_be_removed, :on => :collection
      get :get_datareleases, :on => :collection
      post :update_removal, :on => :collection
      get :receptions_for_select, :on => :collection
      post :filling_for_data_type, :on => :collection
      member do
        get 'edit_validation_results'
        get 'mail'
        get 'can_be_removed'
        get 'mark_as_removed'
        get 'remove_tag'
        get 'favorite'
        get 'show_request'
        get 'edit_request'
        put :update_request
        get 'add_parent'
        get 'delete_parent'
        get 'reject_request'
        put 'reject'
        post 'set_inprogress'
        get :edit_intake_results
        get :render_file_tree
      end
    end
    resources :regions do
      get :dakota_region_ids_file, :on => :collection
      get :dakota_names_file, :on => :collection
      get :download_test_areas_exact_script, :on => :collection
      member do
        get :test_area_configurations
        get :render_tac_frm
        post :create_test_area_configuration
        post :update_test_area_configuration
        get :delete_test_area_configuration
        get :render_tac_generation
      end
    end
    resources :region_aliases do
      member do
      end
    end
    resources :release_note_attachments_product_categories
    resources :release_note_attachments do
      member do
        post :update_line
        post :new_line
        get  :remove
      end
    end
    resources :roles
    resources :role_actions do
 post :new_line, :on => :collection
 post :destroy_line, :on => :collection


    end
    resources :seq_ints
    resources :sequences
    resources :servers do
      get :overview, :on => :collection
      post :toggle_shutdown, :on => :collection
      post :toggle_schedule_on_off, :on => :collection
      post :job_execute, :on => :collection
      post :job_reset, :on => :collection
      post :job_assign, :on => :collection
      post :crashed_job_default_action, :on => :collection
      get :reset, :on => :collection
      get :refresh_overview, :on => :collection
      post 'update_serverprocess', :on => :collection
      member do
        get 'reset'
        post 'update_serverprocess'
        get 'assign_to_server'
      end
    end
    resources :shipment_types
    resources :shipping_orderlines do
      get :call_of_list, :on => :collection
      get :uninvoiced_shipped_list, :on => :collection
      get :unpaid_shipped_list, :on => :collection
      get :new_retroaction, :on => :collection
      get 'multi_invoice', :on => :collection
      get 'start_retroaction', :on => :collection
      get 'perform_retroaction', :on => :collection
      post 'perform_retroaction', :on => :collection
      post 'process_unpaid_to_paid', :on => :collection
      get :invoice_from_list, :on => :collection
      member do
        get 'html_to_pdf_invoice'
        get 'html_to_pdf_call_of'
        get 'paid'
        get 'invoice'
        get 'deallocate'
        get 'allocate'
        get 'destroy_me'
        get 'create_invoice'
        put 'create_invoice'
        get 'allocation'
        put 'allocation'
        get 'ship'
        get 'shipping'

      end
    end
    resources :shipping_jobs do
      get :failures, :on => :collection
      post :tagging_fail_reason, :on => :collection
      member do
        post :cancel
        post :clean_working_dir
        post :edit_new_copy
        post :regenerate
        post :reschedule
        get :cancel
      end
    end
    resources :shipping_orders do
      member do
        post :cancel
      end
    end

    resources :shipping_specifications
    resources :states do
      get :redirect_to_index, :on => :collection
      get :generate_dakota_id, :on => :collection
    end
    resources :static_release_note_titles
    resources :stock do
      get :requests, :on => :collection
      get :documents,  :on => :collection
      get :docs_overview,  :on => :collection
      get :clear_selected, :on => :collection
      get :borrowed, :on => :collection
      get :new_request, :on => :collection
      post :create_request, :on => :collection
      get 'get_datareleases', :on => :collection
      get 'get_data_releases', :on => :collection
      get 'get_details', :on => :collection
      get :cyclecountreport, :on => :collection
      member do
        get :show_request
        get :edit_request
        get :clone_request
        get :medium_validated
        get :reject_medium
        get :html2pdf_medium_request
        get :perform_medium_rejection
        put :perform_medium_rejection
        patch :perform_medium_rejection
        get :move2box
      end
    end
    resources :stock_depots
    resources :stock_orderlines do
      member do
        get 'order'
        get 'confirm'
        put 'confirmed'
        patch 'confirmed'
      end
    end
    resources :stock_orders do
      member do
        get :html2pdf_stock_order
      end
    end
    resources :tags
    resources :test_request_jobs do
      get :failures, :on => :collection
      post :tagging_fail_reason, :on => :collection
      member do
        post :cancel
        post :clean_working_dir
        post :edit_new_copy
        post :reschedule
        get :cancel
        post :generated_script
      end
    end
    resources :test_requests do
      get :conversion_databases_for_select, :on => :collection
      get :render_file_tree, :on => :collection
      member do
        get 'change_status'
        get :handle
      end
    end
    resources :test_plans do
      post :delete_parameter, :on => :collection
      post :delete_test_case, :on => :collection
      post :update_test_case, :on => :collection
      post :create_test_case, :on => :collection
      post :add_update_parameter, :on => :collection
      member do
        get :handle
        get :clone
        get :change_status
        get :approved
      end
    end
    resources :test_types do
      member do
        get :handle
        post :remove
        post :restore
        post :deactivate
        post :activate
      end
    end
    resources :text_blocks do
      member do
        get :handle
        post :change_status
      end
    end
    resources :users do
      get :generate_users_ldap, :on => :collection
      get :save_new_role, :on => :collection
      post 'save_new_role', :on => :collection
      post 'save_new_event', :on => :collection
      post 'save_user_mail_event', :on => :collection
      get 'delete_user_mail', :on => :collection
      member do
        get :exit_user
        post :delete_user_role
        post :add_user_role
        post :delete_user_mail
        post :add_user_mail
        get :edit_user_mail_event
        get :new_event
        get :user_call
      end
    end
    resources :validation_errors
    resources :validation_jobs do
      get :failures, :on => :collection
      post :tagging_fail_reason, :on => :collection
      member do
        post :cancel
        post :clean_working_dir
        post :edit_new_copy
        post :reschedule
        get :cancel
        post :generated_script
      end
    end
    #resources :warehouse_stock => 'listing', :singular => :warehouse_stock_instance
    resources :warehouse_stock do
      get :onhand, :on => :collection
      post :onhand, :on => :collection
    end
    resources :warehouse_stock_statuses
    #resources.shipping_order_call_of "shipping_order_call_off_list", :controller => 'shipping_orderlines', :action => "call_of_list", :method => "get", :id => 1
    get "shipping_order_call_off_list" => 'shipping_orderlines#call_of_list'
    get "main/logistics" => 'main#logistics'
    
    get "login" => 'login#index'

    post "login/do_login" => 'login#do_login'
    get "logout" => 'logout#index'
    resources :update_regions do
      get :generate_dakota_id, :on => :collection
      member do
      end
    end


    #product designer stuff
    resources :product_feature_versions

    resources :executable_versions

    resources :product_design_versions

    resources :parameter_setting_versions

    resources :parameter_settings do
      member do
        get "handle"
        get "approved"
        post "draft"
        post "review"
        post "approve"
        post "approve_directly"
        post "obsolete"
        post "revert"
        post "sync"
        get  "sync"
        get "render_settings"
      end
    end

    resources :design_tags

    resources :executables do
      member do
        get "handle"
        get "approved"
        post "draft"
        post "review"
        post "approve"
        post "approve_directly"
        post "obsolete"
        post "revert"
        post "find_executable"
      end
    end
    resources :product_features do
      get :render_exe_as_json, :on => :collection
      member do
        get "handle"
        get "approved"
        post "draft"
        post "review"
        post "approve"
        post "approve_directly"
        post "obsolete"
        post "revert"
      end
    end



    get 'product_designs/get_relevent_design_tags' => 'product_designs#get_relevant_design_tags'
    get 'product_designs/get_geting_features' => 'product_designs#get_geting_features'
    resources :product_designs do
      get :get_datareleases, :on => :collection
      get :get_matching_features, :on => :collection
      post :render_target_filling, :on => :collection
      member do
        get "handle"
        get "approved"
        post "draft"
        get "clone_it"
        get "show_diff"
        post "review"
        post "approve"
        post "approve_directly"
        post "obsolete"
        post "revert"
        post "add_conversion_step"
        post "import_design"
        post "get_geting_receptions"
        post "sync_model"
        get "edit_model"
        post "syncsubscript"
        post "create_production_order"
        get "html2pdf_show"
      end
    end

    resources :api do
     member do
      get "load"  
      post "status"
      post "execute_job"                              
      post "can_run_job"
      post "async_step"                                     
     end
    end
    resources :chosen
    resources :test_tool_releases do
      post :sync, :on => :collection
      member do
        post :toggle_isactive
      end
    end


    # The priority is based upon order of creation:
    # first created -> highest priority.

    # Sample of regular route:
#    get 'product_designs/allocate_by_design/:id' => 'product_designs#allocate_by_design'
    get  'product_designs/render_model/:id' => 'product_designs#render_model'
    post 'product_designs/render_model/:id' => 'product_designs#render_model'
    get 'product_designs/update_model/:id' => 'product_designs#update_model'
    get 'product_designs/edit_model/:id' => 'product_designs#edit_model'
#    get 'product_designs/recalculate_compiler_steps/:id' => 'product_designs#recalculate_compiler_steps'
    get 'product_designs/fetch_assembly_lines/:id' => 'product_designs#fetch_assembly_lines'
    get 'executables/add_datatype' => 'executables#add_datatype'

    get 'product_designs/get_file_resources' => 'product_designs#get_file_resources'
    get 'product_designs/get_files' => 'product_designs#get_files'

    get 'product_features/render_system_translation/:id' => 'product_features#render_system_translation'

    get 'customer_content_values/render_content/:id' => 'customer_content_values#render_content'
    get 'customer_contents/render_value/:id' => 'customer_contents#render_value'
  end

  scope '/api' do
   get 'executables/synchronize_tool', to: 'executables#synchronize_tool_stub'
   get 'executables/synchronize_bundle', to: 'executables#synchronize_bundle_stub'
   post 'executables/synchronize_tool'
   post 'executables/synchronize_bundle'

  get 'data_types/index', to: 'data_types#api_index'
  get 'data_types/', to: 'data_types#api_index'

  resources :company_abbrs do
    get :get_supplier_name_id,:on => :collection
    get :dakota_supplier_ids, :on => :collection
  end

  resources :conversion_databases do
    get :exist, :on => :collection
    get :get_details, :on => :collection
  end

  get 'product_designs/index', to: 'product_designs#api_index'
  get 'product_designs/', to: 'product_designs#api_index'
  #resources :product_designs do
  #    get :template, :on => :collection
  #    get :sub_template, :on => :collection
  #end

  resources :regions do
    get :get_region_id, :on => :collection
    get :dakota_region_ids, :on => :collection
    get :dakota_names, :on => :collection
    get :test_areas, :on => :collection
  end

   post 'asset_types/get_document_id', to: 'asset_types#get_document_id'
   get 'products/get_product_details', to: 'products#get_product_details'
  end
end
