env :PATH, '/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin'
job_type :rake, "{ cd #{@current_path} > /dev/null; } && RAILS_ENV=:environment bundle exec rake :task --silent :output"
job_type :script, "{ cd #{@current_path} > /dev/null; } && RAILS_ENV=:environment bundle exec script/:task :output"
job_type :scriptcmd, "{ cd #{@current_path} > /dev/null; } && script/:task :output"
puts "ENV:" + @environment
if @environment == 'development'
 @current_path=`pwd`.chomp!
 @rvmpath = `rvm env --path -- ruby-1.9.3@webmis_r3`
 @rvmpath.chomp!
 job_type :runner, "{ cd #{@current_path} > /dev/null; } && source #{@rvmpath} && RAILS_ENV=:environment nice -19 bundle exec rails runner ':task' :output"
else
 job_type :runner, "{ cd #{@current_path} > /dev/null; } && RAILS_ENV=:environment nice -19 bundle exec rails runner ':task' :output"
end


every 1.minutes do
    runner "ProductionOrder.perform", :output => 'log/workflow.log'
    runner "ProductionOrder.schedule", :output => 'log/scheduler.log'
    scriptcmd "watchdog" , :output => 'log/watchdog.log'
end

every 24.hours do
    rake "ts:index"
end

every 5.minutes do
  scriptcmd "start_dj_if_not_running.sh" , :output => 'log/djrunningcheck.log'
end

every 12.hours do
    runner "OutboundOrder.flag_related_databases_removed", :output => 'log/flag_removal.log'
    runner "Conversiontool.sync_with_filesystem", :output => 'log/sync_bundles.log'
    runner "TestToolRelease.sync_with_filesystem", :output => 'log/sync_test_tools.log'
end

every 24.hours do
    runner "ProductionOrder.expire", :output => 'log/expiry.log'
    runner "ParameterSetting.generate_convscript(1)", :output => 'log/webmisscripts.log'
end

every 1.day, :at => '03:00 am' do
  runner "ConversionDatabase.remove_all_obsolete!", :output => 'log/remove_obsolete.log'
end



