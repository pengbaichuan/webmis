class CreateSchedulerjobs < ActiveRecord::Migration
  def self.up
    create_table :scheduler_jobs do |t|
      t.string :name
      t.string :schedule
      t.string :processor_type
      t.integer :status
      t.timestamp :date_last_run
      t.string :dataprocessor_id
      t.string :datasource_id
      t.timestamps
    end
  end

  def self.down
    drop_table :jobs
  end
end
