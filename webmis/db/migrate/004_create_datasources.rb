class CreateDatasources < ActiveRecord::Migration
  def self.up
    create_table :datasources do |t|
      t.string :location
      t.text :script
      t.integer :status
      t.timestamps
    end
  end

  def self.down
    drop_table :datasources
  end
end
