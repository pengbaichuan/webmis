class Addjobtodatasource < ActiveRecord::Migration
  def self.up
     add_column :datasources, :scheduler_job_id, :integer
  end

  def self.down
    remove_column :datasources, :scheduler_job_id
  end
end
