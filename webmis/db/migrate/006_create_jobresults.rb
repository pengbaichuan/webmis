class CreateJobresults < ActiveRecord::Migration
  def self.up
    create_table :jobresults do |t|
      t.text :raw_data
      t.text :converted_data
      t.integer :job_id
      t.integer :status

      t.timestamps
    end
  end

  def self.down
    drop_table :jobresults
  end
end
