class AddProcessingtime < ActiveRecord::Migration
  def self.up
     add_column :jobresults, :processing_time, :float
  end

  def self.down
     remove_column :jobresults, :processing_time
  end
end
