class AddInternalJobid < ActiveRecord::Migration
  def self.up
     add_column :scheduler_jobs, :scheduler_id, :integer
  end

  def self.down
     remove_column :scheduler_jobs, :scheduler_id
  end
end
