class AddNameToDatasource < ActiveRecord::Migration
 def self.up
     add_column :datasources, :name, :string
  end

  def self.down
     remove_column :datasources, :name
  end
end
