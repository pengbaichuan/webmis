class AddTypeDatasource < ActiveRecord::Migration
 def self.up
     add_column :datasources, :processor_type, :string
  end

  def self.down
     remove_column :datasources, :processor_type
  end
end
