class AddSizeHashToJobresults < ActiveRecord::Migration
  def self.up
     add_column :jobresults, :size, :integer
     add_column :jobresults, :hashcode, :integer
     
  end

  def self.down
     remove_column :jobresults, :size
     remove_column :jobresults, :hashcode
  end
end
