class CreatePoints < ActiveRecord::Migration
  def self.up
    create_table :points do |t|
      t.string :name_of_facility
      t.string :iso_language_code
      t.string :category
      t.float :absolute_xcoordinate
      t.float :absolute_ycoordinate
      t.integer :importance
      t.string :brandname
      t.string :phone_number
      t.string :postal_code
      t.string :house_number
      t.string :street_name
      t.string :city_name
      t.string :country_name
      t.string :state_name
      t.string :vanity_city_name
      t.float :vanity_city_xcoordinate
      t.float :vanity_city_ycoordinate
      t.string :datasource
      t.integer :status
      t.timestamps
    end
  end

  def self.down
    drop_table :points
  end
end
