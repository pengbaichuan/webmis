class CreateLanguages < ActiveRecord::Migration
  def self.up
    create_table :languages do |t|
      t.string :code
      t.string :part2b
      t.string :part2t
      t.string :part1
      t.string :scope
      t.string :lang_type
      t.string :ref_name
      t.string :comment

      t.timestamps
    end
  end

  def self.down
    drop_table :languages
  end
end
