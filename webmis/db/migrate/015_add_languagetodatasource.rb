class AddLanguagetodatasource < ActiveRecord::Migration
  def self.up
    add_column :datasources, :language_id , :integer
  end

  def self.down
    remove_column :datasources, :language_id
  end
end
