class Addprocesslogtopoint < ActiveRecord::Migration
  def self.up
    add_column :points,:process_log,:string
  end

  def self.down
    remove_column :points,:process_log
  end
end
