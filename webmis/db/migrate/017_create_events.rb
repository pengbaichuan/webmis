class CreateEvents < ActiveRecord::Migration
  def self.up
    create_table :events do |t|
      t.string :message
      t.integer :datasource_id
      t.integer :point_id
      t.integer :scheduler_job_id
      t.text :trace
      t.string :level
      t.string :category
      t.string :user

      t.timestamps
    end
  end

  def self.down
    drop_table :events
  end
end
