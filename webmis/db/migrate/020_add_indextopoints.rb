class AddIndextopoints < ActiveRecord::Migration
  def self.up
    add_index :points, [:name_of_facility,:datasource], :unique
  end

  def self.down
    remove_index :points, [:name_of_facility,:datasource]
  end
end
