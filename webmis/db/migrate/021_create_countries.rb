class CreateCountries < ActiveRecord::Migration
  def self.up
    create_table :countries do |t|
      t.string :iso
      t.string :name
      t.string :printable_name
      t.string :iso3
      t.integer :num_code
      t.timestamps
    end
    add_index :countries , :name, :unique
  end

  def self.down
    drop_table :countries
    #remove_index :countries, :name
  end
end
