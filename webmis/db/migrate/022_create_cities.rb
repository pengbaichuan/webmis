class CreateCities < ActiveRecord::Migration
  def self.up
    create_table :cities do |t|
      t.string :iso2
      t.string :ascii_name
      t.string :city_name
      t.string :state_region
      t.integer :population
      t.float :latitude
      t.float :longtitude

      t.timestamps
    end
  end

  def self.down
    drop_table :cities
  end
end
