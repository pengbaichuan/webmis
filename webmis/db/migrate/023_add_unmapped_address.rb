class AddUnmappedAddress < ActiveRecord::Migration
  def self.up
    add_column :points,:unmapped_address,:string
  end

  def self.down
    remove_column :points,:unmapped_address
  end
end
