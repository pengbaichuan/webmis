class AddIndexesToCityAndState < ActiveRecord::Migration
  def self.up
    add_index :countries, [:name,:iso] 
    add_index :cities, [:city_name,:iso2]
     add_index :states, [:name,:country_code]
  end

  def self.down
     remove_index :countries, [:name,:iso] 
     remove_index :cities, [:city_name,:iso2]
     remove_index :states, [:name,:country_code]
  end
end
