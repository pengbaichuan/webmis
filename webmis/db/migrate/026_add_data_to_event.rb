class AddDataToEvent < ActiveRecord::Migration
  def self.up
     add_column :events,:related_data,:text
  end

  def self.down
    remove_column :events,:related_data
  end
end
