class AddDeeplinkToPoint < ActiveRecord::Migration
  def self.up
     add_column :points,:deeplink,:string
  end
 

  def self.down
    remove_column :points,:deeplink
  end
end
