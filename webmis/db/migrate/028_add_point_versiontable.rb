class AddPointVersiontable < ActiveRecord::Migration
  def self.up
    create_table :point_versions do |t|
      t.integer :point_id
      t.string :name_of_facility
      t.string :iso_language_code
      t.string :category
      t.float :absolute_xcoordinate
      t.float :absolute_ycoordinate
      t.integer :importance
      t.string :brandname
      t.string :phone_number
      t.string :postal_code
      t.string :house_number
      t.string :street_name
      t.string :city_name
      t.string :country_name
      t.string :state_name
      t.string :vanity_city_name
      t.float :vanity_city_xcoordinate
      t.float :vanity_city_ycoordinate
      t.string :datasource
      t.integer :status
      t.timestamps
      t.string :process_log
      t.string :unmapped_address
      t.string :deeplink
      t.integer :version
    end
      
    add_column :points, :version, :integer
    #add_column :points, :lock_version, :integer
  end

  def self.down
    drop_table :point_versions
    remove_column :points,:version
    #remove_column :points, :lock_version
  end
end
