class AddDateCheckedToPoints < ActiveRecord::Migration
  def self.up
     add_column :points,:checked_at,:timestamp
     add_column :point_versions,:checked_at,:timestamp
  end

  def self.down
    remove_column :points,:checked_at
    remove_column :point_versions,:checked_at,:timestamp
  end
end
