class AddIndexToEvents1 < ActiveRecord::Migration
  def self.up
    add_index :events, [:created_at]
  end

  def self.down
    remove_index :events, [:created_at]
  end
end
