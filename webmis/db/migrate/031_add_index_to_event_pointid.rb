class AddIndexToEventPointid < ActiveRecord::Migration
  def self.up
    add_index :events, [:point_id]
  end

  def self.down
    remove_index :events, [:point_id]
  end
end
