class CreateRoutes < ActiveRecord::Migration
  def self.up
    create_table :routes do |t|
      t.string :name_of_route
      t.string :description
      t.text :coordinates
      t.text :raw_data

      t.timestamps
    end
  end

  def self.down
    drop_table :routes
  end
end
