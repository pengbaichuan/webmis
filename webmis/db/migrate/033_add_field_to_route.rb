class AddFieldToRoute < ActiveRecord::Migration
  def self.up
    add_column :routes,:iso_language_code,:string
    add_column :routes,:datasource,:string
    add_column :routes,:version,:integer
    add_column :routes,:deeplink,:string
    add_column :routes,:checked_at,:timestamp
  end

  def self.down
    remove_column :routes,:iso_language_code
    remove_column :routes,:datasource
    remove_column :routes,:version
    remove_column :routes,:deeplink
    remove_column :routes,:checked_at
  end
end
