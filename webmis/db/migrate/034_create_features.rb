class CreateFeatures < ActiveRecord::Migration
  def self.up
    create_table :features do |t|
      t.string :name
      t.integer :code_navteq
      t.integer :code_teleatlas
      t.integer :cen_gdf_code
      t.integer :svf_code
      t.string :feature_type
      t.integer :version

      t.timestamps
    end
  end

  def self.down
    drop_table :features
  end
end
