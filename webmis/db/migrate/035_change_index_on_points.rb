class ChangeIndexOnPoints < ActiveRecord::Migration
  def self.up
    #remove_index :points, [:name_of_facility,:datasource]
   execute  "CREATE INDEX `index_points_on_name_and_datasource_and_street_and_house_nr` ON `points` (`datasource` ,`country_name` ,`city_name`, `street_name`)"

  end

  def self.down
    execute  "drop INDEX `index_points_on_name_and_datasource_and_street_and_house_nr` ON `points`"
  end
end
