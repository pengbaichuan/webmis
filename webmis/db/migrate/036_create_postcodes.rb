class CreatePostcodes < ActiveRecord::Migration
  def self.up
    create_table :postcodes do |t|
      t.string :code
      t.string :country_code
      t.string :city_name
      t.string :street_name
      t.string :region
      t.timestamps
    end
  end

  def self.down
    drop_table :postcodes
  end
end
