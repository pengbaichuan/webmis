class CreateIndexPointidOnPointVersions < ActiveRecord::Migration
  def self.up
     add_index :point_versions, [:point_id]
  end

  def self.down
    remove_index :point_versions, [:point_id]
  end
end
