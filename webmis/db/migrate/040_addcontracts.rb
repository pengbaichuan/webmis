class Addcontracts < ActiveRecord::Migration
  def self.up
    create_table :contracts do |t|
      t.date :startdate
      t.date :enddate
      t.string :path
      t.timestamps
    end
  end

  def self.down
    drop_table :contracts
  end
end
