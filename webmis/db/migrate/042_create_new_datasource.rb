class CreateNewDatasource < ActiveRecord::Migration
  def self.up
    create_table :datasources do |t|
      t.string :name
      t.string :note
      t.integer :company_id
      t.timestamps
    end
  end

  def self.down
    drop_table :datasources
  end
end
