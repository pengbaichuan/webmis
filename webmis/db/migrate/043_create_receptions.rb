class CreateReceptions < ActiveRecord::Migration
  def self.up
    create_table :receptions do |t|
      t.integer :datasource_id
      t.integer :data_release_id
      t.integer :medium_id
      t.string :dataversion
      t.string :area_id
      t.string :data_type_id
      t.string :referenceid
      t.date :reception_date
      t.string :purpose
      t.text :remarks
      t.text :storage_location

      t.timestamps
    end
  end

  def self.down
    drop_table :receptions
  end
end
