class CreateDataReleases < ActiveRecord::Migration
  def self.up
    create_table :data_releases do |t|
      t.string :name
      t.string :release_code
      t.integer :datasource_id

      t.timestamps
    end
  end

  def self.down
    drop_table :data_releases
  end
end
