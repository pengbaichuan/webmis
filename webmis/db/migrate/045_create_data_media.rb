class CreateDataMedia < ActiveRecord::Migration
  def self.up
    create_table :data_media do |t|
      t.string :name
      t.integer :capacity
      t.string :unit_of_measure

      t.timestamps
    end
  end

  def self.down
    drop_table :data_media
  end
end
