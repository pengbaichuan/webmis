class ChangeMediumColumnOnReception < ActiveRecord::Migration
  def self.up
		rename_column("receptions","medium_id","data_medium_id")
  end

  def self.down
		rename_column("receptions","data_medium_id","medium_id")
  end
end
