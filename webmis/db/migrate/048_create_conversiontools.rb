class CreateConversiontools < ActiveRecord::Migration
  def self.up
    create_table :conversiontools do |t|
      t.string :code
      t.string :description
      t.boolean :isactive

      t.timestamps
    end
  end

  def self.down
    drop_table :conversiontools
  end
end
