class AddFieldsToDatatype < ActiveRecord::Migration
  def self.up
 add_column(:data_types, :isactive, :boolean)
 add_column(:data_types, :description, :string)
  end

  def self.down
 remove_column(:data_types, :isactive, :boolean)
 remove_column(:data_types, :description, :string)
  end
end
