class CreateNavdbfeatureTypes < ActiveRecord::Migration
  def self.up
    create_table :navdbfeature_types do |t|
      t.string :name
      t.string :description
      t.integer :data_type_id
      t.integer :navdbfeature_category_id
      t.boolean :isactive

      t.timestamps
    end
  end

  def self.down
    drop_table :navdbfeature_types
  end
end
