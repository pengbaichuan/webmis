class CreateNavdbfeatureCategories < ActiveRecord::Migration
  def self.up
    create_table :navdbfeature_categories do |t|
      t.string :name
      t.string :description
      t.boolean :isactive

      t.timestamps
    end
  end

  def self.down
    drop_table :navdbfeature_categories
  end
end
