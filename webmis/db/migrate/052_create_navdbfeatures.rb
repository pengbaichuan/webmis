class CreateNavdbfeatures < ActiveRecord::Migration
  def self.up
    create_table :navdbfeatures do |t|
      t.integer :navdb_feature_type_id
      t.timestamp :dateeffective
      t.string :description1
      t.string :description2
      t.string :value
      t.string :remarks

      t.timestamps
    end
  end

  def self.down
    drop_table :navdbfeatures
  end
end
