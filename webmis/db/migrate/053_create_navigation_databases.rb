class CreateNavigationDatabases < ActiveRecord::Migration
  def self.up
    create_table :navigation_databases do |t|
      t.string :name
      t.integer :supplier_id
      t.integer :customer_id
      t.string :tag
      t.integer :major
      t.integer :minor
      t.timestamp :dateeffective
      t.string :changecomment
      t.string :remarks
      t.timestamps
    end
  end

  def self.down
    drop_table :navigation_databases
  end
end
