class CreateProducts < ActiveRecord::Migration
  def self.up
    create_table :products do |t|
      t.string :name
      t.string :volumeid
      t.timestamp :dateeffective
      t.string :detailedcoverage
      t.string :hwncoverage
      t.string :thirdpartydata1
      t.string :thirdpartydata2
      t.string :tmclocation
      t.string :tmcevent
      t.string :remarks

      t.timestamps
    end
  end

  def self.down
    drop_table :products
  end
end
