class CreateProductsets < ActiveRecord::Migration
  def self.up
    create_table :productsets do |t|
      t.string :name
      t.integer :continent_id
      t.integer :supplier_id
      t.integer :customer_id
      t.integer :firsttier_id
      t.string :state
      t.integer :major
      t.integer :minor
      t.timestamp :dateeffective
      t.string :changes
      t.string :remarks

      t.timestamps
    end
  end

  def self.down
    drop_table :productsets
  end
end
