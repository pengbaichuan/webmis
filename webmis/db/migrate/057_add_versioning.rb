class AddVersioning < ActiveRecord::Migration
  def self.up
 add_column(:navdbfeatures, :version, :integer)
 Navdbfeature.create_versioned_table
 NavigationDatabase.create_versioned_table
end

  def self.down
 remove_column(:navdbfeatures, :version)
 Navdbfeature.drop_versioned_table
 remove_column(:navigation_databases, :version)
 NavigationDatabase.drop_versioned_table
  end
end
