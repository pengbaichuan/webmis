class CreateInternalContacts < ActiveRecord::Migration
  def self.up
    create_table :internal_contacts do |t|
      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.string :phone
      t.string :email
      t.string :job_title
      t.timestamps
    end
  end

  def self.down
    drop_table :internal_contacts
  end
end
