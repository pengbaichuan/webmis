class AddRoles < ActiveRecord::Migration
  def self.up
    create_table :roles do |t|
      t.integer :datasource_id, :internal_contact_id
      t.string :role
      t.timestamps 
    end
  end

  def self.down
    drop_table :roles
  end
end
