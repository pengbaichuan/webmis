class AddVersioning < ActiveRecord::Migration
  def self.up
 Product.create_versioned_table
 Productset.create_versioned_table
end

  def self.down
 Product.drop_versioned_table
 Productset.drop_versioned_table
  end
end
