class CreateInboundOrders < ActiveRecord::Migration
  def self.up
    create_table :inbound_orders do |t|
      t.integer :supplier_id
      t.timestamp :arrival_timestamp
      t.string :tracking_nr

      t.timestamps
    end
  end

  def self.down
    drop_table :inbound_orders
  end
end
