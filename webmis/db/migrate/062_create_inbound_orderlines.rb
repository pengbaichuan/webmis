class CreateInboundOrderlines < ActiveRecord::Migration
  def self.up
    create_table :inbound_orderlines do |t|
      t.string :drfmsid
      t.string :mediatype
      t.string :area
      t.string :dbrelease
      t.string :deliveryformat
      t.string :dbversion
      t.integer :inbound_order_id

      t.timestamps
    end
  end

  def self.down
    drop_table :inbound_orderlines
  end
end
