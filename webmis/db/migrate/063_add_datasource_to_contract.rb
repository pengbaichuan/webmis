class AddDatasourceToContract < ActiveRecord::Migration
  def self.up
    add_column :contracts, :datasource_id, :integer
  end

  def self.down
    remove_column :contracts, :datasource_id
  end
end
