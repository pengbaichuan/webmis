class AddReferencesToInboundOrder < ActiveRecord::Migration
  def self.up
    add_column :inbound_orders, :orderref, :string
    add_column :inbound_orders, :ordersubref, :string
  end

  def self.down
    remove_column :inbound_orders, :orderref
    remove_column :inbound_orders, :ordersubref
  end
end
