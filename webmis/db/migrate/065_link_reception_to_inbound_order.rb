class LinkReceptionToInboundOrder < ActiveRecord::Migration
  def self.up
   add_column :receptions, :inbound_order_id, :integer
  end

  def self.down
   remove_column :receptions, :inbound_order_id
  end
end
