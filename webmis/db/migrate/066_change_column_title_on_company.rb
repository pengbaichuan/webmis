class ChangeColumnTitleOnCompany < ActiveRecord::Migration
  def self.up
   rename_column :companies, :title, :name
  end

  def self.down
   rename_column :companies, :name, :title
  end
end
