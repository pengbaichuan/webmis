class CreateReceptionLines < ActiveRecord::Migration
  def self.up
    create_table :reception_lines do |t|
      t.string :path
      t.string :file
      t.integer :reception_id

      t.timestamps
    end
  end

  def self.down
    drop_table :reception_lines
  end
end
