class CreateConversions < ActiveRecord::Migration
  def self.up
    create_table :conversions do |t|
      t.string :request_by
      t.date :request_date
      t.date :conversion_date
      t.string :priority
      t.integer :supplier_id
      t.integer :datasource_id
      t.integer :data_release_id
      t.string :product
      t.string :productrelease
      t.string :cvtool
      t.integer :template_id
      t.text :additional_info
      t.boolean :gsm
      t.string :gsmfile
      t.string :request_data
      t.string :converter
      t.string :poifile
      t.boolean :tdp
      t.text :data_files
      t.text :reference_numbers
      t.text :remarks
      t.boolean :right_data_check
      t.boolean :dealers_check
      t.boolean :cvtool_check
      t.string :outcome
      t.string :cvtoolenv
      t.string :size_bytes
      t.string :size_mb
      t.string :dealer_name
      t.string :database_name
      t.string :productid
      t.string :carribatool
      t.string :reception_reference
      t.string :result

      t.timestamps
    end
  end

  def self.down
    drop_table :conversions
  end
end
