class CreateCarribatools < ActiveRecord::Migration
  def self.up
    create_table :carribatools do |t|
      t.string :name
      t.string :description
      t.string :release

      t.timestamps
    end
  end

  def self.down
    drop_table :carribatools
  end
end
