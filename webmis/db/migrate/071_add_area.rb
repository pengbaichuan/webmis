class AddArea < ActiveRecord::Migration
  def self.up
    add_column :datasources, :area_id, :integer
  end

  def self.down
    remove_column :datasources, :area_id
  end
end
