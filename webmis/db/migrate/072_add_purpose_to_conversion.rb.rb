class AddPurposeToConversion < ActiveRecord::Migration
  def self.up
   add_column :conversions, :purpose, :string
  end

  def self.down
   remove_column :conversions, :purpose
  end
end
