class AddTypeToConversion < ActiveRecord::Migration
  def self.up
    add_column :conversions,:type,:string
  end

  def self.down
    remove_column :conversions,:type
  end
end
