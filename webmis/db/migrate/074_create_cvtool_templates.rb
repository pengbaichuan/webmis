class CreateCvtoolTemplates < ActiveRecord::Migration
  def self.up
    create_table :cvtool_templates do |t|
      t.string :filename
      t.string :description
      t.boolean :isactive

      t.timestamps
    end
  end

  def self.down
    drop_table :cvtool_templates
  end
end
