class AddFieldsToConversion < ActiveRecord::Migration
  def self.up
   add_column :conversions, :rdstmc, :boolean
   add_column:conversions, :rdstmcfile, :boolean
   add_column :conversions, :ca_brn, :string
   add_column :conversions, :tpdfile, :string
   add_column :conversions, :gsm_check, :boolean
   add_column :conversions, :tmc_check, :boolean
   rename_column :conversions, :tdp, :tpd
  end

  def self.down
   remove_column :conversions, :rdstmc
   remove_column :conversions, :rdstmcfile
   remove_column :conversions, :ca_brn
   remove_column :conversions, :tpdfile
   remove_column :conversions, :gsm_check
   remove_column :conversions, :tmc_check
   rename_column :conversions, :tdp, :tpd
  end
end
