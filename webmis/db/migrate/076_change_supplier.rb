class ChangeSupplier < ActiveRecord::Migration
  def self.up
    change_column(:areas, :supplier_id, :string)
    rename_column(:datasources, :company_id, :supplier_id)
    change_column(:datasources, :supplier_id, :string)
    change_column(:conversions, :supplier_id, :string)
    add_column(:products, :supplier_id, :string)
  end

  def self.down
  end
end
