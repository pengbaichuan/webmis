class AddToInboundOrderline < ActiveRecord::Migration
  def self.up
   add_column :inbound_orderlines, :arrival_date, :date
   add_column :inbound_orderlines, :data_type_id, :integer
   add_column :inbound_orderlines, :data_release_id, :integer
  end

  def self.down
  end
end
