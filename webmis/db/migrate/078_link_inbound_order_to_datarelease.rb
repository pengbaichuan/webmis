class LinkInboundOrderToDatarelease < ActiveRecord::Migration
  def self.up
    add_column :inbound_orders, :data_release_id, :integer
  end

  def self.down
    remove_column :inbound_orders, :data_release_id
  end
end
