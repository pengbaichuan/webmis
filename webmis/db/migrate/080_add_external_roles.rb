class AddExternalRoles < ActiveRecord::Migration
  def self.up
    create_table :external_roles do |t|
      t.integer :datasource_id
      t.integer :external_contact_id
      t.string :role
    end
  end

  def self.down
    drop_table :external_roles
  end
end
