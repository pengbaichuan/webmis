class ChangeExternalContactId < ActiveRecord::Migration
  def self.up
    change_column(:external_roles, :external_contact_id, :string)
  end

  def self.down
  end
end
