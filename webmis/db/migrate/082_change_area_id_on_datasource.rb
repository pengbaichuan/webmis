class ChangeAreaIdOnDatasource < ActiveRecord::Migration
  def self.up
   change_column :receptions, :area_id, :integer
  end

  def self.down
   change_column :receptions, :area_id, :string
  end
end
