class AddActive < ActiveRecord::Migration
  def self.up
    add_column :data_releases, :active, :boolean
  end

  def self.down
  end
end
