class AddTpdfieldsToConversions < ActiveRecord::Migration
  def self.up
   add_column :conversions, :tpd_products, :text
   add_column :conversions, :tpd_rejected, :text
   add_column :conversions, :tpd_rejection_remark, :text
   add_column :conversions, :tpd_result_sender, :string
  end

  def self.down
   remove_column :conversions, :tpd_products
   remove_column :conversions, :tpd_rejected
   remove_column :conversions, :tpd_rejection_remark
   removr_column :conversions, :tpd_result_sender
  end
end
