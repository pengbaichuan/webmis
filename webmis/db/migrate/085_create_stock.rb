class CreateStock < ActiveRecord::Migration
  def self.up
    create_table :stock do |t|
      t.date :creation_date
      t.string :commercial_name
      t.string :key_type
      t.integer :supplier_id
      t.string :supplier_name
      t.string :cd_type
      t.integer :product_id
      t.string :product_name
      t.integer :data_release_id
      t.string :product_ref
      t.string :brn
      t.boolean :tpd_phase_1
      t.text :tpd_phase_1_description
      t.boolean :tpd_phase_2
      t.string :tpd_phase_2_description
      t.boolean :tpd_iba
      t.text :tpd_iba_description
      t.integer :cv_template_id
      t.string :cd_template_name
      t.string :databasename
      t.text :extra
      t.boolean :hwn
      t.boolean :gsm
      t.boolean :tmc
      t.string :location
      t.text :rds_tmcfile

      t.timestamps
    end
  end

  def self.down
    drop_table :stock
  end
end
