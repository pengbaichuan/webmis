class CreateLocations < ActiveRecord::Migration
  def self.up
    create_table :locations do |t|
      t.string :location_code
      t.string :reference
      t.integer :cd_no
      t.integer :cd_type
      t.string :borrby
      t.string :borron
      t.string :borrback

      t.timestamps
    end
  end

  def self.down
    drop_table :locations
  end
end
