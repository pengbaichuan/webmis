class AddCopyright < ActiveRecord::Migration
  def self.up
    add_column :datasources, :copyright, :string
  end

  def self.down
  end
end
