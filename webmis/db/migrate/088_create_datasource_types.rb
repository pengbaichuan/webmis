class CreateDatasourceTypes < ActiveRecord::Migration
  def self.up
    create_table :datasource_types do |t|
      t.column :name, :string
      t.timestamps
    end
  end

  def self.down
    drop_table :datasource_types
  end
end
