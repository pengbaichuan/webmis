class AddDatasourceTypeToDatasource < ActiveRecord::Migration
  def self.up
    add_column :datasources, :datasource_type, :integer
  end

  def self.down
    remove_column :datasources, :datasource_type
  end
end
