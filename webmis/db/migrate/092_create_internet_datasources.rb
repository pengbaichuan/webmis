class CreateInternetDatasources < ActiveRecord::Migration
  def self.up
    create_table :internet_datasources do |t|

      t.timestamps
    end
  end

  def self.down
    drop_table :internet_datasources
  end
end
