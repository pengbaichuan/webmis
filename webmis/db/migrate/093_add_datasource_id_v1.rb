class AddDatasourceId < ActiveRecord::Migration
  def self.up
    add_column :internet_datasources, :datasource_id, :integer
  end

  def self.down
    remove_column :internet_datasources, :datasource_id
  end
end
