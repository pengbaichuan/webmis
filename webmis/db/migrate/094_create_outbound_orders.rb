class CreateOutboundOrders < ActiveRecord::Migration
  def self.up
    create_table :outbound_orders do |t|
      t.integer :company_id
      t.date :send_data
      t.string :contact_person
      t.string :shipto_name
      t.string :shipto_postalcode
      t.string :shipto_streetname
      t.string :shipto_country
      t.string :send_by
      t.string :map_no
      t.string :cd_no
      t.string :type
      t.text :extra
      t.string :amount
      t.string :filename
      t.string :from_name
      t.string :remarks
      t.string :courier
      t.boolean :printed_alphatest_delivery
      t.boolean :printed_test_delivery
      t.boolean :printed_master_shipment
      t.boolean :printed_copy_master_shipment
      t.boolean :printed_release_notification
      t.boolean :printed_master_shipment_info
      t.boolean :printed_sample_approval
      t.boolean :printed_release_notification_info

      t.timestamps
    end
  end

  def self.down
    drop_table :outbound_orders
  end
end
