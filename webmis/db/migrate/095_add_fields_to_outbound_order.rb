class AddFieldsToOutboundOrder < ActiveRecord::Migration
  def self.up
   add_column :outbound_orders, :shipto_state, :string
   add_column :outbound_orders, :shipto_city, :string
   rename_column :outbound_orders, :send_data, :send_date
  end

  def self.down
   add_column :outbound_orders, :shipto_state, :string
   add_column :outbound_orders, :shipto_city, :string
   rename_column :outbound_orders, :send_date, :send_data
  end
end
