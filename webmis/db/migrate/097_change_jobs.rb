class ChangeJobs < ActiveRecord::Migration
  def self.up
    change_column :scheduler_jobs, :datasource_id, :integer
  end

  def self.down
  end
end
