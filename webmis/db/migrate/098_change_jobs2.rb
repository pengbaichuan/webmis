class ChangeJobs2 < ActiveRecord::Migration
  def self.up
    rename_column :scheduler_jobs, :datasource_id, :internet_datasource_id
  end

  def self.down
  end
end
