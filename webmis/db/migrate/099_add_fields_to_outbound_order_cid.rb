class AddFieldsToOutboundOrderCid < ActiveRecord::Migration
  def self.up
   change_column :outbound_orders, :company_id, :string
  end

  def self.down
   change_column :outbound_orders, :company_id, :integer
  end
end
