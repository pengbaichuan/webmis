class AddPurpose < ActiveRecord::Migration
  def self.up
    add_column :datasources, :purpose, :string
  end

  def self.down
    remove_column :datasources, :purpose
  end
end
