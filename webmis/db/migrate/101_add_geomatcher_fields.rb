class AddGeomatcherFields < ActiveRecord::Migration
  def self.up
    add_column :points, :distance,:integer
    add_column :points, :db1_coord_x, :float
    add_column :points, :db1_coord_y, :float
    add_column :points, :db1_bestconflvl, :integer
    add_column :points, :db1_conflvl, :integer
    add_column :points, :db1_matchtech, :integer
    add_column :points, :db1_urbrur, :string
    add_column :points, :db1_diglvl, :string
    add_column :points, :db1_attrlvl, :string
    add_column :points, :db1_roadtype, :string
    add_column :points, :db1_matchlvl, :string
    add_column :points, :db1_dist, :string
    add_column :points, :db1_cou, :string
    add_column :points, :db1_citloc, :string
    add_column :points, :db1_cit, :string
    add_column :points, :db1_strloc, :string
    add_column :points, :db1_str, :string
    add_column :points, :db1_hnr, :string
    add_column :points, :db2_coord_x, :float
    add_column :points, :db2_coord_y, :float
    add_column :points, :db2_bestconflvl, :integer
    add_column :points, :db2_conflvl, :integer
    add_column :points, :db2_matchtech, :integer
    add_column :points, :db2_urbrur, :string
    add_column :points, :db2_diglvl, :string
    add_column :points, :db2_attrlvl, :string
    add_column :points, :db2_roadtype, :string
    add_column :points, :db2_matchlvl, :string
    add_column :points, :db2_dist, :string
    add_column :points, :db2_cou, :string
    add_column :points, :db2_citloc, :string
    add_column :points, :db2_cit, :string
    add_column :points, :db2_strloc, :string
    add_column :points, :db2_str, :string
    add_column :points, :db2_hnr, :string
  end

  def self.down
    remove_column :points, :distance
    remove_column :points, :db1_coord_x
    remove_column :points, :db1_coord_y
    remove_column :points, :db1_bestconflvl
    remove_column :points, :db1_conflvl 
    remove_column :points, :db1_matchtech
    remove_column :points, :db1_urbrur
    remove_column :points, :db1_diglvl
    remove_column :points, :db1_attrlvl
    remove_column :points, :db1_roadtype
    remove_column :points, :db1_matchlvl
    remove_column :points, :db1_dist
    remove_column :points, :db1_cou
    remove_column :points, :db1_citloc
    remove_column :points, :db1_cit
    remove_column :points, :db1_strloc
    remove_column :points, :db1_str
    remove_column :points, :db1_hnr
    remove_column :points, :db2_coord_x
    remove_column :points, :db2_coord_y
    remove_column :points, :db2_bestconflvl
    remove_column :points, :db2_conflvl
    remove_column :points, :db2_matchtech
    remove_column :points, :db2_urbrur
    remove_column :points, :db2_diglvl
    remove_column :points, :db2_attrlvl
    remove_column :points, :db2_roadtype
    remove_column :points, :db2_matchlvl
    remove_column :points, :db2_dist
    remove_column :points, :db2_cou
    remove_column :points, :db2_citloc
    remove_column :points, :db2_cit
    remove_column :points, :db2_strloc
    remove_column :points, :db2_str
    remove_column :points, :db2_hnr
  end
end
