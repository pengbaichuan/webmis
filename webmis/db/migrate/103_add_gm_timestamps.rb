class AddGmTimestamps < ActiveRecord::Migration
  def self.up
   add_column :points, :gm_extract_date, :timestamp
   add_column :points, :gm_upload_date, :timestamp
   add_column :point_versions, :gm_extract_date, :timestamp
   add_column :point_versions, :gm_upload_date, :timestamp
  end

  def self.down
   remove_column :points, :gm_extract_date
   remove_column :points, :gm_upload_date
   remove_column :point_versions, :gm_extract_date
   remove_column :point_versions, :gm_upload_date
  end
end
