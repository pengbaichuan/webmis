class ChangePointVersionMatchlvl < ActiveRecord::Migration
  def self.up
   change_column :point_versions,:db1_matchlvl, :integer
   change_column :point_versions,:db1_dist, :integer
   change_column :point_versions,:db2_matchlvl, :integer
   change_column :point_versions,:db2_dist, :integer
  end

  def self.down
   change_column :point_versions,:db1_matchlvl, :string
   change_column :point_versions,:db1_dist, :string
   change_column :point_versions,:db2_matchlvl, :string
   change_column :point_versions,:db2_dist, :string
  end
end
