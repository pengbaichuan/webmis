class AddDatasourceIdToPoints < ActiveRecord::Migration
  def self.up
    add_column :points, :internet_datasource_id, :integer
  end

  def self.down
  end
end
