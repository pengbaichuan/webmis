class AddAreatypeToReception < ActiveRecord::Migration
  def self.up
   add_column :receptions, :area_type, :string
  end

  def self.down
   remove_column :receptions, :area_type
  end
end
