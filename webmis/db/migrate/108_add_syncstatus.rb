class AddSyncstatus < ActiveRecord::Migration
  def self.up
    add_column :scheduler_jobs, :syncstatus, :integer
  end

  def self.down
  end
end
