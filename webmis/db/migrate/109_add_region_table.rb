class AddRegionTable < ActiveRecord::Migration
  def self.up
    create_table :regions do |t|
      t.column :name, :string
      t.column :description, :text
      t.column :population, :integer
      t.column :continent_code, :string
      t.column :country_iso_code, :string
      t.column :country_iso3_code, :string
      t.column :country_num_code, :string
      t.column :city_iso_code, :string
      t.column :city_ascii_name, :string
      t.column :state_code, :string
      t.column :latitude, :float
      t.column :longitude, :float
      t.column :isactive, :boolean
      t.column :area_code, :string
      t.column :supplier_id, :string
      t.column :display_type_id, :integer
      t.column :type, :string
    end
  end

  def self.down
     drop_table :regions
  end
end
