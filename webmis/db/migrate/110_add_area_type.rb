class AddAreaType < ActiveRecord::Migration
  def self.up
    create_table :area_types do |t|
      t.column :name, :string 
      t.column :code, :string
      t.column :description, :text
    end
  end

  def self.down
    drop_table :area_types
  end
end
