class AddDatasourceIdToVersions < ActiveRecord::Migration
  def self.up
    add_column :point_versions, :internet_datasource_id, :integer
  end

  def self.down
  end
end
