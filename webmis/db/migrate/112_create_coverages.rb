class CreateCoverages < ActiveRecord::Migration
  def self.up
    create_table :coverages do |t|
      t.string :name
      t.text :description
      t.text :remarks

      t.timestamps
    end
  end

  def self.down
    drop_table :coverages
  end
end
