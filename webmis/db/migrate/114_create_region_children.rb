class CreateRegionChildren < ActiveRecord::Migration
  def self.up
    create_table :region_relation do |t|
      t.integer :region_id
      t.integer :child_region_id
      t.string  :relation
    end
  end

  def self.down
  end
end
