class RegionRelations < ActiveRecord::Migration
  def self.up
    drop_table :region_relation
    create_table :region_relations do |t|
      t.integer :region_id
      t.integer :child_region_id
      t.string  :relation
    end
  end
  def self.down
  end
end
