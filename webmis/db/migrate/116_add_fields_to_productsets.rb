class AddFieldsToProductsets < ActiveRecord::Migration
  def self.up
   add_column :productsets, :internal_name, :string
   remove_column :productsets, :continent_id
   add_column :productsets, :description, :string
   add_column :productsets, :owner, :string
   add_column :productsets, :created_by, :string
   add_column :productsets, :updated_by, :string
   add_column :productsets, :region_id, :string
   add_column :productsets, :datarelease_id, :integer
   add_column :productsets, :shippingmethod_id, :integer
   add_column :productsets, :launchdate, :timestamp
   add_column :productsets, :underconstructiondate, :timestamp
   add_column :productsets, :conversiontool_id, :integer
   add_column :productsets, :data_media_id, :timestamp

   add_column :productset_versions, :internal_name, :string
   remove_column :productset_versions, :continent_id
   add_column :productset_versions, :description, :string
   add_column :productset_versions, :owner, :string
   add_column :productset_versions, :created_by, :string
   add_column :productset_versions, :updated_by, :string
   add_column :productset_versions, :region_id, :string
   add_column :productset_versions, :datarelease_id, :integer
   add_column :productset_versions, :shippingmethod_id, :integer
   add_column :productset_versions, :launchdate, :timestamp
   add_column :productset_versions, :underconstructiondate, :timestamp
   add_column :productset_versions, :conversiontool_id, :integer
   add_column :productset_versions, :data_media_id, :timestamp
  end

  def self.down
   remove_column :productset_versions, :internal_name
   remove_column :productset_versions, :description
   remove_column :productset_versions, :owner
   remove_column :productset_versions, :created_by
   remove_column :productset_versions, :updated_by
   remove_column :productset_versions, :region_id
   remove_column :productset_versions, :datarelease_id
   remove_column :productset_versions, :shippingmethod_id
   remove_column :productset_versions, :launchdate
   remove_column :productset_versions, :underconstructiondate
   remove_column :productset_versions, :conversiontool_id
   remove_column :productset_versions, :data_media_id

   remove_column :productsets, :internal_name
   remove_column :productsets, :description
   remove_column :productsets, :owner
   remove_column :productsets, :created_by
   remove_column :productsets, :updated_by
   remove_column :productsets, :region_id
   remove_column :productsets, :datarelease_id
   remove_column :productsets, :shippingmethod_id
   remove_column :productsets, :launchdate
   remove_column :productsets, :underconstructiondate
   remove_column :productsets, :conversiontool_id
   remove_column :productsets, :data_media_id
  end
end
