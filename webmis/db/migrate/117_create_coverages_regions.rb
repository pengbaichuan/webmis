class CreateCoveragesRegions < ActiveRecord::Migration
  def self.up
    create_table :coverages_regions do |t|
      t.integer :coverage_id
      t.integer :region_id
      t.timestamps
    end
  end

  def self.down
    drop_table :coverages_regions
  end
end
