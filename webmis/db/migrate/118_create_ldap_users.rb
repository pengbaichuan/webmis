class CreateLdapUsers < ActiveRecord::Migration
  def self.up
    create_table :ldap_users do |t|
      t.integer :uidnumber
      t.string :group

      t.timestamps
    end
  end

  def self.down
  end
end
