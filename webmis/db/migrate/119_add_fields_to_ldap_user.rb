class AddFieldsToLdapUser < ActiveRecord::Migration
  def self.up
    add_column :ldap_users, :first_name, :string
    add_column :ldap_users, :last_name, :string
  end

  def self.down
  end
end
