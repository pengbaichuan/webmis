class AddFieldToPoints < ActiveRecord::Migration
  def self.up
   add_column :points, :facility_status, :string
   add_column :points, :food_type,  :string
  end

  def self.down
   remove_column :points, :facility_status
   remove_column :points, :food_type
  end
end
