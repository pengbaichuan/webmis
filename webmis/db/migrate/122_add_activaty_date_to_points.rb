class AddActivatyDateToPoints < ActiveRecord::Migration
  def self.up
    add_column :points, :last_activity_date, :timestamp
    add_column :points, :country_code, :string
  end

  def self.down
    remove_column :points, :last_activity_date
    remove_column :points, :country_code
  end
end
