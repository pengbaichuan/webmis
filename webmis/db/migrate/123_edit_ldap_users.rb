class EditLdapUsers < ActiveRecord::Migration
  def self.up
    add_column :ldap_users, :phone, :string
    add_column :ldap_users, :email, :string
    add_column :ldap_users, :job_title, :string
    add_column :ldap_users, :group_DSM, :string
    add_column :ldap_users, :group_PPF, :string
    add_column :ldap_users, :group_MIS, :string
    remove_column :ldap_users, :group
  end

  def self.down
  end
end
