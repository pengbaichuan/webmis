class RenameLdapUsers < ActiveRecord::Migration
  def self.up
    #drop_table :users
    rename_table :ldap_users, :users
  end

  def self.down
    rename_table :users, :ldap_users
  end
end
