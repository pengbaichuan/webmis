class DeleteRoles < ActiveRecord::Migration
  def self.up
    Role.find(:all).each { |role| role.destroy }
  end

  def self.down
  end
end
