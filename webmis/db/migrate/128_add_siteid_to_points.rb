class AddSiteidToPoints < ActiveRecord::Migration
  def self.up
   add_column :points, :site_id, :string
  end

  def self.down
   remove_column :points, :site_id
  end
end
