class AddIndexToPointsOnInternetDatasourceId < ActiveRecord::Migration
  def self.up
	add_index :points, :internet_datasource_id
  end

  def self.down
	remove_index :points, :internet_datasource_id
  end
end
