class AddIndexToDatasourceIdInEvents < ActiveRecord::Migration
  def self.up
	add_index :events, :datasource_id
  end

  def self.down
	remove_index :events, :datasource_id
  end
end
