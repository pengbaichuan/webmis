class AddFieldForManuallyAddingPointsToInternetdatasources < ActiveRecord::Migration
  def self.up
    add_column :internet_datasources, :add_manually, :boolean
  end

  def self.down
    remove_column :internet_datasources, :add_manually, :boolean
  end
end
