class AddStatisticFieldsToInternetDatasource < ActiveRecord::Migration
  def self.up
    add_column :internet_datasources, :average_conflvl1, :integer
    add_column :internet_datasources, :average_conflvl2, :integer
    add_column :internet_datasources, :average_matchlvl1, :integer
    add_column :internet_datasources, :average_matchlvl2, :integer
    add_column :internet_datasources, :number_geomatched, :integer
  end

  def self.down
    remove_column :internet_datasources, :average_conflvl1
    remove_column :internet_datasources, :average_conflvl2
    remove_column :internet_datasources, :average_matchlvl1
    remove_column :internet_datasources, :average_matchlvl2
    remove_column :internet_datasources, :number_geomatched
  end
end
