class ChangeTypeStatisticFields < ActiveRecord::Migration
  def self.up
    change_column :internet_datasources, :average_conflvl1, :decimal, :precision => 12, :scale => 5
    change_column :internet_datasources, :average_conflvl2, :decimal, :precision => 12, :scale => 5
    change_column :internet_datasources, :average_matchlvl1, :decimal, :precision => 12, :scale => 5
    change_column :internet_datasources, :average_matchlvl2, :decimal, :precision => 12, :scale => 5
    change_column :internet_datasources, :number_geomatched, :integer
  end

  def self.down
  end
end
