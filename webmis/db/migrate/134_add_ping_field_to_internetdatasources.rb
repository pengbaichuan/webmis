class AddPingFieldToInternetdatasources < ActiveRecord::Migration
  def self.up
	add_column :internet_datasources, :is_online, :boolean
  end

  def self.down
	remove_column :internet_datasources, :is_online
  end
end
