class CreateCategories < ActiveRecord::Migration
  def self.up
    create_table :categories do |t|
      t.string :feature_name
      t.string :feature_type
      t.integer :feature_code_navteq
      t.integer :feature_code_teleatlas
      t.integer :CEN_GDF_3
      t.integer :SVF_code

      t.timestamps
    end
  end

  def self.down
    drop_table :categories
  end
end
