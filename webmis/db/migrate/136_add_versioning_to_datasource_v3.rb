class AddVersioningToDatasource < ActiveRecord::Migration
  def self.up
    Datasource.create_versioned_table
  end

  def self.down
  end
end
