class CreateOnlineEvents < ActiveRecord::Migration
  def self.up
    create_table :online_events do |t|
      t.integer :internet_datasource_id
      t.boolean :is_online
      t.datetime :updated

      t.timestamps
    end
  end

  def self.down
    drop_table :online_events
  end
end
