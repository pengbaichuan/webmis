class RemoveOnlineColumnFromInternetDatasources < ActiveRecord::Migration
  def self.up
    remove_column :internet_datasources, :is_online
  end

  def self.down
    add_column :internet_datasources, :is_online, :boolean
  end
end
