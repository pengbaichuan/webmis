class AddVersioningToInternetDatasource < ActiveRecord::Migration
  def self.up
    InternetDatasource.create_versioned_table
  end

  def self.down
  end
end
