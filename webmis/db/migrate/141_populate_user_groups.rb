class PopulateUserGroups < ActiveRecord::Migration
  def self.up
	@group = UserGroup.new
	@group.group_name = "Admin"
	@group.save
	@group = UserGroup.new
	@group.group_name = "Operation_engineer"
	@group.save
	@group = UserGroup.new
	@group.group_name = "Product Manager"
	@group.save
	@group = UserGroup.new
	@group.group_name = "No right"
	@group.save	
  end

  def self.down
    UserGroup.delete_all
  end
end
