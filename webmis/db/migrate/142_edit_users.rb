class EditUsers < ActiveRecord::Migration
  def self.up
    change_column :users, :group_DSM, :integer
    change_column :users, :group_PPF, :integer
    change_column :users, :group_MIS, :integer
  end

  def self.down
  end
end
