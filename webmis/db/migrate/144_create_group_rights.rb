class CreateGroupRights < ActiveRecord::Migration
  def self.up
    create_table :group_rights do |t|
      t.integer :user_group_id
      t.boolean :main_menu_datasources
      t.boolean :main_menu_IC
      t.boolean :main_menu_administration
      t.boolean :datasources_details
      t.boolean :datasources_edit_fields
      t.boolean :datasource_jobs
      t.boolean :datasource_IC
      t.boolean :datasource_contracts
      t.boolean :datasource_releases
      t.boolean :datasource_EC
      t.boolean :internal_contacts
      t.boolean :administration_places
      t.boolean :administration_datasource_types
      t.boolean :adminstration_coverages
      t.boolean :administration_events
      t.boolean :administration_categories
      t.boolean :administration_point_statistics
      t.boolean :administration_detailed_point_statistics
      t.boolean :administration_management_statistics
      t.boolean :administration_user_rights

      t.timestamps
    end
  end

  def self.down
    drop_table :group_rights
  end
end
