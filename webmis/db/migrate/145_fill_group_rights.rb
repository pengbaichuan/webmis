class FillGroupRights < ActiveRecord::Migration
  def self.up

    #admins
    @group = GroupRight.new
    @group.user_group_id = 1                                                             
    @group.main_menu_datasources = true                   
    @group.main_menu_IC = true                         
    @group.main_menu_administration = true             
    @group.datasources_details = true                    
    @group.datasources_edit_fields = true                 
    @group.datasource_jobs = true                         
    @group.datasource_IC = true                          
    @group.datasource_contracts = true                    
    @group.datasource_releases = true                     
    @group.datasource_EC = true                           
    @group.internal_contacts = true                       
    @group.administration_places = true                   
    @group.administration_datasource_types = true         
    @group.adminstration_coverages = true                 
    @group.administration_events = true                   
    @group.administration_categories = true               
    @group.administration_point_statistics = true          
    @group.administration_detailed_point_statistics = true 
    @group.administration_management_statistics = true     
    @group.administration_user_rights = true  
    @group.save 

    #operation engineers
    @group = GroupRight.new
    @group.user_group_id = 2                                                      
    @group.main_menu_datasources = true                   
    @group.main_menu_IC = true                         
    @group.main_menu_administration = true             
    @group.datasources_details = true                    
    @group.datasources_edit_fields = true                 
    @group.datasource_jobs = false                        
    @group.datasource_IC = false                         
    @group.datasource_contracts = false                   
    @group.datasource_releases = false                  
    @group.datasource_EC = false                           
    @group.internal_contacts = false                       
    @group.administration_places = true                   
    @group.administration_datasource_types = true         
    @group.adminstration_coverages = true                 
    @group.administration_events = false                  
    @group.administration_categories = true               
    @group.administration_point_statistics = false          
    @group.administration_detailed_point_statistics = false 
    @group.administration_management_statistics = false
    @group.administration_user_rights = false 
    @group.save


    #product managers
    @group = GroupRight.new
    @group.user_group_id = 3                                                     
    @group.main_menu_datasources = true                   
    @group.main_menu_IC = true                         
    @group.main_menu_administration = true             
    @group.datasources_details = true                    
    @group.datasources_edit_fields = true                 
    @group.datasource_jobs = true                         
    @group.datasource_IC = true                          
    @group.datasource_contracts = true                    
    @group.datasource_releases = true                     
    @group.datasource_EC = true                           
    @group.internal_contacts = false                      
    @group.administration_places = true                   
    @group.administration_datasource_types = true         
    @group.adminstration_coverages = true                 
    @group.administration_events = true                   
    @group.administration_categories = true               
    @group.administration_point_statistics = true          
    @group.administration_detailed_point_statistics = true 
    @group.administration_management_statistics = true     
    @group.administration_user_rights = false  
    @group.save

    #no_right
    @group = GroupRight.new
    @group.user_group_id = 4                                                          
    @group.main_menu_datasources = false                  
    @group.main_menu_IC =false                         
    @group.main_menu_administration = false            
    @group.datasources_details = false                    
    @group.datasources_edit_fields = false         
    @group.datasource_jobs = false                        
    @group.datasource_IC = false                         
    @group.datasource_contracts = false                  
    @group.datasource_releases = false                     
    @group.datasource_EC = false                           
    @group.internal_contacts = false                    
    @group.administration_places = false                
    @group.administration_datasource_types = false         
    @group.adminstration_coverages = false                 
    @group.administration_events = false                  
    @group.administration_categories = false              
    @group.administration_point_statistics = false          
    @group.administration_detailed_point_statistics = false 
    @group.administration_management_statistics = false     
    @group.administration_user_rights = false 
    @group.save           
  end

  def self.down
  end
end
