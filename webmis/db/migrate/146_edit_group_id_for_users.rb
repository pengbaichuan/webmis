class EditGroupIdForUsers < ActiveRecord::Migration
  def self.up
    @users = User.find(:all)
    @users.each { |user|
      user.group_DSM = 1
      user.save
    }
  end

  def self.down
  end
end
