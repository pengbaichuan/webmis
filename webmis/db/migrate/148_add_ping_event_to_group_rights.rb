class AddPingEventToGroupRights < ActiveRecord::Migration
  def self.up
    add_column :group_rights, :administration_ping_events, :boolean
  end

  def self.down
    remove_column :group_rights, :administration_ping_events
  end
end
