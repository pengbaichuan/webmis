class AddIndicesToPoints < ActiveRecord::Migration
  def self.up
    add_index(:points, [:internet_datasource_id, :postal_code])
    add_index(:points, [:internet_datasource_id, :house_number])
    add_index(:points, [:internet_datasource_id, :street_name])
    add_index(:points, [:internet_datasource_id, :city_name])
    add_index(:points, [:internet_datasource_id, :country_name])
    add_index(:points, [:internet_datasource_id, :food_type])
    add_index(:points, [:internet_datasource_id, :brandname])
    add_index(:points, [:internet_datasource_id, :absolute_xcoordinate])
    add_index(:points, [:internet_datasource_id, :absolute_ycoordinate])
    add_index(:points, [:internet_datasource_id, :last_activity_date])
  end

  def self.down
  end
end
