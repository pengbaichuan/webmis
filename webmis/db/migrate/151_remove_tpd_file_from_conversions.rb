class RemoveTpdFileFromConversions < ActiveRecord::Migration
  def self.up
    remove_column :conversions, :tpdfile
  end

  def self.down
    add_column :conversions, :tpdfile, :string
  end
end
