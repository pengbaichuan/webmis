class RemoveProductreleaseFileFromConversions < ActiveRecord::Migration
  def self.up
    remove_column :conversions, :productrelease
  end

  def self.down
    add_column :conversions, :productrelease, :string
  end
end
