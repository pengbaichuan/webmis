class EditConversiontools < ActiveRecord::Migration
  def self.up
    rename_column :conversiontools, :type, :tool_type
  end

  def self.down
  end
end
