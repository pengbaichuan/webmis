class RenameTableRoles < ActiveRecord::Migration
  def self.up
  	rename_table :roles, :datasource_internalcontacts
  end

  def self.down
   rename_table :datasource_internalcontacts, :roles
  end
end
