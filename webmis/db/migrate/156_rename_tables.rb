class RenameTables < ActiveRecord::Migration
  def self.up
    rename_table :user_groups, :roles
    rename_table :group_users, :roles_users
  end

  def self.down
    rename_table :roles, :user_groups
    rename_table :roles_users, :group_users
  end
end
