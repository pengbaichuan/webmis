class RenameColumns < ActiveRecord::Migration
  def self.up
    rename_column :roles, :group_name, :title
    rename_column :roles_users, :user_group_id, :role_id
  end

  def self.down
     rename_column :roles, :title, :group_name
    rename_column :roles_users, :role_id, :user_group_id 
  end
end
