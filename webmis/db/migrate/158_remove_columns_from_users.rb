class RemoveColumnsFromUsers < ActiveRecord::Migration
  def self.up
    remove_column :users, :group_DSM
    remove_column :users, :group_PPF
    remove_column :users, :group_MIS
  end

  def self.down
  end
end
