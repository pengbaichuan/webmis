class AddDataInRoles < ActiveRecord::Migration
  def self.up
    Role.delete_all
    Role.create(:title => 'Admin')
    Role.create(:title => 'Product Manager')
    Role.create(:title => 'Operations Engineer')
  end

  def self.down
    Role.delete_all
  end
end
