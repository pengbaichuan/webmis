class CreatePointStatistics < ActiveRecord::Migration
  def self.up
    create_table :point_statistics do |t|
      t.integer :amount
      t.float :street_percentage
      t.float :postal_code_percentage
      t.float :housenumber_percentage
      t.float :city_percentage
      t.float :country_percentage
      t.float :food_type_percentage
      t.float :brandname_percentage
      t.float :x_percentage
      t.float :y_percentage
      t.float :activity_percentage

      t.timestamps
    end
  end

  def self.down
    drop_table :point_statistics
  end
end
