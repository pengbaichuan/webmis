class AddDatasourceToPointStatistics < ActiveRecord::Migration
  def self.up
    add_column :point_statistics, :internet_datasource_id, :integer
  end

  def self.down
    remove_column :point_statistics, :internet_datasource_id
  end
end
