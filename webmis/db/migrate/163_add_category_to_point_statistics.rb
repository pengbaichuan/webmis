class AddCategoryToPointStatistics < ActiveRecord::Migration
  def self.up
    add_column :point_statistics, :category, :integer
  end

  def self.down
    remove_column :point_statistics, :category
  end
end
