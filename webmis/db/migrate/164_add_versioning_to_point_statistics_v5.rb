class AddVersioningToPointStatistics < ActiveRecord::Migration
  def self.up
    PointStatistic.create_versioned_table
  end

  def self.down
  end
end
