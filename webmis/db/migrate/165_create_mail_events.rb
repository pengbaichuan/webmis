class CreateMailEvents < ActiveRecord::Migration
  def self.up
    create_table :mail_events do |t|
      t.string :name

      t.timestamps
    end
  end

  def self.down
    drop_table :mail_events
  end
end
