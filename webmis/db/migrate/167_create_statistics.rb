class CreateStatistics < ActiveRecord::Migration
  def self.up
    create_table :statistics do |t|
      t.integer :coverage_id
      t.string :category
      t.integer :amount
      t.integer :datasources

      t.timestamps
    end
  end

  def self.down
    drop_table :statistics
  end
end
