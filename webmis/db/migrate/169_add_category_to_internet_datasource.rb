class AddCategoryToInternetDatasource < ActiveRecord::Migration
  def self.up
    add_column :internet_datasources, :category, :integer
  end

  def self.down
    remove_column :internet_datasources, :category
  end
end
