class AddCategoryToInternetDatasourcesVersioned < ActiveRecord::Migration
  def self.up
    add_column :internet_datasource_versions, :category, :integer
  end

  def self.down
    remove_column :internet_datasource_versions, :category
  end
end
