class RemoveLanguages < ActiveRecord::Migration
  def self.up
    @languages = Language.find(:all)
    @languages.each do |language|
	if language.lang_type != 'L'
		language.destroy
	end
    end
  end

  def self.down
  end
end
