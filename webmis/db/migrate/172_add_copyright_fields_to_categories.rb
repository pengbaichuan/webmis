class AddCopyrightFieldsToCategories < ActiveRecord::Migration
  def self.up
	add_column :categories, :food_type, :boolean
	add_column :categories, :brandname, :boolean
	add_column :categories, :name_of_facility, :boolean
	add_column :categories, :street_name, :boolean
	add_column :categories, :house_number, :boolean
	add_column :categories, :postal_code, :boolean
	add_column :categories, :city_name, :boolean
	add_column :categories, :country_name, :boolean
	add_column :categories, :importance, :boolean
	add_column :categories, :indicator, :boolean
	add_column :categories, :phone_number, :boolean
	add_column :categories, :fax_number, :boolean
	add_column :categories, :deeplink, :boolean
	add_column :categories, :email, :boolean
	add_column :categories, :invicinity, :boolean
	add_column :categories, :fuel_type, :boolean
	add_column :categories, :parking_size, :boolean
	add_column :categories, :departure_times, :boolean
	add_column :categories, :arrival_times, :boolean
	add_column :categories, :picture, :boolean
	add_column :categories, :population, :boolean
	add_column :categories, :size, :boolean
	add_column :categories, :opening_times, :boolean
	add_column :categories, :speed_limit, :boolean
	add_column :categories, :edit_information, :boolean
	add_column :categories, :price_indicator, :boolean
  end

  def self.down
	remove_column :categories, :food_type
	remove_column :categories, :brandname
	remove_column :categories, :name_of_facility
	remove_column :categories, :street_name
	remove_column :categories, :house_number
	remove_column :categories, :postal_code
	remove_column :categories, :city_name
	remove_column :categories, :country_name
	remove_column :categories, :importance
	remove_column :categories, :indicator
	remove_column :categories, :phone_number
	remove_column :categories, :fax_number
	remove_column :categories, :deeplink
	remove_column :categories, :email
	remove_column :categories, :invicinity
	remove_column :categories, :fuel_type
	remove_column :categories, :parking_size
	remove_column :categories, :departure_times
	remove_column :categories, :arrival_times
	remove_column :categories, :picture
	remove_column :categories, :population
	remove_column :categories, :size
	remove_column :categories, :opening_times
	remove_column :categories, :speed_limit
	remove_column :categories, :edit_information
	remove_column :categories, :price_indicator
  end
end
