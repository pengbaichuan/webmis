class AddShiptoIdToOutboundOrder < ActiveRecord::Migration
  def self.up
   add_column :outbound_orders, :shipto_id, :string
  end

  def self.down
   remove_column :outbound_orders, :shipto_id
  end
end
