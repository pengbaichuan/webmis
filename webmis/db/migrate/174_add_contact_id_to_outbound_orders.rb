class AddContactIdToOutboundOrders < ActiveRecord::Migration
  def self.up
    add_column :outbound_orders,:contact_id, :string 
  end

  def self.down
    remove_column :outbound_orders,:contact_id
  end
end
