class AddMediaNumberToStock < ActiveRecord::Migration
  def self.up
   add_column :stock, :medium_number,:string
  end

  def self.down
   remove_column :stock, :medium_number
  end
end
