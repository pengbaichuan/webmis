class AddRegionToConversion < ActiveRecord::Migration
  def self.up
   add_column :conversions, :region_id, :integer
   add_column :conversions, :region_name,:string
  end

  def self.down
   remove_column :conversions, :region_id
   remove_column :conversions, :region_name
  end
end
