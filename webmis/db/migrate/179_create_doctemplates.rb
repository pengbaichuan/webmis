class CreateDoctemplates < ActiveRecord::Migration
  def self.up
   create_table :doctemplates do |t|
      t.string :module
      t.string :name
      t.string :file
      t.timestamps
    end

  end

  def self.down
  end
end
