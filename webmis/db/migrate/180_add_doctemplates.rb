class AddDoctemplates < ActiveRecord::Migration
  def self.up
    d = Doctemplate.new 
    d.file = "gorequest"
    d.module = "WEBMIS"
    d.name = "Go Request"
    d.save

    d = Doctemplate.new 
    d.file = "mastershipmentt"
    d.module = "WEBMIS"
    d.name = "Master Shipment"
    d.save

    d = Doctemplate.new 
    d.file = "mastershipmentinfo"
    d.module = "WEBMIS"
    d.name = "Master Shipment"
    d.save

    d = Doctemplate.new 
    d.file = "mastershipmentSD"
    d.module = "WEBMIS"
    d.name = "Master Shipment SD"
    d.save

    d = Doctemplate.new 
    d.file = "releasenotification"
    d.module = "WEBMIS"
    d.name = "Release Notification"
    d.save

    d = Doctemplate.new 
    d.file = "releasenotificationinfo"
    d.module = "WEBMIS"
    d.name = "Release Notification Info"
    d.save

    d = Doctemplate.new 
    d.file = "sampleapproval"
    d.module = "WEBMIS"
    d.name = "Sample Approval"
    d.save


    d = Doctemplate.new 
    d.file = "testdelivery"
    d.module = "WEBMIS"
    d.name = "Test Delivery"
    d.save

    d = Doctemplate.new 
    d.file = "alphatestdelivery"
    d.module = "WEBMIS"
    d.name = "Alpha Test Delivery"
    d.save
    
    d = Doctemplate.new 
    d.file = "copyofmastershipment"
    d.module = "WEBMIS"
    d.name = "Copy of Master Shipment"
    d.save

    
  end

  def self.down
  end
end
