class ChangeRdstmcfile < ActiveRecord::Migration
  def self.up
    change_column :conversions, :rdstmcfile, :text
  end

  def self.down
  end
end
