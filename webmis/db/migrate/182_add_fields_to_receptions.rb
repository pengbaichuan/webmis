class AddFieldsToReceptions < ActiveRecord::Migration
  def self.up
   add_column :receptions,:send_status,:boolean
   add_column :receptions,:sendto,:text
   add_column :receptions,:senddate,:timestamp
  end

  def self.down
   remove_column :receptions,:send_status
   revove_column :receptions,:sendto
   remove_column :receptions,:senddate
  end
end
