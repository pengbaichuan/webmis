class AddFieldToMigration < ActiveRecord::Migration
  def self.up
   add_column :receptions,:user_name,:string
  end

  def self.down
   remove_column :receptions,:user_name
  end
end
