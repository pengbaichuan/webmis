class ChangeTemplateOnConversion < ActiveRecord::Migration
  def self.up
   remove_column :conversions, :template_id
   add_column :conversions, :template_filename, :string
  end

  def self.down
   add_column :conversions, :template_id, :integer
   remove_column :conversions, :template_filename
  end
end
