class FixSizeFieldsOnConversion < ActiveRecord::Migration
  def self.up
    change_column :conversions,:size_bytes , :float
    change_column :conversions,:size_mb , :float
  end

  def self.down
    change_column :conversions,:size_bytes , :string
    change_column :conversions, :size_mb , :string
  end
end
