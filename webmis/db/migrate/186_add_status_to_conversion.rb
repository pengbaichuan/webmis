class AddStatusToConversion < ActiveRecord::Migration
  def self.up
   add_column :conversions, :status, :integer
  end

  def self.down
   remove_column :conversions, :status
  end
end
