class AddConversiontoolToReception < ActiveRecord::Migration
  def self.up
    add_column :receptions, :conversiontool, :string
  end

  def self.down
    remove_column :receptions, :conversiontool
  end
end
