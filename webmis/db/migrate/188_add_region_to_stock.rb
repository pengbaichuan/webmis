class AddRegionToStock < ActiveRecord::Migration
  def self.up
    add_column :stock, :region_name,:string
    add_column :stock, :region_id,:integer
    add_column :stock, :location_type, :string
  end

  def self.down
    remove_column :stock, :region_name
    remove_column :stock, :region_id
    remove_columm :stock, :location_type
  end
end
