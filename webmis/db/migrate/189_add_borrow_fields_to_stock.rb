class AddBorrowFieldsToStock < ActiveRecord::Migration
  def self.up
   add_column :stock, :borrby, :string
   add_column :stock, :borron, :string
   add_column :stock, :borrback, :string
  end

  def self.down
   remove_column :stock, :borrby
   remove_column :stock, :borron
   remove_column :stock, :borrback
  end
end
