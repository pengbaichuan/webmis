class AddLocationtypeToLocations < ActiveRecord::Migration
  def self.up
    add_column :locations,:locationtype,:string
  end

  def self.down
    remove_column :locations,:locationtype
  end
end
