class AddReleaseNameToReceivedMedia < ActiveRecord::Migration
  def self.up
    add_column :inbound_orderlines,:data_release_name,:string
  end

  def self.down
    remove_column :inbound_orderlines,:data_release_name
  end
end
