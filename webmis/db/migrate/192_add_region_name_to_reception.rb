class AddRegionNameToReception < ActiveRecord::Migration
  def self.up
    add_column :receptions,:region_name,:string
  end

  def self.down
    remove_column :receptions,:region_name
  end
end
