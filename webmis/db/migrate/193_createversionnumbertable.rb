class Createversionnumbertable < ActiveRecord::Migration
  def self.up
    create_version_number_table
  end

  def self.down
    drop version_number_table
  end
end
