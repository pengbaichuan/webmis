class FixPpf < ActiveRecord::Migration
  def self.up
    drop_table :productsets
    drop_table :products
    drop_table :productset_versions
    drop_table :product_versions

 create_versioned_table "productsets" do |t|
    t.column  "name",:string
    t.column  "supplier_id",:string
    t.column  "customer_id",:string
    t.column  "firsttier_id",:string
    t.column   "customer_name",:string
    t.column   "firsttiername",:string
    t.column   "state",:string
    t.column   "major",:integer
    t.column   "minor",:integer
    t.column   "dateeffective",:datetime
    t.column   "changes",:text
    t.column   "remarks",:text
    t.column   "internal_name",:string
    t.column   "description",:string
    t.column   "owner",:string 
    t.column   "region_id",:integer
    t.column   "datarelease_id",:integer
    t.column   "datarelease_name",:string
    t.column   "platform_name",:string
    t.column   "ciq",:string
    t.column   "nds_name",:string
    t.column   "area_name",:string
    t.column   "region_id",:integer
    t.column   "shippingmethod_name",:string
    t.column   "launchdate",:datetime
    t.column   "underconstructiondate",:datetime
    t.column   "conversiontool_name",:string
    t.column   "medium_name",:string
    t.column   "approval_text",:string
    t.column   "status",:string 
  end

 
  create_versioned_table "includedproducts" do |t|
     t.column :productset_id , :integer
     t.column :product_id,:integer
  end 


  create_versioned_table "products" do |t|
    t.column   "name",:string
    t.column   "customer_name",:string
    t.column   "customer_id",:string
    t.column   "volumeid",:string
    t.column "dateeffective",:datetime
    t.column   "detailedcoverage",:text
    t.column   "hwncoverage",:text
    t.column   "thirdpartydata1",:text
    t.column   "thirdpartydata2",:text
    t.column   "tmclocation",:text
    t.column   "tmcevent",:string
    t.column   "remarks",:text
    t.column   "zip",:text
    t.column   "qxs",:text
    t.column   "speedlimits",:text
    t.column   "namerotations",:text
    t.column   "medium_name",:string
  end



  end

  def self.down
   drop_versioned_table :products
   drop_versioned_table :productsets
   drop_versioned_table :includedproducts 
  end
end
