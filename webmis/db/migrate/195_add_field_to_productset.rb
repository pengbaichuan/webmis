class AddFieldToProductset < ActiveRecord::Migration
  def self.up
   add_column :productsets,:supplier_name,:string
   add_column :productsets,:tpdspec,:string
   add_column :productsets,:predecessor,:string
   add_column :productsets,:setcode,:string

   add_column :productset_versions,:supplier_name,:string
   add_column :productset_versions,:tpdspec,:string
   add_column :productset_versions,:predecessor,:string
   add_column :productset_versions,:setcode,:string
  end

  def self.down
   remove_column :productsets,:supplier_name
   remove_column :productsets,:tpdspec
   remove_column :productsets,:predecessor
   remove_column :productsets,:setcode
   remove_column :productset_versions,:supplier_name
   remove_column :productset_versions,:tpdspec
   remove_column :productset_versions,:predecessor
   remove_column :productset_versions,:setcode
  end
end
