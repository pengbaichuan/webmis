class AddNdsToProduct < ActiveRecord::Migration
  def self.up
   add_column :products,:nds_name,:string
   add_column :product_versions,:nds_name,:string
  end

  def self.down
   remove_column :products,:nds_name
   remove_column :product_versions,:nds_name
  end
end
