class ChangeTimeColumnProductset < ActiveRecord::Migration
  def self.up
   change_column :productsets,:dateeffective, :date
   change_column :productsets,:launchdate, :date
   change_column :productsets,:underconstructiondate, :date
  end

  def self.down
   change_column :productsets,:dateeffective, :datetime
   change_column :productsets,:launchdate, :date
   change_column :productsets,:underconstructiondate, :date
  end
end
