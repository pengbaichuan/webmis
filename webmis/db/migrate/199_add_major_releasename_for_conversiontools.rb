class AddMajorReleasenameForConversiontools < ActiveRecord::Migration
  def self.up
    add_column :conversiontools, :major_release, :string
    conversiontools = Conversiontool.find(:all)
    conversiontools.each do |conversiontool|
      if conversiontool.code != "" and conversiontool.code != nil
	      major_release = conversiontool.code.split("_")[0]
      		conversiontool.major_release = major_release
      		conversiontool.save
      end
    end
  end

  def self.down
     remove_column :conversiontools, :major_release
  end
end
