class AddProxyToInternetDatasourcesTable < ActiveRecord::Migration
 def self.up
   add_column :internet_datasources, :high_priority, :integer
  end

  def self.down
   remove_column :internet_datasources, :high_priority
  end
end
