class InternetDatasourcesAddColumnActiveYn < ActiveRecord::Migration
  def self.up
    add_column(:internet_datasources, :active_yn, :boolean)
  end

  def self.down
    remove_column(:internet_datasources, :active_yn, :boolean)
  end
end
