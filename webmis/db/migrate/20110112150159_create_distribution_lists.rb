class CreateDistributionLists < ActiveRecord::Migration
  def self.up
    create_table :distribution_lists do |t|
      t.string :name
      t.boolean :active_yn
      t.boolean :deleted_yn
      t.text :remarks

      t.timestamps
    end
  end

  def self.down
    drop_table :distribution_lists
  end
end
