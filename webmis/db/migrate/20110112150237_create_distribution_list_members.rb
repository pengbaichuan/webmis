class CreateDistributionListMembers < ActiveRecord::Migration
  def self.up
    create_table :distribution_list_members do |t|
      t.integer :distribution_list_id
      t.string :external_contact_id
      t.boolean :electronic_delivery_yn
      t.boolean :hard_copy_delivery_yn
      t.integer :hard_copy_amount
      t.string :transfer_method

      t.timestamps
    end
  end

  def self.down
    drop_table :distribution_list_members
  end
end
