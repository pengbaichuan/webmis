class AddCanBeRemovedToReceptions < ActiveRecord::Migration
  def self.up
    add_column :receptions, :can_be_removed_yn, :boolean
  end

  def self.down
    remove_column :receptions, :can_be_removed_yn
  end
end
