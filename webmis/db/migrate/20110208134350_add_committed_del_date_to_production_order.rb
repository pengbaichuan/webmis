class AddCommittedDelDateToProductionOrder < ActiveRecord::Migration
  def self.up
    add_column :production_orders, :committed_del_date, :datetime
  end

  def self.down
    remove_column :production_orders, :committed_del_date
  end
end
