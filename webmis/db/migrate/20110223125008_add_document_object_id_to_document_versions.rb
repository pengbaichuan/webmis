class AddDocumentObjectIdToDocumentVersions < ActiveRecord::Migration
  def self.up
    add_column :document_versions, :document_object_id, :string
  end

  def self.down
    remove_column :document_versions, :document_object_id
  end
end
