class ChangeReceptionsStorageLocationMediumText < ActiveRecord::Migration
  def self.up
    execute "alter table receptions modify column storage_location mediumtext"
  end

  def self.down
    execute "alter table receptions modify column storage_location text"
  end
end