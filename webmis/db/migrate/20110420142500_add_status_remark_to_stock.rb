class AddStatusRemarkToStock < ActiveRecord::Migration
  def self.up
   add_column :stock, :status_remark, :text
  end

  def self.down
   remove_column :stock, :status_remark
  end
end