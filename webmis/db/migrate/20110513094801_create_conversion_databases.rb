class CreateConversionDatabases < ActiveRecord::Migration
  def self.up
    create_table :conversion_databases do |t|

      t.integer :conversion_id
      t.string :db_type
      t.string :path_and_file
      t.string :filename
      t.string :path
      t.text :remarks
      t.string :product_id
      t.integer :size_bytes
      t.boolean :removed_yn, :default => false

      t.timestamps
    end
  end

  def self.down
    drop_table :conversion_databases
  end
end
