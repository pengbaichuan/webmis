class ReplaceViewReceptionsAndConversiondbs < ActiveRecord::Migration
  def self.up
    sql="create or REPLACE VIEW view_receptions_and_conversiondbs AS
         SELECT 'reception' AS 'source',
            receptions.id, 
            receptions.remarks, 
            receptions.storage_location, 
            receptions.referenceid, 
            receptions.data_release_id, 
            data_types.name AS 'datatype', 
            receptions.region_name, 
            receptions.area_id, 
            receptions.validation_requested, 
            receptions.validated_yn 
        from receptions,data_types where  removed_yn = 0 and receptions.data_type_id = data_types.id
        UNION ALL SELECT 'conversion', 
            conversion_databases.id, 
            conversion_databases.remarks, 
            group_concat(conversion_databases.path_and_file separator ''),  
            cast(conversion_databases.conversion_id AS char), 
            conversions.data_release_id, 
            conversion_databases.db_type, 
            conversions.region_name, 
            conversions.region_id, 
            null, 
            null 
        from conversion_databases,conversions where conversion_databases.removed_yn = 0 and conversion_databases.db_type != 'CAR'  and conversion_databases.conversion_id = conversions.id group by conversion_databases.conversion_id;"
    ActiveRecord::Base.connection.execute(sql) 
  end

  def self.down
    sql="drop VIEW view_receptions_and_conversiondbs"
    ActiveRecord::Base.connection.execute(sql)
  end
end
