class AddImportPathToProductCategories < ActiveRecord::Migration
  def self.up
    add_column :product_categories, :pds_import_file, :text
  end

  def self.down
    remove_column :product_categories, :pds_import_file
  end
end
