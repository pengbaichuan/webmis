class AddStatusRemarkToConversion < ActiveRecord::Migration
  def self.up
   add_column :conversions, :status_remark, :text
  end

  def self.down
   remove_column :conversions, :status_remark
  end
end
