class AddSetReleasesToProductset < ActiveRecord::Migration
  def self.up
   add_column :productsets, :set_release_major, :string
   add_column :productsets, :set_release_minor, :string
   add_column :productset_versions, :set_release_major, :string
   add_column :productset_versions, :set_release_minor, :string
  end

  def self.down
   remove_column :productsets, :set_release_major
   remove_column :productsets, :set_release_minor
   remove_column :productset_versions, :set_release_major
   remove_column :productset_versions, :set_release_minor
  end
end
