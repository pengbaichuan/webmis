class CreateTestRequests < ActiveRecord::Migration
  def self.up
    create_table :test_requests do |t|
      t.integer  :production_order_line_id
      t.integer  :test_type_id
      t.integer  :requested_by_user_id, :assigned_to_user_id
      t.integer  :test_db_conversion_databases_id, :ref_db_conversion_databases_id
      t.string   :status
      t.string   :product_id
      t.date     :start_date, :finish_date, :actual_start_date, :actual_finish_date
      t.text     :request_remarks, :result_remarks      
      t.boolean  :removed_yn
      t.timestamps
    end
  end

  def self.down
    drop_table :test_requests
  end
end
