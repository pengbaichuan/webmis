class AddIndexesToPdsVersionTables < ActiveRecord::Migration
  def self.up
     add_index(:productset_versions, [:productset_id, :version])
     add_index(:product_versions,    [:product_id, :version])
  end

  def self.down
     remove_index(:productset_versions, [:productset_id, :version])
     remove_index(:product_versions,    [:product_id, :version])
  end
end
