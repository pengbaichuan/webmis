class AddIndexesToPds < ActiveRecord::Migration
  def self.up
     add_index(:includedproducts, [:productset_id])
     add_index(:product_data_set_infos, [:product_id])

  end

  def self.down
     remove_index(:includedproducts, [:productset_id])
     remove_index(:product_data_set_infos, [:product_id])
  end
end
