class MvRemarksToAdditionalInfo < ActiveRecord::Migration
  def self.up
    Conversion.find(:all, :conditions => {:type => "SvfConversion"}).each do |c|
      c.additional_info = c.remarks
      c.remarks = "n.a."
      if !c.end_product_yn
        c.end_product_yn = "No"
      end
      c.save!
    end
  end

  def self.down
      Conversion.find(:all, :conditions => {:type => "SvfConversion"}).each do |c|
      c.remarks = c.additional_info
      c.additional_info = nil
      c.save!
    end
  end
end
