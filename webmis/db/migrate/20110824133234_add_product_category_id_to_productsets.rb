
class AddProductCategoryIdToProductsets < ActiveRecord::Migration
  require 'version_fu'
  def self.up
    add_column :productsets, :product_category_id, :integer
    
    Productset.all.each do |ps|
      pc = ProductCategory.find(:first, :conditions => "abbreviation like \"%#{ps.product_category}%\"")
      if pc && ps.product_category
        ps.product_category_id = pc.id
        ps.save
      end      
    end
  end

  def self.down
    remove_column :productsets, :product_category_id
  end
end
