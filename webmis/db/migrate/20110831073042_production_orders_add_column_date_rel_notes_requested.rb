class ProductionOrdersAddColumnDateRelNotesRequested < ActiveRecord::Migration
  def self.up
    add_column :production_orders, :rel_notes_requested_date, :date
    add_column :production_orders, :rel_notes_requested_by_id, :integer
    add_column :production_orders, :rel_notes_send_date, :date
    add_column :production_orders, :rel_notes_send_by_id, :integer
  end

  def self.down
    remove_column :production_orders, :rel_notes_requested_date
    remove_column :production_orders, :rel_notes_requested_by_id
    remove_column :production_orders, :rel_notes_send_date
    remove_column :production_orders, :rel_notes_send_by_id
  end
end
