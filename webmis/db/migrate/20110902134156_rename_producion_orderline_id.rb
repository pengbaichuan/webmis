class RenameProducionOrderlineId < ActiveRecord::Migration
  def self.up
      rename_column :test_requests, :production_order_line_id, :production_orderline_id
  end

  def self.down
      rename_column :test_requests, :production_orderline_id, :production_order_line_id
  end
end
