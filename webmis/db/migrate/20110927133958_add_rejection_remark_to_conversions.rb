class AddRejectionRemarkToConversions < ActiveRecord::Migration
  def self.up
    add_column :conversions, :rejection_remark, :text
  end

  def self.down
    remove_column :conversions, :rejection_remark
  end
end
