class SetConversionStatus < ActiveRecord::Migration
  def self.up
    Conversion.find(:all, :conditions => "outcome = 'Canceled' and status != '30'").each do |c|
      c.status = 30
            c.save
    end
    Conversion.find(:all, :conditions => "result = 'Cancelled' and status != '30'").each do |c|
      c.status = 30
            c.save
    end
    Conversion.find(:all, :conditions => "outcome = 'OK' and status = '10'").each do |c|
      c.status = 20
            c.save
    end
    Conversion.find(:all, :conditions => "outcome = 'OK' and status is null").each do |c|
      c.status = 20
            c.save
    end
    Conversion.find(:all, :conditions => "outcome = 'Not OK' and status = '10'").each do |c|
      c.status = 20
            c.save
    end
    Conversion.find(:all, :conditions => "outcome = 'Crashed' and status = '10'").each do |c|
      c.status = 20
            c.save
    end
    Conversion.find(:all, :conditions => "result = 'Crashed' and status = '10'").each do |c|
      c.status = 20
            c.save
    end
    Conversion.find(:all, :conditions => "result = 'Not OK' and status = '10'").each do |c|
      c.status = 20
            c.save
    end
    Conversion.find(:all, :conditions => "result = 'Partly compiled' and status = '10'").each do |c|
      c.status = 13
            c.save
    end
    Conversion.find(:all, :conditions => "result = 'Correct' and status = '10'").each do |c|
      c.status = 20
            c.save
    end
    

  end

  
  def self.down
    Conversion.find(:all, :conditions => "outcome = 'OK' and status = '20'").each do |c|
      c.status = 10
            c.save
    end
    Conversion.find(:all, :conditions => "outcome = 'Not OK' and status = '20'").each do |c|
      c.status = 10
            c.save
    end
    Conversion.find(:all, :conditions => "outcome = 'Crashed' and status = '20'").each do |c|
      c.status = 10
            c.save
    end
    Conversion.find(:all, :conditions => "result = 'Crashed' and status = '20'").each do |c|
      c.status = 10
            c.save
    end
    Conversion.find(:all, :conditions => "result = 'Not OK' and status = '20'").each do |c|
      c.status = 10
            c.save
    end
    Conversion.find(:all, :conditions => "result = 'Partly compiled' and status = '13'").each do |c|
      c.status = 10
            c.save
    end
    Conversion.find(:all, :conditions => "outcome = 'Canceled' and status = '30'").each do |c|
      c.status = 10
            c.save
    end
    Conversion.find(:all, :conditions => "result = 'Cancelled' and status = '30'").each do |c|
      c.status = 10
            c.save
    end
    Conversion.find(:all, :conditions => "result = 'Correct' and status = '20'").each do |c|
      c.status = 10
            c.save
    end    
  end
end
