class AddProductAcceptanceToProductionOrderlines < ActiveRecord::Migration
  def self.up
    add_column :production_orderlines, :product_accepted, :string
    add_column :production_orderlines, :customer_feedback, :text
  end

  def self.down
    remove_column :production_orderlines, :product_accepted
    remove_column :production_orderlines, :customer_feedback
  end
end
