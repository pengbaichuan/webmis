class RemoveProductCategoryFromProductsets < ActiveRecord::Migration
  def self.up
    remove_column :productsets, :product_category
  end

  def self.down
    # Can't rollback with data
    add_column :productsets, :product_category, :string
  end
end
