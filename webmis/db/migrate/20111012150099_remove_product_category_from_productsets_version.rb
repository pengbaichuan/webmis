class RemoveProductCategoryFromProductsetsVersion < ActiveRecord::Migration
  def self.up
    remove_column :productset_versions, :product_category
    add_column    :productset_versions, :product_category_id, :integer
  end

  def self.down
    # Can't rollback with data
    add_column    :productset_versions, :product_category, :string
    remove_column :productset_versions, :product_category_id
  end
end
