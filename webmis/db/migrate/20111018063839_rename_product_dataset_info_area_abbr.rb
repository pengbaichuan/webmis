class RenameProductDatasetInfoAreaAbbr < ActiveRecord::Migration
  def self.up
    rename_column :product_data_set_infos, :area_abbr, :area_name
  end

  def self.down
    rename_column :product_data_set_infos, :area_name, :area_abbr
  end
end
