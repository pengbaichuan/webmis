class AddCompanyAbbrIdToProductSets < ActiveRecord::Migration
  def self.up
    add_column :productsets, :company_abbr_id, :integer
    add_column :productsets, :data_set_description, :string
  end

  def self.down
    remove_column :productsets, :company_abbr_id
    remove_column :productsets, :data_set_description
  end
end