class AddTemplateToCvtoolTemplates < ActiveRecord::Migration
  def self.up
    add_column :cvtool_templates, :template, :text
    add_column :cvtool_templates, :template_by_user_id, :integer
    rename_column :cvtool_templates, :filename, :name
    add_column :cvtool_templates, :filename, :string
  end

  def self.down
    remove_column :cvtool_templates, :template
    remove_column :cvtool_templates, :template_by_user_id
    rename_column :cvtool_templates, :name
  end
end
