
class AddProductCategoryIdToDistributionLists < ActiveRecord::Migration
  def self.up
    add_column :distribution_lists, :product_category_id, :integer
    add_column :production_orders, :distribution_list_id, :integer 
  end

  def self.down
    remove_column :distribution_lists, :product_category_id
    remove_column :production_orders, :distribution_list_id
  end
end
