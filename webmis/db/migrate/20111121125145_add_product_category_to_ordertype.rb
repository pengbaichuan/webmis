class AddProductCategoryToOrdertype < ActiveRecord::Migration
  def self.up
    add_column :ordertypes, :product_category_id, :integer
  end

  def self.down
    remove_column :ordertypes, :product_category_id
  end
end
