class RemoveLegacyFromConversion < ActiveRecord::Migration
  def self.up
    add_column :conversions, :cv_tool_template_id, :integer
    remove_column :conversions,:gsm
    remove_column :conversions,:gsmfile
    remove_column :conversions,:dealer_name
    remove_column :conversions,:dealers_check
    remove_column :conversions,:carribatool
    remove_column :conversions,:rdstmc
    remove_column :conversions,:rdstmcfile
    remove_column :conversions,:gsm_check
    remove_column :conversions,:tmc_check
    remove_column :conversions,:poifile

  end

  def self.down
    remove_column :conversions, :cv_tool_template_id
    add_column :conversions,:gsm, :boolean
    add_column :conversions,:gsmfile, :string
    add_column :conversions,:dealer_name, :string
    add_column :conversions,:dealers_check, :boolean
    add_column :conversions,:carribatool, :string
    add_column :conversions,:rdstmc, :boolean
    add_column :conversions,:rdstmcfile, :string
    add_column :conversions,:gsm_check, :boolean
    add_column :conversions,:tmc_check, :boolean
    add_column :conversions,:poifile, :string
    
  end
end
