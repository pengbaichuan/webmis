class RemoveLegacyFromConversionSecond < ActiveRecord::Migration
  def self.up
     Conversion.find(:all,:conditions => {:type => 'CarinConversion'}).each do |c|
      c.output_format = 'CAR'
      c.save
     end
     rename_column :conversions, :type, :legacy_type
  end
  
  def self.down
    rename_column :conversions, :legacy_type, :type
    Conversion.find(:all,:conditions => {:type => 'CarinConversion'}).each do |c|
    c.output_format = nil
    c.save 
    end    
  end
end