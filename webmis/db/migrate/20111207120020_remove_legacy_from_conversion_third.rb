class RemoveLegacyFromConversionThird < ActiveRecord::Migration
  def self.up
      Conversion.find(:all, :conditions => {:outcome => nil}).each do |c|
        if c.result 
          case c.result
            when "OK"; 	          c.outcome = "Correct"
            when "Correct";       c.outcome = "Correct"
            when "Crashed";       c.outcome = "Crashed"
            when "Not OK";        c.outcome = "Not OK"
            when "";              c.outcome = "No result"
            when "Not yet known"; c.outcome = "No result"
            when "Canceled";      c.outcome = "Canceled"
          end
          c.save	
        end
      end
  end
  
  def self.down
    # can't be reversed        
  end
end