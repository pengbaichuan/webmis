class AddPersistantYnToReceptions < ActiveRecord::Migration
  def self.up
    add_column :receptions, :persistent_yn, :boolean
    add_column :receptions, :removal_mark_by_username, :string
    add_column :receptions, :removed_by_username, :string
  end

  def self.down
    remove_column :receptions, :persistent_yn
    remove_column :receptions, :removal_mark_by_username
    remove_column :receptions, :removed_by_username
  end
end