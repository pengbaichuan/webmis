class CreateTheme < ActiveRecord::Migration
  def self.up
    create_table :themes do |t|
      t.string :name
      t.string :css_file
      t.text :description
      t.binary :sample_pic      
      t.boolean :active_yn
      t.boolean :default_yn
    end
    
    add_column :users, :theme_id, :integer
  end

  def self.down
    drop_table :themes
    remove_column :users, :theme_id
  end
end
