class CreateHighLevelBom < ActiveRecord::Migration
  def self.up
    create_table :high_level_boms do |t|
      t.string  :revision
      t.string  :internal_reference
      t.integer :product_id
      t.integer :product_location_id
      t.integer :inlay_reception_id
      t.integer :label_reception_id
      t.boolean :deleted,   :default => 0
      t.boolean :active,    :default => 0
      t.integer :status
      t.integer :created_by_user_id
      t.timestamps
    end
  end

  def self.down
    drop_table :high_level_boms
  end
end
