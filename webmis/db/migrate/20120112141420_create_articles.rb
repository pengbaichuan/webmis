class CreateArticles < ActiveRecord::Migration
  def self.up
    create_table :articles do |t|
      t.string  :customer_reference
      t.string  :name              
      t.string  :description       
      t.decimal :price, :precision => 8, :scale => 2             
      t.integer :high_level_bom_id  
      t.boolean :deleted,   :default => 0
      t.boolean :active,   :default => 0
      t.integer :created_by_user_id
      t.timestamps
    end
  end

  def self.down
    drop_table :articles
  end
end
