class CreateStockOrders < ActiveRecord::Migration
  def self.up
    create_table :stock_orders do |t|
      t.string :purchase_reference
      t.date :date_ordered
      t.date :confirmed_date
      t.integer :status
      t.text :remarks
      t.boolean :deleted, :default => 0

      t.timestamps
    end
  end

  def self.down
    drop_table :stock_orders
  end
end
