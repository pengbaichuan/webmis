class CreateStockOrderlines < ActiveRecord::Migration
  def self.up
    create_table :stock_orderlines do |t|
      t.integer :stock_order_id
      t.integer :article_id
      t.integer :amount_order
      t.integer :amount_confirmed
      t.string :stock_reference
      t.text :remarks
      t.integer :status
      t.boolean :deleted

      t.timestamps
    end
  end

  def self.down
    drop_table :stock_orderlines
  end
end
