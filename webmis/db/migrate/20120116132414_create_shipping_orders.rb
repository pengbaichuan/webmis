class CreateShippingOrders < ActiveRecord::Migration
  def self.up
    create_table :shipping_orders do |t|
      t.integer :article_id
      t.string  :company_id
      t.string  :po_reference
      t.integer :status
      t.boolean :invoice_send_yn
      t.binary  :attachment_org
      t.string  :source
      t.integer :initiated_by_user_id
      t.boolean :deleted

      t.timestamps
    end
  end

  def self.down
    drop_table :shipping_orders
  end
end
