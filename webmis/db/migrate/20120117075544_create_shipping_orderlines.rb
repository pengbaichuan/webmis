class CreateShippingOrderlines < ActiveRecord::Migration
  def self.up
    create_table :shipping_orderlines do |t|
      t.integer :shipping_order_id
      t.integer :stock_order_id
      t.string  :call_off_reference
      t.date    :delivery_date      
      t.integer :amount
      t.integer :status
      t.text    :remarks
      t.boolean :deleted
      t.string  :company_id
      t.integer :article_id 
      t.timestamps
    end
  end

  def self.down
    drop_table :shipping_orderlines
  end
end
