class CreateShippingOrderStatuses < ActiveRecord::Migration
  def self.up
    create_table :shipping_order_statuses do |t|
      t.integer :shipping_order_id
      t.integer :shipping_orderline_id
      t.string :level
      t.integer :user_id
      t.integer :status
      t.text :remarks

      t.timestamps
    end
  end

  def self.down
    drop_table :shipping_order_statuses
  end
end
