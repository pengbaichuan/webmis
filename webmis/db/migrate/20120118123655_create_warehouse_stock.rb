class CreateWarehouseStock < ActiveRecord::Migration
  def self.up
    create_table :warehouse_stock do |t|
      t.integer :org_id
      t.integer :stock_order_id
      t.integer :stock_orderline_id
      t.integer :shipping_order_id
      t.integer :shipping_orderline_id
      t.integer :quantity
      t.integer :org_quantity
      t.string :stock_reference
      t.integer :article_id
      t.integer :status
      t.text :remarks

      t.timestamps
    end
  end

  def self.down
    drop_table :warehouse_stock
  end
end
