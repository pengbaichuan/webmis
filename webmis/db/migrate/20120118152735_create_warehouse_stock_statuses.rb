class CreateWarehouseStockStatuses < ActiveRecord::Migration
  def self.up
    create_table :warehouse_stock_statuses do |t|
      t.integer :warehouse_stock_id
      t.integer :user_id
      t.integer :status
      t.text :remarks

      t.timestamps
    end
  end

  def self.down
    drop_table :warehouse_stock_statuses
  end
end
