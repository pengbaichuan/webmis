class CreateStockDepots < ActiveRecord::Migration
  def self.up
    create_table :stock_depots do |t|
      t.string :name
      t.string :street_name
      t.string :postal_code
      t.string :city_name
      t.string :country_name
      t.boolean :active
      t.boolean :deleted

      t.timestamps
    end
  end

  def self.down
    drop_table :stock_depots
  end
end
