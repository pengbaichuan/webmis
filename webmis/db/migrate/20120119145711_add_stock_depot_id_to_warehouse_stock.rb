class AddStockDepotIdToWarehouseStock < ActiveRecord::Migration
  def self.up
    add_column :warehouse_stock, :stock_depot_id, :integer
  end

  def self.down
    remove_column :warehouse_stock, :stock_depot_id
  end
end
