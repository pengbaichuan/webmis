class AddExternReferenceToShippingOrderline < ActiveRecord::Migration
  def self.up
     add_column :shipping_orderlines, :extern_reference, :string
     add_column :shipping_orderlines, :transport_reference, :string
  end

  def self.down
    remove_column :shipping_orderlines, :extern_reference
    remove_column :shipping_orderlines, :transport_reference
  end
end
