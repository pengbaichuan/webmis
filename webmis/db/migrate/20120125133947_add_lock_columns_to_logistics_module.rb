class AddLockColumnsToLogisticsModule < ActiveRecord::Migration
  def self.up
    add_column :shipping_orders, :lock_version, :integer, :default=>0
    add_column :shipping_orderlines, :lock_version, :integer, :default=>0
    add_column :warehouse_stock, :lock_version, :integer, :default=>0
    add_column :stock_orders, :lock_version, :integer, :default=>0
    add_column :stock_orderlines, :lock_version, :integer, :default=>0
    add_column :shipping_orderlines, :amount_requested, :integer
    add_column :stock_orders, :depot_id, :integer 
  end

  def self.down
    remove_column :shipping_orders, :lock_version
    remove_column :shipping_orderlines, :lock_version
    remove_column :warehouse_stock, :lock_version
    remove_column :stock_orders, :lock_version
    remove_column :stock_orderlines, :lock_version
    remove_column :shipping_orderlines, :amount_requested
    remove_column :stock_orders, :depot_id
  end
end
