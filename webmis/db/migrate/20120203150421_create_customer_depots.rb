class CreateCustomerDepots < ActiveRecord::Migration
  def self.up
    create_table :customer_depots do |t|
      t.string :name
      t.string :street_name
      t.string :house_number
      t.string :postal_code
      t.string :city
      t.string :country
      t.string :unloading_point
      t.string :billing_street_name
      t.string :billing_house_number
      t.string :billing_postal_code
      t.string :billing_city
      t.string :billing_country
      t.string :contact_name
      t.string :constact_salutation
      t.string :contact_phone
      t.string :contact_email

      t.timestamps
    end
  end

  def self.down
    drop_table :customer_depots
  end
end
