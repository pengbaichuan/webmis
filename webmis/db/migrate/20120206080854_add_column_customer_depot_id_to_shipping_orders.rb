class AddColumnCustomerDepotIdToShippingOrders < ActiveRecord::Migration
  def self.up
    add_column :shipping_orders, :customer_depot_id, :integer
    remove_column :shipping_orders, :company_id
    remove_column :shipping_orderlines, :company_id
  end

  def self.down
    remove_column :shipping_orders, :customer_depot_id
    add_column :shipping_orders, :company_id, :string
  end
end
