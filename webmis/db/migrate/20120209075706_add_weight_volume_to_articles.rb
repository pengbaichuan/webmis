class AddWeightVolumeToArticles < ActiveRecord::Migration
  def self.up
    add_column :articles, :supplier_reference, :string
    add_column :articles, :weight, :decimal, :precision => 8, :scale => 2   
    add_column :articles, :volume, :decimal, :precision => 8, :scale => 2   
    add_column :articles, :uom_weight, :string
    add_column :articles, :uom_volume, :string
  end

  def self.down
    remove_column :articles, :volume
    remove_column :articles, :uom_weight
    remove_column :articles, :uom_volume
    remove_column :articles, :weight
    remove_column :articles, :supplier_reference
  end
end

#update schema_migrations  set version = "20110902714300" where version = '201109027143000';