class AddLabelToCustomerDepots < ActiveRecord::Migration
  def self.up
    add_column :customer_depots, :label, :string
  end

  def self.down
    remove_column :customer_depots, :label
  end
end
