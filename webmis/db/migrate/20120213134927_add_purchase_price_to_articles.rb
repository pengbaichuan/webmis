class AddPurchasePriceToArticles < ActiveRecord::Migration
  def self.up
    add_column :articles, :purchase_price, :decimal, :precision => 8, :scale => 2             
  end

  def self.down
    remove_column :articles, :purchase_price
  end
end
