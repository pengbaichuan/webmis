class AddContactNameToStockDepots < ActiveRecord::Migration
  def self.up
    add_column :stock_depots, :contact_name, :string
    add_column :stock_depots, :contact_email, :string
  end

  def self.down
    remove_column :stock_depots, :contact_email
    remove_column :stock_depots, :contact_name
  end
end
