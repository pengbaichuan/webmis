class ChangeColumnArticlePricePrecision < ActiveRecord::Migration
  def self.up
    #scale change
    change_column :articles, :price, :decimal, :precision => 8, :scale => 3
    change_column :articles, :purchase_price, :decimal, :precision => 8, :scale => 3
  end

  def self.down
    change_column :articles, :price, :decimal, :precision => 8, :scale => 2
    change_column :articles, :purchase_price, :decimal, :precision => 8, :scale => 2
  end
end
