class AddVatIdentificationToCustomerDepots < ActiveRecord::Migration
  
  def self.up
    add_column :customer_depots, :vat_identification, :string
  end

  def self.down
    remove_column :customer_depots, :vat_identification
  end
end
