class AddInvoiceReferenceToShippingOrderlines < ActiveRecord::Migration
  def self.up
    add_column :shipping_orderlines, :invoice_reference, :string
    add_column :shipping_orderlines, :invoice_remarks, :text
  end

  def self.down
    remove_column :shipping_orderlines, :invoice_reference
    remove_column :shipping_orderlines, :invoice_remarks
  end
end
