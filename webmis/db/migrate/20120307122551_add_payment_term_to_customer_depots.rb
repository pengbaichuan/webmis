class AddPaymentTermToCustomerDepots < ActiveRecord::Migration
  def self.up
    add_column :customer_depots, :payment_term, :integer
    add_column :shipping_orderlines, :payment_due_date, :date
  end

  def self.down
    remove_column :customer_depots, :payment_term
    remove_column :shipping_orderlines, :payment_due_date
  end
end
