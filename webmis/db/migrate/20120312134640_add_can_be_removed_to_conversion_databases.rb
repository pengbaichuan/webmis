class AddCanBeRemovedToConversionDatabases < ActiveRecord::Migration
  def self.up
    add_column :conversion_databases, :can_be_removed_yn, :boolean
  end

  def self.down
    remove_column :conversion_databases, :can_be_removed_yn
  end
end
