class AddUserToTags < ActiveRecord::Migration
def self.up
     add_column :tags, :taggings_count, :integer, :default => 0, :null => false
     add_index :tags, :name
     add_index :tags, :taggings_count
 
     add_column :taggings, :user_id, :integer
    

    # Find objects for a tag
    add_index :taggings, [:tag_id, :taggable_type]
    add_index :taggings, [:user_id, :tag_id, :taggable_type]
    # Find tags for an object
    add_index :taggings, [:taggable_id, :taggable_type]
    add_index :taggings, [:user_id, :taggable_id, :taggable_type]
  end 

  def self.down
    remove_column :tags, :taggings_count
    remove_column :taggings, :user_id
  end
end
