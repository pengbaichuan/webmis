class AddIndexToDocumentVersions < ActiveRecord::Migration
  def self.up
    add_index(:document_versions, [:document_id, :version])
  end

  def self.down
    remove_index(:document_versions, [:document_id, :version])
  end
end
