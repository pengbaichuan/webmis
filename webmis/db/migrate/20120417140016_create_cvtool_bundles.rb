class CreateCvtoolBundles < ActiveRecord::Migration
  def self.up
    create_table :cvtool_bundles do |t|
      t.string :name
      t.text :remarks
      t.boolean :active_yn, :default => 1
    end
    
    add_column :conversions, :conversiontool_id, :integer
    add_column :conversions, :cvtool_bundle_id, :integer
    
  end

  def self.down
    drop_table :cvtool_bundles
    
    remove_column :conversions, :conversiontool_id
    remove_column :conversions, :cvtool_bundle_id
  end
end
