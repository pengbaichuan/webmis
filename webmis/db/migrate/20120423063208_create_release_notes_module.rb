class CreateReleaseNotesModule < ActiveRecord::Migration
  def self.up
    create_table :product_release_notes do |t|
      t.integer :production_order_id
      t.integer :user_id
      t.integer :status
      t.integer :document_id
    end
    
    create_table :content_release_notes do |t|
      t.string :title
      t.text :content
      t.integer :product_release_note_id
      t.integer :user_id
      t.integer :status
      t.integer :rank
      t.integer :subrank
      t.boolean :start_on_new_page
      t.boolean :product_list
    end
    
    create_table :default_content_release_notes do |t|
      t.string :title
      t.text :content
      t.integer :product_category_id
      t.integer :user_id
      t.integer :status
      t.boolean :start_on_new_page
      t.boolean :product_list      
    end
    
    create_table :static_release_note_titles do |t|
      t.string :title
      t.integer :status
      t.boolean :required_yn
      t.integer :product_category_id
    end
    
    add_column :documents, :production_order_id, :integer
    add_column :documents, :package_directory, :string
  end

  def self.down
    drop_table :product_release_notes
    drop_table :content_release_notes
    drop_table :default_content_release_notes
    drop_table :static_release_note_titles
    
    remove_column :documents, :production_order_id
    remove_column :documents, :package_directory
  end
end
