class AddBillingNameToCustomerdepots < ActiveRecord::Migration
  def self.up
    add_column :customer_depots, :billing_name, :string
  end

  def self.down
    remove_column :customer_depots, :billing_name
  end
end
