class AddGenericCustomerDepotToShippingOrders < ActiveRecord::Migration
  def self.up
    add_column :shipping_orders, :ordering_customer_depot_id, :integer
    add_column :shipping_orders, :invoicing_customer_depot_id, :integer
    add_column :shipping_orders, :shipping_customer_depot_id, :integer
    
    add_column :customer_depots, :ordering_yn, :boolean
    add_column :customer_depots, :shipping_yn, :boolean
    add_column :customer_depots, :invoicing_yn, :boolean
    add_column :customer_depots, :customer_code, :string
    add_column :customer_depots, :supplier_code, :string
    
    ShippingOrder.all.each do |so|
      so.ordering_customer_depot_id = so.customer_depot_id
      so.invoicing_customer_depot_id = so.customer_depot_id
      so.shipping_customer_depot_id = so.customer_depot_id
      so.save
    end
  end

  def self.down
    remove_column :shipping_orders, :ordering_customer_depot_id
    remove_column :shipping_orders, :invoicing_customer_depot_id
    remove_column :shipping_orders, :shipping_customer_depot_id

    remove_column :customer_depots, :ordering_yn
    remove_column :customer_depots, :shipping_yn
    remove_column :customer_depots, :invoicing_yn
    remove_column :customer_depots, :customer_code
    remove_column :customer_depots, :supplier_code
  end
end
