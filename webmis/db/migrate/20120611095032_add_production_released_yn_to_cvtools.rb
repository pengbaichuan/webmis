class AddProductionReleasedYnToCvtools < ActiveRecord::Migration
  def self.up
    add_column :conversiontools, :remarks, :text
    add_column :conversiontools, :prod_released_yn, :boolean
  end

  def self.down
    remove_column :conversiontools, :remarks
    remove_column :conversiontools, :prod_released_yn
  end
end
