class CreateArticleLicenses < ActiveRecord::Migration
  def self.up
    create_table :article_licenses do |t|
      t.string  :label
      t.decimal :fee, :precision => 8, :scale => 2
      t.string  :description
      t.integer :high_level_bom_id
      t.string  :external_contact_id
      t.boolean :active_yn

      t.timestamps
    end
  end

  def self.down
    drop_table :article_licenses
  end
end
