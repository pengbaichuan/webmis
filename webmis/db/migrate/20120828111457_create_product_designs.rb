class CreateProductDesigns < ActiveRecord::Migration
  def change
    create_table :product_designs do |t|
      t.string :name
      t.text :description
      t.text :model

      t.timestamps
    end
  end
end
