class CreateProductFeatures < ActiveRecord::Migration
  def change
    create_table :product_features do |t|
      t.string :name
      t.string :abbr_code
      t.text :description
      t.text :system_translation
      t.string :translation_type

      t.timestamps
    end
  end
end
