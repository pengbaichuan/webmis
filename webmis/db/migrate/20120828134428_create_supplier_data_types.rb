class CreateSupplierDataTypes < ActiveRecord::Migration
  def change
    create_table :supplier_data_types do |t|
      t.integer :company_abbr_id
      t.integer :data_type_id

      t.timestamps
    end
  end
end
