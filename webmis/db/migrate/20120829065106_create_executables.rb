class CreateExecutables < ActiveRecord::Migration
  def change
    create_table :executables do |t|
      t.string :name
      t.text :description
      t.text :input
      t.text :output
      t.text :parameters

      t.timestamps
    end
  end
end
