class AddCompanyAbbrIdToProductsetVersions < ActiveRecord::Migration
  def change
    add_column :productset_versions, :company_abbr_id, :integer
    add_column :productset_versions, :data_set_description, :string
  end
end
