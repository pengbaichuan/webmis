class RemoveTypeFromProductsets < ActiveRecord::Migration
  def up
    remove_column :productsets, :type
  end

  def down
    add_column :productsets, :type, :string
  end
end
