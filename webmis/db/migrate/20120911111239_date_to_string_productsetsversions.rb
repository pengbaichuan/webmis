class DateToStringProductsetsversions < ActiveRecord::Migration
  def up
    change_column :productset_versions, :underconstructiondate, :string
  end

  def down
    change_column :productset_versions, :underconstructiondate, :date
  end
end
