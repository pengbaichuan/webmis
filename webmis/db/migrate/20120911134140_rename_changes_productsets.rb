class RenameChangesProductsets < ActiveRecord::Migration
  def up
    rename_column :productsets, :changes, :setchanges
    rename_column :productset_versions, :changes, :setchanges
  end

  def down
    rename_column :productsets, :setchanges, :changes
    rename_column :productset_versions, :setchanges, :changes
  end
end
