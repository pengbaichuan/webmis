class AddRulesToDatatypesExecutables < ActiveRecord::Migration
  def change
    add_column :data_types,  :rules , :text
    add_column :executables,  :rules , :text
  end
end
