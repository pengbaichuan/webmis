class CreateProcessData < ActiveRecord::Migration
  def change
    create_table :process_data do |t|
      t.string :name
      t.string :storage_location
      t.integer :version
      t.integer :status
      t.text :description

      t.timestamps
    end
  end
end
