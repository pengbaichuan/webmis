class CreateProcessDataEvents < ActiveRecord::Migration
  def change
    create_table :process_data_events do |t|
      t.string :name
      t.integer :status

      t.timestamps
    end
  end
end
