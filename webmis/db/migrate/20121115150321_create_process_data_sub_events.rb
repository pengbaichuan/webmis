class CreateProcessDataSubEvents < ActiveRecord::Migration
  def change
    create_table :process_data_sub_events do |t|
      t.string :name
      t.integer :status
      t.integer :process_data_event_id

      t.timestamps
    end
  end
end
