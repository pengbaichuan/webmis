class CreateProcessDataLines < ActiveRecord::Migration
  def change
    create_table :process_data_lines do |t|
      t.integer :process_data_id
      t.integer :process_data_sub_event_id
      t.integer :reception_id
      t.integer :user_id
      t.integer :relation_line_id
      t.integer :type_relation
      t.text :log_forward
      t.text :log_backward
      t.integer :document_id
      t.integer :status

      t.timestamps
    end
  end
end
