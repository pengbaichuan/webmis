class CrReferenceToProcessDataLines < ActiveRecord::Migration
  def up
    add_column :process_data_lines,:cr_reference,:string
    add_column :process_data_lines,:auto_code,:boolean
  end

  def down
    remove_column :process_data_lines,:cr_reference
    remove_column :process_data_lines,:auto_code    
  end
end
