class AddChangedVersionToProcessDataLine < ActiveRecord::Migration
  def change
    add_column :process_data_lines, :change_version, :string
  end
end
