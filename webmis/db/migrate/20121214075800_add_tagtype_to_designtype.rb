class AddTagtypeToDesigntype < ActiveRecord::Migration
  def change
    add_column :design_tags, :tag_type, :string, :default=> 'Other'
  end
end
