class AddSourceDefTypeToExecutable < ActiveRecord::Migration
  def change
    add_column :executables, :source_data_def_type, :string
  end
end
