class RemoveBillingFieldsFromCustomerDepots < ActiveRecord::Migration
  def up
    remove_column :customer_depots, :billing_street_name
    remove_column :customer_depots, :billing_house_number
    remove_column :customer_depots, :billing_postal_code
    remove_column :customer_depots, :billing_city
    remove_column :customer_depots, :billing_name
  end

  def down
    add_column :customer_depots, :billing_street_name, :string
    add_column :customer_depots, :billing_house_number, :string
    add_column :customer_depots, :billing_postal_code, :string
    add_column :customer_depots, :billing_city, :string
    add_column :customer_depots, :billing_name, :string
  end
end
