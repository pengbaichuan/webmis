class AddVatPercentageToCustomerDepots < ActiveRecord::Migration
  def change
    add_column :customer_depots, :vat_percentage, :integer
    add_column :customer_depots, :vat_statement, :text
  end
end
