class CreatePlatformSettings < ActiveRecord::Migration
  def change
    create_table :platform_settings do |t|
      t.string :parameterset

      t.timestamps
    end
  end
end
