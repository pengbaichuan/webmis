class AddProductDesignToProduct < ActiveRecord::Migration
  def up 
    add_column :products, :product_design, :text
    add_column :product_versions, :product_design, :text
    execute "alter table products modify product_design longtext"
    execute "alter table product_versions modify product_design longtext"
  end
  def down 
    remove_column :products, :product_design, :text
    remove_column :product_versions, :product_design, :text
  end
end
