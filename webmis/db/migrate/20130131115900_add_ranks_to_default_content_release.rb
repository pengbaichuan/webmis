class AddRanksToDefaultContentRelease < ActiveRecord::Migration
  def change
    add_column :default_content_release_notes, :addition_required_yn, :boolean
    add_column :default_content_release_notes, :rank, :integer
    add_column :default_content_release_notes, :subrank, :integer
  end
end
