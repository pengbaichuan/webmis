class AddAttachPdsToProductCategory < ActiveRecord::Migration
  def change
    add_column :product_categories, :attach_pds_in_rn_yn, :boolean
  end
end
