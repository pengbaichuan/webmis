class CreateReleaseNoteAttachments < ActiveRecord::Migration
  def change
    create_table :release_note_attachments do |t|
      t.string :title
      t.boolean :active_yn
      t.boolean :removed_yn

      t.timestamps
    end
  end
end
