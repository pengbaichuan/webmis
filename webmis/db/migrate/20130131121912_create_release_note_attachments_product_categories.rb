class CreateReleaseNoteAttachmentsProductCategories < ActiveRecord::Migration
  def change
    create_table :release_note_attachments_product_categories do |t|
      t.integer :release_note_attachment_id
      t.integer :product_category_id
      t.boolean :mandatory_yn
      t.boolean :removed_yn

      t.timestamps
    end
  end
end
