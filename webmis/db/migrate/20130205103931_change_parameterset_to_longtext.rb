class ChangeParametersetToLongtext < ActiveRecord::Migration
  def up
    execute "alter table platform_settings modify parameterset longtext"
  end

  def down
    execute "alter table platform_settings modify parameterset text"
  end
end
