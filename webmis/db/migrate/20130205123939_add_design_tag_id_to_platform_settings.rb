class AddDesignTagIdToPlatformSettings < ActiveRecord::Migration
  def change
    add_column :platform_settings, :design_tag_id, :integer
  end
end
