class CreatePlatformSettingVersions < ActiveRecord::Migration
  def change
    create_table :platform_setting_versions do |t|
      t.integer :platform_setting_id
      t.integer :version
      t.string :updated_by
      t.text :parameterset
      t.integer :design_tag_id

      t.timestamps
    end
    execute 'alter table platform_setting_versions modify parameterset longtext'
    add_column :platform_settings, :version, :integer, :default => 1
  end
end
