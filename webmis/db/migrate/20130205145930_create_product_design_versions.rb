class CreateProductDesignVersions < ActiveRecord::Migration
  def change
    create_table :product_design_versions do |t|
      t.integer :product_design_id
      t.integer :version
      t.string :updated_by
      t.integer :design_tag_id
      t.string :name
      t.text :description
      t.text :model
      t.integer :platform_setting_id

      t.timestamps
    end
    execute 'alter table product_design_versions modify model longtext'
    add_column :product_designs, :version, :integer
  end
end
