class CreateExecutableVersions < ActiveRecord::Migration
  def change
    create_table :executable_versions do |t|
      t.integer :executable_id
      t.integer :version
      t.string :updated_by
      t.string :name
      t.text :description
      t.text :input
      t.text :output
      t.text :parameters

      t.timestamps
    end
    execute 'alter table executable_versions modify input longtext'
    execute 'alter table executable_versions modify output longtext'
    execute 'alter table executable_versions modify parameters longtext'
    add_column :executables, :version, :integer
  end
end
