class CreateProductFeatureVersions < ActiveRecord::Migration
  def change
    create_table :product_feature_versions do |t|
      t.integer :product_feature_id
      t.integer :version
      t.string :updated_by
      t.string :name
      t.text :description
      t.string :abbr_code
      t.text :system_translation
      t.string :translation_type

      t.timestamps
    end
    execute 'alter table product_feature_versions modify column system_translation longtext'
    add_column :product_features, :version, :integer
  end
end
