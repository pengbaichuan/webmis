class AddUpdatedBy < ActiveRecord::Migration
  def change
    add_column :platform_settings, :updated_by, :string
    add_column :product_features, :updated_by, :string
    add_column :executables, :updated_by, :string
    add_column :product_designs, :updated_by, :string
  end

end
