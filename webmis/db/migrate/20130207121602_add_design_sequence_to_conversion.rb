class AddDesignSequenceToConversion < ActiveRecord::Migration
  def change
    add_column :conversions, :design_sequence, :integer
  end
end
