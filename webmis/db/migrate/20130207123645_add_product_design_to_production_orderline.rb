class AddProductDesignToProductionOrderline < ActiveRecord::Migration
  def change
        add_column :production_orderlines, :product_design, :text
        execute "alter table production_orderlines modify product_design longtext"
  end

  def down
        remove_column :production_orderlines, :product_design
  end

end
