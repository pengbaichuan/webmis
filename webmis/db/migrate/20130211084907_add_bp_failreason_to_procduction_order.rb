class AddBpFailreasonToProcductionOrder < ActiveRecord::Migration
  def change
    add_column :production_orders, :bp_failreason, :string
    add_column :production_orders, :bp_name, :string
    add_column :production_orders, :bp_status, :string
    add_column :production_orderlines, :bp_failreason, :string
  end
end
