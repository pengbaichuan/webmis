class AddDesigntagIdToProductDesign < ActiveRecord::Migration
  def change
    add_column :product_designs, :design_tag_id, :integer
  end
end
