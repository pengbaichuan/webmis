class AddAdditionRequiredToContentRelease < ActiveRecord::Migration
  def change
    add_column :content_release_notes, :addition_required_yn, :boolean
    add_column :content_release_notes, :created_at, :date
    add_column :content_release_notes, :updated_at, :date
  end
end