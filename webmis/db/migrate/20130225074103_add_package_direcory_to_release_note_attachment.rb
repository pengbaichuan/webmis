class AddPackageDirecoryToReleaseNoteAttachment < ActiveRecord::Migration
  def change
    add_column :release_note_attachments, :package_directory, :string
    add_column :release_note_attachments, :description, :string
  end
end
