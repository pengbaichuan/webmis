class AddMandatoryYnToDocument < ActiveRecord::Migration
  def change
    add_column :documents, :mandatory_yn, :boolean
  end
end
