class AddSubprocessToExecutable < ActiveRecord::Migration
  def change
    add_column :executables, :subprocs, :string
  end
end
