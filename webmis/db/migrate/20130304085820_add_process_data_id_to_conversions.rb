class AddProcessDataIdToConversions < ActiveRecord::Migration
  def change
    add_column :conversions, :process_data_id, :integer
  end
end
