class AddStatusToPlatformSettings < ActiveRecord::Migration
  def change
    add_column :platform_settings, :status, :string
  end
end
