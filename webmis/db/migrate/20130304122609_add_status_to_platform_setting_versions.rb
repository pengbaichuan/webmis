class AddStatusToPlatformSettingVersions < ActiveRecord::Migration
  def change
    add_column :platform_setting_versions, :status, :string
  end
end
