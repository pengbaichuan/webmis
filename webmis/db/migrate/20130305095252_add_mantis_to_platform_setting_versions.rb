class AddMantisToPlatformSettingVersions < ActiveRecord::Migration
  def change
    add_column :platform_setting_versions, :mantis, :integer
    add_column :platform_setting_versions, :comment, :text
  end
end
