class CreateIncludedDatabaseTestRequests < ActiveRecord::Migration
  def change
    create_table :included_database_test_requests do |t|
      t.integer :test_request_id
      t.integer :conversion_database_id

      t.timestamps
    end
  end
end
