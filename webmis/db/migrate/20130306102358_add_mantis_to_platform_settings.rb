class AddMantisToPlatformSettings < ActiveRecord::Migration
  def change
    add_column :platform_settings, :mantis, :integer
    add_column :platform_settings, :comment, :text
  end
end
