class AddStatusfieldsToConversiondatabase < ActiveRecord::Migration
  def change
     add_column :conversion_databases, :starttime, :datetime
     add_column :conversion_databases, :finishtime, :datetime
     add_column :conversion_databases, :runstatus, :string
     add_column :conversion_databases, :failreason, :text
     add_column :conversion_databases, :process_location, :string
 
  end
end
