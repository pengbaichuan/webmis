class AddRuntimeInfoToConversionDatabase < ActiveRecord::Migration
  def change
    add_column :conversion_databases, :run_script, :text        
    add_column :conversion_databases, :run_command, :string
    add_column :conversion_databases, :run_pid,  :string
  end
end
