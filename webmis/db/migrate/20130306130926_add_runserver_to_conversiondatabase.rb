class AddRunserverToConversiondatabase < ActiveRecord::Migration
  def change
    add_column :conversion_databases, :run_server, :string
  end
end
