class AddRefTestToIncludedDatabaseTestRequest < ActiveRecord::Migration
  def change
    add_column :included_database_test_requests, :test_yn, :boolean
    add_column :included_database_test_requests, :ref_yn, :boolean
  end
end
