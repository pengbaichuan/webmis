class AddMantisToProductDesigns < ActiveRecord::Migration
  def change
    add_column :product_designs, :mantis, :integer
    add_column :product_designs, :comment, :text
  end
end
