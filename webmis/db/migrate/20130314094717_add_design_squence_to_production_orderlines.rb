class AddDesignSquenceToProductionOrderlines < ActiveRecord::Migration
  def change
    add_column :production_orderlines, :design_sequence, :integer
  end
end
