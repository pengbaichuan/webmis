class AddStatusToProductDesigns < ActiveRecord::Migration
  def change
    add_column :product_designs, :status, :string
  end
end
