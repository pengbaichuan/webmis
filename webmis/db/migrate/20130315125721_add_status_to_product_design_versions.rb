class AddStatusToProductDesignVersions < ActiveRecord::Migration
  def change
    add_column :product_design_versions, :status, :string
  end
end
