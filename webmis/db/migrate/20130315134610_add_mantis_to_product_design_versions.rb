class AddMantisToProductDesignVersions < ActiveRecord::Migration
  def change
    add_column :product_design_versions, :mantis, :integer
    add_column :product_design_versions, :comment, :text
  end
end
