class AddMantisToProductFeatureVersions < ActiveRecord::Migration
  def change
    add_column :product_feature_versions, :mantis, :integer
    add_column :product_feature_versions, :comment, :text
    add_column :product_feature_versions, :status, :string
  end
end
