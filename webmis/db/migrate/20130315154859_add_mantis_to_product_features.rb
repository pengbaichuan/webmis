class AddMantisToProductFeatures < ActiveRecord::Migration
  def change
    add_column :product_features, :mantis, :integer
    add_column :product_features, :comment, :text
    add_column :product_features, :status, :string
  end
end
