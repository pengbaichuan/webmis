class AddJobIdToConversionDatabase < ActiveRecord::Migration
  def change
    add_column :conversion_databases, :job_id, :integer
  end
end
