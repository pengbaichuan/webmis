class AddProductidIndexToProductionOrderline < ActiveRecord::Migration
  def change
    add_index(:production_orderlines, :product_id)
  end
end
