class AddChecksumToProductLocations < ActiveRecord::Migration
  def change
    add_column :product_locations, :checksum_storage_file, :string
  end
end
