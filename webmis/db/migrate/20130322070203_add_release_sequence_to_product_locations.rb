class AddReleaseSequenceToProductLocations < ActiveRecord::Migration
  def change
    add_column :product_locations, :release_sequence, :integer
  end
end
