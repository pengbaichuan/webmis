class AddReleaseMailSendToOutboundOrders < ActiveRecord::Migration
  def change
    add_column :outbound_orders, :release_mail_send, :boolean
    add_column :outbound_orders, :distribution_list_member_id, :integer
    add_column :outbound_orders, :release_mail_send_date, :date
  end
end
