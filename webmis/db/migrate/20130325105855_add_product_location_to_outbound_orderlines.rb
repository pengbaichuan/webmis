class AddProductLocationToOutboundOrderlines < ActiveRecord::Migration
  def change
    add_column :outbound_orderlines, :product_location_id, :integer
  end
end
