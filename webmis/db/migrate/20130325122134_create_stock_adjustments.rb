class CreateStockAdjustments < ActiveRecord::Migration
  def change
    create_table :warehouse_stock_adjustments do |t|
      t.integer :article_id
      t.string :stock_order_id
      t.integer :quantity
      t.text :reason
      t.integer :related_stock_adjustment_id
      t.integer :status

      t.timestamps
    end
  end
add_column :warehouse_stock, :warehouse_stock_adjustment_id, :integer
end
