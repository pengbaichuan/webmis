class AddFtpAccountToOutboundOrders < ActiveRecord::Migration
  def change
    add_column :outbound_orders, :file_transfer_account_id, :integer
  end
end
