class AddShippingParentIdToOutboundOrders < ActiveRecord::Migration
  def change
    add_column :outbound_orders, :shipping_parent_id, :integer
  end
end
