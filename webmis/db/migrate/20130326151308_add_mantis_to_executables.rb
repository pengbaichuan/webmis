class AddMantisToExecutables < ActiveRecord::Migration
  def change
    add_column :executables, :mantis, :integer
    add_column :executables, :comment, :text
    add_column :executables, :status, :string
  end
end
