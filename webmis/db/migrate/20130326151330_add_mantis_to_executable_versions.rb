class AddMantisToExecutableVersions < ActiveRecord::Migration
  def change
    add_column :executable_versions, :mantis, :integer
    add_column :executable_versions, :comment, :text
    add_column :executable_versions, :status, :string
  end
end
