class CreateConfigParameters < ActiveRecord::Migration
  def change
    create_table :config_parameters do |t|
      t.string :cfg_name
      t.string :cfg_type
      t.text :cfg_value, :limit => 4294967295 #longtext
      t.text :description
      t.string :cfg_group
      t.integer :version, :default => 1
      t.timestamps
    end
    
    #version_fu
    create_table :config_parameter_versions do |t|
      t.integer :config_parameter_id
      t.integer :version
      t.text :cfg_value, :limit => 4294967295 #longtext
      t.string :cfg_group
      t.timestamps
    end    
  end
end
