class AddParameterMyCompanyNameToConfigParameters < ActiveRecord::Migration
  require 'version_fu'
  
  def self.up
    n = ConfigParameter.new(:cfg_name => "my_company_name", :cfg_value => "Mapscape", :description => 'Current Company name')
    n.save
  end
  def self.down
    ConfigParameter.delete_all(:cfg_name => "my_company_name")
  end
end
