class AddPlatformSettingToProductFeatures < ActiveRecord::Migration
  def change
    add_column :product_features, :platform_setting_id, :integer
    add_column :product_features, :platform_setting_version, :integer
  end
end
