class AddPlatformSettingToProductFeatureVersions < ActiveRecord::Migration
  def change
    add_column :product_feature_versions, :platform_setting_id, :integer
    add_column :product_feature_versions, :platform_setting_version, :integer
  end
end
