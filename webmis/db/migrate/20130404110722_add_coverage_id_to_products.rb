class AddCoverageIdToProducts < ActiveRecord::Migration
  def change
    add_column :products, :primary_coverage_id, :integer
    add_column :products, :secondary_coverage_id, :integer
    add_column :product_versions, :primary_coverage_id, :integer
    add_column :product_versions, :secondary_coverage_id, :integer    
  end
end
