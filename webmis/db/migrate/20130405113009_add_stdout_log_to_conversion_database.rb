class AddStdoutLogToConversionDatabase < ActiveRecord::Migration
  def change
    add_column :conversion_databases, :stdout_log, :text, :limit => 4294967295 
  end
end
