class AddStatusToCoverage < ActiveRecord::Migration
  def change
    add_column :coverages, :status, :integer
  end
end
