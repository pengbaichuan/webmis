class AddPlatformSettingVersionToProductDesigns < ActiveRecord::Migration
  def change
    add_column :product_designs, :platform_setting_version, :integer
  end
end
