class AddPlatformSettingVersionToProductDesignVersions < ActiveRecord::Migration
  def change
    add_column :product_design_versions, :platform_setting_version, :integer
  end
end
