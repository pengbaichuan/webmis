class AddStatusToProduct < ActiveRecord::Migration
  def change
    add_column :products, :status, :integer
    add_column :product_versions, :status, :integer
  end
end
