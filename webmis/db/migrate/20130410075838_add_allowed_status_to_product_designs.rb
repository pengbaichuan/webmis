class AddAllowedStatusToProductDesigns < ActiveRecord::Migration
  def up
    add_column :product_designs, :allowed_status, :string
    add_column :product_design_versions, :allowed_status, :string
    ["executables", "executable_versions", "platform_settings", "platform_setting_versions", "product_features", "product_feature_versions", "product_designs", "product_design_versions"].each do |t|
      sql="update #{t} set status='obsolete' where status='deleted'"
      ActiveRecord::Base.connection.execute(sql)
    end
  end

  def down
    remove_column :product_designs, :allowed_status, :string
    remove_column :product_design_versions, :allowed_status, :string
    ["executables", "executable_versions", "platform_settings", "platform_setting_versions", "product_features", "product_feature_versions", "product_designs", "product_design_versions"].each do |t|
      sql="update #{t} set status='deleted' where status='obsolete'"
      ActiveRecord::Base.connection.execute(sql)
    end
  end
end
