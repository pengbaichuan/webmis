class AddIndexToRegion < ActiveRecord::Migration
  def change
    add_index :regions, [:name,:country_iso_code,:country_iso3_code]
  end
end
