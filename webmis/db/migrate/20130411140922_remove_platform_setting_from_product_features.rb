class RemovePlatformSettingFromProductFeatures < ActiveRecord::Migration
  # wrong classname, but since it is only a name I haven't bothered resolving this.
  def up
    remove_column :product_designs, :platform_setting_id
    remove_column :product_designs, :platform_setting_version
    remove_column :product_design_versions, :platform_setting_id
    remove_column :product_design_versions, :platform_setting_version
  end

  def down
    add_column :product_designs, :platform_setting_version, :integer
    add_column :product_designs, :platform_setting_id, :integer
    add_column :product_design_versions, :platform_setting_version, :integer
    add_column :product_design_versions, :platform_setting_id, :integer
  end
end
