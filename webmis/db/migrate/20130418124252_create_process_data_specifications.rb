class CreateProcessDataSpecifications < ActiveRecord::Migration
  def change
    create_table :process_data_specifications do |t|
      t.string :name
      t.string :directory
      t.string :script_tag
      t.string :script_parameter
      t.text :description
      t.boolean :directory_level_yn
      t.boolean :active_yn

      t.timestamps
    end
  end
end
