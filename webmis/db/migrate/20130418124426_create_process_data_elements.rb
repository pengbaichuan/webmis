class CreateProcessDataElements < ActiveRecord::Migration
  def change
    create_table :process_data_elements do |t|
      t.integer :process_data_item_id
      t.integer :reception_id
      t.integer :process_data_specification_id
      t.text :remark

      t.timestamps
    end
  end
end
