class CreateProcessDataItems < ActiveRecord::Migration
  def change
    create_table :process_data_items do |t|
      t.integer :process_data_id

      t.timestamps
    end
  end
end
