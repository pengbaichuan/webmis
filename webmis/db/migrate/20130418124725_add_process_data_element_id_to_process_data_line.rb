class AddProcessDataElementIdToProcessDataLine < ActiveRecord::Migration
  def change
    add_column :process_data_lines, :process_data_element_id, :integer
  end
end
