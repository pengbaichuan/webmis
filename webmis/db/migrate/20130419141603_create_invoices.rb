class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.integer :status
      t.string :reference
      t.text :remarks
      t.integer :shipping_order_id
      t.date :date
      t.date :due_date
      t.string :currency
      t.integer :vat_percentage
      t.decimal :vat, precision: 8, scale: 3
      t.decimal :total, precision: 8, scale: 3

      t.timestamps
    end
  end
end
