class AddInvoiceToShippingOrderlines < ActiveRecord::Migration
  def change
    add_column :shipping_orderlines, :invoice_id, :integer
  end
end
