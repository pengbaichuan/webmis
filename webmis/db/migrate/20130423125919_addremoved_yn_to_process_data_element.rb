class AddremovedYnToProcessDataElement < ActiveRecord::Migration
  def change
    add_column :process_data_elements, :removed_yn, :boolean,:default => false
  end
end
