# encoding: utf-8

class AddCurrencyToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :currency, :string, default: "€"
    add_column :article_licenses, :currency, :string, default: "€"
  end
end
