class AddParameterCrToolUrlToConfigParameters < ActiveRecord::Migration
  require 'version_fu'
  
  def self.up
    n = ConfigParameter.new(:cfg_name => "cr_tool_url", :cfg_value => 'https://mantis.mapscape.nl/mapscape/view.php?id=', :description => 'URL and Id-prefix for current CR-tool.')
    n.save
  end
  def self.down
    ConfigParameter.delete_all(:cfg_name => "cr_tool_url")
  end
end
