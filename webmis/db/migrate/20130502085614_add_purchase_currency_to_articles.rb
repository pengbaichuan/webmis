# encoding: utf-8

class AddPurchaseCurrencyToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :purchase_currency, :string, default: "€"
  end
end
