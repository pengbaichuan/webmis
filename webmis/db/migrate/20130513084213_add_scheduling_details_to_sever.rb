class AddSchedulingDetailsToSever < ActiveRecord::Migration
  def change
    add_column :servers,  :max_jobs, :integer
    add_column :servers,  :max_work, :integer
    add_column :servers,  :scheduling_allowed_yn, :integer
  end
end
