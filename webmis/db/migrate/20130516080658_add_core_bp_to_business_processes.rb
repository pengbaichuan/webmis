class AddCoreBpToBusinessProcesses < ActiveRecord::Migration
  def change
    add_column :business_processes, :core_bp_yn, :boolean
  end
end
