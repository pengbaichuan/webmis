class AddRequestUserIdToConversions < ActiveRecord::Migration
  def change
    add_column :conversions, :request_user_id, :integer
  end
end
