class AddChosenConversionIdToProductionOrderlines < ActiveRecord::Migration
  def change
    add_column :production_orderlines, :chosen_conversion_id, :integer
  end
end
