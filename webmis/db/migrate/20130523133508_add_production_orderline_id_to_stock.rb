class AddProductionOrderlineIdToStock < ActiveRecord::Migration
  def change
    add_column :stock, :production_orderline_id, :integer
  end
end
