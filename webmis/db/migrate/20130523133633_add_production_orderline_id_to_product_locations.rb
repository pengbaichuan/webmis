class AddProductionOrderlineIdToProductLocations < ActiveRecord::Migration
  def change
    add_column :product_locations, :production_orderline_id, :integer
  end
end
