class AddProductionOrderlineIdToOutboundOrderlines < ActiveRecord::Migration
  def change
    add_column :outbound_orderlines, :production_orderline_id, :integer
  end
end
