class AddParameterDeliveryTeamEmailToConfigParameters < ActiveRecord::Migration
  def self.up
    n = ConfigParameter.new(:cfg_name => "delivery_team_email_string", :cfg_value => 'Delivery-Team Mapscape  <delivery-team@mapscape.eu>', :description => 'E-mail and Alias for the shipping team.')
    n.save
  end
  def self.down
    ConfigParameter.delete_all(:cfg_name => "delivery_team_email_string")
  end
end
