class AddParameterDefaulRoleNameToConfigParameters < ActiveRecord::Migration
  def self.up
    n = ConfigParameter.new(:cfg_name => "default_role_name", :cfg_value => 'read only', :description => 'Default role for new users.')
    n.save
  end
  def self.down
    ConfigParameter.delete_all(:cfg_name => "default_role_name")
  end
end
