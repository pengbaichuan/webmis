class AddLinktoControllerActionToBusinessProcesses < ActiveRecord::Migration
  def change
    add_column :business_processes, :header_link_controller, :string
    add_column :business_processes, :header_link_action, :string
    add_column :business_processes, :line_link_controller, :string
    add_column :business_processes, :line_link_action, :string
  end
end
