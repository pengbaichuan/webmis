class AddParameterDataInPutDirectoryToConfigParameters < ActiveRecord::Migration
  def self.up
    n = ConfigParameter.new(:cfg_name => "data_input_directory", :cfg_value => '/data/input/', :description => 'Directory name where the conversion input data is stored.')
    n.save
  end
  def self.down
    ConfigParameter.delete_all(:cfg_name => "data_input_directory")
  end
end
