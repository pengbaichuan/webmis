class AddToolReleaseToExecutables < ActiveRecord::Migration
  def change
    add_column :executables, :tool_name, :string
    add_column :executables, :release, :string
  end
end
