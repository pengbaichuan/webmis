class AddToolReleaseToExecutableVersions < ActiveRecord::Migration
  def change
    add_column :executable_versions, :tool_name, :string
    add_column :executable_versions, :release, :string
  end
end
