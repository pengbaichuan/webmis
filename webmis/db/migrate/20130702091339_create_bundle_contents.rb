class CreateBundleContents < ActiveRecord::Migration
  def change
    create_table :bundle_contents do |t|
      t.string :bundles_name
      t.string :bundle_name
      t.string :release
      t.string :executable_name
      t.string :tool_release

      t.timestamps
    end
  end
end
