class RenameBpFailreasonToBpMessageProductionOrderlines < ActiveRecord::Migration
  def up
    rename_column :production_orderlines, :bp_failreason, :bp_message
  end
end
