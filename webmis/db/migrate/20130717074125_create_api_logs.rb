class CreateApiLogs < ActiveRecord::Migration
  def change
    create_table :api_logs do |t|
      t.string :ip
      t.string :session
      t.string :user
      t.string :level
      t.string :controller
      t.string :action
      t.text :message

      t.timestamps
    end
  end
end
