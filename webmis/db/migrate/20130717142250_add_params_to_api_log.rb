class AddParamsToApiLog < ActiveRecord::Migration
  def change
    add_column :api_logs, :params, :text
  end
end
