class AddServerColumn < ActiveRecord::Migration
  def change
    add_column :servers, :cores, :integer
    add_column :servers, :memory, :integer
    add_column :servers, :virtual_host, :string
  end
end
