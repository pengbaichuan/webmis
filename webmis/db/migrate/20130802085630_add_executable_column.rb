class AddExecutableColumn < ActiveRecord::Migration
  def change
    add_column :executables, :schedule_manual, :boolean, null: false, default: 1
    add_column :executables, :schedule_cores, :integer
    add_column :executables, :schedule_memory_base, :integer
    add_column :executables, :schedule_memory_times_input, :integer
    add_column :executables, :schedule_diskspace_base, :integer
    add_column :executables, :schedule_diskspace_times_input, :integer
    add_column :executables, :schedule_allow_virtual, :boolean
    add_column :executables, :schedule_exclusive, :boolean

    add_column :executable_versions, :schedule_manual, :boolean, null: false, default: 1
    add_column :executable_versions, :schedule_cores, :integer
    add_column :executable_versions, :schedule_memory_base, :integer
    add_column :executable_versions, :schedule_memory_times_input, :integer
    add_column :executable_versions, :schedule_diskspace_base, :integer
    add_column :executable_versions, :schedule_diskspace_times_input, :integer
    add_column :executable_versions, :schedule_allow_virtual, :boolean
    add_column :executable_versions, :schedule_exclusive, :boolean
  end
end
