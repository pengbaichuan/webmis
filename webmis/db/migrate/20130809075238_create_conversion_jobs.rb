class CreateConversionJobs < ActiveRecord::Migration
  def change
    create_table :conversion_jobs do |t|
      t.string    :conversion_id
      t.string    :run_status
      t.string    :run_server
      t.string    :run_command
      t.text      :run_script
      t.string    :run_pid
      t.string    :working_dir
      t.datetime  :start_time
      t.datetime  :finish_time
      t.text      :stdout_log
      t.integer   :exitcode
      t.string    :fail_reason
      t.boolean   :schedule_manual
      t.integer   :schedule_cores
      t.integer   :schedule_memory
      t.integer   :schedule_diskspace
      t.boolean   :schedule_allow_virtual
      t.boolean   :schedule_exclusive

      t.timestamps
    end

    remove_column :conversion_databases, :starttime
    remove_column :conversion_databases, :finishtime
    remove_column :conversion_databases, :runstatus
    remove_column :conversion_databases, :failreason
    remove_column :conversion_databases, :process_location
    remove_column :conversion_databases, :run_script
    remove_column :conversion_databases, :run_command
    remove_column :conversion_databases, :run_pid
    remove_column :conversion_databases, :run_server
    remove_column :conversion_databases, :job_id
    remove_column :conversion_databases, :stdout_log
    remove_column :conversion_databases, :exitcode
  end

end
