class AddConversionJobId < ActiveRecord::Migration
  def change
    add_column :conversion_databases, :conversion_job_id, :integer
  end
end
