class CreatePoiMappingLayouts < ActiveRecord::Migration
  def change
    create_table :poi_mapping_layouts do |t|
      t.integer :supplier_company_abbr_id
      t.integer :region_id
      t.integer :customer_company_abbr_id
      t.integer :output_data_type_id
      t.integer :input_data_type_id
      t.string :definition_version
      t.text :description
      t.text :import_hash

      t.timestamps
    end
  end
end
