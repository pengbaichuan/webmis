class CreatePoiMappings < ActiveRecord::Migration
  def change
    create_table :poi_mappings do |t|
      t.integer :poi_mapping_layout_id
      t.string :map_or_filter_type
      t.string :map_or_filter_value
      t.string :logical_type
      t.boolean :invert, :default => false
      t.integer :group_id
      t.integer :output_category
      t.boolean :major_filtering, :default => false
      t.text :input_description
      t.text :output_description
      t.text :import_hash

      t.timestamps
    end
  end
end
