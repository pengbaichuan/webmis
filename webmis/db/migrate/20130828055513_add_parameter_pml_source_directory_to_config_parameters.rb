class AddParameterPmlSourceDirectoryToConfigParameters < ActiveRecord::Migration
  def self.up
    n = ConfigParameter.new(:cfg_name => "pml_source_directory", :cfg_value => '/data/newgroups/Product_Management/PML/reception/', :description => 'Directory name where the PML-file are stored for import.')
    n.save
  end
  def self.down
    ConfigParameter.delete_all(:cfg_name => "pml_source_directory")
  end
end
