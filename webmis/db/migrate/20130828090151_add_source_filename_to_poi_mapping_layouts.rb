class AddSourceFilenameToPoiMappingLayouts < ActiveRecord::Migration
  def change
    add_column :poi_mapping_layouts, :source_filename, :string
  end
end
