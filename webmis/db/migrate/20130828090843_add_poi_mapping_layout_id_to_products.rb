class AddPoiMappingLayoutIdToProducts < ActiveRecord::Migration
  def change
    add_column :products, :poi_mapping_layout_id, :integer
  end
end
