class AddPoiMappingLayoutIdToProductVersions < ActiveRecord::Migration
  def change
    add_column :product_versions, :poi_mapping_layout_id, :integer
  end
end
