class CreateRegionMappingHeaders < ActiveRecord::Migration
  def change
    create_table :region_mapping_headers do |t|
      t.string :name
      t.string :sync_filename
      t.integer :version

      t.timestamps
    end
  end
end
