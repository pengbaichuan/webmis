class CreateRegionMappingHeaderVersions < ActiveRecord::Migration
  def change
    create_table :region_mapping_header_versions do |t|
      t.integer :region_mapping_header_id
      t.string :name
      t.string :sync_filename
      t.integer :version

      t.timestamps
    end
  end
end
