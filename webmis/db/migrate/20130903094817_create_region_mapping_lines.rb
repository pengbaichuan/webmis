class CreateRegionMappingLines < ActiveRecord::Migration
  def change
    create_table :region_mapping_lines do |t|
      t.string :region_mapping_header_id
      t.integer :source_data_type_id
      t.integer :source_data_release_id
      t.integer :source_region_code_id
      t.string :destination_region_code
      t.integer :version

      t.timestamps
    end
  end
end
