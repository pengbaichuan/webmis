class AddParameterSharedTempDirToConfigParameters < ActiveRecord::Migration
  def self.up
    n = ConfigParameter.new(:cfg_name => "shared_temp_dir", :cfg_value => '/data/input/.backoffice_tmp/', :description => 'Directory name for shared files wich can be used in whole mapscape file system.')
    n.save
  end
  def self.down
    ConfigParameter.delete_all(:cfg_name => "shared_temp_dir")
  end
end
