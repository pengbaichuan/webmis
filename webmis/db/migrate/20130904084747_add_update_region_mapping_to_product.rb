class AddUpdateRegionMappingToProduct < ActiveRecord::Migration
  def change
    add_column :products, :update_region_mapping_id, :integer
  end
end
