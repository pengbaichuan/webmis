class AddUpdateRegionMappingToProductVersions < ActiveRecord::Migration
  def change
    add_column :product_versions, :update_region_mapping_id, :integer
  end
end
