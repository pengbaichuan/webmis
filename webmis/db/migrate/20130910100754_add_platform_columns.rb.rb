class AddPlatformColumns < ActiveRecord::Migration
  def up
    rename_table :platforms, :target_platforms
    rename_table :platform_settings, :parameter_settings
    rename_table :platform_setting_versions, :parameter_setting_versions
    rename_column :parameter_setting_versions, :platform_setting_id, :parameter_setting_id
    add_column :target_platforms, :company_abbr_id, :integer
    add_column :target_platforms, :parameter_setting_id, :integer
    add_column :target_platforms, :is_active, :boolean, default: 1
    add_column :parameter_settings, :name, :string
    add_column :parameter_setting_versions, :name, :string
    sql="update taggings set taggable_type='ParameterSetting' where taggable_type='PlatformSetting'"
    ActiveRecord::Base.connection.execute(sql)
    TargetPlatform.all.each do |x|
      x.is_active = 0
      x.save
    end
    ParameterSetting.all.each do |x|
      x.name = x.platform_list.to_s
      x.save

      # abbr needs to be unique
      if x.status == 'obsolete'
        abbr = x.name + "-obsolete-" + x.id.to_s
        is_active = 0
      else
        abbr = x.name
        is_active = 1
      end
      y = TargetPlatform.new(abbr: abbr, name: x.name, parameter_setting_id: x.id, is_active: is_active)
      y.save
    end
    ParameterSettingVersion.all.each do |x|
      y = ParameterSetting.find(x.parameter_setting_id)
      x.name = y.name
      x.save
    end
  end

  def down
    sql="update taggings set taggable_type='PlatformSetting' where taggable_type='ParameterSetting'"
    ActiveRecord::Base.connection.execute(sql)
    TargetPlatform.where('parameter_setting_id is not null').delete_all
    remove_column :target_platforms, :company_abbr_id
    remove_column :target_platforms, :parameter_setting_id
    remove_column :target_platforms, :is_active
    remove_column :parameter_settings, :name
    remove_column :parameter_setting_versions, :name
    rename_column :parameter_setting_versions, :parameter_setting_id, :platform_setting_id
    rename_table :parameter_setting_versions, :platform_setting_versions
    rename_table :parameter_settings, :platform_settings
    rename_table :target_platforms, :targets
   end
end
