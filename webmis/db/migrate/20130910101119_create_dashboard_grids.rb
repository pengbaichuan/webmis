class CreateDashboardGrids < ActiveRecord::Migration
  def change
    create_table :dashboard_grids do |t|
      t.integer :user_id
      t.string :name

      t.timestamps
    end
  end
end
