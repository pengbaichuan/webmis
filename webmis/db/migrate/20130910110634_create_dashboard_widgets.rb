class CreateDashboardWidgets < ActiveRecord::Migration
  def change
    create_table :dashboard_widgets do |t|
      t.string :width_css_class
      t.string :name

      t.timestamps
    end
  end
end
