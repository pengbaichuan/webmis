class CreateDashboardGridWidgets < ActiveRecord::Migration
  def change
    create_table :dashboard_grid_widgets do |t|
      t.integer :dashboard_widget_id
      t.integer :dashboard_grid_id
      t.integer :position

      t.timestamps
    end
  end
end
