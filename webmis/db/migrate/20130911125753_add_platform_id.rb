class AddPlatformId < ActiveRecord::Migration
  def up
    add_column :product_features, :target_platform_id, :integer
    add_column :product_feature_versions, :target_platform_id, :integer
    ProductFeature.where("platform_setting_id is not null").each do |x|
      target_platform = TargetPlatform.where("parameter_setting_id = #{x.platform_setting_id}").first
      x.target_platform_id = target_platform.id if target_platform
      x.save
      ProductFeatureVersion.where("product_feature_id = #{x.id} and target_platform_id is null").each do |y|
        y.target_platform_id = x.target_platform_id
        y.save
      end
    end
    ProductDesign.where("model like '%platform%'").each do |x|
      modelhash = JSON.parse(x.model)
      if modelhash["platform"]
        parameter_setting_id = modelhash["platform"]["id"]
        target_platform = TargetPlatform.where("parameter_setting_id = #{parameter_setting_id}").first if parameter_setting_id
        hash = {
          id:   target_platform ? target_platform.id : nil,
          abbr: target_platform ? target_platform.abbr : nil,
          parameter_setting_id: parameter_setting_id,
          parameter_setting_version: modelhash["platform"]["version"],
          parameter_setting_version_id: modelhash["platform"]["version_id"],
          parameter_setting_status: modelhash["platform"]["status"],
          parameter_setting_name: target_platform ? target_platform.name : nil,
          obsolete_design_tag_id: modelhash["platform"]["design_tag_id"],
          obsolete_tags: modelhash["platform"]["tags"],
          parameter_settings: modelhash["platform"]["parameter_settings"]
        }
        modelhash.delete("platform")
        modelhash["target_platform"] = hash
        x.modelhash = modelhash
        x.model = modelhash.to_json
        x.status = 'obsolete' unless x.status
        x.save
      end
    end 

    # remove_column :product_features, :platform_setting_id
    # remove_column :product_features, :platform_setting_version
    # remove_column :product_feature_versions, :platform_setting_id
    # remove_column :product_feature_versions, :platform_setting_version
  end

  def down
    # add_column :product_features, :platform_setting_id, :integer
    # add_column :product_features, :platform_setting_version, :integer
    # add_column :product_feature_versions, :platform_setting_id, :integer
    # add_column :product_feature_versions, :platform_setting_version, :integer

    ProductDesign.where("model like '%platform%'").each do |x|
      modelhash = JSON.parse(x.model)
      if modelhash["target_platform"]
        hash = {
          id: modelhash["target_platform"]["parameter_setting_id"],
          version: modelhash["target_platform"]["parameter_setting_version"],
          version_id: modelhash["target_platform"]["parameter_setting_version_id"],
          status: modelhash["target_platform"]["parameter_setting_status"],
          design_tag_id: modelhash["target_platform"]["obsolete_design_tag_id"],
          tags: modelhash["target_platform"]["obsolete_tags"],
          parameter_settings: modelhash["target_platform"]["parameter_settings"]
        }
        modelhash.delete("target_platform")
        modelhash["platform"] = hash
        x.modelhash = modelhash
        x.model = modelhash.to_json
        x.save
      end
    end 
    ProductFeature.all.each do |x|
      x.platform_setting_id = x.platform.parameter_setting_id if x.platform
      x.platform_setting_version = ParameterSetting.find(x.platform_setting_id).version if x.platform_setting_id
      x.save
      ProductFeatureVersion.where("product_feature_id = #{x.id}").each do |y|
        y.platform_setting_id = x.platform_setting_id if x.platform_setting_id
        y.platform_setting_version = x.platform_setting_version if x.platform_setting_version
        y.save
      end
    end
    remove_column :product_features, :target_platform_id
    remove_column :product_feature_versions, :target_platform_id
  end
end
