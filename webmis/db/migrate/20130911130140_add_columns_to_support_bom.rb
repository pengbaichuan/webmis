class AddColumnsToSupportBom < ActiveRecord::Migration
  def change
    add_column :products, :parent_id, :integer
    add_column :products, :created_from, :string
    add_column :products, :file_detect_regex, :string
    add_column :product_versions, :parent_id, :integer
    add_column :product_versions, :created_from, :string
    add_column :product_versions, :file_detect_regex, :string
    add_column :production_orderlines, :created_from, :string
    add_column :production_orderlines, :parent_linenr, :integer
    add_column :conversion_databases, :production_orderline_id, :integer
  end
end
