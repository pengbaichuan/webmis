class CreateCustomerContentSpecifications < ActiveRecord::Migration
  def change
    create_table :customer_content_specifications do |t|
      t.string :name
      t.integer :content_type
      t.text :description
      t.text :generation_code
      t.text :validation_code
      t.boolean :is_active, default: true
      t.boolean :mandatory, default: true
      t.boolean :manual, default: false

      t.timestamps
    end
  end
end
