class CreateCustomerContentDefinitions < ActiveRecord::Migration
  def change
    create_table :customer_content_definitions do |t|
      t.string :label
      t.integer :output_target
      t.integer :customer_content_specification_id
      t.integer :target_platform_id

      t.timestamps
    end
  end
end
