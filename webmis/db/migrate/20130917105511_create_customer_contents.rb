class CreateCustomerContents < ActiveRecord::Migration
  def change
    create_table :customer_contents do |t|
      t.integer :customer_content_specification_id
      t.integer :production_orderline_id
      t.text :value

      t.timestamps
    end
  end
end
