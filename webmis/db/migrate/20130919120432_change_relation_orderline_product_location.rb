class ChangeRelationOrderlineProductLocation < ActiveRecord::Migration
  def up
    add_column :production_orderlines, :product_location_id, :integer
    ProductLocation.where("production_orderline_id is not null").each do |pl|
        pol = ProductionOrderline.find(pl.production_orderline_id)
        pol.product_location_id = pl.id
        pol.save
    end
    remove_column :product_locations, :production_orderline_id
  end

  def down
    remove_column :production_orderlines, :product_location_id
  end
end
