class AddRefreshTimeToDashboardWidgets < ActiveRecord::Migration
  def change
    add_column :dashboard_widgets, :refresh_time, :integer
    add_column :dashboard_widgets, :description, :text
  end
end
