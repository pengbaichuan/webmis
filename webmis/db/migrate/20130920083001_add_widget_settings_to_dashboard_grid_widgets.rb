class AddWidgetSettingsToDashboardGridWidgets < ActiveRecord::Migration
  def change
    add_column :dashboard_grid_widgets, :widget_settings, :text
  end
end
