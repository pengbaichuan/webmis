class AddDemoWidgetsToDashboardWidgets < ActiveRecord::Migration
  def self.up
    a = DashboardWidget.new(:width_css_class => "span5", :name => "Demo 01", :refresh_time => '60000', :description => 'Shows the status of each product from the production orders wich are in status production.')
    b = DashboardWidget.new(:width_css_class => "span4", :name => "Demo 02", :refresh_time => '30000', :description => 'Shows a pie chart of the outcome of each conversion-request of the last 30 days.')
    c = DashboardWidget.new(:width_css_class => "span3", :name => "Demo 03", :refresh_time => '600000', :description => 'Dummy demo chart.')
    d = DashboardWidget.new(:width_css_class => "span12",:name => "Demo 04", :refresh_time => '60000', :description => 'Shows the assigned conversions or jobs for each active conversion server.')
    e = DashboardWidget.new(:width_css_class => "span6",:name => "My Production orders", :refresh_time => '10000', :description => 'Selects the Production-orders with the lines wich are submitted by yourself. Ordered by last updated order.')
    [a,b,c,d,e].each do |n|
      n.save
    end
  end
  def self.down
    DashboardWidget.delete_all('name="Demo 01" OR name="Demo 02" OR name="Demo 03" OR Name="Demo 04" OR name="My Production orders"')
  end
end
