class CreateCustomerTargets < ActiveRecord::Migration
  def change
    create_table :customer_targets do |t|
      t.string  :label
      t.integer :output_target
      t.integer :customer_content_id
      t.integer :customer_content_definition_id

      t.timestamps
    end
  end
end
