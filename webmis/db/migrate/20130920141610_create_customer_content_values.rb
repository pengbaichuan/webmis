class CreateCustomerContentValues < ActiveRecord::Migration
  def change
    create_table :customer_content_values do |t|
      t.integer :customer_content_specification_id
      t.string :customer_version
      t.date :received_date
      t.text :value

      t.timestamps
    end
  end
end
