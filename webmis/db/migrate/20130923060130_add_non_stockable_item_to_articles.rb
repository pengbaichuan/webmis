class AddNonStockableItemToArticles < ActiveRecord::Migration
  def self.up
    add_column :articles, :non_stockable, :boolean, :default => false
    Article.reset_column_information
    Article.where(:non_stockable => nil).each do |article|
      article.non_stockable = false
      article.save
    end
  end

  def self.down
    remove_column :articles, :non_stockable
  end
end
