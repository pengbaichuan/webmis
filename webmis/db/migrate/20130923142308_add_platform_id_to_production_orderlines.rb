class AddPlatformIdToProductionOrderlines < ActiveRecord::Migration
  def change
    add_column :products, :target_platform_id, :int
    add_column :product_versions, :target_platform_id, :int
  end
end
