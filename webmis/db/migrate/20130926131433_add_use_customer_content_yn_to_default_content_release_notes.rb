class AddUseCustomerContentYnToDefaultContentReleaseNotes < ActiveRecord::Migration
  def change
    add_column :default_content_release_notes, :use_customer_content_yn, :boolean
  end
end
