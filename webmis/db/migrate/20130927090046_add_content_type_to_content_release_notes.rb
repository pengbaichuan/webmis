class AddContentTypeToContentReleaseNotes < ActiveRecord::Migration
  def change
    add_column :content_release_notes, :content_type, :integer
  end
end
