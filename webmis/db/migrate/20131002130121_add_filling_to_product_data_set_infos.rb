class AddFillingToProductDataSetInfos < ActiveRecord::Migration
  def change
    add_column :product_data_set_infos, :filling, :string
  end
end
