class AddProductIdToCustomerContents < ActiveRecord::Migration
  def change
    add_column :customer_contents, :product_id, :integer
    add_column :customer_content_specifications, :for_product, :boolean, default: false
  end
end
