class AddRegionIdToRegions < ActiveRecord::Migration
  def self.up
    add_column :regions, :region_id, :integer
    n = AreaType.new(:name => 'RegionAlias')
    n.save
  end

  def self.down
    remove_column :regions
    r = AreaType.find_by_name('RegionAlias')
    r.destroy if r
  end
end
