class AddProcessDataDirectoryToConfigParameters < ActiveRecord::Migration
  def self.up
    n = ConfigParameter.new(:cfg_name => "process_data_directory", :cfg_value => '/data/proces_data/', :description => 'Directory name for process-data files.')
    n.save
  end
  def self.down
    ConfigParameter.delete_all(:cfg_name => "process_data_directory")
  end
end
