class AddSplitByToProcessDataSpecifications < ActiveRecord::Migration
  def change
    add_column :process_data_specifications, :split_by_key, :string
    add_column :process_data_elements, :split_by_value, :string
  end
end
