class AddTaggingIndex < ActiveRecord::Migration
  def up
   add_index :taggings, [:tag_id, :taggable_type, :taggable_id, :context, :tagger_id, :tagger_type], unique: true, name: 'tagging_idx'
  end

  def down
   remove_index :tags, :name
   remove_index :taggings, name: 'tagging_idx'
  end
end
