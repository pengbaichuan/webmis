class AddCrashedYnToConversionDatabases < ActiveRecord::Migration
  def change
    add_column :conversion_databases, :crashed_yn, :boolean
  end
end
