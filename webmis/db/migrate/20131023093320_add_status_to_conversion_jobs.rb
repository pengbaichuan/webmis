class AddStatusToConversionJobs < ActiveRecord::Migration
  def self.up
    add_column :conversion_jobs, :status, :integer
    ConversionJob.all.each do |cj|
      cj.status = ConversionJob.statusarray.index(cj.run_status)
      cj.save
    end
    remove_column :conversion_jobs, :run_status
  end

  def self.down
    add_column :conversion_jobs, :run_status, :string
    ConversionJob.each do |cj|
      cj.run_status = cj.status_str
      cj.save
    end
    remove_column :conversion_jobs, :status
  end


end
