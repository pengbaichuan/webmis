class AddCreditInvoiceIdToInvoices < ActiveRecord::Migration
  def change
    add_column :invoices, :credit_invoice_parent_id, :integer
  end
end
