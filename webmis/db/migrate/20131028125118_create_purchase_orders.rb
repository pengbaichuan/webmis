class CreatePurchaseOrders < ActiveRecord::Migration
  def change
    create_table :purchase_orders do |t|
      t.string :purchase_reference
      t.date :date_ordered
      t.date :date_paid
      t.date :date_received
      t.integer :status
      t.text :remarks
      t.boolean :deleted
      t.string :external_contact_id
      t.integer :user_id

      t.timestamps
    end
  end
end
