class CreatePurchaseOrderlines < ActiveRecord::Migration
  def change
    create_table :purchase_orderlines do |t|
      t.integer :purchase_order_id
      t.integer :quantity
      t.decimal :unit_price, :precision => 8, :scale => 2
      t.text :item_description
      t.string :item_reference

      t.timestamps
    end
  end
end
