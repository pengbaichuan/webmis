class CreateIncludedArticleLicensePurchaseOrderlines < ActiveRecord::Migration
  def change
    create_table :included_article_license_purchase_orderlines do |t|
      t.integer :shipping_orderline_id
      t.integer :article_license_id
      t.integer :purchase_orderline_id

      t.timestamps
    end
  end
end
