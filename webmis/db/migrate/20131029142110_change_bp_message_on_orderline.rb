class ChangeBpMessageOnOrderline < ActiveRecord::Migration
   def self.up
      execute "alter table production_orderlines modify column bp_message mediumtext"
      execute "alter table conversion_jobs modify column stdout_log longtext"
   end

   def self.down
      execute "alter table production_orderlines modify column bp_message varchar(255)"
      execute "alter table conversion_jobs modify column stdout_log text"
   end
end
