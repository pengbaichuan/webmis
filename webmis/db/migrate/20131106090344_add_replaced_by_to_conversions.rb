class AddReplacedByToConversions < ActiveRecord::Migration
  def change
    add_column :conversions, :replaced_by_id, :integer
  end
end
