class AddFillingToRegionMappingLine < ActiveRecord::Migration
  def change
    add_column :region_mapping_lines, :filling, :string
  end
end
