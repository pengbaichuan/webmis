class AddUpdateRegionToAreaType < ActiveRecord::Migration
  def self.up
    n = AreaType.new(:name => 'UpdateRegion')
    n.save
  end

  def self.down
    r = AreaType.find_by_name('UpdateRegion')
    r.destroy if r
  end
end
