class CreateSchedulerLocks < ActiveRecord::Migration
  def change
    create_table :scheduler_locks do |t|
      t.string :process
      t.integer :pid
      t.string :server
      t.integer :lock_version
      t.timestamps
    end
    add_index :scheduler_locks, :process, :unique => true
  end
end
