class CreateCustomerContentSpecificationVersions < ActiveRecord::Migration
  def change
    create_table :customer_content_specification_versions do |t|
      t.string :name
      t.integer :content_type
      t.text :description
      t.text :generation_code
      t.text :validation_code
      t.boolean :is_active
      t.boolean :mandatory
      t.boolean :manual
      t.boolean :for_product
      t.integer :customer_content_specification_id
      t.string :status
      t.integer :version, :default => 0
      t.integer :mantis
      t.text :comment
      t.string :updated_by

      t.timestamps
    end

    add_column :customer_content_specifications, :version, :integer
    add_column :customer_content_specifications, :status, :string
    add_column :customer_content_specifications, :mantis, :integer
    add_column :customer_content_specifications, :comment, :text
    add_column :customer_content_specifications, :updated_by, :string
  end
end

