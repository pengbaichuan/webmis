class ChangeColumnSchedulingAllowedYnToServer < ActiveRecord::Migration
  def up
    change_column :servers,  :scheduling_allowed_yn, :boolean
  end

  def down
    change_column :servers,  :scheduling_allowed_yn, :integer
  end
end
