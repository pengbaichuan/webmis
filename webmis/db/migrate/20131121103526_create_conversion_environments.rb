class CreateConversionEnvironments < ActiveRecord::Migration
  def change
    create_table :conversion_environments do |t|
      t.string :name
      t.boolean :active_yn, :default => true
      t.text :remarks

      t.timestamps
    end
  end
end
