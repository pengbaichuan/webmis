class AddConversionEnvironmentIdToConversionJob < ActiveRecord::Migration
  def change
    add_column :conversion_jobs, :conversion_environment_id, :integer
  end
end
