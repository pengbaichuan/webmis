class AddConversionEnvironmentIdToConversion < ActiveRecord::Migration
  def change
    add_column :conversions, :conversion_environment_id, :integer
  end
end
