class AddFlagsProductionOrder < ActiveRecord::Migration
  def up
    add_column :production_orders,  :create_frontend_order_yn, :boolean 
    add_column :production_orders,  :allocate_by_update_region_yn, :boolean 
  end

  def down
    remove_column :production_orders,  :create_frontend_order_yn 
    remove_column :production_orders,  :allocate_by_update_region_yn 
  end
end
