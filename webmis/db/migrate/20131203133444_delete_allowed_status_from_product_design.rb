class DeleteAllowedStatusFromProductDesign < ActiveRecord::Migration
  def change
    remove_column :product_designs, :allowed_status
    remove_column :product_design_versions, :allowed_status
  end
end