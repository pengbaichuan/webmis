class CreateAllocationExceptions < ActiveRecord::Migration
  def change
    create_table :allocation_exceptions do |t|
      t.integer :conversion_id
      t.string :exception_type
      t.string :scope
      t.boolean :removed_yn

      t.timestamps
    end
  end
end
