class AddIndexProductionOrderIdToDocuments < ActiveRecord::Migration
  def self.up
    add_index(:documents, :production_order_id)
  end

  def self.down
    remove_index(:documents, :production_order_id)
  end
end