class AddLockVersionToConversionJob < ActiveRecord::Migration
  def change
    add_column :conversion_jobs, :lock_version, :integer, :default => 1
  end
end
