class CreateProcessDataStatuslogs < ActiveRecord::Migration
  def change
    create_table :process_data_statuslogs do |t|
      t.integer :process_data_id
      t.string :level
      t.string :user
      t.integer :version
      t.string :remarks
      t.integer :status
      
      t.timestamps 
    end
 
    add_column :process_data, :updated_by, :string
    add_column :process_data_items, :updated_by, :string

  end
end
