class AddTagTextFieldsToProcessDataItems < ActiveRecord::Migration
  def change
    add_column :process_data_items, :supplier_name, :text
    add_column :process_data_items, :datatype, :text
    add_column :process_data_items, :area_name, :text
    add_column :process_data_items, :data_release, :text
    add_column :process_data_items, :filling, :text
  end
end
