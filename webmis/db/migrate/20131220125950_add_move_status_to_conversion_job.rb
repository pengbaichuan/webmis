class AddMoveStatusToConversionJob < ActiveRecord::Migration
  def change
    add_column :conversion_jobs, :move_status, :string
  end
end
