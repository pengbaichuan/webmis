class ChangeConversionJobToJob < ActiveRecord::Migration
  def up
    #drop_table :jobs
    rename_table :conversion_jobs, :jobs
    add_column :jobs, :reference_id, :integer
    add_column :jobs, :type, :string
    Job.all.each do |job|
      job.type = "ConversionJob"
      job.save
    end
  end

  def down
    remove_column :jobs, :reference_id
    remove_column :jobs, :type
    rename_table  :jobs, :conversion_jobs
  end
end
