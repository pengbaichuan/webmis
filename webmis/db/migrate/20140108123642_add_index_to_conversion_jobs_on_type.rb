class AddIndexToConversionJobsOnType < ActiveRecord::Migration
  def change
    add_index :jobs, :type
  end
end
