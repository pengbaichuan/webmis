class AddStatusToOutboundOrders < ActiveRecord::Migration
  def change
    add_column :outbound_orders, :status, :integer, :default => 0
  end
end
