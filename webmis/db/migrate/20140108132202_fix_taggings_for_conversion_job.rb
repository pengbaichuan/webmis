class FixTaggingsForConversionJob < ActiveRecord::Migration
  def up
    execute "Update taggings set taggable_type = 'Job' where taggable_type = 'ConversionJob'"
  end

  def down
    execute "Update taggings set taggable_type = 'ConversionJob' where taggable_type = 'Job'"
  end
end
