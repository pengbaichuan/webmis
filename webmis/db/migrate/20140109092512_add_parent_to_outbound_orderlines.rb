class AddParentToOutboundOrderlines < ActiveRecord::Migration
  def change
    add_column :outbound_orderlines, :parent_linenr, :integer  
  end
end
