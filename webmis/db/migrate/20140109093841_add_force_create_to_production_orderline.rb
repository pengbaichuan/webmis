class AddForceCreateToProductionOrderline < ActiveRecord::Migration
  def change
    add_column :production_orderlines, :force_create, :boolean, :default => false
  end
end
