class AddCoverageToProductDataSetInfo < ActiveRecord::Migration
  def change
    add_column :product_data_set_infos, :coverage_id, :integer
  end
end
