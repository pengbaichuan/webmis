class CreatePackingSpecificationVersions < ActiveRecord::Migration
  def change
    create_table :packing_specification_versions do |t|
      t.integer :packing_specification_id
      t.string :name
      t.text :description
      t.text :run_script
      t.boolean :manual_yn
      t.string :updated_by
      t.integer :version
      t.integer :mantis
      t.text :comment
      t.string :status
      t.boolean :schedule_manual,  :default => true,  :null => false
      t.integer :schedule_cores
      t.integer :schedule_memory_base
      t.integer :schedule_memory_times_input
      t.integer :schedule_diskspace_base
      t.integer :schedule_diskspace_times_input
      t.boolean :schedule_allow_virtual
      t.boolean :schedule_exclusive
      
      t.timestamps 
    end
  end
end
