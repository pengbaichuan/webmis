class CreatePackings < ActiveRecord::Migration
  def change
    create_table :packings do |t|
      t.integer :outbound_order_id
      t.integer :packing_specification_id
      t.string :filename
      t.string :path
      t.integer :status
      
      t.timestamps 
    end
  end
end
