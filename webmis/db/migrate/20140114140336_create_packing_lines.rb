class CreatePackingLines < ActiveRecord::Migration
  def change
    create_table :packing_lines do |t|
      t.integer :packing_id
      t.integer :outbound_orderline_id

      t.timestamps
    end
  end
end
