class AddDataTypeToProcessDataSpecifications < ActiveRecord::Migration
  def change
    add_column :process_data_specifications, :data_type_id, :integer
    add_column :process_data_specifications, :filling, :string
  end
end
