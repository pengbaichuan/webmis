class AddDirectoryToProcessDataElements < ActiveRecord::Migration
  def change
    add_column :process_data_elements, :directory, :string
  end
end
