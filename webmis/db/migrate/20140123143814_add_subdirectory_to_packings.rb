class AddSubdirectoryToPackings < ActiveRecord::Migration
  def change
    add_column :packings, :subdirectory, :string
    add_column :packings, :md5_checksum, :string
  end
end
