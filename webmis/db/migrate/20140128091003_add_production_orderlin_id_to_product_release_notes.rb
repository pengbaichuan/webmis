class AddProductionOrderlinIdToProductReleaseNotes < ActiveRecord::Migration
  def change
    add_column :product_release_notes, :production_orderline_id, :integer
    add_column :documents, :production_orderline_id, :integer
    add_column :document_versions, :production_orderline_id, :integer
  end
end
