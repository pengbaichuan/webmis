class AddBugTrackerIdToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :bug_tracker_id, :string
  end
end
