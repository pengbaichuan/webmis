class ChangeParametersOnExecutable < ActiveRecord::Migration
  def up
    execute "alter table executables modify column parameters longtext"
  end

  def down
    execute "alter table conversion_jobs modify column parameters longtext"
  end
end
