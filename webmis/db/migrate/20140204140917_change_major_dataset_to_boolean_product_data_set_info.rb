class ChangeMajorDatasetToBooleanProductDataSetInfo < ActiveRecord::Migration
  def up
    change_column :product_data_set_infos,  :major_dataset, :boolean
  end

  def down
    change_column :product_data_set_infos,  :major_dataset, :integer
  end
end
