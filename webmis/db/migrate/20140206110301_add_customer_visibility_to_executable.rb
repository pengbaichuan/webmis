class AddCustomerVisibilityToExecutable < ActiveRecord::Migration
  def change
    add_column :executables, :customer_visibility, :boolean, :default => true
    add_column :executables, :is_improvement_process, :boolean, :default => false
  end
end
