class ChangeModelInDesignToLongtext < ActiveRecord::Migration
  def up
     execute "alter table product_designs modify column model longtext"
  end

  def down
     execute "alter table product_designs modify column model text"
  end
end
