class AddDistributionListIdToOutboundOrders < ActiveRecord::Migration
  def change
    add_column :outbound_orders, :distribution_list_id, :integer
  end
end
