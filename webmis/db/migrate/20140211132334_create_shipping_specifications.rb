class CreateShippingSpecifications < ActiveRecord::Migration
  def change
    create_table :shipping_specifications do |t|
      t.string   :name
      t.integer  :external_contact_id
      t.integer  :target_platform_id
      t.integer  :packing_specification_id
      t.integer  :distribution_list_id
      t.string   :shipment_type
      t.string   :courier
      t.integer  :file_transfer_account_id
      t.integer  :default_filename_specification
      t.integer  :default_subdirectory_specification

      t.timestamps
    end

    add_column :outbound_orders, :shipping_specification_id, :integer
    add_column :target_platforms, :default_shipping_specification_id, :integer
  end
end
