class AddEnvVariablesToConfigParameters < ActiveRecord::Migration
  def self.up
    cjc = ConfigParameter.new(:cfg_name => "conversion_job_crash_dir", :cfg_value => '/data/projects/crash/backoffice/prod/', :description => 'Directory name for crashed conversions')
    cjc.save
    cji = ConfigParameter.new(:cfg_name => "conversion_job_intermediate_dir", :cfg_value => '/data/ops2/backoffice/prod/', :description => 'Directory name for dHIve')
    cji.save
    cjr = ConfigParameter.new(:cfg_name => "conversion_job_result_dir", :cfg_value => '/data/output/backoffice/prod/', :description => 'Directory name for conversion output')
    cjr.save
    pjr = ConfigParameter.new(:cfg_name => "packing_job_result_dir", :cfg_value => '/data/ops/pack/backoffice/prod/', :description => 'Directory name for shipping packs')
    pjr.save
    pjw = ConfigParameter.new(:cfg_name => "packing_job_working_dir", :cfg_value => '/volumes2/dakota/', :description => 'Directory name for tmp shipping packs')
    pjw.save
    dak = ConfigParameter.new(:cfg_name => "dakota_scripts_dir", :cfg_value => '/data/ops/backoffice/prod/scripts/', :description => 'Directory for conversion-job dakota scripts')
    dak.save
    BusinessProcess.find_or_create_by_name("AUTO CONVERSION")
  end
  
  def self.down
    ConfigParameter.delete_all(:cfg_name => "conversion_job_crash_dir")
    ConfigParameter.delete_all(:cfg_name => "conversion_job_intermediate_dir")
    ConfigParameter.delete_all(:cfg_name => "conversion_job_result_dir")
    ConfigParameter.delete_all(:cfg_name => "packing_job_result_dir")
    ConfigParameter.delete_all(:cfg_name => "packing_job_working_dir")
    ConfigParameter.delete_all(:cfg_name => "dakota_scripts_dir")
    BusinessProcess.delete_all(:name => "AUTO CONVERSION")
  end
end
