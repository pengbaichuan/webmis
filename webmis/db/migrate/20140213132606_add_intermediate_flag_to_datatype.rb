class AddIntermediateFlagToDatatype < ActiveRecord::Migration
  def change
    add_column :data_types, :is_intermediate, :boolean, :default => false
    dt = DataType.active.find_by_name("dhive")
    if dt
      dt.is_intermediate = true
      dt.save!
    end
    dt = DataType.active.find_by_name("rdb")
    if dt
      dt.is_intermediate = true
      dt.save!
    end

  end
end
