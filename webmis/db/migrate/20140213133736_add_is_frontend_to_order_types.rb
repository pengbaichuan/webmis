class AddIsFrontendToOrderTypes < ActiveRecord::Migration
  def change
    add_column :ordertypes, :is_frontend, :boolean, :default => false
  end
end
