class AddMasteringAndReleaseToBusinessProcesses < ActiveRecord::Migration
  def up
    execute "UPDATE business_processes set line_link_controller = 'production_orderlines', line_link_action = 'mastering' WHERE name = 'MASTERING'"
    execute "INSERT INTO business_processes (name, description, line_link_action) VALUES ('REGISTER PRODUCT', 'Register the product in product location', 'register_product')"
  end

  def down
    execute "UPDATE business_processes set line_link_controller = NULL, line_link_action = NULL WHERE name = 'MASTERING'"
    execute "DELETE FROM business_processes WHERE name = 'REGISTER PRODUCT'"
  end
end
