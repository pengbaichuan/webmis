class AddProductLocationDirToConfigParameters < ActiveRecord::Migration
  def self.up
    pld = ConfigParameter.new(:cfg_name => "product_location_dir", :cfg_value => '/data/product/', :description => 'Directory name for registered products (product_locations)')
    pld.save
  end

  def self.down
    ConfigParameter.delete_all(:cfg_name => "product_location_dir")
  end
end
