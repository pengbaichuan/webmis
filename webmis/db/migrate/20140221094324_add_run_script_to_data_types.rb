class AddRunScriptToDataTypes < ActiveRecord::Migration
  def change
    add_column :data_types, :automatic_validation_yn, :boolean, :default => false
    add_column :data_types, :run_script, :text
    add_column :data_types, :updated_by, :string, :default => false
    add_column :data_types, :version, :integer, :default => 0
    add_column :data_types, :mantis, :integer
    add_column :data_types, :comment, :text
    add_column :data_types, :status, :string, :default => "approved"
    add_column :data_types, :schedule_manual, :boolean, :default => true, :null => false
    add_column :data_types, :schedule_cores, :integer, :default => 1
    add_column :data_types, :schedule_memory_base, :integer, :default => 0
    add_column :data_types, :schedule_memory_times_input, :integer, :default => 0
    add_column :data_types, :schedule_diskspace_base, :integer, :default => 0
    add_column :data_types, :schedule_diskspace_times_input, :integer, :default => 1
    add_column :data_types, :schedule_allow_virtual, :boolean, :default => true
    add_column :data_types, :schedule_exclusive, :boolean, :default => false

    add_column :conversion_databases, :status, :integer, :default => 1000

    create_table :data_type_versions do |t|
      t.integer :data_type_id
      t.string :name
      t.text :description
      t.boolean :automatic_validation_yn
      t.text :run_script
      t.string :updated_by
      t.integer :version
      t.integer :mantis
      t.text :comment
      t.string :status
      t.boolean :schedule_manual,  :default => true,  :null => false
      t.integer :schedule_cores
      t.integer :schedule_memory_base
      t.integer :schedule_memory_times_input
      t.integer :schedule_diskspace_base
      t.integer :schedule_diskspace_times_input
      t.boolean :schedule_allow_virtual
      t.boolean :schedule_exclusive
      
      t.timestamps 
    end
  end
end
