class AddNoOfSubregionsToJobAndDatabase < ActiveRecord::Migration
  def change
    add_column :jobs, :group_amount, :integer, :default => 1
    add_column :jobs, :group_by, :string
    add_column :jobs, :group_value, :string
    add_column :conversion_databases, :group_amount, :integer, :default => 1
    add_column :conversion_databases, :group_by, :string
    add_column :conversion_databases, :group_value, :string
    execute "alter table jobs modify column run_script longtext"
  end
end
