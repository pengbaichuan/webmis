class AddActionLogToConversionJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :action_log, :text
  end
end
