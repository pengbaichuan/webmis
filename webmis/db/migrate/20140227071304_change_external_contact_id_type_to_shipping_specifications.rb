class ChangeExternalContactIdTypeToShippingSpecifications < ActiveRecord::Migration
  def up
    change_column(:shipping_specifications, :external_contact_id, :string)
  end

  def down
    change_column(:shipping_specifications, :external_contact_id, :integer)
  end
end
