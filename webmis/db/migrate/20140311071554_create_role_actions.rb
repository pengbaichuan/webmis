class CreateRoleActions < ActiveRecord::Migration
  def change
    create_table :role_actions do |t|
      t.integer :role_id
      t.string :controller_name
      t.string :action_name

      t.timestamps
    end
  end
end
