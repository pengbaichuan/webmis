class CreateJobFailReasons < ActiveRecord::Migration
  def change
    create_table :job_fail_reasons do |t|
      t.string :reason
      t.text :description
      t.string :job_type

      t.timestamps
    end
  end
end
