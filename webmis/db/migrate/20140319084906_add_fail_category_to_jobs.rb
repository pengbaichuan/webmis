class AddFailCategoryToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :fail_category, :string
  end
end
