class AddFailCategoryLogToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :fail_category_logged_by_user_id, :integer
    add_column :jobs, :fail_category_logged_on, :date
  end
end
