class MigrateOldAbility < ActiveRecord::Migration
  def up
    RoleAction.migrate_old_ablity
  end

  def down
    puts 'Migration is not reversable!'
  end
end
