class CreateCompanyAbbrVersions < ActiveRecord::Migration
  def change
    create_table :company_abbr_versions do |t|
      t.integer :company_abbr_id
      t.string :company_name
      t.string :ms_abbr_3
      t.string :copyright_name
      t.boolean :is_active
      t.integer :version
      t.string :updated_by

      t.timestamps 
    end
    add_column :company_abbrs, :version, :integer, :default => 0
    add_column :company_abbrs, :updated_by, :string
  end

end
