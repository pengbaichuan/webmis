class AddCurrentConversionJobsToDashboardWidgets < ActiveRecord::Migration
  def self.up
    f = DashboardWidget.new(:width_css_class => "span6",:name => "Production orderlines requiring attention", :refresh_time => '10000', :description => 'Show the production orderlines requiring manual attention.')
    f.save
  end
  def self.down
    DashboardWidget.delete_all('name="Production orderlines requiring attention"')
  end
end
