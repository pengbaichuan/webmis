class AddPickedReceptionIdsToProcessDataItems < ActiveRecord::Migration
  def change
    add_column :process_data_items, :picked_reception_ids, :string
  end
end
