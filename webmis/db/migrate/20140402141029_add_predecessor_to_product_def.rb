class AddPredecessorToProductDef < ActiveRecord::Migration
  def change
    add_column :products, :predecessor_id, :integer
    add_column :product_versions, :predecessor_id, :integer
  end
end
