class AddRevisionToProductDesigns < ActiveRecord::Migration
  # no revision is used anymore, changed to lock_version mechanism
  def change
    add_column :product_designs, :lock_version, :integer, :default => 0
  end
end
