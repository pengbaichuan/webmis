class AddJobSupportYnToOrdertypes < ActiveRecord::Migration
  def change
    add_column :ordertypes, :job_support_yn, :boolean, :default => false
  end
end
