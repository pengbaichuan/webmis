class CreateConversionJobProcessDataElements < ActiveRecord::Migration
  def change
    create_table :conversion_job_process_data_elements do |t|
      t.integer :conversion_job_id
      t.integer :process_data_element_id

      t.timestamps
    end
  end
end
