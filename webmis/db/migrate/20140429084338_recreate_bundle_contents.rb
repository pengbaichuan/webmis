class RecreateBundleContents < ActiveRecord::Migration
  def up
    drop_table :bundle_contents

    create_table :bundle_contents do |t|
      t.integer :conversiontool_id
      t.integer :cvtool_bundle_id
      t.integer :executable_id
      t.string :tool_name
      t.string :tool_release
      t.boolean :is_improvement_process, :default => false
      t.string :name
      t.text :description
      t.text :input
      t.text :output
      t.text :parameters
      t.text :toolspec_contents

      t.timestamps
    end

    remove_column :executables, :tool_name
    remove_column :executables, :release
    remove_column :executable_versions, :tool_name
    remove_column :executable_versions, :release
  end

  def down
    drop_table :bundle_contents

    create_table :bundle_contents do |t|
      t.string :bundles_name
      t.string :bundle_name
      t.string :release
      t.string :executable_name
      t.string :tool_release

      t.timestamps
    end

    add_column :executables, :tool_name, :string
    add_column :executables, :release, :string 
    add_column :executable_versions, :tool_name, :string
    add_column :executable_versions, :release, :string 
  end
end
