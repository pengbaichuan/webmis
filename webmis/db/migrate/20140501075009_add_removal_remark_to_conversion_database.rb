class AddRemovalRemarkToConversionDatabase < ActiveRecord::Migration
  def change
    add_column :conversion_databases, :removal_remark, :text
  end
end
