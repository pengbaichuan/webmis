class AddRemovalAgeToParameters < ActiveRecord::Migration
  def up
    c = ConfigParameter.new
    c.cfg_name = "age_removal_conversion_databases_in_months"
    c.cfg_type = "integer"
    c.cfg_value = 6
    c.description = "Required age (in months) of intermediate databases that can be removed when related product was shipped"
    c.save
  end

  def down
    c = ConfigParameter.where("cfg_name = 'age_removal_conversion_databases_in_months'").first
    c.destroy if c
  end
end
