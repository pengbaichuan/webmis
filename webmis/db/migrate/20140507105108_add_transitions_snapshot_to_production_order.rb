class AddTransitionsSnapshotToProductionOrder < ActiveRecord::Migration
  def change
    add_column :production_orders, :process_transitions_snapshot, :text, :limit => 4294967295
  end
end
