class AddCvToolReleasePathToConfigParameters < ActiveRecord::Migration
  def self.up
    n = ConfigParameter.new(:cfg_name => "cvtool_release_directory", :cfg_value => '/data/id/release/', :description => 'Directory path for released tools / bundles')
    n.save
  end
  def self.down
    ConfigParameter.delete_all(:cfg_name => "cvtool_release_directory")
  end
end
