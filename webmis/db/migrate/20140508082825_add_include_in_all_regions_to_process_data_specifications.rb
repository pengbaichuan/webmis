class AddIncludeInAllRegionsToProcessDataSpecifications < ActiveRecord::Migration
  def change
    add_column :process_data_specifications, :include_in_all_regions, :boolean
  end
end
