class AddSftpSubdirectoryToOutboundOrders < ActiveRecord::Migration
  def change
    add_column :outbound_orders, :sftp_subdirectory, :string
  end
end
