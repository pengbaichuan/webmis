class AddCoverageAttibutesToProductDesign < ActiveRecord::Migration
  def change
    add_column :product_designs, :region_id, :integer
    add_column :product_designs, :company_abbr_id, :integer
    add_column :product_designs, :data_release_id, :integer
  end
end
