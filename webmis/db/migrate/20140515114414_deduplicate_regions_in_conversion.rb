class DeduplicateRegionsInConversion < ActiveRecord::Migration
  def up
    @inactive_areas = Conversion.order(:order => :ruby_type).select("distinct region_id").includes("region").map{|x| [x.region.name,x.region_id] if x.region && x.region.name != nil && !x.region.isactive }.compact
    @inactive_areas.each do |inactive_area|
      @matches = Region.find_all_by_name(inactive_area[0])
      @matches.each do |region|
        if region.id != inactive_area[1]
          puts "DOUBLE ENTRY FOR #{region.name} - #{region.id} - #{region.ruby_type}"
          if region.ruby_type != 'RegionAlias'
            @replacer_conversions = Conversion.where(:region_id => inactive_area[1])
            @replacer_conversions.each do |conversion|
              puts "FOUND CONVERSION FOR REGION-ID-UPDATE: #{conversion.id} #{conversion.region_id} => #{region.id}"
              new_region_id = region.id
              if region.ruby_type == 'Area'
                # Check if there a original region and prefer that
                cnt = Continent.find_by_name(region.name)
                new_region_id = cnt.id if cnt
                cou = Country.find_by_name(region.name)
                new_region_id = cou.id if cou
              end
              conversion.update_column(:region_id,new_region_id)
              conversion.save(:validate => false)
            end
          end
        end
      end
    end
  end

  def down
    puts 'Migration is not reversable!'
  end
end