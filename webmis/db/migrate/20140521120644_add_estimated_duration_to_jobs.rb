class AddEstimatedDurationToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :estimated_duration, :integer
  end
end
