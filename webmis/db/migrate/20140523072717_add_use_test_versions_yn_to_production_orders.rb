class AddUseTestVersionsYnToProductionOrders < ActiveRecord::Migration
  def change
    add_column :production_orders, :use_test_versions_yn, :boolean
  end
end
