class CreateValidationErrors < ActiveRecord::Migration
  def up
    create_table :validation_errors do |t|
      t.integer :conversion_database_id
      t.string  :name
      t.integer :count
      t.boolean :copied_to_crash_yn
      t.boolean :resolved_yn
      t.integer :ticket_nr

      t.timestamps
    end

    # fill validation_errors with the open ones.
    found = 0
    error_count = 0
    notfound = 0
    ConversionDatabase.where("status = #{ConversionDatabase::STATUS_HAS_ERRORS}").each do |cd|
      found +=1
      logfile = File.join(cd.path, "validateDH.log")
      if File.file?(logfile)
        lines = IO.readlines(logfile)
        error_count += cd.validation_jobs.last.fill_validation_errors(lines)
      else
        notfound += 1
      end
    end
    puts "20140603132118_create_validation_errors: Created #{error_count} validation_errors for #{found} found conversion databases with validation errors."
    puts "The validation logs of #{notfound} conversion databases were missing."
  end

  def down
    drop_table :validation_errors
  end
end
