class ChangeBundleContentParametersToLongtext < ActiveRecord::Migration
  def up
    execute "alter table bundle_contents modify parameters longtext"
  end

  def down
    execute "alter table bundle_contents modify parameters text"
  end
end
