class RenameRegionMappingToProductContent < ActiveRecord::Migration
  def up
   rename_table :region_mapping_headers, :product_content_specifications
   rename_table :region_mapping_lines, :product_content_items
   rename_table :region_mapping_header_versions, :product_content_specification_versions
   rename_table :region_mapping_line_versions, :product_content_item_versions
   add_column :product_content_items, :type, :string
   add_column :product_content_item_versions, :type, :string
   add_column :product_content_items, :resource_identification, :string
   add_column :product_content_item_versions, :resource_identification, :string
   add_column :product_content_items, :incr_update_yn, :boolean
   add_column :product_content_item_versions, :incr_update_yn, :boolean
   add_column :product_content_items, :regional_update_yn, :boolean
   add_column :product_content_item_versions, :regional_update_yn, :boolean
   rename_column :product_content_items, :region_mapping_header_id, :product_content_specification_id
   rename_column :product_content_item_versions, :region_mapping_header_id, :product_content_specification_id
   rename_column :product_content_item_versions, :region_mapping_line_id, :product_content_item_id
   rename_column :product_content_specification_versions, :region_mapping_header_id, :product_content_specification_id
  end

  def down
   rename_column :product_content_specification_versions, :product_content_specification_id, :region_mapping_header_id
   rename_column :product_content_item_versions, :product_content_item_id, :region_mapping_line_id
   rename_column :product_content_items, :product_content_specification_id, :region_mapping_header_id
   rename_column :product_content_item_versions, :product_content_specification_id, :region_mapping_header_id
   remove_column :product_content_items, :resource_identification
   remove_column :product_content_item_versions, :resource_identification
   remove_column :product_content_items, :incr_update_yn
   remove_column :product_content_item_versions, :incr_update_yn
   remove_column :product_content_items, :regional_update_yn
   remove_column :product_content_item_versions, :regional_update_yn
   rename_table :product_content_specifications, :region_mapping_headers
   rename_table :product_content_items, :region_mapping_lines
   rename_table :product_content_specification_versions, :region_mapping_header_versions
   rename_table :product_content_item_versions, :region_mapping_line_versions
   remove_column :region_mapping_lines,  :type
   remove_column :region_mapping_line_versions,  :type
  end
end
