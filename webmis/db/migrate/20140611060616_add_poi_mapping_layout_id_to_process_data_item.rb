class AddPoiMappingLayoutIdToProcessDataItem < ActiveRecord::Migration
  def change
    add_column :process_data_items, :poi_mapping_layout_id, :integer
  end
end
