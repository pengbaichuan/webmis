class CreateFillings < ActiveRecord::Migration
  def change
    create_table :fillings do |t|
      t.string :name
      t.boolean :active_yn
      t.boolean :removed_yn

      t.timestamps
    end

    DesignTag.where(:tag_type => 'Filling').order(:name).each do |dt|
      f = Filling.new(:name => dt.name,:active_yn => true, :removed_yn => false)
      f.save!
    end
  end
end
