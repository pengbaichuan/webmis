class CreateDataTypeFillings < ActiveRecord::Migration
  def change
    create_table :data_type_fillings do |t|
      t.integer :data_type_id
      t.integer :filling_id

      t.timestamps
    end
  end
end
