class ChangeTotalPrecisionInvoices < ActiveRecord::Migration
  def up
    change_column(:invoices, :total, :decimal, precision: 13, scale: 3)
    change_column(:invoices, :vat, :decimal, precision: 13, scale: 3)
  end

  def down
    change_column(:invoices, :total, :decimal, precision: 8, scale: 3)
    change_column(:invoices, :vat, :decimal, precision: 8, scale: 3)
  end
end
