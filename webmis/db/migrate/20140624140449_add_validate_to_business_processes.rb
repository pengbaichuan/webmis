class AddValidateToBusinessProcesses < ActiveRecord::Migration
  def up
    BusinessProcess.find_or_create_by_name("HANDLE VALIDATION")

    ConversionDatabase.where(:status => ConversionDatabase::STATUS_HAS_WARNINGS).each do |cd| 
      cd.status = ConversionDatabase::STATUS_NOT_VALIDATED
      cd.save!
    end

    ConversionDatabase.where(:status => ConversionDatabase::STATUS_HAS_ERRORS).each do |cd| 
      cd.status = ConversionDatabase::STATUS_NOT_VALIDATED
      cd.save!
    end
  end

  def down
    BusinessProcess.delete_all(:name => "HANDLE VALIDATION")

    ProductionOrderline.where(:bp_name => "HANDLE VALIDATION").each do |pol|
      pol.bp_name = "COMPLETE"
      pol.save!
    end
  end
end
