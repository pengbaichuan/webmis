class CreateProductionOrderlineConversionDatabases < ActiveRecord::Migration
  def change
    create_table :production_orderline_conversion_databases do |t|
      t.integer :production_orderline_id
      t.integer :conversion_database_id

      t.timestamps
    end
  end
end
