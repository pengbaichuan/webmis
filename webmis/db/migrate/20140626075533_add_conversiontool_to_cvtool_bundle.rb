class AddConversiontoolToCvtoolBundle < ActiveRecord::Migration
  def up
    create_table :cvtool_bundle_releases do |t|
      t.integer :conversiontool_id
      t.integer :cvtool_bundle_id
      t.boolean :active_yn, :default => true

      t.timestamps
    end

    Conversion.all.each do |conv|
      if !conv.conversiontool_id.blank? && !conv.cvtool_bundle_id.blank? && CvtoolBundleRelease.where("conversiontool_id = #{conv.conversiontool_id} AND cvtool_bundle_id = #{conv.cvtool_bundle_id}").empty?
        cbr = CvtoolBundleRelease.new(:conversiontool_id => conv.conversiontool_id, :cvtool_bundle_id => conv.cvtool_bundle_id, :active_yn => true)
        cbr.save!
      end
    end
  end

  def down
    drop_table :cvtool_bundle_releases
  end
end
