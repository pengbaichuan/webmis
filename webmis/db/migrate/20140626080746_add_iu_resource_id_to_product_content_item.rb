class AddIuResourceIdToProductContentItem < ActiveRecord::Migration
  def change
    add_column  :product_content_items, :resource_identification_iu, :string
  end
end
