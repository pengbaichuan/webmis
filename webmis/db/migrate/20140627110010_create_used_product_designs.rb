class CreateUsedProductDesigns < ActiveRecord::Migration
  def change
    create_table :used_product_designs do |t|
      t.integer :product_design_id
      t.integer :used_product_design_id
      t.string :used_product_design_md5

      t.timestamps
    end
  end

  add_column :product_designs, :design_type, :string
  add_column :product_designs, :product_design_md5, :string
  add_column :product_design_versions, :design_type, :string
  add_column :product_design_versions, :product_design_md5, :string
end
