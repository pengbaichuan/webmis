class AddIndexToJobs < ActiveRecord::Migration
  def self.up
     add_index(:jobs, [:conversion_id])
     add_index(:jobs, [:reference_id])
     add_index(:jobs, [:status])
  end

  def self.down
     remove_index(:jobs, [:conversion_id])
     remove_index(:jobs, [:reference_id])
     remove_index(:jobs, [:status])
  end
end
