class AddIndexToConversionDatabases < ActiveRecord::Migration
  def self.up
     add_index(:conversion_databases, [:conversion_id])
     add_index(:conversion_databases, [:conversion_job_id])
     add_index(:conversion_databases, [:production_orderline_id])
     add_index(:conversion_databases, [:status])
  end

  def self.down
     remove_index(:conversion_databases, [:conversion_id])
     remove_index(:conversion_databases, [:conversion_job_id])
     remove_index(:conversion_databases, [:production_orderline_id])
     remove_index(:conversion_databases, [:status])
  end
end
