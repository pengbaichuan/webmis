class AddVersioningToUsedDesigns < ActiveRecord::Migration
  def change
    add_column :used_product_designs, :version_active_from, :integer
    add_column :used_product_designs, :version_active_until, :integer
  end
end
