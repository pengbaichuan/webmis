class AddParameterIncludeConversionToolPrefixToConfigParameters < ActiveRecord::Migration
  def self.up
    n = ConfigParameter.new(:cfg_name => "include_conversion_tool_prefix", :cfg_value => 'dev,alpha,prod', :description => 'Comma separated list of conversion tool prefixes which should automatically be imported into WebMIS when a new release of them is detected on the filesystem.')
    n.save
  end

  def self.down
    ConfigParameter.delete_all(:cfg_name => "include_conversion_tool_prefix")
  end
end
