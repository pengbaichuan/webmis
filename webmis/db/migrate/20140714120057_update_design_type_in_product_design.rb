class UpdateDesignTypeInProductDesign < ActiveRecord::Migration
  def up
    pds = ProductDesign.where("name like '%front%'")
    pds.each do |pd|
      pd.design_type = "frontend"
      pd.save
    end
    pds = ProductDesign.where("design_type is null")
    pds.each do |pd|
      pd.design_type = "backend"
      pd.save
    end
  end

  def down
    pds = ProductDesign.all
    pds.each do |pd|
      pd.design_type = nil
      pd.save
    end
  end
end
