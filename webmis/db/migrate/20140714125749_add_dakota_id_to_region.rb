class AddDakotaIdToRegion < ActiveRecord::Migration
  def change
    add_column :regions, :dakota_id, :integer
  end
end
