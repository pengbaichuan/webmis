class AddDakotaIdToCompanyAbbr < ActiveRecord::Migration
  def change
    add_column :company_abbrs, :dakota_id, :integer
  end
end
