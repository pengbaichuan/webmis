class AddDakotaIdToCompanyAbbrVersion < ActiveRecord::Migration
  def change
    add_column :company_abbr_versions, :dakota_id, :integer
  end
end
