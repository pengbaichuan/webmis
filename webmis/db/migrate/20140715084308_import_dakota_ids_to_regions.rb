class ImportDakotaIdsToRegions < ActiveRecord::Migration
  def up
    nomatch = []
    path = '/data/users/dbconv/CVinfo/REGION_IDS.TXT'
    region_ids_txt = File.read(path)
    region_ids_csv = CSV.parse(region_ids_txt, :headers => false)
    2.times do
      # region_ids_csv[2] is first entry
      region_ids_csv.delete(region_ids_csv[0])
    end

    puts "IMPORTING IDS FROM #{path}"
    region_ids_csv.each do |region|
      next if !region[0]
      m = Region.find_by_region_code(region[0].strip)
      if m
        m.dakota_id = region[1].strip
        m.save(:validate => false)
        puts "#{region[1]} IMPORTED TO #{m.name} #{m.id}."
      else
        nomatch << [region[0].strip,region[1].strip]
      end
    end
    puts '=========================================================='
    puts 'FOLLOWING REGION_IDS ARE NOT IMPORTED:'
    nomatch.each do |n|
      puts "#{n[0]} => #{n[1]}"
    end
    puts "IMPORT FINISHED."
  end

  def down
    path = '/data/users/dbconv/CVinfo/REGION_IDS.TXT'
    region_ids_txt = File.read(path)
    region_ids_csv = CSV.parse(region_ids_txt, :headers => false)
    2.times do
      # region_ids_csv[2] is first entry
      region_ids_csv.delete(region_ids_csv[0])
    end

    puts "REVERTING IMPORT IDS FROM #{path}"
    region_ids_csv.each do |region|
      m = Region.find_by_region_code(region[0].strip)
      if m && m.dakota_id.to_s == region[1].strip
        m.dakota_id = nil
        m.save(:validate => false)
        puts "REVERTED IMPORT OF #{m.name} #{m.id} => #{region[1].strip}."
      end
    end
  end
  puts "IMPORT REVERTED."
end
