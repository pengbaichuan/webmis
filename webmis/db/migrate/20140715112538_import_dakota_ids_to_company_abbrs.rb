class ImportDakotaIdsToCompanyAbbrs < ActiveRecord::Migration
  def up
    nomatch = []
    path = '/data/users/dbconv/CVinfo/SUPPLIER_IDS.TXT'
    supplier_ids_txt = File.read(path)
    supplier_ids_csv = CSV.parse(supplier_ids_txt, :headers => false)
    5.times do
      # supplier_ids_csv[5] is first entry
      supplier_ids_csv.delete(supplier_ids_csv[0])
    end

    puts "IMPORTING IDS FROM #{path}"
    supplier_ids_csv.each do |supplier|
      m = CompanyAbbr.find_by_ms_abbr_3(supplier[0].strip)
      if m
        m.dakota_id = supplier[2].strip
        m.save(:validate => false)
        puts "#{supplier[2]} IMPORTED TO #{m.name}."
      else
        nomatch << [supplier[0].strip,supplier[2].strip]
      end
    end
    puts '=========================================================='
    puts 'FOLLOWING SUPPLIER_IDS ARE NOT IMPORTED:'
    nomatch.each do |n|
      puts "#{n[0]} => #{n[1]}"
    end
    puts "IMPORT FINISHED."
  end

  def down
    path = '/data/users/dbconv/CVinfo/SUPPLIER_IDS.TXT'
    supplier_ids_txt = File.read(path)
    supplier_ids_csv = CSV.parse(supplier_ids_txt, :headers => false)
    5.times do
      # supplier_ids_csv[5] is first entry
      supplier_ids_csv.delete(supplier_ids_csv[0])
    end

    puts "REVERTING IMPORT IDS FROM #{path}"
    supplier_ids_csv.each do |supplier|
      m = CompanyAbbr.find_by_ms_abbr_3(supplier[0].strip)
      if m && m.dakota_id.to_s == supplier[2].strip
        m.dakota_id = nil
        m.save(:validate => false)
        puts "REVERTED IMPORT OF #{m.company_name} #{m.id} => #{supplier[2].strip}."
      end
    end
  end
  puts "IMPORT REVERTED."
end
