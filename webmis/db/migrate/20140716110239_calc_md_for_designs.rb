class CalcMdForDesigns < ActiveRecord::Migration
  def up
    ProductDesign.all.each do |pd|
      if !pd.product_design_md5
        pd.product_design_md5 = pd.calc_md5
        pd.save
      end  
    end
  end

  def down
  end
end
