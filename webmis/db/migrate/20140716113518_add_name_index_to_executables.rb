class AddNameIndexToExecutables < ActiveRecord::Migration
  def change
    add_index(:executables, :name)
  end
end
