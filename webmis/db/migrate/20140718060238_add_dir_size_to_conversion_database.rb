class AddDirSizeToConversionDatabase < ActiveRecord::Migration
  def change
    add_column :jobs, :result_dir_size_bytes, :integer
  end
end
