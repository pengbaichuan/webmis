class CreateSpaceReserveds < ActiveRecord::Migration
  def change
    create_table :space_reserved do |t|
      t.string :path
      t.integer :total_size
      t.integer :buffer_size
      t.integer :reserved_size
      t.integer :reserved_amount
      t.integer :lock_version

      t.timestamps
    end
  end
end
