class AddSpaceReserveParameters < ActiveRecord::Migration
  def up
    cp = ConfigParameter.new(:cfg_name => "reserve_buffer_size_bytes", :cfg_value => '209715200', :description => 'When this size (in bytes) is reached move operations to network (mount) will stop')
    cp.save
  end

  def down
  end
end
