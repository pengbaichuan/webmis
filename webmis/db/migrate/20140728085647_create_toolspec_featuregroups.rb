class CreateToolspecFeaturegroups < ActiveRecord::Migration
  def change
    create_table :toolspec_featuregroups do |t|
      t.string :name
      t.text :description
      t.integer :conversiontool_id
      t.integer :cvtool_bundle_id
      t.integer :executable_id

      t.timestamps
    end
  end
end
