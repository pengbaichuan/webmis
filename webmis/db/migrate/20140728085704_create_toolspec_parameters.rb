class CreateToolspecParameters < ActiveRecord::Migration
  def change
    create_table :toolspec_parameters do |t|
      t.string :name
      t.text :description
      t.integer :toolspec_featuregroup_id
      t.string :datatype
      t.string :default
      t.boolean :mandatory

      t.timestamps
    end
  end
end
