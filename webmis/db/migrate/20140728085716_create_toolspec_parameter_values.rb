class CreateToolspecParameterValues < ActiveRecord::Migration
  def change
    create_table :toolspec_parameter_values do |t|
      t.string :value
      t.text :description
      t.integer :toolspec_parameter_id

      t.timestamps
    end
  end
end
