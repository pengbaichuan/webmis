class AddProcessDataVersionToConversionJobProcessDataElement < ActiveRecord::Migration
  def change
    add_column :conversion_job_process_data_elements, :process_data_version, :integer
  end
end
