class RemoveCvtoolFromConversion < ActiveRecord::Migration
  def up
    remove_column :conversions, :cvtool
  end

  def down
    add_column :conversions, :cvtool, :string
  end
end
