class AddStatusToUsedProductDesign < ActiveRecord::Migration
  def change
    add_column :used_product_designs, :status, :integer
  end
end
