class CreateDataSetDefinitions < ActiveRecord::Migration
  def change
    create_table :data_set_definitions do |t|
      t.string :name
      t.integer :company_abbr_id
      t.integer :data_release_id
      t.integer :region_id
      t.integer :data_type_id
      t.integer :filling_id
      t.text :description
      t.text :remarks
      t.integer :coverage_id
      t.integer :version
      t.integer :status

      t.timestamps
    end

    create_table :data_set_definition_versions do |t|
      t.integer :data_set_definition_id
      t.string :name
      t.integer :company_abbr_id
      t.integer :data_release_id
      t.integer :region_id
      t.integer :data_type_id
      t.integer :filling_id
      t.text :description
      t.text :remarks
      t.integer :coverage_id
      t.integer :version
      t.integer :status

      t.timestamps
    end
  end
end
