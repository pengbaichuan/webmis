class AddDataSetDefinitionToProductDataSetInfo < ActiveRecord::Migration
  def change
    add_column :product_data_set_infos, :data_set_definition_id, :integer
    add_column :product_data_set_infos, :data_set_definition_version, :integer
  end
end
