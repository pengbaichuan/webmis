class AddDataSetDefinitionToReception < ActiveRecord::Migration
  def change
    add_column :receptions, :data_set_definition_id, :integer
    add_column :receptions, :data_set_definition_version, :integer
  end
end
