class AddDataSetDefinitionToProcessDataItem < ActiveRecord::Migration
  def change
    add_column :process_data_items, :data_set_definition_id, :integer
    add_column :process_data_items, :data_set_definition_version, :integer
  end
end
