class AddParameterSettingsToCoveragesRegion < ActiveRecord::Migration
  def change
    add_column :coverages_regions, :do_not_process, :boolean
    add_column :coverages_regions, :parameter_settings, :text
  end
end
