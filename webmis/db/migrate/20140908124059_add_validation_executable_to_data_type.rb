class AddValidationExecutableToDataType < ActiveRecord::Migration
  def up
    add_column :data_types, :validation_executable, :string

    sql = "update data_types set validation_executable = 'ValidateDH' where run_script is not null AND run_script != ''"
    ActiveRecord::Base.connection.execute(sql)

    sql = "update data_types set automatic_validation_yn = false where validation_executable is null"
    ActiveRecord::Base.connection.execute(sql)

    remove_column :data_types, :run_script
  end

  def down
    remove_column :data_types, :validation_executable
    add_column :data_types, :run_script, :text
  end
end
