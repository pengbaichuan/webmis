class AddValidationExecutableToDataTypeVersion < ActiveRecord::Migration
  def change
    add_column :data_type_versions, :validation_executable, :string
    remove_column :data_type_versions, :run_script
  end
end
