class ChangeFailreasonToTextOnJob < ActiveRecord::Migration
  def up
    change_column :jobs, :fail_reason, :text
  end

  def down
    change_column :jobs, :fail_reason, :string
  end
end
