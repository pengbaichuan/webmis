class Removevalidatetobusinessprocess < ActiveRecord::Migration
  def up
    BusinessProcess.find_all_by_name("HANDLE VALIDATION").each do |bp|
      Ordertype.all.each do |ot|
        step_for_validate = ot.process_transitions.where("to_bp = #{bp.id}").first
        validate_step = ot.process_transitions.where("from_bp = #{bp.id}").first
        if step_for_validate && validate_step
          step_for_validate.to_bp = validate_step.to_bp
          step_for_validate.action_label = validate_step.action_label
          step_for_validate.save!
          validate_step.destroy
        end
      end
    end

    ProductionOrderline.where(:bp_name => "HANDLE VALIDATION").each do |pol|
      pol.bp_name = "MONITOR CONVERSION"
      pol.save!
    end
    BusinessProcess.delete_all(:name => "HANDLE VALIDATION")
  end

  def down
    BusinessProcess.find_or_create_by_name("HANDLE VALIDATION")
  end
end







class AddValidateToBusinessProcesses < ActiveRecord::Migration
  def up
    BusinessProcess.find_or_create_by_name("HANDLE VALIDATION")

    ConversionDatabase.where(:status => ConversionDatabase::STATUS_HAS_WARNINGS).each do |cd| 
      cd.status = ConversionDatabase::STATUS_NOT_VALIDATED
      cd.save!
    end

    ConversionDatabase.where(:status => ConversionDatabase::STATUS_HAS_ERRORS).each do |cd| 
      cd.status = ConversionDatabase::STATUS_NOT_VALIDATED
      cd.save!
    end
  end

  def down
    BusinessProcess.delete_all(:name => "HANDLE VALIDATION")

    ProductionOrderline.where(:bp_name => "HANDLE VALIDATION").each do |pol|
      pol.bp_name = "COMPLETE"
      pol.save!
    end
  end
end
