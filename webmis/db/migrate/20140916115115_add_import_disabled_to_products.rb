class AddImportDisabledToProducts < ActiveRecord::Migration
  def change
    add_column :products, :import_disabled, :boolean
    add_column :product_versions, :import_disabled, :boolean
  end
end
