class AddFillingIdToReception < ActiveRecord::Migration
  def change
    add_column :receptions, :filling_id, :integer
  end
end
