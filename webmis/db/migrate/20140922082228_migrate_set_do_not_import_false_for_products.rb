class MigrateSetDoNotImportFalseForProducts < ActiveRecord::Migration
  def up
    Product.all.each do |product|
        if product.import_disabled == nil
        product.import_disabled = false
        product.save!(:validate => false)
      end
    end
  end

  def down
    Product.all.each do |product|
        if product.import_disabled == false
        product.import_disabled = nil
        product.save!(:validate => false)
      end
    end    
  end
end
