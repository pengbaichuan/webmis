class ChangeTicketNrToStringForValidationErrors < ActiveRecord::Migration
  def up
    change_column :validation_errors, :ticket_nr, :string
  end

  def down
    change_column :validation_errors, :ticket_nr, :integer
  end
end
