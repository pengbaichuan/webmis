class CreateTargetPlatformUsers < ActiveRecord::Migration
  def change
    create_table :target_platform_users do |t|
      t.integer :user_id
      t.integer :target_platform_id

      t.timestamps
    end
  end
end
