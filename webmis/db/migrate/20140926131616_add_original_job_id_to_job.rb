class AddOriginalJobIdToJob < ActiveRecord::Migration
  def change
    add_column :jobs, :original_job_id, :integer
  end
end
