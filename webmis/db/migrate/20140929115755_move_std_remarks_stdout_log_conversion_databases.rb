class MoveStdRemarksStdoutLogConversionDatabases < ActiveRecord::Migration
  def up
    conversion_databases = ConversionDatabase.where("remarks like '%x86_64%'")
    conversion_databases.each do |conversion_database|
      conversion_database.stdout_log = conversion_database.remarks
      removal_remark = conversion_database.stdout_log.scan(/Marked as removal by.*$/)
      if !removal_remark.empty?
        conversion_database.remarks = removal_remark.join()
      else
        conversion_database.remarks = ''
      end
      conversion_database.save(:validate => false)
    end
  end

  def down
    conversion_databases = ConversionDatabase.where("stdout_log like '%x86_64%'")
    conversion_databases.each do |conversion_database|
      conversion_database.remarks = conversion_database.stdout_log
      conversion_database.stdout_log = ''
      conversion_database.save(:validate => false)
    end
  end
end
