class RenameMantisToBugTrackerIdForVersionedObjects < ActiveRecord::Migration
  def up
    change_column :customer_content_specifications, :mantis, :string
    rename_column :customer_content_specifications, :mantis,:bug_tracker_id
    change_column :customer_content_specification_versions, :mantis, :string
    rename_column :customer_content_specification_versions, :mantis,:bug_tracker_id

    change_column :data_types, :mantis, :string
    rename_column :data_types, :mantis,:bug_tracker_id
    change_column :data_type_versions, :mantis, :string
    rename_column :data_type_versions, :mantis,:bug_tracker_id

    change_column :executables, :mantis, :string
    rename_column :executables, :mantis,:bug_tracker_id
    change_column :executable_versions, :mantis, :string
    rename_column :executable_versions, :mantis,:bug_tracker_id

    change_column :packing_specifications, :mantis, :string
    rename_column :packing_specifications, :mantis,:bug_tracker_id
    change_column :packing_specification_versions, :mantis, :string
    rename_column :packing_specification_versions, :mantis,:bug_tracker_id

    change_column :parameter_settings, :mantis, :string
    rename_column :parameter_settings, :mantis,:bug_tracker_id
    change_column :parameter_setting_versions, :mantis, :string
    rename_column :parameter_setting_versions, :mantis,:bug_tracker_id

    change_column :product_designs, :mantis, :string
    rename_column :product_designs, :mantis,:bug_tracker_id
    change_column :product_design_versions, :mantis, :string
    rename_column :product_design_versions, :mantis,:bug_tracker_id

    change_column :product_features, :mantis, :string
    rename_column :product_features, :mantis,:bug_tracker_id
    change_column :product_feature_versions, :mantis, :string
    rename_column :product_feature_versions, :mantis,:bug_tracker_id
  end

  def down
    rename_column :customer_content_specifications, :bug_tracker_id,:mantis
    rename_column :customer_content_specification_versions, :bug_tracker_id,:mantis

    rename_column :data_types, :bug_tracker_id,:mantis
    rename_column :data_type_versions, :bug_tracker_id,:mantis

    rename_column :executables, :bug_tracker_id,:mantis
    rename_column :executable_versions, :bug_tracker_id,:mantis

    rename_column :packing_specifications, :bug_tracker_id,:mantis
    rename_column :packing_specification_versions, :bug_tracker_id,:mantis

    rename_column :parameter_settings, :bug_tracker_id,:mantis
    rename_column :parameter_setting_versions, :bug_tracker_id,:mantis

    rename_column :product_designs, :bug_tracker_id,:mantis
    rename_column :product_design_versions, :bug_tracker_id,:mantis

    rename_column :product_features, :bug_tracker_id,:mantis
    rename_column :product_feature_versions, :bug_tracker_id,:mantis    
  end
end
