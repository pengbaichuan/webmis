class AddForceCreateToConversion < ActiveRecord::Migration
  def change
    add_column :conversions, :force_create, :boolean, {:default => false}
  end
end
