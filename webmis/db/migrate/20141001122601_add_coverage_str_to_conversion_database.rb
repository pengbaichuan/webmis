class AddCoverageStrToConversionDatabase < ActiveRecord::Migration
  def up
    add_column :conversion_databases, :region_id, :integer

    ConversionDatabase.all.each do |cd|
      mytagmodel = cd.tagmodel(['coverage'])['coverage']
      if mytagmodel && !mytagmodel.empty?
        cd.region = Region.find_by_region_code(mytagmodel.to_s)
      else
        cd.region = cd.conversion.region if cd.conversion
      end
      cd.save!
    end

  end

  def down
    remove_column :conversion_databases, :region_id
  end
end
