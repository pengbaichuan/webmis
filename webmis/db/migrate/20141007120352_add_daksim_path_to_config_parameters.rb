class AddDaksimPathToConfigParameters < ActiveRecord::Migration
    def up 
      cp = ConfigParameter.new(:cfg_name => "dakota_simulator_dir", :cfg_value => '/data/id/release/Rails/WebMIS_latest/script', :description => 'Dakota simulator path from WebMIS release dir')
      cp.save
    end
  
    def down
      ConfigParameter.delete_all(:cfg_name => "dakota_simulator_dir")
    end
end
