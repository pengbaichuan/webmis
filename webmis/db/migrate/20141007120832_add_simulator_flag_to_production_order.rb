class AddSimulatorFlagToProductionOrder < ActiveRecord::Migration
  def change
    add_column :production_orders, :use_dakota_simulator_yn, :boolean, {:default => false}
  end
end
