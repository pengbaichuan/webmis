class AddOutcomePersistentYnToConversion < ActiveRecord::Migration
  def change
    add_column :conversions, :outcome_persistent_yn, :boolean, :default => false
  end
end
