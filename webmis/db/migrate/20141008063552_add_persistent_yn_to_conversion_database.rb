class AddPersistentYnToConversionDatabase < ActiveRecord::Migration
  def change
    add_column :conversion_databases, :persistent_yn, :boolean, :default => false
  end
end
