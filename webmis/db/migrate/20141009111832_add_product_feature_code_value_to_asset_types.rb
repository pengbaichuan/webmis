class AddProductFeatureCodeValueToAssetTypes < ActiveRecord::Migration

  def up
    gencode =  "week = sprintf('%02d',Date.today.cweek)\r\n"
    gencode += "year = Time.now.year.to_s[2..3]\r\n"
    gencode += "id = \"\#{name}\#{week}\#{year}\"\r\n"
    gencode += "serial =  sprintf('%02d',next_serial(:id => id))\r\n"
    gencode += "return \"\#{year}\#{week}\#{serial}\"\r\n"
    description = "Will be used by the system during the save of a product_feature to generate a product feature abbr_code automatically."

    AssetType.find_or_create_by_name("Product feature code") do |at|
      at.description = description
      at.gencode = gencode
    end
  end

  def down
    AssetType.delete_all(:name => "Product feature code")
  end
end
