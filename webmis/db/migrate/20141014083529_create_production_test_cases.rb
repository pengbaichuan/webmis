class CreateProductionTestCases < ActiveRecord::Migration
  def change
    create_table :production_test_cases do |t|
      t.string :name
      t.boolean :create_process_data
      t.boolean :use_daksim
      t.string :schedule
      t.integer :project_id
      t.boolean :remove_results
      t.boolean :remove_process_data
      t.string :expire_time
      t.string :tool_select
      t.boolean :synchronous
      t.integer :design_id

      t.timestamps
    end
  end
end
