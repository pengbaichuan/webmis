class CreateProjectServers < ActiveRecord::Migration
  def change
    create_table :project_servers do |t|
      t.integer :project_id
      t.integer :server_id

      t.timestamps
    end
  end
end
