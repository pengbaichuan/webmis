class CleanupToolspecImports < ActiveRecord::Migration
  def cleanup_io(io)
    old_io = JSON.parse(io)
    used_datatypes = old_io.collect{|x| x['datatype'].to_s.upcase}.uniq
    new_io = []
    used_datatypes.each do |datatype_str|
      new_elem = {"datatype" => nil, "id" => nil, "version" => nil, "mandatory" => false, "script_tag" => ''}
      old_io.select{|x| x['datatype'].to_s.upcase == datatype_str}.each do |old_elem|
        new_elem["datatype"] = old_elem["datatype"] unless new_elem["datatype"]
        new_elem["id"] = old_elem["id"] unless new_elem["id"]
        new_elem["version"] = old_elem["version"] unless new_elem["version"]
        new_elem["mandatory"] = old_elem["mandatory"] unless new_elem["mandatory"]
        new_elem['script_tag'] = old_elem['script_tag'] if new_elem['script_tag']
      end
      new_io << new_elem
    end
    return new_io.to_json
  end

  def up
    Executable.all.each do |executable|
      executable.input = cleanup_io(executable.input)
      executable.output = cleanup_io(executable.output)
      executable.comment = "Updated by migration script 20141014125046_cleanup_toolspec_imports.rb"
      executable.updated_by = "Rails"
      executable.save!
    end
  end

  def down
  end
end
