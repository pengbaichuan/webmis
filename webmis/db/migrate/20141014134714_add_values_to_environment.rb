class AddValuesToEnvironment < ActiveRecord::Migration
  def up
    env_prod = Environment.new({:name => "production"})
    env_prod.save
    env_dev  = Environment.new({:name => "development"})
    env_dev.save
  end

  def down
    env_prod = Environment.find_by_name("production")
    env_prod.destroy if env_prod
    env_dev = Environment.find_by_name("development")
    env_dev.destroy if env_dev
  end
end
