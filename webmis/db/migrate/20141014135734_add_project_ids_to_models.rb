class AddProjectIdsToModels < ActiveRecord::Migration
  def change
    add_column :config_parameters,          :project_id, :integer
    add_column :roles_users,                :project_id, :integer
    add_column :product_designs,            :project_id, :integer
    add_column :product_design_versions,    :project_id, :integer
    add_column :production_orders,          :project_id, :integer
    add_column :product_features,           :project_id, :integer
    add_column :product_feature_versions,   :project_id, :integer
    add_column :parameter_settings,         :project_id, :integer
    add_column :parameter_setting_versions, :project_id, :integer
  end
end
