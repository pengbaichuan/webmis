class FillProjectIdInModelsWithProduction < ActiveRecord::Migration
  def up
    Project.transaction do
      prj = Project.new({:name => "Production",:status => 20})
      env = Environment.find_by_name("production")
      prj.environment_id = env.id
      prj.save!

      ConfigParameter.update_all :project_id => prj.id
      RolesUser.update_all :project_id => prj.id
      ProductDesign.update_all :project_id => prj.id
      ProductDesignVersion.update_all :project_id => prj.id
      ProductionOrder.update_all :project_id => prj.id
      ProductFeature.update_all :project_id => prj.id
      ProductFeatureVersion.update_all :project_id => prj.id
      ParameterSetting.update_all :project_id => prj.id
      ParameterSettingVersion.update_all :project_id => prj.id
    end
  end

  def down
    Project.transaction do
      prj = Project.find_by_name("Production")

      ConfigParameter.where(:project_id => prj.id).update_all :project_id => nil
      RolesUser.where(:project_id => prj.id).update_all :project_id => nil
      ProductDesign.where(:project_id => prj.id).update_all :project_id => nil
      ProductDesignVersion.where(:project_id => prj.id).update_all :project_id => nil
      ProductionOrder.where(:project_id => prj.id).update_all :project_id => nil
      ProductFeature.where(:project_id => prj.id).update_all :project_id => nil
      ProductFeatureVersion.where(:project_id => prj.id).update_all :project_id => nil
      ParameterSetting.where(:project_id => prj.id).update_all :project_id => nil
      ParameterSettingVersion.where(:project_id => prj.id).update_all :project_id => nil
      prj.destroy
    end
  end
end
