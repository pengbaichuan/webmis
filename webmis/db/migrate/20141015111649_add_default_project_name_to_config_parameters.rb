class AddDefaultProjectNameToConfigParameters < ActiveRecord::Migration
  def up
    n = ConfigParameter.new(:cfg_name => "default_project_name", :cfg_value => 'Production', :description => 'Default project')
    n.save
  end

  def down
    ConfigParameter.delete_all(:cfg_name => "default_project_name")
  end
end
