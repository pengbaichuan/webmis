class AddEachUserToProjectProduction < ActiveRecord::Migration
  def up
    User.transaction do
      prj = Project.find_by_name("Production")
      User.all.each do |u|
        l = ProjectUser.new({:user_id => u.id,:project_id => prj.id})
        l.save
      end
    end
  end

  def down
    prj = Project.find_by_name("Production")
    User.where(:project_id => prj.id).each do |pu|
      pu.destroy
    end
  end
end
