class AddEachServerToProjectProduction < ActiveRecord::Migration
  def up
    Server.transaction do
      prj = Project.find_by_name("Production")
      Server.all.each do |s|
        s = ProjectServer.new({:server_id => s.id,:project_id => prj.id})
        s.save!
      end
    end
  end

  def down
    prj = Project.find_by_name("Production")
    ProjectServer.where(:project_id => prj.id).each do |ps|
      ps.destroy
    end
  end
end
