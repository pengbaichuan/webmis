class FillUserProfiles < ActiveRecord::Migration
  def up
    User.all.each do |u|
      profile = UserProfile.new({:user_id => u.id,:last_used_project_id => Project.default.id})
      profile.save
    end
  end

  def down
    User.all.each do |u|
      if u.user_profile
        profile = u.user_profile
        profile.destroy
      end
    end
  end
end
