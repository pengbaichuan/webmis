class AddTargetPlatformIdToParameterSetting < ActiveRecord::Migration
  def change
    add_column :parameter_settings, :target_platform_id, :integer
    add_column :parameter_setting_versions, :target_platform_id, :integer
  end
end
