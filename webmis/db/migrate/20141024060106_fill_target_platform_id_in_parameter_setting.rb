class FillTargetPlatformIdInParameterSetting < ActiveRecord::Migration
  def up
    TargetPlatform.all.each do |tp|
      ps = tp.parameter_setting
      if ps
        ps.target_platform_id = tp.id
        ps.save(:validate => false)
      end
    end
  end

  def down
    ParameterSetting.all.each do |ps|
      if !ps.target_platform_id.blank?
        tp = TargetPlatform.find(ps.target_platform_id)
        if tp
          tp.parameter_setting_id = ps.id
          tp.save(:validate => false)
        end
      end
    end
  end
end
