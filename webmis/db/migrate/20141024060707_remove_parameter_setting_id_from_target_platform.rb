class RemoveParameterSettingIdFromTargetPlatform < ActiveRecord::Migration
  def change
    remove_column :target_platforms, :parameter_setting_id
  end
end
