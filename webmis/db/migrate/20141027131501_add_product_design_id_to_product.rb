class AddProductDesignIdToProduct < ActiveRecord::Migration
  def up
    add_column :products, :product_design_id, :integer
    add_column :product_versions, :product_design_id, :integer
    Product.where("product_design IS NOT NULL").each do |product|
      if product.product_design && product.product_design.match(/^(\d{1,})$/)
        pd = ProductDesign.find(product.product_design)
        if pd
          product.product_design_id = pd.id
          product.save!(:validate => false)
        end
      end
    end
  end

  def down
    remove_column :products, :product_design_id
    remove_column :product_versions, :product_design_id
  end
end
