class RemoveProductDesignFromProduct < ActiveRecord::Migration
  def up
    remove_column :products, :product_design
    remove_column :product_versions, :product_design
  end

  def down
    add_column :products, :product_design, :string
    add_column :product_versions, :product_design, :string
  end
end
