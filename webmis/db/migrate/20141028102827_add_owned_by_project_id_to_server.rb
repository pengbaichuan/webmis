class AddOwnedByProjectIdToServer < ActiveRecord::Migration
  def change
    add_column :servers, :owned_by_project_id, :integer
    remove_column :servers, :max_work
  end
end
