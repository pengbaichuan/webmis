class FillOwnedByProjectIdInServer < ActiveRecord::Migration
  def up
    Server.all.each do |s|
      s.owned_by_project_id = Project.default.id
      s.save!
    end
  end

  def down
    Server.all.each do |s|
      s.owned_by_project_id = nil
      s.save!
    end
  end
end
