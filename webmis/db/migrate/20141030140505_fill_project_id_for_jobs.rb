class FillProjectIdForJobs < ActiveRecord::Migration
  def up
    Job.all.each do |j|
      j.project_id = Project.default.id
      j.save!
    end
  end
  
  def down
    Job.all.each do |j|
      j.project_id = nil
      j.save!
    end
  end
end
