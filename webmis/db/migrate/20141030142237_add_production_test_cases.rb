class AddProductionTestCases < ActiveRecord::Migration
  def up
    add_column :production_test_cases, :is_frontend, :boolean
    pc = ProductionTestCase.setup("Back End Simple Product")
    pc.is_frontend = false
    pc.use_daksim = true
    pc.save!
    pc = ProductionTestCase.setup("Front End RDF2DH Her")
    pc.is_frontend = true
    pc.use_daksim = true
    pc.save!
  end

  def down
    remove_column :production_test_cases, :is_frontend, :boolean
  end
end
