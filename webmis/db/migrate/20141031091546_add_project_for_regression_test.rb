class AddProjectForRegressionTest < ActiveRecord::Migration
  def up
    pc = Project.find_or_create_by_name("Regression Test")
    env = Environment.find_by_name("Development")
    if !env
      raise "dev environment not found"
    end
    pc.environment_id = env.id 
    pc.description = "Regression test project"
    pc.status = 20
    pc.environment_id = env.id
    pc.save
    pu = ProjectUser.new
    pu.project_id = pc.id
    pu.user_id = User.find_by_user_name("johan.hendrikx").id
    pu.save!
    RolesUser.find_all_by_user_id(User.find_by_user_name("johan.hendrikx").id).each{|x|x.dup;x.project_id = pc.id;x.save}
  end

  def down
    pc = Project.find_by_name("Regression Test")
    pc.destroy
  end
end
