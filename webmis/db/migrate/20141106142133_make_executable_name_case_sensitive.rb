class MakeExecutableNameCaseSensitive < ActiveRecord::Migration
  def up
    sql = "ALTER TABLE executables MODIFY `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin"
    ActiveRecord::Base.connection.execute(sql)
    sql = "ALTER TABLE executable_versions MODIFY `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin"
    ActiveRecord::Base.connection.execute(sql)
  end

  def down
    sql = "ALTER TABLE executables MODIFY `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci"
    ActiveRecord::Base.connection.execute(sql)
    sql = "ALTER TABLE executable_versions MODIFY `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci"
    ActiveRecord::Base.connection.execute(sql)
  end
end
