class AddReviewUseridToProcessData < ActiveRecord::Migration
  def change
    add_column :process_data, :review_user_id, :integer
    add_column :process_data, :review_remark, :text
  end
end
