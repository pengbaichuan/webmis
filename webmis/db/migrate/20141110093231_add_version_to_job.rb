class AddVersionToJob < ActiveRecord::Migration
  def change
    add_column :jobs, :version, :integer
  end
end
