class CreateJobVersions < ActiveRecord::Migration
  def change
    create_table :job_versions do |t|
      t.string :conversion_id
      t.string :run_server
      t.string :run_command
      t.text :run_script
      t.string :run_pid
      t.string :working_dir
      t.datetime :start_time
      t.datetime :finish_time
      t.text :stdout_log
      t.integer :exitcode
      t.text :fail_reason
      t.boolean :schedule_manual
      t.integer :schedule_cores
      t.integer :schedule_memory
      t.integer :schedule_diskspace
      t.boolean :schedule_allow_virtual
      t.boolean :schedule_exclusive
      t.datetime :created_at
      t.datetime :updated_at
      t.integer :status
      t.integer :conversion_environment_id
      t.integer :lock_version
      t.string :move_status
      t.integer :reference_id
      t.string :type
      t.string :bug_tracker_id
      t.integer :group_amount
      t.string :group_by
      t.string :group_value
      t.text :action_log
      t.string :fail_category
      t.integer :fail_category_logged_by_user_id
      t.date :fail_category_logged_on
      t.integer :estimated_duration
      t.integer :result_dir_size_bytes
      t.integer :original_job_id
      t.integer :project_id
      t.integer :version
      t.integer :job_id

      t.timestamps
    end
  end
end
