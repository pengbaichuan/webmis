class FillIntegerForVersionToJobs < ActiveRecord::Migration
  def up
    Job.transaction do
      Job.all.each do |j|
        if j.version.nil?
          j.version = 0
          j.save!(:validate => false)
        end
      end
    end
  end

  def down
    Job.transaction do
      Job.all.each do |j|
        if j.version == 0
          j.version = nil
          j.save!(:validate => false)
        end
      end
    end
  end
end
