class FixDatatypeIdInInputOutputOfExecutables < ActiveRecord::Migration
  def up
    Executable.all.each do |e|
      input = JSON.parse(e.input)
      input_changed = false
      input.each do |i|
        if i["id"].blank?
          dt = DataType.find_by_name(i["datatype"])
          if dt
            i["id"] = dt.id
            i["version"] = dt.version
            input_changed = true
          else
            puts "Not found #{i['datatype']} for executable #{e.name}"
          end
        end
      end
      e.input = input.to_json if input_changed

      output = JSON.parse(e.output)
      output_changed = false
      output.each do |o|
        if o["id"].blank?
          dt = DataType.find_by_name(o["datatype"])
          if dt
            o["id"] = dt.id
            o["version"] = dt.version
            output_changed = true
          else
            puts "Not found #{o['datatype']} for executable #{e.name}"
          end
        end
      end
      e.output = output.to_json if output_changed
      
      e.save! if input_changed || output_changed
    end
  end

  def down
    # no undo as this migration will only be filling missing attributes in the input and output JSONs
  end
end
