class FixApprovedVersionInInputOutputOfExecutables < ActiveRecord::Migration
  def up
    Executable.all.each do |e|
      input = JSON.parse(e.input)
      input_changed = false
      input.each do |i|
        dt = DataType.find_by_name(i["datatype"])
        if dt && dt.id != i["id"]
          dt.use_latest_baseline
          i["id"] = dt.id
          i["version"] = dt.version
          input_changed = true
        end
      end
      e.input = input.to_json if input_changed

      output = JSON.parse(e.output)
      output_changed = false
      output.each do |o|
        dt = DataType.find_by_name(o["datatype"])
        if dt && dt.id != o["id"]
          dt.use_latest_baseline
          o["id"] = dt.id
          o["version"] = dt.version
          output_changed = true
        end
      end
      e.output = output.to_json if output_changed
      
      e.save! if input_changed || output_changed
    end
  end

  def down
    # no undo as this migration will only be replacing wrong id and version attributes in the input and output JSONs
  end
end
