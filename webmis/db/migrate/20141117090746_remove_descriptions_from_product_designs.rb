class RemoveDescriptionsFromProductDesigns < ActiveRecord::Migration
  def up
    ProductDesign.active.each do |pd|
      if pd.has_conversion_steps?
        h = pd.modelhash
        h["conversion_steps"].each do |step|
          if step.class.name == "Hash"
            step.delete_if{|k,v|k == "description"}
            if step["parameters_spec"]
              step["parameters_spec"].each do |spec|
                spec.delete_if{|k,v|k == "description"}
              end
            end
            if step["pre_improvement_steps"]
              step["pre_improvement_steps"].each do |impstep|
                impstep.delete_if{|k,v|k == "description"}
              end
            end
            if step["post_improvement_steps"]
              step["post_improvement_steps"].each do |impstep|
                impstep.delete_if{|k,v|k == "description"}
              end
            end
            if step["connections"]
              step["connections"].each do |conn|
                if conn["target_spec"]
                  conn["target_spec"].delete_if{|k,v|k == "description"}
                end
              end
            end
          end
          pd.model = h.to_json
          pd.save!
        end

      end
    end
  end

  def down
    # not possible
  end
end
