class RenameDefaultShippingSpecificationForTargetPlatforms < ActiveRecord::Migration
  def up
    rename_column :target_platforms, :default_shipping_specification_id, :shipping_specification_id
    remove_column :shipping_specifications, :target_platform_id
  end

  def down
    rename_column :target_platforms, :shipping_specification_id, :default_shipping_specification_id
    add_column :shipping_specifications, :target_platform_id, :integer
  end
end
