class AddIndexesToProcessDataItems < ActiveRecord::Migration
  def change
    add_index(:process_data_items, [:process_data_id])
    add_index(:process_data_elements, [:process_data_item_id])
  end
end
