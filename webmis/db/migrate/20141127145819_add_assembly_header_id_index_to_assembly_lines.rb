class AddAssemblyHeaderIdIndexToAssemblyLines < ActiveRecord::Migration
  def change
    add_index(:assembly_lines, :assembly_header_id)
  end
end
