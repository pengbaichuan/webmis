class AddIndexesToIncludedproducts < ActiveRecord::Migration
  def change
    add_index(:includedproducts, :product_id)
  end
end
