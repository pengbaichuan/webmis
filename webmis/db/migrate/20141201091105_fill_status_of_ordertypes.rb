class FillStatusOfOrdertypes < ActiveRecord::Migration
  def up
    Ordertype.update_all(:status => 20)
  end

  def down
    Ordertype.update_all(:status => nil)
  end
end
