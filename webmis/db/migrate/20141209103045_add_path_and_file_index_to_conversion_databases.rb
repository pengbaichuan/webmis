class AddPathAndFileIndexToConversionDatabases < ActiveRecord::Migration
  def change
    add_index(:conversion_databases, :path_and_file)
  end
end
