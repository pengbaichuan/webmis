class AddRemovedYnToProcessDataItem < ActiveRecord::Migration
  def change
    add_column :process_data_items, :removed_yn, :boolean, :default => false
  end
end
