class AddCidRegionIdIndexToCoveragesRegions < ActiveRecord::Migration
  def change
    add_index :coverages_regions, [:coverage_id, :region_id]
  end
end
