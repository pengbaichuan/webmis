class AddExpirationDateToProductionOrders < ActiveRecord::Migration
  def change
    add_column :production_orders, :expiry_date, :date
  end
end
