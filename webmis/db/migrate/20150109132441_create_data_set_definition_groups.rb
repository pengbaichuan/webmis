class CreateDataSetDefinitionGroups < ActiveRecord::Migration
  def change
    create_table :data_set_definition_groups do |t|
      t.string :company_abbr
      t.string :data_release
      t.string :region_code
      t.string :data_type
      t.string :filling
      t.string :suffix
      t.text :description
      t.integer :status

      t.timestamps
    end
  end
end
