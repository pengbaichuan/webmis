class CreateDataSetDefinitionGroupLines < ActiveRecord::Migration
  def change
    create_table :data_set_definition_group_lines do |t|
      t.integer :data_set_definition_id
      t.integer :data_set_definition_group_id

      t.timestamps
    end
  end
end
