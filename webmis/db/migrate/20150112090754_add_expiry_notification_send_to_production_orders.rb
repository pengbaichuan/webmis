class AddExpiryNotificationSendToProductionOrders < ActiveRecord::Migration
  def change
    add_column :production_orders, :expiry_notification_send, :boolean
  end
end
