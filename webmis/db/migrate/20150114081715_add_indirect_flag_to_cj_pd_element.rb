class AddIndirectFlagToCjPdElement < ActiveRecord::Migration
  def change
    add_column :conversion_job_process_data_elements, :indirect, :boolean
  end
end
