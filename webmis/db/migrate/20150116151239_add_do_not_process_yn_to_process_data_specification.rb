class AddDoNotProcessYnToProcessDataSpecification < ActiveRecord::Migration
  def up
    add_column :process_data_specifications, :do_not_process_yn, :boolean
    ProcessDataSpecification.all.each do |pdspec|
      pdspec.do_not_process_yn = false
      pdspec.save(:validate => false)
    end
  end

  def down
    remove_column :process_data_specifications, :do_not_process_yn
  end
end
