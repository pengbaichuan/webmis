class AddDefaultToCanBeRemovedCd < ActiveRecord::Migration
  def change
     sql="ALTER TABLE `conversion_databases` CHANGE `can_be_removed_yn` `can_be_removed_yn` tinyint(1) NOT NULL DEFAULT 0"
     ActiveRecord::Base.connection.execute(sql)
  end
end
