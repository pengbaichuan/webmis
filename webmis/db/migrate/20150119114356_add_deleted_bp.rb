class AddDeletedBp < ActiveRecord::Migration
  def up
    bp = BusinessProcess.find_or_create_by_name("DELETED")
    bp.description = "Expired order, resources deleted"
    bp.save!
  end

  def down
    bp = BusinessProcess.find_by_name("DELETED")
    bp.destroy if bp
  end
end
