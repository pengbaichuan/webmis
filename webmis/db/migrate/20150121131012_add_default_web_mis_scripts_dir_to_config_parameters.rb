class AddDefaultWebMisScriptsDirToConfigParameters < ActiveRecord::Migration
  def up
    n = ConfigParameter.new(:cfg_name => "default_WebMisScripts_dir", :cfg_value => '/volumes2/WebMisScripts/', :description => 'Default WebMisScripts directory')
    n.save
  end
    
  def down
    ConfigParameter.delete_all(:cfg_name => "default_WebMisScripts_dir")
  end
end
