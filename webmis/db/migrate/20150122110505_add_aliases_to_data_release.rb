class AddAliasesToDataRelease < ActiveRecord::Migration
  def change
    add_column :data_releases, :aliases, :string
  end
end
