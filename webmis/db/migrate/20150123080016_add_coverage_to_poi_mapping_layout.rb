class AddCoverageToPoiMappingLayout < ActiveRecord::Migration
  def up
    add_column :poi_mapping_layouts, :coverage_id, :integer

    PoiMappingLayout.reset_column_information

    PoiMappingLayout.all.each do |poi|
      region = Region.find(poi.region_id)
      coverage = Coverage.new
      coverage.save!
      coverage.add_region(poi.region_id)
      poi.coverage = coverage
      poi.save!
    end

    remove_column :poi_mapping_layouts, :region_id
  end

  def down
    add_column :poi_mapping_layouts, :region_id, :integer

    PoiMappingLayout.reset_column_information

    PoiMappingLayout.all.each do |poi|
      poi.region_id = poi.coverage.regions.first.id if poi.coverage && poi.coverage.regions.first
      poi.save!
    end

    remove column :poi_mapping_layouts, :coverage_id
  end
end
