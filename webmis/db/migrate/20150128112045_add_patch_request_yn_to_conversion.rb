class AddPatchRequestYnToConversion < ActiveRecord::Migration
  def change
    add_column :conversions, :patch_request_yn, :boolean, :default => false
  end
end
