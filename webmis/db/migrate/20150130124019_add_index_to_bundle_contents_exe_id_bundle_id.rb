class AddIndexToBundleContentsExeIdBundleId < ActiveRecord::Migration
  def change
    add_index(:bundle_contents, [:executable_id, :cvtool_bundle_id])
  end
end
