class AddSchedulerLogToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :scheduler_log, :text
  end
end
