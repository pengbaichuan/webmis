class CreateTestAreaConfigurationVersions < ActiveRecord::Migration
  def change
    create_table :test_area_configuration_versions do |t|
      t.string  :name
      t.integer :region_id
      t.integer :target_platform_id
      t.integer :nr_of_test_areas_x
      t.integer :nr_of_test_areas_y
      t.decimal :min_x, :precision => 8, :scale => 3
      t.decimal :max_x, :precision => 8, :scale => 3
      t.decimal :min_y, :precision => 8, :scale => 3
      t.decimal :max_y, :precision => 8, :scale => 3
      t.integer :version
      t.integer :test_area_configuration_id

      t.timestamps
    end
  end
end
