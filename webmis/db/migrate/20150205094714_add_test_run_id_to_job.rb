class AddTestRunIdToJob < ActiveRecord::Migration
  def change
    add_column :jobs, :test_run_id, :integer
    add_column :jobs, :test_case_name, :string
  end
end
