class CreateTestRequestJobConversionDatabases < ActiveRecord::Migration
  def change
    create_table :test_request_job_conversion_databases do |t|
      t.integer :test_request_job_id
      t.integer :conversion_database_id
      t.string :relation_type

      t.timestamps
    end
  end
end
