class CreateTestPlans < ActiveRecord::Migration
  def change
    create_table :test_plans do |t|
      t.string :name
      t.integer :target_platform_id
      t.text :plan_details
      t.integer :status

      t.timestamps
    end
  end
end
