class AddParallelFlagToTestRequest < ActiveRecord::Migration
  def change
    add_column :test_requests, :test_plan_id,  :integer
    add_column :test_requests, :run_parallel,  :boolean
    add_column :test_requests, :use_scheduler, :boolean
    add_column :test_requests, :run_locally,   :boolean
  end
end
