class AddDefaultPlanToTestPlan < ActiveRecord::Migration
  def change
    tc = TestPlan.new
    tc.plan_details =  "[{\"test_case\":\"analyses\",\"parameters\":[]},{\"test_case\":\"comparetemplate\",\"parameters\":[]},{\"test_case\":\"mapstatistics\",\"parameters\":[]},{\"test_case\":\"namecompare\",\"parameters\":[]},{\"test_case\":\"roadconnectivity\",\"parameters\":[]},{\"test_case\":\"routing\",\"parameters\":[]},{\"test_case\":\"validate\",\"parameters\":[]},{\"test_case\":\"3dcompare\",\"parameters\":[]},{\"test_case\":\"adascompare\",\"parameters\":[]},{\"test_case\":\"detailedmapcompare\",\"parameters\":[]},{\"test_case\":\"landusecompare\",\"parameters\":[]},{\"test_case\":\"poicompare\",\"parameters\":[]},{\"test_case\":\"navigability\",\"parameters\":[]},{\"test_case\":\"sql_query_test\",\"parameters\":[]},{\"test_case\":\"levelconnectivity\",\"parameters\":[]},{\"test_case\":\"ThreeDCompare\",\"parameters\":[]},{\"test_case\":\"routingtestcase\",\"parameters\":[]},{\"test_case\":\"travelguidevalidate\",\"parameters\":[]},{\"test_case\":\"acttestcaserunner\",\"parameters\":[]},{\"test_case\":\"act\",\"parameters\":[]},{\"test_case\":\"speechinput\",\"parameters\":[]}]" 
    tc.name = 'Default Test Plan'
    tc.save
  end
end
