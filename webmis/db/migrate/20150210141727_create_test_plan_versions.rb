class CreateTestPlanVersions < ActiveRecord::Migration
  def up
    add_column :test_plans, :version, :integer
    create_table :test_plan_versions do |t|
      t.integer :test_plan_id
      t.string :name
      t.integer :target_platform_id
      t.text :plan_details
      t.integer :status
      t.integer :version

      t.timestamps
    end
    TestPlan.all.each do |tp|
      tp.version = 1
      tp.save(:validate => false)
    end
  end

  def down
    remove_column :test_plans, :version
    drop_table :test_plan_versions
  end
end
