class AddMaptestScriptsDirParameter < ActiveRecord::Migration
  def up
    dak = ConfigParameter.new(:cfg_name => "maptest_scripts_dir", :cfg_value => '/data/ops/backoffice/prod/scripts/', :description => 'Directory for test-request-job maptest xml files')
    dak.save
  end

  def down
    ConfigParameter.delete_all(:cfg_name => "maptest_scripts_dir")
  end
end
