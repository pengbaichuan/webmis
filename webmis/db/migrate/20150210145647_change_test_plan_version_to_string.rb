class ChangeTestPlanVersionToString < ActiveRecord::Migration
  def up
    change_column(:test_plans, :status, :string)
    change_column(:test_plan_versions, :status, :string)
  end

  def down
    change_column(:test_plans, :status, :integer)
    change_column(:test_plan_versions, :status, :integer)
  end
end
