class CreateJobTypes < ActiveRecord::Migration
  def up
    create_table :job_types do |t|
      t.references :server
      t.string :name
      t.boolean :allowed_yn

      t.timestamps
    end

    JobType.reset_column_information
    Server.all.each do |server|
      %w{ConversionJob ValidationJob}.each do |type_name|
        job_type = JobType.new
        job_type.name = type_name
        job_type.allowed_yn = true
        job_type.server = server
        job_type.save!
      end
    end

    add_index :job_types, :server_id
  end

  def down
    remove_index :job_types, :server_id

    drop_table :job_types
  end
end
