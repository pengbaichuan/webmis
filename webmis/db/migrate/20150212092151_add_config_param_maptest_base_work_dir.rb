class AddConfigParamMaptestBaseWorkDir < ActiveRecord::Migration
    def up
        cp = ConfigParameter.new(:cfg_name => "maptest_base_work_dir", :cfg_value => '/volumes1/maptest', :description => 'Maptest base work dir')
        cp.save
    end

    def down
        ConfigParameter.delete_all(:cfg_name => "maptest_base_work_dir")
    end

end
