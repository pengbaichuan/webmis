class AddUpdatedByToTestPlans < ActiveRecord::Migration
  def change
    add_column :test_plans, :updated_by, :string
    add_column :test_plan_versions, :updated_by, :string
  end
end
