class AddMergeLocationFlagToJob < ActiveRecord::Migration
  def change
    add_column :jobs, :merge_location, :boolean
    add_column :job_versions, :merge_location, :boolean
    add_column :job_versions, :test_case_name, :string
    add_column :job_versions, :test_run_id, :integer
  end
end
