class AddProjectIdToTestRequests < ActiveRecord::Migration
  def up
    add_column :test_requests, :project_id, :integer

    TestRequest.reset_column_information

    TestRequest.all.each do |test_request|
      test_request.project_id = Project.default.id
      test_request.save!
    end
  end

  def down
    remove_column :test_requests, :project_id
  end
end
