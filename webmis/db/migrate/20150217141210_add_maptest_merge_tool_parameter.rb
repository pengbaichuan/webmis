
class AddMaptestMergeToolParameter < ActiveRecord::Migration
  def up
      cp = ConfigParameter.new(:cfg_name => "maptest_merge_tool", :cfg_value => '/data/id/release/VVVTools/TestTools_latest/x86_64_Ubuntu_12.04/MapTestResultMerge/bin/mtresultmerge', :description => 'Maptest Merge tool location and executable')
      cp.save
  end

  def down
      ConfigParameter.delete_all(:cfg_name => "maptest_merge_tool")
  end

end
