class ChangeJobRunCommandToText < ActiveRecord::Migration
  def up
    change_column :jobs,:run_command, :text
  end

  def down
    change_column :jobs,:run_command, :string
  end
end
