class AlterColumnForceCreateToIntInConversions < ActiveRecord::Migration
  def up
    add_column :conversions, :force_create_id, :integer
    remove_column :conversions, :force_create
  end

  def down
    remove_column :conversions, :force_create_id
    add_column :conversions, :force_create, :boolean, :default => false
  end
end
