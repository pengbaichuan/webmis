class AddIgnoreDsdRegionsYnToExecutable < ActiveRecord::Migration
  def change
    add_column :executables, :ignore_dsd_regions_yn, :boolean, :default => false
    add_column :executable_versions, :ignore_dsd_regions_yn, :boolean , :default => false
  end
end
