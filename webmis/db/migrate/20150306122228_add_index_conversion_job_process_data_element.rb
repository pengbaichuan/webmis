class AddIndexConversionJobProcessDataElement < ActiveRecord::Migration
  def change
    add_index :conversion_job_process_data_elements, [:conversion_job_id, :process_data_element_id], :name => "conversion_job_id_process_data_element_id"
  end
end
