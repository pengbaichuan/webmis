class CreateTestToolReleases < ActiveRecord::Migration
  def change
    create_table :test_tool_releases do |t|
      t.string :release_name
      t.boolean :active_yn

      t.timestamps
    end
  end
end
