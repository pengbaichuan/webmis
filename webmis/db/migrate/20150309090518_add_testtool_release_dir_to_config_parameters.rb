class AddTesttoolReleaseDirToConfigParameters < ActiveRecord::Migration
   def self.up
       n = ConfigParameter.new(:cfg_name => "testtool_release_directory", :cfg_value => '/data/id/release/VVVTools', :description => 'Directory path for released test tools')
       n.save
   end

   def self.down
       ConfigParameter.delete_all(:cfg_name => "testtool_release_directory")
   end

end
