class AddTestToolReleaseToTestRequest < ActiveRecord::Migration
  def change
    add_column :test_requests, :test_tool_release_id, :integer
  end
end
