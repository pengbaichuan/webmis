class AddDetaultTestToolUsedUbuntuRelease < ActiveRecord::Migration
 def self.up
    n = ConfigParameter.new(:cfg_name => "testtool_ubuntu_release", :cfg_value => 'x86_64_Ubuntu_14.04', :description => 'Default Test Tool Ubuntu release/platform')
    n.save
 end

 def self.down
      ConfigParameter.delete_all(:cfg_name => "testtool_ubuntu_release")
 end

end
