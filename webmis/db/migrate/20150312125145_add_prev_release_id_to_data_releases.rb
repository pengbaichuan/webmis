class AddPrevReleaseIdToDataReleases < ActiveRecord::Migration
  def up
    add_column :data_releases, :prev_data_release_id, :integer
    DataRelease.all.each do |dr|
      prev = DataRelease.where("data_releases.name < \"#{dr.name}\" and data_releases.name <> '.' and data_releases.company_abbr_id = #{dr.company_abbr_id}").order("id desc").last
      if prev && !dr.prev_data_release_id
        dr.prev_data_release_id = prev.id
        dr.save
      end
    end
  end

  def down
    remove_column :data_releases, :prev_data_release_id
  end 
end
