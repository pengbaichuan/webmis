class NilReceptionsRemovedYnToFalse < ActiveRecord::Migration
  def up
    Reception.where(:removed_yn => nil).each do |r|
      r.removed_yn = false
      r.save(:validate => false)
    end
  end

  def down
    puts "Migration unreversible"
  end
end
