class AddIgnoreDsdRegionsYnToRegion < ActiveRecord::Migration
  def change
    add_column :regions, :ignore_dsd_regions_yn, :boolean, :default => false
  end
end
