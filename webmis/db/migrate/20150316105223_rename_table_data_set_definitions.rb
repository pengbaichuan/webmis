class RenameTableDataSetDefinitions < ActiveRecord::Migration
  def up
    rename_table :data_set_definitions, :data_sets
    rename_table :data_set_definition_versions, :data_set_versions
    rename_column :data_set_versions, :data_set_definition_id, :data_set_id
  end

  def down
    rename_table :data_sets, :data_set_definitions
    rename_table :data_set_versions, :data_set_definition_versions
    rename_column :data_set_definition_versions, :data_set_id, :data_set_definition_id
  end
end
