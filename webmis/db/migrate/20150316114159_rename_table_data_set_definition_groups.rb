class RenameTableDataSetDefinitionGroups < ActiveRecord::Migration
  def up
    rename_table  :data_set_definition_groups, :data_set_groups
    rename_table  :data_set_definition_group_lines, :data_set_group_lines
    rename_column :data_set_group_lines, :data_set_definition_id, :data_set_id
    rename_column :data_set_group_lines, :data_set_definition_group_id, :data_set_group_id
  end

  def down
    rename_table  :data_set_groups, :data_set_definition_groups
    rename_table  :data_set_group_lines, :data_set_definition_group_lines
    rename_column :data_set_definition_group_lines, :data_set_id, :data_set_definition_id
    rename_column :data_set_definition_group_lines, :data_set_group_id, :data_set_definition_group_id
  end
end
