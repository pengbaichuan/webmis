class RenameDataSetDefinitionRelationColumns < ActiveRecord::Migration
  def up
    rename_column :receptions, :data_set_definition_id, :data_set_id
    rename_column :receptions, :data_set_definition_version, :data_set_version
    rename_column :product_data_set_infos, :data_set_definition_id, :data_set_id
    rename_column :product_data_set_infos, :data_set_definition_version, :data_set_version
    rename_column :process_data_items, :data_set_definition_id, :data_set_id
    rename_column :process_data_items, :data_set_definition_version, :data_set_version
  end

  def down
    rename_column :receptions, :data_set_id, :data_set_definition_id
    rename_column :receptions, :data_set_version, :data_set_definition_version
    rename_column :product_data_set_infos, :data_set_id, :data_set_definition_id
    rename_column :product_data_set_infos, :data_set_version, :data_set_definition_version
    rename_column :process_data_items, :data_set_id, :data_set_definition_id
    rename_column :process_data_items, :data_set_version, :data_set_definition_version
  end
end
