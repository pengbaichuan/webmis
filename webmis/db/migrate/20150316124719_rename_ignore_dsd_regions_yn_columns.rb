class RenameIgnoreDsdRegionsYnColumns < ActiveRecord::Migration
  def up
    rename_column :executables, :ignore_dsd_regions_yn, :ignore_ds_regions_yn
    rename_column :executable_versions, :ignore_dsd_regions_yn, :ignore_ds_regions_yn
    rename_column :regions, :ignore_dsd_regions_yn, :ignore_ds_regions_yn
  end

  def down
    rename_column :executables, :ignore_ds_regions_yn, :ignore_dsd_regions_yn
    rename_column :executable_versions, :ignore_ds_regions_yn, :ignore_dsd_regions_yn
    rename_column :regions, :ignore_ds_regions_yn, :ignore_dsd_regions_yn
  end
end
