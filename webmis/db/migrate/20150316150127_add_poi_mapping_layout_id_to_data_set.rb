class AddPoiMappingLayoutIdToDataSet < ActiveRecord::Migration
  def change
    add_column :data_sets, :picked_reception_ids, :string
    add_column :data_sets, :poi_mapping_layout_id, :integer
    add_column :data_set_versions, :picked_reception_ids, :string
    add_column :data_set_versions, :poi_mapping_layout_id, :integer
  end
end
