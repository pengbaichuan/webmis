class AddDataSetIdToProcessDataElement < ActiveRecord::Migration
  def change
    add_column :process_data_elements, :data_set_id, :integer
  end
end
