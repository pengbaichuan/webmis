class AddUpdatedByToDataSet < ActiveRecord::Migration
  def change
    add_column :data_sets, :updated_by, :string
    add_column :data_set_versions, :updated_by, :string
  end
end
