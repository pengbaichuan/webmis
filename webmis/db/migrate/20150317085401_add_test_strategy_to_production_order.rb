class AddTestStrategyToProductionOrder < ActiveRecord::Migration
  def change
    add_column :production_orders, :test_strategy, :string
  end
end
