class ChangeConversionDatabase < ActiveRecord::Migration
  def up
    add_column :conversion_databases, :file_system_status, :integer, :default => 10

    ConversionDatabase.reset_column_information

    # change status from STATUS_VALIDATION_CRASHED to STATUS_VALIDATING
    ConversionDatabase.where(:status => 30).each do |cd|
      cd.status = 20 # ConversionDatabase::STATUS_VALIDATING
      cd.save!
    end

    # set the status to REJECTED for those conversion databases with status_yn flag set to true
    ConversionDatabase.where(:crashed_yn => true).each do |cd|
      cd.status = 200 # ConversionDatabase::STATUS_REJECTED
      cd.save!
    end

    # set the status to ACCEPTED for those conversion databases with status VALIDATED
    ConversionDatabase.where(:status => 100).each do |cd|
      cd.status = 300 # the new ConversionDatabase::STATUS_ACCEPTED
      cd.save!
    end

    # renumber the CANCELLED status from 80 to 800
    ConversionDatabase.where(:status => 80).each do |cd|
      cd.status = 800 # the new ConversionDatabase::STATUS_CANCELLED
      cd.save!
    end

    # transfer the can_be_removed_yn flag to the file_system_status
    ConversionDatabase.where(:can_be_removed_yn => true).each do |cd|
      cd.file_system_status = 900 # ConversionDatabase::FILE_SYSTEM_STATUS_MARKED_FOR_REMOVAL
      cd.save!
    end

    # transfer the persistent_yn flag to the file_system_status
    ConversionDatabase.where(:persistent_yn => true).each do |cd|
      cd.file_system_status = 100 # ConversionDatabase::FILE_SYSTEM_STATUS_PERSISTENT
      cd.save!
    end

    # transfer the removed_yn flag to the file_system_status
    ConversionDatabase.where(:removed_yn => true).each do |cd|
      cd.file_system_status = 1000 # ConversionDatabase::FILE_SYSTEM_STATUS_REMOVED
      cd.save!
    end

    remove_column :conversion_databases, :crashed_yn
    remove_column :conversion_databases, :can_be_removed_yn
    remove_column :conversion_databases, :persistent_yn
    remove_column :conversion_databases, :removed_yn
  end

  def down
    add_column :conversion_databases, :crashed_yn, :boolean
    add_column :conversion_databases, :removed_yn, :boolean, :default => false
    add_column :conversion_databases, :can_be_removed_yn, :boolean, :default => false
    add_column :conversion_databases, :persistent_yn, :boolean, :default => false

    ConversionDatabase.reset_column_information

    # set the crashed_yn flag for those conversion databases with status CRASHED
    ConversionDatabase.where(:status => 700).each do |cd|
      cd.crashed_yn = true
      cd.save!
    end

    # renumber the CANCELLED status from 800 to 80
    ConversionDatabase.where(:status => 800).each do |cd|
      cd.status = 80
      cd.save!
    end

    # set the persistent_yn flag for those conversion databases with file_system_status PERSISTENT
    ConversionDatabase.where(:file_system_status => 100).each do |cd|
      cd.persistent_yn = true
      cd.save!
    end

    # set the can_ben_removed_yn flag for those conversion databases with file_system_status MARKED_FOR_REMOVAL
    ConversionDatabase.where(:file_system_status => 900).each do |cd|
      cd.can_be_removed_yn = true
      cd.save!
    end

    # set the can_ben_removed_yn and removed_yn flags for those conversion databases with file_system_status REMOVED
    ConversionDatabase.where(:file_system_status => 1000).each do |cd|
      cd.can_be_removed_yn = true
      cd.removed_yn = true
      cd.save!
    end

    remove_column :conversion_databases, :file_system_status
    # change status from STATUS_VALIDATED to STATUS_ACCEPTED cannot be undone
    # change status from STATUS_VALIDATION_CRASHED to STATUS_VALIDATING cannot be undone
  end
end
