class AddDataSetIdToProcessDataLine < ActiveRecord::Migration
  def change
    add_column :process_data_lines, :data_set_id, :integer
  end
end
