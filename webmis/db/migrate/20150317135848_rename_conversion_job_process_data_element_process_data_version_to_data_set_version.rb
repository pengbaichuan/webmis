class RenameConversionJobProcessDataElementProcessDataVersionToDataSetVersion < ActiveRecord::Migration
  def up
    rename_column :conversion_job_process_data_elements, :process_data_version, :data_set_version
  end

  def down
    rename_column :conversion_job_process_data_elements, :data_set_version, :process_data_version
  end
end
