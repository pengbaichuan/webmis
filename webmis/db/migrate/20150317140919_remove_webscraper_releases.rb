class RemoveWebscraperReleases < ActiveRecord::Migration
  def up
    DataRelease.where("name like 'Webscraper%'").destroy_all
  end

  def down
  end
end
