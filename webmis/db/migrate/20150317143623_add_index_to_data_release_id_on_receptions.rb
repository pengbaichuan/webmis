class AddIndexToDataReleaseIdOnReceptions < ActiveRecord::Migration
  def change
    add_index :receptions, :data_release_id
  end
end
