class RenameTableProcessDataStatuslogToDataSetStatusLog < ActiveRecord::Migration
  def up
    rename_table  :process_data_statuslogs, :data_set_status_logs
    add_column :data_set_status_logs, :data_set_id, :integer
  end

  def down
    rename_table  :data_set_status_logs, :process_data_statuslogs
    remove_column :process_data_statuslogs, :data_set_id
  end
end
