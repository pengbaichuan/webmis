class RemoveStatusDefaultsFromConversionDatabases < ActiveRecord::Migration
  def up
    change_column_default :conversion_databases, :status, nil
    change_column_default :conversion_databases, :file_system_status, nil
  end

  def down
    change_column_default :conversion_databases, :status, 1000
    change_column_default :conversion_databases, :file_system_status, 10
  end
end
