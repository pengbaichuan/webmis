class AddReservedPathToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :reserved_path, :string
  end
end
