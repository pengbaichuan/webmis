class MigrateProcessDataItemToDataSet < ActiveRecord::Migration
  def up
    item_ids = []
    DataSet.all.each do |ds|
      item_ids = []
      sql = "select id from process_data_items where data_set_id = #{ds.id} and removed_yn = 0"
      item_ids << ActiveRecord::Base.connection.execute(sql).to_a

      item_ids.flatten.uniq.each do |item|
        elements = ProcessDataElement.where(:process_data_item_id => item,:removed_yn => false)
        elements.each do |el|
          el.data_set_id = ds.id
          el.save
        end
      end
    end

   process_data_ids = []
   sql = "select id from process_data where status >= 10 and status < 90"
   process_data_ids << ActiveRecord::Base.connection.execute(sql).to_a

    item_ids = []
    process_data_ids.flatten.uniq.each do |pdi|
      item_ids = []
      sql_a = "select id from process_data_items where process_data_id = #{pdi} and data_set_id is null and removed_yn = 0"
      item_ids << ActiveRecord::Base.connection.execute(sql_a).to_a
      item_attr = []
      item_ids.flatten.uniq.each do |item_id|
        item_attr = []
        sql_b = "select supplier_name, datatype, data_release, filling, area_name, picked_reception_ids, poi_mapping_layout_id, updated_by, process_data_id from process_data_items where id = #{item_id} and removed_yn = 0"
        item_attr << ActiveRecord::Base.connection.execute(sql_b).to_a
        item_attr.uniq.each do |attr|
          DataSet.transaction do
            if attr[0]
              supplier_name = attr[0][0]
              data_type_name = attr[0][1]
              data_release_name = attr[0][2]
              filling_name = attr[0][3]
              region_name = attr[0][4]
              poi_mapping_layout_id = attr[0][6]
              updated_by = attr[0][7]
              process_data_id = attr[0][8]
              company_abbr = CompanyAbbr.find_by_company_name(supplier_name)
              data_type = DataType.find_by_name(data_type_name)
              data_release = DataRelease.find_by_name(data_release_name)
              filling = Filling.find_by_name(filling_name)
              filling_id = filling.id if filling
              region = Region.find_by_name(region_name)
              if company_abbr && data_type && data_release && region
                ds = DataSet.new({:name => "#{supplier_name} #{data_release_name} #{region.region_code} #{data_type_name} #{filling_name}",
                    :company_abbr_id => company_abbr.id, :data_release_id => data_release.id,
                    :region_id => region.id, :data_type_id => data_type.id, :filling_id => filling_id,
                    :remarks => "migrated from process_data: #{process_data_id} ,item:#{item_id}",:picked_reception_ids => attr[0][5],
                    :status => DataSet::STATUS_DRAFT, :poi_mapping_layout_id => poi_mapping_layout_id, :updated_by => updated_by})
                if ds.save
                  elements = ProcessDataElement.active.where(:process_data_item_id => item_id)
                  elements.each do |el|
                    el.data_set_id = ds.id
                    el.directory = el.generate_directory
                    el.save!
                  end
                end
              end
            end
          end
        end
      end
    end
  end

  def down
    puts "unreversible"
  end
end
