class ChangeJobStatus < ActiveRecord::Migration
  def up
    add_column :jobs, :file_system_status, :integer, :default => 10
    add_column :job_versions, :file_system_status, :integer, :default => 10

    Job.reset_column_information

    # change move_status == RESERVED to FILE_SYSTEM_STATUS_RESERVED
    # don't care about the move_status information in the versions table
    Job.where(:move_status => 'RESERVED').each do |j|
      j.file_system_status = 500
      j.save!
    end

    # change move_status == MOVING to FILE_SYSTEM_STATUS_MOVING
    Job.where(:move_status => 'MOVING').each do |j|
      j.file_system_status = 600
      j.save!
    end

    # change status QUEUE_MANUAL to QUEUED
    Job.where(:status => 30).each do |j|
      j.status = 20
      j.save!
    end

    # change status CRASHED_ON_SERVER to FAILED
    Job.where(:status => 60).each do |j|
      j.status = 180
      j.file_system_status = 100 if j.file_system_status = 10
      j.save!
    end

    # change status FAILED_ON_SERVER to FAILED
    Job.where(:status => 70).each do |j|
      j.status = 180
      j.file_system_status = 100 if j.file_system_status = 10
      j.save!
    end

    # change status FINISHED_ON_SERVER to COMPLETE
    Job.where(:status => 80).each do |j|
      j.status = 200
      j.file_system_status = 100 if j.file_system_status = 10
      j.save!
    end

    remove_column :jobs, :move_status
    remove_column :job_versions, :move_status
  end

  def down
    add_column :jobs, :move_status, :string
    add_column :job_versions, :move_status, :string

    Job.reset_column_information

    # change FILE_SYSTEM_STATUS_RESERVED to move_status RESERVED
    Job.where(:file_system_status => 500).each do |j|
      j.move_status = 'RESERVED'
      j.save!
    end

    # change FILE_SYSTEM_STATUS_MOVING to move_status_MOVING
    Job.where(:file_system_status => 600).each do |j|
      j.move_status = 'MOVING'
      j.save!
    end

    # change status QUEUED to QUEUE_MANUAL for QUEUED jobs with a run_server
    Job.where(:status => 20).each do |j|
      unless j.run_server.blank?
        j.status = 30
        j.save!
      end
    end

    # change COMPLETE with FILE_SYSTEM_STATUS_ON_SERVER to FINISHED_ON_SERVER
    Job.where(:status => 200, :file_system_status => 100).each do |j|
      j.status = 80
      j.save!
    end

    # change FAILED with FILE_SYSTEM_STATUS_ON_SERVER to FAILED_ON_SERVER (can't rollback the difference between CRASHED_ON_SERVER and FAILED_ON_SERVER)
    Job.where(:status => 180, :file_system_status => 100).each do |j|
      j.status = 70
      j.save!
    end
    
    remove_column :jobs, :file_system_status
    remove_column :job_versions, :file_system_status
  end
end
