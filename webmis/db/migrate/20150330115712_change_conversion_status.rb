class ChangeConversionStatus < ActiveRecord::Migration
  def up

    # set conversions older than 2 year on a end-status so their validation
    # will always succeed.
    Conversion.where("id < 70000 and status < 20").each do |conv|
      conv.status = 30
      conv.save!
    end

    Conversion.where("outcome = 'Crashed' OR outcome = 'Unusable' OR outcome = 'Not OK'").each do |conv|
      if conv.remarks.blank?
        conv_remarks = ""
      else
        conv.remarks += "\r\n"
      end
      if conv.status == 20
        conv.status = 30
        conv.remarks +="New WebMIS Release: Outcome '#{conv.outcome}' removed. Status changed from 'finished' to 'Cancelled'."
      else
        conv.remarks += "New WebMIS Release: Outcome '#{conv.outcome}' removed."
      end
      begin
        conv.save!
      rescue => e
        raise "conv_id: #{conv.id} " + e.message
      end
    end

    Conversion.where(:outcome => ['No result', 'Correct', 'Partly compiled']).each do |conv|
      if conv.remarks.blank?
        conv.remarks = ""
      else
        conv.remarks += "\r\n"
      end
      conv.remarks += "New WebMIS Release: Outcome '#{conv.outcome} removed."
      # don't mind about the validation in this case as we are just adding a comment
      conv.save(:validate => false)
    end

    Conversion.where(:status => [12, 14, 17, 18, 19]).each do |conv|
      case conv.status
        when 12
          status_str = 'Partly Manual'
        when 14
          status_str = 'Partly failed'
        when 17
          status_str = 'All Failed'
        when 18
          status_str = 'Validation'
        when 19
          status_str = 'Validation Errors'
      end
      new_status_str = conv.tpd ? 'Requested TPD' : 'Requested'
      conv.status = conv.tpd ? 15 : 10
      if conv.remarks.blank?
        conv.remarks = ""
      else
        conv.remarks = "\r\n"
      end
      conv.remarks += "New WebMIS Release: Status changed from #{status_str} to #{new_status_str}."
      begin
        conv.save!
      rescue => e
        raise "conv_id: #{conv.id} " + e.message
      end
    end

    Conversion.where(:status => [800, 900, 999]).each do |conv|
      case conv.status
        when 800
        status_str = 'Outcome partly removed'
        when 900
        status_str = 'Outcome removed'
        when 999
        status_str = 'Removed'
      end
      conv.status = 30
      if conv.remarks.blank?
        conv.remarks = ""
      else
        conv.remarks += "\r\n"
      end
      conv.remarks += "New WebMIS Release: Status changed from #{status_str} to Cancelled."
      begin
        conv.save!
      rescue => e
        raise "conv_id: #{conv.id} " + e.message
      end
    end

    remove_column :conversions, :result
    # # just to te sure keep outcome in for now as we can't undo a removal
    # # the field is not used though so can be safely removed if no one missed it
    # remove_column :conversions, :outcome
  end

  def down
    add_column :conversions, :result, :string
    # add_column :conversions, :outcome, :string

    # cannot revert to the old result and outcome values or to the old statuses
  end
end
