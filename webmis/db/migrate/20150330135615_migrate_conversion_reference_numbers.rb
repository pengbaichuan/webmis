class MigrateConversionReferenceNumbers < ActiveRecord::Migration
  def up
    cvs = Conversion.where("reference_numbers like '%process_data%'")
    tomigrate = cvs.map{|x|{x.id => x.reference_numbers.split("\r\n").map{|x|x.scan(/process_data-id-([^ ]*)/)}}}.flatten.uniq.map{|x|{x.keys[0] => x.values.flatten.uniq}}

    tomigrate.each do |cid|
      c = Conversion.find(cid.keys[0])
      cid.values.each do |pdid|
        pdid = pdid[0]
        filepaths = DataSet.connection.execute("select process_data_id,process_data_elements.directory,process_data_elements.data_set_id from process_data,process_data_items,process_data_elements where process_data_elements.process_data_item_id = process_data_items.id and process_data_items.process_data_id = process_data.id and process_data.id = #{pdid.to_i}").map{|x|{x[1] => {:pid => x[0], :dsid => x[2]}}}
        filepaths.each do |path|
          if path.keys[0] && c.data_files.match(path.keys[0]) 
            if path[:dsid]
              c.reference_number.gsub!("process_data-id-#{path[:pid]}","data_set-id-#{path[:dsid]}")
            else
              puts "No data set id found for #{path[:pid]} , filepath #{path}"
            end
          end
        end
        #c.save!

      end
    end

  end

  def down
    "mission impossible"
  end
end
