class ChangeProductionOrderlineStatus < ActiveRecord::Migration
  def up
    rename_column :production_orderlines, :bp_status, :bp_status_old
    add_column :production_orderlines, :bp_status, :integer

    ProductionOrderline.reset_column_information

    ProductionOrderline.all.each do |pol|
      case pol.bp_status_old
      when nil, 'ACTIVE', 'RESET'
        pol.bp_status = 100
      when 'PROCESSING', 'WAIT'
        pol.bp_status = 200
      when 'FAILED'
        pol.bp_status = 300
      when 'COMPLETE'
        pol.bp_status = 400
      end
      pol.save!
    end

    remove_column :production_orderlines, :linestatus
    remove_column :production_orderlines, :bp_status_old
  end

  def down
    rename column :production_orderlines, :bp_status, :bp_status_old
    add_column :production_orderlines, :linestatus, :string, :default => 'NEW'
    add_column :production_orderlines, :bp_status, :string

    ProductionOrderline.reset_column_information

    ProductionOrderline.all.each do |pol|
      case po.bp_status_old
      when 100
        pol.bp_status = 'ACTIVE'
      when 200
        pol.bp_status = 'PROCESSING'
      when 300
        pol.bp_status = 'FAILED'
      when 400
        pol.bp_status = 'COMPLETE'
      end
      pol.save!
    end

    remove_column :production_orderlines, :bp_status_old
  end
end
