class ChangeProductionOrderStatus < ActiveRecord::Migration
  def up
    ProductionOrder.all.each do |po|
      po.bp_name = po.status
      # fix validation
      po.allow_shipment = false if po.allow_shipment && po.distribution_list_id.nil?
      po.save!
    end

    remove_column :production_orders, :status
  end

  def down
    add_column :production_orders, :status, :string

    ProductionOrder.reset_column_information

    ProductionOrder.all.each do |po|
      po.status = po.bp_name
      po.bp_name = 'ORDERENTRY'
      po.save!
    end
  end
end
