class AddReviewAttributesToDataSet < ActiveRecord::Migration
  def change
    add_column :data_sets, :review_request_user_id, :integer
    add_column :data_sets, :review_user_id, :integer
    add_column :data_sets, :review_result, :text
  end
end
