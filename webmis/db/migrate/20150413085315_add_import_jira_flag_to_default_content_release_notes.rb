class AddImportJiraFlagToDefaultContentReleaseNotes < ActiveRecord::Migration
  def change
    add_column :default_content_release_notes, :allow_jira_import_yn, :boolean
    add_column :content_release_notes, :allow_jira_import_yn, :boolean
  end
end
