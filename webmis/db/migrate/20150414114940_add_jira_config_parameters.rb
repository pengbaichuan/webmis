class AddJiraConfigParameters < ActiveRecord::Migration
  def up
    ConfigParameter.new(:cfg_name => "jira_url", :cfg_value => 'https://jira.mapscape.nl', :description => 'Jira URL').save!
    ConfigParameter.new(:cfg_name => "jira_login", :cfg_value => 'BugSyncBMW', :description => 'Jira Login').save!
    ConfigParameter.new(:cfg_name => "jira_password", :cfg_value => 'Mapscape2014', :description => 'Jira password').save!
  end

  def down
    ConfigParameter.delete_all(:cfg_name => "jira_password")
    ConfigParameter.delete_all(:cfg_name => "jira_login")
    ConfigParameter.delete_all(:cfg_name => "jira_url")
  end
end
