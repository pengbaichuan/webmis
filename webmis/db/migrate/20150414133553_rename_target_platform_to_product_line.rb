class RenameTargetPlatformToProductLine < ActiveRecord::Migration
  def up
    rename_table :target_platforms, :product_lines
  end

  def down
    rename_table :product_lines, :target_platforms
  end
end
