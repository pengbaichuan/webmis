class RenameTargetPlatformForeignKeys < ActiveRecord::Migration
  def up
    rename_column :customer_content_definitions, :target_platform_id, :product_line_id
    rename_column :parameter_setting_versions, :target_platform_id, :product_line_id
    rename_column :parameter_settings, :target_platform_id, :product_line_id
    rename_column :product_feature_versions, :target_platform_id, :product_line_id
    rename_column :product_features, :target_platform_id, :product_line_id
    rename_column :product_versions, :target_platform_id, :product_line_id
    rename_column :products, :target_platform_id, :product_line_id
    rename_column :target_platform_users, :target_platform_id, :product_line_id
    rename_column :test_area_configuration_versions, :target_platform_id, :product_line_id
    rename_column :test_area_configurations, :target_platform_id, :product_line_id
    rename_column :test_plan_versions, :target_platform_id, :product_line_id
    rename_column :test_plans, :target_platform_id, :product_line_id
  end

  def down
    rename_column :customer_content_definitions, :product_line_id, :target_platform_id
    rename_column :parameter_setting_versions, :product_line_id, :target_platform_id
    rename_column :parameter_settings, :product_line_id, :target_platform_id
    rename_column :product_feature_versions, :product_line_id, :target_platform_id
    rename_column :product_features, :product_line_id, :target_platform_id
    rename_column :product_versions, :product_line_id, :target_platform_id
    rename_column :products, :product_line_id, :target_platform_id
    rename_column :target_platform_users, :product_line_id, :target_platform_id
    rename_column :test_area_configuration_versions, :product_line_id, :target_platform_id
    rename_column :test_area_configurations, :product_line_id, :target_platform_id
    rename_column :test_plan_versions, :product_line_id, :target_platform_id
    rename_column :test_plans, :product_line_id, :target_platform_id
  end
end
