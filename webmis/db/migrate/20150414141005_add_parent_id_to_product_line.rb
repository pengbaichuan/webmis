class AddParentIdToProductLine < ActiveRecord::Migration
  def change
    add_column :product_lines, :parent_id, :integer
  end
end
