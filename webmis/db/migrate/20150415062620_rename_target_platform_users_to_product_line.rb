class RenameTargetPlatformUsersToProductLine < ActiveRecord::Migration
  def up
    rename_table :target_platform_users, :product_line_users
  end

  def down
    rename_table :product_line_users, :target_platform_users
  end
end
