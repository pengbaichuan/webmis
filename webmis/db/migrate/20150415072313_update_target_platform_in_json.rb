class UpdateTargetPlatformInJson < ActiveRecord::Migration
  def up
    execute "update product_designs set model = replace(model, 'target_platform', 'product_line')"
    execute "update production_orderlines set product_design = replace(product_design, 'target_platform', 'product_line')"
  end

  def down
    execute "update product_designs set model = replace(model, 'product_line', 'target_platform')"
    execute "update production_orderlines set product_design = replace(product_design, 'product_line', 'target_platform')"
  end
end
