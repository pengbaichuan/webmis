class CreateNewCompanies < ActiveRecord::Migration
  def change
    create_table :companies, :force => true do |t|
      t.string :name
      t.string :shipping_address_postalcode
      t.string :shipping_address_street
      t.string :shipping_address_city
      t.string :shipping_address_state
      t.string :shipping_address_country
      t.string :billing_address_postalcode
      t.string :billing_address_street
      t.string :billing_address_city
      t.string :billing_address_state
      t.string :billing_address_country
      t.string :phone_office
      t.string :phone_fax
      t.integer :status
      t.string :external_id
    end
  end
end
