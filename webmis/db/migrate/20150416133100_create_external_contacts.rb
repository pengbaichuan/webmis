class CreateExternalContacts < ActiveRecord::Migration
  def change
    create_table :external_contacts do |t|
      t.string :salutation
      t.string :title
      t.string :first_name
      t.string :last_name
      t.string :primary_address_street
      t.string :primary_address_postalcode
      t.string :primary_address_city
      t.string :primary_address_state
      t.string :primary_address_country
      t.string :phone_work
      t.string :phone_mobile
      t.string :email
      t.integer :status
      t.integer :company_id
      t.string :external_id
    end
  end
end
