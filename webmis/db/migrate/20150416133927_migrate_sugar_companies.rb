class MigrateSugarCompanies < ActiveRecord::Migration
  def up
    sugar_ids = []
    sugar_company_ids = []
    sugar_ids += PurchaseOrder.all.collect{|po|po.external_contact_id}.uniq.compact
    sugar_ids += ShippingSpecification.all.collect{|ss|ss.external_contact_id}.uniq.compact
    sugar_ids += OutboundOrder.all.collect{|oo|oo.contact_id}.uniq.compact
    sugar_ids += ExternalRole.all.collect{|ss|ss.external_contact_id}.uniq.compact
    sugar_ids += DistributionListMember.all.collect{|ss|ss.external_contact_id}.uniq.compact
    sugar_ids += ArticleLicense.all.collect{|ss|ss.external_contact_id}.uniq.compact

    sugar_ids.uniq.each do |sid|
      ct = ExternalContactSugar.find_all_by_id([sid]) if !sid.blank?
      sugar_company_ids << ct[0].company_sugar.id if !sid.blank? && ct.size > 0
    end

    Company.transaction do
      name_arr = []
      sugar_company_ids.uniq.compact.each do |sci|
        scomp = CompanySugar.find(sci)
        ncomp = Company.new
        # ensure unique names      
        if name_arr.include?(scomp.name)
          names = name_arr.uniq.inject({}) {|accu, uni| accu.merge({ uni => name_arr.select{|i| i == uni } })}
          ncomp.name = "#{scomp.name} #{names[scomp.name].size}"
        else
          ncomp.name = scomp.name
        end
        name_arr << scomp.name
        ncomp.shipping_address_postalcode = scomp.shipping_address_postalcode
        ncomp.shipping_address_street = scomp.shipping_address_street
        ncomp.shipping_address_city = scomp.shipping_address_city
        ncomp.shipping_address_state = scomp.shipping_address_state
        ncomp.shipping_address_country = scomp.shipping_address_country
        ncomp.billing_address_postalcode = scomp.billing_address_postalcode
        ncomp.billing_address_street = scomp.billing_address_street
        ncomp.billing_address_city = scomp.billing_address_city
        ncomp.billing_address_state = scomp.billing_address_state
        ncomp.billing_address_country = scomp.billing_address_country
        ncomp.phone_office = scomp.phone_office
        ncomp.phone_fax = scomp.phone_fax
        ncomp.status = Company::STATUS_ACTIVE
        ncomp.external_id = sci
        ncomp.inspect
        ncomp.save!
      end
    end


  end

  def down
    # empty table
    Company.destroy_all
  end
end
