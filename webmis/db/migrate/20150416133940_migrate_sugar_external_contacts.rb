class MigrateSugarExternalContacts < ActiveRecord::Migration
  def up
    sugar_ids = []
    sugar_ids += PurchaseOrder.all.collect{|po|po.external_contact_id}.uniq.compact
    sugar_ids += ShippingSpecification.all.collect{|ss|ss.external_contact_id}.uniq.compact
    sugar_ids += OutboundOrder.all.collect{|oo|oo.contact_id}.uniq.compact
    sugar_ids += ExternalRole.all.collect{|ss|ss.external_contact_id}.uniq.compact
    sugar_ids += DistributionListMember.all.collect{|ss|ss.external_contact_id}.uniq.compact
    sugar_ids += ArticleLicense.all.collect{|ss|ss.external_contact_id}.uniq.compact

    sugar_ids.uniq.each do |sid|
      ct = ExternalContactSugar.find_all_by_id([sid])
      if ct[0]
        ct = ct[0]
        ExternalContact.transaction do
          ec = ExternalContact.new
          ec.salutation = ct.salutation
          ec.title = ct.title
          ec.first_name = ct.first_name
          ec.last_name = ct.last_name
          ec.primary_address_street = ct.primary_address_street
          ec.primary_address_postalcode = ct.primary_address_postalcode
          ec.primary_address_city = ct.primary_address_city
          ec.primary_address_state = ct.primary_address_state
          ec.primary_address_country = ct.primary_address_country
          ec.phone_work = ct.phone_work
          ec.phone_mobile = ct.phone_mobile
          ec.email = ct.email
          ec.external_id = sid
          ec.status = ExternalContact::STATUS_ACTIVE
          sugar_comp = ct.company_sugar
          company = Company.find_by_external_id(sugar_comp.id)
          ec.company_id = company.id if company
          ec.inspect
          ec.save!
          PurchaseOrder.where(:external_contact_id => ec.external_id).each do |po|
            po.external_contact_id = ec.id
            po.save!
          end
          ShippingSpecification.where(:external_contact_id => ec.external_id).each do |ss|
            ss.external_contact_id = ec.id
            ss.save!
          end
          OutboundOrder.where(:contact_id => ec.external_id).each do |oo|
            oo.contact_id = ec.id
            oo.save!
          end
          ExternalRole.where(:external_contact_id => ec.external_id).each do |er|
            er.external_contact_id = ec.id
            er.save!
          end
          DistributionListMember.where(:external_contact_id => ec.external_id).each do |dm|
            dm.external_contact_id = ec.id
            dm.save!
          end
          ArticleLicense.where(:external_contact_id => ec.external_id).each do |al|
            al.external_contact_id = ec.id
            al.save!
          end
        end
      end
    end
  end

  def down
    ExternalContact.transaction do
      ExternalContact.all.each do |ec|
        PurchaseOrder.where(:external_contact_id => ec.id).each do |po|
          po.external_contact_id = ec.external_id
          po.save!
        end
        ShippingSpecification.where(:external_contact_id => ec.id).each do |ss|
          ss.external_contact_id = ec.external_id
          ss.save!
        end
        OutboundOrder.where(:contact_id => ec.id).each do |oo|
          oo.contact_id = ec.external_id
          oo.save!
        end
        ExternalRole.where(:external_contact_id => ec.id).each do |er|
          er.external_contact_id = ec.external_id
          er.save!
        end
        DistributionListMember.where(:external_contact_id => ec.id).each do |dm|
          dm.external_contact_id = ec.external_id
          dm.save!
        end
        ArticleLicense.where(:external_contact_id => ec.id).each do |al|
          al.external_contact_id = ec.external_id
          al.save!
        end
        ec.destroy
      end
    end
  end
end
