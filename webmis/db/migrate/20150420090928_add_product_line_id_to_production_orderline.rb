class AddProductLineIdToProductionOrderline < ActiveRecord::Migration
  def change
    add_column :production_orderlines, :product_line_id, :integer
  end
end
