class MigrateProductLineIdOnProductionOrderline < ActiveRecord::Migration
  def up
    ProductionOrderline.all.each do |line|
      p = line.product
      if p 
        line.product_line_id = p.product_line_id
        line.save
      end
    end
  end

  def down
    ProductionOrderline.all.each do |line|
      p = line.product
      if p 
        line.product_line_id = nil
        line.save
      end
    end
  end
end
