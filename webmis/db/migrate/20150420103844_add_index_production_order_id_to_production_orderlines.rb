class AddIndexProductionOrderIdToProductionOrderlines < ActiveRecord::Migration
  def change
    add_index :production_orderlines, :production_order_id
  end
end
