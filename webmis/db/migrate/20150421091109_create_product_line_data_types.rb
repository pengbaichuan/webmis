class CreateProductLineDataTypes < ActiveRecord::Migration
  def change
    create_table :product_line_allowed_data do |t|
      t.integer :product_line_id
      t.integer :company_abbr_id
      t.integer :data_type_id
      t.integer :region_id
      t.integer :filling_id
      t.timestamps
    end
  end
end
