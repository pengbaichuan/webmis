class UpdateTargetPlatfromInJsonForVersions < ActiveRecord::Migration
  def up
    execute "update product_design_versions set model = replace(model, 'target_platform', 'product_line')"
  end

  def down
    execute "update product_design_versions set model = replace(model, 'product_line', 'target_platform')"
  end
end
