class AddProjectColorProfileToConfigParamaters < ActiveRecord::Migration
  def up
    ConfigParameter.transaction do
      n = ConfigParameter.new(:cfg_name => "project_color_profile", :cfg_value => Rails.env,
        :description => 'Color profile for top menu-bar. Possible values: gray, blue, darkblue, green, lime, orange, pink, purple, red, skyblue, smoke, yellow')
      n.save!
      Project.all.each do |project|
        dfid = Project.default.id
        if project.id != dfid
          proj_cfg_par = ConfigParameter.where(:cfg_name=>"project_color_profile",:project_id=>Project.default.id).last.dup
          proj_cfg_par.cfg_value = 'blue'
          proj_cfg_par.project_id = project.id
          proj_cfg_par.save!
        end
      end
    end
  end

  def down
    ConfigParameter.delete_all(:cfg_name => "project_color_profile")
  end
end
