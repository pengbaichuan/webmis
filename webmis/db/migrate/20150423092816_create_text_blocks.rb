class CreateTextBlocks < ActiveRecord::Migration
  def change
    create_table :text_blocks do |t|
      t.string  :name
      t.text    :content
      t.integer :status
      t.timestamps
    end
  end
end
