class AddPaymentStatementTextBlockIdToCustomerDepots < ActiveRecord::Migration
  def change
    add_column :customer_depots, :payment_statement_text_block_id, :integer
  end
end
