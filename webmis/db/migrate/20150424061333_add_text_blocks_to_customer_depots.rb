class AddTextBlocksToCustomerDepots < ActiveRecord::Migration
  def up
    TextBlock.transaction do
      german_name = "Payment statement with German VAT"
      common_name = "Payment statement"

      german_rec = TextBlock.find_or_create_by_name(german_name)
      common_rec = TextBlock.find_or_create_by_name(common_name)
      german_rec.content = "ABN-AMRO 24.47.49.191\r\nIBAN: NL39 ABNA 0244 7491 91\r\nBIC: ABNANL2A\r\nTAX-Number 116/5941/4081\r\nVAT-Number DE 281837549"
      common_rec.content = "ABN-AMRO 24.47.49.191\r\nIBAN: NL39 ABNA 0244 7491 91\r\nBIC: ABNANL2A"
      german_rec.save!
      common_rec.save!

      CustomerDepot.invoicers.each do |depot|
        if depot.country.to_s.downcase == 'germany'
          depot.payment_statement_text_block_id = german_rec.id
        else
          depot.payment_statement_text_block_id = common_rec.id
        end
        depot.save!
      end
    end
  end
  
  def down
    CustomerDepot.transaction do
      CustomerDepot.invoicers.each do |depot|
        depot.payment_statement_text_block_id = nil
        depot.save!
      end
    end
  end
end
