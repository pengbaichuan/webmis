class UpdateAssemblyViewForDataset < ActiveRecord::Migration
def up
    sql="create or REPLACE VIEW view_receptions_and_conversiondbs AS
         SELECT 'reception' AS 'source',
            receptions.id,
            receptions.remarks,
            receptions.storage_location,
            receptions.id as referenceid,
            receptions.data_release_id,
            data_types.name AS 'datatype',
            receptions.region_name,
            receptions.area_id,
            receptions.validation_requested,
            receptions.validated_yn,
            receptions.id as uniqid
        from receptions,data_types where removed_yn = 0 and receptions.data_type_id = data_types.id
        UNION ALL SELECT 'conversion',
            conversion_databases.id,
            conversion_databases.remarks,
            group_concat(conversion_databases.path_and_file separator '\n'),
            conversion_databases.conversion_id as referenceid,
            conversions.data_release_id,
            conversion_databases.db_type,
            conversions.region_name,
            conversions.region_id,
            null,
            null,
            conversion_databases.conversion_id as uniqid
        from conversion_databases,conversions where conversion_databases.file_system_status < 900 and
        conversion_databases.db_type != 'CAR'  and conversion_databases.conversion_id = conversions.id
        group by conversion_databases.conversion_id
        UNION ALL 
        select 'pdata', 
                data_sets.id,
                data_sets.name as ds_name,
                group_concat(process_data_elements.directory separator '\n'),
                data_sets.id as referenceid,
                0,
                data_types.name as dt_name,
                regions.name,
                regions.id as region_id,
                null,
                null,
                data_sets.id as uniqid
        from data_sets,process_data_elements, regions,data_types
        where data_sets.id = process_data_elements.data_set_id
        and regions.id = data_sets.region_id
        and data_sets.data_type_id = data_types.id
        group by data_sets.id;"

      ActiveRecord::Base.connection.execute(sql)


  end

  def down
    sql = "create or REPLACE VIEW view_receptions_and_conversiondbs AS
        SELECT 'reception' AS 'source',
            receptions.id,
            receptions.remarks,
            receptions.storage_location,
            receptions.id as referenceid,
            receptions.data_release_id,
            data_types.name AS 'datatype',
            receptions.region_name,
            receptions.area_id,
            receptions.validation_requested,
            receptions.validated_yn,
            receptions.id as uniqid
        from receptions,data_types where removed_yn = 0 and receptions.data_type_id = data_types.id
        UNION ALL SELECT 'conversion',
            conversion_databases.id,
            conversion_databases.remarks,
            group_concat(conversion_databases.path_and_file separator '\n'),
            conversion_databases.conversion_id as referenceid,
            conversions.data_release_id,
            conversion_databases.db_type,
            conversions.region_name,
            conversions.region_id,
            null,
            null,
            conversion_databases.conversion_id as uniqid
        from conversion_databases,conversions where conversion_databases.file_system_status < #{ConversionDatabase::FILE_SYSTEM_STATUS_MARKED_FOR_REMOVAL} and
        conversion_databases.db_type != 'CAR'  and conversion_databases.conversion_id = conversions.id
        group by conversion_databases.conversion_id
        UNION ALL 
        select 'pdata', 
                process_data_items.id,
                process_data.description,
                group_concat(CONCAT(process_data.storage_location, process_data_elements.directory) separator '\n'),
                process_data.id as referenceid,
                0,
                process_data_items.datatype,
                process_data_items.area_name,
                regions.id as region_id,
                null,
                null,
                process_data_items.id as uniqid
        from process_data,process_data_items,process_data_elements, regions
        where process_data.id = process_data_items.process_data_id and 
        process_data_items.id = process_data_elements.process_data_item_id
        and regions.name = process_data_items.area_name
        group by process_data_items.id;"

      ActiveRecord::Base.connection.execute(sql)
  end

end
