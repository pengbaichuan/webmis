class AddOrgBinaryDataToPoiMappingLayout < ActiveRecord::Migration
  def change
    add_column :poi_mapping_layouts, :org_binary_data, :binary
  end
end
