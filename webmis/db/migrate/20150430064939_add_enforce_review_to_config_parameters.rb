class AddEnforceReviewToConfigParameters < ActiveRecord::Migration
  def up
    ConfigParameter.transaction do
      n = ConfigParameter.new(:cfg_name => "enforce_review_procedure", :cfg_value => "true", :cfg_type => "boolean",
        :description => 'Boolean ("true" or "false") to enforce review procedure for modules which requires review.')
      n.save!
      p_config = ConfigParameter.where(:cfg_name=>"enforce_review_procedure",:project_id=>Project.default.id).last
      Project.all.each do |project|
        dfid = Project.default.id
        if project.id != dfid
          proj_cfg_par = p_config.dup
          proj_cfg_par.cfg_value = 'false'
          proj_cfg_par.project_id = project.id
          proj_cfg_par.save!
        end
      end
    end
  end

  def down
    ConfigParameter.delete_all(:cfg_name => "enforce_review_procedure")
  end
end
