class AddIndexToConversionDatabasesFileSystemStatus < ActiveRecord::Migration
  def change
    add_index :conversion_databases, [:file_system_status, :conversion_id, :db_type], :name => "fs_status"
  end
end
