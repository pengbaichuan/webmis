class AddIndexToVolumeidOfProducts < ActiveRecord::Migration
  def change
    add_index :products, :volumeid
  end
end
