class AddSendConversionRequestToToBusinessProcesses < ActiveRecord::Migration
  def up
    bp = BusinessProcess.find_or_create_by_name("SEND CONVERSION REQUEST")
    bp.description = "Send conversion request e-mail and update conversion status and related production orderline."
    bp.core_bp_yn = false
    bp.line_link_controller = "conversions"
    bp.line_link_action = "mail"
    bp.save
  end

  def down
    BusinessProcess.delete_all(:name => "SEND CONVERSION REQUEST")
  end
end
