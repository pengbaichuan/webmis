class CreateCommitmentChangeReasons < ActiveRecord::Migration
  def up
    create_table :commitment_change_reasons do |t|
      t.string :reason
      t.text :description
      t.string :commitment_type

      t.timestamps
    end

    CommitmentChangeReason.reset_column_information

    ccr = CommitmentChangeReason.new(:commitment_type => ProductionOrder::COMMITMENT_BASELINE, :reason => 'C00 Initial Value', :description => 'The one should only be used for the initial Baseline Commitment.')
    ccr.save!
    ccr = CommitmentChangeReason.new(:commitment_type => ProductionOrder::COMMITMENT_MODIFIED, :reason => 'C00 Initial Value', :description => 'The one should only be used for the initial Modified Commitment.')
    ccr.save!
  end

  def down
    drop_table :commitment_change_reasons
  end
end
