class ChangeCommitmentDatesProductionOrders < ActiveRecord::Migration
  def up
    rename_column :production_orders, :request_date, :baseline_commitment_date
    rename_column :production_orders, :committed_del_date, :modified_commitment_date
    remove_column :production_orders, :planned_shipdate
  end

  def down
    rename_column :production_orders, :baseline_commitment_date, :request_date
    rename_column :production_orders, :modified_commitment_date, :committed_del_date
    add_column :production_orders, :planned_shipdate, :datetime

    sql = "UPDATE production_orders SET planned_shipdate = committed_del_date"
    ActiveRecord::Base.connection.execute(sql)
  end
end


