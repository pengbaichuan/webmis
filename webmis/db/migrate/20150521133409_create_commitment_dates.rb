class CreateCommitmentDates < ActiveRecord::Migration
  def up

   create_table :commitment_dates do |t|
      t.integer :production_order_id
      t.string :commitment_type
      t.integer :commitment_change_reason_id
      t.datetime :new_date
      t.string :updated_by

      t.timestamps
    end

    CommitmentDate.reset_column_information

    baseline_type = ProductionOrder::COMMITMENT_BASELINE
    modified_type = ProductionOrder::COMMITMENT_MODIFIED

    baseline_reason = CommitmentChangeReason.where(:commitment_type => baseline_type).first
    modified_reason = CommitmentChangeReason.where(:commitment_type => modified_type).first

    ProductionOrder.all.each do |po|
      if po.baseline_commitment_date
        cd = CommitmentDate.new(:production_order_id => po.id, :commitment_type => baseline_type, :commitment_change_reason_id => baseline_reason.id, :new_date => po.baseline_commitment_date, :updated_by => 'rails deployment')
        cd.save!
      end
      if po.modified_commitment_date
        cd = CommitmentDate.new(:production_order_id => po.id, :commitment_type => modified_type, :commitment_change_reason_id => modified_reason.id, :new_date => po.modified_commitment_date, :updated_by => 'rails deployment')
        cd.save!
      end
    end
  end

  def down
    drop_table :commitment_dates
  end
end
 
