class ChangeResultDirSizeBytesToJobs < ActiveRecord::Migration
  def change
    change_column(:jobs, :result_dir_size_bytes, :integer, limit: 5)
    change_column(:job_versions, :result_dir_size_bytes, :integer, limit: 5)
  end
end
