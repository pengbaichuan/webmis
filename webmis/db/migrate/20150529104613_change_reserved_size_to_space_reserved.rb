class ChangeReservedSizeToSpaceReserved < ActiveRecord::Migration
  def change
    change_column(:space_reserved, :total_size,    :integer, limit: 5)
    change_column(:space_reserved, :buffer_size,   :integer, limit: 5)
    change_column(:space_reserved, :reserved_size, :integer, limit: 5)
  end
end
