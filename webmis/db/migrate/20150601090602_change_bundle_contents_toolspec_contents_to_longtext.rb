class ChangeBundleContentsToolspecContentsToLongtext < ActiveRecord::Migration
  def up
    execute 'alter table bundle_contents modify toolspec_contents longtext'
  end

  def down
    execute 'alter table bundle_contents modify toolspec_contents text'
  end
end
