class AddIndexExecutableIdStatusToExecutableVersion < ActiveRecord::Migration
  def change
    add_index :executable_versions, [:executable_id, :status]
  end
end
