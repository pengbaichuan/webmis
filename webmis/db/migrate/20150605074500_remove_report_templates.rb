class RemoveReportTemplates < ActiveRecord::Migration
  def change
    drop_table :report_templates
  end
end
