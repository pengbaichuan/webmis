class RemovePageTemplates < ActiveRecord::Migration
  def change
    drop_table :page_templates
  end
end
