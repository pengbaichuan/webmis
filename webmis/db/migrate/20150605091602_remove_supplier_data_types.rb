class RemoveSupplierDataTypes < ActiveRecord::Migration
  def change
    drop_table :supplier_data_types
  end
end
