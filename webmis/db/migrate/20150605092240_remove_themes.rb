class RemoveThemes < ActiveRecord::Migration
  def change
    drop_table :themes
    remove_column :users, :theme_id
  end
end
