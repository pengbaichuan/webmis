class AddIndexUserIdToDashboardGrids < ActiveRecord::Migration
  def change
    add_index :dashboard_grids, :user_id
  end
end
