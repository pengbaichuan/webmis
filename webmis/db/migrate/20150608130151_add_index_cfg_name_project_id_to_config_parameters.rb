class AddIndexCfgNameProjectIdToConfigParameters < ActiveRecord::Migration
  def change
    add_index :config_parameters, [:cfg_name,:project_id]
  end
end
