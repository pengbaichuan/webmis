class FixProcessTransitionValidationForConversion < ActiveRecord::Migration
  def up
    from_bp_id = BusinessProcess.find_by_name("CONVERSION").id
    assign_test_to_bp_id = BusinessProcess.find_by_name("ASSIGN TEST").id
    request_cnv_to_bp_id = BusinessProcess.find_by_name("REQUEST CONVERSION").id

    ProcessTransition.transaction do
      ProcessTransition.where(:from_bp => from_bp_id, :to_bp => assign_test_to_bp_id).each do |at|
        at.validation = '@conversion.conversion_databases.size > 0'
        at.save!
      end
      ProcessTransition.where(:from_bp => from_bp_id, :to_bp => request_cnv_to_bp_id).each do |rc|
        rc.validation = '@conversion.conversion_databases.size == 0'
        rc.save!
      end
    end 
  end

  def down
    # not possible
  end
end
