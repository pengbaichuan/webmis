class UpdateSendtoInReception < ActiveRecord::Migration
  def up
    # Extract email addresses from the mail object, and store them instead
    Reception.transaction do
      Reception.record_timestamps = false
      Reception.where("sendto IS NOT NULL").each do |reception|
        if !reception.sendto.blank?
          emailadresses = reception.sendto.scan(/([A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4})/i).uniq.join(',')
          reception.sendto = emailadresses
          reception.save!(:validate=>false)
        end
      end
    end
  end

  def down
    puts "It is not possible to revert this migration"
  end
end
