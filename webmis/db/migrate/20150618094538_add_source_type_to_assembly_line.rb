class AddSourceTypeToAssemblyLine < ActiveRecord::Migration
  def up
    add_column :assembly_lines, :source_type, :string
    add_column :assembly_lines, :source_id, :integer
    AssemblyLine.transaction do
      puts "-- update all assembly_lines with source. (time consuming migration)"
      AssemblyLine.all.each do |line|
        if !line.reception_id.blank?
          view_element = ViewReceptionsAndConversiondb.where(uniqid:line.reception_id)[0]
          if view_element
            obj = view_element.source_obj(line.file_name)
            if obj
              line.source_type = obj.class.name
              line.source_id = obj.id
              line.save!
            end
          end
        end
      end
      puts "   _> update assembly_lines finished"
    end
  end

  def down
    remove_column :assembly_lines, :source_type
    remove_column :assembly_lines, :source_id
  end
end
