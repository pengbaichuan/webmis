class AddParameterSettingToProductDesign < ActiveRecord::Migration
  def change
    add_column :product_designs, :parameter_setting_id, :integer
    add_column :product_designs, :parameter_setting_version, :integer
    add_column :product_design_versions, :parameter_setting_id, :integer
    add_column :product_design_versions, :parameter_setting_version, :integer
    add_column :parameter_settings, :sync_template_path,    :string
    add_column :parameter_settings, :sync_template_release, :string
    add_column :parameter_settings, :sync_template_name, :string
    add_column :parameter_settings, :last_sync_at, :datetime
    add_column :parameter_setting_versions, :sync_template_path,    :string
    add_column :parameter_setting_versions, :sync_template_release, :string
    add_column :parameter_setting_versions, :sync_template_name, :string
    add_column :parameter_setting_versions, :last_sync_at, :datetime
  end
end
