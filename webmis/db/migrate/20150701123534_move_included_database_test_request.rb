class MoveIncludedDatabaseTestRequest < ActiveRecord::Migration
  def up
    puts "-- update all test_requests with ref_db and test_db from included_database_test_requests"
    TestRequest.transaction do
      TestRequest.where("test_db_conversion_databases_id IS NULL OR ref_db_conversion_databases_id IS NULL").each do |tr|
        tests = IncludedDatabaseTestRequest.where(test_yn:true,test_request_id:tr.id)
        refs  = IncludedDatabaseTestRequest.where(ref_yn:true, test_request_id:tr.id)
        tr.test_db_conversion_databases_id = tests.last.conversion_database_id if !tests.empty?
        tr.ref_db_conversion_databases_id = refs.last.conversion_database_id if !refs.empty?
        if tr.changed?
          tr.save
        end
      end
      puts "   _> update test_requests with ref_db and test_db finished"
    end
  end

  def down
    puts "Not needed"
  end
end
