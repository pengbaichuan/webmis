class AddMailingRemarksToOutboundOrders < ActiveRecord::Migration
  def change
    add_column :outbound_orders, :mailing_remarks, :text
  end
end
