class ChangeSizeBytesLimitOnConversionDatabase < ActiveRecord::Migration
  def change
    change_column :conversion_databases, :size_bytes, :integer, :limit => 8
  end
end
