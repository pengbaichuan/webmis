class FixStdoutLogInJobVersion < ActiveRecord::Migration
  def change
    change_column :job_versions, :stdout_log, :text, :limit => 4294967295
  end
end
