class CreateCustomerDocuments < ActiveRecord::Migration
  def self.up
    create_table :customer_documents do |t|
      t.integer :customer_id
      t.string :customer_name
      t.boolean :PPF
      t.boolean :NDS
      t.boolean :TMC
      t.boolean :TPD_RN
      t.boolean :country_stats
      t.boolean :target_docs
      t.boolean :quality_doc
      t.boolean :CV_tool_RN
      t.boolean :errorlist
      t.boolean :data_supplier_RN

      t.timestamps
    end
  end

  def self.down
    drop_table :customer_documents
  end
end
