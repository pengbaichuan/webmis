class CreateCiqTypes < ActiveRecord::Migration
  def self.up
    create_table :ciq_types do |t|
      t.string :name

      t.timestamps
    end
  end

  def self.down
    drop_table :ciq_types
  end
end
