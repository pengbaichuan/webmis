class CreateAppModules < ActiveRecord::Migration
  def self.up
    create_table :app_modules do |t|
      t.string :name
      t.string :abbr
      t.string :description

      t.timestamps
    end
  end

  def self.down
    drop_table :app_modules
  end
end
