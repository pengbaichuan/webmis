class AddModules < ActiveRecord::Migration
  def self.up
    a = AppModule.new
    a.name = "WebMIS"
    a.abbr = "MIS"
    a.description = "Production processing tool"
    a.save


    a = AppModule.new
    a.name = "DSM"
    a.abbr = "DSM"
    a.description = "Data supplier management system"
    a.save

    a = AppModule.new
    a.name = "RNT"
    a.abbr = "RNT"
    a.description = "Release notes tool"
    a.save
  end

  def self.down
  end
end
