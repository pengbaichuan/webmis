class CreateRegionModules < ActiveRecord::Migration
  def self.up
    create_table :region_modules do |t|
      t.integer :region_id
      t.integer :app_module_id

      t.timestamps
    end
  end

  def self.down
    drop_table :region_modules
  end
end
