class CreateCompanyAbbrs < ActiveRecord::Migration
  def self.up
    create_table :company_abbrs do |t|
      t.integer :company_id
      t.string :company_name
      t.string :abbr

      t.timestamps
    end
  end

  def self.down
    drop_table :company_abbrs
  end
end
