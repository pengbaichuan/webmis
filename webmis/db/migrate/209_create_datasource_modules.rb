class CreateDatasourceModules < ActiveRecord::Migration
  def self.up
    create_table :datasource_modules do |t|
      t.integer :datasource_id
      t.integer :app_module_id

      t.timestamps
    end
  end

  def self.down
    drop_table :datasource_modules
  end
end
