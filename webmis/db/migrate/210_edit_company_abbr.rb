class EditCompanyAbbr < ActiveRecord::Migration
  def self.up
    change_column :company_abbrs, :company_id, :string
  end

  def self.down
  end
end
