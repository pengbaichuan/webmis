class AddRubytypeRegion < ActiveRecord::Migration
  def self.up
   add_column :regions,:ruby_type,:string
  end

  def self.down
   remove_column :regions,:ruby_type
  end
end
