class CreateReportTemplates < ActiveRecord::Migration
  def self.up
    create_table :report_templates do |t|
      t.string :name
      t.string :font
      t.text :header
      t.text :footer
      t.text :body
      t.boolean :page_numbering

      t.timestamps
    end
  end

  def self.down
    drop_table :report_templates
  end
end
