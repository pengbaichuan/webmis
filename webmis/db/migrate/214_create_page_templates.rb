class CreatePageTemplates < ActiveRecord::Migration
  def self.up
    create_table :page_templates do |t|
      t.string :name
      t.string :font
      t.text :header
      t.text :footer
      t.text :body
      t.boolean :page_numbering
      t.integer :report_template_id

      t.timestamps
    end
  end

  def self.down
    drop_table :page_templates
  end
end
