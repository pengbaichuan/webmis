class CreateServers < ActiveRecord::Migration
  def self.up
    create_table :servers do |t|
      t.string :server_id
      t.string :server_remark
      t.boolean :active
      t.string :diskcapacity
      t.string :processor
      t.text :additionalprocess

      t.timestamps
    end
  end

  def self.down
    drop_table :servers
  end
end
