class AddServeridToConversion < ActiveRecord::Migration
  def self.up
   add_column :conversions , :server_name ,:string
  end

  def self.down
   remove_column :conversions , :server_name
  end
end
