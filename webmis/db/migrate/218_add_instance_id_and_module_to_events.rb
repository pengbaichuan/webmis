class AddInstanceIdAndModuleToEvents < ActiveRecord::Migration
  def self.up
	add_column :events, :instance_id, :integer
	add_column :events, :app_module, :integer
	@events = Event.find(:all)
	@events.each do |event|
		event.app_module = AppModule.find_by_abbr("DSM").id
		event.save
	end
  end

  def self.down
  end
end
