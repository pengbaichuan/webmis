class AddPhoneExtToUser < ActiveRecord::Migration
  def self.up
   add_column :users, :phone_ext, :string
  end

  def self.down
   remove_column :users, :phone_ext
  end
end
