class CreateScenarios < ActiveRecord::Migration
  def self.up
    create_table :scenarios do |t|
      t.text :query
      t.string :naam

      t.timestamps
    end
  end

  def self.down
    drop_table :scenarios
  end
end
