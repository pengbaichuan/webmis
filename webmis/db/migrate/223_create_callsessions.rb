class CreateCallsessions < ActiveRecord::Migration
  def self.up
    create_table :callsessions do |t|
      t.string :feature_code
      t.string :country_code
      t.integer :scenario_id
      t.integer :user_id

      t.timestamps
    end
  end

  def self.down
    drop_table :callsessions
  end
end
