class CreateCalls < ActiveRecord::Migration
  def self.up
    create_table :calls do |t|
      t.integer :callsession_id
      t.integer :poimatch_id

      t.timestamps
    end
  end

  def self.down
    drop_table :calls
  end
end
