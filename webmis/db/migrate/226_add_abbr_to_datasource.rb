class AddAbbrToDatasource < ActiveRecord::Migration
  def self.up
   add_column :datasources, :abbr, :string
  end

  def self.down
   remove_column :datasources, :abbr
  end
end
