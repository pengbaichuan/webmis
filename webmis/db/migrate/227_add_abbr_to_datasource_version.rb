class AddAbbrToDatasourceVersion < ActiveRecord::Migration
  def self.up
   add_column :datasource_versions, :abbr, :string
  end

  def self.down
   remove_column :datasource_versions, :abbr
  end
end
