class AddChecksToScenario < ActiveRecord::Migration
  def self.up
	add_column :scenarios, :name_of_poi, :boolean
	add_column :scenarios, :streetname, :boolean
	add_column :scenarios, :housenumber, :boolean
	add_column :scenarios, :phonenumber, :boolean
	add_column :scenarios, :phonenumber_exists, :boolean
	add_column :scenarios, :remark, :boolean
	add_column :scenarios, :poi_exists, :boolean
  end

  def self.down
  end
end
