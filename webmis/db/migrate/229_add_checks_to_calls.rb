class AddChecksToCalls < ActiveRecord::Migration
  def self.up
	add_column :calls, :correct_name, :string
	add_column :calls, :correct_streetname, :string
	add_column :calls, :correct_housenumber, :string
	add_column :calls, :correct_phonenumber, :string
	add_column :calls, :remark, :text
	add_column :calls, :phonenumber_exists, :boolean
	add_column :calls, :poi_exists, :boolean
  end

  def self.down
  end
end
