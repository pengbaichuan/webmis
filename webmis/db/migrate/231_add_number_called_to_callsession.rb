class AddNumberCalledToCallsession < ActiveRecord::Migration
  def self.up
	add_column :callsessions, :number_called, :integer
  end

  def self.down
  end
end
