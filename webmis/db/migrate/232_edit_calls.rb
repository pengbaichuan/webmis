class EditCalls < ActiveRecord::Migration
  def self.up
	change_column :calls, :poi_exists, :string
	change_column :calls, :phonenumber_exists, :string
  end

  def self.down
  end
end
