class AddDatasourcetypeToCallsession < ActiveRecord::Migration
  def self.up
    add_column :callsessions, :datasource_type, :integer
  end

  def self.down
  end
end
