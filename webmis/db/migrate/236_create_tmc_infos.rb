class CreateTmcInfos < ActiveRecord::Migration
  def self.up
    create_table :tmc_infos do |t|
      t.string :name
      t.integer :data_release_id
      t.string :data_release_name
      t.text :description
      t.string :approved_by
      t.date :approve_data
      t.string :approver_role
      t.integer :status

      t.timestamps
    end
  end

  def self.down
    drop_table :tmc_infos
  end
end
