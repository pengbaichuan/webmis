class CreateTmcTables < ActiveRecord::Migration
  def self.up
    create_table :tmc_tables do |t|
      t.string :country
      t.integer :country_id
      t.integer :supplier_id
      t.string :supplier_name
      t.string :version
      t.timestamps
    end
  end

  def self.down
    drop_table :tmc_tables
  end
end
