class CreateTmcInfoTables < ActiveRecord::Migration
  def self.up
    create_table :tmc_info_tables do |t|
      t.integer :tmc_info_id
      t.integer :tmc_table_id
      t.string :version
      t.timestamps
    end
  end

  def self.down
    drop_table :tmc_info_tables
  end
end
