class AddTmcfileToPset < ActiveRecord::Migration
  def self.up
   add_column :productsets, :tmc_file, :string
  end

  def self.down
   add_column :productsets, :tmc_file, :string
  end
end
