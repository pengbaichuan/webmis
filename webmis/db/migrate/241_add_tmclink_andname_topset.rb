class AddTmclinkAndnameTopset < ActiveRecord::Migration
  def self.up
   add_column :productsets ,:tmc_info_id, :integer
   add_column :productset_versions,:tmc_info_id, :integer
   add_column :productsets ,:tmc_info_name, :string
   add_column :productset_versions,:tmc_info_name, :string
  end

  def self.down
   remove_column :productsets ,:tmc_info_id
   remove_column :productset_versions,:tmc_info_id
   remove_column :productset,:tmc_info_name
   remove_column :productset_versions,:tmc_info_name
  end
end
