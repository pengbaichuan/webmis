class AddLockFieldToTables < ActiveRecord::Migration
  def self.up
   add_column :productsets, :lock_version, :integer
   add_column :products, :lock_version, :integer
   add_column :productset_versions, :lock_version, :integer
   add_column :product_versions, :lock_version, :integer
  end

  def self.down
   remove_column :productsets, :lock_version
   remove_column :products, :lock_version
   remove_column :productset_versions, :lock_version
   remove_column :product_versions, :lock_version
  end
end
