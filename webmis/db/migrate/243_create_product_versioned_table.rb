class CreateProductVersionedTable < ActiveRecord::Migration
  def self.up
   #drop_table :product_versions
   #add_column :products, :version, :integer
   #add_column :products, :created_at, :timestamp
   #add_column :products, :updated_at, :timestamp
   Product.create_versioned_table
  end

  def self.down
   Product.drop_versioned_table
  end
end
