class CreateOutboundOrderlines < ActiveRecord::Migration
  def self.up
    create_table :outbound_orderlines do |t|
      t.string :map_no
      t.string :cd_no
      t.integer :amount
      t.string :product_id
      t.integer :stock_id
      t.string :filename
      t.string :product_name
      t.integer :outbound_order_id

      t.timestamps
    end
  end

  def self.down
    drop_table :outbound_orderlines
  end
end
