class AddLinkProductIdToProduct < ActiveRecord::Migration
  def self.up
   add_column :includedproducts, :link_product_id, :integer
  end

  def self.down
   remove_column :includedproducts, :link_product_id
  end
end
