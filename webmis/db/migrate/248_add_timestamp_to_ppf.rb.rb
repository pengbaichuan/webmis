class AddTimestampToPpf < ActiveRecord::Migration
  def self.up
   add_column :productsets, :created_at, :timestamp
   add_column :productsets, :updated_at, :timestamp
   add_column :productset_versions, :created_at, :timestamp
   add_column :productset_versions, :updated_at, :timestamp
  end

  def self.down
   remove_column :productsets, :created_at
   remove_column :productsets, :updated_at
   remove_column :productset_versions, :created_at
   remove_column :productset_versions, :updated_at
  end
end
