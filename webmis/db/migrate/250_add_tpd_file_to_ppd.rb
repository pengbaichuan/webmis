class AddTpdFileToPpd < ActiveRecord::Migration
  def self.up
   add_column :products, :tpd_file, :string
   add_column :productsets, :tpd_file, :string
   add_column :product_versions, :tpd_file, :string
   add_column :productset_versions, :tpd_file, :string
  end

  def self.down
   remove_column :products, :tpd_file
   remove_column :productsets, :tpd_file
   remove_column :product_versions, :tpd_file, :string
   remove_column :productset_versions, :tpd_file, :string
  end
end
