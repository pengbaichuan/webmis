class AddAddonToPsetDatasource < ActiveRecord::Migration
  def self.up
   add_column :productsets, :datasource_extrainfo, :string
   add_column :productset_versions, :datasource_extrainfo, :string
  end

  def self.down
   remove_column :productsets, :datasource_extrainfo 
   remove_column :productset_versions, :datasource_extrainfo 
  end
end
