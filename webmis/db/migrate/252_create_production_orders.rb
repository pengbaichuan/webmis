class CreateProductionOrders < ActiveRecord::Migration
  def self.up
    create_table :production_orders do |t|
      t.integer :productset_id
      t.integer :major
      t.integer :minor
      t.string :setname
      t.string :status
      t.string :requestor
      t.string :owner
      t.text :comments
      t.timestamp :completion_date
      t.timestamp :request_date
      t.integer :priority

      t.timestamps
    end
  end

  def self.down
    drop_table :production_orders
  end
end
