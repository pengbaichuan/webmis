class ChangeProductsetVersioning < ActiveRecord::Migration
  def self.up
   Productset.drop_versioned_table
   Productset.create_versioned_table
   add_column :productsets , :version, :integer
  end

  def self.down
  end
end
