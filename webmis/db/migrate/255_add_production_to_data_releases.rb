class AddProductionToDataReleases < ActiveRecord::Migration
  def self.up
    add_column :data_releases, :production, :boolean, :default => false
  end

  def self.down
    remove_column :data_releases, :production
  end
end
