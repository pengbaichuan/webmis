class AddProductionToDatasources < ActiveRecord::Migration
  def self.up
    add_column :datasource_versions, :production, :boolean, :default => false
    add_column :datasources, :production, :boolean, :default => false
  end

  def self.down
    remove_column :datasource_versions, :production
    remove_column :datasources, :production
  end
end
