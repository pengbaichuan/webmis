class AddUseProxyToInternetDatasource < ActiveRecord::Migration
  def self.up
    add_column :internet_datasource_versions, :use_proxy, :boolean, :default => false
    add_column :internet_datasources, :use_proxy, :boolean, :default => false
  end

  def self.down
    remove_column :internet_datasource_versions, :use_proxy
    remove_column :internet_datasources, :use_proxy
  end
end
