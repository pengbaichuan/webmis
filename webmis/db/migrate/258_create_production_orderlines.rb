class CreateProductionOrderlines < ActiveRecord::Migration
  def self.up
    create_table :production_orderlines do |t|
      t.integer :production_order_id
      t.string :product_id
      t.string :linestatus
      t.string :bp_name
      t.string :bp_status
      t.integer :quantity
      t.text :remarks

      t.timestamps
    end
  end

  def self.down
    drop_table :production_orderlines
  end
end
