class CreateOrdertypes < ActiveRecord::Migration
  def self.up
    create_table :ordertypes do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end

  def self.down
    drop_table :ordertypes
  end
end
