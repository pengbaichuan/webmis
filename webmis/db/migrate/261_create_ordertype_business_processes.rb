class CreateOrdertypeBusinessProcesses < ActiveRecord::Migration
  def self.up
    create_table :ordertype_business_processes do |t|
      t.integer :ordertype_id
      t.integer :business_process_id
      t.integer :sequence

      t.timestamps
    end
  end

  def self.down
    drop_table :ordertype_business_processes
  end
end
