class CreateAssetTypeNumberRanges < ActiveRecord::Migration
  def self.up
    create_table :asset_type_number_ranges do |t|
      t.string :name
      t.integer :start_range
      t.integer :end_range
      t.integer :asset_type_id

      t.timestamps
    end
  end

  def self.down
    drop_table :asset_type_number_ranges
  end
end
