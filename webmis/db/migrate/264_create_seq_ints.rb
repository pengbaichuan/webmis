class CreateSeqInts < ActiveRecord::Migration
  def self.up
    create_table :seq_ints do |t|
      t.string :name
      t.integer :current_value

      t.timestamps
    end
  end

  def self.down
    drop_table :seq_ints
  end
end
