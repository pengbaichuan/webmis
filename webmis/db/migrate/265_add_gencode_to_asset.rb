class AddGencodeToAsset < ActiveRecord::Migration
  def self.up
    add_column :asset_types, :gencode,  :text
  end

  def self.down
    remove_column :asset_types, :gencode 
  end
end
