class AddLockIdToSeqint < ActiveRecord::Migration
  def self.up
   add_column :seq_ints, :lock_version, :integer
  end

  def self.down
   remove_column :seq_ints, :lock_version
  end
end
