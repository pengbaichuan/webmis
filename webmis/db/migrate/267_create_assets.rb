class CreateAssets < ActiveRecord::Migration
  def self.up
    create_table :assets do |t|
      t.string :ref_id
      t.string :requested_by
      t.string :remarks
      t.string :assettype
      t.timestamps
    end
    add_index :seq_ints, :name, :unique => true 
    add_index :assets, :ref_id, :unique => true
  end

  def self.down
    drop_table :assets
  end
end
