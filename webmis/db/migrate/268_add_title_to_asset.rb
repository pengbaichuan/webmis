class AddTitleToAsset < ActiveRecord::Migration
  def self.up
   add_column :assets, :title,:string
   add_column :company_abbrs, :relation_type,:string
  end

  def self.down
   remove_column :assets, :title
   add_column :company_abbrs, :relation_type,:string
  end
end
