class CreateProcessTransitions < ActiveRecord::Migration
  def self.up
    create_table :process_transitions do |t|
      t.integer :from_bp
      t.integer :to_bp
      t.text :validation
      t.string :action_label

      t.timestamps
    end
  end

  def self.down
    drop_table :process_transitions
  end
end
