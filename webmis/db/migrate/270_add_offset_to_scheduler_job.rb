class AddOffsetToSchedulerJob < ActiveRecord::Migration
  def self.up
   add_column :scheduler_jobs,:last_position,:string
  end

  def self.down
   remove_column :scheduler_jobs,:last_position
  end
end
