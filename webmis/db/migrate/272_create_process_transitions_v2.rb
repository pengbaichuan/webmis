class CreateProcessTransitionsV2 < ActiveRecord::Migration
  def self.up
    create_table :process_transitions do |t|
      t.integer :ordertype_id
      t.integer :from_bp
      t.integer :to_bp
      t.text :validation
      t.string :from_level
      t.string :to_level
      t.string :action_label
      t.integer :sequence

      t.timestamps
    end
  end

  def self.down
    drop_table :process_transitions
  end
end
