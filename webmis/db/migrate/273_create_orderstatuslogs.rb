class CreateOrderstatuslogs < ActiveRecord::Migration
  def self.up
    create_table :orderstatuslogs do |t|
      t.integer :production_order_id
      t.timestamp :completion_date
      t.string :level
      t.string :user
      t.integer :sequence
      t.string :status
      t.text :remarks
      t.string :reference

      t.timestamps
    end
  end

  def self.down
    drop_table :orderstatuslogs
  end
end
