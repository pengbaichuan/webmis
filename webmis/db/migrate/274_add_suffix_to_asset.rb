class AddSuffixToAsset < ActiveRecord::Migration
  def self.up
    add_column :assets, :suffix, :string
  end

  def self.down
    remove_column :assets, :suffix
  end
end
