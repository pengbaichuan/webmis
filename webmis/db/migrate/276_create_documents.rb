class CreateDocuments < ActiveRecord::Migration
  def self.up
    create_table :documents do |t|
      t.string :content_type
      t.string :filename
      t.string :description
      t.binary :binary_data

      t.timestamps
    end
  end

  def self.down
    drop_table :documents
  end
end
