class AddDocumentIdToAsset < ActiveRecord::Migration
  def self.up
   create_table :document_versions do |t|
      t.integer :document_id
      t.string :content_type
      t.string :filename
      t.string :description
      t.binary :binary_data
      t.integer :version
      t.timestamps
   end
   add_column :assets, :document_id, :integer
   add_column :documents, :version, :integer
  end

  def self.down
   remove_column :assets, :document_id
   remove_column :documents, :version
   drop_table :document_versions, :document_id
  end
end
