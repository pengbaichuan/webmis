class AddCascadefieldToProcesstransitions < ActiveRecord::Migration
  def self.up
   add_column :process_transitions, :cascade_to_level, :string
   add_column :process_transitions, :cascade_to_bp, :integer
   add_column :process_transitions, :cascade_type, :string
  end

  def self.down
   remove_column :process_transitions, :cascade_to_level
   remove_column :process_transitions, :cascade_to_bp
   remove_column :process_transitions, :cascade_type
  end
end
