class AddbpSeqnrToPo < ActiveRecord::Migration
  def self.up
   add_column :production_orders, :bp_seqnr, :integer
   add_column :production_orderlines, :bp_seqnr, :integer
  end

  def self.down
   remove_column :production_orders, :bp_seqnr
   remove_column :production_orderlines, :bp_seqnr
  end
end
