class AddAssignedToProductionorderProduct < ActiveRecord::Migration
  def self.up
   add_column :production_orderlines,:owner,:string
   add_column :production_orderlines,:priority, :integer
  end

  def self.down
   add_column production_orderlines,:owner,:string
   add_column :production_orderlines, :priority, :integer
  end
end
