class AddProductionOrderlineIdToConversion < ActiveRecord::Migration
  def self.up
   add_column :conversions, :production_orderline_id, :integer
  end

  def self.down
   remove_column :conversions, :production_orderline_id
  end
end
