class AddTestRequirementToPo < ActiveRecord::Migration
  def self.up
   add_column :productsets, :test_requirement, :text
   add_column :products, :test_requirement, :text
   add_column :productset_versions, :test_requirement, :text
   add_column :product_versions, :test_requirement, :text
   add_column :production_orders, :test_requirement, :text
   add_column :production_orderlines, :test_requirement, :text
  end

  def self.down
   remove_column :productsets, :test_requirement
   remove_column :products, :test_requirement
   remove_column :productset_versions, :test_requirement
   remove_column :product_versions, :test_requirement
   remove__column :production_orders, :test_requirement
   remove__column :production_orderlines, :test_requirement
  end
end
