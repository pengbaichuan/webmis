class AddLock < ActiveRecord::Migration
 def self.up
   add_column :productset_versions, :lock_version, :integer
   add_column :product_versions, :lock_version, :integer
  end

  def self.down
   remove_column :productset_versions, :lock_version
   remove_column :product_versions, :lock_version
  end
end
