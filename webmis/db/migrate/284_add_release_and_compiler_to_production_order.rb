class AddReleaseAndCompilerToProductionOrder < ActiveRecord::Migration
  def self.up
    add_column :production_orders,:datarelease_name,:string
    add_column :production_orders,:conversiontool_name,:string
  end

  def self.down
    remove_column :production_orders,:datarelease_name
    remove_column :production_orders,:conversiontool_name
  end
end
