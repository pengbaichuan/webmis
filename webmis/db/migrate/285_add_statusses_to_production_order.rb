class AddStatussesToProductionOrder < ActiveRecord::Migration
  def self.up
    add_column :production_orders, :allow_planning, :boolean
    add_column :production_orders, :allow_production, :boolean
    add_column :production_orders, :allow_shipment, :boolean
    add_column :production_orders, :on_hold, :boolean
  end

  def self.down
    remove_column :production_orders, :allow_planning
    remove_column :production_orders, :allow_production
    remove_column :production_orders, :allow_shipment
    remove_column :production_orders, :on_hold
  end
end
