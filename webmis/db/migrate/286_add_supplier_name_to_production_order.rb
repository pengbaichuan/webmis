class AddSupplierNameToProductionOrder < ActiveRecord::Migration
  def self.up
   add_column :production_orders, :supplier_name, :string
   add_column :production_orders, :region_name, :string
   add_column :production_orders, :data_release_id, :integer
  end

  def self.down
   remove_column :production_orders, :supplier_name
   remove_column :production_orders, :region_name
   remove_column :production_orders, :data_release_id
  end
end
