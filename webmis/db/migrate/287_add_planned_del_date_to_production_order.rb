class AddPlannedDelDateToProductionOrder < ActiveRecord::Migration
  def self.up
   add_column :production_orders, :planned_shipdate, :date
  end

  def self.down
   remove_column :production_orders, :planned_shipdate
  end
end
