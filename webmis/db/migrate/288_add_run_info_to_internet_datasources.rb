class AddRunInfoToInternetDatasources < ActiveRecord::Migration
  def self.up
    add_column :internet_datasources, :host_last_run, :string
    add_column :internet_datasources, :date_last_run, :datetime
  end

  def self.down
    remove_column :internet_datasources, :host_last_run
    remove_column :internet_datasources, :date_last_run
  end
end
