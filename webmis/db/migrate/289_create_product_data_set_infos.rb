class CreateProductDataSetInfos < ActiveRecord::Migration
  def self.up
    create_table :product_data_set_infos do |t|
      t.integer :product_id
      t.integer :datarelease_id
      t.string :description
      t.text :remarks
      t.integer :datatype_id
      t.string :area_abbr
      t.timestamps
    end
  end

  def self.down
    drop_table :product_data_set_infos
  end
end
