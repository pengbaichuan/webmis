class AddAbbrtypesToCompanyabbrs < ActiveRecord::Migration
  def self.up
   add_column :company_abbrs, :ms_abbr_2, :string
   add_column :company_abbrs, :ms_abbr_3, :string
  end

  def self.down
   remove_column :company_abbrs, :ms_abbr_2
   remove_column :company_abbrs, :ms_abbr_3
  end
end
