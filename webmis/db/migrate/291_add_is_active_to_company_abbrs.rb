class AddIsActiveToCompanyAbbrs < ActiveRecord::Migration
  def self.up
   add_column :company_abbrs, :is_active, :boolean
  end

  def self.down
   remove_column :company_abbrs, :is_active
  end
end
