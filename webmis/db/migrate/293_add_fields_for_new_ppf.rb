class AddFieldsForNewPpf < ActiveRecord::Migration
  def self.up
    add_column :productsets, :settype,  :string
    add_column :productsets, :areaabbr, :string
    add_column :productsets, :contabbr, :string
    add_column :productset_versions, :settype,  :string
    add_column :productset_versions, :areaabbr, :string
    add_column :productset_versions, :contabbr, :string
  end

  def self.down
    remove_column :productsets, :settype
    remove_column :productsets, :areaabbr
    remove_column :productsets, :contabbr
    remove_column :productset_versions, :settype
    remove_column :productset_versions, :areaabbr
    remove_column :productset_versions, :contabbr
  end
end
