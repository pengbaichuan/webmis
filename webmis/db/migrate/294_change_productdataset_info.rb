class ChangeProductdatasetInfo < ActiveRecord::Migration
  def self.up
   remove_column :product_data_set_infos, :datarelease_id
   add_column :product_data_set_infos, :name, :string
   add_column :product_data_set_infos, :company_abbr_id, :integer
   add_column :product_data_set_infos, :data_release, :string
   add_column :product_data_set_infos, :data_info, :string

  
  end

  def self.down
   remove_column :product_data_set_infos, :name
   add_column :product_data_set_infos, :datarelease_id, :string
   remove_column :product_data_set_infos, :company_abbr_id
   remove_column :product_data_set_infos, :data_release
   remove_column :product_data_set_infos, :data_info
  end
end
