class LinkDatareleaseToSupplier < ActiveRecord::Migration
  def self.up
   add_column :data_releases, :company_abbr_id, :integer
  end

  def self.down
   remove_column :data_releases, :company_abbr_id
  end
end
