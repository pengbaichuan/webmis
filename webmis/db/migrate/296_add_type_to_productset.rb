class AddTypeToProductset < ActiveRecord::Migration
  def self.up
    add_column :productsets, :type, :string
  end

  def self.down
    remove_column :productsets, :type
  end
end
