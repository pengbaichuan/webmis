class CreateAssemblyHeaders < ActiveRecord::Migration
  def self.up
    create_table :assembly_headers do |t|
      t.string :product_id
      t.integer :version
      t.text :remarks
      t.string :name

      t.timestamps
    end
  end

  def self.down
    drop_table :assembly_headers
  end
end
