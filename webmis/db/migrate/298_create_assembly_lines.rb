class CreateAssemblyLines < ActiveRecord::Migration
  def self.up
    create_table :assembly_lines do |t|
      t.integer :assembly_header_id
      t.integer :reception_id
      t.string :reference_id
      t.integer :company_abbr_id
      t.integer :data_release_id
      t.string :area
      t.string :file_name

      t.timestamps
    end
  end

  def self.down
    drop_table :assembly_lines
  end
end
