class AddDataReleaseIdAndProductIdToAssembly < ActiveRecord::Migration
  def self.up
   add_column :assembly_lines, :data_release_name, :string
  end

  def self.down
   add_column :assembly_lines, :data_release_name, :string
  end
end
