class AddSessionToPoints < ActiveRecord::Migration
  def self.up
    add_column    :points, :session, :datetime
  end

  def self.down
    remove_column :points, :session
  end
end
