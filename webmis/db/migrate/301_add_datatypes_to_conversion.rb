class AddDatatypesToConversion < ActiveRecord::Migration
  def self.up
    add_column :conversions, :output_format, :string
    add_column :conversions, :input_format, :string
  end

  def self.down
    remove_column :conversions, :input_format
    remove_column :conversions, :output_format
  end
end
