class AddRemovedFlagToReception < ActiveRecord::Migration
  def self.up
    add_column :receptions, :removed_yn,:boolean
  end

  def self.down
    remove_column :receptions, :removed_yn
  end
end
