class AddImportanceToInetsource < ActiveRecord::Migration
  def self.up
   add_column :internet_datasources, :priority, :integer
  end

  def self.down
   remove_column :internet_datasources, :priority
  end
end
