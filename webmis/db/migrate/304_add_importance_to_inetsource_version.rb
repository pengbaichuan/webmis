class AddImportanceToInetsourceVersion < ActiveRecord::Migration
  def self.up
   add_column :internet_datasource_versions, :priority, :integer
  end

  def self.down
   remove_column :internet_datasources_versions, :priority
  end
end
