class CreateCountryCoverages < ActiveRecord::Migration
  def self.up
    create_table :country_coverages do |t|
      t.integer :internet_datasource_id
      t.string :country_code
      t.integer :amount_found
      t.integer :amount_expected

      t.timestamps
    end
  end

  def self.down
    drop_table :country_coverages
  end
end
