class AddActiveFlagToLanguages < ActiveRecord::Migration
  def self.up
   add_column :languages, :active_yn, :integer
  end

  def self.down
   remove_column :languages, :active_yn
  end
end
