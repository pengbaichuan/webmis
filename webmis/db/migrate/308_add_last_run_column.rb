class AddLastRunColumn < ActiveRecord::Migration
  def self.up
    #add_column :internet_datasource_versions, :date_last_run, :timestamp
    add_column :internet_datasource_versions, :host_last_run, :string
  end

  def self.down
  end
end
