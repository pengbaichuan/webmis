class AddParentToPoint < ActiveRecord::Migration
  def self.up
    add_column :points, :parent_name, :string
    add_column :points, :parent_id, :integer
    add_column :points, :parent_url, :string
    add_column :points, :parent_email_address, :string
    add_column :points, :super_point_id, :integer
    add_column :points, :fax_number, :string
    add_column :points, :email_address, :string
  end

  def self.down
    remove_column :points, :parent_name
    remove_column :points, :parent_id
    remove_column :points, :parent_url
    remove_column :points, :parent_email_address
    remove_column :points, :super_point_id
    remove_column :points, :fax_number
    remove_column :points, :email_address
  end
end
