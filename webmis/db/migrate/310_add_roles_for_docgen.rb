class AddRolesForDocgen < ActiveRecord::Migration
  def self.up
    r = Role.new
    r.title = "Managing Partners"
    r.save

    r = Role.new
    r.title = "Sales"
    r.save

    r = Role.new
    r.title = "HRM"
    r.save

    r = Role.new
    r.title = "Others"
    r.save

    add_column :assets, :role, :string


  end

  def self.down
    remove_column :assets, :role
  end
end
