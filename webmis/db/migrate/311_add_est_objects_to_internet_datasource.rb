class AddEstObjectsToInternetDatasource < ActiveRecord::Migration
  def self.up
   add_column :internet_datasources, :estimated_amount, :integer
   add_column :internet_datasource_versions, :estimated_amount, :integer
  end

  def self.down
   remove_column :internet_datasources, :estimated_amount
   remove_column :internet_datasource_versions, :estimated_amount
  end
end
