class AddReleasedAtToPoint < ActiveRecord::Migration
  def self.up
   add_column :points, :released_at, :timestamp
  end

  def self.down
   remove_column :points, :released_at
  end
end
