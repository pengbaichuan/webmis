class AddparentIdToCategory < ActiveRecord::Migration
  def self.up
    add_column :categories, :parent_id, :integer
    add_column :categories, :level, :integer
    add_column :categories, :description, :text
  end

  def self.down
    remove_column :categories, :parent_id
    remove_column :categories, :level
    remove_column :categories, :description
  end
end
