class CreateCategoryAttributes < ActiveRecord::Migration
  def self.up
    create_table :category_attributes do |t|
      t.integer :category_id
      t.integer :attribute_id
      t.integer :description

      t.timestamps
    end
  end

  def self.down
    drop_table :category_attributes
  end
end
