class CreatePointAttributes < ActiveRecord::Migration
  def self.up
    create_table :point_attributes do |t|
      t.string :name
      t.text :description
      t.string :type

      t.timestamps
    end
  end

  def self.down
    drop_table :point_attributes
  end
end
