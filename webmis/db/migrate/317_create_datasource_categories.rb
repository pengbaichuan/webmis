class CreateDatasourceCategories < ActiveRecord::Migration
  def self.up
    create_table :datasource_categories do |t|
      t.integer :datasource_id
      t.integer :category_id
      t.text :remarks
      t.string :site_category

      t.timestamps
    end
  end

  def self.down
    drop_table :datasource_categories
  end
end
