class RenameCategoryAttribute < ActiveRecord::Migration
  def self.up
   rename_column :category_attributes, :attribute_id, :point_attribute_id
  end

  def self.down
   rename_column :category_attributes, :point_attribute_id, :attribute_id
  end
end
