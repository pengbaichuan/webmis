class AddScriptCreator < ActiveRecord::Migration
  def self.up
    add_column :internet_datasources, :script_creator, :string
    add_column :internet_datasource_versions, :script_creator, :string
    add_column :users, :partner_code, :string
  end

  def self.down
    remove_column :internet_datasources, :script_creator
    remove_column :internet_datasource_versions, :script_creator
    remove_column :users, :partner_code
  end
end
