class AlterUnderconstructiondate < ActiveRecord::Migration
  def self.up
    change_column(:productsets,:underconstructiondate,:string)
  end

  def self.down
    change_column(:productsets,:underconstructiondate,:date)
  end
end
