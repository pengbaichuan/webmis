class ProxServerDetails < ActiveRecord::Migration
  def self.up
   add_column :internet_datasources, :proxy_server, :string
   add_column :internet_datasources, :proxy_port, :string
  end

  def self.down
   remove_column :internet_datasources, :proxy_server
   remove_column :internet_datasources, :proxy_port
  end
end
