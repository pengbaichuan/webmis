class CreateMatchmasks < ActiveRecord::Migration
  def self.up
    create_table :matchmasks do |t|
      t.integer :mask
      t.string :mask_string
      t.string :description
      t.timestamps
    end
    m = Matchmask.new
    m.mask = 7
    m.description = "Address"
    m.save
    m = Matchmask.new
    m.mask = 9
    m.description = "Phone"
    m.save
    m = Matchmask.new
    m.mask = 11
    m.description = "Phone & Street"
    m.save
    m = Matchmask.new
    m.mask = 13
    m.description = "Phone & Housenumber"
    m.save
    m = Matchmask.new
    m.mask = 15
    m.description = "Full Except Name & Category"
    m.save
    m = Matchmask.new
    m.mask = 15
    m.description = "Full Except Name & Category"
    m.save
    m = Matchmask.new
    m.mask = 17
    m.description = "Name"
    m.save
    m = Matchmask.new
    m.mask = 19
    m.description = "Name & Street"
    m.save
    m = Matchmask.new
    m.mask = 21
    m.description = "Name & Housenumber"
    m.save
    m = Matchmask.new
    m.mask = 23
    m.description = "Full except Phone and Category"
    m.save
    m = Matchmask.new
    m.mask = 25
    m.description = "Full except Address and Category"
    m.save
    m = Matchmask.new
    m.mask = 29
    m.description = "Full except Street"
    m.save
    m = Matchmask.new
    m.mask = 31
    m.description = "Full Match"
    m.save
    m = Matchmask.new
    m.mask = 39
    m.description = "Address"
    m.save
    m = Matchmask.new
    m.mask = 41
    m.description = "Phone"
    m.save
    m = Matchmask.new
    m.mask = 43
    m.description = "Phone & Street"
    m.save
    m = Matchmask.new
    m.mask = 45
    m.description = "Phone & Housenumber"
    m.save
    m = Matchmask.new
    m.mask = 47
    m.description = "Full Except Name & Category"
    m.save
    m = Matchmask.new
    m.mask = 49
    m.description = "Name"
    m.save
    m = Matchmask.new
    m.mask = 51
    m.description = "Name & Street"
    m.save
    m = Matchmask.new
    m.mask = 53
    m.description = "Name & Housenumber"
    m.save
    m = Matchmask.new
    m.mask = 55
    m.description = "Full except Phone and Category"
    m.save
    m = Matchmask.new
    m.mask = 57
    m.description = "Full except Address and Category"
    m.save
    m = Matchmask.new
    m.mask = 59
    m.description = "Full except Housenumber"
    m.save
    m = Matchmask.new
    m.mask = 61
    m.description = "Full except Street"
    m.save
    m = Matchmask.new
    m.mask = 63
    m.description = "Full Match"
    m.save

  end


  def self.down
    drop_table :matchmasks
  end
end
