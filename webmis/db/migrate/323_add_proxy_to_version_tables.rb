class AddProxyToVersionTables < ActiveRecord::Migration
 def self.up
   add_column :internet_datasource_versions, :proxy_server, :string
   add_column :internet_datasource_versions, :proxy_port, :string
   add_column :internet_datasource_versions, :high_priority, :integer
  end

  def self.down
   remove_column :internet_datasource_versions, :proxy_server
   remove_column :internet_datasource_versions, :proxy_port
   remove_column :internet_datasources_versions, :high_priority
  end
end
