class AddValidationFlagToReception < ActiveRecord::Migration
  def self.up
   add_column :receptions, :validated_yn, :boolean
  end

  def self.down
   drop_column :receptions, :validated_yn
  end
end
