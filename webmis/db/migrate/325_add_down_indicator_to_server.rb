class AddDownIndicatorToServer < ActiveRecord::Migration
  def self.up
   add_column :servers, :down, :boolean
  end

  def self.down
   remove_column :servers, :down
  end
end
