class CreateApiKeys < ActiveRecord::Migration
  def self.up
    create_table :api_keys do |t|
      t.string :key
      t.integer :user_id
      t.string :owner
      t.string :sec_level

      t.timestamps
    end
  end

  def self.down
    drop_table :api_keys
  end
end
