class AddRemovedYnToProductset < ActiveRecord::Migration
  def self.up
   add_column :productsets, :removed_yn, :boolean
   add_column :productset_versions, :removed_yn, :boolean
  end

  def self.down
   remove_column :productsets, :removed_yn
   remove_column :productset_versions, :removed_yn
  end
end
