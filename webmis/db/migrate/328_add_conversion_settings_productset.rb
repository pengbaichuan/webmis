class AddConversionSettingsProductset < ActiveRecord::Migration
  def self.up
   add_column :products, :conversion_settings, :text
   add_column :productsets, :output_format, :string
   add_column :productsets, :output_format_extrainfo, :string
   add_column :productsets, :compressed_yn, :boolean
   add_column :productsets, :bundled_yn, :boolean
   add_column :productsets, :image_yn, :boolean
   add_column :productsets, :physical_medium_yn, :boolean
   add_column :product_versions, :conversion_settings, :text
   add_column :productset_versions, :output_format, :string
   add_column :productset_versions, :output_format_extrainfo, :string
   add_column :productset_versions, :compressed_yn, :boolean
   add_column :productset_versions, :bundled_yn, :boolean
   add_column :productset_versions, :image_yn, :boolean
   add_column :productset_versions, :physical_medium_yn, :boolean
  end

  def self.down
   remove_column :products, :conversion_settings
   remove_column :productsets, :output_format
   remove_column :productsets, :output_format_extrainfo
   remove_column :productsets, :compressed_yn
   remove_column :productsets, :bundled_yn
   remove_column :productsets, :image_yn
   remove_column :productsets, :physical_medium_yn
   remove_column :product_versions, :conversion_settings
   remove_column :productset_versions, :output_format
   remove_column :productset_versions, :output_format_extrainfo
   remove_column :productset_versions, :compressed_yn
   remove_column :productset_versions, :bundled_yn
   remove_column :productset_versions, :image_yn
   remove_column :productset_versions, :physical_medium_yn
  end
end
