class AddStatusToReception < ActiveRecord::Migration
  def self.up
   add_column :receptions, :status, :string
  end

  def self.down
   remove_column :receptions, :status
  end
end
