class AddDateColumnsToReceptions < ActiveRecord::Migration
  def self.up
   add_column :receptions, :expected_arrival_date, :timestamp
   add_column :receptions, :expected_reception_date, :timestamp
   add_column :receptions, :priority, :string
  end

  def self.down
   remove_column :receptions, :expected_arrival_date
   remove_column :receptions, :expected_reception_date
   remove_column :receptions, :priority
  end
end
