class AddReceptionFlags < ActiveRecord::Migration
 def self.up
   add_column :receptions, :data_intake_requested, :boolean
   add_column :receptions, :data_intake_ok, :boolean
   add_column :receptions, :data_intake_result_text, :text
   add_column :receptions, :validation_requested, :tinyint
   add_column :receptions, :validation_result_text, :text
  end

  def self.down
   remove_column :receptions, :data_intake_requested
   remove_column :receptions, :data_intake_ok
   remove_column :receptions, :data_intake_result_text
   remove_column :receptions, :validation_requested
   remove_column :receptions, :validation_result_text
  end

end
