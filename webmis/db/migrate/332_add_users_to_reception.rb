class AddUsersToReception < ActiveRecord::Migration
  def self.up
   add_column :receptions,:validated_by , :string
   add_column :receptions,:data_intake_by, :string
  end

  def self.down
   add_column :receptions,:validated_by 
   add_column :receptions,:data_intake_by
  end

end
