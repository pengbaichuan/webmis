class AddRulesetToReception < ActiveRecord::Migration
  def self.up
   add_column :receptions, :ruleset, :string
  end

  def self.down
   remove_column :receptions, :ruleset
  end
end
