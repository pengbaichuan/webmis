class CreateProductLocations < ActiveRecord::Migration
  def self.up
    create_table :product_locations do |t|
      t.string :product_id
      t.integer :conversion_id
      t.string :product_format
      t.string :area
      t.string :supplier
      t.string :data_release
      t.string :data_path
      t.string :conversiontool
      t.date :registration_date
      t.text :storage_path
      t.text :remarks

      t.timestamps
    end
  end

  def self.down
    drop_table :product_locations
  end
end
