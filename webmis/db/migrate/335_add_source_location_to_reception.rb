class AddSourceLocationToReception < ActiveRecord::Migration
  def self.up
   add_column :receptions , :source_location , :string
   add_column :receptions, :rejection_remark, :string
  end

  def self.down
   remove_column :receptions , :source_location 
   remove_column :receptions, :rejection_remark
  end
end
