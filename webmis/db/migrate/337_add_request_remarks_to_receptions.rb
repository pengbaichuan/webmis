class AddRequestRemarksToReceptions < ActiveRecord::Migration
  def self.up
   add_column :receptions, :request_remarks, :text
  end

  def self.down
   remove_column :receptions, :request_remarks
  end
end
