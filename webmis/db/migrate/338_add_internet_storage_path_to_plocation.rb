class AddInternetStoragePathToPlocation < ActiveRecord::Migration
  def self.up
   add_column :product_locations, :internal_storage_path, :text
  end

  def self.down
   remove_column :product_locations, :internal_storage_path
  end
end
