class AddResultFilesToConversion < ActiveRecord::Migration
  def self.up
   add_column :conversions, :result_files, :text
  end

  def self.down
   remove_column :conversions, :result_files
  end
end
