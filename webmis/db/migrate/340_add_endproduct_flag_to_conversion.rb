class AddEndproductFlagToConversion < ActiveRecord::Migration
  def self.up
   add_column :conversions, :end_product_yn, :string
  end

  def self.down
   remove_column :conversions, :end_product_yn
  end
end
