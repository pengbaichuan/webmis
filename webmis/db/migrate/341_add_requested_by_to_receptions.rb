class AddRequestedByToReceptions < ActiveRecord::Migration
  def self.up
   add_column :receptions, :requested_by, :string
  end

  def self.down
   remove_column :receptions, :requested_by
  end
end
