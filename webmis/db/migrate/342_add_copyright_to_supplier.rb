class AddCopyrightToSupplier < ActiveRecord::Migration
  def self.up
	add_column :company_abbrs,:copyright_name,:string
        add_column :conversions, :cloned_from_id, :integer
  end

  def self.down
        remove_column :company_abbrs,:copyright_name
        remove_column :conversions, :cloned_from_id
  end
end
