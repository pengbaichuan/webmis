class AddPasswordToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :pw, :string
  end

  def self.down
    remove_column :users, :pw
  end
end
