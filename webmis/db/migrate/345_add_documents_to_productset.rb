class AddDocumentsToProductset < ActiveRecord::Migration
  def self.up
 
  create_versioned_table "includedproductsetdocuments" do |t|
     t.column :productset_id,:integer
     t.column :document_version_id , :integer
  end 

  end 



  def self.down
   drop_versioned_table :includedproductsetdocuments
  end
end
