class ChangeDocumentBinarySize < ActiveRecord::Migration
  def self.up

    execute "alter table documents modify column binary_data longblob"
  end

  def self.down
    execute "alter table documents modify column binary_data blob"
  end
end
