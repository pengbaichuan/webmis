class AddAssemblyIdToConversion < ActiveRecord::Migration
  def self.up

   add_column :conversions, :assembly_id, :integer
  end

  def self.down
   add_column :conversions, :assembly_id, :integer
  end
end
