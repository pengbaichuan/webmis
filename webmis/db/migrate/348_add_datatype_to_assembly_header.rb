class AddDatatypeToAssemblyHeader < ActiveRecord::Migration
  def self.up
    add_column :assembly_headers,:output_format, :string
  end

  def self.down
    remove_column :assembly_headers,:output_format
  end
end
