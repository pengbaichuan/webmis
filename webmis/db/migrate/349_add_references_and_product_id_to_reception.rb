class AddReferencesAndProductIdToReception < ActiveRecord::Migration
  def self.up
    add_column :receptions, :references, :text
    add_column :receptions, :product_id, :string
  end

  def self.down
    remove_column :receptions, :references
    remove_column :receptions, :product_id
  end
end
