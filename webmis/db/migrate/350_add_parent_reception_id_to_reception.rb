class AddParentReceptionIdToReception < ActiveRecord::Migration
  def self.up
   add_column :receptions , :parent_id, :integer
  end

  def self.down
   remove_column :receptions , :parent_id
  end
end
