class CreateParentReceptions < ActiveRecord::Migration
  def self.up
    create_table :parent_receptions do |t|
      t.string :reception_id
      t.string :parent_reception_id
      t.string :type

      t.timestamps
    end
  end

  def self.down
    drop_table :parent_receptions
  end
end
