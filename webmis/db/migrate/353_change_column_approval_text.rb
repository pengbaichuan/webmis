class ChangeColumnApprovalText < ActiveRecord::Migration
  def self.up
   change_column :productsets,:approval_text, :text
   change_column :productset_versions,:approval_text, :text
  end

  def self.down
   change_column :productsets,:approval_text, :string
   change_column :productset_versions,:approval_text, :string
  end
end
