class AddConversionSettingsToProductsets < ActiveRecord::Migration
  def self.up
   add_column :productsets, :conversion_settings, :text
  end

  def self.down
   remove_column :productsets, :conversion_settings
  end
end
