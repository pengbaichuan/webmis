class AddConversionSettingsToProductsetVersions < ActiveRecord::Migration
  def self.up
    add_column :productset_versions, :conversion_settings, :text
  end

  def self.down
    add_column :productset_versions, :conversion_settings, :text
  end
end
