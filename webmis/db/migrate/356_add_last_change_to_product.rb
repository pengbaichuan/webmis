class AddLastChangeToProduct < ActiveRecord::Migration
  def self.up
     add_column :products, :removed_yn , :boolean
     add_column :product_versions, :removed_yn , :boolean
     add_column :products, :last_change , :string
     add_column :product_versions, :last_change , :string
  end

  def self.down
     add_column :products, :removed_yn 
     add_column :product_versions, :removed_yn
     add_column :products, :last_change 
     add_column :product_versions, :last_change
  end
end
