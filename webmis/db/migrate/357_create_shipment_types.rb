class CreateShipmentTypes < ActiveRecord::Migration

  def self.up
    create_table :shipment_types do |t|
      t.string :name
      t.timestamps
    end

    s = ShipmentType.new
    s.name = "Software"
    s.save


    s = ShipmentType.new
    s.name = "master"
    s.save


    s = ShipmentType.new
    s.name = "test"
    s.save
  end

  def self.down
    drop_table :shipment_types
  end

end
