class AddCategoryToDocuments < ActiveRecord::Migration
  def self.up
    add_column :documents, :category , :string
    add_column :document_versions, :category, :string
  end

  def self.down
    remove_column :documents, :category 
    remove_column :document_versions, :category
  end
end
