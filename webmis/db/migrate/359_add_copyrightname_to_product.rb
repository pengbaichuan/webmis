class AddCopyrightnameToProduct < ActiveRecord::Migration
  def self.up
    add_column :products, :copyright_name, :string
    add_column :product_versions, :copyright_name, :string

  end

  def self.down
    remove_column :products , :copyright_name
    remove_column :product_versions, :copyright_name
  end
end
