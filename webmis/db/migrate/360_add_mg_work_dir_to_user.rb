class AddMgWorkDirToUser < ActiveRecord::Migration
  def self.up
   add_column :users, :mg_work_dir, :string
  end

  def self.down
   remove_column :users, :mg_work_dir
  end
end
