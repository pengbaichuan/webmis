class CreateLicenses < ActiveRecord::Migration
  def self.up
    create_table :licenses do |t|
      t.string :licensenumber
      t.string :purpose
      t.string :active_yn
      t.timestamp :expiry_date
      t.text :comment

      t.timestamps
    end
  end

  def self.down
    drop_table :licenses
  end
end
