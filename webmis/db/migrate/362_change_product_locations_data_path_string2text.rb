class ChangeProductLocationsDataPathString2text < ActiveRecord::Migration
  def self.up
    change_column :product_locations, :data_path, :text
  end

  def self.down
    change_column :product_locations, :data_path, :string
  end
end
