class CreateFileTransferAccounts < ActiveRecord::Migration
  def self.up
    create_table :file_transfer_accounts do |t|
      t.string :transfer_type
      t.string :user_name
      t.string :password
      t.text :remarks
      t.integer :company_abbrs_id
      t.boolean :active
      t.string :address

      t.timestamps
    end
  end

  def self.down
    drop_table :file_transfer_accounts
  end
end
