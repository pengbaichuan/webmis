class AddMajorReleaseToProductdatasetInfo < ActiveRecord::Migration
  def self.up
    add_column :product_data_set_infos, :major_dataset, :tinyint
  end

  def self.down
    remove_column :product_data_set_infos, :major_dataset
  end
end
