class CreateProductCategories < ActiveRecord::Migration
  def self.up
    create_table :product_categories do |t|
      t.string :abbreviation
      t.text :description
      t.boolean :active
      t.timestamps
    end
    add_column :productsets, :product_category, :string
    add_column :productset_versions, :product_category, :string
  end

  def self.down
    drop_table :product_categories
    remove_column :productsets, :product_category
    remove_column :productset_versions, :product_category
  end
end
