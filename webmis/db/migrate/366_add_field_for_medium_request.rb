class AddFieldForMediumRequest < ActiveRecord::Migration
  def self.up
    add_column :stock, :request_date, :date
    add_column :stock, :requested_by, :string
    add_column :stock, :end_customer_name, :string
    add_column :stock, :recognition_file, :string
    add_column :stock, :nr_of_copies_request, :integer
    add_column :stock, :requested_delivery_date, :date
    add_column :stock, :actual_delivery_date, :date
    add_column :stock, :purpose, :string
    add_column :stock, :request_notes, :text
    add_column :stock, :status, :string
    add_column :stock, :ciq, :string
  end

  def self.down
    remove_column :stock, :request_date
    remove_column :stock, :requested_by
    remove_column :stock,  :end_customer_name
    remove_column :stock, :recognition_file
    remove_column :stock, :nr_of_copies_request
    remove_column :stock, :requested_delivery_date
    remove_column :stock, :actual_delivery_date
    remove_column :stock, :purpose
    remove_column :stock, :request_notes
    remove_column :stock, :status
    remove_column :stock, :ciq
  end
end
