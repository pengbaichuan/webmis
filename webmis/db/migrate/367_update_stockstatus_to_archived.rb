class UpdateStockstatusToArchived < ActiveRecord::Migration
  def self.up
    puts "Updating stock status..."
    Stock.connection.execute("update stock set status = 'ARCHIVED'")
    puts "Update stockstatus completed."
  end

  def self.down
  end
end
