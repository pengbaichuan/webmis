class AddSyncColumnsToDocument < ActiveRecord::Migration
  def self.up
    add_column :documents,:sync_filepath, :string
    add_column :documents,:last_modified_timestamp, :timestamp
    add_index(:documents, :sync_filepath)

  end

  def self.down
    remove_column :documents,:sync_filepath
    remove_column :documents,:last_modified_timestamp
    remove_index(:documents, :sync_filepath)
  end
end
