class CreateMailConditions < ActiveRecord::Migration
  def self.up
    create_table :mail_conditions do |t|
      t.string :field
      t.string :field_value

      t.timestamps
    end
  end

  def self.down
    drop_table :mail_conditions
  end
end
