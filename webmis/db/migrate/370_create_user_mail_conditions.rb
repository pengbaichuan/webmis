class CreateUserMailConditions < ActiveRecord::Migration
  def self.up
    create_table :user_mail_conditions do |t|
      t.integer :users_id
      t.integer :mail_conditions_id

      t.timestamps
    end
  end

  def self.down
    drop_table :user_mail_conditions
  end
end
