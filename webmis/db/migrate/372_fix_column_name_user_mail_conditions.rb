class FixColumnNameUserMailConditions < ActiveRecord::Migration
  def self.up
    rename_column :user_mail_conditions, :users_id, :users_mail_id
  end

  def self.down
   rename_column :user_mail_conditions, :users_mail_id, :users_id 
  end
end
