class AddSyncColumnsToDocumentVersion < ActiveRecord::Migration
  def self.up
    add_column :document_versions,:sync_filepath, :string
    add_column :document_versions,:last_modified_timestamp, :timestamp
    add_index(:document_versions, :sync_filepath)
  end

  def self.down
    remove_column :document_versions,:sync_filepath
    remove_column :document_versions,:last_modified_timestamp
    remove_index(:document_versions, :sync_filepath)
  end
end
