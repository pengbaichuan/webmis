class RenameDocumentVersionId < ActiveRecord::Migration
  def self.up
    rename_column :includeddocuments, :document_version_id, :document_id
    rename_column :includedproductsetdocuments, :document_version_id, :document_id
  end

  def self.down
    rename_column :includeddocuments, :document_id, :document_version_id
    rename_column :includedproductsetdocuments, :document_id, :document_version_id
  end
end
