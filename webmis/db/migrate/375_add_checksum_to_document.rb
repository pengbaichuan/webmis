class AddChecksumToDocument < ActiveRecord::Migration
  def self.up
   add_column :documents, :checksum, :string
   add_column :document_versions, :checksum, :string
  end

  def self.down
   remove_column :documents, :checksum, :string
   remove_column :document_versions, :checksum, :string
  end
end
