class AddConversionIdToStock < ActiveRecord::Migration
  def self.up
   add_column :stock, :conversion_id, :integer
   add_column :stock, :production_order_id, :integer
  end

  def self.down
   remove_column :stock, :conversion_id
   remove_column :stock, :production_order_id
  end
end
