class ChangeSourceLocationDatatype < ActiveRecord::Migration
  def self.up
    change_column :receptions, :source_location, :text
  end

  def self.down
    change_column :receptions, :source_location, :string
  end
end
