class CreateObjectDocumentations < ActiveRecord::Migration
  def self.up
    create_table :object_documentations do |t|
      t.string :object_name
      t.string :attribute_name
      t.text :short_description
      t.text :long_description

      t.timestamps
    end
  end

  def self.down
    drop_table :object_documentations
  end
end
