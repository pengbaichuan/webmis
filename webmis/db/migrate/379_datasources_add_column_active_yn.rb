class DatasourcesAddColumnActiveYn < ActiveRecord::Migration
  def self.up
    add_column(:datasources, :active_yn, :boolean)
  end

  def self.down
    remove_column(:datasources, :active_yn, :boolean)
  end
end
