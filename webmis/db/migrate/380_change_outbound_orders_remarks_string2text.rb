class ChangeOutboundOrdersRemarksString2text < ActiveRecord::Migration
  def self.up
    change_column :outbound_orders, :remarks, :text
  end

  def self.down
    change_column :outbound_orders, :remarks, :string
  end
end
