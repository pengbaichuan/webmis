class AddReceptionIdToProductLocation < ActiveRecord::Migration
  def self.up
    add_column :product_locations, :reception_id, :integer
  end

  def self.down
    remove_column :product_locations, :reception_id
  end
end
