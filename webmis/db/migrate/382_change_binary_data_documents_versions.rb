class ChangeBinaryDataDocumentsVersions < ActiveRecord::Migration
  def self.up
    change_column :document_versions, :binary_data, :longblob
  end

  def self.down
    change_column :document_versions, :binary_data, :binary
  end
end
