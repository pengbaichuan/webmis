class AddActiveYnMigration < ActiveRecord::Migration
  def self.up
   add_column :datasource_versions, :active_yn, :tinyint
  end

  def self.down
   remove_column :datasource_versions, :active_yn
  end
end
