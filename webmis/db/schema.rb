# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150717115452) do

  create_table "allocation_exceptions", force: :cascade do |t|
    t.integer  "conversion_id",  limit: 4
    t.string   "exception_type", limit: 255
    t.string   "scope",          limit: 255
    t.boolean  "removed_yn",     limit: 1
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "api_keys", force: :cascade do |t|
    t.string   "key",        limit: 255
    t.integer  "user_id",    limit: 4
    t.string   "owner",      limit: 255
    t.string   "sec_level",  limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "api_logs", force: :cascade do |t|
    t.string   "ip",         limit: 255
    t.string   "session",    limit: 255
    t.string   "user",       limit: 255
    t.string   "level",      limit: 255
    t.string   "controller", limit: 255
    t.string   "action",     limit: 255
    t.text     "message",    limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.text     "params",     limit: 65535
  end

  create_table "app_modules", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "abbr",        limit: 255
    t.string   "description", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "area_types", force: :cascade do |t|
    t.string "name",        limit: 255
    t.string "code",        limit: 255
    t.text   "description", limit: 65535
  end

  create_table "areas", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "abbr",        limit: 255
    t.string   "supplier_id", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "article_licenses", force: :cascade do |t|
    t.string   "label",               limit: 255
    t.decimal  "fee",                             precision: 8, scale: 2
    t.string   "description",         limit: 255
    t.integer  "high_level_bom_id",   limit: 4
    t.string   "external_contact_id", limit: 255
    t.boolean  "active_yn",           limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "currency",            limit: 255,                         default: "€"
  end

  create_table "articles", force: :cascade do |t|
    t.string   "customer_reference", limit: 255
    t.string   "name",               limit: 255
    t.string   "description",        limit: 255
    t.decimal  "price",                          precision: 8, scale: 3
    t.integer  "high_level_bom_id",  limit: 4
    t.boolean  "deleted",            limit: 1,                           default: false
    t.boolean  "active",             limit: 1,                           default: false
    t.integer  "created_by_user_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "supplier_reference", limit: 255
    t.decimal  "weight",                         precision: 8, scale: 2
    t.decimal  "volume",                         precision: 8, scale: 2
    t.string   "uom_weight",         limit: 255
    t.string   "uom_volume",         limit: 255
    t.decimal  "purchase_price",                 precision: 8, scale: 3
    t.string   "currency",           limit: 255,                         default: "€"
    t.string   "purchase_currency",  limit: 255,                         default: "€"
    t.boolean  "non_stockable",      limit: 1,                           default: false
  end

  create_table "assembly_headers", force: :cascade do |t|
    t.string   "product_id",    limit: 255
    t.integer  "version",       limit: 4
    t.text     "remarks",       limit: 65535
    t.string   "name",          limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "output_format", limit: 255
  end

  create_table "assembly_lines", force: :cascade do |t|
    t.integer  "assembly_header_id", limit: 4
    t.integer  "reception_id",       limit: 4
    t.string   "reference_id",       limit: 255
    t.integer  "company_abbr_id",    limit: 4
    t.integer  "data_release_id",    limit: 4
    t.string   "area",               limit: 255
    t.string   "file_name",          limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "data_release_name",  limit: 255
    t.string   "source_type",        limit: 255
    t.integer  "source_id",          limit: 4
  end

  add_index "assembly_lines", ["assembly_header_id"], name: "index_assembly_lines_on_assembly_header_id", using: :btree

  create_table "asset_type_number_ranges", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.integer  "start_range",   limit: 4
    t.integer  "end_range",     limit: 4
    t.integer  "asset_type_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "asset_types", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "gencode",     limit: 65535
  end

  create_table "assets", force: :cascade do |t|
    t.string   "ref_id",       limit: 255
    t.string   "requested_by", limit: 255
    t.string   "remarks",      limit: 255
    t.string   "assettype",    limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title",        limit: 255
    t.string   "suffix",       limit: 255
    t.integer  "document_id",  limit: 4
    t.string   "role",         limit: 255
  end

  add_index "assets", ["ref_id"], name: "index_assets_on_ref_id", unique: true, using: :btree

  create_table "bulgarian_translations", primary_key: "upcase_original", force: :cascade do |t|
    t.string  "original",   limit: 255, null: false
    t.string  "translated", limit: 255, null: false
    t.integer "data_type",  limit: 1,   null: false
  end

  add_index "bulgarian_translations", ["upcase_original", "data_type"], name: "term", using: :btree

  create_table "bundle_contents", force: :cascade do |t|
    t.integer  "conversiontool_id",      limit: 4
    t.integer  "cvtool_bundle_id",       limit: 4
    t.integer  "executable_id",          limit: 4
    t.string   "tool_name",              limit: 255
    t.string   "tool_release",           limit: 255
    t.boolean  "is_improvement_process", limit: 1,          default: false
    t.string   "name",                   limit: 255
    t.text     "description",            limit: 65535
    t.text     "input",                  limit: 65535
    t.text     "output",                 limit: 65535
    t.text     "parameters",             limit: 4294967295
    t.text     "toolspec_contents",      limit: 4294967295
    t.datetime "created_at",                                                null: false
    t.datetime "updated_at",                                                null: false
  end

  add_index "bundle_contents", ["executable_id", "cvtool_bundle_id"], name: "index_bundle_contents_on_executable_id_and_cvtool_bundle_id", using: :btree

  create_table "business_processes", force: :cascade do |t|
    t.string   "name",                   limit: 255
    t.text     "description",            limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "core_bp_yn",             limit: 1
    t.string   "header_link_controller", limit: 255
    t.string   "header_link_action",     limit: 255
    t.string   "line_link_controller",   limit: 255
    t.string   "line_link_action",       limit: 255
  end

  create_table "calls", force: :cascade do |t|
    t.integer  "callsession_id",      limit: 4
    t.integer  "poimatch_id",         limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "correct_name",        limit: 255
    t.string   "correct_streetname",  limit: 255
    t.string   "correct_housenumber", limit: 255
    t.string   "correct_phonenumber", limit: 255
    t.text     "remark",              limit: 65535
    t.string   "phonenumber_exists",  limit: 255
    t.string   "poi_exists",          limit: 255
    t.boolean  "called",              limit: 1
  end

  create_table "callsessions", force: :cascade do |t|
    t.string   "feature_code",    limit: 255
    t.string   "country_code",    limit: 255
    t.integer  "scenario_id",     limit: 4
    t.integer  "user_id",         limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "number_called",   limit: 4
    t.integer  "datasource_type", limit: 4
    t.integer  "number_of_calls", limit: 4
  end

  create_table "categories", force: :cascade do |t|
    t.string   "feature_name",           limit: 255
    t.string   "feature_type",           limit: 255
    t.integer  "feature_code_navteq",    limit: 4
    t.integer  "feature_code_teleatlas", limit: 4
    t.integer  "CEN_GDF_3",              limit: 4
    t.integer  "SVF_code",               limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "food_type",              limit: 1
    t.boolean  "brandname",              limit: 1
    t.boolean  "name_of_facility",       limit: 1
    t.boolean  "street_name",            limit: 1
    t.boolean  "house_number",           limit: 1
    t.boolean  "postal_code",            limit: 1
    t.boolean  "city_name",              limit: 1
    t.boolean  "country_name",           limit: 1
    t.boolean  "importance",             limit: 1
    t.boolean  "indicator",              limit: 1
    t.boolean  "phone_number",           limit: 1
    t.boolean  "fax_number",             limit: 1
    t.boolean  "deeplink",               limit: 1
    t.boolean  "email",                  limit: 1
    t.boolean  "invicinity",             limit: 1
    t.boolean  "fuel_type",              limit: 1
    t.boolean  "parking_size",           limit: 1
    t.boolean  "departure_times",        limit: 1
    t.boolean  "arrival_times",          limit: 1
    t.boolean  "picture",                limit: 1
    t.boolean  "population",             limit: 1
    t.boolean  "size",                   limit: 1
    t.boolean  "opening_times",          limit: 1
    t.boolean  "speed_limit",            limit: 1
    t.boolean  "edit_information",       limit: 1
    t.boolean  "price_indicator",        limit: 1
    t.integer  "parent_id",              limit: 4
    t.integer  "level",                  limit: 4
    t.text     "description",            limit: 65535
  end

  create_table "category_attributes", force: :cascade do |t|
    t.integer  "category_id",        limit: 4
    t.integer  "point_attribute_id", limit: 4
    t.integer  "description",        limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ciq_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cities", force: :cascade do |t|
    t.string   "iso2",         limit: 255
    t.string   "ascii_name",   limit: 255
    t.string   "city_name",    limit: 255
    t.string   "state_region", limit: 255
    t.integer  "population",   limit: 4
    t.float    "latitude",     limit: 24
    t.float    "longtitude",   limit: 24
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "cities", ["city_name", "iso2"], name: "index_cities_on_city_name_and_iso2", using: :btree
  add_index "cities", ["iso2"], name: "city_iso2", using: :btree

  create_table "commitment_change_reasons", force: :cascade do |t|
    t.string   "reason",          limit: 255
    t.text     "description",     limit: 65535
    t.string   "commitment_type", limit: 255
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "commitment_dates", force: :cascade do |t|
    t.integer  "production_order_id",         limit: 4
    t.string   "commitment_type",             limit: 255
    t.integer  "commitment_change_reason_id", limit: 4
    t.datetime "new_date"
    t.string   "updated_by",                  limit: 255
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  create_table "companies", force: :cascade do |t|
    t.string  "name",                        limit: 255
    t.string  "shipping_address_postalcode", limit: 255
    t.string  "shipping_address_street",     limit: 255
    t.string  "shipping_address_city",       limit: 255
    t.string  "shipping_address_state",      limit: 255
    t.string  "shipping_address_country",    limit: 255
    t.string  "billing_address_postalcode",  limit: 255
    t.string  "billing_address_street",      limit: 255
    t.string  "billing_address_city",        limit: 255
    t.string  "billing_address_state",       limit: 255
    t.string  "billing_address_country",     limit: 255
    t.string  "phone_office",                limit: 255
    t.string  "phone_fax",                   limit: 255
    t.integer "status",                      limit: 4
    t.string  "external_id",                 limit: 255
  end

  create_table "company_abbr_versions", force: :cascade do |t|
    t.integer  "company_abbr_id", limit: 4
    t.string   "company_name",    limit: 255
    t.string   "ms_abbr_3",       limit: 255
    t.string   "copyright_name",  limit: 255
    t.boolean  "is_active",       limit: 1
    t.integer  "version",         limit: 4
    t.string   "updated_by",      limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "dakota_id",       limit: 4
  end

  create_table "company_abbrs", force: :cascade do |t|
    t.string   "company_id",     limit: 255
    t.string   "company_name",   limit: 255
    t.string   "abbr",           limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "relation_type",  limit: 255
    t.string   "ms_abbr_2",      limit: 255
    t.string   "ms_abbr_3",      limit: 255
    t.boolean  "is_active",      limit: 1
    t.string   "copyright_name", limit: 255
    t.integer  "version",        limit: 4,   default: 0
    t.string   "updated_by",     limit: 255
    t.integer  "dakota_id",      limit: 4
  end

  create_table "config_parameter_versions", force: :cascade do |t|
    t.integer  "config_parameter_id", limit: 4
    t.integer  "version",             limit: 4
    t.text     "cfg_value",           limit: 4294967295
    t.string   "cfg_group",           limit: 255
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  create_table "config_parameters", force: :cascade do |t|
    t.string   "cfg_name",    limit: 255
    t.string   "cfg_type",    limit: 255
    t.text     "cfg_value",   limit: 4294967295
    t.text     "description", limit: 65535
    t.string   "cfg_group",   limit: 255
    t.integer  "version",     limit: 4,          default: 1
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.integer  "project_id",  limit: 4
  end

  add_index "config_parameters", ["cfg_name", "project_id"], name: "index_config_parameters_on_cfg_name_and_project_id", using: :btree

  create_table "content_release_notes", force: :cascade do |t|
    t.string  "title",                   limit: 255
    t.text    "content",                 limit: 65535
    t.integer "product_release_note_id", limit: 4
    t.integer "user_id",                 limit: 4
    t.integer "status",                  limit: 4
    t.integer "rank",                    limit: 4
    t.integer "subrank",                 limit: 4
    t.boolean "start_on_new_page",       limit: 1
    t.boolean "product_list",            limit: 1
    t.boolean "addition_required_yn",    limit: 1
    t.date    "created_at"
    t.date    "updated_at"
    t.integer "content_type",            limit: 4
    t.boolean "allow_jira_import_yn",    limit: 1
  end

  create_table "continents", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "code",        limit: 255
    t.string   "description", limit: 255
    t.boolean  "isactive",    limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contracts", force: :cascade do |t|
    t.date     "startdate"
    t.date     "enddate"
    t.string   "path",          limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "datasource_id", limit: 4
  end

  create_table "conversion_databases", force: :cascade do |t|
    t.integer  "conversion_id",           limit: 4
    t.string   "db_type",                 limit: 255
    t.string   "path_and_file",           limit: 255
    t.string   "filename",                limit: 255
    t.string   "path",                    limit: 255
    t.text     "remarks",                 limit: 65535
    t.string   "product_id",              limit: 255
    t.integer  "size_bytes",              limit: 8
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "conversion_job_id",       limit: 4
    t.integer  "production_orderline_id", limit: 4
    t.integer  "status",                  limit: 4
    t.integer  "group_amount",            limit: 4,          default: 1
    t.string   "group_by",                limit: 255
    t.string   "group_value",             limit: 255
    t.text     "removal_remark",          limit: 65535
    t.text     "stdout_log",              limit: 4294967295
    t.integer  "region_id",               limit: 4
    t.integer  "file_system_status",      limit: 4
  end

  add_index "conversion_databases", ["conversion_id"], name: "index_conversion_databases_on_conversion_id", using: :btree
  add_index "conversion_databases", ["conversion_job_id"], name: "index_conversion_databases_on_conversion_job_id", using: :btree
  add_index "conversion_databases", ["file_system_status", "conversion_id", "db_type"], name: "fs_status", using: :btree
  add_index "conversion_databases", ["path_and_file"], name: "index_conversion_databases_on_path_and_file", using: :btree
  add_index "conversion_databases", ["production_orderline_id"], name: "index_conversion_databases_on_production_orderline_id", using: :btree
  add_index "conversion_databases", ["status"], name: "index_conversion_databases_on_status", using: :btree

  create_table "conversion_environments", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.boolean  "active_yn",  limit: 1,     default: true
    t.text     "remarks",    limit: 65535
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  create_table "conversion_job_process_data_elements", force: :cascade do |t|
    t.integer  "conversion_job_id",       limit: 4
    t.integer  "process_data_element_id", limit: 4
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "data_set_version",        limit: 4
    t.boolean  "indirect",                limit: 1
  end

  add_index "conversion_job_process_data_elements", ["conversion_job_id", "process_data_element_id"], name: "conversion_job_id_process_data_element_id", using: :btree

  create_table "conversions", force: :cascade do |t|
    t.string   "request_by",                limit: 255
    t.date     "request_date"
    t.date     "conversion_date"
    t.string   "priority",                  limit: 255
    t.string   "supplier_id",               limit: 255
    t.integer  "datasource_id",             limit: 4
    t.integer  "data_release_id",           limit: 4
    t.string   "product",                   limit: 255
    t.text     "additional_info",           limit: 65535
    t.string   "request_data",              limit: 255
    t.string   "converter",                 limit: 255
    t.boolean  "tpd",                       limit: 1
    t.text     "data_files",                limit: 4294967295
    t.text     "reference_numbers",         limit: 65535
    t.text     "remarks",                   limit: 65535
    t.boolean  "right_data_check",          limit: 1
    t.boolean  "cvtool_check",              limit: 1
    t.string   "cvtoolenv",                 limit: 255
    t.float    "size_bytes",                limit: 24
    t.float    "size_mb",                   limit: 24
    t.string   "database_name",             limit: 255
    t.string   "productid",                 limit: 255
    t.string   "reception_reference",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "purpose",                   limit: 255
    t.string   "legacy_type",               limit: 255
    t.string   "ca_brn",                    limit: 255
    t.text     "tpd_products",              limit: 65535
    t.text     "tpd_rejected",              limit: 65535
    t.text     "tpd_rejection_remark",      limit: 65535
    t.string   "tpd_result_sender",         limit: 255
    t.integer  "region_id",                 limit: 4
    t.string   "region_name",               limit: 255
    t.string   "template_filename",         limit: 255
    t.integer  "status",                    limit: 4
    t.string   "server_name",               limit: 255
    t.integer  "production_orderline_id",   limit: 4
    t.string   "output_format",             limit: 255
    t.string   "input_format",              limit: 255
    t.text     "result_files",              limit: 65535
    t.string   "end_product_yn",            limit: 255
    t.integer  "cloned_from_id",            limit: 4
    t.integer  "assembly_id",               limit: 4
    t.text     "status_remark",             limit: 65535
    t.text     "rejection_remark",          limit: 65535
    t.integer  "cv_tool_template_id",       limit: 4
    t.integer  "conversiontool_id",         limit: 4
    t.integer  "cvtool_bundle_id",          limit: 4
    t.integer  "design_sequence",           limit: 4
    t.integer  "process_data_id",           limit: 4
    t.integer  "request_user_id",           limit: 4
    t.integer  "replaced_by_id",            limit: 4
    t.integer  "conversion_environment_id", limit: 4
    t.boolean  "outcome_persistent_yn",     limit: 1,          default: false
    t.boolean  "patch_request_yn",          limit: 1,          default: false
    t.integer  "force_create_id",           limit: 4
  end

  add_index "conversions", ["data_release_id"], name: "data_release_id_conversions", using: :btree

  create_table "conversiontools", force: :cascade do |t|
    t.string   "code",             limit: 255
    t.string   "description",      limit: 255
    t.boolean  "isactive",         limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "release",          limit: 255
    t.string   "tool_type",        limit: 255
    t.string   "major_release",    limit: 255
    t.text     "remarks",          limit: 65535
    t.boolean  "prod_released_yn", limit: 1
  end

  create_table "countries", force: :cascade do |t|
    t.string   "iso",            limit: 255
    t.string   "name",           limit: 255
    t.string   "printable_name", limit: 255
    t.string   "iso3",           limit: 255
    t.integer  "num_code",       limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "countries", ["name", "iso"], name: "index_countries_on_name_and_iso", using: :btree
  add_index "countries", ["name"], name: "index_countries_on_name", unique: true, using: :btree

  create_table "country_coverages", force: :cascade do |t|
    t.integer  "internet_datasource_id", limit: 4
    t.string   "country_code",           limit: 255
    t.integer  "amount_found",           limit: 4
    t.integer  "amount_expected",        limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "coverages", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.text     "remarks",     limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "status",      limit: 4
  end

  create_table "coverages_regions", force: :cascade do |t|
    t.integer  "coverage_id",        limit: 4
    t.integer  "region_id",          limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "do_not_process",     limit: 1
    t.text     "parameter_settings", limit: 65535
  end

  add_index "coverages_regions", ["coverage_id", "region_id"], name: "index_coverages_regions_on_coverage_id_and_region_id", using: :btree

  create_table "customer_content_definitions", force: :cascade do |t|
    t.string   "label",                             limit: 255
    t.integer  "output_target",                     limit: 4
    t.integer  "customer_content_specification_id", limit: 4
    t.integer  "product_line_id",                   limit: 4
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
  end

  create_table "customer_content_specification_versions", force: :cascade do |t|
    t.string   "name",                              limit: 255
    t.integer  "content_type",                      limit: 4
    t.text     "description",                       limit: 65535
    t.text     "generation_code",                   limit: 65535
    t.text     "validation_code",                   limit: 65535
    t.boolean  "is_active",                         limit: 1
    t.boolean  "mandatory",                         limit: 1
    t.boolean  "manual",                            limit: 1
    t.boolean  "for_product",                       limit: 1
    t.integer  "customer_content_specification_id", limit: 4
    t.string   "status",                            limit: 255
    t.integer  "version",                           limit: 4,     default: 0
    t.string   "bug_tracker_id",                    limit: 255
    t.text     "comment",                           limit: 65535
    t.string   "updated_by",                        limit: 255
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
  end

  create_table "customer_content_specifications", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.integer  "content_type",    limit: 4
    t.text     "description",     limit: 65535
    t.text     "generation_code", limit: 65535
    t.text     "validation_code", limit: 65535
    t.boolean  "is_active",       limit: 1,     default: true
    t.boolean  "mandatory",       limit: 1,     default: true
    t.boolean  "manual",          limit: 1,     default: false
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.boolean  "for_product",     limit: 1,     default: false
    t.integer  "version",         limit: 4
    t.string   "status",          limit: 255
    t.string   "bug_tracker_id",  limit: 255
    t.text     "comment",         limit: 65535
    t.string   "updated_by",      limit: 255
  end

  create_table "customer_content_values", force: :cascade do |t|
    t.integer  "customer_content_specification_id", limit: 4
    t.string   "customer_version",                  limit: 255
    t.date     "received_date"
    t.text     "value",                             limit: 65535
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  create_table "customer_contents", force: :cascade do |t|
    t.integer  "customer_content_specification_id", limit: 4
    t.integer  "production_orderline_id",           limit: 4
    t.text     "value",                             limit: 65535
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.integer  "product_id",                        limit: 4
  end

  create_table "customer_depots", force: :cascade do |t|
    t.string   "name",                            limit: 255
    t.string   "street_name",                     limit: 255
    t.string   "house_number",                    limit: 255
    t.string   "postal_code",                     limit: 255
    t.string   "city",                            limit: 255
    t.string   "country",                         limit: 255
    t.string   "unloading_point",                 limit: 255
    t.string   "billing_country",                 limit: 255
    t.string   "contact_name",                    limit: 255
    t.string   "constact_salutation",             limit: 255
    t.string   "contact_phone",                   limit: 255
    t.string   "contact_email",                   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "label",                           limit: 255
    t.string   "vat_identification",              limit: 255
    t.integer  "payment_term",                    limit: 4
    t.boolean  "ordering_yn",                     limit: 1
    t.boolean  "shipping_yn",                     limit: 1
    t.boolean  "invoicing_yn",                    limit: 1
    t.string   "customer_code",                   limit: 255
    t.string   "supplier_code",                   limit: 255
    t.integer  "vat_percentage",                  limit: 4
    t.text     "vat_statement",                   limit: 65535
    t.integer  "payment_statement_text_block_id", limit: 4
  end

  create_table "customer_documents", force: :cascade do |t|
    t.string   "customer_id",      limit: 255
    t.string   "customer_name",    limit: 255
    t.boolean  "PPF",              limit: 1
    t.boolean  "NDS",              limit: 1
    t.boolean  "TMC",              limit: 1
    t.boolean  "TPD_RN",           limit: 1
    t.boolean  "country_stats",    limit: 1
    t.boolean  "target_docs",      limit: 1
    t.boolean  "quality_doc",      limit: 1
    t.boolean  "CV_tool_RN",       limit: 1
    t.boolean  "errorlist",        limit: 1
    t.boolean  "data_supplier_RN", limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "customer_targets", force: :cascade do |t|
    t.string   "label",                          limit: 255
    t.integer  "output_target",                  limit: 4
    t.integer  "customer_content_id",            limit: 4
    t.integer  "customer_content_definition_id", limit: 4
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  create_table "cvtool_bundle_releases", force: :cascade do |t|
    t.integer  "conversiontool_id", limit: 4
    t.integer  "cvtool_bundle_id",  limit: 4
    t.boolean  "active_yn",         limit: 1, default: true
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  create_table "cvtool_bundles", force: :cascade do |t|
    t.string  "name",      limit: 255
    t.text    "remarks",   limit: 65535
    t.boolean "active_yn", limit: 1,     default: true
  end

  create_table "cvtool_templates", force: :cascade do |t|
    t.string   "name",                limit: 255
    t.string   "description",         limit: 255
    t.boolean  "isactive",            limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "template",            limit: 65535
    t.integer  "template_by_user_id", limit: 4
    t.string   "filename",            limit: 255
  end

  create_table "dashboard_grid_widgets", force: :cascade do |t|
    t.integer  "dashboard_widget_id", limit: 4
    t.integer  "dashboard_grid_id",   limit: 4
    t.integer  "position",            limit: 4
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.text     "widget_settings",     limit: 65535
  end

  create_table "dashboard_grids", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "dashboard_grids", ["user_id"], name: "index_dashboard_grids_on_user_id", using: :btree

  create_table "dashboard_widgets", force: :cascade do |t|
    t.string   "width_css_class", limit: 255
    t.string   "name",            limit: 255
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "refresh_time",    limit: 4
    t.text     "description",     limit: 65535
  end

  create_table "data_media", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.integer  "capacity",        limit: 4
    t.string   "unit_of_measure", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "data_releases", force: :cascade do |t|
    t.string   "name",                 limit: 255
    t.string   "release_code",         limit: 255
    t.integer  "datasource_id",        limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "active",               limit: 1
    t.boolean  "production",           limit: 1,   default: false
    t.integer  "company_abbr_id",      limit: 4
    t.string   "aliases",              limit: 255
    t.integer  "prev_data_release_id", limit: 4
  end

  add_index "data_releases", ["company_abbr_id", "name"], name: "index_data_releases_on_company_abbr_id_and_name", using: :btree

  create_table "data_set_group_lines", force: :cascade do |t|
    t.integer  "data_set_id",       limit: 4
    t.integer  "data_set_group_id", limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "data_set_groups", force: :cascade do |t|
    t.string   "company_abbr", limit: 255
    t.string   "data_release", limit: 255
    t.string   "region_code",  limit: 255
    t.string   "data_type",    limit: 255
    t.string   "filling",      limit: 255
    t.string   "suffix",       limit: 255
    t.text     "description",  limit: 65535
    t.integer  "status",       limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "data_set_status_logs", force: :cascade do |t|
    t.integer  "process_data_id", limit: 4
    t.string   "level",           limit: 255
    t.string   "user",            limit: 255
    t.integer  "version",         limit: 4
    t.string   "remarks",         limit: 255
    t.integer  "status",          limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "data_set_id",     limit: 4
  end

  create_table "data_set_versions", force: :cascade do |t|
    t.integer  "data_set_id",           limit: 4
    t.string   "name",                  limit: 255
    t.integer  "company_abbr_id",       limit: 4
    t.integer  "data_release_id",       limit: 4
    t.integer  "region_id",             limit: 4
    t.integer  "data_type_id",          limit: 4
    t.integer  "filling_id",            limit: 4
    t.text     "description",           limit: 65535
    t.text     "remarks",               limit: 65535
    t.integer  "coverage_id",           limit: 4
    t.integer  "version",               limit: 4
    t.integer  "status",                limit: 4
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "picked_reception_ids",  limit: 255
    t.integer  "poi_mapping_layout_id", limit: 4
    t.string   "updated_by",            limit: 255
  end

  create_table "data_sets", force: :cascade do |t|
    t.string   "name",                   limit: 255
    t.integer  "company_abbr_id",        limit: 4
    t.integer  "data_release_id",        limit: 4
    t.integer  "region_id",              limit: 4
    t.integer  "data_type_id",           limit: 4
    t.integer  "filling_id",             limit: 4
    t.text     "description",            limit: 65535
    t.text     "remarks",                limit: 65535
    t.integer  "coverage_id",            limit: 4
    t.integer  "version",                limit: 4
    t.integer  "status",                 limit: 4
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "picked_reception_ids",   limit: 255
    t.integer  "poi_mapping_layout_id",  limit: 4
    t.string   "updated_by",             limit: 255
    t.integer  "review_request_user_id", limit: 4
    t.integer  "review_user_id",         limit: 4
    t.text     "review_result",          limit: 65535
  end

  create_table "data_type_fillings", force: :cascade do |t|
    t.integer  "data_type_id", limit: 4
    t.integer  "filling_id",   limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "data_type_versions", force: :cascade do |t|
    t.integer  "data_type_id",                   limit: 4
    t.string   "name",                           limit: 255
    t.text     "description",                    limit: 65535
    t.boolean  "automatic_validation_yn",        limit: 1
    t.string   "updated_by",                     limit: 255
    t.integer  "version",                        limit: 4
    t.string   "bug_tracker_id",                 limit: 255
    t.text     "comment",                        limit: 65535
    t.string   "status",                         limit: 255
    t.boolean  "schedule_manual",                limit: 1,     default: true, null: false
    t.integer  "schedule_cores",                 limit: 4
    t.integer  "schedule_memory_base",           limit: 4
    t.integer  "schedule_memory_times_input",    limit: 4
    t.integer  "schedule_diskspace_base",        limit: 4
    t.integer  "schedule_diskspace_times_input", limit: 4
    t.boolean  "schedule_allow_virtual",         limit: 1
    t.boolean  "schedule_exclusive",             limit: 1
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
    t.string   "validation_executable",          limit: 255
  end

  create_table "data_types", force: :cascade do |t|
    t.string   "name",                           limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "isactive",                       limit: 1
    t.string   "description",                    limit: 255
    t.text     "rules",                          limit: 65535
    t.boolean  "is_intermediate",                limit: 1,     default: false
    t.boolean  "automatic_validation_yn",        limit: 1,     default: false
    t.string   "updated_by",                     limit: 255,   default: "0"
    t.integer  "version",                        limit: 4,     default: 0
    t.string   "bug_tracker_id",                 limit: 255
    t.text     "comment",                        limit: 65535
    t.string   "status",                         limit: 255,   default: "approved"
    t.boolean  "schedule_manual",                limit: 1,     default: true,       null: false
    t.integer  "schedule_cores",                 limit: 4,     default: 1
    t.integer  "schedule_memory_base",           limit: 4,     default: 0
    t.integer  "schedule_memory_times_input",    limit: 4,     default: 0
    t.integer  "schedule_diskspace_base",        limit: 4,     default: 0
    t.integer  "schedule_diskspace_times_input", limit: 4,     default: 1
    t.boolean  "schedule_allow_virtual",         limit: 1,     default: true
    t.boolean  "schedule_exclusive",             limit: 1,     default: false
    t.string   "validation_executable",          limit: 255
  end

  create_table "datasource_categories", force: :cascade do |t|
    t.integer  "datasource_id", limit: 4
    t.integer  "category_id",   limit: 4
    t.text     "remarks",       limit: 65535
    t.string   "site_category", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "datasource_internalcontacts", force: :cascade do |t|
    t.integer  "datasource_id", limit: 4
    t.integer  "user_id",       limit: 4
    t.string   "role",          limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "datasource_modules", force: :cascade do |t|
    t.integer  "datasource_id", limit: 4
    t.integer  "app_module_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "datasource_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "datasource_versions", force: :cascade do |t|
    t.integer  "datasource_id",   limit: 4
    t.integer  "version",         limit: 4
    t.string   "name",            limit: 255
    t.string   "note",            limit: 255
    t.string   "supplier_id",     limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "area_id",         limit: 4
    t.string   "copyright",       limit: 255
    t.integer  "datasource_type", limit: 4
    t.string   "purpose",         limit: 255
    t.integer  "coverage_id",     limit: 4
    t.string   "abbr",            limit: 255
    t.boolean  "production",      limit: 1,   default: false
    t.integer  "active_yn",       limit: 1
  end

  create_table "datasources", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.string   "note",            limit: 255
    t.string   "supplier_id",     limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "area_id",         limit: 4
    t.string   "copyright",       limit: 255
    t.integer  "datasource_type", limit: 4
    t.string   "purpose",         limit: 255
    t.integer  "coverage_id",     limit: 4
    t.integer  "version",         limit: 4
    t.string   "abbr",            limit: 255
    t.boolean  "production",      limit: 1,   default: false
    t.boolean  "active_yn",       limit: 1
  end

  create_table "default_content_release_notes", force: :cascade do |t|
    t.string  "title",                   limit: 255
    t.text    "content",                 limit: 65535
    t.integer "product_category_id",     limit: 4
    t.integer "user_id",                 limit: 4
    t.integer "status",                  limit: 4
    t.boolean "start_on_new_page",       limit: 1
    t.boolean "product_list",            limit: 1
    t.boolean "addition_required_yn",    limit: 1
    t.integer "rank",                    limit: 4
    t.integer "subrank",                 limit: 4
    t.boolean "use_customer_content_yn", limit: 1
    t.boolean "allow_jira_import_yn",    limit: 1
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   limit: 4,          default: 0
    t.integer  "attempts",   limit: 4,          default: 0
    t.text     "handler",    limit: 4294967295
    t.text     "last_error", limit: 65535
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by",  limit: 255
    t.string   "queue",      limit: 255
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "design_tag_hierarchies", id: false, force: :cascade do |t|
    t.integer "ancestor_id",   limit: 4, null: false
    t.integer "descendant_id", limit: 4, null: false
    t.integer "generations",   limit: 4, null: false
  end

  add_index "design_tag_hierarchies", ["ancestor_id", "descendant_id"], name: "index_design_tag_hierarchies_on_ancestor_id_and_descendant_id", unique: true, using: :btree
  add_index "design_tag_hierarchies", ["descendant_id"], name: "index_design_tag_hierarchies_on_descendant_id", using: :btree

  create_table "design_tags", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "description", limit: 255
    t.integer  "parent_id",   limit: 4
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.string   "tag_type",    limit: 255, default: "Other"
  end

  create_table "distribution_list_members", force: :cascade do |t|
    t.integer  "distribution_list_id",   limit: 4
    t.string   "external_contact_id",    limit: 255
    t.boolean  "electronic_delivery_yn", limit: 1
    t.boolean  "hard_copy_delivery_yn",  limit: 1
    t.integer  "hard_copy_amount",       limit: 4
    t.string   "transfer_method",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "distribution_lists", force: :cascade do |t|
    t.string   "name",                limit: 255
    t.boolean  "active_yn",           limit: 1
    t.boolean  "deleted_yn",          limit: 1
    t.text     "remarks",             limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "product_category_id", limit: 4
  end

  create_table "document_versions", force: :cascade do |t|
    t.integer  "document_id",             limit: 4
    t.string   "content_type",            limit: 255
    t.string   "filename",                limit: 255
    t.string   "description",             limit: 255
    t.binary   "binary_data",             limit: 4294967295
    t.integer  "version",                 limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "category",                limit: 255
    t.string   "sync_filepath",           limit: 255
    t.datetime "last_modified_timestamp"
    t.string   "checksum",                limit: 255
    t.string   "document_object_id",      limit: 255
    t.integer  "production_orderline_id", limit: 4
  end

  add_index "document_versions", ["document_id", "version"], name: "document_versions_document_id_version", using: :btree
  add_index "document_versions", ["document_id", "version"], name: "index_document_versions_on_document_id_and_version", using: :btree
  add_index "document_versions", ["sync_filepath"], name: "index_document_versions_on_sync_filepath", using: :btree

  create_table "documents", force: :cascade do |t|
    t.string   "content_type",            limit: 255
    t.string   "filename",                limit: 255
    t.string   "description",             limit: 255
    t.binary   "binary_data",             limit: 4294967295
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "version",                 limit: 4
    t.string   "category",                limit: 255
    t.string   "sync_filepath",           limit: 255
    t.datetime "last_modified_timestamp"
    t.string   "checksum",                limit: 255
    t.integer  "user_id",                 limit: 4
    t.string   "document_object_id",      limit: 255
    t.integer  "production_order_id",     limit: 4
    t.string   "package_directory",       limit: 255
    t.boolean  "mandatory_yn",            limit: 1
    t.integer  "production_orderline_id", limit: 4
  end

  add_index "documents", ["production_order_id"], name: "index_documents_on_production_order_id", using: :btree
  add_index "documents", ["sync_filepath"], name: "index_documents_on_sync_filepath", using: :btree

  create_table "environments", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "events", force: :cascade do |t|
    t.string   "message",          limit: 255
    t.integer  "datasource_id",    limit: 4
    t.integer  "point_id",         limit: 4
    t.integer  "scheduler_job_id", limit: 4
    t.text     "trace",            limit: 65535
    t.string   "level",            limit: 255
    t.string   "category",         limit: 255
    t.string   "user",             limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "related_data",     limit: 65535
    t.integer  "instance_id",      limit: 4
    t.integer  "app_module",       limit: 4
  end

  add_index "events", ["app_module", "instance_id"], name: "app_id_idx_instance_id_events", using: :btree
  add_index "events", ["created_at"], name: "index_events_on_created_at", using: :btree
  add_index "events", ["datasource_id"], name: "index_events_on_datasource_id", using: :btree
  add_index "events", ["instance_id"], name: "idx_instance_id_events", using: :btree
  add_index "events", ["point_id"], name: "index_events_on_point_id", using: :btree

  create_table "executable_versions", force: :cascade do |t|
    t.integer  "executable_id",                  limit: 4
    t.integer  "version",                        limit: 4
    t.string   "updated_by",                     limit: 255
    t.string   "name",                           limit: 255
    t.text     "description",                    limit: 65535
    t.text     "input",                          limit: 4294967295
    t.text     "output",                         limit: 4294967295
    t.text     "parameters",                     limit: 4294967295
    t.datetime "created_at",                                                        null: false
    t.datetime "updated_at",                                                        null: false
    t.string   "bug_tracker_id",                 limit: 255
    t.text     "comment",                        limit: 65535
    t.string   "status",                         limit: 255
    t.boolean  "schedule_manual",                limit: 1,          default: true,  null: false
    t.integer  "schedule_cores",                 limit: 4
    t.integer  "schedule_memory_base",           limit: 4
    t.integer  "schedule_memory_times_input",    limit: 4
    t.integer  "schedule_diskspace_base",        limit: 4
    t.integer  "schedule_diskspace_times_input", limit: 4
    t.boolean  "schedule_allow_virtual",         limit: 1
    t.boolean  "schedule_exclusive",             limit: 1
    t.boolean  "ignore_ds_regions_yn",           limit: 1,          default: false
  end

  add_index "executable_versions", ["executable_id", "status"], name: "index_executable_versions_on_executable_id_and_status", using: :btree

  create_table "executables", force: :cascade do |t|
    t.string   "name",                           limit: 255
    t.text     "description",                    limit: 65535
    t.text     "input",                          limit: 65535
    t.text     "output",                         limit: 65535
    t.text     "parameters",                     limit: 4294967295
    t.datetime "created_at",                                                        null: false
    t.datetime "updated_at",                                                        null: false
    t.text     "rules",                          limit: 65535
    t.string   "source_data_def_type",           limit: 255
    t.integer  "version",                        limit: 4
    t.string   "updated_by",                     limit: 255
    t.text     "metalogic",                      limit: 65535
    t.string   "subprocs",                       limit: 255
    t.string   "bug_tracker_id",                 limit: 255
    t.text     "comment",                        limit: 65535
    t.string   "status",                         limit: 255
    t.boolean  "schedule_manual",                limit: 1,          default: true,  null: false
    t.integer  "schedule_cores",                 limit: 4
    t.integer  "schedule_memory_base",           limit: 4
    t.integer  "schedule_memory_times_input",    limit: 4
    t.integer  "schedule_diskspace_base",        limit: 4
    t.integer  "schedule_diskspace_times_input", limit: 4
    t.boolean  "schedule_allow_virtual",         limit: 1
    t.boolean  "schedule_exclusive",             limit: 1
    t.boolean  "customer_visibility",            limit: 1,          default: true
    t.boolean  "is_improvement_process",         limit: 1,          default: false
    t.boolean  "ignore_ds_regions_yn",           limit: 1,          default: false
  end

  add_index "executables", ["name"], name: "index_executables_on_name", using: :btree

  create_table "external_contacts", force: :cascade do |t|
    t.string  "salutation",                 limit: 255
    t.string  "title",                      limit: 255
    t.string  "first_name",                 limit: 255
    t.string  "last_name",                  limit: 255
    t.string  "primary_address_street",     limit: 255
    t.string  "primary_address_postalcode", limit: 255
    t.string  "primary_address_city",       limit: 255
    t.string  "primary_address_state",      limit: 255
    t.string  "primary_address_country",    limit: 255
    t.string  "phone_work",                 limit: 255
    t.string  "phone_mobile",               limit: 255
    t.string  "email",                      limit: 255
    t.integer "status",                     limit: 4
    t.integer "company_id",                 limit: 4
    t.string  "external_id",                limit: 255
  end

  create_table "external_roles", force: :cascade do |t|
    t.integer "datasource_id",       limit: 4
    t.string  "external_contact_id", limit: 255
    t.string  "role",                limit: 255
  end

  create_table "features", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.integer  "code_navteq",    limit: 4
    t.integer  "code_teleatlas", limit: 4
    t.integer  "cen_gdf_code",   limit: 4
    t.integer  "svf_code",       limit: 4
    t.string   "feature_type",   limit: 255
    t.integer  "version",        limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "file_transfer_accounts", force: :cascade do |t|
    t.string   "transfer_type",    limit: 255
    t.string   "user_name",        limit: 255
    t.string   "password",         limit: 255
    t.text     "remarks",          limit: 65535
    t.integer  "company_abbrs_id", limit: 4
    t.boolean  "active",           limit: 1
    t.string   "address",          limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fillings", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.boolean  "active_yn",  limit: 1
    t.boolean  "removed_yn", limit: 1
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "group_rights", force: :cascade do |t|
    t.integer  "user_group_id",                            limit: 4
    t.boolean  "main_menu_datasources",                    limit: 1
    t.boolean  "main_menu_IC",                             limit: 1
    t.boolean  "main_menu_administration",                 limit: 1
    t.boolean  "datasources_details",                      limit: 1
    t.boolean  "datasources_edit_fields",                  limit: 1
    t.boolean  "datasource_jobs",                          limit: 1
    t.boolean  "datasource_IC",                            limit: 1
    t.boolean  "datasource_contracts",                     limit: 1
    t.boolean  "datasource_releases",                      limit: 1
    t.boolean  "datasource_EC",                            limit: 1
    t.boolean  "internal_contacts",                        limit: 1
    t.boolean  "administration_places",                    limit: 1
    t.boolean  "administration_datasource_types",          limit: 1
    t.boolean  "adminstration_coverages",                  limit: 1
    t.boolean  "administration_events",                    limit: 1
    t.boolean  "administration_categories",                limit: 1
    t.boolean  "administration_point_statistics",          limit: 1
    t.boolean  "administration_detailed_point_statistics", limit: 1
    t.boolean  "administration_management_statistics",     limit: 1
    t.boolean  "administration_user_rights",               limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "administration_ping_events",               limit: 1
  end

  create_table "high_level_boms", force: :cascade do |t|
    t.string   "revision",            limit: 255
    t.string   "internal_reference",  limit: 255
    t.integer  "product_id",          limit: 4
    t.integer  "product_location_id", limit: 4
    t.integer  "inlay_reception_id",  limit: 4
    t.integer  "label_reception_id",  limit: 4
    t.boolean  "deleted",             limit: 1,   default: false
    t.boolean  "active",              limit: 1,   default: false
    t.integer  "status",              limit: 4
    t.integer  "created_by_user_id",  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "inbound_orderlines", force: :cascade do |t|
    t.string   "drfmsid",           limit: 255
    t.string   "mediatype",         limit: 255
    t.string   "area",              limit: 255
    t.string   "dbrelease",         limit: 255
    t.string   "deliveryformat",    limit: 255
    t.string   "dbversion",         limit: 255
    t.integer  "inbound_order_id",  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "arrival_date"
    t.integer  "data_type_id",      limit: 4
    t.integer  "data_release_id",   limit: 4
    t.string   "data_release_name", limit: 255
  end

  create_table "inbound_orders", force: :cascade do |t|
    t.integer  "supplier_id",       limit: 4
    t.datetime "arrival_timestamp"
    t.string   "tracking_nr",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "orderref",          limit: 255
    t.string   "ordersubref",       limit: 255
    t.integer  "data_release_id",   limit: 4
  end

  create_table "included_article_license_purchase_orderlines", force: :cascade do |t|
    t.integer  "shipping_orderline_id", limit: 4
    t.integer  "article_license_id",    limit: 4
    t.integer  "purchase_orderline_id", limit: 4
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  create_table "included_database_test_requests", force: :cascade do |t|
    t.integer  "test_request_id",        limit: 4
    t.integer  "conversion_database_id", limit: 4
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.boolean  "test_yn",                limit: 1
    t.boolean  "ref_yn",                 limit: 1
  end

  create_table "includeddocument_versions", force: :cascade do |t|
    t.integer "product_id",          limit: 4
    t.integer "document_version_id", limit: 4
    t.integer "original_id",         limit: 4
    t.integer "version",             limit: 4
    t.boolean "deleted",             limit: 1, default: false, null: false
  end

  create_table "includeddocuments", force: :cascade do |t|
    t.integer "product_id",  limit: 4
    t.integer "document_id", limit: 4
  end

  create_table "includedproduct_versions", force: :cascade do |t|
    t.integer  "includedproduct_id", limit: 4
    t.integer  "version",            limit: 4
    t.integer  "productset_id",      limit: 4
    t.integer  "product_id",         limit: 4
    t.integer  "link_product_id",    limit: 4
    t.datetime "updated_at"
  end

  create_table "includedproducts", force: :cascade do |t|
    t.integer "productset_id",   limit: 4
    t.integer "product_id",      limit: 4
    t.integer "link_product_id", limit: 4
    t.integer "version",         limit: 4
  end

  add_index "includedproducts", ["product_id"], name: "index_includedproducts_on_product_id", using: :btree
  add_index "includedproducts", ["product_id"], name: "ip_includedproducts", using: :btree
  add_index "includedproducts", ["productset_id"], name: "dba_includedproducts_productset_id_idx", using: :btree
  add_index "includedproducts", ["productset_id"], name: "index_includedproducts_on_productset_id", using: :btree
  add_index "includedproducts", ["productset_id"], name: "ip_productset_id", using: :btree

  create_table "includedproductsetdocument_versions", force: :cascade do |t|
    t.integer "productset_id",       limit: 4
    t.integer "document_version_id", limit: 4
    t.integer "original_id",         limit: 4
    t.integer "version",             limit: 4
    t.boolean "deleted",             limit: 1, default: false, null: false
  end

  create_table "includedproductsetdocuments", force: :cascade do |t|
    t.integer "productset_id", limit: 4
    t.integer "document_id",   limit: 4
  end

  create_table "internal_contacts", force: :cascade do |t|
    t.string   "first_name",  limit: 255
    t.string   "middle_name", limit: 255
    t.string   "last_name",   limit: 255
    t.string   "phone",       limit: 255
    t.string   "email",       limit: 255
    t.string   "job_title",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "internet_datasource_versions", force: :cascade do |t|
    t.integer  "internet_datasource_id", limit: 4
    t.integer  "version",                limit: 4
    t.string   "location",               limit: 255
    t.text     "script",                 limit: 65535
    t.integer  "status",                 limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "scheduler_job_id",       limit: 4
    t.string   "name",                   limit: 255
    t.string   "processor_type",         limit: 255
    t.integer  "language_id",            limit: 4
    t.integer  "datasource_id",          limit: 4
    t.boolean  "add_manually",           limit: 1
    t.decimal  "average_conflvl1",                     precision: 12, scale: 5
    t.decimal  "average_conflvl2",                     precision: 12, scale: 5
    t.decimal  "average_matchlvl1",                    precision: 12, scale: 5
    t.decimal  "average_matchlvl2",                    precision: 12, scale: 5
    t.integer  "number_geomatched",      limit: 4
    t.integer  "category",               limit: 4
    t.integer  "rating",                 limit: 4
    t.boolean  "use_proxy",              limit: 1,                              default: false
    t.string   "host_last_run",          limit: 255
    t.datetime "date_last_run"
    t.integer  "priority",               limit: 4
    t.integer  "estimated_amount",       limit: 4
    t.string   "script_creator",         limit: 255
    t.string   "proxy_server",           limit: 255
    t.string   "proxy_port",             limit: 255
    t.integer  "high_priority",          limit: 4
  end

  create_table "internet_datasources", force: :cascade do |t|
    t.string   "location",          limit: 255
    t.text     "script",            limit: 65535
    t.integer  "status",            limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "scheduler_job_id",  limit: 4
    t.string   "name",              limit: 255
    t.string   "processor_type",    limit: 255
    t.integer  "language_id",       limit: 4
    t.integer  "datasource_id",     limit: 4
    t.boolean  "add_manually",      limit: 1
    t.decimal  "average_conflvl1",                precision: 12, scale: 5
    t.decimal  "average_conflvl2",                precision: 12, scale: 5
    t.decimal  "average_matchlvl1",               precision: 12, scale: 5
    t.decimal  "average_matchlvl2",               precision: 12, scale: 5
    t.integer  "number_geomatched", limit: 4
    t.integer  "version",           limit: 4
    t.integer  "category",          limit: 4
    t.integer  "rating",            limit: 4
    t.boolean  "use_proxy",         limit: 1,                              default: false
    t.string   "host_last_run",     limit: 255
    t.datetime "date_last_run"
    t.integer  "priority",          limit: 4
    t.integer  "estimated_amount",  limit: 4
    t.string   "script_creator",    limit: 255
    t.string   "proxy_server",      limit: 255
    t.string   "proxy_port",        limit: 255
    t.integer  "high_priority",     limit: 4
    t.boolean  "active_yn",         limit: 1
  end

  create_table "invoices", force: :cascade do |t|
    t.integer  "status",                   limit: 4
    t.string   "reference",                limit: 255
    t.text     "remarks",                  limit: 65535
    t.integer  "shipping_order_id",        limit: 4
    t.date     "date"
    t.date     "due_date"
    t.string   "currency",                 limit: 255
    t.integer  "vat_percentage",           limit: 4
    t.decimal  "vat",                                    precision: 13, scale: 3
    t.decimal  "total",                                  precision: 13, scale: 3
    t.datetime "created_at",                                                      null: false
    t.datetime "updated_at",                                                      null: false
    t.integer  "credit_invoice_parent_id", limit: 4
  end

  create_table "job_fail_reasons", force: :cascade do |t|
    t.string   "reason",      limit: 255
    t.text     "description", limit: 65535
    t.string   "job_type",    limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "job_types", force: :cascade do |t|
    t.integer  "server_id",  limit: 4
    t.string   "name",       limit: 255
    t.boolean  "allowed_yn", limit: 1
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "job_types", ["server_id"], name: "index_job_types_on_server_id", using: :btree

  create_table "job_versions", force: :cascade do |t|
    t.string   "conversion_id",                   limit: 255
    t.string   "run_server",                      limit: 255
    t.string   "run_command",                     limit: 255
    t.text     "run_script",                      limit: 65535
    t.string   "run_pid",                         limit: 255
    t.string   "working_dir",                     limit: 255
    t.datetime "start_time"
    t.datetime "finish_time"
    t.text     "stdout_log",                      limit: 4294967295
    t.integer  "exitcode",                        limit: 4
    t.text     "fail_reason",                     limit: 65535
    t.boolean  "schedule_manual",                 limit: 1
    t.integer  "schedule_cores",                  limit: 4
    t.integer  "schedule_memory",                 limit: 4
    t.integer  "schedule_diskspace",              limit: 4
    t.boolean  "schedule_allow_virtual",          limit: 1
    t.boolean  "schedule_exclusive",              limit: 1
    t.datetime "created_at",                                                      null: false
    t.datetime "updated_at",                                                      null: false
    t.integer  "status",                          limit: 4
    t.integer  "conversion_environment_id",       limit: 4
    t.integer  "lock_version",                    limit: 4
    t.integer  "reference_id",                    limit: 4
    t.string   "type",                            limit: 255
    t.string   "bug_tracker_id",                  limit: 255
    t.integer  "group_amount",                    limit: 4
    t.string   "group_by",                        limit: 255
    t.string   "group_value",                     limit: 255
    t.text     "action_log",                      limit: 65535
    t.string   "fail_category",                   limit: 255
    t.integer  "fail_category_logged_by_user_id", limit: 4
    t.date     "fail_category_logged_on"
    t.integer  "estimated_duration",              limit: 4
    t.integer  "result_dir_size_bytes",           limit: 8
    t.integer  "original_job_id",                 limit: 4
    t.integer  "project_id",                      limit: 4
    t.integer  "version",                         limit: 4
    t.integer  "job_id",                          limit: 4
    t.boolean  "merge_location",                  limit: 1
    t.string   "test_case_name",                  limit: 255
    t.integer  "test_run_id",                     limit: 4
    t.integer  "file_system_status",              limit: 4,          default: 10
  end

  create_table "jobresults", force: :cascade do |t|
    t.text     "raw_data",        limit: 65535
    t.text     "converted_data",  limit: 65535
    t.integer  "job_id",          limit: 4
    t.integer  "status",          limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "processing_time", limit: 24
    t.integer  "size",            limit: 4
    t.integer  "hashcode",        limit: 4
  end

  create_table "jobs", force: :cascade do |t|
    t.string   "conversion_id",                   limit: 255
    t.string   "run_server",                      limit: 255
    t.text     "run_command",                     limit: 65535
    t.text     "run_script",                      limit: 4294967295
    t.string   "run_pid",                         limit: 255
    t.string   "working_dir",                     limit: 255
    t.datetime "start_time"
    t.datetime "finish_time"
    t.text     "stdout_log",                      limit: 4294967295
    t.integer  "exitcode",                        limit: 4
    t.text     "fail_reason",                     limit: 65535
    t.boolean  "schedule_manual",                 limit: 1
    t.integer  "schedule_cores",                  limit: 4
    t.integer  "schedule_memory",                 limit: 4
    t.integer  "schedule_diskspace",              limit: 4
    t.boolean  "schedule_allow_virtual",          limit: 1
    t.boolean  "schedule_exclusive",              limit: 1
    t.datetime "created_at",                                                      null: false
    t.datetime "updated_at",                                                      null: false
    t.integer  "status",                          limit: 4
    t.integer  "conversion_environment_id",       limit: 4
    t.integer  "lock_version",                    limit: 4,          default: 1
    t.integer  "reference_id",                    limit: 4
    t.string   "type",                            limit: 255
    t.string   "bug_tracker_id",                  limit: 255
    t.integer  "group_amount",                    limit: 4,          default: 1
    t.string   "group_by",                        limit: 255
    t.string   "group_value",                     limit: 255
    t.text     "action_log",                      limit: 65535
    t.string   "fail_category",                   limit: 255
    t.integer  "fail_category_logged_by_user_id", limit: 4
    t.date     "fail_category_logged_on"
    t.integer  "estimated_duration",              limit: 4
    t.integer  "result_dir_size_bytes",           limit: 8
    t.integer  "original_job_id",                 limit: 4
    t.integer  "project_id",                      limit: 4
    t.integer  "version",                         limit: 4
    t.text     "scheduler_log",                   limit: 65535
    t.integer  "test_run_id",                     limit: 4
    t.string   "test_case_name",                  limit: 255
    t.boolean  "merge_location",                  limit: 1
    t.string   "reserved_path",                   limit: 255
    t.integer  "file_system_status",              limit: 4,          default: 10
  end

  add_index "jobs", ["conversion_id"], name: "index_jobs_on_conversion_id", using: :btree
  add_index "jobs", ["original_job_id"], name: "index_jobs_on_original_job_id", using: :btree
  add_index "jobs", ["reference_id"], name: "index_jobs_on_reference_id", using: :btree
  add_index "jobs", ["status"], name: "index_jobs_on_status", using: :btree
  add_index "jobs", ["type"], name: "index_jobs_on_type", using: :btree

  create_table "languages", force: :cascade do |t|
    t.string   "code",       limit: 255
    t.string   "part2b",     limit: 255
    t.string   "part2t",     limit: 255
    t.string   "part1",      limit: 255
    t.string   "scope",      limit: 255
    t.string   "lang_type",  limit: 255
    t.string   "ref_name",   limit: 255
    t.string   "comment",    limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "active_yn",  limit: 4
  end

  create_table "licenses", force: :cascade do |t|
    t.string   "licensenumber", limit: 255
    t.string   "purpose",       limit: 255
    t.string   "active_yn",     limit: 255
    t.datetime "expiry_date"
    t.text     "comment",       limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "locations", force: :cascade do |t|
    t.string   "location_code", limit: 255
    t.string   "reference",     limit: 255
    t.integer  "cd_no",         limit: 4
    t.integer  "cd_type",       limit: 4
    t.string   "borrby",        limit: 255
    t.string   "borron",        limit: 255
    t.string   "borrback",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "locationtype",  limit: 255
  end

  create_table "mail_conditions", force: :cascade do |t|
    t.string   "field",       limit: 255
    t.string   "field_value", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "mail_events", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "manufacturers", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "abbr",        limit: 255
    t.text     "description", limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "mapping_lines", force: :cascade do |t|
    t.integer  "mapping_id",              limit: 4
    t.string   "entity",                  limit: 255
    t.string   "target_attribute",        limit: 255
    t.string   "source_attribute",        limit: 255
    t.string   "constant",                limit: 255
    t.text     "block",                   limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "custom_target_attribute", limit: 1
    t.boolean  "custom_source_attribute", limit: 1
  end

  create_table "mappings", force: :cascade do |t|
    t.string   "name",                                           limit: 255
    t.text     "description",                                    limit: 65535
    t.string   "source_endpoint",                                limit: 255
    t.string   "destination_endpoint",                           limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "issues_filter_id",                               limit: 65535
    t.string   "source_adapter",                                 limit: 255
    t.string   "destination_adapter",                            limit: 255
    t.boolean  "active_yn",                                      limit: 1
    t.string   "source_login_name",                              limit: 255
    t.string   "source_login_password",                          limit: 255
    t.string   "destination_login_name",                         limit: 255
    t.string   "destination_login_password",                     limit: 255
    t.string   "http_authentication_source_login_name",          limit: 255
    t.string   "http_authentication_source_login_password",      limit: 255
    t.string   "http_authentication_destination_login_name",     limit: 255
    t.string   "http_authentication_destination_login_password", limit: 255
  end

  create_table "matchmasks", force: :cascade do |t|
    t.integer  "mask",        limit: 4
    t.string   "mask_string", limit: 255
    t.string   "description", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "mediatypes", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "ref",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "navdbfeature_categories", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "description", limit: 255
    t.boolean  "isactive",    limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "navdbfeature_types", force: :cascade do |t|
    t.string   "name",                     limit: 255
    t.string   "description",              limit: 255
    t.integer  "data_type_id",             limit: 4
    t.integer  "navdbfeature_category_id", limit: 4
    t.boolean  "isactive",                 limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "navdbfeature_versions", force: :cascade do |t|
    t.integer  "navdbfeature_id",       limit: 4
    t.integer  "version",               limit: 4
    t.integer  "navdb_feature_type_id", limit: 4
    t.datetime "dateeffective"
    t.string   "description1",          limit: 255
    t.string   "description2",          limit: 255
    t.string   "value",                 limit: 255
    t.string   "remarks",               limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "navdbfeatures", force: :cascade do |t|
    t.integer  "navdb_feature_type_id", limit: 4
    t.datetime "dateeffective"
    t.string   "description1",          limit: 255
    t.string   "description2",          limit: 255
    t.string   "value",                 limit: 255
    t.string   "remarks",               limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "version",               limit: 4
  end

  create_table "navigation_database_versions", force: :cascade do |t|
    t.integer  "navigation_database_id", limit: 4
    t.integer  "version",                limit: 4
    t.string   "name",                   limit: 255
    t.integer  "supplier_id",            limit: 4
    t.integer  "customer_id",            limit: 4
    t.string   "tag",                    limit: 255
    t.integer  "major",                  limit: 4
    t.integer  "minor",                  limit: 4
    t.datetime "dateeffective"
    t.string   "changecomment",          limit: 255
    t.string   "remarks",                limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "navigation_databases", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.integer  "supplier_id",   limit: 4
    t.integer  "customer_id",   limit: 4
    t.string   "tag",           limit: 255
    t.integer  "major",         limit: 4
    t.integer  "minor",         limit: 4
    t.datetime "dateeffective"
    t.string   "changecomment", limit: 255
    t.string   "remarks",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "version",       limit: 4
  end

  create_table "orderstatuslogs", force: :cascade do |t|
    t.integer  "production_order_id", limit: 4
    t.datetime "completion_date"
    t.string   "level",               limit: 255
    t.string   "user",                limit: 255
    t.integer  "sequence",            limit: 4
    t.string   "status",              limit: 255
    t.text     "remarks",             limit: 65535
    t.string   "reference",           limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ordertypes", force: :cascade do |t|
    t.string   "name",                limit: 255
    t.text     "description",         limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "product_category_id", limit: 4
    t.boolean  "no_pds_yn",           limit: 1
    t.boolean  "is_frontend",         limit: 1,     default: false
    t.boolean  "job_support_yn",      limit: 1,     default: false
    t.integer  "status",              limit: 4
  end

  create_table "outbound_orderlines", force: :cascade do |t|
    t.string   "map_no",                  limit: 255
    t.string   "cd_no",                   limit: 255
    t.integer  "amount",                  limit: 4
    t.string   "product_id",              limit: 255
    t.integer  "stock_id",                limit: 4
    t.string   "filename",                limit: 255
    t.string   "product_name",            limit: 255
    t.integer  "outbound_order_id",       limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "product_location_id",     limit: 4
    t.integer  "production_orderline_id", limit: 4
    t.integer  "parent_linenr",           limit: 4
  end

  add_index "outbound_orderlines", ["cd_no"], name: "oline_cd_no", using: :btree

  create_table "outbound_orders", force: :cascade do |t|
    t.string   "company_id",                        limit: 255
    t.date     "send_date"
    t.string   "contact_person",                    limit: 255
    t.string   "shipto_name",                       limit: 255
    t.string   "shipto_postalcode",                 limit: 255
    t.string   "shipto_streetname",                 limit: 255
    t.string   "shipto_country",                    limit: 255
    t.string   "send_by",                           limit: 255
    t.string   "map_no",                            limit: 255
    t.string   "cd_no",                             limit: 255
    t.string   "type",                              limit: 255
    t.text     "extra",                             limit: 65535
    t.string   "amount",                            limit: 255
    t.string   "filename",                          limit: 255
    t.string   "from_name",                         limit: 255
    t.text     "remarks",                           limit: 65535
    t.string   "courier",                           limit: 255
    t.boolean  "printed_alphatest_delivery",        limit: 1
    t.boolean  "printed_test_delivery",             limit: 1
    t.boolean  "printed_master_shipment",           limit: 1
    t.boolean  "printed_copy_master_shipment",      limit: 1
    t.boolean  "printed_release_notification",      limit: 1
    t.boolean  "printed_master_shipment_info",      limit: 1
    t.boolean  "printed_sample_approval",           limit: 1
    t.boolean  "printed_release_notification_info", limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "shipto_state",                      limit: 255
    t.string   "shipto_city",                       limit: 255
    t.string   "shipto_id",                         limit: 255
    t.string   "contact_id",                        limit: 255
    t.boolean  "release_mail_send",                 limit: 1
    t.integer  "distribution_list_member_id",       limit: 4
    t.date     "release_mail_send_date"
    t.integer  "file_transfer_account_id",          limit: 4
    t.integer  "shipping_parent_id",                limit: 4
    t.integer  "status",                            limit: 4,     default: 0
    t.integer  "distribution_list_id",              limit: 4
    t.integer  "shipping_specification_id",         limit: 4
    t.string   "sftp_subdirectory",                 limit: 255
    t.text     "mailing_remarks",                   limit: 65535
  end

  create_table "packing_lines", force: :cascade do |t|
    t.integer  "packing_id",            limit: 4
    t.integer  "outbound_orderline_id", limit: 4
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  create_table "packing_specification_versions", force: :cascade do |t|
    t.integer  "packing_specification_id",       limit: 4
    t.string   "name",                           limit: 255
    t.text     "description",                    limit: 65535
    t.text     "run_script",                     limit: 65535
    t.boolean  "manual_yn",                      limit: 1
    t.string   "updated_by",                     limit: 255
    t.integer  "version",                        limit: 4
    t.string   "bug_tracker_id",                 limit: 255
    t.text     "comment",                        limit: 65535
    t.string   "status",                         limit: 255
    t.boolean  "schedule_manual",                limit: 1,     default: true, null: false
    t.integer  "schedule_cores",                 limit: 4
    t.integer  "schedule_memory_base",           limit: 4
    t.integer  "schedule_memory_times_input",    limit: 4
    t.integer  "schedule_diskspace_base",        limit: 4
    t.integer  "schedule_diskspace_times_input", limit: 4
    t.boolean  "schedule_allow_virtual",         limit: 1
    t.boolean  "schedule_exclusive",             limit: 1
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
  end

  create_table "packing_specifications", force: :cascade do |t|
    t.string   "name",                           limit: 255
    t.text     "description",                    limit: 65535
    t.text     "run_script",                     limit: 65535
    t.boolean  "manual_yn",                      limit: 1
    t.string   "updated_by",                     limit: 255
    t.integer  "version",                        limit: 4
    t.string   "bug_tracker_id",                 limit: 255
    t.text     "comment",                        limit: 65535
    t.string   "status",                         limit: 255
    t.boolean  "schedule_manual",                limit: 1,     default: true, null: false
    t.integer  "schedule_cores",                 limit: 4
    t.integer  "schedule_memory_base",           limit: 4
    t.integer  "schedule_memory_times_input",    limit: 4
    t.integer  "schedule_diskspace_base",        limit: 4
    t.integer  "schedule_diskspace_times_input", limit: 4
    t.boolean  "schedule_allow_virtual",         limit: 1
    t.boolean  "schedule_exclusive",             limit: 1
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
  end

  create_table "packings", force: :cascade do |t|
    t.integer  "outbound_order_id",        limit: 4
    t.integer  "packing_specification_id", limit: 4
    t.string   "filename",                 limit: 255
    t.string   "path",                     limit: 255
    t.integer  "status",                   limit: 4
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "subdirectory",             limit: 255
    t.string   "md5_checksum",             limit: 255
  end

  create_table "parameter_setting_versions", force: :cascade do |t|
    t.integer  "parameter_setting_id",  limit: 4
    t.integer  "version",               limit: 4
    t.string   "updated_by",            limit: 255
    t.text     "parameterset",          limit: 4294967295
    t.integer  "design_tag_id",         limit: 4
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "status",                limit: 255
    t.string   "bug_tracker_id",        limit: 255
    t.text     "comment",               limit: 65535
    t.string   "name",                  limit: 255
    t.integer  "project_id",            limit: 4
    t.integer  "product_line_id",       limit: 4
    t.string   "sync_template_path",    limit: 255
    t.string   "sync_template_release", limit: 255
    t.string   "sync_template_name",    limit: 255
    t.datetime "last_sync_at"
  end

  create_table "parameter_settings", force: :cascade do |t|
    t.text     "parameterset",          limit: 4294967295
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.integer  "design_tag_id",         limit: 4
    t.integer  "version",               limit: 4,          default: 1
    t.string   "updated_by",            limit: 255
    t.string   "status",                limit: 255
    t.string   "bug_tracker_id",        limit: 255
    t.text     "comment",               limit: 65535
    t.string   "name",                  limit: 255
    t.integer  "parent_id",             limit: 4
    t.integer  "project_id",            limit: 4
    t.integer  "product_line_id",       limit: 4
    t.string   "sync_template_path",    limit: 255
    t.string   "sync_template_release", limit: 255
    t.string   "sync_template_name",    limit: 255
    t.datetime "last_sync_at"
  end

  create_table "parent_receptions", force: :cascade do |t|
    t.string   "reception_id",        limit: 255
    t.string   "parent_reception_id", limit: 255
    t.string   "type",                limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "parts", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "location",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ping_events", force: :cascade do |t|
    t.integer  "internet_datasource_id", limit: 4
    t.boolean  "is_online",              limit: 1
    t.datetime "updated"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "poi_mapping_layouts", force: :cascade do |t|
    t.integer  "supplier_company_abbr_id", limit: 4
    t.integer  "customer_company_abbr_id", limit: 4
    t.integer  "output_data_type_id",      limit: 4
    t.integer  "input_data_type_id",       limit: 4
    t.string   "definition_version",       limit: 255
    t.text     "description",              limit: 65535
    t.text     "import_hash",              limit: 65535
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "source_filename",          limit: 255
    t.integer  "coverage_id",              limit: 4
    t.binary   "org_binary_data",          limit: 65535
  end

  create_table "poi_mappings", force: :cascade do |t|
    t.integer  "poi_mapping_layout_id", limit: 4
    t.string   "map_or_filter_type",    limit: 255
    t.string   "map_or_filter_value",   limit: 255
    t.string   "logical_type",          limit: 255
    t.boolean  "invert",                limit: 1,     default: false
    t.integer  "group_id",              limit: 4
    t.integer  "output_category",       limit: 4
    t.boolean  "major_filtering",       limit: 1,     default: false
    t.text     "input_description",     limit: 65535
    t.text     "output_description",    limit: 65535
    t.text     "import_hash",           limit: 65535
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
  end

  create_table "point_attributes", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.string   "type",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "point_statistic_versions", force: :cascade do |t|
    t.integer  "point_statistic_id",     limit: 4
    t.integer  "version",                limit: 4
    t.integer  "amount",                 limit: 4
    t.float    "street_percentage",      limit: 24
    t.float    "postal_code_percentage", limit: 24
    t.float    "housenumber_percentage", limit: 24
    t.float    "city_percentage",        limit: 24
    t.float    "country_percentage",     limit: 24
    t.float    "food_type_percentage",   limit: 24
    t.float    "brandname_percentage",   limit: 24
    t.float    "x_percentage",           limit: 24
    t.float    "y_percentage",           limit: 24
    t.float    "activity_percentage",    limit: 24
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "internet_datasource_id", limit: 4
    t.integer  "category",               limit: 4
  end

  create_table "point_statistics", force: :cascade do |t|
    t.integer  "amount",                 limit: 4
    t.float    "street_percentage",      limit: 24
    t.float    "postal_code_percentage", limit: 24
    t.float    "housenumber_percentage", limit: 24
    t.float    "city_percentage",        limit: 24
    t.float    "country_percentage",     limit: 24
    t.float    "food_type_percentage",   limit: 24
    t.float    "brandname_percentage",   limit: 24
    t.float    "x_percentage",           limit: 24
    t.float    "y_percentage",           limit: 24
    t.float    "activity_percentage",    limit: 24
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "internet_datasource_id", limit: 4
    t.integer  "category",               limit: 4
    t.integer  "version",                limit: 4
  end

  create_table "point_versions", force: :cascade do |t|
    t.integer  "point_id",                limit: 4
    t.string   "name_of_facility",        limit: 255
    t.string   "iso_language_code",       limit: 255
    t.string   "category",                limit: 255
    t.float    "absolute_xcoordinate",    limit: 24
    t.float    "absolute_ycoordinate",    limit: 24
    t.integer  "importance",              limit: 4
    t.string   "brandname",               limit: 255
    t.string   "phone_number",            limit: 255
    t.string   "postal_code",             limit: 255
    t.string   "house_number",            limit: 255
    t.string   "street_name",             limit: 255
    t.string   "city_name",               limit: 255
    t.string   "country_name",            limit: 255
    t.string   "state_name",              limit: 255
    t.string   "vanity_city_name",        limit: 255
    t.float    "vanity_city_xcoordinate", limit: 24
    t.float    "vanity_city_ycoordinate", limit: 24
    t.string   "datasource",              limit: 255
    t.integer  "status",                  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "process_log",             limit: 255
    t.string   "unmapped_address",        limit: 255
    t.string   "deeplink",                limit: 255
    t.integer  "version",                 limit: 4
    t.datetime "checked_at"
    t.integer  "distance",                limit: 4
    t.float    "db1_coord_x",             limit: 24
    t.float    "db1_coord_y",             limit: 24
    t.integer  "db1_bestconflvl",         limit: 4
    t.integer  "db1_conflvl",             limit: 4
    t.integer  "db1_matchtech",           limit: 4
    t.string   "db1_urbrur",              limit: 255
    t.string   "db1_diglvl",              limit: 255
    t.string   "db1_attrlvl",             limit: 255
    t.string   "db1_roadtype",            limit: 255
    t.integer  "db1_matchlvl",            limit: 4
    t.integer  "db1_dist",                limit: 4
    t.string   "db1_cou",                 limit: 255
    t.string   "db1_citloc",              limit: 255
    t.string   "db1_cit",                 limit: 255
    t.string   "db1_strloc",              limit: 255
    t.string   "db1_str",                 limit: 255
    t.string   "db1_hnr",                 limit: 255
    t.float    "db2_coord_x",             limit: 24
    t.float    "db2_coord_y",             limit: 24
    t.integer  "db2_bestconflvl",         limit: 4
    t.integer  "db2_conflvl",             limit: 4
    t.integer  "db2_matchtech",           limit: 4
    t.string   "db2_urbrur",              limit: 255
    t.string   "db2_diglvl",              limit: 255
    t.string   "db2_attrlvl",             limit: 255
    t.string   "db2_roadtype",            limit: 255
    t.integer  "db2_matchlvl",            limit: 4
    t.integer  "db2_dist",                limit: 4
    t.string   "db2_cou",                 limit: 255
    t.string   "db2_citloc",              limit: 255
    t.string   "db2_cit",                 limit: 255
    t.string   "db2_strloc",              limit: 255
    t.string   "db2_str",                 limit: 255
    t.string   "db2_hnr",                 limit: 255
    t.datetime "gm_extract_date"
    t.datetime "gm_upload_date"
    t.integer  "internet_datasource_id",  limit: 4
    t.string   "site_id",                 limit: 255
    t.string   "facility_status",         limit: 255
    t.string   "food_type",               limit: 255
    t.datetime "session"
  end

  add_index "point_versions", ["point_id"], name: "index_point_versions_on_point_id", using: :btree

  create_table "points", force: :cascade do |t|
    t.string   "name_of_facility",        limit: 255
    t.string   "iso_language_code",       limit: 255
    t.string   "category",                limit: 255
    t.float    "absolute_xcoordinate",    limit: 24
    t.float    "absolute_ycoordinate",    limit: 24
    t.integer  "importance",              limit: 4
    t.string   "brandname",               limit: 255
    t.string   "phone_number",            limit: 255
    t.string   "postal_code",             limit: 255
    t.string   "house_number",            limit: 255
    t.string   "street_name",             limit: 255
    t.string   "city_name",               limit: 255
    t.string   "country_name",            limit: 255
    t.string   "state_name",              limit: 255
    t.string   "vanity_city_name",        limit: 255
    t.float    "vanity_city_xcoordinate", limit: 24
    t.float    "vanity_city_ycoordinate", limit: 24
    t.string   "datasource",              limit: 255
    t.integer  "status",                  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "process_log",             limit: 255
    t.string   "unmapped_address",        limit: 255
    t.string   "deeplink",                limit: 255
    t.integer  "version",                 limit: 4
    t.datetime "checked_at"
    t.integer  "distance",                limit: 4
    t.float    "db1_coord_x",             limit: 24
    t.float    "db1_coord_y",             limit: 24
    t.integer  "db1_bestconflvl",         limit: 4
    t.integer  "db1_conflvl",             limit: 4
    t.integer  "db1_matchtech",           limit: 4
    t.string   "db1_urbrur",              limit: 255
    t.string   "db1_diglvl",              limit: 255
    t.string   "db1_attrlvl",             limit: 255
    t.string   "db1_roadtype",            limit: 255
    t.integer  "db1_matchlvl",            limit: 4
    t.integer  "db1_dist",                limit: 4
    t.string   "db1_cou",                 limit: 255
    t.string   "db1_citloc",              limit: 255
    t.string   "db1_cit",                 limit: 255
    t.string   "db1_strloc",              limit: 255
    t.string   "db1_str",                 limit: 255
    t.string   "db1_hnr",                 limit: 255
    t.float    "db2_coord_x",             limit: 24
    t.float    "db2_coord_y",             limit: 24
    t.integer  "db2_bestconflvl",         limit: 4
    t.integer  "db2_conflvl",             limit: 4
    t.integer  "db2_matchtech",           limit: 4
    t.string   "db2_urbrur",              limit: 255
    t.string   "db2_diglvl",              limit: 255
    t.string   "db2_attrlvl",             limit: 255
    t.string   "db2_roadtype",            limit: 255
    t.integer  "db2_matchlvl",            limit: 4
    t.integer  "db2_dist",                limit: 4
    t.string   "db2_cou",                 limit: 255
    t.string   "db2_citloc",              limit: 255
    t.string   "db2_cit",                 limit: 255
    t.string   "db2_strloc",              limit: 255
    t.string   "db2_str",                 limit: 255
    t.string   "db2_hnr",                 limit: 255
    t.datetime "gm_extract_date"
    t.datetime "gm_upload_date"
    t.integer  "internet_datasource_id",  limit: 4
    t.datetime "last_activity_date"
    t.string   "country_code",            limit: 255
    t.string   "facility_status",         limit: 255
    t.string   "food_type",               limit: 255
    t.string   "site_id",                 limit: 255
    t.text     "details",                 limit: 65535
    t.datetime "session"
    t.string   "parent_name",             limit: 255
    t.integer  "parent_id",               limit: 4
    t.string   "parent_url",              limit: 255
    t.string   "parent_email_address",    limit: 255
    t.integer  "super_point_id",          limit: 4
    t.string   "fax_number",              limit: 255
    t.string   "email_address",           limit: 255
    t.datetime "released_at"
  end

  add_index "points", ["country_code", "category"], name: "cat_country", using: :btree
  add_index "points", ["datasource", "country_name", "city_name", "street_name"], name: "index_points_on_name_and_datasource_and_street_and_house_nr", using: :btree
  add_index "points", ["internet_datasource_id", "absolute_xcoordinate"], name: "index_points_on_internet_datasource_id_and_absolute_xcoordinate", using: :btree
  add_index "points", ["internet_datasource_id", "absolute_ycoordinate"], name: "index_points_on_internet_datasource_id_and_absolute_ycoordinate", using: :btree
  add_index "points", ["internet_datasource_id", "brandname"], name: "index_points_on_internet_datasource_id_and_brandname", using: :btree
  add_index "points", ["internet_datasource_id", "city_name"], name: "index_points_on_internet_datasource_id_and_city_name", using: :btree
  add_index "points", ["internet_datasource_id", "country_name"], name: "index_points_on_internet_datasource_id_and_country_name", using: :btree
  add_index "points", ["internet_datasource_id", "food_type"], name: "index_points_on_internet_datasource_id_and_food_type", using: :btree
  add_index "points", ["internet_datasource_id", "house_number"], name: "index_points_on_internet_datasource_id_and_house_number", using: :btree
  add_index "points", ["internet_datasource_id", "last_activity_date"], name: "index_points_on_internet_datasource_id_and_last_activity_date", using: :btree
  add_index "points", ["internet_datasource_id", "postal_code"], name: "index_points_on_internet_datasource_id_and_postal_code", using: :btree
  add_index "points", ["internet_datasource_id", "street_name"], name: "index_points_on_internet_datasource_id_and_street_name", using: :btree
  add_index "points", ["internet_datasource_id"], name: "index_points_on_internet_datasource_id", using: :btree

  create_table "points_export", force: :cascade do |t|
    t.string   "name_of_facility",        limit: 255
    t.string   "iso_language_code",       limit: 255
    t.string   "category",                limit: 255
    t.float    "absolute_xcoordinate",    limit: 24
    t.float    "absolute_ycoordinate",    limit: 24
    t.integer  "importance",              limit: 4
    t.string   "brandname",               limit: 255
    t.string   "phone_number",            limit: 255
    t.string   "postal_code",             limit: 255
    t.string   "house_number",            limit: 255
    t.string   "street_name",             limit: 255
    t.string   "city_name",               limit: 255
    t.string   "country_name",            limit: 255
    t.string   "state_name",              limit: 255
    t.string   "vanity_city_name",        limit: 255
    t.float    "vanity_city_xcoordinate", limit: 24
    t.float    "vanity_city_ycoordinate", limit: 24
    t.string   "datasource",              limit: 255
    t.integer  "status",                  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "process_log",             limit: 255
    t.string   "unmapped_address",        limit: 255
    t.string   "deeplink",                limit: 255
    t.integer  "version",                 limit: 4
    t.datetime "checked_at"
    t.integer  "distance",                limit: 4
    t.float    "db1_coord_x",             limit: 24
    t.float    "db1_coord_y",             limit: 24
    t.integer  "db1_bestconflvl",         limit: 4
    t.integer  "db1_conflvl",             limit: 4
    t.integer  "db1_matchtech",           limit: 4
    t.string   "db1_urbrur",              limit: 255
    t.string   "db1_diglvl",              limit: 255
    t.string   "db1_attrlvl",             limit: 255
    t.string   "db1_roadtype",            limit: 255
    t.integer  "db1_matchlvl",            limit: 4
    t.integer  "db1_dist",                limit: 4
    t.string   "db1_cou",                 limit: 255
    t.string   "db1_citloc",              limit: 255
    t.string   "db1_cit",                 limit: 255
    t.string   "db1_strloc",              limit: 255
    t.string   "db1_str",                 limit: 255
    t.string   "db1_hnr",                 limit: 255
    t.float    "db2_coord_x",             limit: 24
    t.float    "db2_coord_y",             limit: 24
    t.integer  "db2_bestconflvl",         limit: 4
    t.integer  "db2_conflvl",             limit: 4
    t.integer  "db2_matchtech",           limit: 4
    t.string   "db2_urbrur",              limit: 255
    t.string   "db2_diglvl",              limit: 255
    t.string   "db2_attrlvl",             limit: 255
    t.string   "db2_roadtype",            limit: 255
    t.integer  "db2_matchlvl",            limit: 4
    t.integer  "db2_dist",                limit: 4
    t.string   "db2_cou",                 limit: 255
    t.string   "db2_citloc",              limit: 255
    t.string   "db2_cit",                 limit: 255
    t.string   "db2_strloc",              limit: 255
    t.string   "db2_str",                 limit: 255
    t.string   "db2_hnr",                 limit: 255
    t.datetime "gm_extract_date"
    t.datetime "gm_upload_date"
    t.integer  "internet_datasource_id",  limit: 4
    t.datetime "last_activity_date"
    t.string   "country_code",            limit: 255
    t.string   "facility_status",         limit: 255
    t.string   "food_type",               limit: 255
    t.string   "site_id",                 limit: 255
  end

  add_index "points_export", ["datasource", "country_name", "city_name", "street_name"], name: "index_points_on_name_and_datasource_and_street_and_house_nr", using: :btree
  add_index "points_export", ["internet_datasource_id", "absolute_xcoordinate"], name: "index_points_on_internet_datasource_id_and_absolute_xcoordinate", using: :btree
  add_index "points_export", ["internet_datasource_id", "absolute_ycoordinate"], name: "index_points_on_internet_datasource_id_and_absolute_ycoordinate", using: :btree
  add_index "points_export", ["internet_datasource_id", "brandname"], name: "index_points_on_internet_datasource_id_and_brandname", using: :btree
  add_index "points_export", ["internet_datasource_id", "city_name"], name: "index_points_on_internet_datasource_id_and_city_name", using: :btree
  add_index "points_export", ["internet_datasource_id", "country_name"], name: "index_points_on_internet_datasource_id_and_country_name", using: :btree
  add_index "points_export", ["internet_datasource_id", "food_type"], name: "index_points_on_internet_datasource_id_and_food_type", using: :btree
  add_index "points_export", ["internet_datasource_id", "house_number"], name: "index_points_on_internet_datasource_id_and_house_number", using: :btree
  add_index "points_export", ["internet_datasource_id", "last_activity_date"], name: "index_points_on_internet_datasource_id_and_last_activity_date", using: :btree
  add_index "points_export", ["internet_datasource_id", "postal_code"], name: "index_points_on_internet_datasource_id_and_postal_code", using: :btree
  add_index "points_export", ["internet_datasource_id", "street_name"], name: "index_points_on_internet_datasource_id_and_street_name", using: :btree
  add_index "points_export", ["internet_datasource_id"], name: "index_points_on_internet_datasource_id", using: :btree

  create_table "postcodes", force: :cascade do |t|
    t.string   "code",         limit: 255
    t.string   "country_code", limit: 255
    t.string   "city_name",    limit: 255
    t.string   "street_name",  limit: 255
    t.string   "region",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "process_data", force: :cascade do |t|
    t.string   "name",             limit: 255
    t.string   "storage_location", limit: 255
    t.integer  "version",          limit: 4
    t.integer  "status",           limit: 4
    t.text     "description",      limit: 65535
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "updated_by",       limit: 255
    t.integer  "review_user_id",   limit: 4
    t.text     "review_remark",    limit: 65535
  end

  create_table "process_data_elements", force: :cascade do |t|
    t.integer  "process_data_item_id",          limit: 4
    t.integer  "reception_id",                  limit: 4
    t.integer  "process_data_specification_id", limit: 4
    t.text     "remark",                        limit: 65535
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
    t.boolean  "removed_yn",                    limit: 1,     default: false
    t.string   "split_by_value",                limit: 255
    t.string   "directory",                     limit: 255
    t.integer  "data_set_id",                   limit: 4
  end

  add_index "process_data_elements", ["process_data_item_id"], name: "index_process_data_elements_on_process_data_item_id", using: :btree
  add_index "process_data_elements", ["process_data_item_id"], name: "pde_process_data_id", using: :btree

  create_table "process_data_events", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "status",     limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "process_data_items", force: :cascade do |t|
    t.integer  "process_data_id",       limit: 4
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.string   "updated_by",            limit: 255
    t.text     "supplier_name",         limit: 65535
    t.text     "datatype",              limit: 65535
    t.text     "area_name",             limit: 65535
    t.text     "data_release",          limit: 65535
    t.text     "filling",               limit: 65535
    t.string   "picked_reception_ids",  limit: 255
    t.integer  "poi_mapping_layout_id", limit: 4
    t.integer  "data_set_id",           limit: 4
    t.integer  "data_set_version",      limit: 4
    t.boolean  "removed_yn",            limit: 1,     default: false
  end

  add_index "process_data_items", ["process_data_id"], name: "index_process_data_items_on_process_data_id", using: :btree
  add_index "process_data_items", ["process_data_id"], name: "pdi_process_data_id", using: :btree

  create_table "process_data_lines", force: :cascade do |t|
    t.integer  "process_data_id",           limit: 4
    t.integer  "process_data_sub_event_id", limit: 4
    t.integer  "reception_id",              limit: 4
    t.integer  "user_id",                   limit: 4
    t.integer  "relation_line_id",          limit: 4
    t.integer  "type_relation",             limit: 4
    t.text     "log_forward",               limit: 65535
    t.text     "log_backward",              limit: 65535
    t.integer  "document_id",               limit: 4
    t.integer  "status",                    limit: 4
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "cr_reference",              limit: 255
    t.boolean  "auto_code",                 limit: 1
    t.string   "change_version",            limit: 255
    t.integer  "process_data_element_id",   limit: 4
    t.integer  "data_set_id",               limit: 4
  end

  create_table "process_data_specifications", force: :cascade do |t|
    t.string   "name",                   limit: 255
    t.string   "directory",              limit: 255
    t.string   "script_tag",             limit: 255
    t.string   "script_parameter",       limit: 255
    t.text     "description",            limit: 65535
    t.boolean  "directory_level_yn",     limit: 1
    t.boolean  "active_yn",              limit: 1
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "split_by_key",           limit: 255
    t.integer  "data_type_id",           limit: 4
    t.string   "filling",                limit: 255
    t.boolean  "include_in_all_regions", limit: 1
    t.boolean  "do_not_process_yn",      limit: 1
  end

  create_table "process_data_sub_events", force: :cascade do |t|
    t.string   "name",                  limit: 255
    t.integer  "status",                limit: 4
    t.integer  "process_data_event_id", limit: 4
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "process_transitions", force: :cascade do |t|
    t.integer  "ordertype_id",      limit: 4
    t.integer  "from_bp",           limit: 4
    t.integer  "to_bp",             limit: 4
    t.text     "validation",        limit: 65535
    t.string   "from_level",        limit: 255
    t.string   "to_level",          limit: 255
    t.string   "action_label",      limit: 255
    t.integer  "sequence",          limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "cascade_to_level",  limit: 255
    t.integer  "cascade_to_bp",     limit: 4
    t.string   "cascade_type",      limit: 255
    t.boolean  "succes_transition", limit: 1
  end

  create_table "product_categories", force: :cascade do |t|
    t.string   "abbreviation",        limit: 255
    t.text     "description",         limit: 65535
    t.boolean  "active",              limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "pds_import_file",     limit: 65535
    t.boolean  "attach_pds_in_rn_yn", limit: 1
  end

  create_table "product_content_item_versions", force: :cascade do |t|
    t.integer  "product_content_item_id",          limit: 4
    t.string   "product_content_specification_id", limit: 255
    t.integer  "source_data_type_id",              limit: 4
    t.integer  "source_data_release_id",           limit: 4
    t.integer  "source_region_code_id",            limit: 4
    t.string   "destination_region_code",          limit: 255
    t.integer  "version",                          limit: 4
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.string   "type",                             limit: 255
    t.string   "resource_identification",          limit: 255
    t.boolean  "incr_update_yn",                   limit: 1
    t.boolean  "regional_update_yn",               limit: 1
  end

  create_table "product_content_items", force: :cascade do |t|
    t.string   "product_content_specification_id", limit: 255
    t.integer  "source_data_type_id",              limit: 4
    t.integer  "source_data_release_id",           limit: 4
    t.integer  "source_region_code_id",            limit: 4
    t.string   "destination_region_code",          limit: 255
    t.integer  "version",                          limit: 4
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.string   "filling",                          limit: 255
    t.string   "type",                             limit: 255
    t.string   "resource_identification",          limit: 255
    t.boolean  "incr_update_yn",                   limit: 1
    t.boolean  "regional_update_yn",               limit: 1
    t.string   "resource_identification_iu",       limit: 255
  end

  create_table "product_content_specification_versions", force: :cascade do |t|
    t.integer  "product_content_specification_id", limit: 4
    t.string   "name",                             limit: 255
    t.string   "sync_filename",                    limit: 255
    t.integer  "version",                          limit: 4
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
  end

  create_table "product_content_specifications", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.string   "sync_filename", limit: 255
    t.integer  "version",       limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "product_data_set_infos", force: :cascade do |t|
    t.integer  "product_id",       limit: 4
    t.string   "description",      limit: 255
    t.text     "remarks",          limit: 65535
    t.integer  "datatype_id",      limit: 4
    t.string   "area_name",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",             limit: 255
    t.integer  "company_abbr_id",  limit: 4
    t.string   "data_release",     limit: 255
    t.string   "data_info",        limit: 255
    t.boolean  "major_dataset",    limit: 1
    t.string   "filling",          limit: 255
    t.integer  "coverage_id",      limit: 4
    t.integer  "data_set_id",      limit: 4
    t.integer  "data_set_version", limit: 4
  end

  add_index "product_data_set_infos", ["product_id"], name: "dba_product_data_set_infos_product_id_idx", using: :btree
  add_index "product_data_set_infos", ["product_id"], name: "index_product_data_set_infos_on_product_id", using: :btree

  create_table "product_design_versions", force: :cascade do |t|
    t.integer  "product_design_id",         limit: 4
    t.integer  "version",                   limit: 4
    t.string   "updated_by",                limit: 255
    t.integer  "design_tag_id",             limit: 4
    t.string   "name",                      limit: 255
    t.text     "description",               limit: 65535
    t.text     "model",                     limit: 4294967295
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.string   "status",                    limit: 255
    t.string   "bug_tracker_id",            limit: 255
    t.text     "comment",                   limit: 65535
    t.string   "design_type",               limit: 255
    t.string   "product_design_md5",        limit: 255
    t.integer  "project_id",                limit: 4
    t.integer  "parameter_setting_id",      limit: 4
    t.integer  "parameter_setting_version", limit: 4
  end

  create_table "product_designs", force: :cascade do |t|
    t.string   "name",                      limit: 255
    t.text     "description",               limit: 65535
    t.text     "model",                     limit: 4294967295
    t.datetime "created_at",                                               null: false
    t.datetime "updated_at",                                               null: false
    t.integer  "version",                   limit: 4
    t.string   "updated_by",                limit: 255
    t.integer  "design_tag_id",             limit: 4
    t.string   "bug_tracker_id",            limit: 255
    t.text     "comment",                   limit: 65535
    t.string   "status",                    limit: 255
    t.integer  "lock_version",              limit: 4,          default: 0
    t.integer  "region_id",                 limit: 4
    t.integer  "company_abbr_id",           limit: 4
    t.integer  "data_release_id",           limit: 4
    t.string   "design_type",               limit: 255
    t.string   "product_design_md5",        limit: 255
    t.integer  "project_id",                limit: 4
    t.integer  "parameter_setting_id",      limit: 4
    t.integer  "parameter_setting_version", limit: 4
  end

  create_table "product_feature_versions", force: :cascade do |t|
    t.integer  "product_feature_id",       limit: 4
    t.integer  "version",                  limit: 4
    t.string   "updated_by",               limit: 255
    t.string   "name",                     limit: 255
    t.text     "description",              limit: 65535
    t.string   "abbr_code",                limit: 255
    t.text     "system_translation",       limit: 4294967295
    t.string   "translation_type",         limit: 255
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.string   "bug_tracker_id",           limit: 255
    t.text     "comment",                  limit: 65535
    t.string   "status",                   limit: 255
    t.integer  "platform_setting_id",      limit: 4
    t.integer  "platform_setting_version", limit: 4
    t.integer  "product_line_id",          limit: 4
    t.integer  "project_id",               limit: 4
  end

  create_table "product_features", force: :cascade do |t|
    t.string   "name",                     limit: 255
    t.string   "abbr_code",                limit: 255
    t.text     "description",              limit: 65535
    t.text     "system_translation",       limit: 65535
    t.string   "translation_type",         limit: 255
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.integer  "version",                  limit: 4
    t.string   "updated_by",               limit: 255
    t.string   "bug_tracker_id",           limit: 255
    t.text     "comment",                  limit: 65535
    t.string   "status",                   limit: 255
    t.integer  "platform_setting_id",      limit: 4
    t.integer  "platform_setting_version", limit: 4
    t.integer  "product_line_id",          limit: 4
    t.integer  "project_id",               limit: 4
  end

  create_table "product_line_allowed_data", force: :cascade do |t|
    t.integer  "product_line_id", limit: 4
    t.integer  "company_abbr_id", limit: 4
    t.integer  "data_type_id",    limit: 4
    t.integer  "region_id",       limit: 4
    t.integer  "filling_id",      limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "product_line_users", force: :cascade do |t|
    t.integer  "user_id",         limit: 4
    t.integer  "product_line_id", limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "product_lines", force: :cascade do |t|
    t.string   "name",                      limit: 255
    t.string   "abbr",                      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "company_abbr_id",           limit: 4
    t.boolean  "is_active",                 limit: 1,   default: true
    t.integer  "shipping_specification_id", limit: 4
    t.integer  "parent_id",                 limit: 4
  end

  create_table "product_locations", force: :cascade do |t|
    t.string   "product_id",            limit: 255
    t.integer  "conversion_id",         limit: 4
    t.string   "product_format",        limit: 255
    t.string   "area",                  limit: 255
    t.string   "supplier",              limit: 255
    t.string   "data_release",          limit: 255
    t.text     "data_path",             limit: 65535
    t.string   "conversiontool",        limit: 255
    t.date     "registration_date"
    t.text     "storage_path",          limit: 65535
    t.text     "remarks",               limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "product_type",          limit: 255
    t.text     "internal_storage_path", limit: 65535
    t.integer  "reception_id",          limit: 4
    t.string   "checksum_storage_file", limit: 255
    t.integer  "release_sequence",      limit: 4
  end

  create_table "product_release_notes", force: :cascade do |t|
    t.integer "production_order_id",     limit: 4
    t.integer "user_id",                 limit: 4
    t.integer "status",                  limit: 4
    t.integer "document_id",             limit: 4
    t.integer "production_orderline_id", limit: 4
  end

  create_table "product_versions", force: :cascade do |t|
    t.integer  "product_id",               limit: 4
    t.integer  "version",                  limit: 4
    t.string   "name",                     limit: 255
    t.string   "customer_name",            limit: 255
    t.string   "customer_id",              limit: 255
    t.string   "volumeid",                 limit: 255
    t.datetime "dateeffective"
    t.text     "detailedcoverage",         limit: 65535
    t.text     "hwncoverage",              limit: 65535
    t.text     "thirdpartydata1",          limit: 65535
    t.text     "thirdpartydata2",          limit: 65535
    t.text     "tmclocation",              limit: 65535
    t.string   "tmcevent",                 limit: 255
    t.text     "remarks",                  limit: 65535
    t.text     "zip",                      limit: 65535
    t.text     "qxs",                      limit: 65535
    t.text     "speedlimits",              limit: 65535
    t.text     "namerotations",            limit: 65535
    t.string   "medium_name",              limit: 255
    t.string   "nds_name",                 limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "tpd_file",                 limit: 255
    t.text     "test_requirement",         limit: 65535
    t.integer  "lock_version",             limit: 4
    t.text     "conversion_settings",      limit: 65535
    t.boolean  "removed_yn",               limit: 1
    t.string   "last_change",              limit: 255
    t.string   "copyright_name",           limit: 255
    t.integer  "primary_coverage_id",      limit: 4
    t.integer  "secondary_coverage_id",    limit: 4
    t.integer  "status",                   limit: 4
    t.integer  "poi_mapping_layout_id",    limit: 4
    t.integer  "update_region_mapping_id", limit: 4
    t.integer  "parent_id",                limit: 4
    t.string   "created_from",             limit: 255
    t.string   "file_detect_regex",        limit: 255
    t.integer  "product_line_id",          limit: 4
    t.integer  "predecessor_id",           limit: 4
    t.boolean  "import_disabled",          limit: 1
    t.integer  "product_design_id",        limit: 4
  end

  add_index "product_versions", ["product_id", "version"], name: "index_product_versions_on_product_id_and_version", using: :btree
  add_index "product_versions", ["product_id"], name: "dba_product_versions_product_id_idx", using: :btree

  create_table "production_orderline_conversion_databases", force: :cascade do |t|
    t.integer  "production_orderline_id", limit: 4
    t.integer  "conversion_database_id",  limit: 4
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "production_orderlines", force: :cascade do |t|
    t.integer  "production_order_id",  limit: 4
    t.string   "product_id",           limit: 255
    t.string   "bp_name",              limit: 255
    t.integer  "quantity",             limit: 4
    t.text     "remarks",              limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "bp_seqnr",             limit: 4
    t.string   "owner",                limit: 255
    t.integer  "priority",             limit: 4
    t.text     "test_requirement",     limit: 65535
    t.string   "product_accepted",     limit: 255
    t.text     "customer_feedback",    limit: 65535
    t.text     "product_design",       limit: 4294967295
    t.text     "bp_message",           limit: 16777215
    t.integer  "design_sequence",      limit: 4
    t.integer  "chosen_conversion_id", limit: 4
    t.string   "created_from",         limit: 255
    t.integer  "parent_linenr",        limit: 4
    t.integer  "product_location_id",  limit: 4
    t.boolean  "force_create",         limit: 1,          default: false
    t.integer  "bp_status",            limit: 4
    t.integer  "product_line_id",      limit: 4
  end

  add_index "production_orderlines", ["product_id"], name: "index_production_orderlines_on_product_id", using: :btree
  add_index "production_orderlines", ["production_order_id"], name: "index_production_orderlines_on_production_order_id", using: :btree

  create_table "production_orders", force: :cascade do |t|
    t.integer  "productset_id",                limit: 4
    t.integer  "major",                        limit: 4
    t.integer  "minor",                        limit: 4
    t.string   "name",                         limit: 255
    t.string   "requestor",                    limit: 255
    t.string   "owner",                        limit: 255
    t.text     "comments",                     limit: 65535
    t.datetime "completion_date"
    t.datetime "baseline_commitment_date"
    t.integer  "priority",                     limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "ordertype_id",                 limit: 4
    t.integer  "bp_seqnr",                     limit: 4
    t.text     "test_requirement",             limit: 65535
    t.string   "datarelease_name",             limit: 255
    t.string   "conversiontool_name",          limit: 255
    t.boolean  "allow_planning",               limit: 1
    t.boolean  "allow_production",             limit: 1
    t.boolean  "allow_shipment",               limit: 1
    t.boolean  "on_hold",                      limit: 1
    t.string   "supplier_name",                limit: 255
    t.string   "region_name",                  limit: 255
    t.integer  "data_release_id",              limit: 4
    t.datetime "modified_commitment_date"
    t.date     "rel_notes_requested_date"
    t.integer  "rel_notes_requested_by_id",    limit: 4
    t.date     "rel_notes_send_date"
    t.integer  "rel_notes_send_by_id",         limit: 4
    t.integer  "distribution_list_id",         limit: 4
    t.string   "bp_failreason",                limit: 255
    t.string   "bp_name",                      limit: 255
    t.string   "bp_status",                    limit: 255
    t.boolean  "create_frontend_order_yn",     limit: 1
    t.boolean  "allocate_by_update_region_yn", limit: 1
    t.text     "process_transitions_snapshot", limit: 4294967295
    t.boolean  "use_test_versions_yn",         limit: 1
    t.boolean  "use_dakota_simulator_yn",      limit: 1,          default: false
    t.integer  "project_id",                   limit: 4
    t.date     "expiry_date"
    t.boolean  "expiry_notification_send",     limit: 1
    t.string   "test_strategy",                limit: 255
  end

  create_table "production_test_cases", force: :cascade do |t|
    t.string   "name",                limit: 255
    t.boolean  "create_process_data", limit: 1
    t.boolean  "use_daksim",          limit: 1
    t.string   "schedule",            limit: 255
    t.integer  "project_id",          limit: 4
    t.boolean  "remove_results",      limit: 1
    t.boolean  "remove_process_data", limit: 1
    t.string   "expire_time",         limit: 255
    t.string   "tool_select",         limit: 255
    t.boolean  "synchronous",         limit: 1
    t.integer  "design_id",           limit: 4
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.boolean  "is_frontend",         limit: 1
  end

  create_table "products", force: :cascade do |t|
    t.string   "name",                     limit: 255
    t.string   "customer_name",            limit: 255
    t.string   "customer_id",              limit: 255
    t.string   "volumeid",                 limit: 255
    t.datetime "dateeffective"
    t.text     "detailedcoverage",         limit: 65535
    t.text     "hwncoverage",              limit: 65535
    t.text     "thirdpartydata1",          limit: 65535
    t.text     "thirdpartydata2",          limit: 65535
    t.text     "tmclocation",              limit: 65535
    t.string   "tmcevent",                 limit: 255
    t.text     "remarks",                  limit: 65535
    t.text     "zip",                      limit: 65535
    t.text     "qxs",                      limit: 65535
    t.text     "speedlimits",              limit: 65535
    t.text     "namerotations",            limit: 65535
    t.string   "medium_name",              limit: 255
    t.string   "nds_name",                 limit: 255
    t.integer  "lock_version",             limit: 4
    t.integer  "version",                  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "tpd_file",                 limit: 255
    t.text     "test_requirement",         limit: 65535
    t.text     "conversion_settings",      limit: 65535
    t.boolean  "removed_yn",               limit: 1
    t.string   "last_change",              limit: 255
    t.string   "copyright_name",           limit: 255
    t.integer  "primary_coverage_id",      limit: 4
    t.integer  "secondary_coverage_id",    limit: 4
    t.integer  "status",                   limit: 4
    t.integer  "poi_mapping_layout_id",    limit: 4
    t.integer  "update_region_mapping_id", limit: 4
    t.integer  "parent_id",                limit: 4
    t.string   "created_from",             limit: 255
    t.string   "file_detect_regex",        limit: 255
    t.integer  "product_line_id",          limit: 4
    t.integer  "predecessor_id",           limit: 4
    t.boolean  "import_disabled",          limit: 1
    t.integer  "product_design_id",        limit: 4
  end

  add_index "products", ["volumeid"], name: "index_products_on_volumeid", using: :btree

  create_table "productset_versions", force: :cascade do |t|
    t.integer  "productset_id",           limit: 4
    t.integer  "version",                 limit: 4
    t.string   "name",                    limit: 255
    t.string   "supplier_id",             limit: 255
    t.string   "customer_id",             limit: 255
    t.string   "firsttier_id",            limit: 255
    t.string   "customer_name",           limit: 255
    t.string   "firsttiername",           limit: 255
    t.string   "state",                   limit: 255
    t.integer  "major",                   limit: 4
    t.integer  "minor",                   limit: 4
    t.date     "dateeffective"
    t.text     "setchanges",              limit: 65535
    t.text     "remarks",                 limit: 65535
    t.string   "internal_name",           limit: 255
    t.string   "description",             limit: 255
    t.string   "owner",                   limit: 255
    t.integer  "region_id",               limit: 4
    t.integer  "datarelease_id",          limit: 4
    t.string   "datarelease_name",        limit: 255
    t.string   "platform_name",           limit: 255
    t.string   "ciq",                     limit: 255
    t.string   "nds_name",                limit: 255
    t.string   "area_name",               limit: 255
    t.string   "shippingmethod_name",     limit: 255
    t.date     "launchdate"
    t.string   "underconstructiondate",   limit: 255
    t.string   "conversiontool_name",     limit: 255
    t.string   "medium_name",             limit: 255
    t.text     "approval_text",           limit: 65535
    t.string   "status",                  limit: 255
    t.string   "supplier_name",           limit: 255
    t.string   "tpdspec",                 limit: 255
    t.string   "predecessor",             limit: 255
    t.string   "setcode",                 limit: 255
    t.string   "tmc_file",                limit: 255
    t.integer  "tmc_info_id",             limit: 4
    t.string   "tmc_info_name",           limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "tpd_file",                limit: 255
    t.string   "datasource_extrainfo",    limit: 255
    t.text     "test_requirement",        limit: 65535
    t.integer  "lock_version",            limit: 4
    t.string   "settype",                 limit: 255
    t.string   "areaabbr",                limit: 255
    t.string   "contabbr",                limit: 255
    t.boolean  "removed_yn",              limit: 1
    t.string   "output_format",           limit: 255
    t.string   "output_format_extrainfo", limit: 255
    t.boolean  "compressed_yn",           limit: 1
    t.boolean  "bundled_yn",              limit: 1
    t.boolean  "image_yn",                limit: 1
    t.boolean  "physical_medium_yn",      limit: 1
    t.text     "conversion_settings",     limit: 65535
    t.string   "set_release_major",       limit: 255
    t.string   "set_release_minor",       limit: 255
    t.integer  "product_category_id",     limit: 4
    t.integer  "company_abbr_id",         limit: 4
    t.string   "data_set_description",    limit: 255
  end

  add_index "productset_versions", ["productset_id", "version"], name: "index_productset_versions_on_productset_id_and_version", using: :btree

  create_table "productsets", force: :cascade do |t|
    t.string   "name",                    limit: 255
    t.string   "supplier_id",             limit: 255
    t.string   "customer_id",             limit: 255
    t.string   "firsttier_id",            limit: 255
    t.string   "customer_name",           limit: 255
    t.string   "firsttiername",           limit: 255
    t.string   "state",                   limit: 255
    t.integer  "major",                   limit: 4
    t.integer  "minor",                   limit: 4
    t.date     "dateeffective"
    t.text     "setchanges",              limit: 65535
    t.text     "remarks",                 limit: 65535
    t.string   "internal_name",           limit: 255
    t.string   "description",             limit: 255
    t.string   "owner",                   limit: 255
    t.integer  "region_id",               limit: 4
    t.integer  "datarelease_id",          limit: 4
    t.string   "datarelease_name",        limit: 255
    t.string   "platform_name",           limit: 255
    t.string   "ciq",                     limit: 255
    t.string   "nds_name",                limit: 255
    t.string   "area_name",               limit: 255
    t.string   "shippingmethod_name",     limit: 255
    t.date     "launchdate"
    t.string   "underconstructiondate",   limit: 255
    t.string   "conversiontool_name",     limit: 255
    t.string   "medium_name",             limit: 255
    t.text     "approval_text",           limit: 65535
    t.string   "status",                  limit: 255
    t.string   "supplier_name",           limit: 255
    t.string   "tpdspec",                 limit: 255
    t.string   "predecessor",             limit: 255
    t.string   "setcode",                 limit: 255
    t.string   "tmc_file",                limit: 255
    t.integer  "tmc_info_id",             limit: 4
    t.string   "tmc_info_name",           limit: 255
    t.integer  "lock_version",            limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "tpd_file",                limit: 255
    t.string   "datasource_extrainfo",    limit: 255
    t.integer  "version",                 limit: 4
    t.text     "test_requirement",        limit: 65535
    t.string   "settype",                 limit: 255
    t.string   "areaabbr",                limit: 255
    t.string   "contabbr",                limit: 255
    t.boolean  "removed_yn",              limit: 1
    t.string   "output_format",           limit: 255
    t.string   "output_format_extrainfo", limit: 255
    t.boolean  "compressed_yn",           limit: 1
    t.boolean  "bundled_yn",              limit: 1
    t.boolean  "image_yn",                limit: 1
    t.boolean  "physical_medium_yn",      limit: 1
    t.text     "conversion_settings",     limit: 65535
    t.string   "set_release_major",       limit: 255
    t.string   "set_release_minor",       limit: 255
    t.integer  "product_category_id",     limit: 4
    t.integer  "company_abbr_id",         limit: 4
    t.string   "data_set_description",    limit: 255
  end

  create_table "project_servers", force: :cascade do |t|
    t.integer  "project_id", limit: 4
    t.integer  "server_id",  limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "project_users", force: :cascade do |t|
    t.integer  "project_id", limit: 4
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "projects", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.text     "description",    limit: 65535
    t.integer  "status",         limit: 4
    t.integer  "environment_id", limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "purchase_orderlines", force: :cascade do |t|
    t.integer  "purchase_order_id", limit: 4
    t.integer  "quantity",          limit: 4
    t.decimal  "unit_price",                      precision: 8, scale: 2
    t.text     "item_description",  limit: 65535
    t.string   "item_reference",    limit: 255
    t.datetime "created_at",                                              null: false
    t.datetime "updated_at",                                              null: false
  end

  create_table "purchase_orders", force: :cascade do |t|
    t.string   "purchase_reference",  limit: 255
    t.date     "date_ordered"
    t.date     "date_paid"
    t.date     "date_received"
    t.integer  "status",              limit: 4
    t.text     "remarks",             limit: 65535
    t.boolean  "deleted",             limit: 1
    t.string   "external_contact_id", limit: 255
    t.integer  "user_id",             limit: 4
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "reception_lines", force: :cascade do |t|
    t.string   "path",         limit: 255
    t.string   "file",         limit: 255
    t.integer  "reception_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "receptions", force: :cascade do |t|
    t.integer  "datasource_id",            limit: 4
    t.integer  "data_release_id",          limit: 4
    t.integer  "data_medium_id",           limit: 4
    t.string   "dataversion",              limit: 255
    t.integer  "area_id",                  limit: 4
    t.string   "data_type_id",             limit: 255
    t.string   "referenceid",              limit: 255
    t.date     "reception_date"
    t.string   "purpose",                  limit: 255
    t.text     "remarks",                  limit: 65535
    t.text     "storage_location",         limit: 16777215
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "inbound_order_id",         limit: 4
    t.string   "area_type",                limit: 255
    t.boolean  "send_status",              limit: 1
    t.text     "sendto",                   limit: 65535
    t.datetime "senddate"
    t.string   "userid",                   limit: 255
    t.string   "user_name",                limit: 255
    t.string   "conversiontool",           limit: 255
    t.string   "region_name",              limit: 255
    t.boolean  "removed_yn",               limit: 1
    t.boolean  "validated_yn",             limit: 1
    t.string   "status",                   limit: 255
    t.datetime "expected_arrival_date"
    t.datetime "expected_reception_date"
    t.string   "priority",                 limit: 255
    t.boolean  "data_intake_requested",    limit: 1
    t.boolean  "data_intake_ok",           limit: 1
    t.text     "data_intake_result_text",  limit: 65535
    t.integer  "validation_requested",     limit: 1
    t.text     "validation_result_text",   limit: 65535
    t.string   "validated_by",             limit: 255
    t.string   "data_intake_by",           limit: 255
    t.string   "ruleset",                  limit: 255
    t.text     "source_location",          limit: 65535
    t.string   "rejection_remark",         limit: 255
    t.text     "request_remarks",          limit: 65535
    t.string   "requested_by",             limit: 255
    t.text     "references",               limit: 65535
    t.string   "product_id",               limit: 255
    t.integer  "parent_id",                limit: 4
    t.boolean  "can_be_removed_yn",        limit: 1
    t.boolean  "persistent_yn",            limit: 1
    t.string   "removal_mark_by_username", limit: 255
    t.string   "removed_by_username",      limit: 255
    t.integer  "data_set_id",              limit: 4
    t.integer  "data_set_version",         limit: 4
    t.integer  "filling_id",               limit: 4
  end

  add_index "receptions", ["area_id"], name: "area_id_reception", using: :btree
  add_index "receptions", ["data_release_id"], name: "index_receptions_on_data_release_id", using: :btree
  add_index "receptions", ["data_release_id"], name: "receptions_data_release_id", using: :btree

  create_table "region_hierarchies", id: false, force: :cascade do |t|
    t.integer "ancestor_id",   limit: 4, null: false
    t.integer "descendant_id", limit: 4, null: false
    t.integer "generations",   limit: 4, null: false
  end

  add_index "region_hierarchies", ["ancestor_id", "descendant_id"], name: "index_region_hierarchies_on_ancestor_id_and_descendant_id", unique: true, using: :btree
  add_index "region_hierarchies", ["descendant_id"], name: "index_region_hierarchies_on_descendant_id", using: :btree

  create_table "region_modules", force: :cascade do |t|
    t.integer  "region_id",     limit: 4
    t.integer  "app_module_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "region_relations", force: :cascade do |t|
    t.integer "region_id",       limit: 4
    t.integer "child_region_id", limit: 4
    t.string  "relation",        limit: 255
  end

  create_table "regions", force: :cascade do |t|
    t.string  "name",                 limit: 255
    t.text    "description",          limit: 65535
    t.integer "population",           limit: 4
    t.string  "continent_code",       limit: 255
    t.string  "country_iso_code",     limit: 255
    t.string  "country_iso3_code",    limit: 255
    t.string  "country_num_code",     limit: 255
    t.string  "city_iso_code",        limit: 255
    t.string  "city_ascii_name",      limit: 255
    t.string  "state_code",           limit: 255
    t.float   "latitude",             limit: 24
    t.float   "longitude",            limit: 24
    t.boolean "isactive",             limit: 1
    t.string  "area_code",            limit: 255
    t.string  "supplier_id",          limit: 255
    t.integer "display_type_id",      limit: 4
    t.string  "type",                 limit: 255
    t.string  "ruby_type",            limit: 255
    t.integer "parent_id",            limit: 4
    t.integer "region_id",            limit: 4
    t.integer "dakota_id",            limit: 4
    t.boolean "ignore_ds_regions_yn", limit: 1,     default: false
  end

  add_index "regions", ["name", "country_iso_code", "country_iso3_code"], name: "index_regions_on_name_and_country_iso_code_and_country_iso3_code", using: :btree

  create_table "release_note_attachments", force: :cascade do |t|
    t.string   "title",             limit: 255
    t.boolean  "active_yn",         limit: 1
    t.boolean  "removed_yn",        limit: 1
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "package_directory", limit: 255
    t.string   "description",       limit: 255
  end

  create_table "release_note_attachments_product_categories", force: :cascade do |t|
    t.integer  "release_note_attachment_id", limit: 4
    t.integer  "product_category_id",        limit: 4
    t.boolean  "mandatory_yn",               limit: 1
    t.boolean  "removed_yn",                 limit: 1
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "remove", id: false, force: :cascade do |t|
    t.string "fieldname", limit: 255
  end

  create_table "role_actions", force: :cascade do |t|
    t.integer  "role_id",         limit: 4
    t.string   "controller_name", limit: 255
    t.string   "action_name",     limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles_users", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "role_id",    limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "module",     limit: 255
    t.integer  "project_id", limit: 4
  end

  create_table "routes", force: :cascade do |t|
    t.string   "name_of_route",     limit: 255
    t.string   "description",       limit: 255
    t.text     "coordinates",       limit: 16777215
    t.text     "raw_data",          limit: 16777215
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "iso_language_code", limit: 255
    t.string   "datasource",        limit: 255
    t.integer  "version",           limit: 4
    t.string   "deeplink",          limit: 255
    t.datetime "checked_at"
  end

  create_table "scenarios", force: :cascade do |t|
    t.text     "query",              limit: 65535
    t.string   "name",               limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "name_of_poi",        limit: 1
    t.boolean  "streetname",         limit: 1
    t.boolean  "housenumber",        limit: 1
    t.boolean  "phonenumber",        limit: 1
    t.boolean  "phonenumber_exists", limit: 1
    t.boolean  "remark",             limit: 1
    t.boolean  "poi_exists",         limit: 1
  end

  create_table "scheduler_jobs", force: :cascade do |t|
    t.string   "name",                   limit: 255
    t.string   "schedule",               limit: 255
    t.string   "processor_type",         limit: 255
    t.integer  "status",                 limit: 4
    t.datetime "date_last_run"
    t.string   "dataprocessor_id",       limit: 255
    t.integer  "internet_datasource_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "contract_id",            limit: 4
    t.integer  "scheduler_id",           limit: 4
    t.integer  "syncstatus",             limit: 4
    t.string   "last_position",          limit: 255
  end

  create_table "scheduler_locks", force: :cascade do |t|
    t.string   "process",      limit: 255
    t.integer  "pid",          limit: 4
    t.string   "server",       limit: 255
    t.integer  "lock_version", limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "scheduler_locks", ["process"], name: "index_scheduler_locks_on_process", unique: true, using: :btree

  create_table "schema_info", id: false, force: :cascade do |t|
    t.integer "version", limit: 4
  end

  create_table "seq_ints", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.integer  "current_value", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "lock_version",  limit: 4
  end

  add_index "seq_ints", ["name"], name: "index_seq_ints_on_name", unique: true, using: :btree

  create_table "servers", force: :cascade do |t|
    t.string   "server_id",             limit: 255
    t.string   "server_remark",         limit: 255
    t.boolean  "active",                limit: 1
    t.string   "diskcapacity",          limit: 255
    t.string   "processor",             limit: 255
    t.text     "additionalprocess",     limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "down",                  limit: 1
    t.integer  "max_jobs",              limit: 4
    t.boolean  "scheduling_allowed_yn", limit: 1
    t.integer  "cores",                 limit: 4
    t.integer  "memory",                limit: 4
    t.string   "virtual_host",          limit: 255
    t.integer  "owned_by_project_id",   limit: 4
  end

  create_table "sessions", force: :cascade do |t|
    t.string   "session_id", limit: 255,   null: false
    t.text     "data",       limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", using: :btree
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at", using: :btree

  create_table "shipment_types", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "shipping_order_statuses", force: :cascade do |t|
    t.integer  "shipping_order_id",     limit: 4
    t.integer  "shipping_orderline_id", limit: 4
    t.string   "level",                 limit: 255
    t.integer  "user_id",               limit: 4
    t.integer  "status",                limit: 4
    t.text     "remarks",               limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "shipping_orderlines", force: :cascade do |t|
    t.integer  "shipping_order_id",   limit: 4
    t.integer  "stock_order_id",      limit: 4
    t.string   "call_off_reference",  limit: 255
    t.date     "delivery_date"
    t.integer  "amount",              limit: 4
    t.integer  "status",              limit: 4
    t.text     "remarks",             limit: 65535
    t.boolean  "deleted",             limit: 1
    t.integer  "article_id",          limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "extern_reference",    limit: 255
    t.string   "transport_reference", limit: 255
    t.integer  "lock_version",        limit: 4,     default: 0
    t.integer  "amount_requested",    limit: 4
    t.string   "invoice_reference",   limit: 255
    t.text     "invoice_remarks",     limit: 65535
    t.date     "payment_due_date"
    t.integer  "invoice_id",          limit: 4
  end

  create_table "shipping_orders", force: :cascade do |t|
    t.integer  "article_id",                  limit: 4
    t.string   "po_reference",                limit: 255
    t.integer  "status",                      limit: 4
    t.boolean  "invoice_send_yn",             limit: 1
    t.binary   "attachment_org",              limit: 65535
    t.string   "source",                      limit: 255
    t.integer  "initiated_by_user_id",        limit: 4
    t.boolean  "deleted",                     limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "lock_version",                limit: 4,     default: 0
    t.integer  "customer_depot_id",           limit: 4
    t.integer  "ordering_customer_depot_id",  limit: 4
    t.integer  "invoicing_customer_depot_id", limit: 4
    t.integer  "shipping_customer_depot_id",  limit: 4
  end

  create_table "shipping_specifications", force: :cascade do |t|
    t.string   "name",                               limit: 255
    t.string   "external_contact_id",                limit: 255
    t.integer  "packing_specification_id",           limit: 4
    t.integer  "distribution_list_id",               limit: 4
    t.string   "shipment_type",                      limit: 255
    t.string   "courier",                            limit: 255
    t.integer  "file_transfer_account_id",           limit: 4
    t.integer  "default_filename_specification",     limit: 4
    t.integer  "default_subdirectory_specification", limit: 4
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
  end

  create_table "space_reserved", force: :cascade do |t|
    t.string   "path",            limit: 255
    t.integer  "total_size",      limit: 8
    t.integer  "buffer_size",     limit: 8
    t.integer  "reserved_size",   limit: 8
    t.integer  "reserved_amount", limit: 4
    t.integer  "lock_version",    limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "states", force: :cascade do |t|
    t.string   "country_code", limit: 255
    t.string   "region_code",  limit: 255
    t.string   "name",         limit: 255
    t.string   "country_id",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "states", ["name", "country_code"], name: "index_states_on_name_and_country_code", using: :btree

  create_table "static_release_note_titles", force: :cascade do |t|
    t.string  "title",               limit: 255
    t.integer "status",              limit: 4
    t.boolean "required_yn",         limit: 1
    t.integer "product_category_id", limit: 4
  end

  create_table "statistic_versions", force: :cascade do |t|
    t.integer  "statistic_id", limit: 4
    t.integer  "version",      limit: 4
    t.integer  "coverage_id",  limit: 4
    t.string   "category",     limit: 255
    t.integer  "amount",       limit: 4
    t.integer  "datasources",  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "statistics", force: :cascade do |t|
    t.integer  "coverage_id", limit: 4
    t.string   "category",    limit: 255
    t.integer  "amount",      limit: 4
    t.integer  "datasources", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "version",     limit: 4
  end

  create_table "stats", id: false, force: :cascade do |t|
    t.string  "country_code",                                    limit: 255
    t.string  "feature_name",                                    limit: 255
    t.integer "count(*)",                                        limit: 8,                            default: 0, null: false
    t.integer "count(distinct internet_datasource_id)",          limit: 8,                            default: 0, null: false
    t.decimal "avg(rating)",                                                 precision: 14, scale: 4
    t.decimal "count(*)/count(distinct internet_datasource_id)",             precision: 24, scale: 4
  end

  create_table "stock", force: :cascade do |t|
    t.date     "creation_date"
    t.string   "commercial_name",         limit: 255
    t.string   "key_type",                limit: 255
    t.integer  "supplier_id",             limit: 4
    t.string   "supplier_name",           limit: 255
    t.string   "cd_type",                 limit: 255
    t.integer  "product_id",              limit: 4
    t.string   "product_name",            limit: 255
    t.integer  "data_release_id",         limit: 4
    t.string   "product_ref",             limit: 255
    t.string   "brn",                     limit: 255
    t.boolean  "tpd_phase_1",             limit: 1
    t.text     "tpd_phase_1_description", limit: 65535
    t.boolean  "tpd_phase_2",             limit: 1
    t.string   "tpd_phase_2_description", limit: 255
    t.boolean  "tpd_iba",                 limit: 1
    t.text     "tpd_iba_description",     limit: 65535
    t.integer  "cv_template_id",          limit: 4
    t.string   "cd_template_name",        limit: 255
    t.string   "databasename",            limit: 255
    t.text     "extra",                   limit: 65535
    t.boolean  "hwn",                     limit: 1
    t.boolean  "gsm",                     limit: 1
    t.boolean  "tmc",                     limit: 1
    t.string   "location",                limit: 255
    t.text     "rds_tmcfile",             limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "medium_number",           limit: 255
    t.string   "region_name",             limit: 255
    t.integer  "region_id",               limit: 4
    t.string   "location_type",           limit: 255
    t.string   "borrby",                  limit: 255
    t.string   "borron",                  limit: 255
    t.string   "borrback",                limit: 255
    t.date     "request_date"
    t.string   "requested_by",            limit: 255
    t.string   "end_customer_name",       limit: 255
    t.string   "recognition_file",        limit: 255
    t.integer  "nr_of_copies_request",    limit: 4
    t.date     "requested_delivery_date"
    t.date     "actual_delivery_date"
    t.string   "purpose",                 limit: 255
    t.text     "request_notes",           limit: 65535
    t.string   "status",                  limit: 255
    t.string   "ciq",                     limit: 255
    t.integer  "conversion_id",           limit: 4
    t.integer  "production_order_id",     limit: 4
    t.text     "status_remark",           limit: 65535
    t.integer  "production_orderline_id", limit: 4
  end

  create_table "stock_depots", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.string   "street_name",   limit: 255
    t.string   "postal_code",   limit: 255
    t.string   "city_name",     limit: 255
    t.string   "country_name",  limit: 255
    t.boolean  "active",        limit: 1
    t.boolean  "deleted",       limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "contact_name",  limit: 255
    t.string   "contact_email", limit: 255
  end

  create_table "stock_no_release", id: false, force: :cascade do |t|
    t.integer  "id",                      limit: 4,     default: 0, null: false
    t.date     "creation_date"
    t.string   "commercial_name",         limit: 255
    t.string   "key_type",                limit: 255
    t.integer  "supplier_id",             limit: 4
    t.string   "supplier_name",           limit: 255
    t.string   "cd_type",                 limit: 255
    t.integer  "product_id",              limit: 4
    t.string   "product_name",            limit: 255
    t.integer  "data_release_id",         limit: 4
    t.string   "product_ref",             limit: 255
    t.string   "brn",                     limit: 255
    t.boolean  "tpd_phase_1",             limit: 1
    t.text     "tpd_phase_1_description", limit: 65535
    t.boolean  "tpd_phase_2",             limit: 1
    t.string   "tpd_phase_2_description", limit: 255
    t.boolean  "tpd_iba",                 limit: 1
    t.text     "tpd_iba_description",     limit: 65535
    t.integer  "cv_template_id",          limit: 4
    t.string   "cd_template_name",        limit: 255
    t.string   "databasename",            limit: 255
    t.text     "extra",                   limit: 65535
    t.boolean  "hwn",                     limit: 1
    t.boolean  "gsm",                     limit: 1
    t.boolean  "tmc",                     limit: 1
    t.string   "location",                limit: 255
    t.text     "rds_tmcfile",             limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "medium_number",           limit: 255
    t.string   "region_name",             limit: 255
    t.integer  "region_id",               limit: 4
    t.string   "location_type",           limit: 255
    t.string   "borrby",                  limit: 255
    t.string   "borron",                  limit: 255
    t.string   "borrback",                limit: 255
  end

  create_table "stock_orderlines", force: :cascade do |t|
    t.integer  "stock_order_id",   limit: 4
    t.integer  "article_id",       limit: 4
    t.integer  "amount_order",     limit: 4
    t.integer  "amount_confirmed", limit: 4
    t.string   "stock_reference",  limit: 255
    t.text     "remarks",          limit: 65535
    t.integer  "status",           limit: 4
    t.boolean  "deleted",          limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "lock_version",     limit: 4,     default: 0
  end

  create_table "stock_orders", force: :cascade do |t|
    t.string   "purchase_reference", limit: 255
    t.date     "date_ordered"
    t.date     "confirmed_date"
    t.integer  "status",             limit: 4
    t.text     "remarks",            limit: 65535
    t.boolean  "deleted",            limit: 1,     default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "lock_version",       limit: 4,     default: 0
    t.integer  "depot_id",           limit: 4
  end

  create_table "sync_sessions", force: :cascade do |t|
    t.integer  "amount_created",    limit: 4
    t.integer  "amount_updated",    limit: 4
    t.string   "status",            limit: 255
    t.text     "errorlog",          limit: 4294967295
    t.string   "source_system",     limit: 255
    t.datetime "datetime_started"
    t.datetime "datetime_finished"
    t.string   "result_code",       limit: 255
    t.text     "remarks",           limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "mapping_id",        limit: 4
    t.integer  "amount_found",      limit: 4
    t.integer  "amount_unchanged",  limit: 4
    t.integer  "amount_failed",     limit: 4
  end

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id",        limit: 4
    t.integer  "taggable_id",   limit: 4
    t.string   "taggable_type", limit: 255
    t.datetime "created_at"
    t.integer  "user_id",       limit: 4
    t.string   "context",       limit: 255
    t.integer  "tagger_id",     limit: 4
    t.string   "tagger_type",   limit: 255
  end

  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
  add_index "taggings", ["tag_id", "taggable_type", "taggable_id", "context", "tagger_id", "tagger_type"], name: "tagging_idx", unique: true, using: :btree
  add_index "taggings", ["tag_id", "taggable_type"], name: "index_taggings_on_tag_id_and_taggable_type", using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree
  add_index "taggings", ["taggable_id", "taggable_type"], name: "index_taggings_on_taggable_id_and_taggable_type", using: :btree
  add_index "taggings", ["user_id", "tag_id", "taggable_type"], name: "index_taggings_on_user_id_and_tag_id_and_taggable_type", using: :btree
  add_index "taggings", ["user_id", "taggable_id", "taggable_type"], name: "index_taggings_on_user_id_and_taggable_id_and_taggable_type", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string  "name",           limit: 255
    t.integer "taggings_count", limit: 4,   default: 0, null: false
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree
  add_index "tags", ["taggings_count"], name: "index_tags_on_taggings_count", using: :btree

  create_table "test_area_configuration_versions", force: :cascade do |t|
    t.string   "name",                       limit: 255
    t.integer  "region_id",                  limit: 4
    t.integer  "product_line_id",            limit: 4
    t.integer  "nr_of_test_areas_x",         limit: 4
    t.integer  "nr_of_test_areas_y",         limit: 4
    t.decimal  "min_x",                                  precision: 8, scale: 3
    t.decimal  "max_x",                                  precision: 8, scale: 3
    t.decimal  "min_y",                                  precision: 8, scale: 3
    t.decimal  "max_y",                                  precision: 8, scale: 3
    t.integer  "version",                    limit: 4
    t.integer  "test_area_configuration_id", limit: 4
    t.datetime "created_at",                                                     null: false
    t.datetime "updated_at",                                                     null: false
  end

  create_table "test_area_configurations", force: :cascade do |t|
    t.string   "name",               limit: 255
    t.integer  "region_id",          limit: 4
    t.integer  "product_line_id",    limit: 4
    t.integer  "nr_of_test_areas_x", limit: 4
    t.integer  "nr_of_test_areas_y", limit: 4
    t.decimal  "min_x",                          precision: 8, scale: 3, default: 0.0
    t.decimal  "max_x",                          precision: 8, scale: 3, default: 0.0
    t.decimal  "min_y",                          precision: 8, scale: 3, default: 0.0
    t.decimal  "max_y",                          precision: 8, scale: 3, default: 0.0
    t.integer  "version",            limit: 4
    t.datetime "created_at",                                                           null: false
    t.datetime "updated_at",                                                           null: false
  end

  create_table "test_plan_versions", force: :cascade do |t|
    t.integer  "test_plan_id",    limit: 4
    t.string   "name",            limit: 255
    t.integer  "product_line_id", limit: 4
    t.text     "plan_details",    limit: 65535
    t.string   "status",          limit: 255
    t.integer  "version",         limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "updated_by",      limit: 255
  end

  create_table "test_plans", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.integer  "product_line_id", limit: 4
    t.text     "plan_details",    limit: 65535
    t.string   "status",          limit: 255
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "version",         limit: 4
    t.string   "updated_by",      limit: 255
  end

  create_table "test_request_job_conversion_databases", force: :cascade do |t|
    t.integer  "test_request_job_id",    limit: 4
    t.integer  "conversion_database_id", limit: 4
    t.string   "relation_type",          limit: 255
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "test_requests", force: :cascade do |t|
    t.integer  "production_orderline_id",         limit: 4
    t.integer  "test_type_id",                    limit: 4
    t.integer  "requested_by_user_id",            limit: 4
    t.integer  "assigned_to_user_id",             limit: 4
    t.integer  "test_db_conversion_databases_id", limit: 4
    t.integer  "ref_db_conversion_databases_id",  limit: 4
    t.string   "status",                          limit: 255
    t.string   "product_id",                      limit: 255
    t.date     "start_date"
    t.date     "finish_date"
    t.date     "actual_start_date"
    t.date     "actual_finish_date"
    t.text     "request_remarks",                 limit: 65535
    t.text     "result_remarks",                  limit: 65535
    t.boolean  "removed_yn",                      limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "test_plan_id",                    limit: 4
    t.boolean  "run_parallel",                    limit: 1
    t.boolean  "use_scheduler",                   limit: 1
    t.boolean  "run_locally",                     limit: 1
    t.integer  "project_id",                      limit: 4
    t.integer  "test_tool_release_id",            limit: 4
  end

  create_table "test_tool_releases", force: :cascade do |t|
    t.string   "release_name", limit: 255
    t.boolean  "active_yn",    limit: 1
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "test_types", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.boolean  "active_yn",   limit: 1
    t.boolean  "removed_yn",  limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "text_blocks", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.text     "content",    limit: 65535
    t.integer  "status",     limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "tmc_info_tables", force: :cascade do |t|
    t.integer  "tmc_info_id",  limit: 4
    t.integer  "tmc_table_id", limit: 4
    t.string   "version",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tmc_infos", force: :cascade do |t|
    t.string   "name",              limit: 255
    t.integer  "data_release_id",   limit: 4
    t.string   "data_release_name", limit: 255
    t.text     "description",       limit: 65535
    t.string   "approved_by",       limit: 255
    t.date     "approve_data"
    t.string   "approver_role",     limit: 255
    t.integer  "status",            limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tmc_tables", force: :cascade do |t|
    t.string   "country",       limit: 255
    t.integer  "country_id",    limit: 4
    t.integer  "supplier_id",   limit: 4
    t.string   "supplier_name", limit: 255
    t.string   "version",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "toolspec_featuregroups", force: :cascade do |t|
    t.string   "name",              limit: 255
    t.text     "description",       limit: 65535
    t.integer  "conversiontool_id", limit: 4
    t.integer  "cvtool_bundle_id",  limit: 4
    t.integer  "executable_id",     limit: 4
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  create_table "toolspec_parameter_values", force: :cascade do |t|
    t.string   "value",                 limit: 255
    t.text     "description",           limit: 65535
    t.integer  "toolspec_parameter_id", limit: 4
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "toolspec_parameters", force: :cascade do |t|
    t.string   "name",                     limit: 255
    t.text     "description",              limit: 65535
    t.integer  "toolspec_featuregroup_id", limit: 4
    t.string   "datatype",                 limit: 255
    t.string   "default",                  limit: 255
    t.boolean  "mandatory",                limit: 1
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  create_table "used_product_designs", force: :cascade do |t|
    t.integer  "product_design_id",       limit: 4
    t.integer  "used_product_design_id",  limit: 4
    t.string   "used_product_design_md5", limit: 255
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "version_active_from",     limit: 4
    t.integer  "version_active_until",    limit: 4
    t.integer  "status",                  limit: 4
  end

  create_table "user_mail_conditions", force: :cascade do |t|
    t.integer  "users_mail_id",      limit: 4
    t.integer  "mail_conditions_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_mails", force: :cascade do |t|
    t.integer  "user_id",       limit: 4
    t.integer  "mail_event_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_profiles", force: :cascade do |t|
    t.integer  "user_id",              limit: 4
    t.integer  "last_used_project_id", limit: 4
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "users", force: :cascade do |t|
    t.integer  "uidnumber",    limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "first_name",   limit: 255
    t.string   "last_name",    limit: 255
    t.string   "user_name",    limit: 255
    t.string   "phone",        limit: 255
    t.string   "email",        limit: 255
    t.string   "job_title",    limit: 255
    t.string   "phone_ext",    limit: 255
    t.string   "partner_code", limit: 255
    t.string   "pw",           limit: 255
    t.string   "mg_work_dir",  limit: 255
  end

  create_table "validation_errors", force: :cascade do |t|
    t.integer  "conversion_database_id", limit: 4
    t.string   "name",                   limit: 255
    t.integer  "count",                  limit: 4
    t.boolean  "copied_to_crash_yn",     limit: 1
    t.boolean  "resolved_yn",            limit: 1
    t.string   "ticket_nr",              limit: 255
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "version_numbers", force: :cascade do |t|
    t.datetime "created_at",             null: false
    t.string   "user",       limit: 255
  end

  create_table "versions", force: :cascade do |t|
    t.integer  "versionable_id",   limit: 4
    t.string   "versionable_type", limit: 255
    t.integer  "number",           limit: 4
    t.text     "yaml",             limit: 65535
    t.datetime "created_at"
  end

  add_index "versions", ["versionable_id", "versionable_type"], name: "index_versions_on_versionable_id_and_versionable_type", using: :btree

  create_table "view_receptions_and_conversiondbs", id: false, force: :cascade do |t|
    t.string  "source",               limit: 10,       default: "", null: false
    t.integer "id",                   limit: 4,        default: 0,  null: false
    t.text    "remarks",              limit: 65535
    t.text    "storage_location",     limit: 16777215
    t.integer "referenceid",          limit: 4
    t.integer "data_release_id",      limit: 8
    t.string  "datatype",             limit: 255
    t.string  "region_name",          limit: 255
    t.integer "area_id",              limit: 4
    t.integer "validation_requested", limit: 1
    t.integer "validated_yn",         limit: 1
    t.integer "uniqid",               limit: 4
  end

  create_table "warehouse_stock", force: :cascade do |t|
    t.integer  "org_id",                        limit: 4
    t.integer  "stock_order_id",                limit: 4
    t.integer  "stock_orderline_id",            limit: 4
    t.integer  "shipping_order_id",             limit: 4
    t.integer  "shipping_orderline_id",         limit: 4
    t.integer  "quantity",                      limit: 4
    t.integer  "org_quantity",                  limit: 4
    t.string   "stock_reference",               limit: 255
    t.integer  "article_id",                    limit: 4
    t.integer  "status",                        limit: 4
    t.text     "remarks",                       limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "stock_depot_id",                limit: 4
    t.integer  "lock_version",                  limit: 4,     default: 0
    t.integer  "warehouse_stock_adjustment_id", limit: 4
  end

  create_table "warehouse_stock_adjustments", force: :cascade do |t|
    t.integer  "article_id",                  limit: 4
    t.string   "stock_order_id",              limit: 255
    t.integer  "quantity",                    limit: 4
    t.text     "reason",                      limit: 65535
    t.integer  "related_stock_adjustment_id", limit: 4
    t.integer  "status",                      limit: 4
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  create_table "warehouse_stock_statuses", force: :cascade do |t|
    t.integer  "warehouse_stock_id", limit: 4
    t.integer  "user_id",            limit: 4
    t.integer  "status",             limit: 4
    t.text     "remarks",            limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
