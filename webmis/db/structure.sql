CREATE TABLE `allocation_exceptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `conversion_id` int(11) DEFAULT NULL,
  `exception_type` varchar(255) DEFAULT NULL,
  `scope` varchar(255) DEFAULT NULL,
  `removed_yn` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `api_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `sec_level` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE `api_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) DEFAULT NULL,
  `session` varchar(255) DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `controller` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `message` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `params` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19631 DEFAULT CHARSET=utf8;

CREATE TABLE `app_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `abbr` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE `area_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

CREATE TABLE `areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `abbr` varchar(255) DEFAULT NULL,
  `supplier_id` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=534 DEFAULT CHARSET=utf8;

CREATE TABLE `article_licenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `fee` decimal(8,2) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `high_level_bom_id` int(11) DEFAULT NULL,
  `external_contact_id` varchar(255) DEFAULT NULL,
  `active_yn` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `currency` varchar(255) DEFAULT '€',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

CREATE TABLE `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_reference` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `price` decimal(8,3) DEFAULT NULL,
  `high_level_bom_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `active` tinyint(1) DEFAULT '0',
  `created_by_user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `supplier_reference` varchar(255) DEFAULT NULL,
  `weight` decimal(8,2) DEFAULT NULL,
  `volume` decimal(8,2) DEFAULT NULL,
  `uom_weight` varchar(255) DEFAULT NULL,
  `uom_volume` varchar(255) DEFAULT NULL,
  `purchase_price` decimal(8,3) DEFAULT NULL,
  `currency` varchar(255) DEFAULT '€',
  `purchase_currency` varchar(255) DEFAULT '€',
  `non_stockable` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

CREATE TABLE `assembly_headers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `remarks` text,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `output_format` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1212 DEFAULT CHARSET=utf8;

CREATE TABLE `assembly_lines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assembly_header_id` int(11) DEFAULT NULL,
  `reception_id` int(11) DEFAULT NULL,
  `reference_id` varchar(255) DEFAULT NULL,
  `company_abbr_id` int(11) DEFAULT NULL,
  `data_release_id` int(11) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `data_release_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62599 DEFAULT CHARSET=utf8;

CREATE TABLE `asset_type_number_ranges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `start_range` int(11) DEFAULT NULL,
  `end_range` int(11) DEFAULT NULL,
  `asset_type_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `asset_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `gencode` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

CREATE TABLE `assets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_id` varchar(255) DEFAULT NULL,
  `requested_by` varchar(255) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `assettype` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `suffix` varchar(255) DEFAULT NULL,
  `document_id` int(11) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_assets_on_ref_id` (`ref_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1702 DEFAULT CHARSET=utf8;

CREATE TABLE `bulgarian_translations` (
  `original` varchar(255) NOT NULL,
  `translated` varchar(255) NOT NULL,
  `upcase_original` varchar(255) NOT NULL,
  `data_type` tinyint(4) NOT NULL COMMENT '0 - term; 1 - object\n',
  PRIMARY KEY (`upcase_original`),
  KEY `term` (`upcase_original`,`data_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 6144 kB';

CREATE TABLE `bundle_contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `conversiontool_id` int(11) DEFAULT NULL,
  `cvtool_bundle_id` int(11) DEFAULT NULL,
  `executable_id` int(11) DEFAULT NULL,
  `tool_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tool_release` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_improvement_process` tinyint(1) DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `input` text COLLATE utf8_unicode_ci,
  `output` text COLLATE utf8_unicode_ci,
  `parameters` longtext COLLATE utf8_unicode_ci,
  `toolspec_contents` text COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25041 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `business_processes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `core_bp_yn` tinyint(1) DEFAULT NULL,
  `header_link_controller` varchar(255) DEFAULT NULL,
  `header_link_action` varchar(255) DEFAULT NULL,
  `line_link_controller` varchar(255) DEFAULT NULL,
  `line_link_action` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

CREATE TABLE `calls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `callsession_id` int(11) DEFAULT NULL,
  `poimatch_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `correct_name` varchar(255) DEFAULT NULL,
  `correct_streetname` varchar(255) DEFAULT NULL,
  `correct_housenumber` varchar(255) DEFAULT NULL,
  `correct_phonenumber` varchar(255) DEFAULT NULL,
  `remark` text,
  `phonenumber_exists` varchar(255) DEFAULT NULL,
  `poi_exists` varchar(255) DEFAULT NULL,
  `called` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `callsessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feature_code` varchar(255) DEFAULT NULL,
  `country_code` varchar(255) DEFAULT NULL,
  `scenario_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `number_called` int(11) DEFAULT NULL,
  `datasource_type` int(11) DEFAULT NULL,
  `number_of_calls` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `carribatools` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `release` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feature_name` varchar(255) DEFAULT NULL,
  `feature_type` varchar(255) DEFAULT NULL,
  `feature_code_navteq` int(11) DEFAULT NULL,
  `feature_code_teleatlas` int(11) DEFAULT NULL,
  `CEN_GDF_3` int(11) DEFAULT NULL,
  `SVF_code` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `food_type` tinyint(1) DEFAULT NULL,
  `brandname` tinyint(1) DEFAULT NULL,
  `name_of_facility` tinyint(1) DEFAULT NULL,
  `street_name` tinyint(1) DEFAULT NULL,
  `house_number` tinyint(1) DEFAULT NULL,
  `postal_code` tinyint(1) DEFAULT NULL,
  `city_name` tinyint(1) DEFAULT NULL,
  `country_name` tinyint(1) DEFAULT NULL,
  `importance` tinyint(1) DEFAULT NULL,
  `indicator` tinyint(1) DEFAULT NULL,
  `phone_number` tinyint(1) DEFAULT NULL,
  `fax_number` tinyint(1) DEFAULT NULL,
  `deeplink` tinyint(1) DEFAULT NULL,
  `email` tinyint(1) DEFAULT NULL,
  `invicinity` tinyint(1) DEFAULT NULL,
  `fuel_type` tinyint(1) DEFAULT NULL,
  `parking_size` tinyint(1) DEFAULT NULL,
  `departure_times` tinyint(1) DEFAULT NULL,
  `arrival_times` tinyint(1) DEFAULT NULL,
  `picture` tinyint(1) DEFAULT NULL,
  `population` tinyint(1) DEFAULT NULL,
  `size` tinyint(1) DEFAULT NULL,
  `opening_times` tinyint(1) DEFAULT NULL,
  `speed_limit` tinyint(1) DEFAULT NULL,
  `edit_information` tinyint(1) DEFAULT NULL,
  `price_indicator` tinyint(1) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=293 DEFAULT CHARSET=utf8;

CREATE TABLE `category_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `point_attribute_id` int(11) DEFAULT NULL,
  `description` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ciq_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iso2` varchar(255) DEFAULT NULL,
  `ascii_name` varchar(255) DEFAULT NULL,
  `city_name` varchar(255) DEFAULT NULL,
  `state_region` varchar(255) DEFAULT NULL,
  `population` int(11) DEFAULT NULL,
  `latitude` float DEFAULT NULL,
  `longtitude` float DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_cities_on_city_name_and_iso2` (`city_name`,`iso2`),
  KEY `city_iso2` (`iso2`)
) ENGINE=InnoDB AUTO_INCREMENT=8098066 DEFAULT CHARSET=utf8;

CREATE TABLE `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE `company_abbr_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_abbr_id` int(11) DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ms_abbr_3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `copyright_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `dakota_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `company_abbrs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `abbr` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `relation_type` varchar(255) DEFAULT NULL,
  `ms_abbr_2` varchar(255) DEFAULT NULL,
  `ms_abbr_3` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `copyright_name` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT '0',
  `updated_by` varchar(255) DEFAULT NULL,
  `dakota_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=274 DEFAULT CHARSET=utf8;

CREATE TABLE `config_parameter_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `config_parameter_id` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `cfg_value` longtext,
  `cfg_group` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

CREATE TABLE `config_parameters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cfg_name` varchar(255) DEFAULT NULL,
  `cfg_type` varchar(255) DEFAULT NULL,
  `cfg_value` longtext,
  `description` text,
  `cfg_group` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

CREATE TABLE `content_release_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `product_release_note_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `subrank` int(11) DEFAULT NULL,
  `start_on_new_page` tinyint(1) DEFAULT NULL,
  `product_list` tinyint(1) DEFAULT NULL,
  `addition_required_yn` tinyint(1) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `content_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1065 DEFAULT CHARSET=utf8;

CREATE TABLE `continents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `contracts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `startdate` date DEFAULT NULL,
  `enddate` date DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `datasource_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

CREATE TABLE `conversion_databases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `conversion_id` int(11) DEFAULT NULL,
  `db_type` varchar(255) DEFAULT NULL,
  `path_and_file` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `remarks` text,
  `product_id` varchar(255) DEFAULT NULL,
  `size_bytes` int(11) DEFAULT NULL,
  `removed_yn` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `can_be_removed_yn` tinyint(1) DEFAULT NULL,
  `conversion_job_id` int(11) DEFAULT NULL,
  `production_orderline_id` int(11) DEFAULT NULL,
  `crashed_yn` tinyint(1) DEFAULT NULL,
  `group_amount` int(11) DEFAULT '1',
  `group_by` varchar(255) DEFAULT NULL,
  `group_value` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT '1000',
  `removal_remark` text,
  `region_id` int(11) DEFAULT NULL,
  `stdout_log` longtext,
  `persistent_yn` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_conversion_databases_on_conversion_id` (`conversion_id`),
  KEY `index_conversion_databases_on_conversion_job_id` (`conversion_job_id`),
  KEY `index_conversion_databases_on_production_orderline_id` (`production_orderline_id`),
  KEY `index_conversion_databases_on_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=30062 DEFAULT CHARSET=utf8;

CREATE TABLE `conversion_environments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `active_yn` tinyint(1) DEFAULT '1',
  `remarks` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `conversion_job_process_data_elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `conversion_job_id` int(11) DEFAULT NULL,
  `process_data_element_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `process_data_version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8400 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `conversions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_by` varchar(255) DEFAULT NULL,
  `request_date` date DEFAULT NULL,
  `conversion_date` date DEFAULT NULL,
  `priority` varchar(255) DEFAULT NULL,
  `supplier_id` varchar(255) DEFAULT NULL,
  `datasource_id` int(11) DEFAULT NULL,
  `data_release_id` int(11) DEFAULT NULL,
  `product` varchar(255) DEFAULT NULL,
  `additional_info` text,
  `request_data` varchar(255) DEFAULT NULL,
  `converter` varchar(255) DEFAULT NULL,
  `tpd` tinyint(1) DEFAULT NULL,
  `data_files` longtext,
  `reference_numbers` text,
  `remarks` text,
  `right_data_check` tinyint(1) DEFAULT NULL,
  `cvtool_check` tinyint(1) DEFAULT NULL,
  `outcome` varchar(255) DEFAULT NULL,
  `cvtoolenv` varchar(255) DEFAULT NULL,
  `size_bytes` float DEFAULT NULL,
  `size_mb` float DEFAULT NULL,
  `database_name` varchar(255) DEFAULT NULL,
  `productid` varchar(255) DEFAULT NULL,
  `reception_reference` varchar(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `purpose` varchar(255) DEFAULT NULL,
  `legacy_type` varchar(255) DEFAULT NULL,
  `ca_brn` varchar(255) DEFAULT NULL,
  `tpd_products` text,
  `tpd_rejected` text,
  `tpd_rejection_remark` text,
  `tpd_result_sender` varchar(255) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `region_name` varchar(255) DEFAULT NULL,
  `template_filename` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `server_name` varchar(255) DEFAULT NULL,
  `production_orderline_id` int(11) DEFAULT NULL,
  `output_format` varchar(255) DEFAULT NULL,
  `input_format` varchar(255) DEFAULT NULL,
  `result_files` text,
  `end_product_yn` varchar(255) DEFAULT NULL,
  `cloned_from_id` int(11) DEFAULT NULL,
  `assembly_id` int(11) DEFAULT NULL,
  `status_remark` text,
  `rejection_remark` text,
  `cv_tool_template_id` int(11) DEFAULT NULL,
  `conversiontool_id` int(11) DEFAULT NULL,
  `cvtool_bundle_id` int(11) DEFAULT NULL,
  `design_sequence` int(11) DEFAULT NULL,
  `process_data_id` int(11) DEFAULT NULL,
  `request_user_id` int(11) DEFAULT NULL,
  `replaced_by_id` int(11) DEFAULT NULL,
  `conversion_environment_id` int(11) DEFAULT NULL,
  `force_create` tinyint(1) DEFAULT '0',
  `outcome_persistent_yn` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `data_release_id_conversions` (`data_release_id`)
) ENGINE=InnoDB AUTO_INCREMENT=73271 DEFAULT CHARSET=utf8;

CREATE TABLE `conversiontools` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `release` varchar(255) DEFAULT NULL,
  `tool_type` varchar(255) DEFAULT NULL,
  `major_release` varchar(255) DEFAULT NULL,
  `remarks` text,
  `prod_released_yn` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2460 DEFAULT CHARSET=utf8;

CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iso` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `printable_name` varchar(255) DEFAULT NULL,
  `iso3` varchar(255) DEFAULT NULL,
  `num_code` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_countries_on_name` (`name`),
  KEY `index_countries_on_name_and_iso` (`name`,`iso`)
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=utf8;

CREATE TABLE `country_coverages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `internet_datasource_id` int(11) DEFAULT NULL,
  `country_code` varchar(255) DEFAULT NULL,
  `amount_found` int(11) DEFAULT NULL,
  `amount_expected` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `coverages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `remarks` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22475 DEFAULT CHARSET=utf8;

CREATE TABLE `coverages_regions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coverage_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `do_not_process` tinyint(1) DEFAULT NULL,
  `parameter_settings` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37935 DEFAULT CHARSET=utf8;

CREATE TABLE `customer_content_definitions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `output_target` int(11) DEFAULT NULL,
  `customer_content_specification_id` int(11) DEFAULT NULL,
  `product_line_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

CREATE TABLE `customer_content_specification_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `content_type` int(11) DEFAULT NULL,
  `description` text,
  `generation_code` text,
  `validation_code` text,
  `is_active` tinyint(1) DEFAULT NULL,
  `mandatory` tinyint(1) DEFAULT NULL,
  `manual` tinyint(1) DEFAULT NULL,
  `for_product` tinyint(1) DEFAULT NULL,
  `customer_content_specification_id` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT '0',
  `bug_tracker_id` varchar(255) DEFAULT NULL,
  `comment` text,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

CREATE TABLE `customer_content_specifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `content_type` int(11) DEFAULT NULL,
  `description` text,
  `generation_code` text,
  `validation_code` text,
  `is_active` tinyint(1) DEFAULT '1',
  `mandatory` tinyint(1) DEFAULT '1',
  `manual` tinyint(1) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `for_product` tinyint(1) DEFAULT '0',
  `version` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `bug_tracker_id` varchar(255) DEFAULT NULL,
  `comment` text,
  `updated_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

CREATE TABLE `customer_content_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_content_specification_id` int(11) DEFAULT NULL,
  `customer_version` varchar(255) DEFAULT NULL,
  `received_date` date DEFAULT NULL,
  `value` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

CREATE TABLE `customer_contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_content_specification_id` int(11) DEFAULT NULL,
  `production_orderline_id` int(11) DEFAULT NULL,
  `value` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1321 DEFAULT CHARSET=utf8;

CREATE TABLE `customer_depots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `street_name` varchar(255) DEFAULT NULL,
  `house_number` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `unloading_point` varchar(255) DEFAULT NULL,
  `billing_country` varchar(255) DEFAULT NULL,
  `contact_name` varchar(255) DEFAULT NULL,
  `constact_salutation` varchar(255) DEFAULT NULL,
  `contact_phone` varchar(255) DEFAULT NULL,
  `contact_email` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `vat_identification` varchar(255) DEFAULT NULL,
  `payment_term` int(11) DEFAULT NULL,
  `ordering_yn` tinyint(1) DEFAULT NULL,
  `shipping_yn` tinyint(1) DEFAULT NULL,
  `invoicing_yn` tinyint(1) DEFAULT NULL,
  `customer_code` varchar(255) DEFAULT NULL,
  `supplier_code` varchar(255) DEFAULT NULL,
  `vat_percentage` int(11) DEFAULT NULL,
  `vat_statement` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

CREATE TABLE `customer_documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` varchar(255) DEFAULT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `PPF` tinyint(1) DEFAULT NULL,
  `NDS` tinyint(1) DEFAULT NULL,
  `TMC` tinyint(1) DEFAULT NULL,
  `TPD_RN` tinyint(1) DEFAULT NULL,
  `country_stats` tinyint(1) DEFAULT NULL,
  `target_docs` tinyint(1) DEFAULT NULL,
  `quality_doc` tinyint(1) DEFAULT NULL,
  `CV_tool_RN` tinyint(1) DEFAULT NULL,
  `errorlist` tinyint(1) DEFAULT NULL,
  `data_supplier_RN` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `customer_targets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `output_target` int(11) DEFAULT NULL,
  `customer_content_id` int(11) DEFAULT NULL,
  `customer_content_definition_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1389 DEFAULT CHARSET=utf8;

CREATE TABLE `cvtool_bundle_releases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `conversiontool_id` int(11) DEFAULT NULL,
  `cvtool_bundle_id` int(11) DEFAULT NULL,
  `active_yn` tinyint(1) DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3430 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `cvtool_bundles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `remarks` text,
  `active_yn` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=824 DEFAULT CHARSET=utf8;

CREATE TABLE `cvtool_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `template` text,
  `template_by_user_id` int(11) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=718 DEFAULT CHARSET=utf8;

CREATE TABLE `dashboard_grid_widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dashboard_widget_id` int(11) DEFAULT NULL,
  `dashboard_grid_id` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `widget_settings` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

CREATE TABLE `dashboard_grids` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

CREATE TABLE `dashboard_widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `width_css_class` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `refresh_time` int(11) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

CREATE TABLE `data_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `capacity` int(11) DEFAULT NULL,
  `unit_of_measure` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

CREATE TABLE `data_releases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `release_code` varchar(255) DEFAULT NULL,
  `datasource_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `production` tinyint(1) DEFAULT '0',
  `company_abbr_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22038 DEFAULT CHARSET=utf8;

CREATE TABLE `data_set_definition_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_set_definition_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `company_abbr_id` int(11) DEFAULT NULL,
  `data_release_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `data_type_id` int(11) DEFAULT NULL,
  `filling_id` int(11) DEFAULT NULL,
  `description` text,
  `remarks` text,
  `coverage_id` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `data_set_definitions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `company_abbr_id` int(11) DEFAULT NULL,
  `data_release_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `data_type_id` int(11) DEFAULT NULL,
  `filling_id` int(11) DEFAULT NULL,
  `description` text,
  `remarks` text,
  `coverage_id` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `data_type_fillings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_type_id` int(11) DEFAULT NULL,
  `filling_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `data_type_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_type_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `automatic_validation_yn` tinyint(1) DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `bug_tracker_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `schedule_manual` tinyint(1) NOT NULL DEFAULT '1',
  `schedule_cores` int(11) DEFAULT NULL,
  `schedule_memory_base` int(11) DEFAULT NULL,
  `schedule_memory_times_input` int(11) DEFAULT NULL,
  `schedule_diskspace_base` int(11) DEFAULT NULL,
  `schedule_diskspace_times_input` int(11) DEFAULT NULL,
  `schedule_allow_virtual` tinyint(1) DEFAULT NULL,
  `schedule_exclusive` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `validation_executable` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `data_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `rules` text,
  `is_intermediate` tinyint(1) DEFAULT '0',
  `automatic_validation_yn` tinyint(1) DEFAULT '0',
  `updated_by` varchar(255) DEFAULT '0',
  `version` int(11) DEFAULT '0',
  `bug_tracker_id` varchar(255) DEFAULT NULL,
  `comment` text,
  `status` varchar(255) DEFAULT 'approved',
  `schedule_manual` tinyint(1) NOT NULL DEFAULT '1',
  `schedule_cores` int(11) DEFAULT '1',
  `schedule_memory_base` int(11) DEFAULT '0',
  `schedule_memory_times_input` int(11) DEFAULT '0',
  `schedule_diskspace_base` int(11) DEFAULT '0',
  `schedule_diskspace_times_input` int(11) DEFAULT '1',
  `schedule_allow_virtual` tinyint(1) DEFAULT '1',
  `schedule_exclusive` tinyint(1) DEFAULT '0',
  `validation_executable` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;

CREATE TABLE `datasource_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datasource_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `remarks` text,
  `site_category` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `datasource_internalcontacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datasource_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;

CREATE TABLE `datasource_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datasource_id` int(11) DEFAULT NULL,
  `app_module_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1155 DEFAULT CHARSET=utf8;

CREATE TABLE `datasource_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

CREATE TABLE `datasource_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datasource_id` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `supplier_id` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `copyright` varchar(255) DEFAULT NULL,
  `datasource_type` int(11) DEFAULT NULL,
  `purpose` varchar(255) DEFAULT NULL,
  `coverage_id` int(11) DEFAULT NULL,
  `abbr` varchar(255) DEFAULT NULL,
  `production` tinyint(1) DEFAULT '0',
  `active_yn` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4668 DEFAULT CHARSET=utf8;

CREATE TABLE `datasources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `supplier_id` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `copyright` varchar(255) DEFAULT NULL,
  `datasource_type` int(11) DEFAULT NULL,
  `purpose` varchar(255) DEFAULT NULL,
  `coverage_id` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `abbr` varchar(255) DEFAULT NULL,
  `production` tinyint(1) DEFAULT '0',
  `active_yn` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1856 DEFAULT CHARSET=utf8;

CREATE TABLE `default_content_release_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `product_category_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `start_on_new_page` tinyint(1) DEFAULT NULL,
  `product_list` tinyint(1) DEFAULT NULL,
  `addition_required_yn` tinyint(1) DEFAULT NULL,
  `rank` int(11) DEFAULT NULL,
  `subrank` int(11) DEFAULT NULL,
  `use_customer_content_yn` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

CREATE TABLE `delayed_jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `priority` int(11) DEFAULT '0',
  `attempts` int(11) DEFAULT '0',
  `handler` text,
  `last_error` text,
  `run_at` datetime DEFAULT NULL,
  `locked_at` datetime DEFAULT NULL,
  `failed_at` datetime DEFAULT NULL,
  `locked_by` varchar(255) DEFAULT NULL,
  `queue` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `delayed_jobs_priority` (`priority`,`run_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `design_tag_hierarchies` (
  `ancestor_id` int(11) NOT NULL,
  `descendant_id` int(11) NOT NULL,
  `generations` int(11) NOT NULL,
  UNIQUE KEY `index_design_tag_hierarchies_on_ancestor_id_and_descendant_id` (`ancestor_id`,`descendant_id`),
  KEY `index_design_tag_hierarchies_on_descendant_id` (`descendant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `design_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `tag_type` varchar(255) DEFAULT 'Other',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=626 DEFAULT CHARSET=utf8;

CREATE TABLE `distribution_list_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `distribution_list_id` int(11) DEFAULT NULL,
  `external_contact_id` varchar(255) DEFAULT NULL,
  `electronic_delivery_yn` tinyint(1) DEFAULT NULL,
  `hard_copy_delivery_yn` tinyint(1) DEFAULT NULL,
  `hard_copy_amount` int(11) DEFAULT NULL,
  `transfer_method` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=utf8;

CREATE TABLE `distribution_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `active_yn` tinyint(1) DEFAULT NULL,
  `deleted_yn` tinyint(1) DEFAULT NULL,
  `remarks` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `product_category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

CREATE TABLE `doctemplates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

CREATE TABLE `document_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document_id` int(11) DEFAULT NULL,
  `content_type` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `binary_data` longblob,
  `version` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `sync_filepath` varchar(255) DEFAULT NULL,
  `last_modified_timestamp` datetime DEFAULT NULL,
  `checksum` varchar(255) DEFAULT NULL,
  `document_object_id` varchar(255) DEFAULT NULL,
  `production_orderline_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_document_versions_on_sync_filepath` (`sync_filepath`),
  KEY `document_versions_document_id_version` (`document_id`,`version`),
  KEY `index_document_versions_on_document_id_and_version` (`document_id`,`version`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

CREATE TABLE `documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_type` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `binary_data` longblob,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `sync_filepath` varchar(255) DEFAULT NULL,
  `last_modified_timestamp` datetime DEFAULT NULL,
  `checksum` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `document_object_id` varchar(255) DEFAULT NULL,
  `production_order_id` int(11) DEFAULT NULL,
  `package_directory` varchar(255) DEFAULT NULL,
  `mandatory_yn` tinyint(1) DEFAULT NULL,
  `production_orderline_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_documents_on_sync_filepath` (`sync_filepath`),
  KEY `index_documents_on_production_order_id` (`production_order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5439 DEFAULT CHARSET=utf8;

CREATE TABLE `environments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` varchar(255) DEFAULT NULL,
  `datasource_id` int(11) DEFAULT NULL,
  `point_id` int(11) DEFAULT NULL,
  `scheduler_job_id` int(11) DEFAULT NULL,
  `trace` text,
  `level` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `related_data` text,
  `instance_id` int(11) DEFAULT NULL,
  `app_module` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_events_on_created_at` (`created_at`),
  KEY `index_events_on_point_id` (`point_id`),
  KEY `index_events_on_datasource_id` (`datasource_id`),
  KEY `idx_instance_id_events` (`instance_id`),
  KEY `app_id_idx_instance_id_events` (`app_module`,`instance_id`)
) ENGINE=InnoDB AUTO_INCREMENT=146082 DEFAULT CHARSET=utf8;

CREATE TABLE `executable_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `executable_id` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `description` text,
  `input` longtext,
  `output` longtext,
  `parameters` longtext,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `bug_tracker_id` varchar(255) DEFAULT NULL,
  `comment` text,
  `status` varchar(255) DEFAULT NULL,
  `schedule_manual` tinyint(1) NOT NULL DEFAULT '1',
  `schedule_cores` int(11) DEFAULT NULL,
  `schedule_memory_base` int(11) DEFAULT NULL,
  `schedule_memory_times_input` int(11) DEFAULT NULL,
  `schedule_diskspace_base` int(11) DEFAULT NULL,
  `schedule_diskspace_times_input` int(11) DEFAULT NULL,
  `schedule_allow_virtual` tinyint(1) DEFAULT NULL,
  `schedule_exclusive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4183 DEFAULT CHARSET=utf8;

CREATE TABLE `executables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `description` text,
  `input` text,
  `output` text,
  `parameters` longtext,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `rules` text,
  `source_data_def_type` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `metalogic` text,
  `subprocs` varchar(255) DEFAULT NULL,
  `bug_tracker_id` varchar(255) DEFAULT NULL,
  `comment` text,
  `status` varchar(255) DEFAULT NULL,
  `schedule_manual` tinyint(1) NOT NULL DEFAULT '1',
  `schedule_cores` int(11) DEFAULT NULL,
  `schedule_memory_base` int(11) DEFAULT NULL,
  `schedule_memory_times_input` int(11) DEFAULT NULL,
  `schedule_diskspace_base` int(11) DEFAULT NULL,
  `schedule_diskspace_times_input` int(11) DEFAULT NULL,
  `schedule_allow_virtual` tinyint(1) DEFAULT NULL,
  `schedule_exclusive` tinyint(1) DEFAULT NULL,
  `customer_visibility` tinyint(1) DEFAULT '1',
  `is_improvement_process` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_executables_on_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=190 DEFAULT CHARSET=utf8;

CREATE TABLE `external_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datasource_id` int(11) DEFAULT NULL,
  `external_contact_id` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE `features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `code_navteq` int(11) DEFAULT NULL,
  `code_teleatlas` int(11) DEFAULT NULL,
  `cen_gdf_code` int(11) DEFAULT NULL,
  `svf_code` int(11) DEFAULT NULL,
  `feature_type` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `file_transfer_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transfer_type` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `remarks` text,
  `company_abbrs_id` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

CREATE TABLE `fillings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active_yn` tinyint(1) DEFAULT NULL,
  `removed_yn` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `group_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_group_id` int(11) DEFAULT NULL,
  `main_menu_datasources` tinyint(1) DEFAULT NULL,
  `main_menu_IC` tinyint(1) DEFAULT NULL,
  `main_menu_administration` tinyint(1) DEFAULT NULL,
  `datasources_details` tinyint(1) DEFAULT NULL,
  `datasources_edit_fields` tinyint(1) DEFAULT NULL,
  `datasource_jobs` tinyint(1) DEFAULT NULL,
  `datasource_IC` tinyint(1) DEFAULT NULL,
  `datasource_contracts` tinyint(1) DEFAULT NULL,
  `datasource_releases` tinyint(1) DEFAULT NULL,
  `datasource_EC` tinyint(1) DEFAULT NULL,
  `internal_contacts` tinyint(1) DEFAULT NULL,
  `administration_places` tinyint(1) DEFAULT NULL,
  `administration_datasource_types` tinyint(1) DEFAULT NULL,
  `adminstration_coverages` tinyint(1) DEFAULT NULL,
  `administration_events` tinyint(1) DEFAULT NULL,
  `administration_categories` tinyint(1) DEFAULT NULL,
  `administration_point_statistics` tinyint(1) DEFAULT NULL,
  `administration_detailed_point_statistics` tinyint(1) DEFAULT NULL,
  `administration_management_statistics` tinyint(1) DEFAULT NULL,
  `administration_user_rights` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `administration_ping_events` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE `high_level_boms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `revision` varchar(255) DEFAULT NULL,
  `internal_reference` varchar(255) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_location_id` int(11) DEFAULT NULL,
  `inlay_reception_id` int(11) DEFAULT NULL,
  `label_reception_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `active` tinyint(1) DEFAULT '0',
  `status` int(11) DEFAULT NULL,
  `created_by_user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

CREATE TABLE `inbound_orderlines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `drfmsid` varchar(255) DEFAULT NULL,
  `mediatype` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `dbrelease` varchar(255) DEFAULT NULL,
  `deliveryformat` varchar(255) DEFAULT NULL,
  `dbversion` varchar(255) DEFAULT NULL,
  `inbound_order_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `arrival_date` date DEFAULT NULL,
  `data_type_id` int(11) DEFAULT NULL,
  `data_release_id` int(11) DEFAULT NULL,
  `data_release_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12298 DEFAULT CHARSET=utf8;

CREATE TABLE `inbound_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) DEFAULT NULL,
  `arrival_timestamp` datetime DEFAULT NULL,
  `tracking_nr` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `orderref` varchar(255) DEFAULT NULL,
  `ordersubref` varchar(255) DEFAULT NULL,
  `data_release_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20649 DEFAULT CHARSET=utf8;

CREATE TABLE `included_article_license_purchase_orderlines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipping_orderline_id` int(11) DEFAULT NULL,
  `article_license_id` int(11) DEFAULT NULL,
  `purchase_orderline_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

CREATE TABLE `included_database_test_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_request_id` int(11) DEFAULT NULL,
  `conversion_database_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `test_yn` tinyint(1) DEFAULT NULL,
  `ref_yn` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1235 DEFAULT CHARSET=utf8;

CREATE TABLE `includeddocument_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `document_version_id` int(11) DEFAULT NULL,
  `original_id` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `includeddocuments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `document_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6386 DEFAULT CHARSET=utf8;

CREATE TABLE `includedproduct_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `includedproduct_id` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `productset_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `link_product_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=917099 DEFAULT CHARSET=utf8;

CREATE TABLE `includedproducts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productset_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `link_product_id` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dba_includedproducts_productset_id_idx` (`productset_id`),
  KEY `index_includedproducts_on_productset_id` (`productset_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5052 DEFAULT CHARSET=utf8;

CREATE TABLE `includedproductsetdocument_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productset_id` int(11) DEFAULT NULL,
  `document_version_id` int(11) DEFAULT NULL,
  `original_id` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `includedproductsetdocuments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productset_id` int(11) DEFAULT NULL,
  `document_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7460 DEFAULT CHARSET=utf8;

CREATE TABLE `internal_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `job_title` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

CREATE TABLE `internet_datasource_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `internet_datasource_id` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `script` text,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `scheduler_job_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `processor_type` varchar(255) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `datasource_id` int(11) DEFAULT NULL,
  `add_manually` tinyint(1) DEFAULT NULL,
  `average_conflvl1` decimal(12,5) DEFAULT NULL,
  `average_conflvl2` decimal(12,5) DEFAULT NULL,
  `average_matchlvl1` decimal(12,5) DEFAULT NULL,
  `average_matchlvl2` decimal(12,5) DEFAULT NULL,
  `number_geomatched` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `use_proxy` tinyint(1) DEFAULT '0',
  `host_last_run` varchar(255) DEFAULT NULL,
  `date_last_run` datetime DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `estimated_amount` int(11) DEFAULT NULL,
  `script_creator` varchar(255) DEFAULT NULL,
  `proxy_server` varchar(255) DEFAULT NULL,
  `proxy_port` varchar(255) DEFAULT NULL,
  `high_priority` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4574 DEFAULT CHARSET=utf8;

CREATE TABLE `internet_datasources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location` varchar(255) DEFAULT NULL,
  `script` text,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `scheduler_job_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `processor_type` varchar(255) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `datasource_id` int(11) DEFAULT NULL,
  `add_manually` tinyint(1) DEFAULT NULL,
  `average_conflvl1` decimal(12,5) DEFAULT NULL,
  `average_conflvl2` decimal(12,5) DEFAULT NULL,
  `average_matchlvl1` decimal(12,5) DEFAULT NULL,
  `average_matchlvl2` decimal(12,5) DEFAULT NULL,
  `number_geomatched` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `use_proxy` tinyint(1) DEFAULT '0',
  `host_last_run` varchar(255) DEFAULT NULL,
  `date_last_run` datetime DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `estimated_amount` int(11) DEFAULT NULL,
  `script_creator` varchar(255) DEFAULT NULL,
  `proxy_server` varchar(255) DEFAULT NULL,
  `proxy_port` varchar(255) DEFAULT NULL,
  `high_priority` int(11) DEFAULT NULL,
  `active_yn` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1401 DEFAULT CHARSET=utf8;

CREATE TABLE `invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `remarks` text,
  `shipping_order_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `vat_percentage` int(11) DEFAULT NULL,
  `vat` decimal(13,3) DEFAULT NULL,
  `total` decimal(13,3) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `credit_invoice_parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8;

CREATE TABLE `job_fail_reasons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reason` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `job_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `jobresults` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `raw_data` text,
  `converted_data` text,
  `job_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `processing_time` float DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `hashcode` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=utf8;

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `conversion_id` varchar(255) DEFAULT NULL,
  `run_server` varchar(255) DEFAULT NULL,
  `run_command` varchar(255) DEFAULT NULL,
  `run_script` longtext,
  `run_pid` varchar(255) DEFAULT NULL,
  `working_dir` varchar(255) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `finish_time` datetime DEFAULT NULL,
  `stdout_log` longtext,
  `exitcode` int(11) DEFAULT NULL,
  `fail_reason` text,
  `schedule_manual` tinyint(1) DEFAULT NULL,
  `schedule_cores` int(11) DEFAULT NULL,
  `schedule_memory` int(11) DEFAULT NULL,
  `schedule_diskspace` int(11) DEFAULT NULL,
  `schedule_allow_virtual` tinyint(1) DEFAULT NULL,
  `schedule_exclusive` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(11) DEFAULT NULL,
  `conversion_environment_id` int(11) DEFAULT NULL,
  `lock_version` int(11) DEFAULT '1',
  `move_status` varchar(255) DEFAULT NULL,
  `reference_id` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `bug_tracker_id` varchar(255) DEFAULT NULL,
  `group_amount` int(11) DEFAULT '1',
  `group_by` varchar(255) DEFAULT NULL,
  `group_value` varchar(255) DEFAULT NULL,
  `action_log` text,
  `fail_category` varchar(255) DEFAULT NULL,
  `fail_category_logged_by_user_id` int(11) DEFAULT NULL,
  `fail_category_logged_on` date DEFAULT NULL,
  `estimated_duration` int(11) DEFAULT NULL,
  `result_dir_size_bytes` int(11) DEFAULT NULL,
  `original_job_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_jobs_on_type` (`type`),
  KEY `index_jobs_on_conversion_id` (`conversion_id`),
  KEY `index_jobs_on_reference_id` (`reference_id`),
  KEY `index_jobs_on_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=13683 DEFAULT CHARSET=utf8;

CREATE TABLE `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `part2b` varchar(255) DEFAULT NULL,
  `part2t` varchar(255) DEFAULT NULL,
  `part1` varchar(255) DEFAULT NULL,
  `scope` varchar(255) DEFAULT NULL,
  `lang_type` varchar(255) DEFAULT NULL,
  `ref_name` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `active_yn` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61579 DEFAULT CHARSET=utf8;

CREATE TABLE `licenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `licensenumber` varchar(255) DEFAULT NULL,
  `purpose` varchar(255) DEFAULT NULL,
  `active_yn` varchar(255) DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `comment` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_code` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `cd_no` int(11) DEFAULT NULL,
  `cd_type` int(11) DEFAULT NULL,
  `borrby` varchar(255) DEFAULT NULL,
  `borron` varchar(255) DEFAULT NULL,
  `borrback` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `locationtype` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1932 DEFAULT CHARSET=utf8;

CREATE TABLE `mail_conditions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field` varchar(255) DEFAULT NULL,
  `field_value` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

CREATE TABLE `mail_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;

CREATE TABLE `manufacturers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `abbr` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `mapping_lines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mapping_id` int(11) DEFAULT NULL,
  `entity` varchar(255) DEFAULT NULL,
  `target_attribute` varchar(255) DEFAULT NULL,
  `source_attribute` varchar(255) DEFAULT NULL,
  `constant` varchar(255) DEFAULT NULL,
  `block` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `custom_target_attribute` tinyint(1) DEFAULT NULL,
  `custom_source_attribute` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=402 DEFAULT CHARSET=utf8;

CREATE TABLE `mappings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `source_endpoint` varchar(255) DEFAULT NULL,
  `destination_endpoint` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `issues_filter_id` text,
  `source_adapter` varchar(255) DEFAULT NULL,
  `destination_adapter` varchar(255) DEFAULT NULL,
  `active_yn` tinyint(1) DEFAULT NULL,
  `source_login_name` varchar(255) DEFAULT NULL,
  `source_login_password` varchar(255) DEFAULT NULL,
  `destination_login_name` varchar(255) DEFAULT NULL,
  `destination_login_password` varchar(255) DEFAULT NULL,
  `http_authentication_source_login_name` varchar(255) DEFAULT NULL,
  `http_authentication_source_login_password` varchar(255) DEFAULT NULL,
  `http_authentication_destination_login_name` varchar(255) DEFAULT NULL,
  `http_authentication_destination_login_password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

CREATE TABLE `matchmasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mask` int(11) DEFAULT NULL,
  `mask_string` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

CREATE TABLE `mediatypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `ref` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

CREATE TABLE `navdbfeature_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE `navdbfeature_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `data_type_id` int(11) DEFAULT NULL,
  `navdbfeature_category_id` int(11) DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `navdbfeature_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `navdbfeature_id` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `navdb_feature_type_id` int(11) DEFAULT NULL,
  `dateeffective` datetime DEFAULT NULL,
  `description1` varchar(255) DEFAULT NULL,
  `description2` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `navdbfeatures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `navdb_feature_type_id` int(11) DEFAULT NULL,
  `dateeffective` datetime DEFAULT NULL,
  `description1` varchar(255) DEFAULT NULL,
  `description2` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `navigation_database_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `navigation_database_id` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `major` int(11) DEFAULT NULL,
  `minor` int(11) DEFAULT NULL,
  `dateeffective` datetime DEFAULT NULL,
  `changecomment` varchar(255) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

CREATE TABLE `navigation_databases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `major` int(11) DEFAULT NULL,
  `minor` int(11) DEFAULT NULL,
  `dateeffective` datetime DEFAULT NULL,
  `changecomment` varchar(255) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

CREATE TABLE `object_documentations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_name` varchar(255) DEFAULT NULL,
  `attribute_name` varchar(255) DEFAULT NULL,
  `short_description` text,
  `long_description` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

CREATE TABLE `orderstatuslogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `production_order_id` int(11) DEFAULT NULL,
  `completion_date` datetime DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `remarks` text,
  `reference` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20797 DEFAULT CHARSET=utf8;

CREATE TABLE `ordertype_business_processes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ordertype_id` int(11) DEFAULT NULL,
  `business_process_id` int(11) DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ordertypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `product_category_id` int(11) DEFAULT NULL,
  `no_pds_yn` tinyint(1) DEFAULT NULL,
  `is_frontend` tinyint(1) DEFAULT '0',
  `job_support_yn` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

CREATE TABLE `outbound_orderlines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `map_no` varchar(255) DEFAULT NULL,
  `cd_no` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `product_id` varchar(255) DEFAULT NULL,
  `stock_id` int(11) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `outbound_order_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `product_location_id` int(11) DEFAULT NULL,
  `production_orderline_id` int(11) DEFAULT NULL,
  `parent_linenr` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oline_cd_no` (`cd_no`)
) ENGINE=InnoDB AUTO_INCREMENT=20572 DEFAULT CHARSET=utf8;

CREATE TABLE `outbound_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` varchar(255) DEFAULT NULL,
  `send_date` date DEFAULT NULL,
  `contact_person` varchar(255) DEFAULT NULL,
  `shipto_name` varchar(255) DEFAULT NULL,
  `shipto_postalcode` varchar(255) DEFAULT NULL,
  `shipto_streetname` varchar(255) DEFAULT NULL,
  `shipto_country` varchar(255) DEFAULT NULL,
  `send_by` varchar(255) DEFAULT NULL,
  `map_no` varchar(255) DEFAULT NULL,
  `cd_no` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `extra` text,
  `amount` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `from_name` varchar(255) DEFAULT NULL,
  `remarks` text,
  `courier` varchar(255) DEFAULT NULL,
  `printed_alphatest_delivery` tinyint(1) DEFAULT NULL,
  `printed_test_delivery` tinyint(1) DEFAULT NULL,
  `printed_master_shipment` tinyint(1) DEFAULT NULL,
  `printed_copy_master_shipment` tinyint(1) DEFAULT NULL,
  `printed_release_notification` tinyint(1) DEFAULT NULL,
  `printed_master_shipment_info` tinyint(1) DEFAULT NULL,
  `printed_sample_approval` tinyint(1) DEFAULT NULL,
  `printed_release_notification_info` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `shipto_state` varchar(255) DEFAULT NULL,
  `shipto_city` varchar(255) DEFAULT NULL,
  `shipto_id` varchar(255) DEFAULT NULL,
  `contact_id` varchar(255) DEFAULT NULL,
  `release_mail_send` tinyint(1) DEFAULT NULL,
  `distribution_list_member_id` int(11) DEFAULT NULL,
  `release_mail_send_date` date DEFAULT NULL,
  `file_transfer_account_id` int(11) DEFAULT NULL,
  `shipping_parent_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `distribution_list_id` int(11) DEFAULT NULL,
  `shipping_specification_id` int(11) DEFAULT NULL,
  `sftp_subdirectory` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17750 DEFAULT CHARSET=utf8;

CREATE TABLE `packing_lines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `packing_id` int(11) DEFAULT NULL,
  `outbound_orderline_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `packing_specification_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `packing_specification_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `run_script` text COLLATE utf8_unicode_ci,
  `manual_yn` tinyint(1) DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `bug_tracker_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `schedule_manual` tinyint(1) NOT NULL DEFAULT '1',
  `schedule_cores` int(11) DEFAULT NULL,
  `schedule_memory_base` int(11) DEFAULT NULL,
  `schedule_memory_times_input` int(11) DEFAULT NULL,
  `schedule_diskspace_base` int(11) DEFAULT NULL,
  `schedule_diskspace_times_input` int(11) DEFAULT NULL,
  `schedule_allow_virtual` tinyint(1) DEFAULT NULL,
  `schedule_exclusive` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `packing_specifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `run_script` text COLLATE utf8_unicode_ci,
  `manual_yn` tinyint(1) DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `bug_tracker_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `schedule_manual` tinyint(1) NOT NULL DEFAULT '1',
  `schedule_cores` int(11) DEFAULT NULL,
  `schedule_memory_base` int(11) DEFAULT NULL,
  `schedule_memory_times_input` int(11) DEFAULT NULL,
  `schedule_diskspace_base` int(11) DEFAULT NULL,
  `schedule_diskspace_times_input` int(11) DEFAULT NULL,
  `schedule_allow_virtual` tinyint(1) DEFAULT NULL,
  `schedule_exclusive` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `packings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `outbound_order_id` int(11) DEFAULT NULL,
  `packing_specification_id` int(11) DEFAULT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `subdirectory` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `md5_checksum` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `page_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `font` varchar(255) DEFAULT NULL,
  `header` text,
  `footer` text,
  `body` text,
  `page_numbering` tinyint(1) DEFAULT NULL,
  `report_template_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `parameter_setting_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parameter_setting_id` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `parameterset` longtext,
  `design_tag_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `bug_tracker_id` varchar(255) DEFAULT NULL,
  `comment` text,
  `name` varchar(255) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `product_line_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

CREATE TABLE `parameter_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parameterset` longtext,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `design_tag_id` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT '1',
  `updated_by` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `bug_tracker_id` varchar(255) DEFAULT NULL,
  `comment` text,
  `name` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `product_line_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

CREATE TABLE `parent_receptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reception_id` varchar(255) DEFAULT NULL,
  `parent_reception_id` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1038 DEFAULT CHARSET=utf8;

CREATE TABLE `parts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

CREATE TABLE `ping_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `internet_datasource_id` int(11) DEFAULT NULL,
  `is_online` tinyint(1) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=691723 DEFAULT CHARSET=utf8;

CREATE TABLE `poi_mapping_layouts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_company_abbr_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `customer_company_abbr_id` int(11) DEFAULT NULL,
  `output_data_type_id` int(11) DEFAULT NULL,
  `input_data_type_id` int(11) DEFAULT NULL,
  `definition_version` varchar(255) DEFAULT NULL,
  `description` text,
  `import_hash` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `source_filename` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

CREATE TABLE `poi_mappings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poi_mapping_layout_id` int(11) DEFAULT NULL,
  `map_or_filter_type` varchar(255) DEFAULT NULL,
  `map_or_filter_value` varchar(255) DEFAULT NULL,
  `logical_type` varchar(255) DEFAULT NULL,
  `invert` tinyint(1) DEFAULT '0',
  `group_id` int(11) DEFAULT NULL,
  `output_category` int(11) DEFAULT NULL,
  `major_filtering` tinyint(1) DEFAULT '0',
  `input_description` text,
  `output_description` text,
  `import_hash` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8075 DEFAULT CHARSET=utf8;

CREATE TABLE `point_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `type` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `point_statistic_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `point_statistic_id` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `street_percentage` float DEFAULT NULL,
  `postal_code_percentage` float DEFAULT NULL,
  `housenumber_percentage` float DEFAULT NULL,
  `city_percentage` float DEFAULT NULL,
  `country_percentage` float DEFAULT NULL,
  `food_type_percentage` float DEFAULT NULL,
  `brandname_percentage` float DEFAULT NULL,
  `x_percentage` float DEFAULT NULL,
  `y_percentage` float DEFAULT NULL,
  `activity_percentage` float DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `internet_datasource_id` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=929195 DEFAULT CHARSET=utf8;

CREATE TABLE `point_statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` int(11) DEFAULT NULL,
  `street_percentage` float DEFAULT NULL,
  `postal_code_percentage` float DEFAULT NULL,
  `housenumber_percentage` float DEFAULT NULL,
  `city_percentage` float DEFAULT NULL,
  `country_percentage` float DEFAULT NULL,
  `food_type_percentage` float DEFAULT NULL,
  `brandname_percentage` float DEFAULT NULL,
  `x_percentage` float DEFAULT NULL,
  `y_percentage` float DEFAULT NULL,
  `activity_percentage` float DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `internet_datasource_id` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=611 DEFAULT CHARSET=utf8;

CREATE TABLE `point_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `point_id` int(11) DEFAULT NULL,
  `name_of_facility` varchar(255) DEFAULT NULL,
  `iso_language_code` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `absolute_xcoordinate` float(11,8) DEFAULT NULL,
  `absolute_ycoordinate` float(11,8) DEFAULT NULL,
  `importance` int(11) DEFAULT NULL,
  `brandname` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `house_number` varchar(255) DEFAULT NULL,
  `street_name` varchar(255) DEFAULT NULL,
  `city_name` varchar(255) DEFAULT NULL,
  `country_name` varchar(255) DEFAULT NULL,
  `state_name` varchar(255) DEFAULT NULL,
  `vanity_city_name` varchar(255) DEFAULT NULL,
  `vanity_city_xcoordinate` float DEFAULT NULL,
  `vanity_city_ycoordinate` float DEFAULT NULL,
  `datasource` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `process_log` varchar(255) DEFAULT NULL,
  `unmapped_address` varchar(255) DEFAULT NULL,
  `deeplink` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `checked_at` datetime DEFAULT NULL,
  `distance` int(11) DEFAULT NULL,
  `db1_coord_x` float DEFAULT NULL,
  `db1_coord_y` float DEFAULT NULL,
  `db1_bestconflvl` int(11) DEFAULT NULL,
  `db1_conflvl` int(11) DEFAULT NULL,
  `db1_matchtech` int(11) DEFAULT NULL,
  `db1_urbrur` varchar(255) DEFAULT NULL,
  `db1_diglvl` varchar(255) DEFAULT NULL,
  `db1_attrlvl` varchar(255) DEFAULT NULL,
  `db1_roadtype` varchar(255) DEFAULT NULL,
  `db1_matchlvl` int(11) DEFAULT NULL,
  `db1_dist` int(11) DEFAULT NULL,
  `db1_cou` varchar(255) DEFAULT NULL,
  `db1_citloc` varchar(255) DEFAULT NULL,
  `db1_cit` varchar(255) DEFAULT NULL,
  `db1_strloc` varchar(255) DEFAULT NULL,
  `db1_str` varchar(255) DEFAULT NULL,
  `db1_hnr` varchar(255) DEFAULT NULL,
  `db2_coord_x` float DEFAULT NULL,
  `db2_coord_y` float DEFAULT NULL,
  `db2_bestconflvl` int(11) DEFAULT NULL,
  `db2_conflvl` int(11) DEFAULT NULL,
  `db2_matchtech` int(11) DEFAULT NULL,
  `db2_urbrur` varchar(255) DEFAULT NULL,
  `db2_diglvl` varchar(255) DEFAULT NULL,
  `db2_attrlvl` varchar(255) DEFAULT NULL,
  `db2_roadtype` varchar(255) DEFAULT NULL,
  `db2_matchlvl` int(11) DEFAULT NULL,
  `db2_dist` int(11) DEFAULT NULL,
  `db2_cou` varchar(255) DEFAULT NULL,
  `db2_citloc` varchar(255) DEFAULT NULL,
  `db2_cit` varchar(255) DEFAULT NULL,
  `db2_strloc` varchar(255) DEFAULT NULL,
  `db2_str` varchar(255) DEFAULT NULL,
  `db2_hnr` varchar(255) DEFAULT NULL,
  `gm_extract_date` datetime DEFAULT NULL,
  `gm_upload_date` datetime DEFAULT NULL,
  `internet_datasource_id` int(11) DEFAULT NULL,
  `site_id` varchar(255) DEFAULT NULL,
  `facility_status` varchar(255) DEFAULT NULL,
  `food_type` varchar(255) DEFAULT NULL,
  `session` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_point_versions_on_point_id` (`point_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_of_facility` varchar(255) DEFAULT NULL,
  `iso_language_code` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `absolute_xcoordinate` float(11,8) DEFAULT NULL,
  `absolute_ycoordinate` float(11,8) DEFAULT NULL,
  `importance` int(11) DEFAULT NULL,
  `brandname` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `house_number` varchar(255) DEFAULT NULL,
  `street_name` varchar(255) DEFAULT NULL,
  `city_name` varchar(255) DEFAULT NULL,
  `country_name` varchar(255) DEFAULT NULL,
  `state_name` varchar(255) DEFAULT NULL,
  `vanity_city_name` varchar(255) DEFAULT NULL,
  `vanity_city_xcoordinate` float DEFAULT NULL,
  `vanity_city_ycoordinate` float DEFAULT NULL,
  `datasource` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `process_log` varchar(255) DEFAULT NULL,
  `unmapped_address` varchar(255) DEFAULT NULL,
  `deeplink` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `checked_at` datetime DEFAULT NULL,
  `distance` int(11) DEFAULT NULL,
  `db1_coord_x` float DEFAULT NULL,
  `db1_coord_y` float DEFAULT NULL,
  `db1_bestconflvl` int(11) DEFAULT NULL,
  `db1_conflvl` int(11) DEFAULT NULL,
  `db1_matchtech` int(11) DEFAULT NULL,
  `db1_urbrur` varchar(255) DEFAULT NULL,
  `db1_diglvl` varchar(255) DEFAULT NULL,
  `db1_attrlvl` varchar(255) DEFAULT NULL,
  `db1_roadtype` varchar(255) DEFAULT NULL,
  `db1_matchlvl` int(11) DEFAULT NULL,
  `db1_dist` int(11) DEFAULT NULL,
  `db1_cou` varchar(255) DEFAULT NULL,
  `db1_citloc` varchar(255) DEFAULT NULL,
  `db1_cit` varchar(255) DEFAULT NULL,
  `db1_strloc` varchar(255) DEFAULT NULL,
  `db1_str` varchar(255) DEFAULT NULL,
  `db1_hnr` varchar(255) DEFAULT NULL,
  `db2_coord_x` float DEFAULT NULL,
  `db2_coord_y` float DEFAULT NULL,
  `db2_bestconflvl` int(11) DEFAULT NULL,
  `db2_conflvl` int(11) DEFAULT NULL,
  `db2_matchtech` int(11) DEFAULT NULL,
  `db2_urbrur` varchar(255) DEFAULT NULL,
  `db2_diglvl` varchar(255) DEFAULT NULL,
  `db2_attrlvl` varchar(255) DEFAULT NULL,
  `db2_roadtype` varchar(255) DEFAULT NULL,
  `db2_matchlvl` int(11) DEFAULT NULL,
  `db2_dist` int(11) DEFAULT NULL,
  `db2_cou` varchar(255) DEFAULT NULL,
  `db2_citloc` varchar(255) DEFAULT NULL,
  `db2_cit` varchar(255) DEFAULT NULL,
  `db2_strloc` varchar(255) DEFAULT NULL,
  `db2_str` varchar(255) DEFAULT NULL,
  `db2_hnr` varchar(255) DEFAULT NULL,
  `gm_extract_date` datetime DEFAULT NULL,
  `gm_upload_date` datetime DEFAULT NULL,
  `internet_datasource_id` int(11) DEFAULT NULL,
  `last_activity_date` datetime DEFAULT NULL,
  `country_code` varchar(255) DEFAULT NULL,
  `facility_status` varchar(255) DEFAULT NULL,
  `food_type` varchar(255) DEFAULT NULL,
  `site_id` varchar(255) DEFAULT NULL,
  `details` text,
  `session` datetime DEFAULT NULL,
  `parent_name` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `parent_url` varchar(255) DEFAULT NULL,
  `parent_email_address` varchar(255) DEFAULT NULL,
  `super_point_id` int(11) DEFAULT NULL,
  `fax_number` varchar(255) DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `released_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_points_on_name_and_datasource_and_street_and_house_nr` (`datasource`,`country_name`,`city_name`,`street_name`),
  KEY `index_points_on_internet_datasource_id` (`internet_datasource_id`),
  KEY `index_points_on_internet_datasource_id_and_postal_code` (`internet_datasource_id`,`postal_code`),
  KEY `index_points_on_internet_datasource_id_and_house_number` (`internet_datasource_id`,`house_number`),
  KEY `index_points_on_internet_datasource_id_and_street_name` (`internet_datasource_id`,`street_name`),
  KEY `index_points_on_internet_datasource_id_and_city_name` (`internet_datasource_id`,`city_name`),
  KEY `index_points_on_internet_datasource_id_and_country_name` (`internet_datasource_id`,`country_name`),
  KEY `index_points_on_internet_datasource_id_and_food_type` (`internet_datasource_id`,`food_type`),
  KEY `index_points_on_internet_datasource_id_and_brandname` (`internet_datasource_id`,`brandname`),
  KEY `index_points_on_internet_datasource_id_and_absolute_xcoordinate` (`internet_datasource_id`,`absolute_xcoordinate`),
  KEY `index_points_on_internet_datasource_id_and_absolute_ycoordinate` (`internet_datasource_id`,`absolute_ycoordinate`),
  KEY `index_points_on_internet_datasource_id_and_last_activity_date` (`internet_datasource_id`,`last_activity_date`),
  KEY `cat_country` (`country_code`,`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `points_export` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_of_facility` varchar(255) DEFAULT NULL,
  `iso_language_code` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `absolute_xcoordinate` float(11,8) DEFAULT NULL,
  `absolute_ycoordinate` float(11,8) DEFAULT NULL,
  `importance` int(11) DEFAULT NULL,
  `brandname` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `house_number` varchar(255) DEFAULT NULL,
  `street_name` varchar(255) DEFAULT NULL,
  `city_name` varchar(255) DEFAULT NULL,
  `country_name` varchar(255) DEFAULT NULL,
  `state_name` varchar(255) DEFAULT NULL,
  `vanity_city_name` varchar(255) DEFAULT NULL,
  `vanity_city_xcoordinate` float DEFAULT NULL,
  `vanity_city_ycoordinate` float DEFAULT NULL,
  `datasource` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `process_log` varchar(255) DEFAULT NULL,
  `unmapped_address` varchar(255) DEFAULT NULL,
  `deeplink` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `checked_at` datetime DEFAULT NULL,
  `distance` int(11) DEFAULT NULL,
  `db1_coord_x` float DEFAULT NULL,
  `db1_coord_y` float DEFAULT NULL,
  `db1_bestconflvl` int(11) DEFAULT NULL,
  `db1_conflvl` int(11) DEFAULT NULL,
  `db1_matchtech` int(11) DEFAULT NULL,
  `db1_urbrur` varchar(255) DEFAULT NULL,
  `db1_diglvl` varchar(255) DEFAULT NULL,
  `db1_attrlvl` varchar(255) DEFAULT NULL,
  `db1_roadtype` varchar(255) DEFAULT NULL,
  `db1_matchlvl` int(11) DEFAULT NULL,
  `db1_dist` int(11) DEFAULT NULL,
  `db1_cou` varchar(255) DEFAULT NULL,
  `db1_citloc` varchar(255) DEFAULT NULL,
  `db1_cit` varchar(255) DEFAULT NULL,
  `db1_strloc` varchar(255) DEFAULT NULL,
  `db1_str` varchar(255) DEFAULT NULL,
  `db1_hnr` varchar(255) DEFAULT NULL,
  `db2_coord_x` float DEFAULT NULL,
  `db2_coord_y` float DEFAULT NULL,
  `db2_bestconflvl` int(11) DEFAULT NULL,
  `db2_conflvl` int(11) DEFAULT NULL,
  `db2_matchtech` int(11) DEFAULT NULL,
  `db2_urbrur` varchar(255) DEFAULT NULL,
  `db2_diglvl` varchar(255) DEFAULT NULL,
  `db2_attrlvl` varchar(255) DEFAULT NULL,
  `db2_roadtype` varchar(255) DEFAULT NULL,
  `db2_matchlvl` int(11) DEFAULT NULL,
  `db2_dist` int(11) DEFAULT NULL,
  `db2_cou` varchar(255) DEFAULT NULL,
  `db2_citloc` varchar(255) DEFAULT NULL,
  `db2_cit` varchar(255) DEFAULT NULL,
  `db2_strloc` varchar(255) DEFAULT NULL,
  `db2_str` varchar(255) DEFAULT NULL,
  `db2_hnr` varchar(255) DEFAULT NULL,
  `gm_extract_date` datetime DEFAULT NULL,
  `gm_upload_date` datetime DEFAULT NULL,
  `internet_datasource_id` int(11) DEFAULT NULL,
  `last_activity_date` datetime DEFAULT NULL,
  `country_code` varchar(255) DEFAULT NULL,
  `facility_status` varchar(255) DEFAULT NULL,
  `food_type` varchar(255) DEFAULT NULL,
  `site_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_points_on_name_and_datasource_and_street_and_house_nr` (`datasource`,`country_name`,`city_name`,`street_name`),
  KEY `index_points_on_internet_datasource_id` (`internet_datasource_id`),
  KEY `index_points_on_internet_datasource_id_and_postal_code` (`internet_datasource_id`,`postal_code`),
  KEY `index_points_on_internet_datasource_id_and_house_number` (`internet_datasource_id`,`house_number`),
  KEY `index_points_on_internet_datasource_id_and_street_name` (`internet_datasource_id`,`street_name`),
  KEY `index_points_on_internet_datasource_id_and_city_name` (`internet_datasource_id`,`city_name`),
  KEY `index_points_on_internet_datasource_id_and_country_name` (`internet_datasource_id`,`country_name`),
  KEY `index_points_on_internet_datasource_id_and_food_type` (`internet_datasource_id`,`food_type`),
  KEY `index_points_on_internet_datasource_id_and_brandname` (`internet_datasource_id`,`brandname`),
  KEY `index_points_on_internet_datasource_id_and_absolute_xcoordinate` (`internet_datasource_id`,`absolute_xcoordinate`),
  KEY `index_points_on_internet_datasource_id_and_absolute_ycoordinate` (`internet_datasource_id`,`absolute_ycoordinate`),
  KEY `index_points_on_internet_datasource_id_and_last_activity_date` (`internet_datasource_id`,`last_activity_date`)
) ENGINE=InnoDB AUTO_INCREMENT=1557645 DEFAULT CHARSET=utf8;

CREATE TABLE `postcodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `country_code` varchar(255) DEFAULT NULL,
  `city_name` varchar(255) DEFAULT NULL,
  `street_name` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=116009 DEFAULT CHARSET=utf8;

CREATE TABLE `process_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `storage_location` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=274 DEFAULT CHARSET=utf8;

CREATE TABLE `process_data_elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `process_data_item_id` int(11) DEFAULT NULL,
  `reception_id` int(11) DEFAULT NULL,
  `process_data_specification_id` int(11) DEFAULT NULL,
  `remark` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `removed_yn` tinyint(1) DEFAULT '0',
  `split_by_value` varchar(255) DEFAULT NULL,
  `directory` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=453 DEFAULT CHARSET=utf8;

CREATE TABLE `process_data_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE `process_data_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `process_data_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `supplier_name` text,
  `datatype` text,
  `area_name` text,
  `data_release` text,
  `filling` text,
  `picked_reception_ids` varchar(255) DEFAULT NULL,
  `poi_mapping_layout_id` int(11) DEFAULT NULL,
  `data_set_definition_id` int(11) DEFAULT NULL,
  `data_set_definition_version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8;

CREATE TABLE `process_data_lines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `process_data_id` int(11) DEFAULT NULL,
  `process_data_sub_event_id` int(11) DEFAULT NULL,
  `reception_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `relation_line_id` int(11) DEFAULT NULL,
  `type_relation` int(11) DEFAULT NULL,
  `log_forward` text,
  `log_backward` text,
  `document_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `cr_reference` varchar(255) DEFAULT NULL,
  `auto_code` tinyint(1) DEFAULT NULL,
  `change_version` varchar(255) DEFAULT NULL,
  `process_data_element_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=548 DEFAULT CHARSET=utf8;

CREATE TABLE `process_data_specifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `directory` varchar(255) DEFAULT NULL,
  `script_tag` varchar(255) DEFAULT NULL,
  `script_parameter` varchar(255) DEFAULT NULL,
  `description` text,
  `directory_level_yn` tinyint(1) DEFAULT NULL,
  `active_yn` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `split_by_key` varchar(255) DEFAULT NULL,
  `data_type_id` int(11) DEFAULT NULL,
  `filling` varchar(255) DEFAULT NULL,
  `include_in_all_regions` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

CREATE TABLE `process_data_statuslogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `process_data_id` int(11) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2064 DEFAULT CHARSET=utf8;

CREATE TABLE `process_data_sub_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `process_data_event_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `process_transitions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ordertype_id` int(11) DEFAULT NULL,
  `from_bp` int(11) DEFAULT NULL,
  `to_bp` int(11) DEFAULT NULL,
  `validation` text,
  `from_level` varchar(255) DEFAULT NULL,
  `to_level` varchar(255) DEFAULT NULL,
  `action_label` varchar(255) DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `cascade_to_level` varchar(255) DEFAULT NULL,
  `cascade_to_bp` int(11) DEFAULT NULL,
  `cascade_type` varchar(255) DEFAULT NULL,
  `succes_transition` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=latin1;

CREATE TABLE `product_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `abbreviation` varchar(255) DEFAULT NULL,
  `description` text,
  `active` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `pds_import_file` text,
  `attach_pds_in_rn_yn` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

CREATE TABLE `product_content_item_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_content_item_id` int(11) DEFAULT NULL,
  `product_content_specification_id` varchar(255) DEFAULT NULL,
  `source_data_type_id` int(11) DEFAULT NULL,
  `source_data_release_id` int(11) DEFAULT NULL,
  `source_region_code_id` int(11) DEFAULT NULL,
  `destination_region_code` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `resource_identification` varchar(255) DEFAULT NULL,
  `incr_update_yn` tinyint(1) DEFAULT NULL,
  `regional_update_yn` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

CREATE TABLE `product_content_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_content_specification_id` varchar(255) DEFAULT NULL,
  `source_data_type_id` int(11) DEFAULT NULL,
  `source_data_release_id` int(11) DEFAULT NULL,
  `source_region_code_id` int(11) DEFAULT NULL,
  `destination_region_code` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `filling` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `resource_identification` varchar(255) DEFAULT NULL,
  `incr_update_yn` tinyint(1) DEFAULT NULL,
  `regional_update_yn` tinyint(1) DEFAULT NULL,
  `resource_identification_iu` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8;

CREATE TABLE `product_content_specification_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_content_specification_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sync_filename` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

CREATE TABLE `product_content_specifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `sync_filename` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

CREATE TABLE `product_data_set_infos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `remarks` text,
  `datatype_id` int(11) DEFAULT NULL,
  `area_name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `company_abbr_id` int(11) DEFAULT NULL,
  `data_release` varchar(255) DEFAULT NULL,
  `data_info` varchar(255) DEFAULT NULL,
  `major_dataset` tinyint(1) DEFAULT NULL,
  `filling` varchar(255) DEFAULT NULL,
  `coverage_id` int(11) DEFAULT NULL,
  `data_set_definition_id` int(11) DEFAULT NULL,
  `data_set_definition_version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dba_product_data_set_infos_product_id_idx` (`product_id`),
  KEY `index_product_data_set_infos_on_product_id` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=335402 DEFAULT CHARSET=utf8;

CREATE TABLE `product_design_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_design_id` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `design_tag_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `model` longtext,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `bug_tracker_id` varchar(255) DEFAULT NULL,
  `comment` text,
  `design_type` varchar(255) DEFAULT NULL,
  `product_design_md5` varchar(255) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1551 DEFAULT CHARSET=utf8;

CREATE TABLE `product_designs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `model` longtext,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `design_tag_id` int(11) DEFAULT NULL,
  `bug_tracker_id` varchar(255) DEFAULT NULL,
  `comment` text,
  `status` varchar(255) DEFAULT NULL,
  `lock_version` int(11) DEFAULT '0',
  `region_id` int(11) DEFAULT NULL,
  `company_abbr_id` int(11) DEFAULT NULL,
  `data_release_id` int(11) DEFAULT NULL,
  `design_type` varchar(255) DEFAULT NULL,
  `product_design_md5` varchar(255) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

CREATE TABLE `product_feature_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_feature_id` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `abbr_code` varchar(255) DEFAULT NULL,
  `system_translation` longtext,
  `translation_type` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `bug_tracker_id` varchar(255) DEFAULT NULL,
  `comment` text,
  `status` varchar(255) DEFAULT NULL,
  `platform_setting_id` int(11) DEFAULT NULL,
  `platform_setting_version` int(11) DEFAULT NULL,
  `product_line_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8;

CREATE TABLE `product_features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `abbr_code` varchar(255) DEFAULT NULL,
  `description` text,
  `system_translation` text,
  `translation_type` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `version` int(11) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `bug_tracker_id` varchar(255) DEFAULT NULL,
  `comment` text,
  `status` varchar(255) DEFAULT NULL,
  `platform_setting_id` int(11) DEFAULT NULL,
  `platform_setting_version` int(11) DEFAULT NULL,
  `product_line_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

CREATE TABLE `product_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` varchar(255) DEFAULT NULL,
  `conversion_id` int(11) DEFAULT NULL,
  `product_format` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `supplier` varchar(255) DEFAULT NULL,
  `data_release` varchar(255) DEFAULT NULL,
  `data_path` text,
  `conversiontool` varchar(255) DEFAULT NULL,
  `registration_date` date DEFAULT NULL,
  `storage_path` text,
  `remarks` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `product_type` varchar(255) DEFAULT NULL,
  `internal_storage_path` text,
  `reception_id` int(11) DEFAULT NULL,
  `checksum_storage_file` varchar(255) DEFAULT NULL,
  `release_sequence` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=603 DEFAULT CHARSET=utf8;

CREATE TABLE `product_release_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `production_order_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `document_id` int(11) DEFAULT NULL,
  `production_orderline_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=utf8;

CREATE TABLE `product_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `customer_id` varchar(255) DEFAULT NULL,
  `volumeid` varchar(255) DEFAULT NULL,
  `dateeffective` datetime DEFAULT NULL,
  `detailedcoverage` text,
  `hwncoverage` text,
  `thirdpartydata1` text,
  `thirdpartydata2` text,
  `tmclocation` text,
  `tmcevent` varchar(255) DEFAULT NULL,
  `remarks` text,
  `zip` text,
  `qxs` text,
  `speedlimits` text,
  `namerotations` text,
  `medium_name` varchar(255) DEFAULT NULL,
  `nds_name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `tpd_file` varchar(255) DEFAULT NULL,
  `test_requirement` text,
  `lock_version` int(11) DEFAULT NULL,
  `conversion_settings` text,
  `removed_yn` tinyint(1) DEFAULT NULL,
  `last_change` varchar(255) DEFAULT NULL,
  `copyright_name` varchar(255) DEFAULT NULL,
  `primary_coverage_id` int(11) DEFAULT NULL,
  `secondary_coverage_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `poi_mapping_layout_id` int(11) DEFAULT NULL,
  `update_region_mapping_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_from` varchar(255) DEFAULT NULL,
  `file_detect_regex` varchar(255) DEFAULT NULL,
  `product_line_id` int(11) DEFAULT NULL,
  `predecessor_id` int(11) DEFAULT NULL,
  `import_disabled` tinyint(1) DEFAULT NULL,
  `product_design_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dba_product_versions_product_id_idx` (`product_id`),
  KEY `index_product_versions_on_product_id_and_version` (`product_id`,`version`)
) ENGINE=InnoDB AUTO_INCREMENT=190261 DEFAULT CHARSET=utf8;

CREATE TABLE `production_orderline_conversion_databases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `production_orderline_id` int(11) DEFAULT NULL,
  `conversion_database_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32539 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `production_orderlines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `production_order_id` int(11) DEFAULT NULL,
  `product_id` varchar(255) DEFAULT NULL,
  `linestatus` varchar(255) DEFAULT NULL,
  `bp_name` varchar(255) DEFAULT NULL,
  `bp_status` varchar(255) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `remarks` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `bp_seqnr` int(11) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `test_requirement` text,
  `product_accepted` varchar(255) DEFAULT NULL,
  `customer_feedback` text,
  `product_design` longtext,
  `bp_message` mediumtext,
  `design_sequence` int(11) DEFAULT NULL,
  `chosen_conversion_id` int(11) DEFAULT NULL,
  `created_from` varchar(255) DEFAULT NULL,
  `parent_linenr` int(11) DEFAULT NULL,
  `product_location_id` int(11) DEFAULT NULL,
  `force_create` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_production_orderlines_on_product_id` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2857 DEFAULT CHARSET=utf8;

CREATE TABLE `production_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productset_id` int(11) DEFAULT NULL,
  `major` int(11) DEFAULT NULL,
  `minor` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `requestor` varchar(255) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `comments` text,
  `completion_date` datetime DEFAULT NULL,
  `request_date` datetime DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `ordertype_id` int(11) DEFAULT NULL,
  `bp_seqnr` int(11) DEFAULT NULL,
  `test_requirement` text,
  `datarelease_name` varchar(255) DEFAULT NULL,
  `conversiontool_name` varchar(255) DEFAULT NULL,
  `allow_planning` tinyint(1) DEFAULT NULL,
  `allow_production` tinyint(1) DEFAULT NULL,
  `allow_shipment` tinyint(1) DEFAULT NULL,
  `on_hold` tinyint(1) DEFAULT NULL,
  `supplier_name` varchar(255) DEFAULT NULL,
  `region_name` varchar(255) DEFAULT NULL,
  `data_release_id` int(11) DEFAULT NULL,
  `planned_shipdate` date DEFAULT NULL,
  `committed_del_date` datetime DEFAULT NULL,
  `rel_notes_requested_date` date DEFAULT NULL,
  `rel_notes_requested_by_id` int(11) DEFAULT NULL,
  `rel_notes_send_date` date DEFAULT NULL,
  `rel_notes_send_by_id` int(11) DEFAULT NULL,
  `distribution_list_id` int(11) DEFAULT NULL,
  `bp_failreason` varchar(255) DEFAULT NULL,
  `bp_name` varchar(255) DEFAULT NULL,
  `bp_status` varchar(255) DEFAULT NULL,
  `create_frontend_order_yn` tinyint(1) DEFAULT NULL,
  `allocate_by_update_region_yn` tinyint(1) DEFAULT NULL,
  `process_transitions_snapshot` longtext,
  `use_test_versions_yn` tinyint(1) DEFAULT NULL,
  `use_dakota_simulator_yn` tinyint(1) DEFAULT '0',
  `project_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1502 DEFAULT CHARSET=utf8;

CREATE TABLE `production_test_cases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `create_process_data` tinyint(1) DEFAULT NULL,
  `use_daksim` tinyint(1) DEFAULT NULL,
  `schedule` varchar(255) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `remove_results` tinyint(1) DEFAULT NULL,
  `remove_process_data` tinyint(1) DEFAULT NULL,
  `expire_time` varchar(255) DEFAULT NULL,
  `tool_select` varchar(255) DEFAULT NULL,
  `synchronous` tinyint(1) DEFAULT NULL,
  `design_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `is_frontend` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `customer_id` varchar(255) DEFAULT NULL,
  `volumeid` varchar(255) DEFAULT NULL,
  `dateeffective` datetime DEFAULT NULL,
  `detailedcoverage` text,
  `hwncoverage` text,
  `thirdpartydata1` text,
  `thirdpartydata2` text,
  `tmclocation` text,
  `tmcevent` varchar(255) DEFAULT NULL,
  `remarks` text,
  `zip` text,
  `qxs` text,
  `speedlimits` text,
  `namerotations` text,
  `medium_name` varchar(255) DEFAULT NULL,
  `nds_name` varchar(255) DEFAULT NULL,
  `lock_version` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `tpd_file` varchar(255) DEFAULT NULL,
  `test_requirement` text,
  `conversion_settings` text,
  `removed_yn` tinyint(1) DEFAULT NULL,
  `last_change` varchar(255) DEFAULT NULL,
  `copyright_name` varchar(255) DEFAULT NULL,
  `primary_coverage_id` int(11) DEFAULT NULL,
  `secondary_coverage_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `poi_mapping_layout_id` int(11) DEFAULT NULL,
  `update_region_mapping_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_from` varchar(255) DEFAULT NULL,
  `file_detect_regex` varchar(255) DEFAULT NULL,
  `product_line_id` int(11) DEFAULT NULL,
  `predecessor_id` int(11) DEFAULT NULL,
  `import_disabled` tinyint(1) DEFAULT NULL,
  `product_design_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3310 DEFAULT CHARSET=utf8;

CREATE TABLE `productset_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productset_id` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `supplier_id` varchar(255) DEFAULT NULL,
  `customer_id` varchar(255) DEFAULT NULL,
  `firsttier_id` varchar(255) DEFAULT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `firsttiername` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `major` int(11) DEFAULT NULL,
  `minor` int(11) DEFAULT NULL,
  `dateeffective` date DEFAULT NULL,
  `setchanges` text,
  `remarks` text,
  `internal_name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `datarelease_id` int(11) DEFAULT NULL,
  `datarelease_name` varchar(255) DEFAULT NULL,
  `platform_name` varchar(255) DEFAULT NULL,
  `ciq` varchar(255) DEFAULT NULL,
  `nds_name` varchar(255) DEFAULT NULL,
  `area_name` varchar(255) DEFAULT NULL,
  `shippingmethod_name` varchar(255) DEFAULT NULL,
  `launchdate` date DEFAULT NULL,
  `underconstructiondate` varchar(255) DEFAULT NULL,
  `conversiontool_name` varchar(255) DEFAULT NULL,
  `medium_name` varchar(255) DEFAULT NULL,
  `approval_text` text,
  `status` varchar(255) DEFAULT NULL,
  `supplier_name` varchar(255) DEFAULT NULL,
  `tpdspec` varchar(255) DEFAULT NULL,
  `predecessor` varchar(255) DEFAULT NULL,
  `setcode` varchar(255) DEFAULT NULL,
  `tmc_file` varchar(255) DEFAULT NULL,
  `tmc_info_id` int(11) DEFAULT NULL,
  `tmc_info_name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `tpd_file` varchar(255) DEFAULT NULL,
  `datasource_extrainfo` varchar(255) DEFAULT NULL,
  `test_requirement` text,
  `lock_version` int(11) DEFAULT NULL,
  `settype` varchar(255) DEFAULT NULL,
  `areaabbr` varchar(255) DEFAULT NULL,
  `contabbr` varchar(255) DEFAULT NULL,
  `removed_yn` tinyint(1) DEFAULT NULL,
  `output_format` varchar(255) DEFAULT NULL,
  `output_format_extrainfo` varchar(255) DEFAULT NULL,
  `compressed_yn` tinyint(1) DEFAULT NULL,
  `bundled_yn` tinyint(1) DEFAULT NULL,
  `image_yn` tinyint(1) DEFAULT NULL,
  `physical_medium_yn` tinyint(1) DEFAULT NULL,
  `conversion_settings` text,
  `set_release_major` varchar(255) DEFAULT NULL,
  `set_release_minor` varchar(255) DEFAULT NULL,
  `product_category_id` int(11) DEFAULT NULL,
  `company_abbr_id` int(11) DEFAULT NULL,
  `data_set_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_productset_versions_on_productset_id_and_version` (`productset_id`,`version`)
) ENGINE=InnoDB AUTO_INCREMENT=113437 DEFAULT CHARSET=utf8;

CREATE TABLE `productsets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `supplier_id` varchar(255) DEFAULT NULL,
  `customer_id` varchar(255) DEFAULT NULL,
  `firsttier_id` varchar(255) DEFAULT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `firsttiername` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `major` int(11) DEFAULT NULL,
  `minor` int(11) DEFAULT NULL,
  `dateeffective` date DEFAULT NULL,
  `setchanges` text,
  `remarks` text,
  `internal_name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `datarelease_id` int(11) DEFAULT NULL,
  `datarelease_name` varchar(255) DEFAULT NULL,
  `platform_name` varchar(255) DEFAULT NULL,
  `ciq` varchar(255) DEFAULT NULL,
  `nds_name` varchar(255) DEFAULT NULL,
  `area_name` varchar(255) DEFAULT NULL,
  `shippingmethod_name` varchar(255) DEFAULT NULL,
  `launchdate` date DEFAULT NULL,
  `underconstructiondate` varchar(255) DEFAULT NULL,
  `conversiontool_name` varchar(255) DEFAULT NULL,
  `medium_name` varchar(255) DEFAULT NULL,
  `approval_text` text,
  `status` varchar(255) DEFAULT NULL,
  `supplier_name` varchar(255) DEFAULT NULL,
  `tpdspec` varchar(255) DEFAULT NULL,
  `predecessor` varchar(255) DEFAULT NULL,
  `setcode` varchar(255) DEFAULT NULL,
  `tmc_file` varchar(255) DEFAULT NULL,
  `tmc_info_id` int(11) DEFAULT NULL,
  `tmc_info_name` varchar(255) DEFAULT NULL,
  `lock_version` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `tpd_file` varchar(255) DEFAULT NULL,
  `datasource_extrainfo` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `test_requirement` text,
  `settype` varchar(255) DEFAULT NULL,
  `areaabbr` varchar(255) DEFAULT NULL,
  `contabbr` varchar(255) DEFAULT NULL,
  `removed_yn` tinyint(1) DEFAULT NULL,
  `output_format` varchar(255) DEFAULT NULL,
  `output_format_extrainfo` varchar(255) DEFAULT NULL,
  `compressed_yn` tinyint(1) DEFAULT NULL,
  `bundled_yn` tinyint(1) DEFAULT NULL,
  `image_yn` tinyint(1) DEFAULT NULL,
  `physical_medium_yn` tinyint(1) DEFAULT NULL,
  `conversion_settings` text,
  `set_release_major` varchar(255) DEFAULT NULL,
  `set_release_minor` varchar(255) DEFAULT NULL,
  `product_category_id` int(11) DEFAULT NULL,
  `company_abbr_id` int(11) DEFAULT NULL,
  `data_set_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1875 DEFAULT CHARSET=utf8;

CREATE TABLE `project_servers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `server_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;

CREATE TABLE `project_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=271 DEFAULT CHARSET=latin1;

CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `status` int(11) DEFAULT NULL,
  `environment_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

CREATE TABLE `purchase_orderlines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_order_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `unit_price` decimal(8,2) DEFAULT NULL,
  `item_description` text,
  `item_reference` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

CREATE TABLE `purchase_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_reference` varchar(255) DEFAULT NULL,
  `date_ordered` date DEFAULT NULL,
  `date_paid` date DEFAULT NULL,
  `date_received` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `remarks` text,
  `deleted` tinyint(1) DEFAULT NULL,
  `external_contact_id` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE `reception_lines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `reception_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=119284 DEFAULT CHARSET=utf8;

CREATE TABLE `receptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datasource_id` int(11) DEFAULT NULL,
  `data_release_id` int(11) DEFAULT NULL,
  `data_medium_id` int(11) DEFAULT NULL,
  `dataversion` varchar(255) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `data_type_id` varchar(255) DEFAULT NULL,
  `referenceid` varchar(255) DEFAULT NULL,
  `reception_date` date DEFAULT NULL,
  `purpose` varchar(255) DEFAULT NULL,
  `remarks` text,
  `storage_location` mediumtext,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `inbound_order_id` int(11) DEFAULT NULL,
  `area_type` varchar(255) DEFAULT NULL,
  `send_status` tinyint(1) DEFAULT NULL,
  `sendto` text,
  `senddate` datetime DEFAULT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `conversiontool` varchar(255) DEFAULT NULL,
  `region_name` varchar(255) DEFAULT NULL,
  `removed_yn` tinyint(1) DEFAULT NULL,
  `validated_yn` tinyint(1) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `expected_arrival_date` datetime DEFAULT NULL,
  `expected_reception_date` datetime DEFAULT NULL,
  `priority` varchar(255) DEFAULT NULL,
  `data_intake_requested` tinyint(1) DEFAULT NULL,
  `data_intake_ok` tinyint(1) DEFAULT NULL,
  `data_intake_result_text` text,
  `validation_requested` tinyint(4) DEFAULT NULL,
  `validation_result_text` text,
  `validated_by` varchar(255) DEFAULT NULL,
  `data_intake_by` varchar(255) DEFAULT NULL,
  `ruleset` varchar(255) DEFAULT NULL,
  `source_location` text,
  `rejection_remark` varchar(255) DEFAULT NULL,
  `request_remarks` text,
  `requested_by` varchar(255) DEFAULT NULL,
  `references` text,
  `product_id` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `can_be_removed_yn` tinyint(1) DEFAULT NULL,
  `persistent_yn` tinyint(1) DEFAULT NULL,
  `removal_mark_by_username` varchar(255) DEFAULT NULL,
  `removed_by_username` varchar(255) DEFAULT NULL,
  `data_set_definition_id` int(11) DEFAULT NULL,
  `data_set_definition_version` int(11) DEFAULT NULL,
  `filling_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `area_id_reception` (`area_id`),
  KEY `receptions_data_release_id` (`data_release_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25075 DEFAULT CHARSET=utf8;

CREATE TABLE `region_hierarchies` (
  `ancestor_id` int(11) NOT NULL,
  `descendant_id` int(11) NOT NULL,
  `generations` int(11) NOT NULL,
  UNIQUE KEY `index_region_hierarchies_on_ancestor_id_and_descendant_id` (`ancestor_id`,`descendant_id`),
  KEY `index_region_hierarchies_on_descendant_id` (`descendant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `region_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `region_id` int(11) DEFAULT NULL,
  `app_module_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=763 DEFAULT CHARSET=utf8;

CREATE TABLE `region_relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `region_id` int(11) DEFAULT NULL,
  `child_region_id` int(11) DEFAULT NULL,
  `relation` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8487 DEFAULT CHARSET=utf8;

CREATE TABLE `regions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `population` int(11) DEFAULT NULL,
  `continent_code` varchar(255) DEFAULT NULL,
  `country_iso_code` varchar(255) DEFAULT NULL,
  `country_iso3_code` varchar(255) DEFAULT NULL,
  `country_num_code` varchar(255) DEFAULT NULL,
  `city_iso_code` varchar(255) DEFAULT NULL,
  `city_ascii_name` varchar(255) DEFAULT NULL,
  `state_code` varchar(255) DEFAULT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  `area_code` varchar(255) DEFAULT NULL,
  `supplier_id` varchar(255) DEFAULT NULL,
  `display_type_id` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `ruby_type` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `dakota_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_regions_on_name_and_country_iso_code_and_country_iso3_code` (`name`,`country_iso_code`,`country_iso3_code`)
) ENGINE=InnoDB AUTO_INCREMENT=2754945 DEFAULT CHARSET=utf8;

CREATE TABLE `release_note_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `active_yn` tinyint(1) DEFAULT NULL,
  `removed_yn` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `package_directory` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

CREATE TABLE `release_note_attachments_product_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `release_note_attachment_id` int(11) DEFAULT NULL,
  `product_category_id` int(11) DEFAULT NULL,
  `mandatory_yn` tinyint(1) DEFAULT NULL,
  `removed_yn` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE `remove` (
  `fieldname` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `report_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `font` varchar(255) DEFAULT NULL,
  `header` text,
  `footer` text,
  `body` text,
  `page_numbering` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `role_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `controller_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `action_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

CREATE TABLE `roles_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `module` varchar(255) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=761 DEFAULT CHARSET=utf8;

CREATE TABLE `routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_of_route` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `coordinates` mediumtext,
  `raw_data` mediumtext,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `iso_language_code` varchar(255) DEFAULT NULL,
  `datasource` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `deeplink` varchar(255) DEFAULT NULL,
  `checked_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=utf8;

CREATE TABLE `scenarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `query` text,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `name_of_poi` tinyint(1) DEFAULT NULL,
  `streetname` tinyint(1) DEFAULT NULL,
  `housenumber` tinyint(1) DEFAULT NULL,
  `phonenumber` tinyint(1) DEFAULT NULL,
  `phonenumber_exists` tinyint(1) DEFAULT NULL,
  `remark` tinyint(1) DEFAULT NULL,
  `poi_exists` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

CREATE TABLE `scheduler_jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `schedule` varchar(255) DEFAULT NULL,
  `processor_type` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `date_last_run` datetime DEFAULT NULL,
  `dataprocessor_id` varchar(255) DEFAULT NULL,
  `internet_datasource_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `contract_id` int(11) DEFAULT NULL,
  `scheduler_id` int(11) DEFAULT NULL,
  `syncstatus` int(11) DEFAULT NULL,
  `last_position` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=326 DEFAULT CHARSET=utf8;

CREATE TABLE `scheduler_locks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `process` varchar(255) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `server` varchar(255) DEFAULT NULL,
  `lock_version` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_scheduler_locks_on_process` (`process`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

CREATE TABLE `schema_info` (
  `version` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `schema_migrations` (
  `version` varchar(255) NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `seq_ints` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `current_value` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `lock_version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_seq_ints_on_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=450 DEFAULT CHARSET=utf8;

CREATE TABLE `servers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server_id` varchar(255) DEFAULT NULL,
  `server_remark` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `diskcapacity` varchar(255) DEFAULT NULL,
  `processor` varchar(255) DEFAULT NULL,
  `additionalprocess` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `down` tinyint(1) DEFAULT NULL,
  `max_jobs` int(11) DEFAULT NULL,
  `scheduling_allowed_yn` tinyint(1) DEFAULT NULL,
  `cores` int(11) DEFAULT NULL,
  `memory` int(11) DEFAULT NULL,
  `virtual_host` varchar(255) DEFAULT NULL,
  `owned_by_project_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=utf8;

CREATE TABLE `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(255) NOT NULL,
  `data` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_sessions_on_session_id` (`session_id`),
  KEY `index_sessions_on_updated_at` (`updated_at`)
) ENGINE=InnoDB AUTO_INCREMENT=18819 DEFAULT CHARSET=utf8;

CREATE TABLE `shipment_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

CREATE TABLE `shipping_order_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipping_order_id` int(11) DEFAULT NULL,
  `shipping_orderline_id` int(11) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `remarks` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2362 DEFAULT CHARSET=utf8;

CREATE TABLE `shipping_orderlines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipping_order_id` int(11) DEFAULT NULL,
  `stock_order_id` int(11) DEFAULT NULL,
  `call_off_reference` varchar(255) DEFAULT NULL,
  `delivery_date` date DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `remarks` text,
  `deleted` tinyint(1) DEFAULT NULL,
  `article_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `extern_reference` varchar(255) DEFAULT NULL,
  `transport_reference` varchar(255) DEFAULT NULL,
  `lock_version` int(11) DEFAULT '0',
  `amount_requested` int(11) DEFAULT NULL,
  `invoice_reference` varchar(255) DEFAULT NULL,
  `invoice_remarks` text,
  `payment_due_date` date DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=415 DEFAULT CHARSET=utf8;

CREATE TABLE `shipping_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) DEFAULT NULL,
  `po_reference` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `invoice_send_yn` tinyint(1) DEFAULT NULL,
  `attachment_org` blob,
  `source` varchar(255) DEFAULT NULL,
  `initiated_by_user_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `lock_version` int(11) DEFAULT '0',
  `customer_depot_id` int(11) DEFAULT NULL,
  `ordering_customer_depot_id` int(11) DEFAULT NULL,
  `invoicing_customer_depot_id` int(11) DEFAULT NULL,
  `shipping_customer_depot_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;

CREATE TABLE `shipping_specifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `external_contact_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_line_id` int(11) DEFAULT NULL,
  `packing_specification_id` int(11) DEFAULT NULL,
  `distribution_list_id` int(11) DEFAULT NULL,
  `shipment_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `courier` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_transfer_account_id` int(11) DEFAULT NULL,
  `default_filename_specification` int(11) DEFAULT NULL,
  `default_subdirectory_specification` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `space_reserved` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_size` int(11) DEFAULT NULL,
  `buffer_size` int(11) DEFAULT NULL,
  `reserved_size` int(11) DEFAULT NULL,
  `reserved_amount` int(11) DEFAULT NULL,
  `lock_version` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1379 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(255) DEFAULT NULL,
  `region_code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `country_id` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_states_on_name_and_country_code` (`name`,`country_code`)
) ENGINE=InnoDB AUTO_INCREMENT=8412 DEFAULT CHARSET=utf8;

CREATE TABLE `static_release_note_titles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `required_yn` tinyint(1) DEFAULT NULL,
  `product_category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

CREATE TABLE `statistic_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `statistic_id` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `coverage_id` int(11) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `datasources` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70908 DEFAULT CHARSET=utf8;

CREATE TABLE `statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coverage_id` int(11) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `datasources` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;

CREATE TABLE `stats` (
  `country_code` varchar(255) DEFAULT NULL,
  `feature_name` varchar(255) DEFAULT NULL,
  `count(*)` bigint(21) NOT NULL DEFAULT '0',
  `count(distinct internet_datasource_id)` bigint(21) NOT NULL DEFAULT '0',
  `avg(rating)` decimal(14,4) DEFAULT NULL,
  `count(*)/count(distinct internet_datasource_id)` decimal(24,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creation_date` date DEFAULT NULL,
  `commercial_name` varchar(255) DEFAULT NULL,
  `key_type` varchar(255) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `supplier_name` varchar(255) DEFAULT NULL,
  `cd_type` varchar(255) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `data_release_id` int(11) DEFAULT NULL,
  `product_ref` varchar(255) DEFAULT NULL,
  `brn` varchar(255) DEFAULT NULL,
  `tpd_phase_1` tinyint(1) DEFAULT NULL,
  `tpd_phase_1_description` text,
  `tpd_phase_2` tinyint(1) DEFAULT NULL,
  `tpd_phase_2_description` varchar(255) DEFAULT NULL,
  `tpd_iba` tinyint(1) DEFAULT NULL,
  `tpd_iba_description` text,
  `cv_template_id` int(11) DEFAULT NULL,
  `cd_template_name` varchar(255) DEFAULT NULL,
  `databasename` varchar(255) DEFAULT NULL,
  `extra` text,
  `hwn` tinyint(1) DEFAULT NULL,
  `gsm` tinyint(1) DEFAULT NULL,
  `tmc` tinyint(1) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `rds_tmcfile` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `medium_number` varchar(255) DEFAULT NULL,
  `region_name` varchar(255) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `location_type` varchar(255) DEFAULT NULL,
  `borrby` varchar(255) DEFAULT NULL,
  `borron` varchar(255) DEFAULT NULL,
  `borrback` varchar(255) DEFAULT NULL,
  `request_date` date DEFAULT NULL,
  `requested_by` varchar(255) DEFAULT NULL,
  `end_customer_name` varchar(255) DEFAULT NULL,
  `recognition_file` varchar(255) DEFAULT NULL,
  `nr_of_copies_request` int(11) DEFAULT NULL,
  `requested_delivery_date` date DEFAULT NULL,
  `actual_delivery_date` date DEFAULT NULL,
  `purpose` varchar(255) DEFAULT NULL,
  `request_notes` text,
  `status` varchar(255) DEFAULT NULL,
  `ciq` varchar(255) DEFAULT NULL,
  `conversion_id` int(11) DEFAULT NULL,
  `production_order_id` int(11) DEFAULT NULL,
  `status_remark` text,
  `production_orderline_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30287 DEFAULT CHARSET=utf8;

CREATE TABLE `stock_depots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `street_name` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `city_name` varchar(255) DEFAULT NULL,
  `country_name` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `contact_name` varchar(255) DEFAULT NULL,
  `contact_email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE `stock_no_release` (
  `id` int(11) NOT NULL DEFAULT '0',
  `creation_date` date DEFAULT NULL,
  `commercial_name` varchar(255) DEFAULT NULL,
  `key_type` varchar(255) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `supplier_name` varchar(255) DEFAULT NULL,
  `cd_type` varchar(255) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `data_release_id` int(11) DEFAULT NULL,
  `product_ref` varchar(255) DEFAULT NULL,
  `brn` varchar(255) DEFAULT NULL,
  `tpd_phase_1` tinyint(1) DEFAULT NULL,
  `tpd_phase_1_description` text,
  `tpd_phase_2` tinyint(1) DEFAULT NULL,
  `tpd_phase_2_description` varchar(255) DEFAULT NULL,
  `tpd_iba` tinyint(1) DEFAULT NULL,
  `tpd_iba_description` text,
  `cv_template_id` int(11) DEFAULT NULL,
  `cd_template_name` varchar(255) DEFAULT NULL,
  `databasename` varchar(255) DEFAULT NULL,
  `extra` text,
  `hwn` tinyint(1) DEFAULT NULL,
  `gsm` tinyint(1) DEFAULT NULL,
  `tmc` tinyint(1) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `rds_tmcfile` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `medium_number` varchar(255) DEFAULT NULL,
  `region_name` varchar(255) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `location_type` varchar(255) DEFAULT NULL,
  `borrby` varchar(255) DEFAULT NULL,
  `borron` varchar(255) DEFAULT NULL,
  `borrback` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `stock_orderlines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_order_id` int(11) DEFAULT NULL,
  `article_id` int(11) DEFAULT NULL,
  `amount_order` int(11) DEFAULT NULL,
  `amount_confirmed` int(11) DEFAULT NULL,
  `stock_reference` varchar(255) DEFAULT NULL,
  `remarks` text,
  `status` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `lock_version` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

CREATE TABLE `stock_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_reference` varchar(255) DEFAULT NULL,
  `date_ordered` date DEFAULT NULL,
  `confirmed_date` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `remarks` text,
  `deleted` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `lock_version` int(11) DEFAULT '0',
  `depot_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

CREATE TABLE `supplier_data_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_abbr_id` int(11) DEFAULT NULL,
  `data_type_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `sync_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount_created` int(11) DEFAULT NULL,
  `amount_updated` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `errorlog` text,
  `source_system` varchar(255) DEFAULT NULL,
  `datetime_started` datetime DEFAULT NULL,
  `datetime_finished` datetime DEFAULT NULL,
  `result_code` varchar(255) DEFAULT NULL,
  `remarks` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `mapping_id` int(11) DEFAULT NULL,
  `amount_found` int(11) DEFAULT NULL,
  `amount_unchanged` int(11) DEFAULT NULL,
  `amount_failed` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7648 DEFAULT CHARSET=utf8;

CREATE TABLE `taggings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) DEFAULT NULL,
  `taggable_id` int(11) DEFAULT NULL,
  `taggable_type` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `context` varchar(255) DEFAULT NULL,
  `tagger_id` int(11) DEFAULT NULL,
  `tagger_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tagging_idx` (`tag_id`,`taggable_type`,`taggable_id`,`context`,`tagger_id`,`tagger_type`),
  KEY `index_taggings_on_tag_id` (`tag_id`),
  KEY `index_taggings_on_taggable_id_and_taggable_type` (`taggable_id`,`taggable_type`),
  KEY `index_taggings_on_tag_id_and_taggable_type` (`tag_id`,`taggable_type`),
  KEY `index_taggings_on_user_id_and_tag_id_and_taggable_type` (`user_id`,`tag_id`,`taggable_type`),
  KEY `index_taggings_on_user_id_and_taggable_id_and_taggable_type` (`user_id`,`taggable_id`,`taggable_type`),
  KEY `index_taggings_on_taggable_id_and_taggable_type_and_context` (`taggable_id`,`taggable_type`,`context`)
) ENGINE=InnoDB AUTO_INCREMENT=466598 DEFAULT CHARSET=utf8;

CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `taggings_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_tags_on_name` (`name`),
  KEY `index_tags_on_taggings_count` (`taggings_count`)
) ENGINE=InnoDB AUTO_INCREMENT=2916 DEFAULT CHARSET=utf8;

CREATE TABLE `product_line_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `product_line_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `product_lines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `abbr` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `company_abbr_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `default_shipping_specification_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `test_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `production_orderline_id` int(11) DEFAULT NULL,
  `test_type_id` int(11) DEFAULT NULL,
  `requested_by_user_id` int(11) DEFAULT NULL,
  `assigned_to_user_id` int(11) DEFAULT NULL,
  `test_db_conversion_databases_id` int(11) DEFAULT NULL,
  `ref_db_conversion_databases_id` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `product_id` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `finish_date` date DEFAULT NULL,
  `actual_start_date` date DEFAULT NULL,
  `actual_finish_date` date DEFAULT NULL,
  `request_remarks` text,
  `result_remarks` text,
  `removed_yn` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=664 DEFAULT CHARSET=utf8;

CREATE TABLE `test_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `active_yn` tinyint(1) DEFAULT NULL,
  `removed_yn` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

CREATE TABLE `themes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `css_file` varchar(255) DEFAULT NULL,
  `description` text,
  `sample_pic` blob,
  `active_yn` tinyint(1) DEFAULT NULL,
  `default_yn` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

CREATE TABLE `tmc_info_tables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tmc_info_id` int(11) DEFAULT NULL,
  `tmc_table_id` int(11) DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tmc_infos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `data_release_id` int(11) DEFAULT NULL,
  `data_release_name` varchar(255) DEFAULT NULL,
  `description` text,
  `approved_by` varchar(255) DEFAULT NULL,
  `approve_data` date DEFAULT NULL,
  `approver_role` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

CREATE TABLE `tmc_tables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(255) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `supplier_name` varchar(255) DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `toolspec_featuregroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `conversiontool_id` int(11) DEFAULT NULL,
  `cvtool_bundle_id` int(11) DEFAULT NULL,
  `executable_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23143 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `toolspec_parameter_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `toolspec_parameter_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `toolspec_parameters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `toolspec_featuregroup_id` int(11) DEFAULT NULL,
  `datatype` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `default` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mandatory` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=116645 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `used_product_designs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_design_id` int(11) DEFAULT NULL,
  `used_product_design_id` int(11) DEFAULT NULL,
  `used_product_design_md5` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `version_active_from` int(11) DEFAULT NULL,
  `version_active_until` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `user_mail_conditions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_mail_id` int(11) DEFAULT NULL,
  `mail_conditions_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

CREATE TABLE `user_mails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `mail_event_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `last_used_project_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=265 DEFAULT CHARSET=latin1;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uidnumber` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `job_title` varchar(255) DEFAULT NULL,
  `phone_ext` varchar(255) DEFAULT NULL,
  `partner_code` varchar(255) DEFAULT NULL,
  `pw` varchar(255) DEFAULT NULL,
  `mg_work_dir` varchar(255) DEFAULT NULL,
  `theme_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=275 DEFAULT CHARSET=utf8;

CREATE TABLE `validation_errors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `conversion_database_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `copied_to_crash_yn` tinyint(1) DEFAULT NULL,
  `resolved_yn` tinyint(1) DEFAULT NULL,
  `ticket_nr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=579 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `version_numbers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `user` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1198 DEFAULT CHARSET=utf8;

CREATE TABLE `versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `versionable_id` int(11) DEFAULT NULL,
  `versionable_type` varchar(255) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `yaml` text,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_versions_on_versionable_id_and_versionable_type` (`versionable_id`,`versionable_type`)
) ENGINE=InnoDB AUTO_INCREMENT=577 DEFAULT CHARSET=utf8;

CREATE TABLE `warehouse_stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `org_id` int(11) DEFAULT NULL,
  `stock_order_id` int(11) DEFAULT NULL,
  `stock_orderline_id` int(11) DEFAULT NULL,
  `shipping_order_id` int(11) DEFAULT NULL,
  `shipping_orderline_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `org_quantity` int(11) DEFAULT NULL,
  `stock_reference` varchar(255) DEFAULT NULL,
  `article_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `remarks` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `stock_depot_id` int(11) DEFAULT NULL,
  `lock_version` int(11) DEFAULT '0',
  `warehouse_stock_adjustment_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=370 DEFAULT CHARSET=utf8;

CREATE TABLE `warehouse_stock_adjustments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) DEFAULT NULL,
  `stock_order_id` varchar(255) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `reason` text,
  `related_stock_adjustment_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE `warehouse_stock_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `warehouse_stock_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `remarks` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=776 DEFAULT CHARSET=utf8;

INSERT INTO schema_migrations (version) VALUES ('10');

INSERT INTO schema_migrations (version) VALUES ('100');

INSERT INTO schema_migrations (version) VALUES ('101');

INSERT INTO schema_migrations (version) VALUES ('102');

INSERT INTO schema_migrations (version) VALUES ('103');

INSERT INTO schema_migrations (version) VALUES ('104');

INSERT INTO schema_migrations (version) VALUES ('105');

INSERT INTO schema_migrations (version) VALUES ('106');

INSERT INTO schema_migrations (version) VALUES ('107');

INSERT INTO schema_migrations (version) VALUES ('108');

INSERT INTO schema_migrations (version) VALUES ('109');

INSERT INTO schema_migrations (version) VALUES ('11');

INSERT INTO schema_migrations (version) VALUES ('110');

INSERT INTO schema_migrations (version) VALUES ('111');

INSERT INTO schema_migrations (version) VALUES ('112');

INSERT INTO schema_migrations (version) VALUES ('113');

INSERT INTO schema_migrations (version) VALUES ('114');

INSERT INTO schema_migrations (version) VALUES ('115');

INSERT INTO schema_migrations (version) VALUES ('116');

INSERT INTO schema_migrations (version) VALUES ('117');

INSERT INTO schema_migrations (version) VALUES ('118');

INSERT INTO schema_migrations (version) VALUES ('119');

INSERT INTO schema_migrations (version) VALUES ('12');

INSERT INTO schema_migrations (version) VALUES ('120');

INSERT INTO schema_migrations (version) VALUES ('121');

INSERT INTO schema_migrations (version) VALUES ('122');

INSERT INTO schema_migrations (version) VALUES ('123');

INSERT INTO schema_migrations (version) VALUES ('124');

INSERT INTO schema_migrations (version) VALUES ('125');

INSERT INTO schema_migrations (version) VALUES ('126');

INSERT INTO schema_migrations (version) VALUES ('127');

INSERT INTO schema_migrations (version) VALUES ('128');

INSERT INTO schema_migrations (version) VALUES ('129');

INSERT INTO schema_migrations (version) VALUES ('13');

INSERT INTO schema_migrations (version) VALUES ('130');

INSERT INTO schema_migrations (version) VALUES ('131');

INSERT INTO schema_migrations (version) VALUES ('132');

INSERT INTO schema_migrations (version) VALUES ('133');

INSERT INTO schema_migrations (version) VALUES ('134');

INSERT INTO schema_migrations (version) VALUES ('135');

INSERT INTO schema_migrations (version) VALUES ('136');

INSERT INTO schema_migrations (version) VALUES ('137');

INSERT INTO schema_migrations (version) VALUES ('138');

INSERT INTO schema_migrations (version) VALUES ('139');

INSERT INTO schema_migrations (version) VALUES ('14');

INSERT INTO schema_migrations (version) VALUES ('140');

INSERT INTO schema_migrations (version) VALUES ('141');

INSERT INTO schema_migrations (version) VALUES ('142');

INSERT INTO schema_migrations (version) VALUES ('143');

INSERT INTO schema_migrations (version) VALUES ('144');

INSERT INTO schema_migrations (version) VALUES ('145');

INSERT INTO schema_migrations (version) VALUES ('146');

INSERT INTO schema_migrations (version) VALUES ('147');

INSERT INTO schema_migrations (version) VALUES ('148');

INSERT INTO schema_migrations (version) VALUES ('149');

INSERT INTO schema_migrations (version) VALUES ('15');

INSERT INTO schema_migrations (version) VALUES ('150');

INSERT INTO schema_migrations (version) VALUES ('151');

INSERT INTO schema_migrations (version) VALUES ('152');

INSERT INTO schema_migrations (version) VALUES ('153');

INSERT INTO schema_migrations (version) VALUES ('154');

INSERT INTO schema_migrations (version) VALUES ('155');

INSERT INTO schema_migrations (version) VALUES ('156');

INSERT INTO schema_migrations (version) VALUES ('157');

INSERT INTO schema_migrations (version) VALUES ('158');

INSERT INTO schema_migrations (version) VALUES ('159');

INSERT INTO schema_migrations (version) VALUES ('16');

INSERT INTO schema_migrations (version) VALUES ('160');

INSERT INTO schema_migrations (version) VALUES ('161');

INSERT INTO schema_migrations (version) VALUES ('162');

INSERT INTO schema_migrations (version) VALUES ('163');

INSERT INTO schema_migrations (version) VALUES ('164');

INSERT INTO schema_migrations (version) VALUES ('165');

INSERT INTO schema_migrations (version) VALUES ('166');

INSERT INTO schema_migrations (version) VALUES ('167');

INSERT INTO schema_migrations (version) VALUES ('168');

INSERT INTO schema_migrations (version) VALUES ('169');

INSERT INTO schema_migrations (version) VALUES ('17');

INSERT INTO schema_migrations (version) VALUES ('170');

INSERT INTO schema_migrations (version) VALUES ('171');

INSERT INTO schema_migrations (version) VALUES ('172');

INSERT INTO schema_migrations (version) VALUES ('173');

INSERT INTO schema_migrations (version) VALUES ('174');

INSERT INTO schema_migrations (version) VALUES ('175');

INSERT INTO schema_migrations (version) VALUES ('176');

INSERT INTO schema_migrations (version) VALUES ('177');

INSERT INTO schema_migrations (version) VALUES ('178');

INSERT INTO schema_migrations (version) VALUES ('179');

INSERT INTO schema_migrations (version) VALUES ('18');

INSERT INTO schema_migrations (version) VALUES ('180');

INSERT INTO schema_migrations (version) VALUES ('181');

INSERT INTO schema_migrations (version) VALUES ('182');

INSERT INTO schema_migrations (version) VALUES ('183');

INSERT INTO schema_migrations (version) VALUES ('184');

INSERT INTO schema_migrations (version) VALUES ('185');

INSERT INTO schema_migrations (version) VALUES ('186');

INSERT INTO schema_migrations (version) VALUES ('187');

INSERT INTO schema_migrations (version) VALUES ('188');

INSERT INTO schema_migrations (version) VALUES ('189');

INSERT INTO schema_migrations (version) VALUES ('19');

INSERT INTO schema_migrations (version) VALUES ('190');

INSERT INTO schema_migrations (version) VALUES ('191');

INSERT INTO schema_migrations (version) VALUES ('192');

INSERT INTO schema_migrations (version) VALUES ('193');

INSERT INTO schema_migrations (version) VALUES ('194');

INSERT INTO schema_migrations (version) VALUES ('195');

INSERT INTO schema_migrations (version) VALUES ('196');

INSERT INTO schema_migrations (version) VALUES ('197');

INSERT INTO schema_migrations (version) VALUES ('198');

INSERT INTO schema_migrations (version) VALUES ('199');

INSERT INTO schema_migrations (version) VALUES ('2');

INSERT INTO schema_migrations (version) VALUES ('20');

INSERT INTO schema_migrations (version) VALUES ('200');

INSERT INTO schema_migrations (version) VALUES ('201');

INSERT INTO schema_migrations (version) VALUES ('20101026125634');

INSERT INTO schema_migrations (version) VALUES ('20101027085736');

INSERT INTO schema_migrations (version) VALUES ('20101027085845');

INSERT INTO schema_migrations (version) VALUES ('20101027104706');

INSERT INTO schema_migrations (version) VALUES ('20101104084242');

INSERT INTO schema_migrations (version) VALUES ('20101104094303');

INSERT INTO schema_migrations (version) VALUES ('20101104105238');

INSERT INTO schema_migrations (version) VALUES ('20101105091609');

INSERT INTO schema_migrations (version) VALUES ('20101108115855');

INSERT INTO schema_migrations (version) VALUES ('20101119121850');

INSERT INTO schema_migrations (version) VALUES ('20101122115613');

INSERT INTO schema_migrations (version) VALUES ('20101222120614');

INSERT INTO schema_migrations (version) VALUES ('20110105093644');

INSERT INTO schema_migrations (version) VALUES ('20110112150159');

INSERT INTO schema_migrations (version) VALUES ('20110112150237');

INSERT INTO schema_migrations (version) VALUES ('20110127111226');

INSERT INTO schema_migrations (version) VALUES ('20110128102715');

INSERT INTO schema_migrations (version) VALUES ('20110208134350');

INSERT INTO schema_migrations (version) VALUES ('20110223125007');

INSERT INTO schema_migrations (version) VALUES ('20110223125008');

INSERT INTO schema_migrations (version) VALUES ('20110321154500');

INSERT INTO schema_migrations (version) VALUES ('20110420142500');

INSERT INTO schema_migrations (version) VALUES ('20110513094801');

INSERT INTO schema_migrations (version) VALUES ('20110608110156');

INSERT INTO schema_migrations (version) VALUES ('20110616160000');

INSERT INTO schema_migrations (version) VALUES ('20110627120000');

INSERT INTO schema_migrations (version) VALUES ('20110704091500');

INSERT INTO schema_migrations (version) VALUES ('20110704152500');

INSERT INTO schema_migrations (version) VALUES ('20110808095033');

INSERT INTO schema_migrations (version) VALUES ('20110808095134');

INSERT INTO schema_migrations (version) VALUES ('20110809101128');

INSERT INTO schema_migrations (version) VALUES ('20110811083947');

INSERT INTO schema_migrations (version) VALUES ('20110817131754');

INSERT INTO schema_migrations (version) VALUES ('20110824133234');

INSERT INTO schema_migrations (version) VALUES ('20110831073042');

INSERT INTO schema_migrations (version) VALUES ('20110902134156');

INSERT INTO schema_migrations (version) VALUES ('20110902714300');

INSERT INTO schema_migrations (version) VALUES ('20110927133958');

INSERT INTO schema_migrations (version) VALUES ('20110929095658');

INSERT INTO schema_migrations (version) VALUES ('20111004083142');

INSERT INTO schema_migrations (version) VALUES ('20111012084139');

INSERT INTO schema_migrations (version) VALUES ('20111012150099');

INSERT INTO schema_migrations (version) VALUES ('20111018063839');

INSERT INTO schema_migrations (version) VALUES ('20111018113659');

INSERT INTO schema_migrations (version) VALUES ('20111019074703');

INSERT INTO schema_migrations (version) VALUES ('20111114093000');

INSERT INTO schema_migrations (version) VALUES ('20111121125145');

INSERT INTO schema_migrations (version) VALUES ('20111124120017');

INSERT INTO schema_migrations (version) VALUES ('20111206120017');

INSERT INTO schema_migrations (version) VALUES ('20111207120020');

INSERT INTO schema_migrations (version) VALUES ('20111215125405');

INSERT INTO schema_migrations (version) VALUES ('20111220102059');

INSERT INTO schema_migrations (version) VALUES ('20120112110515');

INSERT INTO schema_migrations (version) VALUES ('20120112141420');

INSERT INTO schema_migrations (version) VALUES ('20120113164630');

INSERT INTO schema_migrations (version) VALUES ('20120116114256');

INSERT INTO schema_migrations (version) VALUES ('20120116132414');

INSERT INTO schema_migrations (version) VALUES ('20120117075544');

INSERT INTO schema_migrations (version) VALUES ('20120117082800');

INSERT INTO schema_migrations (version) VALUES ('20120118123655');

INSERT INTO schema_migrations (version) VALUES ('20120118152735');

INSERT INTO schema_migrations (version) VALUES ('20120119144949');

INSERT INTO schema_migrations (version) VALUES ('20120119145711');

INSERT INTO schema_migrations (version) VALUES ('20120123141633');

INSERT INTO schema_migrations (version) VALUES ('20120125133947');

INSERT INTO schema_migrations (version) VALUES ('20120203150421');

INSERT INTO schema_migrations (version) VALUES ('20120206080854');

INSERT INTO schema_migrations (version) VALUES ('20120209075706');

INSERT INTO schema_migrations (version) VALUES ('20120209094852');

INSERT INTO schema_migrations (version) VALUES ('20120213134927');

INSERT INTO schema_migrations (version) VALUES ('20120213141214');

INSERT INTO schema_migrations (version) VALUES ('20120305084447');

INSERT INTO schema_migrations (version) VALUES ('20120305091031');

INSERT INTO schema_migrations (version) VALUES ('20120305104814');

INSERT INTO schema_migrations (version) VALUES ('20120307122551');

INSERT INTO schema_migrations (version) VALUES ('20120312134640');

INSERT INTO schema_migrations (version) VALUES ('20120321115753');

INSERT INTO schema_migrations (version) VALUES ('20120406123000');

INSERT INTO schema_migrations (version) VALUES ('20120417140016');

INSERT INTO schema_migrations (version) VALUES ('20120423063208');

INSERT INTO schema_migrations (version) VALUES ('20120521095907');

INSERT INTO schema_migrations (version) VALUES ('20120607123601');

INSERT INTO schema_migrations (version) VALUES ('20120611095032');

INSERT INTO schema_migrations (version) VALUES ('20120611134539');

INSERT INTO schema_migrations (version) VALUES ('20120828111457');

INSERT INTO schema_migrations (version) VALUES ('20120828125722');

INSERT INTO schema_migrations (version) VALUES ('20120828134428');

INSERT INTO schema_migrations (version) VALUES ('20120829065106');

INSERT INTO schema_migrations (version) VALUES ('20120905065449');

INSERT INTO schema_migrations (version) VALUES ('20120910132929');

INSERT INTO schema_migrations (version) VALUES ('20120911111239');

INSERT INTO schema_migrations (version) VALUES ('20120911134140');

INSERT INTO schema_migrations (version) VALUES ('20121102122818');

INSERT INTO schema_migrations (version) VALUES ('20121115145532');

INSERT INTO schema_migrations (version) VALUES ('20121115150051');

INSERT INTO schema_migrations (version) VALUES ('20121115150321');

INSERT INTO schema_migrations (version) VALUES ('20121115151520');

INSERT INTO schema_migrations (version) VALUES ('20121129133409');

INSERT INTO schema_migrations (version) VALUES ('20121206120455');

INSERT INTO schema_migrations (version) VALUES ('20121212073757');

INSERT INTO schema_migrations (version) VALUES ('20121213112513');

INSERT INTO schema_migrations (version) VALUES ('20121213112843');

INSERT INTO schema_migrations (version) VALUES ('20121214075800');

INSERT INTO schema_migrations (version) VALUES ('20121221082508');

INSERT INTO schema_migrations (version) VALUES ('20130114120121');

INSERT INTO schema_migrations (version) VALUES ('20130128141521');

INSERT INTO schema_migrations (version) VALUES ('20130129070418');

INSERT INTO schema_migrations (version) VALUES ('20130129143047');

INSERT INTO schema_migrations (version) VALUES ('20130131073118');

INSERT INTO schema_migrations (version) VALUES ('20130131115900');

INSERT INTO schema_migrations (version) VALUES ('20130131120042');

INSERT INTO schema_migrations (version) VALUES ('20130131121833');

INSERT INTO schema_migrations (version) VALUES ('20130131121912');

INSERT INTO schema_migrations (version) VALUES ('20130205103931');

INSERT INTO schema_migrations (version) VALUES ('20130205123939');

INSERT INTO schema_migrations (version) VALUES ('20130205130828');

INSERT INTO schema_migrations (version) VALUES ('20130205145226');

INSERT INTO schema_migrations (version) VALUES ('20130205145930');

INSERT INTO schema_migrations (version) VALUES ('20130205150334');

INSERT INTO schema_migrations (version) VALUES ('20130205150516');

INSERT INTO schema_migrations (version) VALUES ('20130205151838');

INSERT INTO schema_migrations (version) VALUES ('20130207121602');

INSERT INTO schema_migrations (version) VALUES ('20130207123645');

INSERT INTO schema_migrations (version) VALUES ('20130211080112');

INSERT INTO schema_migrations (version) VALUES ('20130211084907');

INSERT INTO schema_migrations (version) VALUES ('20130212130353');

INSERT INTO schema_migrations (version) VALUES ('20130219100415');

INSERT INTO schema_migrations (version) VALUES ('20130221085900');

INSERT INTO schema_migrations (version) VALUES ('20130221103515');

INSERT INTO schema_migrations (version) VALUES ('20130225074103');

INSERT INTO schema_migrations (version) VALUES ('20130225093849');

INSERT INTO schema_migrations (version) VALUES ('20130227133556');

INSERT INTO schema_migrations (version) VALUES ('20130304085820');

INSERT INTO schema_migrations (version) VALUES ('20130304122444');

INSERT INTO schema_migrations (version) VALUES ('20130304122609');

INSERT INTO schema_migrations (version) VALUES ('20130305095252');

INSERT INTO schema_migrations (version) VALUES ('20130306100618');

INSERT INTO schema_migrations (version) VALUES ('20130306102358');

INSERT INTO schema_migrations (version) VALUES ('20130306114320');

INSERT INTO schema_migrations (version) VALUES ('20130306125910');

INSERT INTO schema_migrations (version) VALUES ('20130306130926');

INSERT INTO schema_migrations (version) VALUES ('20130306141532');

INSERT INTO schema_migrations (version) VALUES ('20130308151713');

INSERT INTO schema_migrations (version) VALUES ('20130313100956');

INSERT INTO schema_migrations (version) VALUES ('20130314094717');

INSERT INTO schema_migrations (version) VALUES ('20130315125623');

INSERT INTO schema_migrations (version) VALUES ('20130315125721');

INSERT INTO schema_migrations (version) VALUES ('20130315134610');

INSERT INTO schema_migrations (version) VALUES ('20130315154835');

INSERT INTO schema_migrations (version) VALUES ('20130315154859');

INSERT INTO schema_migrations (version) VALUES ('20130318130535');

INSERT INTO schema_migrations (version) VALUES ('20130319075615');

INSERT INTO schema_migrations (version) VALUES ('20130321131312');

INSERT INTO schema_migrations (version) VALUES ('20130322070203');

INSERT INTO schema_migrations (version) VALUES ('20130325084342');

INSERT INTO schema_migrations (version) VALUES ('20130325105855');

INSERT INTO schema_migrations (version) VALUES ('20130325122134');

INSERT INTO schema_migrations (version) VALUES ('20130325123959');

INSERT INTO schema_migrations (version) VALUES ('20130326092015');

INSERT INTO schema_migrations (version) VALUES ('20130326151308');

INSERT INTO schema_migrations (version) VALUES ('20130326151330');

INSERT INTO schema_migrations (version) VALUES ('20130327135909');

INSERT INTO schema_migrations (version) VALUES ('20130328095148');

INSERT INTO schema_migrations (version) VALUES ('20130402095024');

INSERT INTO schema_migrations (version) VALUES ('20130402095041');

INSERT INTO schema_migrations (version) VALUES ('20130404110722');

INSERT INTO schema_migrations (version) VALUES ('20130405113009');

INSERT INTO schema_migrations (version) VALUES ('20130405134847');

INSERT INTO schema_migrations (version) VALUES ('20130408092216');

INSERT INTO schema_migrations (version) VALUES ('20130408092235');

INSERT INTO schema_migrations (version) VALUES ('20130408140359');

INSERT INTO schema_migrations (version) VALUES ('20130410064903');

INSERT INTO schema_migrations (version) VALUES ('20130410075838');

INSERT INTO schema_migrations (version) VALUES ('20130410082751');

INSERT INTO schema_migrations (version) VALUES ('20130411140922');

INSERT INTO schema_migrations (version) VALUES ('20130418124252');

INSERT INTO schema_migrations (version) VALUES ('20130418124426');

INSERT INTO schema_migrations (version) VALUES ('20130418124608');

INSERT INTO schema_migrations (version) VALUES ('20130418124725');

INSERT INTO schema_migrations (version) VALUES ('20130419141603');

INSERT INTO schema_migrations (version) VALUES ('20130423082955');

INSERT INTO schema_migrations (version) VALUES ('20130423125919');

INSERT INTO schema_migrations (version) VALUES ('20130423143242');

INSERT INTO schema_migrations (version) VALUES ('20130424111548');

INSERT INTO schema_migrations (version) VALUES ('20130502085614');

INSERT INTO schema_migrations (version) VALUES ('20130507110610');

INSERT INTO schema_migrations (version) VALUES ('20130513084213');

INSERT INTO schema_migrations (version) VALUES ('20130516080658');

INSERT INTO schema_migrations (version) VALUES ('20130517100332');

INSERT INTO schema_migrations (version) VALUES ('20130523070606');

INSERT INTO schema_migrations (version) VALUES ('20130523110518');

INSERT INTO schema_migrations (version) VALUES ('20130523133508');

INSERT INTO schema_migrations (version) VALUES ('20130523133633');

INSERT INTO schema_migrations (version) VALUES ('20130524135436');

INSERT INTO schema_migrations (version) VALUES ('20130604133302');

INSERT INTO schema_migrations (version) VALUES ('20130611073938');

INSERT INTO schema_migrations (version) VALUES ('20130612105930');

INSERT INTO schema_migrations (version) VALUES ('20130619102458');

INSERT INTO schema_migrations (version) VALUES ('20130702085916');

INSERT INTO schema_migrations (version) VALUES ('20130702085945');

INSERT INTO schema_migrations (version) VALUES ('20130702091339');

INSERT INTO schema_migrations (version) VALUES ('20130703131622');

INSERT INTO schema_migrations (version) VALUES ('20130717074125');

INSERT INTO schema_migrations (version) VALUES ('20130717142250');

INSERT INTO schema_migrations (version) VALUES ('20130802085553');

INSERT INTO schema_migrations (version) VALUES ('20130802085630');

INSERT INTO schema_migrations (version) VALUES ('20130806122536');

INSERT INTO schema_migrations (version) VALUES ('20130809075238');

INSERT INTO schema_migrations (version) VALUES ('20130816064931');

INSERT INTO schema_migrations (version) VALUES ('20130820124327');

INSERT INTO schema_migrations (version) VALUES ('20130820125411');

INSERT INTO schema_migrations (version) VALUES ('20130828055513');

INSERT INTO schema_migrations (version) VALUES ('20130828090151');

INSERT INTO schema_migrations (version) VALUES ('20130828090843');

INSERT INTO schema_migrations (version) VALUES ('20130830090843');

INSERT INTO schema_migrations (version) VALUES ('20130903094228');

INSERT INTO schema_migrations (version) VALUES ('20130903094350');

INSERT INTO schema_migrations (version) VALUES ('20130903094817');

INSERT INTO schema_migrations (version) VALUES ('20130903094857');

INSERT INTO schema_migrations (version) VALUES ('20130904075744');

INSERT INTO schema_migrations (version) VALUES ('20130904084747');

INSERT INTO schema_migrations (version) VALUES ('20130904085308');

INSERT INTO schema_migrations (version) VALUES ('20130910100754');

INSERT INTO schema_migrations (version) VALUES ('20130910101119');

INSERT INTO schema_migrations (version) VALUES ('20130910110634');

INSERT INTO schema_migrations (version) VALUES ('20130910110859');

INSERT INTO schema_migrations (version) VALUES ('20130911125753');

INSERT INTO schema_migrations (version) VALUES ('20130911130140');

INSERT INTO schema_migrations (version) VALUES ('20130913091010');

INSERT INTO schema_migrations (version) VALUES ('20130913091157');

INSERT INTO schema_migrations (version) VALUES ('20130917105511');

INSERT INTO schema_migrations (version) VALUES ('20130919120432');

INSERT INTO schema_migrations (version) VALUES ('20130920082839');

INSERT INTO schema_migrations (version) VALUES ('20130920083001');

INSERT INTO schema_migrations (version) VALUES ('20130920112820');

INSERT INTO schema_migrations (version) VALUES ('20130920141545');

INSERT INTO schema_migrations (version) VALUES ('20130920141610');

INSERT INTO schema_migrations (version) VALUES ('20130923060130');

INSERT INTO schema_migrations (version) VALUES ('20130923142308');

INSERT INTO schema_migrations (version) VALUES ('20130926131433');

INSERT INTO schema_migrations (version) VALUES ('20130927090046');

INSERT INTO schema_migrations (version) VALUES ('20131002130121');

INSERT INTO schema_migrations (version) VALUES ('20131008092522');

INSERT INTO schema_migrations (version) VALUES ('20131008125145');

INSERT INTO schema_migrations (version) VALUES ('20131011062944');

INSERT INTO schema_migrations (version) VALUES ('20131011073514');

INSERT INTO schema_migrations (version) VALUES ('20131017114522');

INSERT INTO schema_migrations (version) VALUES ('20131018082636');

INSERT INTO schema_migrations (version) VALUES ('20131023093320');

INSERT INTO schema_migrations (version) VALUES ('20131028090522');

INSERT INTO schema_migrations (version) VALUES ('20131028125118');

INSERT INTO schema_migrations (version) VALUES ('20131028130951');

INSERT INTO schema_migrations (version) VALUES ('20131028132143');

INSERT INTO schema_migrations (version) VALUES ('20131029142110');

INSERT INTO schema_migrations (version) VALUES ('20131106090344');

INSERT INTO schema_migrations (version) VALUES ('20131106122141');

INSERT INTO schema_migrations (version) VALUES ('20131106150453');

INSERT INTO schema_migrations (version) VALUES ('20131107121947');

INSERT INTO schema_migrations (version) VALUES ('20131112082405');

INSERT INTO schema_migrations (version) VALUES ('20131112134511');

INSERT INTO schema_migrations (version) VALUES ('20131121103526');

INSERT INTO schema_migrations (version) VALUES ('20131122090056');

INSERT INTO schema_migrations (version) VALUES ('20131122110527');

INSERT INTO schema_migrations (version) VALUES ('20131127113412');

INSERT INTO schema_migrations (version) VALUES ('20131203133444');

INSERT INTO schema_migrations (version) VALUES ('20131205145432');

INSERT INTO schema_migrations (version) VALUES ('20131218110942');

INSERT INTO schema_migrations (version) VALUES ('20131219081546');

INSERT INTO schema_migrations (version) VALUES ('20131219114842');

INSERT INTO schema_migrations (version) VALUES ('20131220083548');

INSERT INTO schema_migrations (version) VALUES ('20131220125950');

INSERT INTO schema_migrations (version) VALUES ('20140108075316');

INSERT INTO schema_migrations (version) VALUES ('20140108123642');

INSERT INTO schema_migrations (version) VALUES ('20140108124326');

INSERT INTO schema_migrations (version) VALUES ('20140108132202');

INSERT INTO schema_migrations (version) VALUES ('20140109092512');

INSERT INTO schema_migrations (version) VALUES ('20140109093841');

INSERT INTO schema_migrations (version) VALUES ('20140110131118');

INSERT INTO schema_migrations (version) VALUES ('20140110131739');

INSERT INTO schema_migrations (version) VALUES ('20140110131929');

INSERT INTO schema_migrations (version) VALUES ('20140114140325');

INSERT INTO schema_migrations (version) VALUES ('20140114140336');

INSERT INTO schema_migrations (version) VALUES ('20140115130303');

INSERT INTO schema_migrations (version) VALUES ('20140117092334');

INSERT INTO schema_migrations (version) VALUES ('20140123143814');

INSERT INTO schema_migrations (version) VALUES ('20140128091003');

INSERT INTO schema_migrations (version) VALUES ('20140203092909');

INSERT INTO schema_migrations (version) VALUES ('20140203150228');

INSERT INTO schema_migrations (version) VALUES ('20140204140917');

INSERT INTO schema_migrations (version) VALUES ('20140206110301');

INSERT INTO schema_migrations (version) VALUES ('20140210105232');

INSERT INTO schema_migrations (version) VALUES ('20140211071241');

INSERT INTO schema_migrations (version) VALUES ('20140211132334');

INSERT INTO schema_migrations (version) VALUES ('20140213121411');

INSERT INTO schema_migrations (version) VALUES ('20140213132606');

INSERT INTO schema_migrations (version) VALUES ('20140213133736');

INSERT INTO schema_migrations (version) VALUES ('20140213134217');

INSERT INTO schema_migrations (version) VALUES ('20140214154639');

INSERT INTO schema_migrations (version) VALUES ('20140221094324');

INSERT INTO schema_migrations (version) VALUES ('20140224121202');

INSERT INTO schema_migrations (version) VALUES ('20140224123219');

INSERT INTO schema_migrations (version) VALUES ('20140227071304');

INSERT INTO schema_migrations (version) VALUES ('20140311071554');

INSERT INTO schema_migrations (version) VALUES ('20140319075628');

INSERT INTO schema_migrations (version) VALUES ('20140319084906');

INSERT INTO schema_migrations (version) VALUES ('20140321070510');

INSERT INTO schema_migrations (version) VALUES ('20140324102728');

INSERT INTO schema_migrations (version) VALUES ('20140325145458');

INSERT INTO schema_migrations (version) VALUES ('20140325153252');

INSERT INTO schema_migrations (version) VALUES ('20140326144702');

INSERT INTO schema_migrations (version) VALUES ('20140402112051');

INSERT INTO schema_migrations (version) VALUES ('20140402141029');

INSERT INTO schema_migrations (version) VALUES ('20140404092435');

INSERT INTO schema_migrations (version) VALUES ('20140411073933');

INSERT INTO schema_migrations (version) VALUES ('20140416083232');

INSERT INTO schema_migrations (version) VALUES ('20140429084338');

INSERT INTO schema_migrations (version) VALUES ('20140501075009');

INSERT INTO schema_migrations (version) VALUES ('20140501101556');

INSERT INTO schema_migrations (version) VALUES ('20140507105108');

INSERT INTO schema_migrations (version) VALUES ('20140507135058');

INSERT INTO schema_migrations (version) VALUES ('20140508082825');

INSERT INTO schema_migrations (version) VALUES ('20140508120015');

INSERT INTO schema_migrations (version) VALUES ('20140514121725');

INSERT INTO schema_migrations (version) VALUES ('20140515114414');

INSERT INTO schema_migrations (version) VALUES ('20140521120644');

INSERT INTO schema_migrations (version) VALUES ('20140523072717');

INSERT INTO schema_migrations (version) VALUES ('20140603132118');

INSERT INTO schema_migrations (version) VALUES ('20140610113756');

INSERT INTO schema_migrations (version) VALUES ('20140610130654');

INSERT INTO schema_migrations (version) VALUES ('20140611060616');

INSERT INTO schema_migrations (version) VALUES ('20140613080903');

INSERT INTO schema_migrations (version) VALUES ('20140613082046');

INSERT INTO schema_migrations (version) VALUES ('20140618124425');

INSERT INTO schema_migrations (version) VALUES ('20140624140449');

INSERT INTO schema_migrations (version) VALUES ('20140625101438');

INSERT INTO schema_migrations (version) VALUES ('20140626075533');

INSERT INTO schema_migrations (version) VALUES ('20140626080746');

INSERT INTO schema_migrations (version) VALUES ('20140627110010');

INSERT INTO schema_migrations (version) VALUES ('20140704095925');

INSERT INTO schema_migrations (version) VALUES ('20140704095950');

INSERT INTO schema_migrations (version) VALUES ('20140707123654');

INSERT INTO schema_migrations (version) VALUES ('20140714105622');

INSERT INTO schema_migrations (version) VALUES ('20140714120057');

INSERT INTO schema_migrations (version) VALUES ('20140714125749');

INSERT INTO schema_migrations (version) VALUES ('20140714125835');

INSERT INTO schema_migrations (version) VALUES ('20140714125837');

INSERT INTO schema_migrations (version) VALUES ('20140715084308');

INSERT INTO schema_migrations (version) VALUES ('20140715112538');

INSERT INTO schema_migrations (version) VALUES ('20140716110239');

INSERT INTO schema_migrations (version) VALUES ('20140716113518');

INSERT INTO schema_migrations (version) VALUES ('20140718060238');

INSERT INTO schema_migrations (version) VALUES ('20140718060839');

INSERT INTO schema_migrations (version) VALUES ('20140718121104');

INSERT INTO schema_migrations (version) VALUES ('20140728085647');

INSERT INTO schema_migrations (version) VALUES ('20140728085704');

INSERT INTO schema_migrations (version) VALUES ('20140728085716');

INSERT INTO schema_migrations (version) VALUES ('20140814085002');

INSERT INTO schema_migrations (version) VALUES ('20140819083049');

INSERT INTO schema_migrations (version) VALUES ('20140826090120');

INSERT INTO schema_migrations (version) VALUES ('20140901141029');

INSERT INTO schema_migrations (version) VALUES ('20140903140713');

INSERT INTO schema_migrations (version) VALUES ('20140903141344');

INSERT INTO schema_migrations (version) VALUES ('20140903141439');

INSERT INTO schema_migrations (version) VALUES ('20140903141512');

INSERT INTO schema_migrations (version) VALUES ('20140903141609');

INSERT INTO schema_migrations (version) VALUES ('20140908124059');

INSERT INTO schema_migrations (version) VALUES ('20140908133650');

INSERT INTO schema_migrations (version) VALUES ('20140912072859');

INSERT INTO schema_migrations (version) VALUES ('20140912075525');

INSERT INTO schema_migrations (version) VALUES ('20140916115115');

INSERT INTO schema_migrations (version) VALUES ('20140919111120');

INSERT INTO schema_migrations (version) VALUES ('20140922082228');

INSERT INTO schema_migrations (version) VALUES ('20140923081721');

INSERT INTO schema_migrations (version) VALUES ('20140923121943');

INSERT INTO schema_migrations (version) VALUES ('20140926131616');

INSERT INTO schema_migrations (version) VALUES ('20140929105525');

INSERT INTO schema_migrations (version) VALUES ('20140929115755');

INSERT INTO schema_migrations (version) VALUES ('20140929133607');

INSERT INTO schema_migrations (version) VALUES ('20140930141909');

INSERT INTO schema_migrations (version) VALUES ('20141001122601');

INSERT INTO schema_migrations (version) VALUES ('20141007120352');

INSERT INTO schema_migrations (version) VALUES ('20141007120832');

INSERT INTO schema_migrations (version) VALUES ('20141008063505');

INSERT INTO schema_migrations (version) VALUES ('20141008063552');

INSERT INTO schema_migrations (version) VALUES ('20141009111832');

INSERT INTO schema_migrations (version) VALUES ('20141014083529');

INSERT INTO schema_migrations (version) VALUES ('20141014110938');

INSERT INTO schema_migrations (version) VALUES ('20141014111600');

INSERT INTO schema_migrations (version) VALUES ('20141014111751');

INSERT INTO schema_migrations (version) VALUES ('20141014111821');

INSERT INTO schema_migrations (version) VALUES ('20141014125046');

INSERT INTO schema_migrations (version) VALUES ('20141014134714');

INSERT INTO schema_migrations (version) VALUES ('20141014135734');

INSERT INTO schema_migrations (version) VALUES ('20141014140934');

INSERT INTO schema_migrations (version) VALUES ('20141015111649');

INSERT INTO schema_migrations (version) VALUES ('20141015133433');

INSERT INTO schema_migrations (version) VALUES ('20141015133525');

INSERT INTO schema_migrations (version) VALUES ('20141016104314');

INSERT INTO schema_migrations (version) VALUES ('20141016105543');

INSERT INTO schema_migrations (version) VALUES ('20141024060005');

INSERT INTO schema_migrations (version) VALUES ('20141024060106');

INSERT INTO schema_migrations (version) VALUES ('20141024060707');

INSERT INTO schema_migrations (version) VALUES ('20141027131501');

INSERT INTO schema_migrations (version) VALUES ('20141027133619');

INSERT INTO schema_migrations (version) VALUES ('20141028102827');

INSERT INTO schema_migrations (version) VALUES ('20141028103327');

INSERT INTO schema_migrations (version) VALUES ('20141030140443');

INSERT INTO schema_migrations (version) VALUES ('20141030140505');

INSERT INTO schema_migrations (version) VALUES ('20141030142237');

INSERT INTO schema_migrations (version) VALUES ('20141031091546');

INSERT INTO schema_migrations (version) VALUES ('20141106142133');

INSERT INTO schema_migrations (version) VALUES ('202');

INSERT INTO schema_migrations (version) VALUES ('203');

INSERT INTO schema_migrations (version) VALUES ('204');

INSERT INTO schema_migrations (version) VALUES ('205');

INSERT INTO schema_migrations (version) VALUES ('206');

INSERT INTO schema_migrations (version) VALUES ('207');

INSERT INTO schema_migrations (version) VALUES ('208');

INSERT INTO schema_migrations (version) VALUES ('209');

INSERT INTO schema_migrations (version) VALUES ('21');

INSERT INTO schema_migrations (version) VALUES ('210');

INSERT INTO schema_migrations (version) VALUES ('211');

INSERT INTO schema_migrations (version) VALUES ('212');

INSERT INTO schema_migrations (version) VALUES ('213');

INSERT INTO schema_migrations (version) VALUES ('214');

INSERT INTO schema_migrations (version) VALUES ('215');

INSERT INTO schema_migrations (version) VALUES ('216');

INSERT INTO schema_migrations (version) VALUES ('217');

INSERT INTO schema_migrations (version) VALUES ('218');

INSERT INTO schema_migrations (version) VALUES ('219');

INSERT INTO schema_migrations (version) VALUES ('22');

INSERT INTO schema_migrations (version) VALUES ('220');

INSERT INTO schema_migrations (version) VALUES ('221');

INSERT INTO schema_migrations (version) VALUES ('222');

INSERT INTO schema_migrations (version) VALUES ('223');

INSERT INTO schema_migrations (version) VALUES ('224');

INSERT INTO schema_migrations (version) VALUES ('225');

INSERT INTO schema_migrations (version) VALUES ('226');

INSERT INTO schema_migrations (version) VALUES ('227');

INSERT INTO schema_migrations (version) VALUES ('228');

INSERT INTO schema_migrations (version) VALUES ('229');

INSERT INTO schema_migrations (version) VALUES ('23');

INSERT INTO schema_migrations (version) VALUES ('230');

INSERT INTO schema_migrations (version) VALUES ('231');

INSERT INTO schema_migrations (version) VALUES ('232');

INSERT INTO schema_migrations (version) VALUES ('233');

INSERT INTO schema_migrations (version) VALUES ('234');

INSERT INTO schema_migrations (version) VALUES ('235');

INSERT INTO schema_migrations (version) VALUES ('236');

INSERT INTO schema_migrations (version) VALUES ('237');

INSERT INTO schema_migrations (version) VALUES ('238');

INSERT INTO schema_migrations (version) VALUES ('239');

INSERT INTO schema_migrations (version) VALUES ('24');

INSERT INTO schema_migrations (version) VALUES ('240');

INSERT INTO schema_migrations (version) VALUES ('241');

INSERT INTO schema_migrations (version) VALUES ('242');

INSERT INTO schema_migrations (version) VALUES ('243');

INSERT INTO schema_migrations (version) VALUES ('244');

INSERT INTO schema_migrations (version) VALUES ('246');

INSERT INTO schema_migrations (version) VALUES ('247');

INSERT INTO schema_migrations (version) VALUES ('248');

INSERT INTO schema_migrations (version) VALUES ('249');

INSERT INTO schema_migrations (version) VALUES ('25');

INSERT INTO schema_migrations (version) VALUES ('250');

INSERT INTO schema_migrations (version) VALUES ('251');

INSERT INTO schema_migrations (version) VALUES ('252');

INSERT INTO schema_migrations (version) VALUES ('253');

INSERT INTO schema_migrations (version) VALUES ('254');

INSERT INTO schema_migrations (version) VALUES ('255');

INSERT INTO schema_migrations (version) VALUES ('256');

INSERT INTO schema_migrations (version) VALUES ('257');

INSERT INTO schema_migrations (version) VALUES ('258');

INSERT INTO schema_migrations (version) VALUES ('259');

INSERT INTO schema_migrations (version) VALUES ('26');

INSERT INTO schema_migrations (version) VALUES ('260');

INSERT INTO schema_migrations (version) VALUES ('261');

INSERT INTO schema_migrations (version) VALUES ('262');

INSERT INTO schema_migrations (version) VALUES ('263');

INSERT INTO schema_migrations (version) VALUES ('264');

INSERT INTO schema_migrations (version) VALUES ('265');

INSERT INTO schema_migrations (version) VALUES ('266');

INSERT INTO schema_migrations (version) VALUES ('267');

INSERT INTO schema_migrations (version) VALUES ('268');

INSERT INTO schema_migrations (version) VALUES ('269');

INSERT INTO schema_migrations (version) VALUES ('27');

INSERT INTO schema_migrations (version) VALUES ('270');

INSERT INTO schema_migrations (version) VALUES ('271');

INSERT INTO schema_migrations (version) VALUES ('272');

INSERT INTO schema_migrations (version) VALUES ('273');

INSERT INTO schema_migrations (version) VALUES ('274');

INSERT INTO schema_migrations (version) VALUES ('275');

INSERT INTO schema_migrations (version) VALUES ('276');

INSERT INTO schema_migrations (version) VALUES ('277');

INSERT INTO schema_migrations (version) VALUES ('278');

INSERT INTO schema_migrations (version) VALUES ('279');

INSERT INTO schema_migrations (version) VALUES ('28');

INSERT INTO schema_migrations (version) VALUES ('280');

INSERT INTO schema_migrations (version) VALUES ('281');

INSERT INTO schema_migrations (version) VALUES ('282');

INSERT INTO schema_migrations (version) VALUES ('283');

INSERT INTO schema_migrations (version) VALUES ('284');

INSERT INTO schema_migrations (version) VALUES ('285');

INSERT INTO schema_migrations (version) VALUES ('286');

INSERT INTO schema_migrations (version) VALUES ('287');

INSERT INTO schema_migrations (version) VALUES ('288');

INSERT INTO schema_migrations (version) VALUES ('289');

INSERT INTO schema_migrations (version) VALUES ('29');

INSERT INTO schema_migrations (version) VALUES ('290');

INSERT INTO schema_migrations (version) VALUES ('291');

INSERT INTO schema_migrations (version) VALUES ('292');

INSERT INTO schema_migrations (version) VALUES ('293');

INSERT INTO schema_migrations (version) VALUES ('294');

INSERT INTO schema_migrations (version) VALUES ('295');

INSERT INTO schema_migrations (version) VALUES ('296');

INSERT INTO schema_migrations (version) VALUES ('297');

INSERT INTO schema_migrations (version) VALUES ('298');

INSERT INTO schema_migrations (version) VALUES ('299');

INSERT INTO schema_migrations (version) VALUES ('30');

INSERT INTO schema_migrations (version) VALUES ('300');

INSERT INTO schema_migrations (version) VALUES ('301');

INSERT INTO schema_migrations (version) VALUES ('302');

INSERT INTO schema_migrations (version) VALUES ('303');

INSERT INTO schema_migrations (version) VALUES ('304');

INSERT INTO schema_migrations (version) VALUES ('305');

INSERT INTO schema_migrations (version) VALUES ('306');

INSERT INTO schema_migrations (version) VALUES ('307');

INSERT INTO schema_migrations (version) VALUES ('308');

INSERT INTO schema_migrations (version) VALUES ('309');

INSERT INTO schema_migrations (version) VALUES ('31');

INSERT INTO schema_migrations (version) VALUES ('310');

INSERT INTO schema_migrations (version) VALUES ('311');

INSERT INTO schema_migrations (version) VALUES ('313');

INSERT INTO schema_migrations (version) VALUES ('314');

INSERT INTO schema_migrations (version) VALUES ('315');

INSERT INTO schema_migrations (version) VALUES ('316');

INSERT INTO schema_migrations (version) VALUES ('317');

INSERT INTO schema_migrations (version) VALUES ('318');

INSERT INTO schema_migrations (version) VALUES ('319');

INSERT INTO schema_migrations (version) VALUES ('32');

INSERT INTO schema_migrations (version) VALUES ('320');

INSERT INTO schema_migrations (version) VALUES ('321');

INSERT INTO schema_migrations (version) VALUES ('322');

INSERT INTO schema_migrations (version) VALUES ('323');

INSERT INTO schema_migrations (version) VALUES ('324');

INSERT INTO schema_migrations (version) VALUES ('325');

INSERT INTO schema_migrations (version) VALUES ('326');

INSERT INTO schema_migrations (version) VALUES ('327');

INSERT INTO schema_migrations (version) VALUES ('328');

INSERT INTO schema_migrations (version) VALUES ('329');

INSERT INTO schema_migrations (version) VALUES ('33');

INSERT INTO schema_migrations (version) VALUES ('330');

INSERT INTO schema_migrations (version) VALUES ('331');

INSERT INTO schema_migrations (version) VALUES ('332');

INSERT INTO schema_migrations (version) VALUES ('333');

INSERT INTO schema_migrations (version) VALUES ('334');

INSERT INTO schema_migrations (version) VALUES ('335');

INSERT INTO schema_migrations (version) VALUES ('336');

INSERT INTO schema_migrations (version) VALUES ('337');

INSERT INTO schema_migrations (version) VALUES ('338');

INSERT INTO schema_migrations (version) VALUES ('339');

INSERT INTO schema_migrations (version) VALUES ('34');

INSERT INTO schema_migrations (version) VALUES ('340');

INSERT INTO schema_migrations (version) VALUES ('341');

INSERT INTO schema_migrations (version) VALUES ('342');

INSERT INTO schema_migrations (version) VALUES ('343');

INSERT INTO schema_migrations (version) VALUES ('344');

INSERT INTO schema_migrations (version) VALUES ('345');

INSERT INTO schema_migrations (version) VALUES ('346');

INSERT INTO schema_migrations (version) VALUES ('347');

INSERT INTO schema_migrations (version) VALUES ('348');

INSERT INTO schema_migrations (version) VALUES ('349');

INSERT INTO schema_migrations (version) VALUES ('35');

INSERT INTO schema_migrations (version) VALUES ('350');

INSERT INTO schema_migrations (version) VALUES ('352');

INSERT INTO schema_migrations (version) VALUES ('353');

INSERT INTO schema_migrations (version) VALUES ('354');

INSERT INTO schema_migrations (version) VALUES ('355');

INSERT INTO schema_migrations (version) VALUES ('356');

INSERT INTO schema_migrations (version) VALUES ('357');

INSERT INTO schema_migrations (version) VALUES ('358');

INSERT INTO schema_migrations (version) VALUES ('359');

INSERT INTO schema_migrations (version) VALUES ('36');

INSERT INTO schema_migrations (version) VALUES ('360');

INSERT INTO schema_migrations (version) VALUES ('361');

INSERT INTO schema_migrations (version) VALUES ('362');

INSERT INTO schema_migrations (version) VALUES ('363');

INSERT INTO schema_migrations (version) VALUES ('364');

INSERT INTO schema_migrations (version) VALUES ('365');

INSERT INTO schema_migrations (version) VALUES ('366');

INSERT INTO schema_migrations (version) VALUES ('367');

INSERT INTO schema_migrations (version) VALUES ('368');

INSERT INTO schema_migrations (version) VALUES ('369');

INSERT INTO schema_migrations (version) VALUES ('37');

INSERT INTO schema_migrations (version) VALUES ('370');

INSERT INTO schema_migrations (version) VALUES ('371');

INSERT INTO schema_migrations (version) VALUES ('372');

INSERT INTO schema_migrations (version) VALUES ('373');

INSERT INTO schema_migrations (version) VALUES ('374');

INSERT INTO schema_migrations (version) VALUES ('375');

INSERT INTO schema_migrations (version) VALUES ('376');

INSERT INTO schema_migrations (version) VALUES ('377');

INSERT INTO schema_migrations (version) VALUES ('378');

INSERT INTO schema_migrations (version) VALUES ('379');

INSERT INTO schema_migrations (version) VALUES ('38');

INSERT INTO schema_migrations (version) VALUES ('380');

INSERT INTO schema_migrations (version) VALUES ('381');

INSERT INTO schema_migrations (version) VALUES ('382');

INSERT INTO schema_migrations (version) VALUES ('383');

INSERT INTO schema_migrations (version) VALUES ('39');

INSERT INTO schema_migrations (version) VALUES ('4');

INSERT INTO schema_migrations (version) VALUES ('40');

INSERT INTO schema_migrations (version) VALUES ('41');

INSERT INTO schema_migrations (version) VALUES ('42');

INSERT INTO schema_migrations (version) VALUES ('43');

INSERT INTO schema_migrations (version) VALUES ('44');

INSERT INTO schema_migrations (version) VALUES ('45');

INSERT INTO schema_migrations (version) VALUES ('46');

INSERT INTO schema_migrations (version) VALUES ('47');

INSERT INTO schema_migrations (version) VALUES ('48');

INSERT INTO schema_migrations (version) VALUES ('49');

INSERT INTO schema_migrations (version) VALUES ('5');

INSERT INTO schema_migrations (version) VALUES ('50');

INSERT INTO schema_migrations (version) VALUES ('51');

INSERT INTO schema_migrations (version) VALUES ('52');

INSERT INTO schema_migrations (version) VALUES ('53');

INSERT INTO schema_migrations (version) VALUES ('54');

INSERT INTO schema_migrations (version) VALUES ('55');

INSERT INTO schema_migrations (version) VALUES ('56');

INSERT INTO schema_migrations (version) VALUES ('57');

INSERT INTO schema_migrations (version) VALUES ('58');

INSERT INTO schema_migrations (version) VALUES ('59');

INSERT INTO schema_migrations (version) VALUES ('6');

INSERT INTO schema_migrations (version) VALUES ('60');

INSERT INTO schema_migrations (version) VALUES ('61');

INSERT INTO schema_migrations (version) VALUES ('62');

INSERT INTO schema_migrations (version) VALUES ('63');

INSERT INTO schema_migrations (version) VALUES ('64');

INSERT INTO schema_migrations (version) VALUES ('65');

INSERT INTO schema_migrations (version) VALUES ('66');

INSERT INTO schema_migrations (version) VALUES ('67');

INSERT INTO schema_migrations (version) VALUES ('68');

INSERT INTO schema_migrations (version) VALUES ('69');

INSERT INTO schema_migrations (version) VALUES ('7');

INSERT INTO schema_migrations (version) VALUES ('70');

INSERT INTO schema_migrations (version) VALUES ('71');

INSERT INTO schema_migrations (version) VALUES ('72');

INSERT INTO schema_migrations (version) VALUES ('73');

INSERT INTO schema_migrations (version) VALUES ('74');

INSERT INTO schema_migrations (version) VALUES ('75');

INSERT INTO schema_migrations (version) VALUES ('76');

INSERT INTO schema_migrations (version) VALUES ('77');

INSERT INTO schema_migrations (version) VALUES ('78');

INSERT INTO schema_migrations (version) VALUES ('79');

INSERT INTO schema_migrations (version) VALUES ('8');

INSERT INTO schema_migrations (version) VALUES ('80');

INSERT INTO schema_migrations (version) VALUES ('81');

INSERT INTO schema_migrations (version) VALUES ('82');

INSERT INTO schema_migrations (version) VALUES ('83');

INSERT INTO schema_migrations (version) VALUES ('84');

INSERT INTO schema_migrations (version) VALUES ('85');

INSERT INTO schema_migrations (version) VALUES ('86');

INSERT INTO schema_migrations (version) VALUES ('87');

INSERT INTO schema_migrations (version) VALUES ('88');

INSERT INTO schema_migrations (version) VALUES ('89');

INSERT INTO schema_migrations (version) VALUES ('9');

INSERT INTO schema_migrations (version) VALUES ('91');

INSERT INTO schema_migrations (version) VALUES ('92');

INSERT INTO schema_migrations (version) VALUES ('93');

INSERT INTO schema_migrations (version) VALUES ('94');

INSERT INTO schema_migrations (version) VALUES ('95');

INSERT INTO schema_migrations (version) VALUES ('97');

INSERT INTO schema_migrations (version) VALUES ('98');

INSERT INTO schema_migrations (version) VALUES ('99');