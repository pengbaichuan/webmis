module FileSystemUtil
  require 'find'

  def self.recursive_list_hash(path)
    return_hash = {}
    normalized_path = path.gsub(/\/$/,'') + '/'
    Find.find(normalized_path).to_a.each do |fs|
      dir = File.dirname(fs)
      if return_hash.has_key?(dir)
        return_hash[dir] = return_hash[dir] << File.basename(fs)
      else
        return_hash[dir] = [ File.basename(fs) ]
      end
    end
    return return_hash
  end

  def self.directory_hash(path, name=nil)
    data = []
    normalized_path = path.gsub(/\/$/,'') + '/'
    if !path.blank? && File.directory?(normalized_path)
      creation_date = File.stat(path).ctime.strftime("%m-%d-%Y %I:%M %p")
      data = {:label => (name || normalized_path),:creation_date => creation_date,:file_size => humanize_bytes(0),:file_type => File.ftype(path)}
      data[:children] = children = []
      Dir.entries(normalized_path).map { |file| [File.ftype("#{normalized_path}#{file}"), file] }.sort.map { |file| file[1] }.each do |entry|
        next if (entry == '..' || entry == '.')
        full_path = File.join(normalized_path, entry)
        if File.directory?(full_path)
          children << directory_hash(full_path, entry)
        else
          # Files in directory
          if count_files_in_directory(normalized_path) <= 1000
          creation_date = File.stat(full_path).ctime.strftime("%m-%d-%Y %I:%M %p")
          file_size = File.stat(full_path).size
          children << {:label => entry,:creation_date => creation_date, :file_size => humanize_bytes(file_size),:file_type => File.ftype(full_path)}
          else
            children << {:label => "MORE THEN 1000 FILES IN DIRECTORY, SKIPPED PROCESSING",:creation_date => Time.now.strftime("%m-%d-%Y %I:%M %p"), :file_size => humanize_bytes(0)}
            break
          end
        end
      end
    else
      # Directory does not exist
      data = {:label => "DIRECTORY '#{path}' DOES NOT EXIST!", :creation_date => Time.now.strftime("%m-%d-%Y %I:%M %p"),:file_size => humanize_bytes(0)}
    end
    data[:children] = data[:children].group_by {|item| item[:file_type]}.sort.reverse.collect{|x|x[1]}.flatten if children && !children.empty?
    return data
  end

  def self.new_web_mis_script_sub_dir_release(sub_dir_name = 'CVENV', project_id = Project.default.id)
    project = Project.find(project_id)
    sub_dir = File.join(ConfigParameter.get("default_WebMisScripts_dir"), sub_dir_name)
    Server.ssh_localhost("mkdir -p #{sub_dir}") unless Dir.exist?(sub_dir)

    base_name = File.join(sub_dir, project.name + '_' + Date.current.strftime('%y%m%d'))
    release_dir_name = base_name
    n = 0
    while Dir.exist?(release_dir_name)
      n += 1
      release_dir_name = base_name + "_" + n.to_s
    end

    Server.ssh_localhost("mkdir -p #{release_dir_name}")
    return release_dir_name
  end

  def self.update_web_mis_script_sub_dir_latest(release_dir_name, sub_dir_name = 'CVENV', project_id = Project.default.id)
    project = Project.find(project_id)
    latest_release_dir_name = File.join(ConfigParameter.get("default_WebMisScripts_dir"), sub_dir_name, project.name + '_latest')
    Server.ssh_localhost("rm -f #{latest_release_dir_name} && ln -s #{release_dir_name} #{latest_release_dir_name}")
  end

  def self.web_mis_script_target_dir(release_dir_name, target_dir_name)
    target_dir_name = 'none' if target_dir_name.blank?
    target_dir = File.join(release_dir_name, sanitized_filename(target_dir_name))
    Server.ssh_localhost("mkdir -p #{target_dir}") unless Dir.exist?(target_dir)
    return target_dir
  end

  def self.update_link(target_path, link_name)
    Server.ssh_localhost("rm -f #{link_name} && ln -s #{target_path} #{link_name}")
  end

  def self.humanize_bytes(n)
    humanize = [ "Bytes", "KB", "MB", "GB", "TB"]
    if n > 0      
      index = ( Math.log( n ) / Math.log( 2 ) ).to_i / 10
      return "#{n.to_i / ( 1024 ** index ) } #{humanize[index]}"
    else
      return "0 #{humanize.first}"
    end
  end

  def self.count_files_in_directory(dir)
    normalized_path = dir.gsub(/\*$/,'') + '*'
    return Dir[normalized_path].count { |file| File.file?(file) }
  end

  def self.is_a_number?(s)
    s.to_s.match(/\A[+-]?\d+?(\.\d+)?\Z/) == nil ? false : true
  end

  def self.sanitized_filename(s)
    # sanitize the string used as filename (no suspect characters, whitespace changed to underscores)
    s.gsub(/[^\.\w\s_-]+/, '').gsub(/\s+/, '_')
  end

  def self.read(file)
    if File.readable?(file)
      return File.read(file)
    else
      raise "#{file} not readable by the system."
    end
  end

  def self.temp_file_path(basename = 'webmis')
    t = Tempfile.new(basename)
    result = t.path
    t.close
    t.unlink
    return result
  end

  def self.move_directory(old_path,new_path)
    # for renaming purposes only
    # for large transfers use delayed job
    if File.directory?(old_path)
      ssh_cmd = "mv #{File.join(old_path.strip)} #{File.join(new_path.strip)}"
    else
      ssh_cmd = "mkdir -p #{File.join(new_path.strip)}"
    end
    Server.ssh_localhost(ssh_cmd)
  end

  def self.remove_directory(path)
    if File.directory?(path)
      ssh_cmd = "rm -rf #{File.join(path.strip)}"
      Server.ssh_localhost(ssh_cmd)
    end
  end
end
