module HandleVersioning

  # however this is a integer mapping for status,
  # the status is string for all versioned objects
  STATUS_NEW =       0
  STATUS_DRAFT =    10
  STATUS_PENDING =  20
  STATUS_REVIEW =   30
  STATUS_APPROVED = 40
  STATUS_OBSOLETE = 90

  @@statusarray = Array.new
  @@statusarray[STATUS_NEW]      = 'new'
  @@statusarray[STATUS_DRAFT]    = 'draft'
  @@statusarray[STATUS_PENDING]  = 'pending'
  @@statusarray[STATUS_REVIEW]   = 'review'
  @@statusarray[STATUS_APPROVED] = 'approved'
  @@statusarray[STATUS_OBSOLETE] = 'obsolete'

  def self.statusarray
    return @@statusarray
  end

  def self.manual_statuses
    # statuses which may be handled via a UI
    statuses = HandleVersioning.statusarray.uniq.compact
    statuses.delete("new")
    statuses.delete("pending")
    return statuses
  end

  module HandleVersioningClass

    # a modified find to find a specific version of an instance instead of just the instance itself
    def find_version_id(id, version)
      if id && version && inst = self.find(id)
        inst.version_id(version)
      end
    end

    def find_latest_approved(id)
      if id && inst = self.find(id)
        inst.latest_approved
      end
    end

    # generate a list which can be used in a select box
    def option_list(id = nil)
      list = []
      if id && instance = self.find(id)
        # add the current instance to the option list if it would normally not be included
        list << [instance.name, instance.id] if instance.obsolete? || inst.name.blank?
      end
      # add the normal options to the list
      self.where("status != 'obsolete' and name is not null").sort_by{|r| r.name }.each { |inst| list << [inst.name, inst.id] unless inst.name.blank? }

      list
    end

    # generate a versioned list which can be used in a select box the parameter hash contains the current
    # value in {id: 'id', version: 'version'} format, which must also be present in the list.
    def versioned_option_list(hash = nil)
      option_list = []
      id = hash["id"] if hash
      version = hash["version"] if hash
      if hash && id && version && instance = self.find(id)
        if instance.not_current?(version)
          # add the current instance version to the option list
          option_list << [instance.versioned_name(version),instance.version_id(version)] unless instance.version_removed?(version)
          if instance.obsolete? && instance.latest_approved
            # the instance is obsolete and the current version is not approved make it possible to at least
            # set the instance to the latest approved version
            option_list << [instance.versioned_name(instance.latest_approved.version), instance.latest_approved.id]
          end
        end
      else
        # the current selected option, identified by id and version, is not a valid choice add a blank option
        # to indicate the current value
        option_list << ["", nil]
      end
      # add the normal options to the list
      self.where("status != 'obsolete' AND status != 'new'").sort_by{|r| r.name}.each do |inst|
        if inst.approved
          option_list << [inst.versioned_name(inst.approved.version), inst.approved.id]
        end
        if inst.latest
          option_list << [inst.versioned_name(inst.latest.version), inst.latest.id]
        end
      end
      option_list
    end

  end
  # little trick to include the class methods above while including the instant methods below
  def self.included (host_class)
    host_class.extend( HandleVersioningClass )
  end

  # returns the latest version, if it is a draft or ready for review
  def latest
    versions.last if status == 'draft' || status == 'review'
  end

  # returns the latest approved version, if it exists
  def latest_approved
    version_obj = versions.where("status='approved'").last
    # when no version records available but self is approved
    if self.status == 'approved' && !version_obj
      version_obj = instant_revision
    end
    return version_obj
  end

  # returns the latest approved version, if it exists and the instance has not been set to obsolete
  def approved
    latest_approved unless self.status == 'obsolete'
  end

  # returns the latest approved version if it exists, and the initial version otherwise this is the version
  # the revert method will revert back to
  def latest_baseline
    if (latest_approved)
      latest_approved
    else
      versions.order(:version).first
    end
  end

  # returns false if version is not the latest or latest approved version or if the instance is obsolete
  def not_current?(version)
    if !versions.where("version=#{version}").first
      # version does not exist
      true
    elsif status == 'obsolete'
      # instance is obsolete and should no longer be used
      true
    elsif version == self.version || version == latest_approved
      # we have the latest or latest approved version, both of which are current
      false
    else
      # version is not the latest or latest approved
      true
    end
  end

  def obsolete?
    status == 'obsolete'
  end

  def version_removed?(version)
    versions.where("version=#{version}").first.nil?
  end

  def version_id(version)
    versions.where("version=#{version}").first.id if versions.where("version=#{version}")
  end

  def versioned_name(version)
    version = 1 unless version # just make sure we can search for a version
    current_version = versions.where("version=#{version}").first
    obsolete = (status == 'obsolete')
    approved = current_version && current_version.status == 'approved'
    old = current_version && version != latest_approved && version < self.version
    removed = !current_version

    label = name
    case
    when removed
      label += " (REMOVED VERSION)"
    when obsolete && approved && old
      label += " (old & obsolete)"
    when obsolete && approved # && !old
      label += " (obsolete)"
    when obsolete # && !approved
      label += " (outdated & obsolete)"
    when approved && old # && !obsolete
      label += " (old)"
    when old # && !approved && !obsolete
      label += " (outdated)"
    when approved # && !old && !obsolete
      label += ""
    else # !approved && !old && !obsolete, i.e. latest
      label += " (latest)"
    end
    label
  end

  def versioned_status(version)
    current_version = versions.where("version=#{version}").first
    if current_version
      current_version.status
    else
      "obsolete"
    end
  end

  # find who made the last change on an attribute which must be reviewed. If none of the input attributes have
  # changed since the latest approved or initial version, there is nothing to review and a nil is returned.
  # Otherwise the usercode of the person who made the last change to one of the input attributes is returned
  # as that person is the one who should not be allowed to review these attributes.
  def to_be_reviewed_change_made_by
    start_version = self.latest_baseline.version
    attributes = self.class.attrs_requiring_review
    return nil unless attributes && start_version   # if no attributes have been specified or no versions exist, there is nothing to review
    return nil if start_version == self.version     # entity is still in its initial revision or has already been approved, there is nothing to review
    return nil if attributes.inject(true){|result,a| result && self.__send__(a) == self.latest_baseline.__send__(a)} # none of the attributes changed

    next_one = self
    self.versions.where("version >= #{start_version}").reverse_order.each do |v|
      attributes.each do |a|
        return next_one.updated_by unless v.__send__(a) == next_one.latest_baseline.__send__(a)
      end
      next_one = v
    end

    # should not occur, so log it, but just return nil for now
    puts "ERROR: In last_change_to_be_reviewed_made_by for id '#{self.id}' in class '#{self.class}' for attributes '#{attributes.join(",")}.'"
    return nil
  end

  # place holders for status specific validations
  def valid_new?
    true
  end

  def valid_draft?
    true
  end

  def valid_pending?
    false
  end

  def valid_review?
    true
  end

  def valid_approved?
    true
  end

  def valid_obsolete?
    true
  end

  # used by the WorkflowValidator, return true is the specified status is valid as for status transitions: the
  # begin status is 'new', the end status is 'obsolete' and in between you can freely switch between the
  # 'draft', 'review' and 'approved' statuses
  def valid_status?(status)
    case status.to_s
    when 'new'
      valid_new? && (!self.status || self.status == 'new')
    when 'draft'
      valid_draft?  && self.status != 'obsolete'
    when 'pending'
      valid_pending? && self.status && self.status != 'obsolete'
    when 'review'
      valid_review? && self.status && self.status != 'obsolete'
    when 'approved'
      valid_approved? && self.status && self.status != 'obsolete'
    when 'obsolete'
      valid_obsolete? && self.status && self.status != 'new'
    end
  end

  # allowed_status is used to check whether a status transition is allowed, it is more strict than
  # valid_status? as the latter is a model validation
  def allowed_status?(status, user_name = nil, project_id = nil)
    return false unless self.valid_status?(status)
    # only add checks not contained in valid_status
    case status.to_s
    when 'new'
      false      # new is only meant as initial status, you cannot change to the 'new' status later on
    when 'draft'
      self.status != 'draft'
    when 'review'
      self.status == 'draft'
    when 'approved'
      (self.status == 'review' && user_name && self.updated_by != user_name) ||
        ( (self.status == 'draft' || self.status == 'review') && !ConfigParameter.get("enforce_review_procedure",project_id) )
    when 'obsolete'
      self.status != 'obsolete'
    end
  end

  def use_version(version)
    version_to_be_used =
      case version.to_s
    when 'latest_approved'
      self.latest_approved
    when 'latest_baseline'
      self.latest_baseline
    when 'latest'
      self.versions.last
    else
      self.find_version(version)
    end
    if version_to_be_used
      self.versioned_columns.each do |attribute|
        self.__send__ "#{attribute}=", version_to_be_used.__send__(attribute)
      end
      self.version = version_to_be_used.version
    end
    return self
  end

  def revert_to(version)
    self.use_version(version)
    self.save(:validate => false)
  end

  def use_latest_approved
    self.use_version('latest_approved')
  end

  def use_latest_baseline
    self.use_version('latest_baseline')
  end

  def use_latest
    self.use_version('latest')
  end

  def instant_revision
    new_version = versions.build
    versioned_columns.each do |attribute|
      new_version.__send__ "#{attribute}=", __send__(attribute)
    end
    version_number = new_record? ? 1 : version + 1
    new_version.version = version_number
    self.class.transaction do
      self.version = version_number
      self.save
      new_version.save
      return new_version
    end
  end

end
