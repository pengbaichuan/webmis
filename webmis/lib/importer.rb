require 'rubygems'
require 'csv'

class Importer

  def self.import(cat_id)
    @warning = Array.new
    @message = Array.new
    h = Hash.new
    if cat_id
      prod_cat = ProductCategory.find(cat_id)
      pds_import_file = prod_cat.pds_import_file
    else
      abort("No import without product-category!")
    end
    cmd = "/bin/cat #{pds_import_file} | /usr/bin/dos2unix > /tmp/ppfunx.csv"
    `#{cmd}`
    arr = CSV.read('/tmp/ppfunx.csv', :headers => true)
    lastproductid = nil
    oldproductid = 0

    oldsetid = 0
    setid = 0

    foundproducts= []
    foundproddocs = []
    founddocs = []
    
    arr.each  do |set|
      set_pml_id = ''
      sethash = set.to_hash

      if sethash["ObjectType"] == "ProductSetRemove"
        ps = Productset.where(['setcode = ? and product_category_id = ?', sethash["SetID"], cat_id]).first
        if ps
          ps.products.each do |prd|
            icp = Includedproduct.find_by_product_id(prd.id)
            @setname = sethash["SetID"]
            if prd.in_multiple_productsets?
              @warning << h = {@setname => "#{prd.name} #{prd.volumeid} is also used in another productset, and therefor not disabled."}
            else
              prd.removed_yn = true
              prd.save
              @warning << h = {@setname => "#{prd.name} #{prd.volumeid} disabled."}
            end

            ps.removed_yn = true
            ps.save
            icp.destroy if icp
            @warning << h = {@setname => "Disabled."}
          end
        end
      end


      if sethash["ObjectType"] == "ProductSet"
        found = Productset.where(['setcode = ? and product_category_id = ?', sethash["SetID"], prod_cat.id]).first
        p = nil
        p_org = nil
        if found
          p = found
          p_org = p.dup

          #p.setchanges = "Update via Spreadsheet"
        else
          p = Productset.new
          p_org = nil
        end

        p.removed_yn = 0
        p.product_category_id = cat_id
        p.setcode = sethash["SetID"]
        p.name = sethash["SetName"]
        p.customer_name = sethash["SetCustomer"]
        p.firsttiername = sethash["SetFirstTier"]
        suppl = CompanyAbbr.where("company_name LIKE '#{sethash["SetDataSupplier"]}'").last
        if suppl.nil?
          suppl = CompanyAbbr.where("ms_abbr_3 LIKE '#{sethash["SetDataSupplier"]}'").last
        end
        if !suppl.nil?
          p.company_abbr_id = suppl.id
        end
        p.supplier_name = sethash["SetDataSupplier"]
        p.data_set_description = sethash["SetDataSource"]
        p.platform_name = sethash["SetSystemPlatForm"]
        p.ciq = sethash["SetCIQBRN"]
        p.area_name = sethash["SetArea"]
        p.nds_name = sethash["SetNDS"]
        p.tpdspec = sethash["SetTPDSpec"]
        p.tmc_file = sethash["SetTMCSpecFile"]
        p.tmc_info_name = sethash["SetTMCSpec"]
        p.output_format = sethash["SetOutputFormat"]
        @setname = p.setcode

        # New Major may not be smaller
        if p.set_release_major.to_i > sethash["SetReleaseMajor"].to_i
          @warning << h = {@setname => "SetReleaseMajor decreased (old:#{p.set_release_major} vs new:#{sethash["SetReleaseMajor"]})"}
        end

        # Minor may not be smaller or equeal when Major not changed
        if p.set_release_major.to_i == sethash["SetReleaseMajor"] && sethash["SetReleaseMinor"].to_i >= p.set_release_minor.to_i
          minor_release_conflict = true
        else
          minor_release_conflict = false
        end

        p.set_release_major = sethash["SetReleaseMajor"]
        p.set_release_minor = sethash["SetReleaseMinor"]


        if ((!sethash["SetReleaseMajor"] || sethash["SetReleaseMajor"] == "") && (p.set_release_major.to_s == ""))
          p.set_release_major = "0"
          p.set_release_minor = "0"
        end

        found = TmcInfo.find_by_name(sethash["SetTMCSpec"])
        if found
          p.tmc_info_id = found.id
        else
          ti = TmcInfo.new
          ti.name = sethash["SetTMCSpec"]
          ti.save
          p.tmc_info_id = ti.id
        end

        p.predecessor = sethash["SetPredecessor"]
        if sethash["SetLaunchDate"].to_s.size > 5
          begin
            p.launchdate =  Date.strptime(sethash["SetLaunchDate"],"Date: %d-%m-%Y")
          rescue => error
            puts error.message
          end
        end
        if sethash["SetUnderConstrDate"].to_s.size > 5
          begin
            #p.underconstructiondate =  Date.strptime(sethash["SetUnderConstrDate"],"Date: %d-%m-%Y")
            p.underconstructiondate =  sethash["SetUnderConstrDate"]
          rescue => error
            puts error.message
          end
        end

        if sethash["SetPML"] && !sethash["SetPML"].blank?
          pml_dir = ConfigParameter.get("pml_source_directory")
          pml_file = sethash["SetPML"] + ".csv"
          existing_pml = PoiMappingLayout.where("source_filename = '#{pml_file}'").last
          if existing_pml.nil?
            # Import here
            begin
              poi_mapping_layout = PoiMappingLayout.import_file(pml_dir+pml_file)
              set_pml_id = poi_mapping_layout.id
            rescue => e
              @warning << h = {@setname => "FAILURE #{pml_file}: #{e.message} "}
            end
          else
            set_pml_id = existing_pml.id
          end
        end

        p.conversiontool_name = sethash["SetCVTool"]
        if sethash["SetMedium"]
          p.medium_name = sethash["SetMedium"]
        end
        p.remarks = sethash["SetRemarks"]
        p.approval_text = sethash["SetSignature"]
        p.status = (Productset.statushash.index sethash["SetApproved"].to_sym).to_s

        if p_org
          differences =  p.get_differences_with p_org
          if differences.size > 0
            if minor_release_conflict
              @warning << h = {@setname => "SetReleaseMinor decreased or equal (old:#{p.set_release_minor} vs new:#{sethash["SetReleaseMinor"]})"}
            end
            p.save
            @message << h = {@setname => "Imported"}
          else
            #p.save
          end
        else
          # new productset save
          begin
            p.save!
            @message << h = {@setname => "Imported"}
          rescue => e
            @warning << h = {@setname => "IMPORT FAILED: #{e.message} "}
            next
          end
        end

        setid = p.id
      elsif sethash["ObjectType"] == "Product"
        ref = false

        productid = sethash['ProdID']

        if productid.scan(/-ref$/)[0]
          productid =  productid.scan(/(.*)-ref$/).to_s
          ref = true
        end
        product = Product.where("volumeid = '#{productid}'").first
        if product && product.import_disabled
          @warning << h = {"#{@setname}" => "#{product.volumeid} is blocked for import updates. "}
        end
        if product && !product.active_orders.empty?
          @warning << h = {"#{@setname}" => "#{product.volumeid} has pending production orders and therefore blocked for import updates. "}
        end
        product_org = nil
        new = false
        dataset = nil
        ds_changed = false
        ds_existing = false
        if !product
          product = Product.new
          new = true
        else
          product_org = product.dup
        end

        product.volumeid = productid
        idparts = product.volumeid.scan(/(.*)_RU(.*)$/).flatten
        product.created_from = "SELF"
        if idparts.size > 0
          parent_product = Product.find_by_volumeid(idparts[0])
          if parent_product
            product.parent_id = parent_product.id
          end
          product.file_detect_regex =  "_diff_.*_#{idparts[1]}/ROOT.NDS$"
          product.created_from = "PARENT"
        end
        
        if !ref && product.allow_import?
          product.status = 20 # Approved
          product.name = sethash['ProdName']
          product.detailedcoverage = sethash['ProdCoverageDetailed'].to_s
          product.hwncoverage = sethash['ProdCoverageHWN'].to_s
          primary_coverage_arr = product.detailedcoverage.split("\n")
          secondary_coverage_arr = product.hwncoverage.split("\n")
          tmp_coverage = Coverage.new
          tmp_coverage.status = 0 #tmp
          tmp_coverage.save
          tmp_sec_coverage = Coverage.new
          tmp_sec_coverage.status = 0 #tmp
          tmp_sec_coverage.save

          primary_coverage_arr.each do |pc|

            region = Region.active.where("name = \"#{pc.strip}\"").first
            regioncode = region.region_code if region

            if region.nil?
              @warning << h = {"#{@setname}" => "No match for area  \"#{pc}\" in product #{product.volumeid}. No normalization possible, please FIX! "}
              #product.status = 10 # Draft not set yet
              puts ">> region mapping failed for [#{pc}]"
            elsif regioncode.nil? && region
              @warning << h = {"#{@setname}" => "No regioncode for area  \"#{pc}\" in product #{product.volumeid}. No normalization possible, please FIX! "}
              puts ">> regioncode n.a. for [#{pc}]"
            else
              #match region
              tmp_coverages_region = CoveragesRegion.new
              tmp_coverages_region.region_id = region.id
              tmp_coverages_region.coverage_id = tmp_coverage.id
              tmp_coverages_region.save
            end

          end

          secondary_coverage_arr.each do |sc|
            if sc.strip != 'No'
              secregion = Region.active.where("name = \"#{sc.strip}\"").first
              secregioncode = secregion.region_code if secregion

              if secregion.nil?
                @warning << h = {"#{@setname}" => "No match for area  \"#{sc}\" in product #{product.volumeid}. No normalization possible, please FIX! "}
                # product.status = 10 # Draft not set yet
                puts ">> region mapping failed for [#{sc}]"
              elsif
                @warning << h = {"#{@setname}" => "No regioncode for area  \"#{sc}\" in product #{product.volumeid}. No normalization possible, please FIX! "}
                puts ">> regioncode n.a. for [#{sc}]"
              else
                # match region
                tmp_sec_coverages_region = CoveragesRegion.new
                tmp_sec_coverages_region.region_id = secregion.id
                tmp_sec_coverages_region.coverage_id = tmp_sec_coverage.id
                tmp_sec_coverages_region.save
              end
            end
          end

          if sethash['ProdProductLine']
            pl = ProductLine.find_by_abbr(sethash['ProdProductLine'])
            if !pl
              @warning << {"#{@setname}" => "Productline abbreviation #{sethash['ProdProductLine']} specified for product #{product.volumeid} not known. "}
            else
              product.product_line_id = pl.id
            end
          else
            @warning << {"#{@setname}" => "No productline specified for product #{product.volumeid}. (Use ProdProductLine)"}
          end

          product.tmclocation = sethash['ProdTMCLocTab'].to_s
          product.tmcevent = sethash['ProdTMCEventTab'].to_s
          product.thirdpartydata1 = sethash['ProdTPD1'].to_s
          product.thirdpartydata2 = sethash['ProdTPD2'].to_s
          product.nds_name = sethash['ProdNDS'].to_s
          product.remarks = sethash['ProdRemarks'].to_s
          product.zip = sethash['ProdZIP'].to_s
          product.qxs = sethash['ProdQXS'].to_s
          product.speedlimits = sethash['ProdSpeedlimits'].to_s
          product.namerotations = sethash['ProdnameRotations'].to_s
          product.removed_yn = 0

          if sethash['ProdPML'] && !sethash['ProdPML'].blank?
            pml_dir = ConfigParameter.get("pml_source_directory")
            pml_file = sethash["ProdPML"] + ".csv"
            existing_pml = PoiMappingLayout.where("source_filename = '#{pml_file}'").last
            if existing_pml.nil?
              begin
                poi_mapping_layout = PoiMappingLayout.import_file(pml_dir+pml_file)
                product.poi_mapping_layout_id = poi_mapping_layout.id
              rescue => e
                @warning << h = {@setname => "FAILURE #{pml_file}: #{e.message}"}
              end
            else
              product.poi_mapping_layout_id = existing_pml.id
            end
          else
            product.poi_mapping_layout_id = set_pml_id
          end

          dataset = nil
          if !new
            #remove other ProductsetInfo objects
            ProductDataSetInfo.where("product_id = #{product.id}").each do |data|
              data.destroy
            end
          end
        end

        if !tmp_coverage
          tmp_coverage = Coverage.new
          tmp_coverage.status = nil
          tmp_coverage.save
        end

        if !tmp_sec_coverage
          tmp_sec_coverage = Coverage.new
          tmp_sec_coverage.status = nil
          tmp_sec_coverage.save
        end

        if new
          product.name = "reference-product" if ref
          tmp_coverage.status = nil
          tmp_coverage.save
          product.primary_coverage_id = tmp_coverage.id
          tmp_sec_coverage.status = nil
          tmp_sec_coverage.save
          product.secondary_coverage_id = tmp_sec_coverage.id
          begin
            product.save!
          rescue => e
            @warning << h = {@setname => "FAILURE SAVING NEW PRODUCT: #{e.message} "}
          end

        else
          if product.primary_coverage_id.blank?
            product.primary_coverage_id = tmp_coverage.id
            product.save if product.allow_import?
          else
            primary_coverage_delta = product.compare_coverage(tmp_coverage.id)
            if !primary_coverage_delta.empty?
              product.coverage.swap_temp_status(tmp_coverage.id) if product.primary_coverage_id
              product.primary_coverage_id = tmp_coverage.id
              product.last_change = primary_coverage_delta.join(' ')
              product.save if product.allow_import?
            end

          end
          if product.secondary_coverage_id.blank?
            product.secondary_coverage_id = tmp_sec_coverage.id

            product.save if product.allow_import?
          else
            secondary_coverage_delta = product.compare_coverage(tmp_sec_coverage.id,'secondary')
            if !secondary_coverage_delta.empty?
              product.coverage.swap_temp_status(tmp_sec_coverage.id) if product.secondary_coverage_id
              product.secondary_coverage_id = tmp_coverage.id
              product.last_change = secondary_coverage_delta.join(' ')
              product.save if product.allow_import?
            end
          end

          differences = product.get_differences_with product_org
          if product_org && differences.size > 0

            begin
              product.save! if product.allow_import?
            rescue => e
              @warning << h = {@setname => "FAILURE SAVING PRODUCT #{product.id}: #{e.message} "}
            end

          else
            #?
          end
        end

        # for now ds_changed is always false and dataset always nil
        # care about handing the save! exception if this changes.
        if ds_changed
          res = dataset.save! if product.allow_import?
        else

        end
        dataset = nil

        lastproductid = product.id
        ip = Includedproduct.where("product_id = #{product.id} and productset_id = #{setid}").first

        if !ip
          ip = Includedproduct.new
          ip.product_id = product.id
          ip.productset_id = setid
          ip.save
          foundproducts << ip.id
        else
          foundproducts << ip.id
        end

      elsif sethash["ObjectType"] == "ProductSetDocInfo"

        fullpath  =    ENV['pds_document_path'] + sethash["DocumentLocation"].to_s.strip
        filename = fullpath.scan(/\/([^\/]*)$/).join
        typedoc = fullpath.scan(/.([^\.]*)$/).join.downcase
        founddocs << fullpath

        doc = Document.where("sync_filepath = '#{fullpath}'").first
        relateddoc = nil
        if !doc
          #document Found
          doc = Document.new
        else
          conditions = "productset_id = #{setid} and document_id = #{doc.id}"
          relateddoc = Includedproductsetdocument.where(conditions).first
        end

        f = nil
        bindata = nil
        begin
          f = File.new(fullpath)
          if File.directory?(fullpath)
            @setname = Productset.find(setid).setcode
            @warning << h = {"#{@setname}" => "Missing file for  #{sethash["DocumentTitle"]} (#{sethash["DocumentObjectID"]})"}
            raise "DirectoryException"
          end
          digest = Digest::MD5.new
          bindata = f.read
          digest << bindata
        rescue => e
          f= nil
        end

        if (!doc.last_modified_timestamp) || (f == nil || (doc.last_modified_timestamp.to_i - f.mtime.to_i < 0 || doc.checksum != digest.hexdigest))    
          Document.transaction do
            doc.filename = filename
            doc.content_type = "application/msword"
            doc.content_type = "application/vnd.ms-excel" if typedoc == "xls"
            doc.content_type = "application/pdf" if typedoc == "pdf"
            doc.content_type = "application/msword" if typedoc == "doc"
            doc.content_type = "application/vnd.sun.xml.writer " if typedoc == "odt"
            doc.sync_filepath = fullpath
            doc.checksum = digest.hexdigest if digest

            save = true
            if f != nil
              doc.binary_data = bindata
              doc.last_modified_timestamp = f.mtime
              doc.description = sethash["DocumentTitle"].to_s.strip
              doc.document_object_id = sethash["DocumentObjectID"].to_s.strip
            else
              if doc.description.to_s.scan(/^NOT AVAILABLE/).size == 0

                doc.description = "NOT AVAILABLE: " + sethash["DocumentTitle"].to_s.strip
              else
                save = false
              end
            end

            if save
              doc.save
            end

            if relateddoc
              if f != nil
                relateddoc.document_id = doc.id
                relateddoc.productset.setchanges = "Document #{doc.id.to_s} - #{doc.filename} was changed"
                relateddoc.save
              end
            else
              id= Includedproductsetdocument.new
              id.productset_id = setid
              id.document_id = doc.id
              pset = Productset.find(setid)
              pset.setchanges = "Document #{doc.id.to_s} - #{doc.filename} was added"
              pset.save
              id.save
            end
          end
        else
          if !relateddoc

            id= Includedproductsetdocument.new
            id.productset_id = setid
            id.document_id = doc.id
            pset = Productset.find(setid)
            pset.setchanges = "Document #{doc.id.to_s} - #{doc.filename} was added"
            pset.save
            id.save
          end

        end
        
      elsif sethash["ObjectType"] == "ProductDataSetInfo"
        using_groups = false
        product_volume_id = Product.find(lastproductid).volumeid
        dt_filling_ar = sethash["DataInfo"].split('[')
        data_type_string = dt_filling_ar[0].strip
        filling_string = ''
        filling_name = nil
        if dt_filling_ar.size > 1
          filling_string = dt_filling_ar[1].chop
          pdsi_filling = Filling.find_by_name(filling_string) if filling_string != ''
          filling_name = pdsi_filling.name if pdsi_filling
        end
        
        pdsi_company_abbr = CompanyAbbr.find_by_ms_abbr_3("#{sethash['DataSupplierAbbr']}")
        pdsi_region = Region.find_by_name(sethash["DataAreaName"])
        pdsi_region = Region.find_by_region_code(sethash["DataAreaName"]) if !pdsi_region
        pdsi_data_release = DataRelease.find_by_name(sethash["DataVersion"])
        if !pdsi_data_release 
          # fallback when import is 14Q2 and record is 2014Q2
          repaired_data_release_str = "20" + sethash["DataVersion"]
          pdsi_data_release = DataRelease.find_by_name(repaired_data_release_str)
          @message << h = {"#{@setname}" => "Mapped data release #{sethash["DataVersion"]} => #{pdsi_data_release.name}"} if pdsi_data_release
        end
        # repaired string still no match, maybe alias is used?
        if !pdsi_data_release
          drs = DataRelease.where("data_releases.aliases LIKE '%#{sethash["DataVersion"]}%' AND data_releases.company_abbr_id = #{pdsi_company_abbr.id}")
          if !drs.empty?
            pdsi_data_release = drs.last
            @message << h = {"#{@setname}" => "Mapped data release alias #{sethash["DataVersion"]} => #{pdsi_data_release.name}"}
          end
        end
        
        pdsi_data_type =  DataType.find_by_name(data_type_string)
        
        if sethash["DataMainDataset"] && ( sethash["DataMainDataset"].downcase != "yes" && sethash["DataMainDataset"].downcase != "no")
          using_groups = true
          # group defined instead of product_data_set_info
          grp_attr_suffix = sethash["DataMainDataset"] if sethash["DataMainDataset"].downcase != 'group'
          grp_attr_company_abbr = pdsi_company_abbr ? pdsi_company_abbr.ms_abbr_3 : ''
          grp_attr_region_code = pdsi_region ? pdsi_region.region_code : ''
          grp_attr_data_release = pdsi_data_release ? pdsi_data_release.name : ''
          grp_attr_data_type = pdsi_data_type ? pdsi_data_type.name : ''
          grp_attr_filling = filling_name.to_s.blank? ? filling_name : nil

          ds_group = DataSetGroup.find_by_code(grp_attr_company_abbr,grp_attr_region_code,grp_attr_data_release,grp_attr_data_type,grp_attr_filling,grp_attr_suffix)
          if ds_group && !ds_group.data_sets.empty?
            ds_group.data_sets.each do |ds|
              g_pdsi = ds.map_to_product_data_set_info
              g_pdsi.product_id = lastproductid
              g_pdsi.remarks = sethash["DataRemarks"]
              if g_pdsi.datatype_id == pdsi_data_type.id && g_pdsi.data_release == pdsi_data_release.name && g_pdsi.area_name == pdsi_region.name
                # group tags is same as dataset tags, so major dataset
                g_pdsi.major_dataset = 1
              else
                g_pdsi.major_dataset = 0
              end
              g_pdsi.save if Product.find(lastproductid).allow_import?
            end
          else
            @warning << h = {"#{@setname}" => "No Data Set Definition GROUP #{sethash["DataMainDataset"]} found for #{sethash["DataInfo"]}-#{sethash['DataSupplierAbbr']}-#{sethash["DataVersion"]}-#{sethash["DataAreaName"]} in #{product_volume_id}."}
            # TODO New future CR coming to create a DataSetGroupTemplate automaticly when group not available
          end      
          
        else        
          # find or create product_data_set_infos
          if pdsi_company_abbr && pdsi_region && pdsi_data_release && pdsi_data_type &&
              data_set = DataSet.find_or_create_by_core_attributes(pdsi_data_type.name,pdsi_company_abbr.company_name,pdsi_data_release.name,pdsi_region.region_code,filling_name,false,false)
            # data_set = DataSet.find_or_create_by_core_attributes(pdsi_data_type.name,pdsi_company_abbr.company_name,pdsi_data_release.name,pdsi_region.region_code,filling_name,false,false)
            pdsi = data_set.map_to_product_data_set_info
          else
            msg_data_release = sethash["DataVersion"]
            msg_data_release = pdsi_data_release.name + "( #{sethash["DataVersion"]} )" if pdsi_data_release && (sethash["DataVersion"] != pdsi_data_release.name)
            @warning << h = {"#{@setname}" => "No Data Set Definition found for #{sethash["DataInfo"]}-#{sethash['DataSupplierAbbr']}-#{msg_data_release}-#{sethash["DataAreaName"]} in #{product_volume_id}."}
            pdsi = ProductDataSetInfo.new
            pdsi.company_abbr_id = pdsi_company_abbr.id if pdsi_company_abbr
            
            if pdsi_data_type
              pdsi.datatype_id = pdsi_data_type.id
            else
              @warning << h = {"#{@setname}" => "Data-type \"#{data_type_string}\" not found for \"#{sethash["DataName"]}\" in #{product_volume_id}."}
            end
            
            if pdsi_data_release
              pdsi.data_release = pdsi_data_release.name
            else
              @warning << h = {"#{@setname}" => "Data Relase \"#{sethash["DataVersion"]}\" not found for \"#{sethash["DataName"]}\" in #{product_volume_id}."}
              pdsi.data_release = sethash["DataVersion"]
            end
            
            if pdsi_region
              pdsi.area_name = pdsi_region.name
            else
              @warning << h = {"#{@setname}" => "Region \"#{sethash["DataAreaName"]}\" not found for \"#{sethash["DataName"]}\" in #{product_volume_id}."}
              pdsi.area_name = sethash["DataAreaName"]
            end
            
            if filling_name
              pdsi.filling = filling_name
            else
              if filling_string != ''
                pdsi.filling = filling_string
                @warning << h = {"#{@setname}" => "Filling \"#{filling_string}\" not found for \"#{sethash["DataName"]}\" in #{product_volume_id}."}
              end
            end
            
          end
          
          pdsi.product_id = lastproductid
          pdsi.name = sethash["DataName"]
          pdsi.data_info =  sethash["DataInfo"]
          pdsi.remarks = sethash["DataRemarks"]
          if sethash["DataMainDataset"] && setid > 0
            if sethash["DataMainDataset"].downcase == "yes"
              ps = Productset.find(setid)
              ps.datarelease_name = sethash["DataVersion"]
              pdsi.major_dataset = 1
            else
              pdsi.major_dataset = 0           
            end
          else
            pdsi.major_dataset = 0
          end
          if !Product.find(lastproductid).allow_import?
            @warning << h = {"#{@setname}" => "#{product_volume_id} with #{sethash["DataName"]}\" is blocked for import updates. "}
          else
            pdsi.save
          end
        end

      elsif sethash["ObjectType"] == "ProductDocInfo"
        fullpath  =    ENV['pds_document_path'] + sethash["DocumentLocation"].to_s.strip
        filename = fullpath.scan(/\/([^\/]*)$/).to_s
        typedoc = fullpath.scan(/.([^\.]*)$/).to_s.downcase
        foundproddocs << fullpath
        doc = Document.where("sync_filepath = '#{fullpath}'").first
        relateddoc = nil
        if !doc
          #document Found
          doc = Document.new

        else
          relateddoc = Includeddocument.where("product_id = #{lastproductid} and document_id = #{doc.id}").first
        end

        f = nil
        digest = nil
        bindata = nil
        begin
          f = File.new(fullpath)
          if File.directory?(fullpath)
            @setname = Productset.find(setid).setcode
            raise "DirectoryException"
          end
          digest = Digest::MD5.new
          bindata = f.read
          digest << bindata
        rescue => e
          f = nil
        end
       
        if (!doc.last_modified_timestamp) || (f == nil || (doc.last_modified_timestamp.to_i - f.mtime.to_i < 0  || doc.checksum != digest.hexdigest))
          Document.transaction do
            doc.filename = filename
            doc.content_type = "application/msword"
            doc.content_type = "application/vnd.ms-excel" if typedoc == "xls"
            doc.content_type = "application/pdf" if typedoc == "pdf"
            doc.content_type = "application/msword" if typedoc == "doc"
            doc.content_type = "application/vnd.sun.xml.writer " if typedoc == "odt"
            doc.sync_filepath = fullpath
            doc.checksum = digest.hexdigest if digest
            #doc.description = sethash["DocumentTitle"].to_s.strip
            #
            save = true
            if f != nil

              doc.binary_data = bindata
              doc.last_modified_timestamp = f.mtime
              doc.description = sethash["DocumentTitle"].to_s.strip
              doc.document_object_id = sethash["DocumentObjectID"].to_s.strip
            else
              if doc.description.to_s.scan(/^NOT AVAILABLE/).size == 0

                doc.description = "NOT AVAILABLE: " + sethash["DocumentTitle"].to_s.strip
              else
                save = false
              end
            end
            if save
              doc.save
            end

            if !relateddoc
              id= Includeddocument.new
              id.product_id = lastproductid
              id.document_id = doc.id
              id.save
            end
          end
        else
          if !relateddoc

            id= Includeddocument.new
            id.product_id = lastproductid
            id.document_id = doc.id
            id.save
          end

        end

      end

      if oldsetid != setid && oldsetid != 0 && foundproducts.size > 0
        #new set found
        remove = Includedproduct.where("productset_id = #{oldsetid} and id not in #{foundproducts.inspect.gsub('[','(').gsub(']',')')}")

        remove.each do |ip|
          ip.destroy
          ip.save
        end

        if !p_org
          # new productset, need to initialize includedproduct_versions
          Productset.find(oldsetid).take_snapshot
        end


        foundproducts = []
      end

      if oldsetid != setid && oldsetid != 0  && oldsetid != nil
        conditions = "productset_id = #{oldsetid}"
        relateddocs = Includedproductsetdocument.where(conditions)
        relateddocs.each do |rdoc|
          if !founddocs.include?(rdoc.document.sync_filepath)
            rdoc.destroy
            rdoc.save
          end
        end
        founddocs = []
      end

      if oldproductid != lastproductid && oldproductid != 0 &&  oldproductid != nil
        conditions = "product_id = #{oldproductid}"
        relateddocs = Includeddocument.where(conditions)
        relateddocs.each do |rdoc|
          if !foundproddocs.include?(rdoc.document.sync_filepath)
            rdoc.destroy
            rdoc.save
          end
        end
        foundproddocs = []
      end

      oldsetid = setid
      oldproductid = lastproductid

    end #loop

    @warnstr = "<br />"
    @msgstr = "<br />"
    setname = ""
    cur_setname = ""
    @warning.each do |x|
      x.each_key {|k|cur_setname = k.to_s}
      if setname != cur_setname
        x.each_key {|k|@warnstr = @warnstr + "<span style='color:red;'><b>" + k.to_s + "</b></span><br />"}
        x.each_value {|v|@warnstr = @warnstr + v.to_s + "<br />"}
      else
        x.each_value {|v|@warnstr = @warnstr + v.to_s + "<br />"}
      end
      setname = cur_setname
    end

    @message.uniq.each do |m|
      m.each_key {|k|cur_setname = k.to_s}
      if setname != cur_setname
        m.each_key {|k|@msgstr += "<span style='color:green;'><b>" + k.to_s + "</b></span><br />"}
        m.each_value {|v|@msgstr += v.to_s + "<br />"}
      else
        m.each_value {|v|@msgstr += v.to_s + "<br />"}
      end
      setname = cur_setname
    end

    ENV['import_warning'] = @warnstr + @msgstr

  end

  def self.fix_jobs(dest_env)
    Job.establish_connection(dest_env)
    Job.outstanding.each do |job|
      job.file_system_status = Job::FILE_SYSTEM_STATUS_FREE
      job.status = Job::STATUS_CANCELLED
      job.stdout_log = job.stdout_log.to_s + "Cancelled by import db script"
      job.save!
    end
  end

  def self.fix_databases(dest_env)
    ConversionDatabase.establish_connection(dest_env)
    ConversionDatabase.all.each do |db|
      db.file_system_status = ConversionDatabase::FILE_SYSTEM_STATUS_REMOVED
      db.stdout_log = db.stdout_log.to_s + "File System Status set to removed by import db script"
      db.save!
    end
    ConversionDatabase.outstanding.each do |db|
      db.status = ConversionDatabase::STATUS_CANCELLED
      db.stdout_log = db.stdout_log.to_s + "Cancelled by import db script"
      db.save!
    end
  end

  def self.fix_servers(dest_env)
    Server.establish_connection(dest_env)
    Server.all.each do |server|
      server.scheduling_allowed_yn = false
      server.save!
    end
  end


  def self.import_db

    puts "This will DROP all tables and import a new database from the specified source to target!"
    
    import_file = ""
    source_env = ""
    found = false
    while !found
      puts "Specify import file (press <ENTER> to import db directly from source mysql db)"
      import_file = gets
      found = File.exists?(import_file.chomp.strip)
      break if import_file.blank?
      if !found
        puts "File does not exist, try again"
      end
    end

    if !import_file.blank?
      puts "Will import from file #{import_file}"
    else
      puts "Will dump & import directly"
      puts "Please specify Source database? (development/staging/production)"
      source_env = gets
      source_env.chomp!.strip!
    end
    config   = Rails.configuration.database_configuration
    if !source_env.blank?
      source_host     = config[source_env]["host"]
      source_database = config[source_env]["database"]
      source_username = config[source_env]["username"]
      source_password = config[source_env]["password"]
    end
    
    puts "Please specify Destination? (development,staging, production)"
    dest_env = gets
    dest_env.chomp!.strip!
    config   = Rails.configuration.database_configuration
    dest_host     = config[dest_env]["host"]
    dest_database = config[dest_env]["database"]
    dest_username = config[dest_env]["username"]
    dest_password = config[dest_env]["password"]

    if source_password && source_password != ""
      source_password = "-p#{source_password}"
    end

    if dest_password && dest_password != ""
      dest_password = "-p#{dest_password}"
    end

    if dest_env == "production"
      puts 'Dont use this script to import TO production'
      exit 
    end

    if source_env == dest_env
      puts "Source and destination database can't be equal!"
      exit
    end
    
    cmdbackup  = "mysqldump --max_allowed_packet=100M -h #{dest_host} -u #{dest_username} #{dest_password} #{dest_database} > /volumes1/dump_original.sql 2>&1"
    if import_file.blank?
      cmd = "mysqldump --max_allowed_packet=100M --ignore-table=#{source_database}.document_versions -h #{source_host} -u #{source_username} #{source_password} #{source_database} | mysql -h #{dest_host} -u #{dest_username} #{dest_password} #{dest_database} 2>&1"
    else
      cmd = "mysql --max_allowed_packet=100M -h #{dest_host} -u #{dest_username} #{dest_password} #{dest_database} 2>&1 < #{import_file}"
    end
    puts cmdbackup

    puts cmd
    # exit
    puts "CHECK COMMANDS Before importing (enter OK to continue)"
    go = gets 
    if go.chomp == "OK"
      puts "Dumping original database"
      r = `#{cmdbackup}`
      puts "Dump completed"
      puts "Dropping tables in destination"

      User.establish_connection(dest_env)
      ActiveRecord::Base.establish_connection(dest_env)
      ActiveRecord::Base.connection.tables.each do |table|
        begin
          User.connection.execute("DROP table #{dest_database}.#{table}") if (table != "document_versions" || table != "sync_sessions")
          puts "Dropped table #{dest_database}.#{table}"
        rescue => e
          puts "Could not drop table #{dest_database}.#{table}"
        end
      end
      if import_file.blank?
        puts "Importing #{source_database} from #{source_host} to #{dest_database} on #{dest_host}"
      else
        puts "Importing #{import_file} to #{dest_database} on #{dest_host}"
      end
      r = `#{cmd}`

      puts "Import finished -  result: "  + r
      if r != ''
        "Some problem with importing, exiting now, please review logs"
        exit
      end
      # post process removed and map stuff
      #remove usermail stuff
      UserMail.establish_connection(dest_env)
      UserMail.destroy_all
      puts "UserMail Config was removed"
      update_prod_params(dest_env)

    end
    
    fix_jobs(dest_env)
    fix_databases(dest_env)
    fix_servers(dest_env)
    puts "IMPORT COMPLETED - CHECK LOG FOR ANY POTENTIAL ERRORS"
  end

  def self.update_prod_params(dest_env)
      ConfigParameter.establish_connection(dest_env)
      cp = ConfigParameter.find_by_cfg_name("process_data_directory")
      if !cp
        cp = ConfigParameter.new
        cp.cfg_name = "process_data_directory"
      end
      if dest_env == "staging"
        cp.cfg_value = "/data/process/backofficestaging"
        cp.save
      end
      if dest_env == "development"
        cp.cfg_value = "/data/process/backofficedevelopment"
        cp.save
      end


      cp = ConfigParameter.find_by_cfg_name("shared_temp_dir")
      if !cp
        cp = ConfigParameter.new
        cp.cfg_name = "shared_temp_dir"
      end
      if dest_env == "staging"
        cp.cfg_value = "/data/input/.backoffice_tmp/backofficestaging"
        cp.save
      end
      if dest_env == "development"
        cp.cfg_value = "/data/input/.backoffice_tmp/backofficedevelopment"
        cp.save
      end

      cp = ConfigParameter.find_by_cfg_name("conversion_job_crash_dir")
      if !cp
        cp = ConfigParameter.new
        cp.cfg_name = "conversion_job_crash_dir"
      end
      if dest_env == "staging"
        cp.cfg_value = "/data/projects/crash/backoffice/test/"
        cp.save
      end
      if dest_env == "development"
        cp.cfg_value = "/data/projects/crash/backoffice/test/"
        cp.save
      end

      cp = ConfigParameter.find_by_cfg_name("conversion_job_intermediate_dir")
      if !cp
        cp = ConfigParameter.new
        cp.cfg_name = "conversion_job_intermediate_dir"
      end
      if dest_env == "staging"
        cp.cfg_value = "/data/projects/webmis_staging/ops/"
        cp.save
      end
      if dest_env == "development"
        cp.cfg_value = "/data/projects/webmis_staging/ops/"
        cp.save
      end

      cp = ConfigParameter.find_by_cfg_name("conversion_job_result_dir")
      if !cp
        cp = ConfigParameter.new
        cp.cfg_name = "conversion_job_result_dir"
      end
      if dest_env == "staging"
        cp.cfg_value = "/data/projects/webmis_staging/output/"
        cp.save
      end
      if dest_env == "development"
        cp.cfg_value = "/data/projects/webmis_staging/output/"
        cp.save
      end

      cp = ConfigParameter.find_by_cfg_name("dakota_scripts_dir")
      if !cp
        cp = ConfigParameter.new
        cp.cfg_name = "dakota_scripts_dir"
      end
      if dest_env == "staging"
        cp.cfg_value = "/data/projects/webmis_staging/scripts/"
        cp.save
      end
      if dest_env == "development"
        cp.cfg_value = "/data/projects/webmis_staging/scripts/"
        cp.save
      end

      cp = ConfigParameter.find_by_cfg_name("packing_job_result_dir")
      if !cp
        cp = ConfigParameter.new
        cp.cfg_name = "packing_job_result_dir"
      end
      if dest_env == "staging"
        cp.cfg_value = "/volumes2/packing/"
        cp.save
      end
      if dest_env == "development"
        cp.cfg_value = "/volumes2/packing/"
        cp.save
      end

      cp = ConfigParameter.find_by_cfg_name("packing_job_working_dir")
      if !cp
        cp = ConfigParameter.new
        cp.cfg_name = "packing_job_working_dir"
      end
      if dest_env == "staging"
        cp.cfg_value = "/tmp/packing/"
        cp.save
      end
      if dest_env == "development"
        cp.cfg_value = "/tmp/packing/"
        cp.save
      end

      cp = ConfigParameter.find_by_cfg_name("product_location_dir")
      if !cp
        cp = ConfigParameter.new
        cp.cfg_name = "product_location_dir"
      end
      if dest_env == "staging"
        cp.cfg_value = "/data/test/test_staging/"
        cp.save
      end
      if dest_env == "development"
        cp.cfg_value = "/tmp/product_location/"
        cp.save
      end

      puts "Config parameters where adapted"
  end

end

