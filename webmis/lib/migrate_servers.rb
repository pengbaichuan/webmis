require 'rubygems'
require 'fastercsv'
arrs = FasterCSV.read("public/Servers.txt", :headers => true)

arrs.each do |server|
  server = server.to_hash
  s = Server.new
  s.server_id = server["Server_Id"]
  s.server_remark = server["Server_Remark"]
  s.active = server["Server_OnOff"]
  s.diskcapacity = server["Server_DiskCap"]
  s.processor = server["Server_Proc"]
  s.additionalprocess = server["Server_AdditionalProcess"]
  s.save
end

