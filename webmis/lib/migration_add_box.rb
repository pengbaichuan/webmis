require '~/NetBeansProjects/WebMIS/config/environment'
require 'hpricot'

	doc = Hpricot.parse(File.read("/data/users/johan.hendrikx/tblBox.xml"))	
        data = doc.search("//tblbox")

	#record = data[10]
	data.each do |record|
           loc = record.search("//box_no").inner_text
	   l = Location.where("location_code = '#{loc}' and locationtype = 'BOX'").first

           if (!l)
             l = Location.new
             l.location_code = loc
             l.locationtype = "BOX"
             l.borrby  = record.search("/borrby").inner_text
             l.borron  = record.search("/borron").inner_text
             l.borrback  = record.search("/borrback").inner_text
             l.save
           end
           cd = record.search("/boxcdlist_no").inner_text
           if cd && cd.size > 1 

           stocks = Stock.where(medium_number:cd)
           if stocks.size == 0
            puts "added stock location " + loc
            r = Stock.new
            r.medium_number = cd + " *UNMATCHED*"
            media = Mediatype.find_by_ref(record.search("//cdtype").inner_text)
            if media
              r.cd_type = media.name
            end
            r.location_type = "BOX"
            r.data_release_id = 1321
            r.location = "BOX|" + loc
            r.extra = "LOCATION ADDED FROM BOXLIST"
            r.save
           end
	end
end

