require '~/NetBeansProjects/WebMIS/config/environment'
require 'hpricot'

#doc = open("/data/users/johan.hendrikx/Conversions.xml") do |f|


	#doc = Hpricot.parse(File.read("/data/users/johan.hendrikx/Conversions.xml"))	
	doc = Hpricot.parse(File.read("/data/users/johan.hendrikx/j.xml"))	
        data = doc.search("//conversions")

	#record = data[10]
	data.each do |record|
	  if record.search("//conversionid").inner_text && record.search("//conversionid").inner_text.to_i > 0
	    r = Conversion.new
	    r.id = record.search("//conversionid").inner_text.to_i
	    r.request_by = record.search("//request_x0020_by").inner_text

	    if  record.search("//date").inner_text &&  record.search("//date").inner_text.size > 4
	      r.request_date =  DateTime.strptime(record.search("//date").inner_text,"%Y-%m-%dT%H:%M:%S")
	    end

	    if  record.search("//conversion_x0020_date").inner_text &&  record.search("//conversion_x0020_date").inner_text.size > 4
	      r.conversion_date =  DateTime.strptime(record.search("//conversion_x0020_date").inner_text,"%Y-%m-%dT%H:%M:%S")
	    end

	    r.priority = record.search("//priority").inner_text
	    
	    supplier = "UNKNOWN"
	    if (record.search("//map_x0020_supplier").inner_text)
		    supplier =  record.search("//map_x0020_supplier").inner_text
		    if supplier == "TeleAtlas" 
                       supplier = "Tele Atlas"
		    end
            end
	    release = DataRelease.includes(:datasource).where("data_releases.name = '#{record.search("//mapsupplier_release_detail").inner_text}' and datasources.name = '#{supplier}'").first
	    if (release)
	      r.data_release_id =  release.id
	      r.datasource_id = release.datasource_id
	      r.supplier_id = release.datasource.supplier_id
	    else
	      ds = Datasource.where("datasources.name = '#{supplier}'").first
	      if (!ds)
		      ds = Datasource.new 
		      ds.name = supplier
		      ds.supplier_id = "9bef5a30-2034-01f5-3a80-47d68fb56db6" #sugarcrm 
		      ds.save
	      end
	      dr = DataRelease.new
	      dr.datasource_id = ds.id
	      dr.name = record.search("//mapsupplier_release_detail").inner_text
	      dr.save
	      r.data_release_id = dr.id
	      r.supplier_id = ds.supplier_id
	      r.datasource_id = ds.id
	    end

	    if record.search("//productid")[0]
		r.product =  record.search("/releaseconv/").inner_text
   		r.productid =  record.search("//productid").inner_text
	    end
	    
	    cvtool = record.search("//conversion_x0020_tool").inner_text
	    
	    c = Conversiontool.find_by_code(cvtool)
	    if !c
   	     c =  Conversiontool.new
             c.code = cvtool
	     c.description = cvtool
             c.tool_type = "Conversion"
             c.save
	    end
	    r.cvtool = cvtool
            template = record.search("//cvtool_template_file").inner_text
	    if template && template.size > 0
             t = CvtoolTemplate.find_by_filename(template)
	     if !t
               t = CvtoolTemplate.new
               t.filename = template
               t.description = template
               t.save
	     end
             r.template_filename = template
            end
	    

            r.additional_info = record.search("//additional_x0020_info").inner_text
	    r.gsm = record.search("//gsm_x0020_check").inner_text.to_i
	    r.gsmfile = record.search("//gsmfile").inner_text
	    r.converter = record.search("//converterid").inner_text
            r.poifile = record.search("//tpdfile").inner_text
            r.tpd = record.search("//tpd").inner_text
	    r.data_files = record.search("//gdf-files").inner_text
            r.reference_numbers = record.search("//drf-nr").inner_text
            r.remarks = record.search("//remarks").inner_text
	    r.right_data_check =  record.search("//right_x0020_gdf_x0027_s_x0020_check").inner_text
            r.dealers_check  = record.search("/dealers_x0020_check")
            r.cvtool_check =  record.search("/cvtool_x0020_check").inner_text
            r.outcome =  record.search("/outcome").inner_text
	    r.cvtoolenv = record.search("/cvtool_environment").inner_text
            size = record.search("/size_x0020_bites/").inner_text

	    if size && size.to_f > 0
	            r.size_bytes = size.to_f
		    r.size_mb = size.to_f / 1024 /1024
	    end

	    r.database_name = record.search("/databasename/").inner_text
            r.type = "CarinConversion"
            r.rdstmc = record.search("//rds-tmc").inner_text
	    r.rdstmcfile =  record.search("//rds-tmcfile").inner_text
	    r.ca_brn =  record.search("/ca_brn").inner_text
            r.gsm_check =  record.search("/gsm_x0020_check").inner_text
            r.gsm_check =  record.search("/gsm_x0020_check").inner_text
            r.tmc_check =  record.search("/tmc_x0020_check").inner_text
	    r.tpd_products =  record.search("/tpd_product").inner_text
            r.tpd_rejected =  record.search("/rejected_x0020_tpd").inner_text
            r.tpd_rejection_remark =  record.search("/tpdrejectionremark").inner_text
            r.tpd_result_sender = record.search("/tpdresultsendby").inner_text



	    region = Region.find_by_name(record.to_s.scan(/area..>([^<]*)/).to_s.strip)

	    if (region)
	      #if region.class.name == "Region"
		r.region_id = region.id
	        r.region_name = region.name
	      #else
	      #  r.area_id = -20
	      #end
	    else
	        re = Region.new
                re.name = record.to_s.scan(/area..>([^<]*)/).to_s.strip
	        re.type = "Area"
	        re.save
                r.region_id = re.id
	        r.region_name = re.name
	    end

	    r.save
	  end
	end

