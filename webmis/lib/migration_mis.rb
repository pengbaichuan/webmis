require '~/NetBeansProjects/WebMIS/config/environment'
require 'hpricot'

	require 'AReception'
	#data = AReception.find_where("1=1")
	#data = AReception.find_first(:Reception_Id => 10)
	doc = Hpricot.parse(File.read("/data/users/johan.hendrikx/tblReception.xml"))	
        data = doc.search("//tblreception")

	#record = data[10]
	data.each do |record|
	  if record.search("//recepstionid").inner_text && record.search("//receptionid").inner_text.to_i > 0
	    r = Reception.new
	    r.id = record.search("//receptionid").inner_text.to_i
	    r.sendto = record.search("//sendto").inner_text
	    r.purpose = record.search("//purpose").inner_text
	    if !r.purpose 
	     r.purpose = "MIGRATION"
	    end

            cvtool = record.search("//carribatool").inner_text

            c = Conversiontool.find_by_code(cvtool)
            if !c
             c =  Conversiontool.new
             c.code = cvtool
             c.description = cvtool
             c.tool_type = "Conversion"
             c.save
            end
            r.cvtool = cvtool
	    supplier = "UNKNOWN"
	    if (record.search("//supplierdata").inner_text)
		    supplier = record.search("//supplierdata").inner_text
            end
	    release = DataRelease.include(:datasource).where("data_releases.name = '#{record.search("//supplierrelease").inner_text}' and datasources.name = '#{supplier}'").first
	    if (release)
	      r.data_release_id =  release.id
	      r.datasource_id = release.datasource_id
	    else
	      ds = Datasource.where("datasources.name = '#{supplier}'").first
	      if (!ds)
		      ds = Datasource.new 
		      ds.name = supplier
		      ds.supplier_id = "9bef5a30-2034-01f5-3a80-47d68fb56db6" #sugarcrm 
		      ds.save
	      end
	      
	      dr = DataRelease.new
	      dr.datasource_id = ds.id
	      dr.name = record.search("//supplierrelease").inner_text
	      dr.save

	      r.data_release_id = dr.id
	      r.datasource_id = ds.id
	    end

	    r.remarks =  record.search("//receptionremarks").inner_text

	    medium = DataMedium.find_by_name(record.search("//datamedium").inner_text)
	    if (medium)
	      r.data_medium_id = medium.id
	    else
              dm =  DataMedium.new
              dm.name = record.search("//datamedium").inner_text
              dm.save
	      r.data_medium_id = dm.id
	    end

	    datatype = DataType.find_by_description(record.search("//datatype").inner_text)

	    if datatype
	      r.data_type_id = datatype.id
	    else
              dm =  DataType.new
              dm.name = record.search("//datatype").inner_text
              dm.description = record.search("//datatype").inner_text
              dm.save

	      r.data_medium_id = dm.id
	      r.data_type_id = -10
	    end


	    r.referenceid = record.search("//deliveryreference").inner_text
	    if record.search("//senddate").inner_text && record.search("//senddate").inner_text.size > 4
	      r.senddate =  DateTime.strptime(record.search("//senddate").inner_text,"%Y-%m-%dT%H:%M:%S")
	    end

	    region = Region.find_by_name(record.to_s.scan(/area..>([^<]*)/).to_s.strip)

	    if (region)
	      #if region.class.name == "Region"
		r.area_id = region.id
		r.area_type = region.type.to_s
	      #else
	      #  r.area_id = -20
	      #end
	    else
	        re = Region.new
                re.name = record.to_s.scan(/area..>([^<]*)/).to_s.strip
	        re.type = "Area"
	        re.save
 		
		r.area_type = "Area"
	        r.area_id = re.id
	    end

	    r.storage_location =  record.search("//storagelocation").inner_text
	    r.dataversion = record.search("//dataversion").inner_text
	    r.send_status = true
	    r.user_name = record.search("//userid").inner_text
	    if (record.search("//receptiondate").inner_text && record.search("//receptiondate").inner_text.size > 5)
              #puts record.reception_date
	      r.reception_date = DateTime.strptime(record.search("//receptiondate").inner_text,"%Y-%m-%dT%H:%M:%S")
	    end
	    r.save
	  end
	end

