require '~/NetBeansProjects/WebMIS/config/environment'
require 'hpricot'

	require 'AReception'
	#data = AReception.find_where("1=1")
	#data = AReception.find_first(:Reception_Id => 10)
	doc = Hpricot.parse(File.read("/data/users/johan.hendrikx/tblReception.xml"))	
        data = doc.search("//tblreception")

	#record = data[10]
	data.each do |record|
	  if record.reception_id && record.reception_id > 0
	    r = Reception.new
	    r.id = record.reception_id
	    r.sendto = record.send_to
	    r.purpose = record.purpose
	    if !r.purpose 
	     r.purpose = "MIGRATION"
	    end

	    supplier = "UNKNOWN"
	    if (record.supplier_data)
		    supplier = record.supplier_data.gsub("'","")
            end
	    release = DataRelease.includes(:datasource).where("data_releases.name = '#{record.supplier_release}' and datasources.name = '#{supplier}'").first
	    if (release)
	      r.data_release_id =  release.id
	      r.datasource_id = release.datasource_id
	    else
	      ds = Datasource.where("datasources.name = '#{supplier}'").first
	      if (!ds)
		      ds = Datasource.new 
		      ds.name = supplier
		      ds.supplier_id = "9bef5a30-2034-01f5-3a80-47d68fb56db6" #sugarcrm 
		      ds.save
	      end
	      
	      dr = DataRelease.new
	      dr.datasource_id = ds.id
	      dr.name = record.supplier_release
	      dr.save

	      r.data_release_id = dr.id
	      r.datasource_id = ds.id
	    end

	    r.remarks = record.reception_remarks

	    medium = DataMedium.find_by_name(record.data_medium)
	    if (medium)
	      r.data_medium_id = medium.id
	    else
	      r.data_medium_id = -10
	    end

	    datatype = DataType.find_by_description(record.data_type)

	    if datatype
	      r.data_type_id = datatype.id
	    else
	      r.data_type_id = -10
	    end


	    r.referenceid = record.delivery_reference
	    if record.send_date
	      r.senddate =  DateTime.strptime(record.send_date,'%m/%d/%y %H:%M:%S')
	    end

	    region = Region.find_by_name(record.area)

	    if (region)
	      #if region.class.name == "Region"
		r.area_id = region.id
		r.area_type = region.type.name
	      #else
	      #  r.area_id = -20
	      #end
	    else
	      r.area_id = -10
	    end

	    r.storage_location = record.storage_location
	    r.dataversion = record.data_version
	    r.send_status = true
	    r.user_name = record.user_id
	    if (record.reception_date && record.reception_date.size > 5)
              puts record.reception_date
	      r.reception_date = DateTime.strptime(record.reception_date,'%m/%d/%y %H:%M:%S')
	    end
	    r.save
	  end
	end

