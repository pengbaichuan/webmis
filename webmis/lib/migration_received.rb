#require '~/NetBeansProjects/WebMIS/config/environment'
require 'hpricot'

	doc = Hpricot.parse(File.read("/tmp/liveReceived.xml"))	
        data = doc.search("//tblreceived")

	#record = data[10]
	data.each do |record|
          i = InboundOrder.new
          drf = record.search("//drf_ms_id").inner_text
          i.orderref = drf
          i.save

          il = InboundOrderline.new
          il.id = record.search("/autonumber").inner_text.to_i
          il.drfmsid = drf
          cdtype = record.search("/cdtyperec").inner_text
          m = Mediatype.find_by_ref(cdtype)
          if m
          il.mediatype = m.name
          else
           il.mediatype ="UNKNOWN"
          end
          il.area = record.search("/country_area").inner_text
          area = record.search("//country_area").inner_text
          if (area && area.strip.size > 1)
             region = Region.find_by_name(area)
             if (region)
                il.area = region.name
             else
                re = Region.new
                re.name = area
                re.type = "Area"
                re.save
                il.area = area
             end
          end

          il.dbrelease = record.search("//db_release").inner_text
          il.deliveryformat = record.search("/delivery_format").inner_text
          il.dbversion = record.search("/db_version").inner_text
          il.inbound_order_id = i.id
          adate = record.search("/vdo_arrival").inner_text
          if (data.size > 4)
           begin
             il.arrival_date =  DateTime.strptime(adate,"%Y-%m-%dT%H:%M:%S")
           rescue
             puts "invalid arrival date"
           end
          end
          
          # GDF_RN
            datatype = DataType.find_by_name(record.search("//datatype").inner_text)

            if datatype
              il.data_type_id = datatype.id
            else
              dm =  DataType.new
              dm.name = record.search("//datatype").inner_text
              dm.description = record.search("//datatype").inner_text
              dm.save

              il.data_type_id = dm.id
            end
            il.data_release_name = record.search("//db_release").inner_text
            il.save
	end

