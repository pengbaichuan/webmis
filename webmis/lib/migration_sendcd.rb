require 'hpricot'
	doc = Hpricot.parse(File.read("/tmp/livesendcd.xml"))	
        data = doc.search("//tblsendcd")

	data.each do |record|
            o = OutboundOrder.new
            adate = record.search("/send_x0020_date").inner_text
            if (data.size > 4)
             begin
              adate =  DateTime.strptime(adate,"%Y-%m-%dT%H:%M:%S")
             rescue
              puts "invalid arrival date"
              adate = nil
             end
            end
            o.send_date = adate
            o.filename = record.search("/filename").inner_text
            o.map_no = record.search("/map_x0020_no").inner_text
            o.cd_no = record.search("/cd_x0020_no").inner_text
            o.type =  record.search("/test_x002f_master/").inner_text
            o.contact_person = record.search("/send_x0020_to").inner_text
            o.courier = record.search("/send_x0020_by").inner_text
            o.amount = record.search("/_x0023_/").inner_text
            o.extra = record.search("/extra").inner_text
            o.save
	end

