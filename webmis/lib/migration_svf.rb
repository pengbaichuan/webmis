#require '~/NetBeansProjects/WebMIS/config/environment'
require 'hpricot'
	doc = Hpricot.parse(File.read("/tmp/livesvf.xml"))	
        data = doc.search("//tblcaribbaconv")

	data.each do |record|
	  if record.search("/carconvid").inner_text && record.search("/carconvid").inner_text.to_i > 0
	    r = Conversion.new
	    r.id = record.search("//carconvid").inner_text.to_i
	    r.request_by = record.search("//carconvrequester").inner_text

	    if  record.search("//carconvrequestdate").inner_text &&  record.search("//carconvrequestdate").inner_text.size > 4
	      r.request_date =  DateTime.strptime(record.search("//carconvrequestdate").inner_text,"%Y-%m-%dT%H:%M:%S")
	    end

	    if  record.search("//carconvresultdate").inner_text &&  record.search("//carconvresultdate").inner_text.size > 4
	      r.conversion_date =  DateTime.strptime(record.search("//carconvresultdate").inner_text,"%Y-%m-%dT%H:%M:%S")
	    end  
	    supplier = "Tele Atlas"
            releaset = record.search("//carconvsupplrel").inner_text
	    release = DataRelease.includes(:datasource).where("data_releases.name = '#{releaset}' and datasources.name = '#{supplier}'").first
	    if (release)
	      r.data_release_id =  release.id
	      r.datasource_id = release.datasource_id
	      r.supplier_id = release.datasource.supplier_id
	    else
	      ds = Datasource.where("datasources.name = '#{supplier}'")
	      if (!ds)
		      ds = Datasource.new 
		      ds.name = supplier
		      ds.supplier_id = "9bef5a30-2034-01f5-3a80-47d68fb56db6" #sugarcrm 
		      ds.save
	      end
	      dr = DataRelease.new
	      dr.datasource_id = ds.id
	      dr.name = release
	      dr.save
	      r.data_release_id = dr.id
	      r.supplier_id = ds.supplier_id
	      r.datasource_id = ds.id
	    end

#	    if record.search("//productid")[0]
#		r.product =  record.search("/releaseconv/").inner_text
#   		r.productid =  record.search("//productid").inner_text
#	    end
	    
	    cvtool = record.search("//carconvcarribatool").inner_text
	    
	    c = Conversiontool.find_by_code(cvtool)
	    if !c
   	     c =  Conversiontool.new
             c.code = cvtool
	     c.description = cvtool
             c.tool_type = "Caribba"
             c.save
	    end
	    r.cvtool = cvtool
	    r.reception_reference = record.search("/carconvreceptdrf").inner_text
	    r.reference_numbers = record.search("/carconvdataref").inner_text
	    r.data_files = record.search("/carconvdatapath").inner_text
 
	    region = Region.find_by_name(record.search("/carconvarea").inner_text.strip)
           puts ":" + region.name + ":"

	    if (region)
	      #if region.class.name == "Region"
		r.region_id = region.id
	        r.region_name = region.name
	      #else
	      #  r.area_id = -20
	      #end
	    else
	        re = Region.new
                re.name = record.search("/carconvarea").inner_text.strip
	        re.type = "Area"
	        re.save
                r.region_id = re.id
	        r.region_name = re.name
	    end
            result = record.search("//carconvresult").inner_text
            r.result = result
            r.outcome = result
            r.purpose = record.search("/purpose").inner_text
            r.remarks = record.search("/carvonvremarks").inner_text
            r.type = "SvfConversion"
            #puts r.inspect
	    r.save 
            #puts "saved"
	  end
	end

