class CustomerContentValidValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if record.customer_content_specification.hash? && !value.to_s.blank?
      result = JSON.parse(value).class == Hash rescue false
      unless result
        puts "'#{value}' is not a valid hash in JSON format."
        record.errors[:value] << (options[:message] || "is not a valid hash in JSON format.")
        return
      end
    end

    if value.to_s.blank?
      puts "Empty content (not validated as otherwise you can't create the contents)."
      # if record.customer_content_specification.mandatory
      #   record.errors[:value] << (options[:message] || "may not be empty.")
      # end
    elsif !record.customer_content_specification.validation_code.blank?
      puts "Evaluating '#{self}.instance_eval(\"#{record.customer_content_specification.validation_code}\")'."
      begin
        unless record.instance_eval(record.customer_content_specification.validation_code)
          record.errors[:value] << (options[:message] || "did not satisfy the validaton code.")
        end
      rescue Exception => exc
        puts "ERROR: Validation exception for customer_content #{self.id}."
        puts exc
        record.errors[:value] << (options[:message] || "contains a syntax error in the validation code.")
      end
    end
  end
end