class WorkflowValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    record.errors[attribute] << (options[:message] || "'#{value}' is not a valid status for the current field values") unless record.valid_status?(value)
  end
end
