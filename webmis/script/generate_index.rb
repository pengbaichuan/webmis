def app_action_controllers_hash
  returnhash = {}
  Rails.application.routes.routes.map do |route|
    if returnhash[route.defaults[:controller]].nil?
      returnhash[route.defaults[:controller]] = [] << route.defaults[:action]
    else
      returnhash[route.defaults[:controller]] =  returnhash[route.defaults[:controller]] << route.defaults[:action]
    end
  end
  return returnhash
end

Rails.application.eager_load!

template = <<TEMPLATE
ThinkingSphinx::Index.define :<CLASSNAME>, :with => :active_record do

<INDEXSTR>
  
  <FIELDTEMPLATE>
  set_property :delta => true
end
TEMPLATE

indextemplate =
  "    indexes <COLUMNNAME>, :sortable => true\n"

# to create new index file
# rm app/indices/*
# rails runner script/generate_index.rb
# rake ts:rebuild

INDEXPATH = File.expand_path('../../app/indices/',  __FILE__)
amount = 254
ActiveRecord::Base.descendants.each do |c|
  begin
    # class name blacklist alphabetical ordered by line
    skip_array = [
      "ApiKey","ApiLog","AppModule","AreaType","AssetTypeNumberRange",
      "Call","Callsession","CategoryHierarchy","CategoryAttribute","CiqType","CityOrg","CommitmentDate","Contract",
      "ConversionJobProcessDataElement","CoveragesRegion","CarinProductset","CountryConversionProductset",
      "DashboardGrid","DashboardGridWidget","DashboardWidget","DataSetGroupLine","Datasource","DummyConversion","DhivePoi",
      "DatasourceCategory","DatasourceInternalcontact","DatasourceModule","DatasourceType","DesignTagHierarchy","DhiveMetadata",
      "Firsttier",
      "GroupRight",
      "HABTM_PoiMappingLayouts",
      "InboundOrder","InboundOrderLine","IncludedArticleLicensePurchaseOrderline",
      "Job",
      "Language","Location",
      "Matchmask","MailCondition",
      "Navdbfeature",
      "ProjectServer","ProjectUser","Point",
      "QlDatasource",
      "RegionModule","RegionRelation","RolesUser","ReceptionLine","ReleaseNoteAttachmentsProductCategory","RoleAction",
      "Region","RegionHierarchy",
      "Scenario","SchedulerLock","SeqInt","Sequence","SpaceReserved","Stat","Statistic","Supplier",
      "TmcInfo","TmcInfoTable","TmcTable",
      "UserMail","UserMailCondition","User",
      "ViewReceptionsAndConversiondb",
      "WarehouseStockAdjustment"
      ]
    next if skip_array.include?(c.name)
    next if c.name.match(/Version$/)
    next if c.name.match(/Versions$/)
    next if c.name.index("::") && c.name.index("::") > 0
    foundcols = false
    
    classfile = template.clone.gsub("<CLASSNAME>", c.name)
    fieldtemplate = "has "
    indexstr = ""
    idfound = false
    created_atfound = false
    c.columns.each do |col|
      next if col.name == "group_by"
      next if col.name == "stdout_log"
      next if col.name == "sendto"
      if col.name == "created_at"
        fieldtemplate = fieldtemplate + "created_at, updated_at "
        created_atfound = true
      end
       
      if col.name == "id"
        idfound = true
      end
      if col.type == :text || col.name == "id" || col.type == :string
        next if col.name == 'module'
        foundcols = true
        indexstr <<  indextemplate.gsub("<COLUMNNAME>",col.name)
      end
    end
    
    raise "Id not found for #{c.name}" if !idfound
    raise "Created_at not found for #{c.name}" if !idfound
    chash = app_action_controllers_hash
       
    if foundcols && chash[c.name.tableize] && chash[c.name.tableize].index("show")
      classfile.gsub!("<INDEXSTR>",indexstr)
      classfile.gsub!("<FIELDTEMPLATE>",fieldtemplate)
      fullpath = File.join(INDEXPATH,c.name.gsub("::","_") + "_index.rb")
      puts fullpath
      amount = amount - 1
      break if amount <= 0
      File.open(fullpath, 'w') { |file| file.write(classfile) }
    end
  rescue => e
    puts "NOT PROCESSED " + c.name + " #{e.message}"
  end
end;0
