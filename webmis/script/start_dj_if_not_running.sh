if ps aux | grep delayed_job | grep -v grep
then
  echo "DJ running"
else
  echo "DJ not running"
  cd /var/rails_apps/WebMIS/current
  script/delayed_job start
  echo "DJ Started"
fi
