require 'test_helper'

class CommitmentChangeReasonsControllerTest < ActionController::TestCase
  def setup
    session[:username] = users(:TESTUSER).user_name
    session[:user_id] = users(:TESTUSER).id
    session[:project_id] = projects(:PRODUCTION).id
  end

  test 'should get index' do
    get :index
    assert_response :success
    commitment_change_reasons = assigns(:commitment_change_reasons)
    assert_not_nil commitment_change_reasons
    assert_equal 2, commitment_change_reasons.size
  end

  test 'should get index by Ajax call' do
    xhr :get, :index
    assert_response :success
    commitment_change_reasons = assigns(:commitment_change_reasons)
    assert_not_nil commitment_change_reasons
    assert_equal 2, commitment_change_reasons.size
    assert_template 'commitment_change_reasons/_commitment_change_reason'
  end

  test 'search and sorting' do
    get :index, { search: 'modified' }, { sort: 'commitment_type' }, { direction: 'asc' }
    assert_response :success
    assert_equal 1, assigns(:commitment_change_reasons).size
    assert_template 'commitment_change_reasons/index'
    assert_template 'commitment_change_reasons/_commitment_change_reason'
  end

  test 'show one' do
    get :show, { id: commitment_change_reasons(:DEFAULT_BASELINE).id.to_s }
    assert_response :success
    assert_not_nil assigns(:commitment_change_reason)
    assert_template 'commitment_change_reasons/show'
  end

  test 'new one' do
    get :new
    assert_response :success
    assert_not_nil assigns(:commitment_change_reason)
    assert_template 'commitment_change_reasons/new'
    assert_template 'commitment_change_reasons/_form'
  end

  test 'edit one' do
    get :edit, { id: commitment_change_reasons(:DEFAULT_BASELINE).id.to_s }
    assert_response :success
    assert_not_nil assigns(:commitment_change_reason)
    assert_template 'commitment_change_reasons/edit'
    assert_template 'commitment_change_reasons/_form'
  end

  test 'create one' do
    post :create, commitment_change_reason: { reason: 'reason', description: 'description', commitment_type: ProductionOrder::COMMITMENT_BASELINE }
    assert_redirected_to commitment_change_reason_path(assigns(:commitment_change_reason))
    assert_equal 3, CommitmentChangeReason.count, 'The number of commitment change reasons should have increased by one.'
    assert_equal 'reason', assigns(:commitment_change_reason).reason
    assert_equal 'description', assigns(:commitment_change_reason).description
    assert_equal ProductionOrder::COMMITMENT_BASELINE, assigns(:commitment_change_reason).commitment_type
  end

  test 'failed validation for create' do
    post :create, commitment_change_reason: { reason: 'reason', description: 'description', commitment_type: ''}
    assert_response :success
    assert_equal 1, assigns(:commitment_change_reason).errors.count, 'One validation error expected.'    # commitment type can't be blank
    assert_template 'commitment_change_reasons/new'
  end

  test 'update one' do
    put :update, id: commitment_change_reasons(:DEFAULT_BASELINE).id.to_s, commitment_change_reason: { reason: 'reason' , description: 'description', commitment_type: ProductionOrder::COMMITMENT_MODIFIED }
    assert_redirected_to commitment_change_reason_path(assigns(:commitment_change_reason))
    assert_equal 2, CommitmentChangeReason.count, 'The number of commitment change reasons should stay the same.'
    assert_equal 'reason', assigns(:commitment_change_reason).reason
    assert_equal 'description', assigns(:commitment_change_reason).description
    assert_equal ProductionOrder::COMMITMENT_MODIFIED, assigns(:commitment_change_reason).commitment_type
  end

  test 'failed validation for update' do
    put :update, id: commitment_change_reasons(:DEFAULT_BASELINE).id.to_s, commitment_change_reason: { reason: '' , description: 'description', commitment_type: ProductionOrder::COMMITMENT_MODIFIED }
    assert_response :success
    assert_equal 1, assigns(:commitment_change_reason).errors.count, 'One validation error expected.'    # reason can't be blank
    assert_template 'commitment_change_reasons/edit'
  end

  test 'destroy one' do
    delete :destroy, id: commitment_change_reasons(:DEFAULT_BASELINE).id.to_s
    assert_equal 1, CommitmentChangeReason.count, 'The number of commitment change reasons should have decreased by one.'
    assert_redirected_to commitment_change_reasons_path
  end
end
