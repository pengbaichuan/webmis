require 'test_helper'

class DataSetsControllerTest < ActionController::TestCase
  def setup
    session[:username] = users(:TESTUSER).user_name
    session[:user_id] = users(:TESTUSER).id
    session[:project_id] = projects(:PRODUCTION).id
    request.env['HTTP_REFERER'] = 'dummy_url'
  end

  test 'should get index' do
    get :index
    assert_response :success
    data_sets = assigns(:data_sets)
    assert_not_nil data_sets
    assert_equal 12, data_sets.size
    assert_template 'data_sets/index'
    assert_template 'data_sets/_data_set'
  end

  test 'should get index by Ajax call' do
    xhr :get, :index
    assert_response :success
    assert_equal 12, assigns(:data_sets).size
    assert_template 'data_sets/_data_set'
  end

  test 'search and sorting' do
    get :index, { search: 'TEST Advanced City Models' }, { sort: 'data_sets.id' }, { direction: 'desc' }
    assert_response :success
    assert_equal 1, assigns(:data_sets).size
    assert_template 'data_sets/index'
    assert_template 'data_sets/_data_set'
  end

  test 'search on reception references not linked to a data_set' do
    get :index, { search: 'REFAA11' }
    assert_response :success
    assert_empty assigns(:data_sets)
  end

  test 'search on reception references directly linked to a data_set' do
    get :index, { search: 'BB2' }
    assert_response :success
    assert_equal 1, assigns(:data_sets).size
  end

  test 'search on reception references indirectly linked to a data_set' do
    get :index, { search: 'REFCC' }
    assert_response :success
    assert_equal 2, assigns(:data_sets).size
  end

  test 'search on reception references both directly and indirectly linked to different data_sets' do
    get :index, { search: 'REFDD44' }
    assert_response :success
    assert_equal 3, assigns(:data_sets).size
  end

  test 'search on reception references both directly and indirectly linked to the same data_set' do
    get :index, { search: 'REFEE55' }
    assert_response :success
    assert_equal 1, assigns(:data_sets).size
  end

  test 'column search id' do
    get :index, { column_search_field: { 'exact-data_sets-id': "#{data_sets(:DUMMY).id}" } }, { sort: 'data_sets.id'}, { direction: 'desc' }
    assert_response :success
    assert_equal 1, assigns(:data_sets).size
    assert_template 'data_sets/index'
  end

  test 'column company_abbrs.company_name' do
    get :index, { column_search_field: { 'exact-company_abbrs-id': "#{company_abbrs(:RDF_SUPPLIER1).id}" } }, { sort: 'company_abbrs.company_name'}, { direction: 'desc' }
    assert_response :success
    assert_equal 2, assigns(:data_sets).size
    assert_template 'data_sets/index'
  end

  test 'column data_releases.name' do
    get :index, { column_search_field: { 'exact-data_releases-id': "#{data_releases(:TEST_RELEASE).id}" } }, { sort: 'data_releases.name'}, { direction: 'asc' }
    assert_response :success
    assert_equal 2, assigns(:data_sets).size
    assert_template 'data_sets/index'
  end

  test 'column regions.name' do
    get :index, { column_search_field: { 'exact-regions-id': "#{regions(:TEST_REGION1).id}" } }, { sort: 'regions.name'}, { direction: 'desc' }
    assert_response :success
    assert_equal 3, assigns(:data_sets).size
    assert_template 'data_sets/index'
  end

  test 'column data_type.name' do
    get :index, { column_search_field: { 'exact-data_types-id': "#{data_types(:VAR).id}" } }, { sort: 'data_types.name'}, { direction: 'desc' }
    assert_response :success
    assert_equal 1, assigns(:data_sets).size
    assert_template 'data_sets/index'
  end

  test 'column fillings.name' do
    get :index, { column_search_field: { 'exact-fillings-id': "#{fillings(:TEST_FILLING).id}" } }, { sort: 'fillings.name'}, { direction: 'desc' }
    assert_response :success
    assert_equal 2, assigns(:data_sets).size
    assert_template 'data_sets/index'
  end

  test 'column data_sets.status' do
    get :index, { column_search_field: { 'exact-data_sets-status': '0' } }, { sort: 'data_sets.status'}, { direction: 'desc' }
    assert_response :success
    assert_equal 1, assigns(:data_sets).size
    assert_template 'data_sets/index'
  end

  test 'removed index' do
    get :index, { removed: 1 }
    assert_response :success
    assert_equal 13, assigns(:data_sets).size
    assert_template 'data_sets/index'
  end

  test 'removal index' do
    get :index, { removal: 1 }
    assert_response :success
    assert_equal 13, assigns(:data_sets).size
    assert_template 'data_sets/index'
  end

  test 'reset index' do
    get :index, { reset: 1}, { removal: 1}, { removed: 1}, { column_search_field: { 'exact-data_sets-status': '0' } }, { search: 'REFEE55' }
    assert_response :success
    assert_equal 12, assigns(:data_sets).size
    assert_template 'data_sets/index'
  end

  test 'show one' do
    get :show, { id: data_sets(:DUMMY).id.to_s }
    assert_response :success
    assert_not_nil assigns(:data_set)
    assert_template 'data_sets/show'
    assert_template 'data_sets/_show_data'
  end

  test 'handle one' do
    get :handle, { id: data_sets(:DUMMY).id.to_s }
    assert_response :success
    assert_not_nil assigns(:data_set)
    assert_template 'data_sets/handle'
    assert_template 'data_sets/_show_data'
  end

  test 'handle wrong project' do
    session[:project_id] = projects(:REGRESSION).id
    get :handle, { id: data_sets(:DUMMY).id.to_s }
    assert_redirected_to handle_data_set_path(data_sets(:DUMMY))
    assert_equal projects(:PRODUCTION).id, session[:project_id]
  end

  test 'new one' do
    get :new
    assert_response :success
    assert_template 'data_sets/new'
    assert_template 'data_sets/_form'
  end

  test 'edit one' do
    get :edit, { id: data_sets(:DUMMY).id.to_s }
    assert_response :success
    assert_not_nil assigns(:data_set)
    assert_template 'data_sets/edit'
    assert_template 'data_sets/_form'
  end

  test 'edit not allowed' do
    get :edit, { id: data_sets(:TEST1).id.to_s }
    assert_redirected_to in_use_orders_data_set_path(data_sets(:TEST1))
  end

  test 'no edit because of review' do
    get :edit, { id: data_sets(:TEST6).id.to_s }
    assert_redirected_to 'dummy_url'
  end

  test 'in use orders' do
    get :in_use_orders, { id: data_sets(:TEST1).id.to_s }
    assert_response :success
    assert_not_nil assigns(:data_set)
    assert_equal 1, assigns(:production_orders).count, 'Expecting 1 in use production order.'
    assert_template 'data_sets/in_use_orders'
  end

  test 'create minimal' do
    old_count = DataSet.count
    post :create, data_set: { name: 'Testing Coverage Create' , region_id: regions(:TEST_REGION2).id, data_type_id: data_types(:DOC).id,
                              company_abbr_id: company_abbrs(:MAPSCAPE).id, data_release_id: data_releases(:TEST_RELEASE).id } , coverage_regions: ''
    assert_redirected_to data_set_path(assigns(:data_set))
    assert_equal regions(:TEST_REGION2), assigns(:data_set).region
    assert_equal data_types(:DOC), assigns(:data_set).data_type
    assert_equal company_abbrs(:MAPSCAPE), assigns(:data_set).company_abbr
    assert_equal data_releases(:TEST_RELEASE), assigns(:data_set).data_release
    assert_equal old_count + 1, DataSet.count, 'The number of DataSets should have increased by 1.'
  end

  test 'create with coverage and new data set group' do
    old_count = DataSet.count
    old_group_count = DataSetGroup.count
    post :create, data_set: { name: 'Testing Create' , region_id: regions(:TEST_REGION2).id, data_type_id: data_types(:POI).id,
                              company_abbr_id: company_abbrs(:MAPSCAPE).id, data_release_id: data_releases(:TEST_RELEASE).id, filling_id: fillings(:NOKF).id,
                              description: 'test\r\ntest', coverage_id: '', remarks: 'remarks\r\nremarks' },
         coverage_regions: "#{regions(:Netherlands).id},#{regions(:Belgium).id}", region_selector: '', data_set_definition_selector: '',
         parameters_json: { "#{regions(:Netherlands).id}" => '', "#{regions(:Belgium).id}" => '' },
         do_not_process: { "#{regions(:Netherlands).id}" => '', "#{regions(:Belgium).id}" => '' }, parameters: '', data_set_group_id: '', create_group: '1'
    assert_redirected_to data_set_path(assigns(:data_set))
    assert_equal fillings(:NOKF), assigns(:data_set).filling
    assert_equal 'test\r\ntest', assigns(:data_set).description
    assert_equal 'remarks\r\nremarks', assigns(:data_set).remarks
    assert_equal old_count + 1, DataSet.count, 'The number of DataSets should have increased by 1.'
    assert_equal old_group_count + 1, DataSetGroup.count, 'The number of DataSetGroups should have increased by 1.'
    assert_equal 1, assigns(:data_set).data_set_groups.count, 'The data set should belong to 1 data set group.'
    assert_equal DataSetGroup.last, assigns(:data_set).data_set_groups.first
  end

  test 'create with parameters and adding to data set group' do
    old_count = DataSet.count
    old_group_count = DataSetGroup.count
    post :create, data_set: { name: 'Testing Create' , region_id: regions(:TEST_REGION2).id, data_type_id: data_types(:VAR).id,
                              company_abbr_id: company_abbrs(:MAPSCAPE).id, data_release_id: data_releases(:TEST_RELEASE).id, filling_id: '',
                              description: 'test\r\ntest', remarks: 'remarks\r\nremarks' },
         coverage_regions: "#{regions(:Netherlands).id},#{regions(:Belgium).id}", region_selector: '', data_set_definition_selector: '',
         parameters_json: { "#{regions(:Netherlands).id}" => '[{"parameter_name":"Use 3D Templates","parameter_value":"1"}]', "#{regions(:Belgium).id}" => '' },
         do_not_process: { "#{regions(:Netherlands).id}" => '', "#{regions(:Belgium).id}" => 'true' }, parameters: '', data_set_group_id: data_set_groups(:DEFAULT).id
    assert_redirected_to data_set_path(assigns(:data_set))
    assert_equal old_count + 1, DataSet.count, 'The number of DataSets should have increased by 1.'
    assert_equal old_group_count, DataSetGroup.count, 'The number of DataSetGroups should have stayed the same.'
    assert_equal 1, assigns(:data_set).data_set_groups.count, 'The data set should belong to 1 data set group.'
    assert_equal data_set_groups(:DEFAULT), assigns(:data_set).data_set_groups.first
    assert_equal '[{"parameter_name":"Use 3D Templates","parameter_value":"1"}]', assigns(:data_set).coverage.coverages_regions.find_by_region_id(regions(:Netherlands).id).parameter_settings
    assert_equal '', assigns(:data_set).coverage.coverages_regions.find_by_region_id(regions(:Belgium).id).parameter_settings
    assert_not assigns(:data_set).coverage.coverages_regions.find_by_region_id(regions(:Netherlands).id).do_not_process, 'Netherlands should be processed.'
    assert assigns(:data_set).coverage.coverages_regions.find_by_region_id(regions(:Belgium).id).parameter_settings, 'Belgium should not be processed.'
  end

  test 'failed validation for create' do
    old_count = DataSet.count
    post :create, data_set: { name: '' },  coverage_regions: ''
    assert_response :success
    assert_equal 4, assigns(:data_set).errors.count, 'Four validation errors expected as some mandatory attributes are missing.'
    assert_template 'data_sets/new'
    assert_equal old_count, DataSet.count, 'The number of DataSets should not have changed.'
  end

  test 'update minimal' do
    old_count = DataSet.count
    post :update, id: data_sets(:DUMMY).id.to_s, data_set: { data_type_id: data_types(:RDF).id, filling_id: fillings(:NOKF).id }, coverage_regions: ''
    assert_redirected_to data_set_path(data_sets(:DUMMY))
    assert_nil assigns(:data_set).filling   # filling should be reset to nil as the current data type of the data set can't have a filling.
    assert_equal old_count, DataSet.count, 'The number of DataSets should not have changed.'
    assert_equal DataSet::STATUS_DRAFT, assigns(:data_set).status
  end

  test 'update maximal' do
    post :update, id: data_sets(:TEST1).id.to_s, data_set: { name: 'Testing Create' , region_id: regions(:TEST_REGION2).id, data_type_id: data_types(:POI).id,
                                                             company_abbr_id: company_abbrs(:MAPSCAPE).id, data_release_id: data_releases(:TEST_RELEASE).id,
                                                             filling_id: fillings(:NOKF).id, description: 'test\r\ntest', coverage_id: '',
                                                             remarks: 'remarks\r\nremarks' },
         coverage_regions: "#{regions(:Netherlands).id},#{regions(:Belgium).id}", region_selector: '', data_set_definition_selector: '',
         parameters_json: { "#{regions(:Netherlands).id}" => '[{"parameter_name":"Use 3D Templates","parameter_value":"1"}]', "#{regions(:Belgium).id}" => '' },
         do_not_process: { "#{regions(:Netherlands).id}" => '', "#{regions(:Belgium).id}" => 'true' }, parameters: ''
    assert_redirected_to data_set_path(data_sets(:TEST1))
    assert_equal regions(:TEST_REGION2), assigns(:data_set).region
    assert_equal data_types(:POI), assigns(:data_set).data_type
    assert_equal company_abbrs(:MAPSCAPE), assigns(:data_set).company_abbr
    assert_equal data_releases(:TEST_RELEASE), assigns(:data_set).data_release
    assert_equal fillings(:NOKF), assigns(:data_set).filling
    assert_equal 'test\r\ntest', assigns(:data_set).description
    assert_equal 'remarks\r\nremarks', assigns(:data_set).remarks
    assert_equal '[{"parameter_name":"Use 3D Templates","parameter_value":"1"}]', assigns(:data_set).coverage.coverages_regions.find_by_region_id(regions(:Netherlands).id).parameter_settings
    assert_equal '', assigns(:data_set).coverage.coverages_regions.find_by_region_id(regions(:Belgium).id).parameter_settings
    assert_not assigns(:data_set).coverage.coverages_regions.find_by_region_id(regions(:Netherlands).id).do_not_process, 'Netherlands should be processed.'
    assert assigns(:data_set).coverage.coverages_regions.find_by_region_id(regions(:Belgium).id).parameter_settings, 'Belgium should not be processed.'
    assert_equal DataSet::STATUS_IN_PROGRESS, assigns(:data_set).status
  end

  test 'failed validation for update' do
    post :update, id: data_sets(:DUMMY).id.to_s, data_set: {name: '', region_id: '', data_type_id: '', company_abbr_id: '', data_release_id: '', filling_id: '',
                description: '', coverage_id: '', remarks: ''}, coverage_regions: ''
    assert_response :success
    assert_equal 4, assigns(:data_set).errors.count, 'Four validation errors expected as some mandatory attributes are now missing.'
    assert_template 'data_sets/edit'
  end

  test 'destroy one' do
    old_count = DataSet.count
    delete :destroy, id: data_sets(:DUMMY).id.to_s
    assert_equal old_count - 1, DataSet.count, 'The number of data sets should have decreased by one.'
    assert_redirected_to data_sets_url
  end

  test 'receptions for select' do
    get :receptions_for_select, format: 'json'
    assert_equal 2, JSON.parse(@response.body).count
    assert_equal receptions(:TEST4).id, JSON.parse(@response.body).last['id']
  end

  test 'receptions for select with search' do
    get :receptions_for_select, q: 'REFEE55', format: 'json'
    assert_equal 1, JSON.parse(@response.body).count
    assert_equal receptions(:TEST5).id, JSON.parse(@response.body).last['id']
  end
end