require 'test_helper'

class ParameterSettingsControllerTest < ActionController::TestCase
  def setup
    session[:username] = users(:TESTUSER).user_name
    session[:user_id] = users(:TESTUSER).id
    session[:project_id] = projects(:PRODUCTION).id
  end

  test 'should get index' do
    get :index
    assert_response :success
    parameter_settings = assigns(:parameter_settings)
    assert_not_nil parameter_settings
    assert_equal 1, parameter_settings.size
  end

  test 'should get index by Ajax call' do
    xhr :get, :index
    assert_response :success
    parameter_settings = assigns(:parameter_settings)
    assert_not_nil parameter_settings
    assert_equal 1, parameter_settings.size
    assert_template 'parameter_settings/_parameter_setting'
  end

  test 'search and sorting' do
    get :index, { search: 'NTG55H' }, { sort: 'name' }, { direction: 'asc' }
    assert_response :success
    assert_equal 1, assigns(:parameter_settings).size
    assert_template 'parameter_settings/index'
    assert_template 'parameter_settings/_parameter_setting'
  end

  test 'show one' do
    get :show, { id: parameter_settings(:GMN_PARAMS).id.to_s }
    assert_response :success
    assert_not_nil assigns(:parameter_setting)
    assert_template 'parameter_settings/show'
  end


  test 'edit one' do
    get :edit, { id: parameter_settings(:GMN_PARAMS).id.to_s }
    assert_response :success
    assert_not_nil assigns(:parameter_setting)
    assert_template 'parameter_settings/edit'
    assert_template 'parameter_settings/_form'
  end

  test 'create one' do
    post :create, parameter_setting: {}#, company_abbr_id: company_abbrs(:GMN).id }
    assert_redirected_to edit_parameter_setting_path(assigns(:parameter_setting))
    assert_equal 2, ParameterSetting.count, 'The number of parameter settings should have increased by one.'
  end


  test 'update one' do
    put :update, id: parameter_settings(:GMN_PARAMS).id.to_s, parameter_setting: { name: 'nameupdate' , comment: 'abbrupdate' }
    assert_response :success
    assert_equal 'nameupdate', assigns(:parameter_setting).name
    assert_equal 'abbrupdate', assigns(:parameter_setting).comment
  end

 

end
