require 'test_helper'

class ProductLinesControllerTest < ActionController::TestCase
  def setup
    session[:username] = users(:TESTUSER).user_name
    session[:user_id] = users(:TESTUSER).id
    session[:project_id] = projects(:PRODUCTION).id
  end

  test 'should get index' do
    get :index
    assert_response :success
    product_lines = assigns(:product_lines)
    assert_not_nil product_lines
    assert_equal 2, product_lines.size
  end

  test 'should get index by Ajax call' do
    xhr :get, :index
    assert_response :success
    product_lines = assigns(:product_lines)
    assert_not_nil product_lines
    assert_equal 2, product_lines.size
    assert_template 'product_lines/_product_line'
  end

  test 'search and sorting' do
    get :index, { search: 'dHive' }, { sort: 'abbr' }, { direction: 'asc' }
    assert_response :success
    assert_equal 1, assigns(:product_lines).size
    assert_template 'product_lines/index'
    assert_template 'product_lines/_product_line'
  end

  test 'show one' do
    get :show, { id: product_lines(:NTG55H).id.to_s }
    assert_response :success
    assert_not_nil assigns(:product_line)
    assert_template 'product_lines/show'
  end

  test 'new one' do
    get :new
    assert_response :success
    assert_not_nil assigns(:product_line)
    assert_template 'product_lines/new'
    assert_template 'product_lines/_form'
  end

  test 'edit one' do
    get :edit, { id: product_lines(:NTG55H).id.to_s }
    assert_response :success
    assert_not_nil assigns(:product_line)
    assert_template 'product_lines/edit'
    assert_template 'product_lines/_form'
  end

  test 'create one' do
    post :create, product_line: { name: 'BLA123', abbr: 'abbr', company_abbr_id: company_abbrs(:GMN).id }
    assert_redirected_to product_line_path(assigns(:product_line))
    assert_equal 3, ProductLine.count, 'The number of product lines should have increased by one.'
    assert_equal 'BLA123',assigns(:product_line).name
  end

  test 'failed validation for create' do
    post :create, product_line: { name: 'BLA123', abbr: nil, company_abbr_id: company_abbrs(:GMN).id }
    assert_response :success
    assert_equal 1, assigns(:product_line).errors.count, 'One validation error expected. - Abbr is mandatory'    # commitment type can't be blank
    assert_template 'product_lines/new'
  end

  test 'update one' do
    put :update, id: product_lines(:NTG55H).id.to_s, product_line: { name: 'nameupdate' , abbr: 'abbrupdate' }
    assert_redirected_to product_line_path(assigns(:product_line))
    assert_equal 2, ProductLine.count, 'The number of product lines should stay the same.'
    assert_equal 'nameupdate', assigns(:product_line).name
    assert_equal 'abbrupdate', assigns(:product_line).abbr
  end

  test 'failed validation for update' do
    put :update, id: product_lines(:NTG55H).id.to_s, product_line: { name: ''}
    assert_response :success
    assert_equal 1, assigns(:product_line).errors.count, 'One validation error expected.'    # reason can't be blank
    assert_template 'product_lines/edit'

    put :update, id: product_lines(:NTG55H).id.to_s, product_line: { abbr: ''}
    assert_response :success
    assert_equal 1, assigns(:product_line).errors.count, 'One validation error expected.'    # reason can't be blank
    assert_template 'product_lines/edit'
  end

end
