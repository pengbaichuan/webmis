require 'test_helper'

class ShippingOrdersControllerTest < ActionController::TestCase
  def setup
    session[:username] = users(:TESTUSER).user_name
    session[:user_id] = users(:TESTUSER).id
    session[:project_id] = projects(:PRODUCTION).id
  end

  test 'should get index' do
    get :index
    assert_response :success
    shipping_orders = assigns(:shipping_orders)
    assert_not_nil shipping_orders
    assert_equal 2, shipping_orders.size
  end

  test 'should get index by Ajax call' do
    xhr :get, :index
    assert_response :success
    shipping_orders = assigns(:shipping_orders)
    assert_not_nil shipping_orders
    assert_equal 2, shipping_orders.size
    assert_template 'shipping_orders/_shipping_order'
  end

  test 'search uninvoiced' do
    get :index, { uninvoiced: 'true' }
    assert_response :success
    assert_equal 1, assigns(:shipping_orders).size
    assert_template 'shipping_orders/index'
    assert_template 'shipping_orders/_shipping_order'
  end

  test 'search and sorting' do
    get :index, { search: '111111111' }, { sort: 'customer_depots.label' }, { direction: 'asc' }
    assert_response :success
    assert_equal 1, assigns(:shipping_orders).size
    assert_template 'shipping_orders/index'
    assert_template 'shipping_orders/_shipping_order'
  end

  test 'column search id' do
    get :index, {column_search_field: { 'shipping_orders-id': "#{shipping_orders(:DEFAULT_ORDER).id}" } }, { sort: 'shipping_orders.id' }, { direction: 'desc' }
    assert_response :success
    assert_equal 1, assigns(:shipping_orders).size
    assert_template 'shipping_orders/index'
  end

  test 'column search status' do
    get :index, {column_search_field: { 'shipping_orders-status': "#{ShippingOrder::STATUS_SHIPPED}" } }, { sort: 'shipping_orders.status' }, { direction: 'desc' }
    assert_response :success
    assert_equal 1, assigns(:shipping_orders).size
    assert_template 'shipping_orders/index'
  end

  test 'column search order by' do
    get :index, {column_search_field: { 'customer_depots-label': 'Label2'} }, { sort: 'customer_depots.label' }, { direction: 'desc' }
    assert_response :success
    assert_equal 1, assigns(:shipping_orders).size
    assert_template 'shipping_orders/index'
  end

  test 'column search shipping to' do
    get :index, {column_search_field: { 'shipping_customer_depots_shipping_orders-label': 'Label3' } }, { sort: 'shipping_customer_depots_shipping_orders.label' }, { direction: 'desc' }
    assert_response :success
    assert_equal 1, assigns(:shipping_orders).size
    assert_template 'shipping_orders/index'
  end

  test 'column search invoicing to' do
    get :index, {column_search_field: { 'invoicing_customer_depots_shipping_orders-label': 'Label2' } }, { sort: 'invoicing_customer_depots_shipping_orders.label' }, { direction: 'desc' }
    assert_response :success
    assert_equal 1, assigns(:shipping_orders).size
    assert_template 'shipping_orders/index'
  end

  test 'column search po_reference' do
    get :index, {column_search_field: { 'shipping_orders-po_reference': '0' } }, { sort: 'shipping_orders.po_reference' }, { direction: 'desc' }
    assert_response :success
    assert_equal 1, assigns(:shipping_orders).size
    assert_template 'shipping_orders/index'
  end

  test 'column search article' do
    get :index, {column_search_field: { 'articles-customer_reference': 'Ref2'} }, { sort: 'articles.customer_reference' }, { direction: 'desc' }
    assert_response :success
    assert_equal 1, assigns(:shipping_orders).size
    assert_template 'shipping_orders/index'
  end

  test 'show one' do
    get :show, { id: shipping_orders(:DEFAULT_ORDER).id.to_s }
    assert_response :success
    assert_not_nil assigns(:shipping_order)
    assert_template 'shipping_orders/show'
  end

  test 'edit one' do
    get :edit, { id: shipping_orders(:DEFAULT_ORDER).id.to_s }
    assert_response :success
    assert_not_nil assigns(:shipping_order)
    assert_template 'shipping_orders/edit'
    assert_template 'shipping_orders/_form_fields'
  end

  test 'create one' do
    post :create, shipping_order: { article_id: articles(:DUMMY_ARTICLE).id, po_reference: 'Z', ordering_customer_depot_id: customer_depots(:DUMMY_DEPOT).id,
                shipping_customer_depot_id: customer_depots(:DUMMY_DEPOT).id, invoicing_customer_depot_id: customer_depots(:DUMMY_DEPOT).id }
    assert_redirected_to shipping_order_path(assigns(:shipping_order))
    assert_equal 3, ShippingOrder.count, 'The number of shipping orders should have increased by one.'
  end

  test 'failed validation for create' do
    post :create, shipping_order: { status: ShippingOrder::STATUS_DRAFT }
    assert_response :success
    assert_equal 5, assigns(:shipping_order).errors.count, 'Five validation errors expected as all mandatory attributes are missing.'
    assert_template 'shipping_orders/new'
  end

  test 'update one' do
    put :update, id: shipping_orders(:DEFAULT_ORDER).id.to_s, shipping_order: { source: 'SMB', po_reference: 'Y' }
    assert_redirected_to shipping_order_path(assigns(:shipping_order))
    assert_equal 'SMB', assigns(:shipping_order).source
    assert_equal 'Y', assigns(:shipping_order).po_reference
  end

  test 'failed validation for update' do
    put :update, id: shipping_orders(:DEFAULT_ORDER).id.to_s, shipping_order: { po_reference: nil }
    assert_response :success
    assert_equal 1, assigns(:shipping_order).errors.count, 'One validation error expected.'
    assert_template 'shipping_orders/edit'
  end

  test 'cancel' do
    request.env['HTTP_REFERER'] = shipping_orders_path
    post :cancel, id: shipping_orders(:DEFAULT_ORDER).id.to_s
    assert_redirected_to shipping_orders_path
    # shipping_orders(:DEFAULT_ORDER) is the last one in ShippingOrder
    assert_equal ShippingOrder::STATUS_CANCELLED, ShippingOrder.last.status, 'Status should be Cancelled.'
  end
end