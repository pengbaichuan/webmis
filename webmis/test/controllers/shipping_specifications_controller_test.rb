require 'test_helper'

class ShippingSpecificationsControllerTest < ActionController::TestCase
  def setup
    session[:username] = users(:TESTUSER).user_name
    session[:user_id] = users(:TESTUSER).id
    session[:project_id] = projects(:PRODUCTION).id
  end

  test 'should get index' do
    get :index
    assert_response :success
    shipping_specifications = assigns(:shipping_specifications)
    assert_not_nil shipping_specifications
    assert_equal 1, shipping_specifications.size
  end

  test 'should get index by Ajax call' do
    xhr :get, :index
    assert_response :success
    shipping_specifications = assigns(:shipping_specifications)
    assert_not_nil shipping_specifications
    assert_equal 1, shipping_specifications.size
    assert_template 'shipping_specifications/_shipping_specification'
  end

  test 'search and sorting' do
    get :index, { search: 'Shipping Test' }, { direction: 'asc' }
    assert_response :success
    assert_equal 1, assigns(:shipping_specifications).size
    assert_template 'shipping_specifications/index'
    assert_template 'shipping_specifications/_shipping_specification'
  end

  test 'show one' do
    get :show, { id: shipping_specifications(:SHIP_TEST).id.to_s }
    assert_response :success
    assert_not_nil assigns(:shipping_specification)
    assert_template 'shipping_specifications/show'
  end

  test 'new one' do
    get :new
    assert_response :success
    assert_not_nil assigns(:shipping_specification)
    assert_template 'shipping_specifications/new'
    assert_template 'shipping_specifications/_form'
  end

  test 'edit one' do
    get :edit, { id: shipping_specifications(:SHIP_TEST).id.to_s }
    assert_response :success
    assert_not_nil assigns(:shipping_specification).name
    assert_template 'shipping_specifications/edit'
    assert_template 'shipping_specifications/_form'
  end

  test 'create one' do
    post :create, shipping_specification: { name: 'testshipspec' }
    assert_redirected_to shipping_specification_path(assigns(:shipping_specification))
    assert_equal 2, ShippingSpecification.count, 'The number of commitment change reasons should have increased by one.'
    assert_equal 'testshipspec', assigns(:shipping_specification).name
    
  end
  

  test 'update one' do
    put :update, id: shipping_specifications(:SHIP_TEST).id.to_s, shipping_specification: { name: 'TESTSHIPSPEC'  }
    assert_redirected_to shipping_specification_path(assigns(:shipping_specification))
    assert_equal 1, ShippingSpecification.count, 'The number of shipping specification should stay the same.'
    assert_equal 'TESTSHIPSPEC', assigns(:shipping_specification).name
  end

  
  test 'destroy one' do
    delete :destroy, id: shipping_specifications(:SHIP_TEST).id.to_s
    assert_equal 0, ShippingSpecification.count, 'The number of shipping_specification should have decreased by one.'
    assert_redirected_to shipping_specifications_path
  end
end
