require 'test_helper'

class SuppliersTest < ActionDispatch::IntegrationTest

  test 'supplier update' do
    ds = DataSet.last
    ca = ds.company_abbr
    old_supplier = ds.company_abbr.company_name
    new_supplier = old_supplier + '_NEW'
    ca.company_name = new_supplier
    ca.save!
    ca.reload
    ds.update_tag_list
    ds.save!
    # process data and related tag list should be updated as well
    ds.reload
    assert_equal new_supplier.downcase , ds.company_abbr.company_name.downcase, "Expecting data set #{ds.id} to be updated to new supplier name."
    assert_nil ds.tag_list.index(old_supplier.downcase), "Old supplier name #{old_supplier.downcase} found in data set #{ds.id} tag list #{ds.tag_list}."
  end
end
