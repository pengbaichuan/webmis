require 'test_helper'

class WorkflowTest < ActionDispatch::IntegrationTest
  def setup
    # ids in the model JSON cannot be filled by the yml references, fix the ones being used.
    ProductDesign.all.each do |pd|
      pd.modelhash = JSON.parse(pd.model)
      pd.modelhash['product_line']['id'] = ProductLine.find_by_abbr(pd.modelhash['product_line']['abbr']).id
      pd.save!
    end

    # tag lists are too hard to set up in the yml file. Just generate them
    DataSet.all.each do |ds|
      ds.update_tag_list
      ds.save!
    end

    # the workflow tests assume some files to be present on the file system, generate dummy ones.
    symlinked = '/volumes2/test/symlinked'
    dir_list = [ symlinked,
                 '/volumes2/production_tests/test/data/input',
                 '/volumes2/production_tests/test/data/output/output',
                 '/volumes2/production_tests/test/data/output/ops',
                 '/volumes2/production_tests/test/data/packing_job',
                 '/volumes2/production_tests/test/data/process/backofficedevelopment',
                 '/volumes2/production_tests/test/data/product',
                 '/volumes2/production_tests/test/data/output/temp/incoming',
                 '/volumes2/production_tests/test/data/output/temp/outgoing'
               ]
    FileUtils.mkdir_p dir_list, mode: 0777
    ProcessDataElement.all.each do |e|
      target_file = e.process_data_specification.directory + '.test'
      target_path = File.join(e.directory, target_file)
      FileUtils.mkdir_p File.join(e.directory, 'dummy'), mode: 0777
      FileUtils.touch File.join(symlinked, target_file)
      FileUtils.ln_s File.join(symlinked, target_file), target_path unless File.exist?(target_path)
    end
    FileTransferAccount.all.each do |a|
      # TODO: Proper test sftp account
      a.user_name ||= 'Rails.test'
      a.password = 'mooiweervandaag'
      a.save!
    end
  end

  test 'frontend workflow' do
    checklog = Checklog.new(only_check = true, Checklog::LOGLEVEL_WARNING)
    data_set_hash = {}
    DataSet.where(updated_by: 'frontend.test').each{|d| data_set_hash[d.id.to_s] = [d.region.region_code]}
    design = ProductDesign.find_by_name('Front-End RDF2DH HER')
    match_report = design.create_process_data_matchreport(data_set_hash)
    match_report.matches.each{ |x| x.include = true }

    po = design.create_production_order_from_process_data(data_set_hash.keys.first.to_i,
                                                          data_set_hash,
                                                          Conversiontool.find_by_code('dev_latest').id,
                                                          CvtoolBundle.find_by_name('dev').id,
                                                          Ordertype.find_by_name('FrontEnd').id,
                                                          User.find_by_user_name('johan.hendrikx').id,
                                                          'Production',
                                                          true,
                                                          'latest',
                                                          [true, true],
                                                          checklog,
                                                          match_report,
                                                          Project.find_by_name('Production').id,
                                                          true)


    assert_not_nil po.id, "Front end production order does not have an id.\n#{po.checklog.to_s}"
    assert_equal 'PRODUCTION', po.bp_name, "Wrong business process for front end production order.\n#{po.checklog.to_s}"
    assert_equal 1, po.production_orderlines.size, "Wrong number of production orderlines for front end production order.\n#{po.checklog.to_s}"
    pol = po.production_orderlines.first
    assert_equal 'SELECT DATARELEASE', pol.bp_name, "Wrong business process for front end production order.\n#{po.checklog.to_s}"
    assert_equal ProductionOrderline::BP_STATUS_ACTIVE, pol.bp_status, "Wrong business process status '#{pol.bp_status_str}' for front end production order.\n#{po.checklog.to_s}"
    assert_equal 1, pol.conversions.size, "Wrong number of conversions for front end production order.\n#{po.checklog.to_s}"
    conv = pol.conversions.first
    assert_equal Conversion::STATUS_REQUESTED, conv.status, "Wrong status '#{conv.status_str}' for conversion of front end production order.\n#{po.checklog.to_s}"
    assert_equal 0, conv.conversion_jobs.size, "Wrong number of conversion jobs for front end production order.\n#{po.checklog.to_s}"
    assert_equal 0, conv.conversion_databases.size, "Wrong number of conversion databases for front end production order.\n#{po.checklog.to_s}"

    po.execute
    assert !po.checklog.errors_or_warnings? , "Found errors or warnings in checklog of front end production order:\n#{po.checklog.to_s}"

    assert_equal 'PRODUCTION', po.bp_name, "Wrong business process for front end production order.\n#{po.checklog.to_s}"
    pol.reload
    assert_equal 'MONITOR CONVERSION', pol.bp_name, "Wrong business process for production orderline of front end production order.\n#{po.checklog.to_s}"
    assert_equal ProductionOrderline::BP_STATUS_PROCESSING, pol.bp_status, "Wrong business process status '#{pol.bp_status_str}' for front end production order.\n#{po.checklog.to_s}"
    design = JSON.parse(pol.product_design)
    resource = design['conversion_steps'][0]['connections'][0]['source_spec']['datasets'][0]['file_resources'][0]
    assert resource.keys.index('realpath'), "Production orderline of front end production order expected resource '#{resource.inspect}' to contain 'realpath'.\n#{po.checklog.to_s}"
    conv.reload
    assert_equal Conversion::STATUS_CONVERSION, conv.status, "Wrong status '#{conv.status_str}' for conversion of front end production order.\n#{po.checklog.to_s}"
    assert_equal 1, conv.conversion_jobs.size, "Wrong number of conversion jobs for front end production order.\n#{po.checklog.to_s}"
    assert_equal 0, conv.conversion_databases.size, "Wrong number of conversion databases for front end production order.\n#{po.checklog.to_s}"
    job = conv.conversion_jobs.first
    assert_equal Job::STATUS_QUEUED, job.status, "Wrong status '#{job.status_str}' for conversion job of front end production order.\n#{po.checklog.to_s}"
    assert_equal Job::FILE_SYSTEM_STATUS_FREE, job.file_system_status, "Wrong file system status '#{job.file_system_status_str}' for conversion job of front end production order.\n#{po.checklog.to_s}"
    assert_match /SYMLINK/, job.job_script.to_dakota(job.id,job.id), "Missing comments type in conversion job of front end production order.\n#{po.checklog.to_s}"
    assert_not_equal 0, job.versions.size, "No job versions found for front end production order.\n#{po.checklog.to_s}"

    server = Server.find_by_server_id('localhost')
    server.active = false
    server.save!
    ProductionOrder.schedule
    job.reload
    assert_equal Job::STATUS_HOLD, job.status, "Wrong status '#{job.status_str}' for conversion job of front end production order.\n#{job.scheduler_log}\n#{po.checklog.to_s}"
    server.active = true
    server.save!

    ProductionOrder.schedule
    conv.reload
    job.reload
    assert_equal Job::STATUS_PROCESSING, job.status, "Wrong status '#{job.status_str}' for conversion job of front end production order.\n#{job.scheduler_log}\n#{po.checklog.to_s}"
    assert_equal Job::FILE_SYSTEM_STATUS_ON_SERVER, job.file_system_status, "Wrong file system status '#{job.file_system_status_str}' for conversion job of front end production order.\n#{po.checklog.to_s}"
    assert_equal Conversion::STATUS_CONVERSION, conv.status, "Wrong status '#{conv.status_str}' for conversion of front end production order.\n#{po.checklog.to_s}"

    po.execute
    assert !po.checklog.errors_or_warnings? , "Found errors or warnings in checklog of front endproduction order:\n#{po.checklog.to_s}"

    pol.reload
    conv.reload
    job.reload
    assert_equal 'TOTAL: 1 ACTIVE: 1', pol.bp_message, "Wrong business process message for front end production orderline.\n#{po.checklog.to_s}"
    assert_equal Job::STATUS_PROCESSING, job.status, "Wrong status '#{job.status_str}' for conversion job of front end production order.\n#{po.checklog.to_s}"
    assert job.working_dir_exists?, "The working dir '#{job.working_dir}' of the conversion job of front end production order should exist on server '#{job.run_server}'.\n#{po.checklog.to_s}"

    v = job.version
    ProductionOrder.schedule
    job.reload
    conv.reload
    assert_equal Job::STATUS_COMPLETE, job.status, "Wrong status '#{job.status_str}' for conversion job of front end production order.\n#{po.checklog.to_s}"
    assert_not_equal v, job.version, "Version number of conversion job of front end production order should have been increased.\n#{po.checklog.to_s}"
    assert !job.working_dir_exists?, "The working dir '#{job.working_dir}' of the conversion job of front end production_order should no longer exist on server '#{job.run_server}'.\n#{po.checklog.to_s}"
    assert_equal Job::FILE_SYSTEM_STATUS_FREE, job.file_system_status, "Wrong file system status '#{job.file_system_status_str}' for conversion job of front end production order.\n#{po.checklog.to_s}"
    assert_equal 1, job.conversion_databases.size, "Wrong number of conversion databases for front end production order.\n#{po.checklog.to_s}"
    cd = job.conversion_databases.first
    assert_equal Conversion::STATUS_CONVERSION, conv.status, "Wrong status '#{conv.status_str}' for conversion of front end production order.\n#{po.checklog.to_s}"
    assert_equal 1, conv.conversion_databases.size, "Wrong number of conversion databases for front end production order.\n#{po.checklog.to_s}"
    assert_equal cd.id, conv.conversion_databases.first.id, "The conversion and the conversion job of the front end production order should have the same conversion database.\n#{po.checklog.to_s}"
    assert_equal ConversionDatabase::STATUS_VALIDATING, cd.status, "Wrong status '#{cd.status_str}' for conversion database of front end production order.\n#{po.checklog.to_s}"
    assert_equal ConversionDatabase::FILE_SYSTEM_STATUS_AVAILABLE, cd.file_system_status, "Wrong file system status '#{cd.file_system_status_str}' for conversion database of front end production order.\n#{po.checklog.to_s}"
    assert_equal 1, cd.validation_jobs.size, "Wrong number of validation jobs for front end production order.\n#{po.checklog.to_s}"
    vj = cd.validation_jobs.first
    assert_equal Job::STATUS_QUEUED, vj.status, "Wrong status '#{job.status_str}' for validation job of front end production order.\n#{po.checklog.to_s}"
    assert_equal Job::FILE_SYSTEM_STATUS_FREE, vj.file_system_status, "Wrong file system status '#{vj.file_system_status_str}' for validation job of front end production order.\n#{po.checklog.to_s}"
    assert !vj.working_dir_exists?,  "The working dir '#{vj.working_dir}' of the validation job of front end production order should not exist as it is a placeholder and the run_server '#{vj.run_server}' has not been set yet.\n#{po.checklog.to_s}"

    v = vj.version
    ProductionOrder.schedule
    vj.reload
    cd.reload
    assert_equal Job::STATUS_PROCESSING, vj.status, "Wrong status '#{job.status_str}' for validation job of front end production order.\n#{po.checklog.to_s}"
    assert_equal Job::FILE_SYSTEM_STATUS_ON_SERVER, vj.file_system_status, "Wrong file system status '#{vj.file_system_status_str}' for validation job of front end production order.\n#{po.checklog.to_s}"
    assert vj.working_dir_exists?,  "The working dir '#{vj.working_dir}' of the validation job of front end production order should exist on run_server '#{vj.run_server}' but it doesn't.\n#{po.checklog.to_s}"
    assert_not_equal v, vj.version, "Version number of validation job of front end production order should have been increased.\n#{po.checklog.to_s}"
    assert_equal vj.version, vj.versions.size, "The version number of the validation job of front end production order should be equal to its number of versions.\n#{po.checklog.to_s}"
    assert_equal ConversionDatabase::STATUS_VALIDATING, cd.status, "Wrong status '#{cd.status_str}' for conversion database of front end production order.\n#{po.checklog.to_s}"

    ProductionOrder.schedule
    vj.reload
    cd.reload
    conv.reload
    assert_equal Job::STATUS_COMPLETE, vj.status, "Wrong status '#{job.status_str}' for validation job of front end production order.\n#{po.checklog.to_s}"
    assert_equal Job::FILE_SYSTEM_STATUS_FREE, vj.file_system_status, "Wrong file system status '#{vj.file_system_status_str}' for validation job of front end production order.\n#{po.checklog.to_s}"
    assert !vj.working_dir_exists?,  "The working dir '#{vj.working_dir}' of the validation job of front end production order should no longer exist on run_server '#{vj.run_server}' but it does.\n#{po.checklog.to_s}"
    assert_equal ConversionDatabase::STATUS_ACCEPTED, cd.status, "Wrong status '#{cd.status_str}' for conversion database of front end production order.\n#{po.checklog.to_s}"
    assert_equal ConversionDatabase::FILE_SYSTEM_STATUS_AVAILABLE, cd.file_system_status, "Wrong file system status '#{cd.file_system_status_str}' for conversion database of front end production order.\n#{po.checklog.to_s}"
    assert_equal Conversion::STATUS_CONVERSION, conv.status, "Wrong status '#{conv.status_str}' for conversion of front end production order.\n#{po.checklog.to_s}"

    po.execute
    assert !po.checklog.errors_or_warnings? , "Found errors or warnings in checklog of front end production order:\n#{po.checklog.to_s}"

    po.reload
    pol.reload
    conv.reload
    assert_equal Conversion::STATUS_FINISHED, conv.status, "Wrong status '#{conv.status_str}' for conversion of front end production order.\n#{po.checklog.to_s}"
    assert_equal ProductionOrderline::BP_NAME_COMPLETE, pol.bp_name, "Wrong business process for production orderline of front end production order.\n#{po.checklog.to_s}"
    assert_equal ProductionOrderline::BP_STATUS_COMPLETE, pol.bp_status, "Wrong business process status '#{pol.bp_status_str}' for front end production order.\n#{po.checklog.to_s}"
    assert_equal ProductionOrder::BP_NAME_COMPLETE, po.bp_name, "Wrong business process for front end production order.\n#{po.checklog.to_s}"

    cd.reload
    assert File.exist?(cd.path_and_file), "Could not find '#{cd.path_and_file}' of conversion database of front end production order on the file system.\n#{po.checklog.to_s}"
    assert_equal ConversionDatabase::STATUS_ACCEPTED, cd.status, "Wrong status '#{cd.status_str}' for conversion database of front end production order.\n#{po.checklog.to_s}"
    assert_equal ConversionDatabase::FILE_SYSTEM_STATUS_AVAILABLE, cd.file_system_status, "Wrong file system status '#{cd.file_system_status_str}' for conversion database of front end production order.\n#{po.checklog.to_s}"
    cd.remove
    assert !File.exist?(cd.path_and_file), "Could still find '#{cd.path_and_file}' of conversion database of front end production order #{po.id} on the file system, conversion database not removed.\n#{po.checklog.to_s}"
    assert_equal ConversionDatabase::STATUS_ACCEPTED, cd.status, "Wrong status '#{cd.status_str}' for conversion database of front end production order.\n#{po.checklog.to_s}"
    assert_equal ConversionDatabase::FILE_SYSTEM_STATUS_REMOVED, cd.file_system_status, "Wrong file system status '#{cd.file_system_status_str}' for conversion database of front end production order.\n#{po.checklog.to_s}"

    u = User.last
    tool_id = Conversiontool.last.id
    bundle_id = CvtoolBundle.last.id

    po.reset(u, tool_id, bundle_id)
    assert_equal ProductionOrder::BP_NAME_COMPLETE, po.bp_name, "Wrong business process for front end production order.\n#{po.checklog.to_s}"
    assert_equal 1, po.production_orderlines.size, "Wrong number of production orderlines for front end production order.\n#{po.checklog.to_s}"
    pol = po.production_orderlines.first
    assert_equal ProductionOrderline::BP_NAME_COMPLETE, pol.bp_name, "Wrong business process for production orderline of front end production order.\n#{po.checklog.to_s}"
    assert_equal ProductionOrderline::BP_STATUS_COMPLETE, pol.bp_status, "Wrong business process status '#{pol.bp_status_str}' for front end production order.\n#{po.checklog.to_s}"
    assert !po.checklog.errors_or_warnings? ,"Found errors or warnings in checklog of front end production order:\n #{po.checklog.to_s}"
  end

  test 'backend workflow' do
    # STEP 1
    # Use force recreate to force recreation of all databases and check the frontend workflow
    project = Project.find_by(name: 'Production')
    ordertype = Ordertype.find_by(name: 'AUTO-NDS_CONV-DML')
    production_order = ProductionOrder.new(project_id: project.id, use_test_versions_yn: false, ordertype_id: ordertype.id, use_dakota_simulator_yn: true, create_frontend_order_yn: true, allow_production: true)
    force_create = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11,12,13,14,15,16,17]
    product_design = ProductDesign.find_by(name: 'GMN NTG55H MASTERING')
    product = Product.find_by(name: 'Garmin NTG55H EUR')
    conversiontool = Conversiontool.find_by(code: 'dev_latest')
    cvtool_bundle = CvtoolBundle.find_by(name: 'dev')
    results = product_design.create_production_order(product.volumeid, production_order, conversiontool.id, cvtool_bundle.id, true, force_create, use_test_versions_yn = false, nil, use_dakota_simulator_yn = true)
    fpo = results[0]
    po = results[1]
    po.production_orderlines.each{ |pol| pol.create_outbound_order }
    input_RDF = 0
    input_RDB = 1
    input_TMC = 2
    input_POI = 3
    input_dHive = 0
    input_NDS = 1

    # some sanity checks to confirm the production order has the expected contents
    assert_not_nil fpo, 'Frontend order not found'
    assert_equal 4, fpo.production_orderlines.count, 'Wrong number of production orderlines for frontend production order.'
    assert_equal 'RDF', fpo.production_orderlines[input_RDF].conversions[0].input_format, 'Wrong input type for conversion.'
    assert_equal 'RDB', fpo.production_orderlines[input_RDB].conversions[0].input_format, 'Wrong input type for conversion.'
    assert_equal 'TMC', fpo.production_orderlines[input_TMC].conversions[0].input_format, 'Wrong input type for conversion.'
    assert_equal 'POI', fpo.production_orderlines[input_POI].conversions[0].input_format, 'Wrong input type for conversion.'
    assert_not_nil po, 'Backend order not found'
    assert_equal 1, po.production_orderlines.count, 'Wrong number of production orderlines for backend production order.'
    assert_equal 2, po.production_orderlines[0].conversions.count,  'Wrong number of conversions for backend production order.'
    assert_equal 'dHive', po.production_orderlines[0].conversions[input_dHive].input_format, 'Wrong input type for conversion.'
    assert_equal 'NDS', po.production_orderlines[0].conversions[input_NDS].input_format, 'Wrong input type for conversion.'

    fpo.execute
    fpo.reload
    # there should be three jobs which should be queued
    assert_equal 'MONITOR CONVERSION', fpo.production_orderlines[input_RDF].bp_name, 'Wrong business process for RDF frontend production orderline.'
    assert_equal 'ALLOCATE DATA', fpo.production_orderlines[input_RDB].bp_name, 'Wrong business process for RDB frontend production orderline.'
    assert_equal 'MONITOR CONVERSION', fpo.production_orderlines[input_TMC].bp_name, 'Wrong business process for TMC frontend production orderline.'
    assert_equal 'MONITOR CONVERSION', fpo.production_orderlines[input_POI].bp_name, 'Wrong business process for POI frontend production orderline.'
    assert_equal ProductionOrderline::BP_NAME_CREATED, po.production_orderlines[0].bp_name, 'Wrong business process for backend production orderline.'

    assert_equal Conversion::STATUS_CONVERSION, fpo.production_orderlines[input_RDF].conversions[0].status, 'Wrong status for RDF frontend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, fpo.production_orderlines[input_RDB].conversions[0].status, 'Wrong status for RDB frontend conversion.'
    assert_equal Conversion::STATUS_CONVERSION, fpo.production_orderlines[input_TMC].conversions[0].status, 'Wrong status for TMC frontend conversion.'
    assert_equal Conversion::STATUS_CONVERSION, fpo.production_orderlines[input_POI].conversions[0].status, 'Wrong status for POI frontend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, po.production_orderlines[0].conversions[input_dHive].status, 'Wrong status for dHive backend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, po.production_orderlines[0].conversions[input_NDS].status, 'Wrong status for NDS backend conversion.'

    assert_equal 1, fpo.production_orderlines[input_RDF].conversion_jobs.count, 'Wrong number of conversion jobs for RDF frontend production orderline.'
    assert_empty fpo.production_orderlines[input_RDB].conversion_jobs, 'Wrong number of conversion jobs for RDB frontend production orderline.'
    assert_equal 1, fpo.production_orderlines[input_TMC].conversion_jobs.count, 'Wrong number of conversion jobs for TMC frontend production orderline.'
    assert_equal 1, fpo.production_orderlines[input_POI].conversion_jobs.count, 'Wrong number of conversion jobs for POI frontend production orderline.'
    assert_empty po.production_orderlines[0].conversion_jobs, 'Wrong number of conversion jobs for backend production orderline.'
    assert_equal Job::STATUS_QUEUED, fpo.production_orderlines[input_RDF].conversion_jobs[0].status, 'Wrong status for RDF frontend conversion job.'
    assert_equal Job::STATUS_QUEUED, fpo.production_orderlines[input_TMC].conversion_jobs[0].status, 'Wrong status for TMC frontend conversion job.'
    assert_equal Job::STATUS_QUEUED, fpo.production_orderlines[input_POI].conversion_jobs[0].status, 'Wrong status for POI frontend conversion job.'

    ProductionOrder.schedule
    fpo.reload
    # the three jobs should be processing
    assert_equal Job::STATUS_PROCESSING, fpo.production_orderlines[input_RDF].conversion_jobs[0].status, 'Wrong status for RDF frontend conversion job.'
    assert_equal Job::STATUS_PROCESSING, fpo.production_orderlines[input_TMC].conversion_jobs[0].status, 'Wrong status for TMC frontend conversion job.'
    assert_equal Job::STATUS_PROCESSING, fpo.production_orderlines[input_POI].conversion_jobs[0].status, 'Wrong status for POI frontend conversion job.'

    ProductionOrder.schedule
    fpo.reload
    # the three jobs should be complete
    assert_equal Job::STATUS_COMPLETE, fpo.production_orderlines[input_RDF].conversion_jobs[0].status, 'Wrong status for RDF frontend conversion job.'
    assert_equal Job::STATUS_COMPLETE, fpo.production_orderlines[input_TMC].conversion_jobs[0].status, 'Wrong status for TMC frontend conversion job.'
    assert_equal Job::STATUS_COMPLETE, fpo.production_orderlines[input_POI].conversion_jobs[0].status, 'Wrong status for POI frontend conversion job.'
    assert_equal 1, fpo.production_orderlines[input_RDF].conversion_databases.count, 'Wrong number of conversion databases for RDF frontend production orderline.'
    assert_empty fpo.production_orderlines[input_RDB].conversion_databases, 'Wrong number of conversion databases for RDB frontend production orderline.'
    assert_equal 1, fpo.production_orderlines[input_TMC].conversion_databases.count, 'Wrong number of conversion databases for TMC frontend production orderline.'
    assert_equal 1, fpo.production_orderlines[input_POI].conversion_databases.count, 'Wrong number of conversion databases for POI frontend production orderline.'
    assert_equal ConversionDatabase::STATUS_NOT_VALIDATED, fpo.production_orderlines[input_RDF].conversion_databases[0].status, 'Wrong status for RDF frontend conversion database.'
    assert_equal ConversionDatabase::STATUS_VALIDATING, fpo.production_orderlines[input_TMC].conversion_databases[0].status, 'Wrong status for TMC frontend conversion database.'
    assert_equal ConversionDatabase::STATUS_VALIDATING, fpo.production_orderlines[input_POI].conversion_databases[0].status, 'Wrong status for POI frontend conversion database.'
    assert_empty fpo.production_orderlines[input_RDF].conversion_databases[0].validation_jobs, 'Wrong number of validation jobs for RDF frontend conversion database.'
    assert_equal 1, fpo.production_orderlines[input_TMC].conversion_databases[0].validation_jobs.count, 'Wrong number of validation jobs for TMC frontend conversion database.'
    assert_equal 1, fpo.production_orderlines[input_POI].conversion_databases[0].validation_jobs.count, 'Wrong number of validation jobs for POI frontend conversion database.'
    assert_equal Job::STATUS_QUEUED, fpo.production_orderlines[input_TMC].conversion_databases[0].validation_jobs[0].status, 'Wrong status for TMC frontend validation job.'
    assert_equal Job::STATUS_QUEUED, fpo.production_orderlines[input_POI].conversion_databases[0].validation_jobs[0].status, 'Wrong status for POI frontend validation job.'

    # check whether job versions are working
    assert_operator fpo.production_orderlines[input_RDF].conversion_jobs[0].version, '>', 1, 'Version number not increased.'
    assert_equal fpo.production_orderlines[input_RDF].conversion_jobs[0].versions.count, fpo.production_orderlines[input_RDF].conversion_jobs[0].version, 'Version number not equal to number of versions.'

    # check whether orders and conversions are still in the expected states
    assert_equal Conversion::STATUS_CONVERSION, fpo.production_orderlines[input_RDF].conversions[0].status, 'Wrong status for RDF frontend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, fpo.production_orderlines[input_RDB].conversions[0].status, 'Wrong status for RDB frontend conversion.'
    assert_equal Conversion::STATUS_CONVERSION, fpo.production_orderlines[input_TMC].conversions[0].status, 'Wrong status for TMC frontend conversion.'
    assert_equal Conversion::STATUS_CONVERSION, fpo.production_orderlines[input_POI].conversions[0].status, 'Wrong status for POI frontend conversion.'
    assert_equal 'MONITOR CONVERSION', fpo.production_orderlines[input_RDF].bp_name, 'Wrong business process for RDF frontend production orderline.'
    assert_equal 'ALLOCATE DATA', fpo.production_orderlines[input_RDB].bp_name, 'Wrong business process for RDB frontend production orderline.'
    assert_equal 'MONITOR CONVERSION', fpo.production_orderlines[input_TMC].bp_name, 'Wrong business process for TMC frontend production orderline.'
    assert_equal 'MONITOR CONVERSION', fpo.production_orderlines[input_POI].bp_name, 'Wrong business process for POI frontend production orderline.'

    fpo.execute
    fpo.reload

    assert_equal ProductionOrderline::BP_NAME_COMPLETE, fpo.production_orderlines[input_RDF].bp_name, 'Wrong business process for RDF frontend production orderline.'
    assert_equal 'MONITOR CONVERSION', fpo.production_orderlines[input_RDB].bp_name, 'Wrong business process for RDB frontend production orderline.'
    assert_equal 'MONITOR CONVERSION', fpo.production_orderlines[input_TMC].bp_name, 'Wrong business process for TMC frontend production orderline.'
    assert_equal 'MONITOR CONVERSION', fpo.production_orderlines[input_POI].bp_name, 'Wrong business process for POI frontend production orderline.'
    assert_equal Conversion::STATUS_FINISHED, fpo.production_orderlines[input_RDF].conversions[0].status, 'Wrong status for RDF frontend conversion.'
    assert_equal Conversion::STATUS_CONVERSION, fpo.production_orderlines[input_RDB].conversions[0].status, 'Wrong status for RDB frontend conversion.'
    assert_equal Conversion::STATUS_CONVERSION, fpo.production_orderlines[input_TMC].conversions[0].status, 'Wrong status for TMC frontend conversion.'
    assert_equal Conversion::STATUS_CONVERSION, fpo.production_orderlines[input_POI].conversions[0].status, 'Wrong status for POI frontend conversion.'
    assert_equal 1, fpo.production_orderlines[input_RDB].conversion_jobs.count, 'Wrong number of conversion jobs for RDB frontend production orderline.'
    assert_equal Job::STATUS_QUEUED, fpo.production_orderlines[input_RDB].conversion_jobs[0].status, 'Wrong status for RDB frontend conversion job.'
    assert_equal ConversionDatabase::STATUS_VALIDATING, fpo.production_orderlines[input_TMC].conversion_databases[0].status, 'Wrong status for TMC frontend conversion database.'
    assert_equal ConversionDatabase::STATUS_VALIDATING, fpo.production_orderlines[input_POI].conversion_databases[0].status, 'Wrong status for POI frontend conversion database.'
    assert_equal Job::STATUS_QUEUED, fpo.production_orderlines[input_TMC].conversion_databases[0].validation_jobs[0].status, 'Wrong status for TMC frontend validation job.'
    assert_equal Job::STATUS_QUEUED, fpo.production_orderlines[input_POI].conversion_databases[0].validation_jobs[0].status, 'Wrong status for POI frontend validation job.'

    fpo.execute
    fpo.reload
    # should do nothing
    assert_equal ProductionOrderline::BP_NAME_COMPLETE, fpo.production_orderlines[input_RDF].bp_name, 'Wrong business process for RDF frontend production orderline.'
    assert_equal 'MONITOR CONVERSION', fpo.production_orderlines[input_RDB].bp_name, 'Wrong business process for RDB frontend production orderline.'
    assert_equal 'MONITOR CONVERSION', fpo.production_orderlines[input_TMC].bp_name, 'Wrong business process for TMC frontend production orderline.'
    assert_equal 'MONITOR CONVERSION', fpo.production_orderlines[input_POI].bp_name, 'Wrong business process for POI frontend production orderline.'
    assert_equal Conversion::STATUS_FINISHED, fpo.production_orderlines[input_RDF].conversions[0].status, 'Wrong status for RDF frontend conversion.'
    assert_equal Conversion::STATUS_CONVERSION, fpo.production_orderlines[input_RDB].conversions[0].status, 'Wrong status for RDB frontend conversion.'
    assert_equal Conversion::STATUS_CONVERSION, fpo.production_orderlines[input_TMC].conversions[0].status, 'Wrong status for TMC frontend conversion.'
    assert_equal Conversion::STATUS_CONVERSION, fpo.production_orderlines[input_POI].conversions[0].status, 'Wrong status for POI frontend conversion.'
    assert_equal Job::STATUS_QUEUED, fpo.production_orderlines[input_RDB].conversion_jobs[0].status, 'Wrong status for RDB frontend conversion job.'
    assert_equal ConversionDatabase::STATUS_VALIDATING, fpo.production_orderlines[input_TMC].conversion_databases[0].status, 'Wrong status for TMC frontend conversion database.'
    assert_equal ConversionDatabase::STATUS_VALIDATING, fpo.production_orderlines[input_POI].conversion_databases[0].status, 'Wrong status for POI frontend conversion database.'
    assert_equal Job::STATUS_QUEUED, fpo.production_orderlines[input_TMC].conversion_databases[0].validation_jobs[0].status, 'Wrong status for TMC frontend validation job.'
    assert_equal Job::STATUS_QUEUED, fpo.production_orderlines[input_POI].conversion_databases[0].validation_jobs[0].status, 'Wrong status for POI frontend validation job.'

    ProductionOrder.schedule
    fpo.reload
    assert_equal Job::STATUS_PROCESSING, fpo.production_orderlines[input_RDB].conversion_jobs[0].status, 'Wrong status for RDB frontend conversion job.'
    assert_equal Job::STATUS_PROCESSING, fpo.production_orderlines[input_TMC].conversion_databases[0].validation_jobs[0].status, 'Wrong status for TMC frontend validation job.'
    assert_equal Job::STATUS_PROCESSING, fpo.production_orderlines[input_POI].conversion_databases[0].validation_jobs[0].status, 'Wrong status for POI frontend validation job.'

    ProductionOrder.schedule
    fpo.reload
    assert_equal Job::STATUS_COMPLETE, fpo.production_orderlines[input_RDB].conversion_jobs[0].status, 'Wrong status for RDB frontend conversion job.'
    assert_equal Job::STATUS_COMPLETE, fpo.production_orderlines[input_TMC].conversion_databases[0].validation_jobs[0].status, 'Wrong status for TMC frontend validation job.'
    assert_equal Job::STATUS_COMPLETE, fpo.production_orderlines[input_POI].conversion_databases[0].validation_jobs[0].status, 'Wrong status for POI frontend validation job.'
    assert_equal ConversionDatabase::STATUS_VALIDATING, fpo.production_orderlines[input_RDB].conversion_databases[0].status, 'Wrong status for RDB frontend conversion database.'
    assert_equal ConversionDatabase::STATUS_ACCEPTED, fpo.production_orderlines[input_TMC].conversion_databases[0].status, 'Wrong status for TMC frontend conversion database.'
    assert_equal ConversionDatabase::STATUS_ACCEPTED, fpo.production_orderlines[input_POI].conversion_databases[0].status, 'Wrong status for POI frontend conversion database.'
    assert_equal 1, fpo.production_orderlines[input_RDB].conversion_databases[0].validation_jobs.count, 'Wrong number of validation jobs for RDB frontend validation job.'
    assert_equal Job::STATUS_QUEUED, fpo.production_orderlines[input_RDB].conversion_databases[0].validation_jobs[0].status, 'Wrong status for RDB frontend validation job.'

    assert_equal 'MONITOR CONVERSION', fpo.production_orderlines[input_RDB].bp_name, 'Wrong business process for RDB frontend production orderline.'
    assert_equal 'MONITOR CONVERSION', fpo.production_orderlines[input_TMC].bp_name, 'Wrong business process for TMC frontend production orderline.'
    assert_equal 'MONITOR CONVERSION', fpo.production_orderlines[input_POI].bp_name, 'Wrong business process for POI frontend production orderline.'

    fpo.execute
    fpo.reload
    assert_equal 'MONITOR CONVERSION', fpo.production_orderlines[input_RDB].bp_name, 'Wrong business process for RDB frontend production orderline.'
    assert_equal ProductionOrderline::BP_NAME_COMPLETE, fpo.production_orderlines[input_TMC].bp_name, 'Wrong business process for TMC frontend production orderline.'
    assert_equal ProductionOrderline::BP_NAME_COMPLETE, fpo.production_orderlines[input_POI].bp_name, 'Wrong business process for POI frontend production orderline.'

    fpo.execute
    fpo.reload
    # nothing to do
    assert_equal 'MONITOR CONVERSION', fpo.production_orderlines[input_RDB].bp_name, 'Wrong business process for RDB frontend production orderline.'
    assert_equal ProductionOrderline::BP_NAME_COMPLETE, fpo.production_orderlines[input_TMC].bp_name, 'Wrong business process for TMC frontend production orderline.'
    assert_equal ProductionOrderline::BP_NAME_COMPLETE, fpo.production_orderlines[input_POI].bp_name, 'Wrong business process for POI frontend production orderline.'

    ProductionOrder.schedule
    fpo.reload
    assert_equal ConversionDatabase::STATUS_VALIDATING, fpo.production_orderlines[input_RDB].conversion_databases[0].status, 'Wrong status for RDB frontend conversion database.'
    assert_equal Job::STATUS_PROCESSING, fpo.production_orderlines[input_RDB].conversion_databases[0].validation_jobs[0].status, 'Wrong status for RDB frontend validation job.'

    ProductionOrder.schedule
    fpo.reload
    assert_equal ConversionDatabase::STATUS_ACCEPTED, fpo.production_orderlines[input_RDB].conversion_databases[0].status, 'Wrong status for RDB frontend conversion database.'
    assert_equal Job::STATUS_COMPLETE, fpo.production_orderlines[input_RDB].conversion_databases[0].validation_jobs[0].status, 'Wrong status for RDB frontend validation job.'
    assert_equal 'PRODUCTION', fpo.bp_name, 'Wrong business process for frontend production order.'

    fpo.execute
    fpo.reload
    assert_equal ProductionOrderline::BP_NAME_COMPLETE, fpo.production_orderlines[input_RDB].bp_name, 'Wrong business process for RDB frontend production orderline.'
    assert_equal ProductionOrder::BP_NAME_COMPLETE, fpo.bp_name, 'Wrong business process for frontend production order.'

    # STEP 2
    # Test the backend order with a force recreate, using the dHives of STEP 1
    production_order = ProductionOrder.new(project_id: project.id, use_test_versions_yn: false, ordertype_id: ordertype.id, use_dakota_simulator_yn: true, create_frontend_order_yn: true, allow_production: true)
    force_create = [4, 5]
    results = product_design.create_production_order(product.volumeid, production_order, conversiontool.id, cvtool_bundle.id, true, force_create, use_test_versions_yn = false, nil, use_dakota_simulator_yn = true)
    fpo2 = results[0]
    po2 = results[1]
    po2.production_orderlines.each{ |pol| pol.create_outbound_order }
    po2.save!
    po2.transition
    po2.reload

    po2.production_orderlines.each do |pol|
      assert_not_nil pol.product_location, 'Expecting a product location placeholder for backend production orderline.'
    end

    assert_equal 'PRODUCTION', fpo2.bp_name, 'Wrong business process for frontend production order.'
    assert_equal 'PRODUCTION', po2.bp_name, 'Wrong business process for backend production order.'
    assert_equal 4, fpo2.production_orderlines.count, 'Wrong number of production orderlines for frontend production order.'
    assert_equal 1, po2.production_orderlines.count, 'Wrong number of production orderlines for backend production order.'
    assert_equal 'SELECT DATARELEASE', fpo2.production_orderlines[input_RDF].bp_name, 'Wrong business process for RDF frontend production orderline.'
    assert_equal 'SELECT DATARELEASE', fpo2.production_orderlines[input_RDB].bp_name, 'Wrong business process for RDB frontend production orderline.'
    assert_equal 'SELECT DATARELEASE', fpo2.production_orderlines[input_TMC].bp_name, 'Wrong business process for TMC frontend production orderline.'
    assert_equal 'SELECT DATARELEASE', fpo2.production_orderlines[input_POI].bp_name, 'Wrong business process for POI frontend production orderline.'
    assert_equal 'AUTO CONVERSION', po2.production_orderlines[0].bp_name, 'Wrong business process for backend production orderline.'
    assert_equal 1, fpo2.production_orderlines[input_RDF].conversions.count, 'Wrong number of conversions for RDF frontend production orderline.'
    assert_equal 1, fpo2.production_orderlines[input_RDB].conversions.count, 'Wrong number of conversions for RDB frontend production orderline.'
    assert_equal 1, fpo2.production_orderlines[input_TMC].conversions.count, 'Wrong number of conversions for TMC frontend production orderline.'
    assert_equal 1, fpo2.production_orderlines[input_POI].conversions.count, 'Wrong number of conversions for POI frontend production orderline.'
    assert_equal 2, po2.production_orderlines[0].conversions.count, 'Wrong number of conversions for backend production orderline.'
    assert_equal Conversion::STATUS_REQUESTED, fpo2.production_orderlines[input_RDF].conversions[0].status, 'Wrong status for RDF frontend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, fpo2.production_orderlines[input_RDB].conversions[0].status, 'Wrong status for RDB frontend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, fpo2.production_orderlines[input_TMC].conversions[0].status, 'Wrong status for TMC frontend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, fpo2.production_orderlines[input_POI].conversions[0].status, 'Wrong status for POI frontend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, po2.production_orderlines[0].conversions[input_dHive].status, 'Wrong status for dHive backend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, po2.production_orderlines[0].conversions[input_NDS].status, 'Wrong status for NDS backend conversion.'

    fpo2.execute
    po2.execute
    fpo2.reload
    po2.reload

    assert_equal ProductionOrder::BP_NAME_COMPLETE, fpo2.bp_name, 'Wrong business process for frontend production order.'
    assert_equal 'PRODUCTION', po2.bp_name, 'Wrong business process for backend production order.'
    assert_equal ProductionOrderline::BP_NAME_COMPLETE, fpo2.production_orderlines[input_RDF].bp_name, 'Wrong business process for RDF frontend production orderline.'
    assert_equal ProductionOrderline::BP_NAME_COMPLETE, fpo2.production_orderlines[input_RDB].bp_name, 'Wrong business process for RDB frontend production orderline.'
    assert_equal ProductionOrderline::BP_NAME_COMPLETE, fpo2.production_orderlines[input_TMC].bp_name, 'Wrong business process for TMC frontend production orderline.'
    assert_equal ProductionOrderline::BP_NAME_COMPLETE, fpo2.production_orderlines[input_POI].bp_name, 'Wrong business process for POI frontend production orderline.'
    assert_equal 'AUTO CONVERSION', po2.production_orderlines[0].bp_name, 'Wrong business process for backend production orderline.'
    assert_empty fpo2.production_orderlines[input_RDF].conversions, 'Wrong number of conversions for RDF frontend production orderline.'
    assert_empty fpo2.production_orderlines[input_RDB].conversions, 'Wrong number of conversions for RDB frontend production orderline.'
    assert_empty fpo2.production_orderlines[input_TMC].conversions, 'Wrong number of conversions for TMC frontend production orderline.'
    assert_empty fpo2.production_orderlines[input_POI].conversions, 'Wrong number of conversions for POI frontend production orderline.'
    assert_equal 2, po2.production_orderlines[0].conversions.count, 'Wrong number of conversions for backend production orderline.'
    assert_equal Conversion::STATUS_CONVERSION, po2.production_orderlines[0].conversions[input_dHive].status, 'Wrong status for dHive backend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, po2.production_orderlines[0].conversions[input_NDS].status, 'Wrong status for NDS backend conversion.'
    assert_equal 1, po2.production_orderlines[0].conversions[input_dHive].conversion_jobs.count, 'Wrong number of conversion jobs for dHive backend conversion.'
    assert_empty po2.production_orderlines[0].conversions[input_NDS].conversion_jobs, 'Wrong number of conversion jobs for NDS backend conversion.'
    assert_equal Job::STATUS_QUEUED, po2.production_orderlines[0].conversions[input_dHive].conversion_jobs[0].status, 'Wrong status for dHive backend conversion job.'

    po2.execute
    po2.reload
    # should do nothing
    assert_equal 'PRODUCTION', po2.bp_name, 'Wrong business process for backend production order.'
    assert_equal 'AUTO CONVERSION', po2.production_orderlines[0].bp_name, 'Wrong business process for backend production orderline.'
    assert_equal 2, po2.production_orderlines[0].conversions.count, 'Wrong number of conversions for backend production orderline.'
    assert_equal Conversion::STATUS_CONVERSION, po2.production_orderlines[0].conversions[input_dHive].status, 'Wrong status for dHive backend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, po2.production_orderlines[0].conversions[input_NDS].status, 'Wrong status for NDS backend conversion.'
    assert_equal Job::STATUS_QUEUED, po2.production_orderlines[0].conversions[input_dHive].conversion_jobs[0].status, 'Wrong status for dHive backend conversion job.'

    ProductionOrder.schedule
    po2.reload
    assert_equal Conversion::STATUS_CONVERSION, po2.production_orderlines[0].conversions[input_dHive].status, 'Wrong status for dHive backend conversion.'
    assert_equal Job::STATUS_PROCESSING, po2.production_orderlines[0].conversions[input_dHive].conversion_jobs[0].status, 'Wrong status for dHive backend conversion job.'

    po2.execute
    po2.reload
    # should still do nothing
    assert_equal 'PRODUCTION', po2.bp_name, 'Wrong business process for backend production order.'
    assert_equal 'AUTO CONVERSION', po2.production_orderlines[0].bp_name, 'Wrong business process for backend production orderline.'
    assert_equal 2, po2.production_orderlines[0].conversions.count, 'Wrong number of conversions for backend production orderline.'
    assert_equal Conversion::STATUS_CONVERSION, po2.production_orderlines[0].conversions[input_dHive].status, 'Wrong status for dHive backend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, po2.production_orderlines[0].conversions[input_NDS].status, 'Wrong status for NDS backend conversion.'

    ProductionOrder.schedule
    po2.reload
    assert_equal Conversion::STATUS_CONVERSION, po2.production_orderlines[0].conversions[input_dHive].status, 'Wrong status for dHive backend conversion.'
    assert_equal Job::STATUS_COMPLETE, po2.production_orderlines[0].conversions[input_dHive].conversion_jobs[0].status, 'Wrong status for dHive backend conversion job.'
    assert_equal 1, po2.production_orderlines[0].conversions[input_dHive].conversion_databases.count, 'Wrong number of conversion databases for dHive backend conversion.'
    assert_empty po2.production_orderlines[0].conversions[input_NDS].conversion_databases, 'Wrong number of conversion databases for NDS backend conversion.'
    assert_equal ConversionDatabase::STATUS_NOT_VALIDATED, po2.production_orderlines[0].conversions[input_dHive].conversion_databases[0].status, 'Wrong status for dHive backend conversion database.'

    po2.execute
    po2.reload
    assert_equal 'PRODUCTION', po2.bp_name, 'Wrong business process for backend production order.'
    assert_equal 'AUTO CONVERSION', po2.production_orderlines[0].bp_name, 'Wrong business process for backend production orderline.'
    assert_equal Conversion::STATUS_FINISHED, po2.production_orderlines[0].conversions[input_dHive].status, 'Wrong status for dHive backend conversion.'
    assert_equal Conversion::STATUS_CONVERSION, po2.production_orderlines[0].conversions[input_NDS].status, 'Wrong status for NDS backend conversion.'
    assert_equal 1, po2.production_orderlines[0].conversions[input_NDS].conversion_jobs.count, 'Wrong number of conversion jobs for NDS backend conversion.'
    assert_equal Job::STATUS_QUEUED, po2.production_orderlines[0].conversions[input_NDS].conversion_jobs[0].status, 'Wrong status for NDS backend conversion job.'

    ProductionOrder.schedule
    po2.reload
    assert_equal Conversion::STATUS_CONVERSION, po2.production_orderlines[0].conversions[input_NDS].status, 'Wrong status for NDS backend conversion.'
    assert_equal Job::STATUS_PROCESSING, po2.production_orderlines[0].conversions[input_NDS].conversion_jobs[0].status, 'Wrong status for NDS backend conversion job.'

    ProductionOrder.schedule
    po2.reload
    assert_equal 'PRODUCTION', po2.bp_name, 'Wrong business process for backend production order.'
    assert_equal 'AUTO CONVERSION', po2.production_orderlines[0].bp_name, 'Wrong business process for backend production orderline.'
    assert_equal Conversion::STATUS_CONVERSION, po2.production_orderlines[0].conversions[input_NDS].status, 'Wrong status for NDS backend conversion.'
    assert_equal Job::STATUS_COMPLETE, po2.production_orderlines[0].conversions[input_NDS].conversion_jobs[0].status, 'Wrong status for NDS backend conversion job.'
    assert_equal 1, po2.production_orderlines[0].conversions[input_NDS].conversion_databases.count, 'Wrong number of conversion databases for NDS backend conversion.'
    assert_equal ConversionDatabase::STATUS_NOT_VALIDATED, po2.production_orderlines[0].conversions[input_NDS].conversion_databases[0].status, 'Wrong status for NDS backend conversion database.'
    assert_equal Job.last.id, po2.production_orderlines[0].conversions[input_NDS].conversion_jobs[0].id, 'Expecting backend NDS conversion job to be the last created job.'

    po2.execute
    po2.reload
    assert_equal 'PRODUCTION', po2.bp_name, 'Wrong business process for backend production order.'
    assert_equal 1, po2.production_orderlines.count, 'Wrong number of backend production orderlines.'
    assert_equal 'MASTERING', po2.production_orderlines[0].bp_name, 'Wrong business process for backend production orderline.'
    assert_equal 2, po2.production_orderlines[0].conversions.count, 'Wrong number of conversions for backend production orderline.'
    assert_equal Conversion::STATUS_FINISHED, po2.production_orderlines[0].conversions[input_dHive].status, 'Wrong status for dHive backend conversion.'
    assert_equal Conversion::STATUS_FINISHED, po2.production_orderlines[0].conversions[input_NDS].status, 'Wrong status for NDS backend conversion.'

    po2.execute
    po2.reload
    assert_equal 'PRODUCTION', po2.bp_name, 'Wrong business process for backend production order.'
    assert_equal 1, po2.production_orderlines.count, 'Wrong number of backend production orderlines.'
    assert_equal 'ASSIGN TEST', po2.production_orderlines[0].bp_name, 'Wrong business process for backend production orderline.'

    # TODO logic in controllers needs to move in model for next workflow steps - for now just transitioning orderline to RELEASE
    po2.production_orderlines[0].set_bp_status(ProductionOrderline::BP_STATUS_COMPLETE)
    po2.production_orderlines[0].transition
    po2.reload
    assert_equal 'TEST', po2.production_orderlines[0].bp_name, 'Wrong business process for backend production orderline.'

    po2.production_orderlines[0].set_bp_status(ProductionOrderline::BP_STATUS_COMPLETE)
    po2.production_orderlines[0].transition
    po2.reload
    # note that the space after the EVALUATE is intentional, sight
    assert_equal 'EVALUATE ', po2.production_orderlines[0].bp_name, 'Wrong business process for backend production orderline.'

    po2.production_orderlines[0].set_bp_status(ProductionOrderline::BP_STATUS_COMPLETE)
    po2.production_orderlines[0].transition
    po2.reload
    assert_nil po2.production_orderlines[0].product_release_note, 'Backend production orderline should not have a product release note attached yet.'

    po2.execute
    po2.reload
    assert_not_nil po2.production_orderlines[0].product_release_note, 'Backend production orderline should have a product release note attached now.'
    product_release_note = po2.production_orderlines[0].product_release_note
    assert_equal ProductReleaseNote::STATUS_DRAFT, product_release_note.status, 'Wrong status for product release note.'
    product_release_note.status = ProductReleaseNote::STATUS_REVIEW
    product_release_note.save!
    product_release_note.status = ProductReleaseNote::STATUS_APPROVED
    product_release_note.save!
    po2.reload
    assert_equal ProductReleaseNote::STATUS_APPROVED, po2.production_orderlines[0].product_release_note.status, 'Wrong status for product release note of backend production orderline.'

    po2.execute
    po2.reload
    assert_equal 'REGISTER PRODUCT', po2.production_orderlines[0].bp_name, 'Wrong business process for backend production orderline.'
    assert_equal ProductReleaseNote::STATUS_RELEASED, po2.production_orderlines[0].product_release_note.status, 'Wrong status for product release note of backend production orderline.'

    assert_nil po2.production_orderlines[0].product_location.conversion, 'Product location should not have a conversion attached yet.'
    assert_equal OutboundOrder::STATUS_PLACEHOLDER, po2.production_orderlines[0].outbound_orderlines.first.outbound_order.status, 'Wrong status for outbound orderline.'

    po2.execute
    po2.reload
    assert_equal ProductionOrder::BP_NAME_COMPLETE, po2.bp_name, 'Wrong business process for backend production order.'
    assert_equal ProductionOrderline::BP_NAME_COMPLETE, po2.production_orderlines[0].bp_name, 'Wrong business process for backend production orderline.'

    # check product_location
    assert_not_nil po2.production_orderlines[0].product_location.conversion, 'Product location should have a conversion attached to it.'
    assert_equal po2.production_orderlines[0].conversions[input_NDS].id, po2.production_orderlines[0].product_location.conversion.id, 'Product location should be associated with the NDS backend conversion.'
    conversiontool_str = po2.production_orderlines[0].conversions[input_NDS].conversiontool.code.to_s + '/' + po2.production_orderlines[0].conversions[input_NDS].cvtool_bundle.name.to_s
    assert_equal conversiontool_str, po2.production_orderlines[0].product_location.conversiontool, 'Wrong conversiontool for product location.'
    assert_operator po2.production_orderlines[0].product_location.storage_path, 'start_with?', po2.production_orderlines[0].get_product_location_dir, 'Wrong product location directory for backend production orderline.'

    # check outbound order
    oo = po2.production_orderlines[0].outbound_orderlines.first.outbound_order rescue nil
    assert_not_nil oo, 'Outbound order should exist.'
    assert_equal 'Automatic', oo.courier, 'Wrong courier for outbound order.'
    assert_equal OutboundOrder::STATUS_PACKING, oo.status, 'Wrong status for outbound order.'
    assert_equal 2, oo.outbound_orderlines.count, 'Wrong number of outbound orderlines.'
    assert_equal po2.production_orderlines[0].product_location.id, oo.outbound_orderlines[0].product_location_id, 'Outbound orderline does not point to the correct product location.'
    assert_nil oo.outbound_orderlines[1].product_location_id, 'The second outbound orderline should not point to a product location.'
    assert_equal 'release notes', oo.outbound_orderlines[1].product_name, 'Wrong product name for second outbound orderline.'
    assert_nil oo.outbound_orderlines[0].parent_linenr, 'First outbound orderline should not have a parent line.'
    assert_equal oo.outbound_orderlines[0].id, oo.outbound_orderlines[1].parent_linenr, 'Second outbound orderline should be a child of the first one.'

    assert_equal 1, oo.packings.count, 'Wrong number of packings for outbound order.'
    assert_not_nil oo.packings[0].packing_specification, 'Packing should have a specification.'
    assert_equal 'Test Pack', oo.packings[0].packing_specification.name, 'Wrong packing specification for packing.'
    assert_equal Packing::STATUS_PROCESSING, oo.packings[0].status, 'Wrong status for packing.'
    assert_equal 2, oo.packings[0].packing_lines.count, 'Wrong number of packing lines for packing.'
    assert_equal oo.outbound_orderlines[0].id, oo.packings[0].packing_lines[0].outbound_orderline_id, 'First packing line points to the wrong outbound orderline.'
    assert_equal oo.outbound_orderlines[1].id, oo.packings[0].packing_lines[1].outbound_orderline_id, 'Second packing line points to the wrong outbound orderline.'
    assert_equal 1, oo.packings[0].packing_jobs.count, 'Wrong number of packing jobs.'
    assert_equal Job::STATUS_QUEUED, oo.packings[0].packing_jobs[0].status, 'Wrong status for packing job.'

    ProductionOrder.schedule
    oo.reload
    assert_equal OutboundOrder::STATUS_PACKING, oo.status, 'Wrong status for outbound order.'
    assert_equal Job::STATUS_PROCESSING, oo.packings[0].packing_jobs[0].status, 'Wrong status for packing job.'
    assert_empty oo.packings[0].shipping_jobs, 'Wrong number of shipping jobs.'

    sleep 5 # needed so the packing job can finish (increase if necessary)
    ProductionOrder.schedule
    oo.reload
    assert_equal Job::STATUS_COMPLETE, oo.packings[0].packing_jobs[0].status, 'Wrong status for packing job.'
    assert_equal Packing::STATUS_SHIPPING, oo.packings[0].status, 'Wrong status for packing.'
    assert_equal OutboundOrder::STATUS_SHIPPING, oo.status, 'Wrong status for outbound order.'
    assert_equal 1, oo.packings[0].shipping_jobs.count, 'Wrong number of shipping jobs.'
    assert_equal Job::STATUS_QUEUED, oo.packings[0].shipping_jobs[0].status, 'Wrong status for shipping job.'

    ProductionOrder.schedule
    oo.reload
    assert_equal Job::STATUS_PROCESSING, oo.packings[0].shipping_jobs[0].status, 'Wrong status for shipping job.'

#### the Shipping ftp can currently not be tested as we don't have a test account on our sftp server, and using localhost
#### as a replacement results in all kinds of authentication problems which are not worth effort.
#### TODO: enable this test once a test sftp account has been set up
#    sleep 5 # needed so the shipping job can finish (increase if necessary)
#    ProductionOrder.schedule
#    oo.reload
#    assert_equal Job::STATUS_COMPLETE, oo.packings[0].shipping_jobs[0].status, 'Wrong status for shipping job.'
#    assert_equal Packing::STATUS_SHIPPED, oo.packings[0].status, 'Wrong status for packing.'
#    assert equal OutboundOrder::STATUS_SHIPPED, oo.status, 'Wrong status for outbound order.'

#    ftp_file = File.join(ConfigParameter.get('shared_temp_dir'), 'outgoing', oo.packings[0].filename)
#    assert_operator File, 'exists?', ftp_file, 'File not found on ftp location.'

    # step 3
    # Test the backend order with a partial force recreate, recreating the RDB dHive of STEP 1
    production_order = ProductionOrder.new(project_id: project.id, use_test_versions_yn: false, ordertype_id: ordertype.id, use_dakota_simulator_yn: true, create_frontend_order_yn: true, allow_production: true)
    force_create = [1, 4, 5]
    results = product_design.create_production_order(product.volumeid, production_order, conversiontool.id, cvtool_bundle.id, true, force_create, use_test_versions_yn = false, nil, use_dakota_simulator_yn = true)
    fpo3 = results[0]
    po3 = results[1]

    assert_not_nil fpo3, 'Frontend order not found'
    assert_not_nil po3, 'Backend order not found'
    assert_equal 'PRODUCTION', fpo3.bp_name, 'Wrong business process for frontend production order.'
    assert_equal 'AUTO CONVERSION', po3.bp_name, 'Wrong business process for backend production order.'
    assert_equal 4, fpo3.production_orderlines.count, 'Wrong number of production orderlines for frontend production order.'
    assert_equal 1, po3.production_orderlines.count, 'Wrong number of production orderlines for backend production order.'
    assert_equal 'SELECT DATARELEASE', fpo3.production_orderlines[input_RDF].bp_name, 'Wrong business process for RDF frontend production orderline.'
    assert_equal 'SELECT DATARELEASE', fpo3.production_orderlines[input_RDB].bp_name, 'Wrong business process for RDB frontend production orderline.'
    assert_equal 'SELECT DATARELEASE', fpo3.production_orderlines[input_TMC].bp_name, 'Wrong business process for TMC frontend production orderline.'
    assert_equal 'SELECT DATARELEASE', fpo3.production_orderlines[input_POI].bp_name, 'Wrong business process for POI frontend production orderline.'
    assert_equal ProductionOrderline::BP_NAME_CREATED, po3.production_orderlines[0].bp_name, 'Wrong business process for backend production orderline.'
    assert_equal 1, fpo3.production_orderlines[input_RDF].conversions.count, 'Wrong number of conversions for RDF frontend production orderline.'
    assert_equal 1, fpo3.production_orderlines[input_RDB].conversions.count, 'Wrong number of conversions for RDB frontend production orderline.'
    assert_equal 1, fpo3.production_orderlines[input_TMC].conversions.count, 'Wrong number of conversions for TMC frontend production orderline.'
    assert_equal 1, fpo3.production_orderlines[input_POI].conversions.count, 'Wrong number of conversions for POI frontend production orderline.'
    assert_equal 2, po3.production_orderlines[0].conversions.count, 'Wrong number of conversions for backend production orderline.'
    assert_equal Conversion::STATUS_REQUESTED, fpo3.production_orderlines[input_RDF].conversions[0].status, 'Wrong status for RDF frontend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, fpo3.production_orderlines[input_RDB].conversions[0].status, 'Wrong status for RDB frontend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, fpo3.production_orderlines[input_TMC].conversions[0].status, 'Wrong status for TMC frontend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, fpo3.production_orderlines[input_POI].conversions[0].status, 'Wrong status for POI frontend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, po3.production_orderlines[0].conversions[input_dHive].status, 'Wrong status for dHive backend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, po3.production_orderlines[0].conversions[input_NDS].status, 'Wrong status for NDS backend conversion.'

    po3.transition
    po3.reload
    assert_equal 'PRODUCTION', po3.bp_name, 'Wrong business process for backend production order.'
    assert_equal 'AUTO CONVERSION', po3.production_orderlines[0].bp_name, 'Wrong business process for backend production orderline.'
    assert_equal Conversion::STATUS_REQUESTED, po3.production_orderlines[0].conversions[input_dHive].status, 'Wrong status for dHive backend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, po3.production_orderlines[0].conversions[input_NDS].status, 'Wrong status for NDS backend conversion.'

    fpo3.execute
    po3.execute
    fpo3.reload
    po3.reload
    assert_equal ProductionOrderline::BP_NAME_COMPLETE, fpo3.production_orderlines[input_RDF].bp_name, 'Wrong business process for RDF frontend production orderline.'
    assert_equal 'MONITOR CONVERSION', fpo3.production_orderlines[input_RDB].bp_name, 'Wrong business process for RDB frontend production orderline.'
    assert_equal ProductionOrderline::BP_NAME_COMPLETE, fpo3.production_orderlines[input_TMC].bp_name, 'Wrong business process for TMC frontend production orderline.'
    assert_equal ProductionOrderline::BP_NAME_COMPLETE, fpo3.production_orderlines[input_POI].bp_name, 'Wrong business process for POI frontend production orderline.'
    assert_equal 'AUTO CONVERSION', po3.production_orderlines[0].bp_name, 'Wrong business process for backend production orderline.'
    assert_empty fpo3.production_orderlines[input_RDF].conversions, 'Wrong number of conversions for RDF frontend production orderline.'
    assert_equal 1, fpo3.production_orderlines[input_RDB].conversions.count, 'Wrong number of conversions for RDB frontend production orderline.'
    assert_empty fpo3.production_orderlines[input_TMC].conversions, 'Wrong number of conversions for TMC frontend production orderline.'
    assert_empty fpo3.production_orderlines[input_POI].conversions, 'Wrong number of conversions for POI frontend production orderline.'
    assert_equal Conversion::STATUS_CONVERSION, fpo3.production_orderlines[input_RDB].conversions[0].status, 'Wrong status for RDB frontend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, po3.production_orderlines[0].conversions[input_dHive].status, 'Wrong status for dHive frontend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, po3.production_orderlines[0].conversions[input_NDS].status, 'Wrong status for NDS frontend conversion.'
    assert_equal 1, fpo3.production_orderlines[input_RDB].conversions[0].conversion_jobs.count, 'Wrong number of conversion jobs for RDB frontend conversions.'
    assert_equal Job::STATUS_QUEUED, fpo3.production_orderlines[input_RDB].conversions[0].conversion_jobs[0].status, 'Wrong status for RDB frontend conversion job.'
    assert_empty fpo3.production_orderlines[input_RDB].conversions[0].conversion_databases, 'Wrong number of conversion databases for RDB frontend conversions.'
    assert_empty po3.production_orderlines[0].conversions[input_dHive].conversion_jobs, 'Wrong number of conversion jobs for dHive backend conversion.'
    assert_empty po3.production_orderlines[0].conversions[input_NDS].conversion_jobs, 'Wrong number of conversion jobs for NDS backend conversion.'

    ProductionOrder.schedule
    fpo3.reload
    po3.reload
    assert_equal Job::STATUS_PROCESSING, fpo3.production_orderlines[input_RDB].conversions[0].conversion_jobs[0].status, 'Wrong status for RDB frontend conversion job.'

    fpo3.execute
    po3.execute
    fpo3.reload
    po3.reload
    assert_equal Conversion::STATUS_CONVERSION, fpo3.production_orderlines[input_RDB].conversions[0].status, 'Wrong status for RDB frontend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, po3.production_orderlines[0].conversions[input_dHive].status, 'Wrong status for dHive backend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, po3.production_orderlines[0].conversions[input_NDS].status, 'Wrong status for NDS backend conversion.'
    assert_empty fpo3.production_orderlines[input_RDB].conversions[0].conversion_databases, 'Wrong number of conversion databases for RDB frontend conversion.'

    ProductionOrder.schedule
    fpo3.reload
    po3.reload
    assert_equal Conversion::STATUS_CONVERSION, fpo3.production_orderlines[input_RDB].conversions[0].status, 'Wrong status for RDB frontend conversion.'
    assert_equal Conversion::STATUS_REQUESTED,po3.production_orderlines[0].conversions[input_dHive].status, 'Wrong status for dHive backend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, po3.production_orderlines[0].conversions[input_NDS].status, 'Wrong status for NDS backend conversion.'
    assert_equal Job::STATUS_COMPLETE, fpo3.production_orderlines[input_RDB].conversions[0].conversion_jobs[0].status, 'Wrong status for RDB frontend conversion job.'
    assert_equal 1, fpo3.production_orderlines[input_RDB].conversions[0].conversion_databases.count, 'Wrong number of conversion databases for RDB frontend conversion.'
    assert_equal ConversionDatabase::STATUS_VALIDATING, fpo3.production_orderlines[input_RDB].conversions[0].conversion_databases[0].status, 'Wrong status for RDB frontend conversion database.'
    assert_equal 1, fpo3.production_orderlines[input_RDB].conversions[0].conversion_databases[0].validation_jobs.count, 'Wrong number of validation jobs for RDB frontend conversion database.'
    assert_equal Job::STATUS_QUEUED, fpo3.production_orderlines[input_RDB].conversions[0].conversion_databases[0].validation_jobs[0].status, 'Wrong status for RDB frontend validation job.'
    assert_empty po3.production_orderlines[0].conversions[input_dHive].conversion_jobs, 'Wrong number of conversion jobs for dHive backend conversion.'
    assert_empty po3.production_orderlines[0].conversions[input_NDS].conversion_jobs, 'Wrong number of conversion jobs for NDS backend conversion.'

    fpo3.execute
    po3.execute
    fpo3.reload
    po3.reload
    assert_equal Conversion::STATUS_CONVERSION, fpo3.production_orderlines[input_RDB].conversions[0].status, 'Wrong status for RDB frontend conversion.'
    assert_equal Conversion::STATUS_CONVERSION, po3.production_orderlines[0].conversions[input_dHive].status, 'Wrong status for dHive backend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, po3.production_orderlines[0].conversions[input_NDS].status, 'Wrong status for NDS backend conversion.'
    assert_equal 1, po3.production_orderlines[0].conversions[input_dHive].conversion_jobs.count, 'Wrong number of conversion jobs for dHive backend conversion.'
    assert_empty po3.production_orderlines[0].conversions[input_NDS].conversion_jobs, 'Wrong number of conversion jobs for NDS backend conversion.'
    assert_equal Job::STATUS_QUEUED, po3.production_orderlines[0].conversions[input_dHive].conversion_jobs[0].status, 'Wrong status for dHive backend conversion job.'

    ProductionOrder.schedule
    fpo3.reload
    po3.reload
    assert_equal Job::STATUS_PROCESSING, fpo3.production_orderlines[input_RDB].conversions[0].conversion_databases[0].validation_jobs[0].status, 'Wrong status for RDB frontend validation job.'
    assert_equal Job::STATUS_PROCESSING, po3.production_orderlines[0].conversions[input_dHive].conversion_jobs[0].status, 'Wrong status for dHive backend conversion.'
    assert_equal ConversionDatabase::STATUS_VALIDATING, fpo3.production_orderlines[input_RDB].conversions[0].conversion_databases[0].status, 'Wrong status for RDB backend conversion database.'

    ProductionOrder.schedule
    fpo3.reload
    po3.reload
    assert_equal Job::STATUS_COMPLETE, fpo3.production_orderlines[input_RDB].conversions[0].conversion_databases[0].validation_jobs[0].status, 'Wrong status for RDB frontend validation job.'
    assert_equal ConversionDatabase::STATUS_ACCEPTED, fpo3.production_orderlines[input_RDB].conversions[0].conversion_databases[0].status, 'Wrong status for RDF frontend conversion.'
    assert_equal Conversion::STATUS_CONVERSION, fpo3.production_orderlines[input_RDB].conversions[0].status, 'Wrong status for RDB frontend conversion.'
    assert_equal Job::STATUS_COMPLETE, po3.production_orderlines[0].conversions[input_dHive].conversion_jobs[0].status, 'Wrong status for dHive backend conversion job.'
    assert_equal 1, po3.production_orderlines[0].conversions[input_dHive].conversion_databases.count, 'Wrong number of conversion databases for dHive backend conversion.'
    assert_empty po3.production_orderlines[0].conversions[input_NDS].conversion_databases, 'Wrong number of conversion databases for NDS backend conversion.'
    assert_equal ConversionDatabase::STATUS_NOT_VALIDATED, po3.production_orderlines[0].conversions[input_dHive].conversion_databases[0].status, 'Wrong status for dHive backend conversion database.'
    assert_equal 'PRODUCTION', fpo3.bp_name, 'Wrong business process for frontend production order.'
    assert_equal 'MONITOR CONVERSION', fpo3.production_orderlines[input_RDB].bp_name, 'Wrong business process for RDB frontend production orderline.'
    assert_equal Conversion::STATUS_CONVERSION, po3.production_orderlines[0].conversions[input_dHive].status, 'Wrong status for dHive backend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, po3.production_orderlines[0].conversions[input_NDS].status, 'Wrong status for NDS backend conversion.'

    fpo3.execute
    po3.execute
    fpo3.reload
    po3.reload
    assert_equal Conversion::STATUS_FINISHED, fpo3.production_orderlines[input_RDB].conversions[0].status, 'Wrong status for RDB frontend conversion.'
    assert_equal ProductionOrderline::BP_NAME_COMPLETE, fpo3.production_orderlines[input_RDB].bp_name, 'Wrong business process for RDB frontend production orderline.'
    assert_equal ProductionOrder::BP_NAME_COMPLETE, fpo3.bp_name, 'Wrong business process for frontend production order.'
    assert_equal Conversion::STATUS_FINISHED, po3.production_orderlines[0].conversions[input_dHive].status, 'Wrong status for dHive backend conversion.'
    assert_equal Conversion::STATUS_CONVERSION, po3.production_orderlines[0].conversions[input_NDS].status, 'Wrong status for NDS backend conversion.'
    assert_equal 1, po3.production_orderlines[0].conversions[input_NDS].conversion_jobs.count, 'Wrong number of conversion jobs for NDS backend conversion.'
    assert_equal Job::STATUS_QUEUED, po3.production_orderlines[0].conversions[input_NDS].conversion_jobs[0].status, 'Wrong status for NDS backend conversion job.'

    ProductionOrder.schedule
    fpo3.reload
    po3.reload
    assert_equal Job::STATUS_PROCESSING, po3.production_orderlines[0].conversions[input_NDS].conversion_jobs[0].status, 'Wrong status for NDS backend conversion job.'

    fpo3.execute
    po3.execute
    fpo3.reload
    po3.reload
    assert_equal Conversion::STATUS_FINISHED, po3.production_orderlines[0].conversions[input_dHive].status, 'Wrong status for dHive backend conversion.'
    assert_equal Conversion::STATUS_CONVERSION, po3.production_orderlines[0].conversions[input_NDS].status, 'Wrong status for NDS backend conversion.'

    ProductionOrder.schedule
    fpo3.reload
    po3.reload
    assert_equal Job::STATUS_COMPLETE, po3.production_orderlines[0].conversions[input_NDS].conversion_jobs[0].status, 'Wrong status for NDS backend conversion job.'
    assert_equal 1, po3.production_orderlines[0].conversions[input_NDS].conversion_databases.count, 'Wrong number of conversion databases for NDS backend conversion.'
    assert_equal ConversionDatabase::STATUS_NOT_VALIDATED, po3.production_orderlines[0].conversions[input_NDS].conversion_databases[0].status, 'Wrong status for NDS backend conversion database.'
    assert_equal 'AUTO CONVERSION', po3.production_orderlines[0].bp_name, 'Wrong business process for backend production orderline.'
    assert_equal Conversion::STATUS_CONVERSION, po3.production_orderlines[0].conversions[input_NDS].status, 'Wrong status for NDS backend conversion.'

    fpo3.execute
    po3.execute
    fpo3.reload
    po3.reload
    assert_equal Conversion::STATUS_FINISHED, po3.production_orderlines[0].conversions[input_dHive].status, 'Wrong status for dHive backend conversion.'
    assert_equal Conversion::STATUS_FINISHED, po3.production_orderlines[0].conversions[input_NDS].status, 'Wrong status for NDS backend conversion.'
    assert_equal 'MASTERING', po3.production_orderlines[0].bp_name, 'Wrong business process for backend production orderline.'
    assert_equal 'PRODUCTION', po3.bp_name, 'Wrong business process for backend production order.'

    # STEP 4
    # Test the backend order with all databases already available
    production_order = ProductionOrder.new(project_id: project.id, use_test_versions_yn: false, ordertype_id: ordertype.id, use_dakota_simulator_yn: true, create_frontend_order_yn: true, allow_production: true)
    force_create = []
    results = product_design.create_production_order(product.volumeid, production_order, conversiontool.id, cvtool_bundle.id, true, force_create, use_test_versions_yn = false, nil, use_dakota_simulator_yn = true)
    fpo4 = results[0]
    po4 = results[1]

    assert_not_nil fpo4, 'Frontend order not found'
    assert_not_nil po4, 'Backend order not found'
    assert_equal 'PRODUCTION', fpo4.bp_name, 'Wrong business process for frontend production order.'
    assert_equal 'AUTO CONVERSION', po4.bp_name, 'Wrong business process for backend production order.'
    assert_equal 4, fpo4.production_orderlines.count, 'Wrong number of production orderlines for frontend production order.'
    assert_equal 1, po4.production_orderlines.count, 'Wrong number of production orderlines for backend production order.'
    assert_equal 'SELECT DATARELEASE', fpo4.production_orderlines[input_RDF].bp_name, 'Wrong business process for RDF frontend production orderline.'
    assert_equal 'SELECT DATARELEASE', fpo4.production_orderlines[input_RDB].bp_name, 'Wrong business process for RDB frontend production orderline.'
    assert_equal 'SELECT DATARELEASE', fpo4.production_orderlines[input_TMC].bp_name, 'Wrong business process for TMC frontend production orderline.'
    assert_equal 'SELECT DATARELEASE', fpo4.production_orderlines[input_POI].bp_name, 'Wrong business process for POI frontend production orderline.'
    assert_equal ProductionOrderline::BP_NAME_CREATED, po4.production_orderlines[0].bp_name, 'Wrong business process for backend production orderline.'
    assert_equal 1, fpo4.production_orderlines[input_RDF].conversions.count, 'Wrong number of conversions for RDF frontend production orderline.'
    assert_equal 1, fpo4.production_orderlines[input_RDB].conversions.count, 'Wrong number of conversions for RDB frontend production orderline.'
    assert_equal 1, fpo4.production_orderlines[input_TMC].conversions.count, 'Wrong number of conversions for TMC frontend production orderline.'
    assert_equal 1, fpo4.production_orderlines[input_POI].conversions.count, 'Wrong number of conversions for POI frontend production orderline.'
    assert_equal 2, po4.production_orderlines[0].conversions.count, 'Wrong number of conversions for backend production orderline.'
    assert_equal Conversion::STATUS_REQUESTED, fpo4.production_orderlines[input_RDF].conversions[0].status, 'Wrong status for RDF frontend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, fpo4.production_orderlines[input_RDB].conversions[0].status, 'Wrong status for RDB frontend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, fpo4.production_orderlines[input_TMC].conversions[0].status, 'Wrong status for TMC frontend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, fpo4.production_orderlines[input_POI].conversions[0].status, 'Wrong status for POI frontend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, po4.production_orderlines[0].conversions[input_dHive].status, 'Wrong status for dHive backend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, po4.production_orderlines[0].conversions[input_NDS].status, 'Wrong status for NDS backend conversion.'

    po4.transition
    po4.reload
    assert_equal 'PRODUCTION', po4.bp_name, 'Wrong business process for backend production order.'
    assert_equal 'AUTO CONVERSION',po4.production_orderlines[0].bp_name, 'Wrong business process for backend production orderline.'
    assert_equal Conversion::STATUS_REQUESTED, po4.production_orderlines[0].conversions[input_dHive].status, 'Wrong status for dHive backend conversion.'
    assert_equal Conversion::STATUS_REQUESTED, po4.production_orderlines[0].conversions[input_NDS].status, 'Wrong status for NDS backend conversion.'

    fpo4.execute
    po4.execute
    fpo4.reload
    po4.reload
    assert_empty fpo4.production_orderlines[input_RDF].conversions, 'Wrong number of conversions for RDF frontend production orderline.'
    assert_empty fpo4.production_orderlines[input_RDB].conversions, 'Wrong number of conversions for RDB frontend production orderline.'
    assert_empty fpo4.production_orderlines[input_TMC].conversions, 'Wrong number of conversions for TMC frontend production orderline.'
    assert_empty fpo4.production_orderlines[input_POI].conversions, 'Wrong number of conversions for POI frontend production orderline.'
    assert_equal ProductionOrderline::BP_NAME_COMPLETE, fpo4.production_orderlines[input_RDF].bp_name, 'Wrong business process for RDF frontend production orderline.'
    assert_equal ProductionOrderline::BP_NAME_COMPLETE, fpo4.production_orderlines[input_RDB].bp_name, 'Wrong business process for RDB frontend production orderline.'
    assert_equal ProductionOrderline::BP_NAME_COMPLETE, fpo4.production_orderlines[input_TMC].bp_name, 'Wrong business process for TMC frontend production orderline.'
    assert_equal ProductionOrderline::BP_NAME_COMPLETE, fpo4.production_orderlines[input_POI].bp_name, 'Wrong business process for POI frontend production orderline.'
    assert_equal ProductionOrder::BP_NAME_COMPLETE, fpo4.bp_name, 'Wrong business process for frontend production order.'
    assert_empty po4.production_orderlines[0].conversions, 'Wrong number of conversions for backend production orderline.'
    assert_equal 'MASTERING', po4.production_orderlines[0].bp_name, 'Wrong business process for backend production orderline.'
    assert_equal 'PRODUCTION', po4.bp_name, 'Wrong business process for backend production order.'
    assert_not_nil po4.production_orderlines[0].chosen_conversion, 'Backend production orderline should have a chosen conversion.'
  end
end
