require 'test_helper'

class ShippingOrderTest < ActiveSupport::TestCase
  test 'article validation' do
    so = shipping_orders(:DEFAULT_ORDER)
    article = so.article
    so.article = nil
    assert_raises(Exception){so.save!}
    so.article = article
    assert so.save
  end

  test 'po_reference validation' do
    so = shipping_orders(:DEFAULT_ORDER)
    so.po_reference = nil
    assert_raises(Exception){so.save!}
    so.po_reference = 'X'
    assert so.save
  end

  test 'ordering_customer_depot validation' do
    so = shipping_orders(:DEFAULT_ORDER)
    so.ordering_customer_depot = nil
    assert_raises(Exception){so.save!}
    so.ordering_customer_depot = customer_depots(:DUMMY_DEPOT)
    assert so.save
  end

  test 'shipping_customer_depot validation' do
    so = shipping_orders(:DEFAULT_ORDER)
    so.shipping_customer_depot = nil
    assert_raises(Exception){so.save!}
    so.shipping_customer_depot = customer_depots(:DUMMY_DEPOT)
    assert so.save
  end

  test 'invoicing_customer_depot validation' do
    so = shipping_orders(:DEFAULT_ORDER)
    so.invoicing_customer_depot = nil
    assert_raises(Exception){so.save!}
    so.invoicing_customer_depot = customer_depots(:DUMMY_DEPOT)
    assert so.save
  end

  test 'status_str' do
    so = shipping_orders(:DEFAULT_ORDER)
    so.status = ShippingOrder::STATUS_DRAFT
    assert_equal 'Draft', so.status_str
    so.status = ShippingOrder::STATUS_RECEIVED
    assert_equal 'Received', so.status_str
    so.status = ShippingOrder::STATUS_ALLOCATED
    assert_equal 'Allocated', so.status_str
    so.status = ShippingOrder::STATUS_SHIPPED
    assert_equal 'Shipped', so.status_str
    so.status = ShippingOrder::STATUS_DELIVERED
    assert_equal 'Delivered', so.status_str
    so.status = ShippingOrder::STATUS_INVOICED
    assert_equal 'Invoiced', so.status_str
    so.status = ShippingOrder::STATUS_PAID
    assert_equal 'Paid', so.status_str
    so.status = ShippingOrder::STATUS_CANCELLED
    assert_equal 'Cancelled', so.status_str
  end

  test 'status_array' do
    assert_equal 8, ShippingOrder.status_array.compact.count, 'Status_array does not have the correct number of non-nil elements.'
    assert_equal 'Draft', ShippingOrder.status_array.first, 'Incorrect first element of the status_array.'
    assert_equal 'Cancelled', ShippingOrder.status_array.last, 'Incorrect last element of the status_array.'
  end

  test 'status_hash' do
    assert_equal 8, ShippingOrder.status_hash.size, 'Status_hash does not have the correct number of elements.'
    assert_equal ['Draft', ShippingOrder::STATUS_DRAFT], ShippingOrder.status_hash.first
    assert_equal ['Cancelled', ShippingOrder::STATUS_CANCELLED], ShippingOrder.status_hash.last
    assert_equal ['Invoiced', ShippingOrder::STATUS_INVOICED], ShippingOrder.status_hash[5], 'The 5th element of status_hash should point to Invoiced.'
  end

  test 'allocate' do
    so = shipping_orders(:DEFAULT_ORDER)
    assert_raises(Exception){so.allocate}
    so.status = ShippingOrder::STATUS_ALLOCATED
    assert_raises(Exception){so.allocate}
    so.status = ShippingOrder::STATUS_RECEIVED
    assert so.allocate, 'allocate did not return true.'
    assert_equal ShippingOrder::STATUS_ALLOCATED, so.status
  end

  test 'all_lines_equal_status' do
    so = shipping_orders(:FINISHED_ORDER)
    assert so.all_lines_equal_status?, 'All shipping orderlines should have the same status.'
    so = shipping_orders(:DEFAULT_ORDER)
    assert_not so.all_lines_equal_status?, 'The shipping orderlines should not all have the same status.'
  end

  test 'cancel' do
    so = shipping_orders(:DEFAULT_ORDER)
    so.cancel
    assert_equal ShippingOrder::STATUS_CANCELLED, so.status, 'Status should be Cancelled.'
    assert_equal 3, so.shipping_orderlines.where(status: ShippingOrder::STATUS_CANCELLED).count, '3 of the 4 shipping orderlines should be in status cancelled.'
  end

  test 'cancel on finished shipping_order' do
    so = shipping_orders(:FINISHED_ORDER)
    so.cancel
    assert_equal ShippingOrder::STATUS_PAID, so.status, 'Status should remain Paid.'
    assert_empty so.shipping_orderlines.where(status: ShippingOrder::STATUS_CANCELLED), 'no shipping orderlines should be cancelled.'
  end

end
