require 'test_helper'

class ToolSpecsTest < ActiveSupport::TestCase
  test 'synchronize bundle' do
    old_storeDH = executables(:STOREDH)
    old_NOKFuel2DH = executables(:NOKFUEL2DH)
    old_DHGateways = executables(:DHGateways)

    bundles = '1409'
    release = '20141104'
    bundle = 'ntg55h_common'
    tool_spec_path = File.join(File.dirname(__FILE__), '..', 'fixtures', 'Bundles_1409.toolspec')
    contents = File.read(tool_spec_path)
    mail_exception = false
    ToolSpec.synchronize_bundle(bundles, bundle, release, contents, mail_exception)

    # test storeDH, which is an existing, unchanged executable
    storeDH = Executable.find_by_name('StoreDH')
    assert_not_nil storeDH, 'Executable \'StoreDH\' not found.'
    assert_equal storeDH.version, old_storeDH.version, 'Expecting no version change for \'StoreDH\'.'
    assert_equal storeDH.status, 'approved', 'Wrong status for \'StoreDH\''
    assert_equal JSON.parse(storeDH.input).count, 1, 'Wrong number of inputs for \'StoreDH\'.'
    assert_equal JSON.parse(storeDH.input)[0]['datatype'], 'dHive', 'Wrong input datatype for \'StoreDH\'.'
    assert_equal JSON.parse(storeDH.output).count, 1, 'Wrong number of outputs for \'StoreDH\'.'
    assert_equal JSON.parse(storeDH.output)[0]['datatype'], 'dHive', 'Wrong output datatype for \'StoreDH\'.'
    assert_equal JSON.parse(storeDH.parameters)['parameters'].count, 2, 'Wrong number of parameters for \'StoreDH\'.'

    # test DHTollPOI2DHTOB, which is a new executable
    dHTollPOI2DHTOB = Executable.find_by_name('DHTollPOI2DHTOB')
    assert_not_nil dHTollPOI2DHTOB, 'Executable \'DHTollPOI2DHTOB\' not found.'
    assert_equal dHTollPOI2DHTOB.version, 2, 'Wrong version number for \'DHTollPOI2DHTOB\'.'
    assert_equal dHTollPOI2DHTOB.status, 'new', 'Wrong status for \'DHTollPOI2DHTOB\'.'
    assert_equal JSON.parse(dHTollPOI2DHTOB.input).count, 1, 'Wrong number of inputs for \'DHTollPOI2DHTOB\'.'
    assert_equal JSON.parse(dHTollPOI2DHTOB.input)[0]['datatype'], 'dHive', 'Wrong input datatype for \'DHTollPOI2DHTOB\'.'
    assert_equal JSON.parse(dHTollPOI2DHTOB.output).count, 1, 'Wrong number of outputs for \'DHTollPOI2DHTOB\'.'
    assert_equal JSON.parse(dHTollPOI2DHTOB.output)[0]['datatype'], 'dHive', 'Wrong output datatype for \'DHTollPOI2DHTOB\'.'
    assert_equal JSON.parse(dHTollPOI2DHTOB.parameters)['parameters'].count, 0, 'Wrong number of parameters for \'DHTollPOI2DHTOB\'.'

    # test NOKFuel2DH, which contains an unsupported input
    nOKFuel2DH = Executable.find_by_name('NOKFuel2DH')
    assert_not_nil nOKFuel2DH, 'Executable \'NOKFuel2DH\' not found.'
    assert_equal nOKFuel2DH.version, old_NOKFuel2DH.version + 1, 'Wrong version number for \'NOKFuel2DH\'.'
    assert_equal nOKFuel2DH.status, 'draft', 'Wrong status for \'NOKFuel2DH\'.'
    assert_equal nOKFuel2DH.input, old_NOKFuel2DH.input, 'Input of executable \'NOKFuel2DH\' imported unsupported datatype.'

    # test DHGateways, which is a changed executable
    dHGateways = Executable.find_by_name('DHGateways')
    assert_not_nil dHGateways, 'Executable \'DHGateways\' not found.'
    assert_equal dHGateways.version, old_DHGateways.version + 1, 'Wrong version number for \'DHGateways\'.'
    assert_equal dHGateways.status, 'approved', 'Wrong status for \'DHGateways\'.'
    assert_equal JSON.parse(dHGateways.input).count, JSON.parse(old_DHGateways.input).count + 1, 'Wrong number of inputs for \'DHGateways\'.'
    assert_equal JSON.parse(dHGateways.output).count, JSON.parse(old_DHGateways.output).count + 1, 'Wrong number of outputs for \'DHGateways\'.'
    assert_equal JSON.parse(dHGateways.parameters)['parameters'].count, JSON.parse(old_DHGateways.parameters)['parameters'].count + 1, 'Wrong number of parameters for \'DHGateways\'.'

    # test parameterRef
    dhAddMapSymbols = Executable.find_by_name('DHAddMapSymbols')
    assert_not_nil dhAddMapSymbols, 'Executable \'DHAddMapSymbols\' not found.'
    assert_equal 4, JSON.parse(dhAddMapSymbols.parameters)['parameters'].count, 'DHAddMapSymbols should contain four parameter references.'

    # test featuregroup
    ndsCreateMaster = Executable.find_by_name('NDSCreateMaster')
    assert_not_nil ndsCreateMaster, 'Executable \'NDSCreateMaster\' not found.'
    assert_not JSON.parse(ndsCreateMaster.parameters)['parameters'].select{|x| x['name'] == 'BaselineMapId' }.empty?, 'Parameter \'BaselineMapId\', which is part of a featuregroup, not found.'

  end

  test 'synchronize featuregroupRef' do
    # dev_telematics is the only branch using featuregroupRefs at the moment.
    # Unfortunately it supports other tools than the 1409 bundle, so using a dev_telematics bundle instead of the 1409 bundle would break
    # some of the other test cases. Hence the use of both.
    bundles = 'dev_telematics'
    release = '20150708_1'
    bundle = 'sfm'
    tool_spec_path = File.join(File.dirname(__FILE__), '..', 'fixtures', 'Bundles_dev_telematics.toolspec')
    contents = File.read(tool_spec_path)
    mail_exception = false
    ToolSpec.synchronize_bundle(bundles, bundle, release, contents, mail_exception)

    sfmSensorClustering = Executable.find_by_name('SFMSensorClustering')
    assert_not_nil sfmSensorClustering, 'Executable \'SFMSensorClustering\' nto found.'
    assert_not JSON.parse(sfmSensorClustering.parameters)['parameters'].select{ |x| x['name'] == 'SFMRawSensorSpeed' }.empty?, 'Parameter \'SFMRawSensorSpeed\', which is part of a featuregroupRef, not found.'

    # # TEST CASE FAILS as it needs the 'config.active_record.schema_format = :sql' in development.rb
    # # unfortunately enabling this breaks other tests at the moment so for now this testcase is disabled.
    # # # test case sensitive
    # lTEF2DH_1 = Executable.find_by_name('ltef2DH')
    # lTEF2DH_2 = Executable.find_by_name('LTEF2DH')
    # assert_not_equal lTEF2DH_1.id, lTEF2DH_2.id, 'Executables are not case sensitive.'
  end

  # TODO: Test the other class methods
end
